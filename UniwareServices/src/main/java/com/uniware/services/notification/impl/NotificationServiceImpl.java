/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.services.notification.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IEmailProvider;
import com.unifier.scraper.sl.runtime.IScriptProvider;
import com.unifier.scraper.sl.runtime.ISmsProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.sms.ISmsService;
import com.uniware.core.api.notification.ProcessPendingNotificationsRequest;
import com.uniware.core.api.notification.ProcessPendingNotificationsResponse;
import com.uniware.core.api.notification.UpdateNotificationRequest;
import com.uniware.core.api.notification.UpdateNotificationResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelConfigurationParameter;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.ChannelConnectorParameter;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.Notification;
import com.uniware.core.entity.Notification.Status;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.Source.Code;
import com.uniware.core.entity.VendorInvoice;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.notification.INotificationDao;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.inflow.IInflowService;
import com.uniware.services.inflow.IVendorInvoiceService;
import com.uniware.services.material.IMaterialService;
import com.uniware.services.notification.INotificationService;
import com.uniware.services.putaway.IPutawayService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tasks.notifications.NotificationsProcessor.PendingNotificationStatus;

/**
 * @author singla
 */
@Service
public class NotificationServiceImpl implements INotificationService {

    private static final String   SERVER_DOWN_MESSAGE = "SERVER DOWN";
    private static final Logger   LOG                 = LoggerFactory.getLogger(NotificationServiceImpl.class);

    @Autowired
    private INotificationDao      notificationDao;

    @Autowired
    private ISaleOrderService     saleOrderService;

    @Autowired
    private IShippingService      shippingService;

    @Autowired
    private IDispatchService      dispatchService;

    @Autowired
    private IPurchaseDao          purchaseDao;

    @Autowired
    private ApplicationContext    applicationContext;

    @Autowired
    private IReturnsService       returnsService;

    @Autowired
    private IMaterialService      materialService;

    @Autowired
    private IInflowService        inflowService;

    @Autowired
    private IReturnsService       returnService;

    @Autowired
    private IPutawayService       putawayService;

    @Autowired
    private IVendorInvoiceService vendorInvoiceService;

    @Autowired
    private IEmailService         emailService;

    @Autowired
    private ISmsService           smsService;

    @Transactional(readOnly = true)
    @Override
    public List<Notification> getPendingNotifications(int batchSize, int maxId) {
        return notificationDao.getPendingNotifications(batchSize, maxId);
    }

    @Transactional(readOnly = true)
    @Override
    public PendingNotificationStatus getPendingNotificationsStatus() {
        return notificationDao.getPendingNotificationsStatus();
    }

    @Override
    @Transactional
    public Notification updateNotification(Notification notification) {
        return notificationDao.updateNotification(notification);
    }

    @Override
    public ProcessPendingNotificationsResponse processPendingNotifications(ProcessPendingNotificationsRequest request) {
        ProcessPendingNotificationsResponse response = new ProcessPendingNotificationsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Map<String, Map<String, List<Notification>>> channelToGroupToNotifications = new HashMap<String, Map<String, List<Notification>>>();
            // Group the notifications by channel and group identifier.
            for (Notification notification : request.getPendingNotifications()) {
                Channel channel = getNotificationChannel(notification);
                if (request.getChannelsToProcess().contains(channel.getCode())) {
                    Map<String, List<Notification>> groupToNotifications = channelToGroupToNotifications.get(channel.getCode());
                    if (groupToNotifications == null) {
                        groupToNotifications = new LinkedHashMap<>();
                        channelToGroupToNotifications.put(channel.getCode(), groupToNotifications);
                    }
                    List<Notification> notifications = groupToNotifications.get(notification.getGroupIdentifier());
                    if (notifications == null) {
                        notifications = new ArrayList<>();
                        groupToNotifications.put(notification.getGroupIdentifier(), notifications);
                    }
                    notifications.add(notification);
                }
            }

            // Now start processing.
            for (Entry<String, Map<String, List<Notification>>> e : channelToGroupToNotifications.entrySet()) {
                ChannelCache channelCache = CacheManager.getInstance().getCache(ChannelCache.class);
                Channel channel = channelCache.getChannelByCode(e.getKey());
                int processNotificationCount = channelCache.getProcessNotificationCount(channel.getCode());
                try {
                    if (processNotificationCount == 1) {
                        for (List<Notification> groupNotifications : e.getValue().values()) {
                            processNotifications(context, channel, groupNotifications);
                        }
                    } else {
                        Collection<List<Notification>> notications = e.getValue().values();
                        Iterator<List<List<Notification>>> iterator = CollectionUtils.sublistIterator(CollectionUtils.asList(notications), processNotificationCount);
                        while (iterator.hasNext()) {
                            processGroupNotifications(context, channel, iterator.next());
                        }
                    }
                } catch (Exception ex) {
                    LOG.error("Error processing notifications for channel: {}, message: {}", channel.getName(), ex.getMessage());
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional
    @Override
    public Channel getNotificationChannel(Notification notification) {
        String channelCode;
        if (Notification.EntityNames.SaleOrderItem.name().equals(notification.getEntity())) {
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemById(Integer.parseInt(notification.getIdentifier()));
            channelCode = saleOrderItem.getSaleOrder().getChannel().getCode();
        } else if (Notification.EntityNames.ShippingPackage.name().equals(notification.getEntity())) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageById(Integer.parseInt(notification.getIdentifier()));
            channelCode = shippingPackage.getSaleOrder().getChannel().getCode();
        } else if (Notification.EntityNames.PurchaseOrder.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.SaleOrder.name().equals(notification.getEntity())) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderById(Integer.parseInt(notification.getIdentifier()));
            channelCode = saleOrder.getChannel().getCode();
        } else if (Notification.EntityNames.Reshipment.name().equals(notification.getEntity())) {
            Reshipment reshipment = returnsService.getReshipmentById(Integer.parseInt(notification.getIdentifier()));
            channelCode = reshipment.getSaleOrder().getChannel().getCode();
        } else if (Notification.EntityNames.ShippingManifest.name().equals(notification.getEntity())) {
            ShippingManifest shippingManifest = dispatchService.getShippingManifestById(Integer.parseInt(notification.getIdentifier()));
            channelCode = shippingManifest.getChannel().getCode();
        } else if (Notification.EntityNames.InflowReceipt.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.ReturnManifest.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.ReversePickup.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.Gatepass.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.Putaway.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else if (Notification.EntityNames.VendorInvoice.name().equals(notification.getEntity())) {
            channelCode = Code.CUSTOM.name();
        } else {
            throw new RuntimeException("Invalid notifcation identifier");
        }
        return CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(channelCode);
    }

    @Transactional
    @Override
    public NotificationDetail getNotificationDetail(Notification notification, Channel channel) {
        NotificationDetail notificationDetail;
        if (Notification.EntityNames.SaleOrderItem.name().equals(notification.getEntity())) {
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), saleOrderItem);
        } else if (Notification.EntityNames.ShippingPackage.name().equals(notification.getEntity())) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), shippingPackage);
        } else if (Notification.EntityNames.PurchaseOrder.name().equals(notification.getEntity())) {
            PurchaseOrder purchaseOrder = purchaseDao.getPurchaseOrderById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), purchaseOrder);
        } else if (Notification.EntityNames.SaleOrder.name().equals(notification.getEntity())) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), saleOrder);
        } else if (Notification.EntityNames.Reshipment.name().equals(notification.getEntity())) {
            Reshipment reshipment = returnsService.getReshipmentById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), reshipment);
        } else if (Notification.EntityNames.ShippingManifest.name().equals(notification.getEntity())) {
            ShippingManifest shippingManifest = dispatchService.getShippingManifestById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), shippingManifest);
        } else if (Notification.EntityNames.InflowReceipt.name().equals(notification.getEntity())) {
            InflowReceipt inflowReceipt = inflowService.getInflowReceiptById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), inflowReceipt);
        } else if (Notification.EntityNames.ReturnManifest.name().equals(notification.getEntity())) {
            ReturnManifest returnManifest = returnService.getReturnManifestById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), returnManifest);
        } else if (Notification.EntityNames.ReversePickup.name().equals(notification.getEntity())) {
            ReversePickup reversePickup = returnsService.getReversePickupById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), reversePickup);
        } else if (Notification.EntityNames.Gatepass.name().equals(notification.getEntity())) {
            OutboundGatePass outboundGatePass = materialService.getGatePassById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), outboundGatePass);
        } else if (Notification.EntityNames.Putaway.name().equals(notification.getEntity())) {
            Putaway putaway = putawayService.getPutawayById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), putaway);
        } else if (Notification.EntityNames.VendorInvoice.name().equals(notification.getEntity())) {
            VendorInvoice vendorInvoice = vendorInvoiceService.getVendorInvoiceById(Integer.parseInt(notification.getIdentifier()));
            notificationDetail = new NotificationDetail(notification, channel.getCode(), vendorInvoice);
        } else {
            throw new RuntimeException("Invalid notifcation identifier");
        }
        return notificationDetail;
    }

    @Transactional(readOnly = true)
    public Notification getFailedNotification(String groupIdentifier) {
        return notificationDao.getFailedNotification(groupIdentifier);
    }

    @Override
    public boolean processNotifications(ValidationContext context, Channel channel, List<Notification> groupNotifications) {
        boolean someoneInGroupFailed = false;
        LOG.info("Processing notification group: {}", groupNotifications.get(0).getGroupIdentifier());
        if (getFailedNotification(groupNotifications.get(0).getGroupIdentifier()) != null) {
            // If any if the earlier notifications of this group failed in past, this entire group should go in WAITING state.
            someoneInGroupFailed = true;
        }
        for (Notification notification : groupNotifications) {
            someoneInGroupFailed = processNotification(context, channel, notification, someoneInGroupFailed);
        }
        return true;
    }

    @Transactional
    public boolean processNotification(ValidationContext context, Channel channel, Notification notification, boolean someoneInGroupFailed) {
        NotificationDetail notificationDetail = getNotificationDetail(notification, channel);
        if (!channel.isNotificationsEnabled()) {
            notificationDetail.getNotification().setStatusCode(Status.SUCCESS.name());
        } else if (someoneInGroupFailed) {
            notificationDetail.getNotification().setStatusCode(Status.WAITING.name());
        } else {
            try {
                processSingleNotification(channel, notificationDetail);
                notificationDetail.getNotification().setStatusCode(Status.SUCCESS.name());
            } catch (Exception ex) {
                LOG.error("Error executing script for notification: {}" + notificationDetail.getNotification());
                LOG.error("Stack trace: ", ex);
                context.addError(WsResponseCode.NOTIFICATION_ERROR, "Entity: " + notificationDetail.getNotification().getEntity() + ", Identifier: "
                        + notificationDetail.getNotification().getIdentifier() + ", Message: " + ex.getMessage());
                if (SERVER_DOWN_MESSAGE.equals(ex.getMessage()) || (ex.getCause() instanceof HttpTransportException)) {
                    // If server is down, don't update status.
                    throw new RuntimeException("Server down for channel: " + channel.getName() + ", will retry in next run.");
                } else {
                    notificationDetail.getNotification().setFailureReason(getFailureMessage(ex.getMessage(), notificationDetail));
                    notificationDetail.getNotification().setStatusCode(Status.FAILED.name());
                    // Skip the entire group if any one notification in a group fails.
                    someoneInGroupFailed = true;
                }
            }
        }
        notificationDao.updateNotification(notificationDetail.getNotification());
        return someoneInGroupFailed;
    }

    private String getFailureMessage(String message, NotificationDetail notificationDetail) {
        String messagePrefix;
        Notification notification = notificationDetail.getNotification();
        if (Notification.EntityNames.SaleOrderItem.name().equals(notification.getEntity())) {
            SaleOrderItem saleOrderItem = (SaleOrderItem) notificationDetail.getEntity();
            messagePrefix = "[SaleOrderCode: " + saleOrderItem.getSaleOrder().getCode() + ", SaleOrderItemCode: " + saleOrderItem.getCode() + "]";
        } else if (Notification.EntityNames.ShippingPackage.name().equals(notification.getEntity())) {
            ShippingPackage shippingPackage = (ShippingPackage) notificationDetail.getEntity();
            messagePrefix = "[SaleOrderCode: " + shippingPackage.getSaleOrder().getCode() + ", ShippingPackageCode: " + shippingPackage.getCode() + "]";
        } else if (Notification.EntityNames.PurchaseOrder.name().equals(notification.getEntity())) {
            PurchaseOrder purchaseOrder = (PurchaseOrder) notificationDetail.getEntity();
            messagePrefix = "[PurchaseOrderCode: " + purchaseOrder.getCode() + "]";
        } else if (Notification.EntityNames.SaleOrder.name().equals(notification.getEntity())) {
            SaleOrder saleOrder = (SaleOrder) notificationDetail.getEntity();
            messagePrefix = "[SaleOrderCode: " + saleOrder.getCode() + "]";
        } else if (Notification.EntityNames.Reshipment.name().equals(notification.getEntity())) {
            Reshipment reshipment = (Reshipment) notificationDetail.getEntity();
            messagePrefix = "[SaleOrderCode: " + reshipment.getSaleOrder().getCode() + ", ReshipmentCode: " + reshipment.getCode() + "]";
        } else if (Notification.EntityNames.ShippingManifest.name().equals(notification.getEntity())) {
            ShippingManifest shippingManifest = (ShippingManifest) notificationDetail.getEntity();
            messagePrefix = "[ShippingManifestCode: " + shippingManifest.getCode() + "]";
        } else if (Notification.EntityNames.InflowReceipt.name().equals(notification.getEntity())) {
            InflowReceipt inflowReceipt = (InflowReceipt) notificationDetail.getEntity();
            messagePrefix = "[PurchaseOrderCode: " + inflowReceipt.getPurchaseOrder().getCode() + ", InflowReceiptCode: " + inflowReceipt.getCode() + "]";
        } else if (Notification.EntityNames.ReturnManifest.name().equals(notification.getEntity())) {
            ReturnManifest returnManifest = (ReturnManifest) notificationDetail.getEntity();
            messagePrefix = "[ReturnManifest: " + returnManifest.getCode() + "]";
        } else if (Notification.EntityNames.ReversePickup.name().equals(notification.getEntity())) {
            ReversePickup reversePickup = returnsService.getReversePickupById(Integer.parseInt(notification.getIdentifier()));
            messagePrefix = "[ReversePickup: " + reversePickup.getCode() + "]";
        } else if (Notification.EntityNames.Gatepass.name().equals(notification.getEntity())) {
            OutboundGatePass gatepass = materialService.getGatePassById(Integer.parseInt(notification.getIdentifier()));
            messagePrefix = "[Gatepass: " + gatepass.getCode() + "]";
        } else if (Notification.EntityNames.Putaway.name().equals(notification.getEntity())) {
            Putaway putaway = putawayService.getPutawayById(Integer.parseInt(notification.getIdentifier()));
            messagePrefix = "[Putaway: " + putaway.getCode() + "]";
        } else if (Notification.EntityNames.VendorInvoice.name().equals(notification.getEntity())) {
            VendorInvoice vendorInvoice = vendorInvoiceService.getVendorInvoiceById(Integer.parseInt(notification.getIdentifier()));
            messagePrefix = "[VendorInvoice: " + vendorInvoice.getCode() + "]";
        } else {
            throw new RuntimeException("Invalid notifcation identifier");
        }
        return messagePrefix + " " + message;
    }

    @Transactional
    @Override
    public boolean processGroupNotifications(ValidationContext context, Channel channel, List<List<Notification>> notifications) {
        List<List<NotificationDetail>> notificationDetails = new ArrayList<List<NotificationDetail>>();
        for (List<Notification> nds : notifications) {
            List<NotificationDetail> groupnotificationDetails = new ArrayList<NotificationDetail>();
            notificationDetails.add(groupnotificationDetails);
            for (Notification n : nds) {
                groupnotificationDetails.add(getNotificationDetail(n, channel));
            }
        }
        String notificationScriptExceptionMessage = null;
        Map<Integer, String> successfulNotifications = new HashMap(notificationDetails.size());
        Map<Integer, String> failedNotifications = new HashMap(notificationDetails.size());
        boolean batchFailed = false;
        if (channel.isNotificationsEnabled()
                && CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), com.uniware.core.entity.Source.NOTIFICATION_SCRIPT_NAME) != null) {
            ScraperScript notificationScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.NOTIFICATION_SCRIPT_NAME);
            if (notificationScript != null) {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                seContext.addVariable("channel", channel);
                seContext.addVariable("notifications", notificationDetails);
                seContext.addVariable("failedNotifications", failedNotifications);
                seContext.addVariable("successfulNotifications", successfulNotifications);
                seContext.addVariable("applicationContext", applicationContext);
                seContext.setScriptProvider(scriptName -> CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName));
                seContext.setEmailProvider(emailMessage -> {
                    emailMessage.setTemplateName(EmailTemplate.Type.SCRIPT_INSTRUCTION_EMAIL.name());
                    emailService.send(emailMessage);
                });
                seContext.setSmsProvider(smsMessage -> smsService.send(smsMessage));
                for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                    for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                        seContext.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                    }
                }
                for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                    seContext.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                }
                try {
                    seContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    notificationScript.execute();
                } catch (ScriptExecutionException e) {
                    context.addError(WsResponseCode.NOTIFICATION_ERROR, e.getMessage());
                    notificationScriptExceptionMessage = e.getMessage();
                    batchFailed = true;
                    LOG.error("exception while sending notifications:{}", e.getMessage());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        for (List<NotificationDetail> nds : notificationDetails) {
            for (NotificationDetail nd : nds) {
                if (failedNotifications.containsKey(nd.getNotification().getId())) {
                    nd.getNotification().setStatusCode(Status.FAILED.name());
                    nd.getNotification().setFailureReason(failedNotifications.get(nd.getNotification().getId()));
                } else if (successfulNotifications.containsKey(nd.getNotification().getId())) {
                    nd.getNotification().setStatusCode(Status.SUCCESS.name());
                } else if (batchFailed) {
                    nd.getNotification().setStatusCode(Status.FAILED.name());
                    nd.getNotification().setFailureReason(notificationScriptExceptionMessage);
                } else {
                    nd.getNotification().setStatusCode(Status.SUCCESS.name());
                }
                notificationDao.updateNotification(nd.getNotification());
            }
        }
        return true;
    }

    private boolean processSingleNotification(Channel channel, NotificationDetail notification) {
        List<NotificationDetail> notifications = new ArrayList<>();
        notifications.add(notification);
        if (CacheManager.getInstance().getCache(ChannelCache.class).getScriptName(channel.getCode(), com.uniware.core.entity.Source.NOTIFICATION_SCRIPT_NAME) != null) {
            ScraperScript notificationScript = CacheManager.getInstance().getCache(ChannelCache.class).getScriptByName(channel.getCode(),
                    com.uniware.core.entity.Source.NOTIFICATION_SCRIPT_NAME);
            if (notificationScript != null) {
                ScriptExecutionContext context = ScriptExecutionContext.current();
                for (ChannelConnector channelConnector : channel.getChannelConnectors()) {
                    for (ChannelConnectorParameter channelConnectorParameter : channelConnector.getChannelConnectorParameters()) {
                        context.addVariable(channelConnectorParameter.getName(), channelConnectorParameter.getValue());
                    }
                }
                for (ChannelConfigurationParameter channelConfigurationParameter : channel.getChannelConfigurationParameters()) {
                    context.addVariable(channelConfigurationParameter.getSourceConfigurationParameterName(), channelConfigurationParameter.getValue());
                }
                context.setScriptProvider(new IScriptProvider() {

                    @Override
                    public ScraperScript getScript(String scriptName) {
                        return CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName);
                    }
                });
                context.setEmailProvider(new IEmailProvider() {
                    @Override
                    public void sendEmail(EmailMessage emailMessage) {
                        emailMessage.setTemplateName(EmailTemplate.Type.SCRIPT_INSTRUCTION_EMAIL.name());
                        emailService.send(emailMessage);
                    }
                });
                context.setSmsProvider(new ISmsProvider() {
                    @Override
                    public void sendSms(SmsMessage smsMessage) {
                        smsService.send(smsMessage);
                    }
                });
                context.addVariable("channel", channel);
                context.addVariable("notifications", notifications);
                context.addVariable("applicationContext", applicationContext);
                try {
                    context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    notificationScript.execute();
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        return true;
    }

    public static class NotificationDetail {
        private final Notification notification;
        private String             channelCode;
        private final Object       entity;
        private boolean            acknowledge;

        /**
         * @param notification
         * @param entity
         */
        public NotificationDetail(Notification notification, Object entity) {
            this.notification = notification;
            this.entity = entity;
        }

        public NotificationDetail(Notification notification, String channelCode, Object entity) {
            this.notification = notification;
            this.channelCode = channelCode;
            this.entity = entity;
        }

        /**
         * @return the notification
         */
        public Notification getNotification() {
            return notification;
        }

        /**
         * @return the entity
         */
        public Object getEntity() {
            return entity;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public boolean isAcknowledge() {
            return acknowledge;
        }

        public void setAcknowledge(boolean acknowledge) {
            this.acknowledge = acknowledge;
        }

        @Override
        public String toString() {
            return "NotificationDetail [notification=" + notification + ", channelCode=" + channelCode + ", entity=" + entity + ", acknowledge=" + acknowledge + "]";
        }
    }

    @Override
    @Transactional
    public UpdateNotificationResponse retryOrSkipNotification(UpdateNotificationRequest request) {
        UpdateNotificationResponse response = new UpdateNotificationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            notificationDao.updateFailedNotificationsStatusByIds(request.getNotificationIds(), request.getStatus());
            List<String> groupIdentifiers = notificationDao.getGroupIdentifiers(request.getNotificationIds());
            notificationDao.updateNotificationsStatusByGroupIdentifiers(groupIdentifiers, "NEW");
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public static void main(String args[]) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String[] cmd = { "/bin/sh", "-c", "cat /Users/sunny/Desktop/build.sql | grep sunny" };
        Process proc = rt.exec(cmd);
        BufferedReader is = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String line;
        while ((line = is.readLine()) != null) {
            System.out.println(line);
        }
    }
}
