/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author parijat
 */
package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.services.channel.IChannelReconciliationInvoiceSyncService;

/**
 * @author parijat
 */
public class SyncReconciliationInvoiceMessageListener extends UniwareMessageListener<SyncReconciliationInvoiceEvent> {

    private static final Logger                      logger = LoggerFactory.getLogger(SyncReconciliationInvoiceMessageListener.class);

    @Autowired
    private IChannelReconciliationInvoiceSyncService reconciliationInvoiceSyncService;

    private String                                   name;

    public SyncReconciliationInvoiceMessageListener(String name) {
        super(SyncReconciliationInvoiceEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(SyncReconciliationInvoiceEvent syncReconciliationInvoiceEvent) {

        logger.info("Processing reconciliation invoice JMS for channel code [{}]", syncReconciliationInvoiceEvent.getChannelCode());
        try {
            reconciliationInvoiceSyncService.syncChannelReconciliationInvoices(syncReconciliationInvoiceEvent);
        } catch (Exception e) {
            logger.error("Error processing reconciliation invoice sync for channel: " + syncReconciliationInvoiceEvent.getChannelCode(), e);
        }
    }

    @Override
    public String getListenerName() {
        return "ReconciliationInvoiceMessageListener";
    }

    @Override
    public String toString() {
        return "SyncReconciliationInvoiceMessageListener{" + "reconciliationInvoiceSyncService=" + reconciliationInvoiceSyncService + ", name='" + name + '\'' + '}';
    }
}