package com.uniware.services.messaging.jms;

import java.io.Serializable;
import java.util.Date;

import com.uniware.core.api.channel.SyncChannelCatalogRequest;

public class SyncChannelCatalogEvent implements Serializable {

    private static final long         serialVersionUID = 3101112988791851324L;

    private SyncChannelCatalogRequest syncChannelCatalogRequest;

    private Date                      requestTimestamp;

    public SyncChannelCatalogRequest getSyncChannelCatalogRequest() {
        return syncChannelCatalogRequest;
    }

    public void setSyncChannelCatalogRequest(SyncChannelCatalogRequest syncChannelCatalogRequest) {
        this.syncChannelCatalogRequest = syncChannelCatalogRequest;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "SyncChannelCatalogEvent{" + "syncChannelCatalogRequest=" + syncChannelCatalogRequest + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
