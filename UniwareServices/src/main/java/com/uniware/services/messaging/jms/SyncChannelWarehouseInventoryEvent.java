package com.uniware.services.messaging.jms;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by digvijaysharma on 21/01/17.
 */
public class SyncChannelWarehouseInventoryEvent implements Serializable {

    private static final long         serialVersionUID = 3101112988791851324L;

    private String            channelCode;

    private Date requestTimestamp;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "SyncChannelWarehouseInventoryEvent{" + "channelCode=" + channelCode + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
