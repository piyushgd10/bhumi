package com.uniware.services.messaging.jms;

import com.unifier.services.job.IJobResultService;
import com.unifier.services.job.IJobService;
import com.unifier.services.job.JobEvent;
import com.uniware.core.locking.ILockingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class JobEventMessageListener extends UniwareMessageListener<JobEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(JobEventMessageListener.class);

    @Autowired
    private IJobService         jobService;

    @Autowired
    private ILockingService     lockingService;

    @Autowired
    private ApplicationContext  applicationContext;

    @Autowired
    private IJobResultService   jobResultService;

    private String              name;

    public JobEventMessageListener(String name) {
        super(JobEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(JobEvent event) {
        jobService.processMessage(event);
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "JobEventMessageListener {" + "name='" + name + '\'' + "} " + super.toString();
    }
}
