/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, 07-Jan-2014
 *  @author parijat
 */
package com.uniware.services.messaging.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ThreadContextUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.messaging.jms.ActiveMQConnector.UnifierMessage;

/**
 * @author parijat
 */
public abstract class UniwareMessageListener<T> implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(UniwareMessageListener.class);

    private Class<T>            clazz;

    public UniwareMessageListener(Class<T> clazz) {
        this.clazz = clazz;
    }

    /* (non-Javadoc)
     * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
     */
    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                UnifierMessage unifierMessage = JsonUtils.stringToJson(((TextMessage) message).getText(), UnifierMessage.class);
                Tenant tenant = CacheManager.getInstance().getCache(TenantCache.class).getTenantByCode(unifierMessage.getTenantCode());// processing cache dirty message for inactive tenants also to allow reactivation of terminated tenants
                if (tenant != null) {
                    UserContext.current().setTenant(tenant);
                    UserContext.current().setLogRoutingKey(unifierMessage.getLogRoutingKey());
                    if (StringUtils.isNotEmpty(unifierMessage.getLogRoutingKey()) && !unifierMessage.getLogRoutingKey().equals(UserContext.current().getTenant().getCode())) {
                        UserContext.current().setTraceLogEnabled(true);
                    }
                    ThreadContextUtils.setThreadMetadata("CONSUMER-", UserContext.current());
                    if (StringUtils.isNotBlank(unifierMessage.getFacilityCode())) {
                        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(unifierMessage.getFacilityCode());
                        if (facility != null) {
                            UserContext.current().setFacility(facility);
                        }
                    }
                    T messageObject = JsonUtils.stringToJson(unifierMessage.getObjectJson(), clazz);
                    processMessage(messageObject);
                }
            } catch (JMSException e) {
                logger.error("Error in processing JMS message.", e);
            } catch (Throwable e) {
                logger.error("Error in processing message {}", message);
            } finally {
                UserContext.destroy();
            }
        }
    }

    public abstract void processMessage(T messageObject);

    public abstract String getListenerName();

}
