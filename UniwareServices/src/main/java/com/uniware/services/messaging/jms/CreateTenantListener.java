package com.uniware.services.messaging.jms;

import com.unifier.core.utils.JsonUtils;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.services.tenant.ITenantSetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by bhuvneshwarkumar on 01/10/15.
 */
public class CreateTenantListener extends UniwareMessageListener<CreateTenantEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(CreateTenantListener.class);

    @Autowired
    private ITenantSetupService tenantSetupService;

    private String name;

    private String createTenantRequestString;

    public CreateTenantListener(String name) {
        super(CreateTenantEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(CreateTenantEvent event) {
        try {
            LOG.info("Dequeuing Tenant Creation Task from MQ");
            CreateTenantResponse response = tenantSetupService.createTenant(event.getTenantSetupLog().getRequest());
            LOG.info("CreateTenantResponse {}", response.toString());
        } catch (Exception e) {
            LOG.error("Error processing create tenant event " + event.getName(), e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "TenantCreationListener{" + "name='" + name + '\'' + '}';
    }
}
