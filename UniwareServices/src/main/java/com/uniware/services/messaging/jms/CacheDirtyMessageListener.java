/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author parijat
 */
package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.cache.CacheDirtyEvent;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;

/**
 * @author Sunny
 */
public class CacheDirtyMessageListener extends UniwareMessageListener<CacheDirtyEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(CacheDirtyMessageListener.class);

    public CacheDirtyMessageListener() {
        super(CacheDirtyEvent.class);
    }

    @Override
    public void processMessage(CacheDirtyEvent event) {
        LOG.info("Processing cacheDirtyEvent [{}]", event);
        try {
            switch (event.getType()) {
                case CACHE:
                    if (CacheDirtyEvent.Action.CLEAR_ALL.equals(event.getAction())) {
                        CacheManager.getInstance().doClearTenantCaches();
                    } else {
                        CacheManager.getInstance().doMarkCacheDirty(event.getName(), event.getRequestTimestamp());
                    }
                    break;
                case CONFIGURATION:
                    if (CacheDirtyEvent.Action.CLEAR_ALL.equals(event.getAction())) {
                        ConfigurationManager.getInstance().doClearTenantConfigurations();
                    } else {
                        ConfigurationManager.getInstance().doMarkConfigurationDirty(event.getName(), event.getRequestTimestamp());
                    }
                    break;
            }
        } catch (Exception e) {
            LOG.error("Error processing cache dirty event", e);
        }
    }

    @Override
    public String getListenerName() {
        return "CacheDirtyMessageListener";
    }

}
