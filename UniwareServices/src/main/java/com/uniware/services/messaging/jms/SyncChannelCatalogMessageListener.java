package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.services.channel.IChannelCatalogSyncService;

public class SyncChannelCatalogMessageListener extends UniwareMessageListener<SyncChannelCatalogEvent> {

    private static final Logger        LOG = LoggerFactory.getLogger(SyncChannelCatalogMessageListener.class);

    @Autowired
    private IChannelCatalogSyncService channelCatalogSyncService;

    private String                     name;

    public SyncChannelCatalogMessageListener(String name) {
        super(SyncChannelCatalogEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(SyncChannelCatalogEvent event) {
        LOG.info("Processing SyncChannelCatalogEvent [{}]", event);
        try {
            channelCatalogSyncService.syncChannelCatalog(event);
        } catch (Exception e) {
            LOG.error("Error processing SyncChannelCatalogEvent for channel: " + event.getSyncChannelCatalogRequest().getChannelCode(), e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "SyncChannelCatalogMessageListener{" + "name='" + name + '\'' + "} " + super.toString();
    }
}
