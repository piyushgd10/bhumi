package com.uniware.services.messaging.jms;

import com.uniware.core.api.tenant.TenantSetupLog;

import java.io.Serializable;

/**
 * Created by bhuvneshwarkumar on 01/10/15.
 */
public class CreateTenantEvent implements Serializable {
    private String         name;
    private TenantSetupLog tenantSetupLog;

    public CreateTenantEvent() {
    }

    public TenantSetupLog getTenantSetupLog() {
        return tenantSetupLog;
    }

    public void setTenantSetupLog(TenantSetupLog request) {
        this.tenantSetupLog = request;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override public String toString() {
        return "TenantCreationEvent{" +
                "name='" + name + '\'' +
                '}';
    }
}
