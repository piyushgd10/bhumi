package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.services.channel.saleorder.IChannelOrderSyncService;

public class SyncChannelOrdersMessageListener extends UniwareMessageListener<SyncChannelOrdersEvent> {

    private static final Logger      LOG = LoggerFactory.getLogger(SyncChannelOrdersMessageListener.class);

    @Autowired
    private IChannelOrderSyncService orderSyncService;

    private String                   name;

    public SyncChannelOrdersMessageListener(String name) {
        super(SyncChannelOrdersEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(SyncChannelOrdersEvent event) {
        LOG.info("Processing ChannelOrderSyncEvent [{}]", event);
        try {
            orderSyncService.syncChannelOrders(event);
        } catch (Exception e) {
            LOG.error("Error processing SyncChannelOrdersEvent for channel: " + event.getChannelCode(), e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "SyncChannelOrdersMessageListener{" + "name='" + name + '\'' + "} " + super.toString();
    }
}
