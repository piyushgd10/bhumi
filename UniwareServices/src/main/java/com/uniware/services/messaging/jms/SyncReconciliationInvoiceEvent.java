package com.uniware.services.messaging.jms;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by akshayag on 9/21/16.
 */

public class SyncReconciliationInvoiceEvent implements Serializable {

    private String  channelCode;

    private Date    requestTimestamp;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }
}
