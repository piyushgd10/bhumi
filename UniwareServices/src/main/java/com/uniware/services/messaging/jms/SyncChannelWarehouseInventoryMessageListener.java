package com.uniware.services.messaging.jms;

import com.uniware.services.channelWarehouse.IChannelWarehouseInventorySyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by digvijaysharma on 22/01/17.
 */
public class SyncChannelWarehouseInventoryMessageListener extends UniwareMessageListener<SyncChannelWarehouseInventoryEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(SyncChannelWarehouseInventoryMessageListener.class);

    @Autowired
    private IChannelWarehouseInventorySyncService channelWarehouseInventorySyncService;

    private String                       name;

    public SyncChannelWarehouseInventoryMessageListener(String name) {
        super(SyncChannelWarehouseInventoryEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(SyncChannelWarehouseInventoryEvent event) {
        LOG.info("Processing SyncChannelInventoryEvent [{}]", event);
        try {
            channelWarehouseInventorySyncService.syncChannelWarehouseInventory(event);
        } catch (Exception e) {
            LOG.error("Error processing SyncChannelInventoryEvent for channel: " + event.getChannelCode(), e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "SyncChannelWarehouseInventoryMessageListener{" + "name='" + name + '\'' + "} " + super.toString();
    }
}

