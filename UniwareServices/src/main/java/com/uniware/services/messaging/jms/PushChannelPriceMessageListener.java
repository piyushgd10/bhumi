/*
 *  Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Nov-2015
 *  @author akshay
 */
package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.services.pricing.IPricingFacade;

public class PushChannelPriceMessageListener extends UniwareMessageListener<PushAllPricesOnChannelRequest> {

    private static final Logger          LOG = LoggerFactory.getLogger(PushChannelPriceMessageListener.class);
    
    private String         name;
    
    @Autowired
    private IPricingFacade pricingFacade;

    public PushChannelPriceMessageListener(String name) {
        super(PushAllPricesOnChannelRequest.class);
        this.name = name;
    }

    @Override
    public void processMessage(PushAllPricesOnChannelRequest request) {
        LOG.info("Processing PushAllPricesOnChannelRequest [{}]", request);
        try {
            request.setLocalExecution(true);
            pricingFacade.pushAllPricesOnChannel(request);
        } catch (Exception e) {
            LOG.error("Error processing PushAllPricesOnChannelRequest for channel: " + request.getChannelCode(), e);
        }

    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "PushChannelPriceMessageListener{" + "name='" + name + '\'' + "} " + super.toString();
    }

}
