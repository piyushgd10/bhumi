package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.services.channel.IChannelInventorySyncService;

public class SyncChannelInventoryMessageListener extends UniwareMessageListener<SyncChannelInventoryEvent> {

    private static final Logger          LOG = LoggerFactory.getLogger(SyncChannelInventoryMessageListener.class);

    @Autowired
    private IChannelInventorySyncService channelInventorySyncService;

    private String                       name;

    public SyncChannelInventoryMessageListener(String name) {
        super(SyncChannelInventoryEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(SyncChannelInventoryEvent event) {
        LOG.info("Processing SyncChannelInventoryEvent [{}]", event);
        try {
            channelInventorySyncService.doProcessChannelInventory(event);
        } catch (Exception e) {
            LOG.error("Error processing SyncChannelInventoryEvent for channel: " + event.getChannelCode(), e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "SyncChannelInventoryMessageListener{" + "name='" + name + '\'' + "} " + super.toString();
    }
}
