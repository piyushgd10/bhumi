package com.uniware.services.messaging.jms;

import java.io.Serializable;
import java.util.Date;

public class SyncChannelOrdersEvent implements Serializable {

    private static final long serialVersionUID = 3224676866343498249L;

    private String            channelCode;

    private boolean           syncNewOrders;

    private Date              requestTimestamp;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public boolean isSyncNewOrders() {
        return syncNewOrders;
    }

    public void setSyncNewOrders(boolean syncNewOrders) {
        this.syncNewOrders = syncNewOrders;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "SyncChannelOrdersEvent{" + "channelCode='" + channelCode + '\'' + ", syncNewOrders=" + syncNewOrders + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
