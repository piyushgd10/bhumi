package com.uniware.services.messaging.jms;

import java.io.Serializable;
import java.util.Date;

public class SyncChannelInventoryEvent implements Serializable {

    private static final long serialVersionUID = -8843275991294159863L;

    private String            channelCode;
    private boolean           processStockoutsOnly;
    private boolean           processPendingInventorySnapshots;
    private Date              requestTimestamp;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public boolean isProcessStockoutsOnly() {
        return processStockoutsOnly;
    }

    public void setProcessStockoutsOnly(boolean processStockoutsOnly) {
        this.processStockoutsOnly = processStockoutsOnly;
    }

    public boolean isProcessPendingInventorySnapshots() {
        return processPendingInventorySnapshots;
    }

    public void setProcessPendingInventorySnapshots(boolean processPendingInventorySnapshots) {
        this.processPendingInventorySnapshots = processPendingInventorySnapshots;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "SyncChannelInventoryEvent{" + "channelCode='" + channelCode + '\'' + ", processStockoutsOnly=" + processStockoutsOnly + ", processPendingInventorySnapshots="
                + processPendingInventorySnapshots + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
