/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author parijat
 */
package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.entity.ExportJob;
import com.unifier.services.export.IExportService;

/**
 * @author Sunny
 */
public class ExportMessageListener extends UniwareMessageListener<ExportJobEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(ExportMessageListener.class);

    @Autowired
    private IExportService      exportService;

    private String              name;

    public ExportMessageListener(String name) {
        super(ExportJobEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(ExportJobEvent event) {
        LOG.info("Processing export [{}]", event);
        try {
            exportService.executeExportJob(event.getId());
        } catch (Exception e) {
            LOG.error("Error processing export event " + event.getName(), e);
            ExportJob exportJob = exportService.getExportJobById(event.getId());
            exportJob.setStatusCode(ExportJob.Status.FAILED);
            exportJob.setMessage(e.getMessage());
            exportService.updateExportJob(exportJob);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "ExportMessageListener{" + "name='" + name + '\'' + '}';
    }
}
