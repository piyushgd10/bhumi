package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.uniware.services.publisher.IMessageBroadcaster;
import com.uniware.services.publisher.Message;

public class StompMessageListener extends UniwareMessageListener<Message> {

    private static final Logger LOG = LoggerFactory.getLogger(StompMessageListener.class);

    @Autowired
    private IMessageBroadcaster messageBroadcaster;

    private String              name;

    public StompMessageListener(String name) {
        super(Message.class);
        this.name = name;
    }

    @Override
    public void processMessage(Message stompMessage) {
        try {
            messageBroadcaster.publish(stompMessage);
        } catch (Exception e) {
            LOG.error("Error processing StompMessage: " + stompMessage, e);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }
}
