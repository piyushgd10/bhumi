/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Dec-2013
 *  @author parijat
 */
package com.uniware.services.messaging.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.entity.ImportJob;
import com.unifier.services.imports.IImportService;

/**
 * @author Sunny
 */
public class ImportMessageListener extends UniwareMessageListener<ImportJobEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(ImportMessageListener.class);

    @Autowired
    private IImportService      importService;

    private String              name;

    public ImportMessageListener(String name) {
        super(ImportJobEvent.class);
        this.name = name;
    }

    @Override
    public void processMessage(ImportJobEvent event) {
        LOG.info("Processing import [{}]", event);
        try {
            importService.executeImportJob(event.getId());
        } catch (Exception e) {
            LOG.error("Error processing import event " + event.getName(), e);
            ImportJob importJob = importService.getImportJobById(event.getId());
            importJob.setStatusCode(ImportJob.Status.FAILED);
            importJob.setMessage(e.getMessage());
            importService.updateImportJob(importJob);
        }
    }

    @Override
    public String getListenerName() {
        return name;
    }

    @Override
    public String toString() {
        return "ImportMessageListener{" + "name='" + name + '\'' + '}';
    }
}
