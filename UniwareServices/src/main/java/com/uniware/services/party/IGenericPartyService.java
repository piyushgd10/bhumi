/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.party;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.party.WsGenericParty;
import com.uniware.core.entity.Party;

public interface IGenericPartyService {

    void prepareParty(Party party, WsGenericParty wsParty, ValidationContext context);

    void prepareParty(Party party, WsGenericParty wsParty, ValidationContext context, boolean isCreateParty);
}
