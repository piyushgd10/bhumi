/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.party.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.party.CreateOrUpdatePartyAddressRequest;
import com.uniware.core.api.party.CreateOrUpdatePartyAddressResponse;
import com.uniware.core.api.party.CreateOrUpdatePartyContactRequest;
import com.uniware.core.api.party.CreateOrUpdatePartyContactResponse;
import com.uniware.core.api.party.CreatePartyAddressRequest;
import com.uniware.core.api.party.CreatePartyAddressResponse;
import com.uniware.core.api.party.CreatePartyContactRequest;
import com.uniware.core.api.party.CreatePartyContactResponse;
import com.uniware.core.api.party.DeletePartyContactRequest;
import com.uniware.core.api.party.DeletePartyContactResponse;
import com.uniware.core.api.party.EditPartyAddressRequest;
import com.uniware.core.api.party.EditPartyAddressResponse;
import com.uniware.core.api.party.EditPartyContactRequest;
import com.uniware.core.api.party.EditPartyContactResponse;
import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINRequest;
import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINResponse;
import com.uniware.core.api.party.PartyAddressDTO;
import com.uniware.core.api.party.PartyContactDTO;
import com.uniware.core.api.party.PartySearchDTO;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.dto.GenericPartyDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.State;
import com.uniware.dao.party.IPartyDao;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.cache.PartyCache;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.party.IPartyService;
import com.uniware.services.warehouse.IFacilityService;

@Service
@Transactional
public class PartyServiceImpl implements IPartyService {

    @Autowired
    private IPartyDao partyDao;

    @Autowired
    private IBillingPartyService billingPartyService;

    @Autowired
    private IFacilityService     facilityService;

    @Override
    public CreateOrUpdatePartyAddressResponse createOrUpdateAddress(CreateOrUpdatePartyAddressRequest request) {
        CreateOrUpdatePartyAddressResponse response = new CreateOrUpdatePartyAddressResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyAddressType partyAddressType = CacheManager.getInstance().getCache(PartyCache.class).getAddressTypeByCode(request.getPartyAddress().getAddressType());
            PartyAddress partyAddress = partyDao.getPartyAddress(request.getPartyAddress().getPartyCode(), partyAddressType.getId());
            if (partyAddress == null) {
                response.setResponse(createAddress(new CreatePartyAddressRequest(request.getPartyAddress())));
            } else {
                response.setResponse(editAddress(new EditPartyAddressRequest(request.getPartyAddress())));
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @MarkDirty(values = { FacilityCache.class })
    public CreatePartyAddressResponse createAddress(CreatePartyAddressRequest request) {
        CreatePartyAddressResponse response = new CreatePartyAddressResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyAddressType partyAddressType = CacheManager.getInstance().getCache(PartyCache.class).getAddressTypeByCode(request.getPartyAddress().getAddressType());
            PartyAddress partyAddress = partyDao.getPartyAddress(request.getPartyAddress().getPartyCode(), partyAddressType.getId());
            if (partyAddress != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Address exists for type :" + request.getPartyAddress().getAddressType());
            } else {
                partyAddress = new PartyAddress();
                preparePartyAddress(partyAddress, request.getPartyAddress(), partyAddressType, context);
                if (!context.hasErrors()) {
                    partyAddress.setCreated(DateUtils.getCurrentTime());
                    partyAddress = partyDao.createAddress(partyAddress);
                    response.setPartyAddressDTO(new PartyAddressDTO(partyAddress));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void preparePartyAddress(PartyAddress partyAddress, WsPartyAddress wsPartyAddress, PartyAddressType partyAddressType, ValidationContext context) {
        LocationCache cache = CacheManager.getInstance().getCache(LocationCache.class);
        Country country = cache.getCountryByCode(wsPartyAddress.getCountryCode());
        if (country == null) {
            context.addError(WsResponseCode.INVALID_COUNTRY_CODE, "Invalid country code/name");
        } else {
            if (country.isDefinedStateCodes()) {
                State state = cache.getStateByCode(wsPartyAddress.getStateCode(), country.getCode());
                if (state == null) {
                    context.addError(WsResponseCode.INVALID_STATE_CODE, "Invalid state code/name");
                } else {
                    wsPartyAddress.setStateCode(state.getIso2Code());
                }
            }

            if (StringUtils.isNotBlank(country.getPostcodePattern()) && ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).isValidatePostcode()) {
                if (!Pattern.matches(country.getPostcodePattern(), wsPartyAddress.getPincode())) {
                    context.addError(ResponseCode.INVALID_FORMAT, "invalid pincode");
                }
            }
        }
        if (!context.hasErrors()) {
            partyAddress.setAddressLine1(wsPartyAddress.getAddressLine1());
            partyAddress.setAddressLine2(wsPartyAddress.getAddressLine2());
            partyAddress.setCity(wsPartyAddress.getCity());
            partyAddress.setPhone(wsPartyAddress.getPhone());
            partyAddress.setPincode(wsPartyAddress.getPincode());
            partyAddress.setStateCode(wsPartyAddress.getStateCode());
            partyAddress.setCountryCode(country.getCode());
            partyAddress.setPartyAddressType(partyAddressType);
            partyAddress.setParty(partyDao.getPartyByCode(wsPartyAddress.getPartyCode()));
            partyAddress.setEnabled(true);
            partyAddress.setUpdated(DateUtils.getCurrentTime());
        }
    }

    @Override
    @MarkDirty(values = { FacilityCache.class })
    public EditPartyAddressResponse editAddress(EditPartyAddressRequest request) {
        EditPartyAddressResponse response = new EditPartyAddressResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyAddressType partyAddressType = CacheManager.getInstance().getCache(PartyCache.class).getAddressTypeByCode(request.getPartyAddress().getAddressType());
            PartyAddress partyAddress = partyDao.getPartyAddress(request.getPartyAddress().getPartyCode(), partyAddressType.getId());
            if (partyAddress == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Address doesn't exists for type :" + request.getPartyAddress().getAddressType());
            } else {
                preparePartyAddress(partyAddress, request.getPartyAddress(), partyAddressType, context);
                if (!context.hasErrors()) {
                    partyAddress = partyDao.updateAddress(partyAddress);
                    response.setPartyAddressDTO(new PartyAddressDTO(partyAddress));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public CreateOrUpdatePartyContactResponse createOrUpdateContact(CreateOrUpdatePartyContactRequest request) {
        CreateOrUpdatePartyContactResponse response = new CreateOrUpdatePartyContactResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyContactType partyContactType = CacheManager.getInstance().getCache(PartyCache.class).getContactTypeByCode(request.getPartyContact().getContactType());
            PartyContact partyContact = partyDao.getPartyContact(request.getPartyContact().getPartyCode(), partyContactType.getId());
            if (partyContact == null) {
                response.setResponse(createContact(new CreatePartyContactRequest(request.getPartyContact())));
            } else {
                response.setResponse(editContact(new EditPartyContactRequest(request.getPartyContact())));
            }
        }
        return response;
    }

    @Override
    public CreatePartyContactResponse createContact(CreatePartyContactRequest request) {
        CreatePartyContactResponse response = new CreatePartyContactResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyContactType partyContactType = CacheManager.getInstance().getCache(PartyCache.class).getContactTypeByCode(request.getPartyContact().getContactType());
            PartyContact partyContact = partyDao.getPartyContact(request.getPartyContact().getPartyCode(), partyContactType.getId());
            if (partyContact != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Contact exists for type :" + request.getPartyContact().getContactType());
            } else {
                partyContact = new PartyContact();
                preparePartyContact(partyContact, request.getPartyContact(), partyContactType, context);
                if (!context.hasErrors()) {
                    partyContact.setCreated(DateUtils.getCurrentTime());
                    partyContact = partyDao.createContact(partyContact);
                    response.setPartyContactDTO(new PartyContactDTO(partyContact));
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void preparePartyContact(PartyContact partyContact, WsPartyContact wsPartyContact, PartyContactType partyContactType, ValidationContext context) {
        try {
            partyContact.setEmail(ValidatorUtils.validateAndNormalizeEmail(wsPartyContact.getEmail()));
        } catch (IllegalArgumentException e) {
            context.addError(ResponseCode.INVALID_FORMAT, e.getMessage());
        }
        if (!context.hasErrors()) {
            partyContact.setFax(wsPartyContact.getFax());
            partyContact.setName(wsPartyContact.getName());
            partyContact.setPhone(wsPartyContact.getPhone());
            partyContact.setPartyContactType(partyContactType);
            partyContact.setParty(partyDao.getPartyByCode(wsPartyContact.getPartyCode()));
        }
    }

    @Override
    public EditPartyContactResponse editContact(EditPartyContactRequest request) {
        EditPartyContactResponse response = new EditPartyContactResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyContactType partyContactType = CacheManager.getInstance().getCache(PartyCache.class).getContactTypeByCode(request.getPartyContact().getContactType());
            PartyContact partyContact = partyDao.getPartyContact(request.getPartyContact().getPartyCode(), partyContactType.getId());
            if (partyContact == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Contact doesn't exists for type :" + request.getPartyContact().getContactType());
            } else {
                preparePartyContact(partyContact, request.getPartyContact(), partyContactType, context);
                if (!context.hasErrors()) {
                    partyContact = partyDao.updateContact(partyContact);
                    response.setSuccessful(true);
                    response.setPartyContactDTO(new PartyContactDTO(partyContact));
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public DeletePartyContactResponse deleteContact(DeletePartyContactRequest request) {
        DeletePartyContactResponse response = new DeletePartyContactResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PartyContactType partyContactType = CacheManager.getInstance().getCache(PartyCache.class).getContactTypeByCode(request.getContactType());
            PartyContact partyContact = partyDao.getPartyContact(request.getPartyCode(), partyContactType.getId());
            partyDao.deletePartyContact(partyContact);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public Party getPartyById(Integer id) {
        return partyDao.getPartyById(id);
    }

    @Override
    public Party getPartyByCode(String partyCode) {
        return partyDao.getPartyByCode(partyCode);
    }

    @Override
    public List<PartySearchDTO> lookupParty(String keyword) {
        List<PartySearchDTO> searchDTOs = new ArrayList<PartySearchDTO>();
        for (Party party : partyDao.lookupParty(keyword)) {
            PartySearchDTO partyDTO = new PartySearchDTO();
            partyDTO.setCode(party.getCode());
            partyDTO.setName(party.getName());
            partyDTO.setId(party.getId());
            searchDTOs.add(partyDTO);
        }
        return searchDTOs;
    }

    @Override
    @Transactional
    public List<PartyAddressType> getPartyAddressTypes() {
        return partyDao.getPartyAddressTypes();
    }

    @Override
    @Transactional
    public List<PartyContactType> getPartyContactTypes() {
        return partyDao.getPartyContactTypes();
    }

    @Override
    @Transactional
    public GetBillingPartyAndFacilityWithGSTINResponse getBillingPartyAndFacilityWithAdresses(GetBillingPartyAndFacilityWithGSTINRequest request) {
        GetBillingPartyAndFacilityWithGSTINResponse response = new GetBillingPartyAndFacilityWithGSTINResponse();
        List<GenericPartyDTO> partyDTOList = new ArrayList<>();
        List<Party> parties = new ArrayList<>();
        parties.addAll(billingPartyService.getAllBillingPartiesWithGstNumber());
        parties.addAll(facilityService.getAllEnabledFacilitiesWithGstNumber());
        for(Party party : parties){
            GenericPartyDTO genericPartyDTO = new GenericPartyDTO(party);
            partyDTOList.add(genericPartyDTO);
        }
        response.setPartyDTOList(partyDTOList);
        response.setSuccessful(true);
        return response;
    }

}