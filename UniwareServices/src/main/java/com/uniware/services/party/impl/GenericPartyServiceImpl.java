/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.party.impl;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.BinaryObject;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.uniware.core.api.party.WsGenericParty;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.LocationCache;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.State;
import com.uniware.services.cache.PartyCache;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.party.IGenericPartyService;
import com.uniware.services.party.IPartyService;

@Service
@Transactional
public class GenericPartyServiceImpl implements IGenericPartyService {

    @Autowired
    IPartyService partyService;

    @Override
    public void prepareParty(Party party, WsGenericParty wsParty, ValidationContext context) {
        if (party.getCreated() == null) {
            party.setCode(wsParty.getCode());
            party.setCreated(DateUtils.getCurrentTime());
        }
        party.setAlternateCode(wsParty.getAlternateCode());
        party.setName(ValidatorUtils.getOrDefaultValue(wsParty.getName(), party.getName()));
        party.setPan(ValidatorUtils.getOrDefaultValue(wsParty.getPan(), party.getPan()));
        party.setTin(ValidatorUtils.getOrDefaultValue(wsParty.getTin(), party.getTin()));
        party.setCstNumber(ValidatorUtils.getOrDefaultValue(wsParty.getCstNumber(), party.getCstNumber()));
        party.setCinNumber(ValidatorUtils.getOrDefaultValue(wsParty.getCinNumber(), party.getCinNumber()));
        party.setStNumber(ValidatorUtils.getOrDefaultValue(wsParty.getStNumber(), party.getStNumber()));
        party.setGstNumber(ValidatorUtils.getOrDefaultValue(wsParty.getGstNumber(), party.getGstNumber()));
        party.setEnabled(ValidatorUtils.getOrDefaultValue(wsParty.getEnabled(), party.isEnabled()));
        party.setTaxExempted(ValidatorUtils.getOrDefaultValue(wsParty.getTaxExempted(), party.isTaxExempted()));
        party.setWebsite(ValidatorUtils.getOrDefaultValue(wsParty.getWebsite(), party.getWebsite()));
        if (wsParty.getBinaryObjectId() != null) {
            party.setBinaryObject(new BinaryObject(wsParty.getBinaryObjectId()));
        }
        if (wsParty.getSignatureBinaryObjectId() != null) {
            party.setSignatureBinaryObject(new BinaryObject(wsParty.getSignatureBinaryObjectId()));
        }
        party.setUniwareAccessUrl(ValidatorUtils.getOrDefaultValue(wsParty.getUniwareAccessUrl(), party.getUniwareAccessUrl()));
        party.setUniwareApiUser(ValidatorUtils.getOrDefaultValue(wsParty.getUniwareApiUser(), party.getUniwareApiUser()));
        party.setUniwareApiPassword(ValidatorUtils.getOrDefaultValue(wsParty.getUniwareApiPassword(), party.getUniwareApiPassword()));
        party.setRegisteredDealer(wsParty.isRegisteredDealer());
        party.setUpdated(DateUtils.getCurrentTime());
        PartyAddress billingAddress = party.getPartyAddressByType(PartyAddressType.Code.BILLING.name());
        PartyAddressType partyAddressType = CacheManager.getInstance().getCache(PartyCache.class).getAddressTypeByCode(PartyAddressType.Code.BILLING.name());
        if (wsParty.getBillingAddress() != null) {
            if (billingAddress == null) {
                billingAddress = new PartyAddress();
                party.getPartyAddresses().add(billingAddress);
                billingAddress.setParty(party);
                billingAddress.setCreated(DateUtils.getCurrentTime());
            }
            preparePartyAddress(billingAddress, wsParty.getBillingAddress(), partyAddressType, context);
        }
        if (!context.hasErrors()) {
            PartyAddress shippingAddress = party.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
            partyAddressType = CacheManager.getInstance().getCache(PartyCache.class).getAddressTypeByCode(PartyAddressType.Code.SHIPPING.name());
            if (wsParty.getShippingAddress() != null) {
                if (shippingAddress == null) {
                    shippingAddress = new PartyAddress();
                    party.getPartyAddresses().add(shippingAddress);
                    shippingAddress.setParty(party);
                    shippingAddress.setCreated(DateUtils.getCurrentTime());
                }
                preparePartyAddress(shippingAddress, wsParty.getShippingAddress(), partyAddressType, context);
            }
        }
        if (!context.hasErrors()) {
            for (WsPartyContact wsPartyContact : wsParty.getPartyContacts()) {
                PartyContactType partyContactType = CacheManager.getInstance().getCache(PartyCache.class).getContactTypeByCode(wsPartyContact.getContactType());
                if (partyContactType == null) {
                    context.addError(WsResponseCode.INVALID_PARTY_CONTACT_TYPE, "Invalid party contact type: " + wsPartyContact.getContactType() + ", Valid contact types: "
                            + PartyContactType.Code.values());
                } else {
                    PartyContact partyContact = party.getPartyContactByType(wsPartyContact.getContactType());
                    if (partyContact == null) {
                        partyContact = new PartyContact();
                        party.getPartyContacts().add(partyContact);
                        partyContact.setParty(party);
                        partyContact.setCreated(DateUtils.getCurrentTime());
                    }
                    preparePartyContact(partyContact, wsPartyContact, partyContactType, context);
                }
            }
        }
    }

    @Override
    public void prepareParty(Party party, WsGenericParty wsParty, ValidationContext context, boolean isCreateParty) {
        if (isCreateParty) {
            Party existingParty = partyService.getPartyByCode(wsParty.getCode());
            if (existingParty != null) {
                context.addError(WsResponseCode.INVALID_PARTY_CODE, "This code is already used. Please try some other code.");
                return;
            }
        }
        prepareParty(party, wsParty, context);

    }

    private void preparePartyAddress(PartyAddress partyAddress, WsPartyAddress wsPartyAddress, PartyAddressType partyAddressType, ValidationContext context) {
        LocationCache cache = CacheManager.getInstance().getCache(LocationCache.class);
        Country country = cache.getCountryByCode(wsPartyAddress.getCountryCode());
        if (country == null) {
            context.addError(WsResponseCode.INVALID_COUNTRY_CODE, "Invalid country code/name");
        } else {
            if (country.isDefinedStateCodes()) {
                State state = cache.getStateByCode(wsPartyAddress.getStateCode(), country.getCode());
                if (state == null) {
                    context.addError(WsResponseCode.INVALID_STATE_CODE, "Invalid state code/name");
                } else {
                    wsPartyAddress.setStateCode(state.getIso2Code());
                }
            }

            if (StringUtils.isNotBlank(country.getPostcodePattern()) && ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).isValidatePostcode()) {
                if (!Pattern.matches(country.getPostcodePattern(), wsPartyAddress.getPincode())) {
                    context.addError(ResponseCode.INVALID_FORMAT, "invalid pincode");
                }
            }
        }
        if (!context.hasErrors()) {
            partyAddress.setAddressLine1(wsPartyAddress.getAddressLine1());
            partyAddress.setAddressLine2(wsPartyAddress.getAddressLine2());
            partyAddress.setCity(wsPartyAddress.getCity());
            partyAddress.setPhone(wsPartyAddress.getPhone());
            partyAddress.setPincode(wsPartyAddress.getPincode());
            partyAddress.setStateCode(wsPartyAddress.getStateCode());
            partyAddress.setCountryCode(country.getCode());
            partyAddress.setPartyAddressType(partyAddressType);
            partyAddress.setEnabled(true);
        }
    }

    private void preparePartyContact(PartyContact partyContact, WsPartyContact wsPartyContact, PartyContactType partyContactType, ValidationContext context) {
        try {
            partyContact.setEmail(ValidatorUtils.validateAndNormalizeEmail(wsPartyContact.getEmail()));
        } catch (IllegalArgumentException e) {
            context.addError(ResponseCode.INVALID_FORMAT, e.getMessage());
        }
        if (!context.hasErrors()) {
            partyContact.setFax(wsPartyContact.getFax());
            partyContact.setName(wsPartyContact.getName());
            partyContact.setPhone(wsPartyContact.getPhone());
            partyContact.setPartyContactType(partyContactType);
        }
    }
}