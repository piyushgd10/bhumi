/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.services.party;

import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINRequest;
import com.uniware.core.api.party.GetBillingPartyAndFacilityWithGSTINResponse;
import java.util.List;

import com.uniware.core.api.party.CreateOrUpdatePartyAddressRequest;
import com.uniware.core.api.party.CreateOrUpdatePartyAddressResponse;
import com.uniware.core.api.party.CreateOrUpdatePartyContactRequest;
import com.uniware.core.api.party.CreateOrUpdatePartyContactResponse;
import com.uniware.core.api.party.CreatePartyAddressRequest;
import com.uniware.core.api.party.CreatePartyAddressResponse;
import com.uniware.core.api.party.CreatePartyContactRequest;
import com.uniware.core.api.party.CreatePartyContactResponse;
import com.uniware.core.api.party.DeletePartyContactRequest;
import com.uniware.core.api.party.DeletePartyContactResponse;
import com.uniware.core.api.party.EditPartyAddressRequest;
import com.uniware.core.api.party.EditPartyAddressResponse;
import com.uniware.core.api.party.EditPartyContactRequest;
import com.uniware.core.api.party.EditPartyContactResponse;
import com.uniware.core.api.party.PartySearchDTO;
import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContactType;

public interface IPartyService {

    CreatePartyAddressResponse createAddress(CreatePartyAddressRequest request);

    EditPartyAddressResponse editAddress(EditPartyAddressRequest request);

    CreatePartyContactResponse createContact(CreatePartyContactRequest request);

    EditPartyContactResponse editContact(EditPartyContactRequest request);

    Party getPartyById(Integer id);

    Party getPartyByCode(String partyCode);

    List<PartySearchDTO> lookupParty(String keyword);

    DeletePartyContactResponse deleteContact(DeletePartyContactRequest request);

    CreateOrUpdatePartyAddressResponse createOrUpdateAddress(CreateOrUpdatePartyAddressRequest request);

    CreateOrUpdatePartyContactResponse createOrUpdateContact(CreateOrUpdatePartyContactRequest request);

    List<PartyAddressType> getPartyAddressTypes();

    List<PartyContactType> getPartyContactTypes();

    GetBillingPartyAndFacilityWithGSTINResponse getBillingPartyAndFacilityWithAdresses(GetBillingPartyAndFacilityWithGSTINRequest request);
}
