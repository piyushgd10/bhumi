/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.services.packer.impl;

import static com.uniware.core.entity.PicklistItem.StatusCode.PUTBACK_PENDING;
import static com.uniware.core.entity.SaleOrderItem.StatusCode.STAGED;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.inventory.MarkItemTypeInventoryNotFoundRequest;
import com.uniware.core.api.item.ItemDetailFieldDTO;
import com.uniware.core.api.packer.AbstractAllocateItemRequest;
import com.uniware.core.api.packer.AbstractAllocateItemResponse;
import com.uniware.core.api.packer.AcceptPackagePutbackRequest;
import com.uniware.core.api.packer.AcceptPackagePutbackResponse;
import com.uniware.core.api.packer.AcceptPutbackItemRequest;
import com.uniware.core.api.packer.AcceptPutbackItemResponse;
import com.uniware.core.api.packer.AcceptPutbackRequest;
import com.uniware.core.api.packer.AcceptPutbackResponse;
import com.uniware.core.api.packer.AllocateItemInPackageRequest;
import com.uniware.core.api.packer.AllocateItemInPackageResponse;
import com.uniware.core.api.packer.AllocateItemRequest;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageRequest;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageResposne;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityRequest;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityResponse;
import com.uniware.core.api.packer.GetPacklistRequest;
import com.uniware.core.api.packer.GetPacklistResponse;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.packer.PacklistDTO;
import com.uniware.core.api.packer.PickShippingPackageRequest;
import com.uniware.core.api.packer.PickShippingPackageResponse;
import com.uniware.core.api.packer.PrintPicklistItemRequest;
import com.uniware.core.api.packer.PrintPicklistItemResponse;
import com.uniware.core.api.packer.ReceivePickBucketForWebRequest;
import com.uniware.core.api.packer.ReceivePickBucketForWebResponse;
import com.uniware.core.api.packer.ReceivePicklistRequest;
import com.uniware.core.api.packer.ReceivePicklistResponse;
import com.uniware.core.api.packer.RemoveItemFromPicklistRequest;
import com.uniware.core.api.packer.RemoveItemFromPicklistResponse;
import com.uniware.core.api.packer.ShippingPackageDTO;
import com.uniware.core.api.packer.ShippingPackageItemDTO;
import com.uniware.core.api.packer.SuggestItemAllocationRequest;
import com.uniware.core.api.packer.SuggestItemAllocationResponse;
import com.uniware.core.api.picker.CompletePickingRequest;
import com.uniware.core.api.picker.CompletePickingResponse;
import com.uniware.core.api.picker.CompletePicklistRequest;
import com.uniware.core.api.picker.CompletePicklistResponse;
import com.uniware.core.api.picker.CreatePickBatchRequest;
import com.uniware.core.api.picker.CreatePickBatchResponse;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.EditPicklistResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItem.ItemDetailingStatus;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.picker.IPickerDao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.ChannelCache;
import com.uniware.services.cache.ItemTypeDetailCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.item.IItemService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.warehouse.IShelfService;

/**
 * @author singla
 */
@Service
public class PackerServiceImpl implements IPackerService {

    private static final Logger LOG    = LoggerFactory.getLogger(PackerServiceImpl.class);

    @Autowired
    private IPickerDao          pickerDao;

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private ICatalogService     catalogService;

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private ILockingService     lockingService;

    @Autowired
    private IPickerService      pickerService;

    @Autowired
    private IShelfService       shelfService;

    @Autowired
    private IUsersService       usersService;

    @Autowired
    private IPicksetService     picksetService;

    @Autowired
    private IItemService        itemService;

    private SimpleDateFormat    format = new SimpleDateFormat("yyyy.MM.dd");

    /* (non-Javadoc)
     * @see com.uniware.services.packer.IPackerService#getPacklist(com.uniware.core.api.packer.GetPacklistRequest)
     */
    @Override
    @Transactional(readOnly = true)
    public GetPacklistResponse getPacklist(GetPacklistRequest request) {
        GetPacklistResponse response = new GetPacklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else {
                PacklistDTO packlistDTO = new PacklistDTO();
                packlistDTO.setCode(picklist.getCode());
                packlistDTO.setUsername(picklist.getUser().getUsername());
                packlistDTO.setStatusCode(picklist.getStatusCode());
                packlistDTO.setType(picklist.getType());
                packlistDTO.setDestination(picklist.getDestination().name());
                packlistDTO.setCreated(picklist.getCreated());
                packlistDTO.setUpdated(picklist.getUpdated());
                Map<String, Integer> pickBucketScannedQuantity = new HashMap<>();
                if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled()) {
                    picklist.getPickBatches().forEach(pickBatch -> {
                        pickBucketScannedQuantity.computeIfAbsent(pickBatch.getPickBucket().getCode(), k -> pickBatch.getPicklistItems().size());
                    });
                }
                packlistDTO.setPickBucketScannedQuantity(pickBucketScannedQuantity);
                Map<String, List<PicklistItem>> shippingPackageToPicklistItems = new HashMap<>();
                for (PicklistItem pi : picklist.getPicklistItems()) {
                    if (!shippingPackageToPicklistItems.containsKey(pi.getShippingPackageCode())) {
                        shippingPackageToPicklistItems.put(pi.getShippingPackageCode(), new ArrayList<>());
                    }
                    shippingPackageToPicklistItems.get(pi.getShippingPackageCode()).add(pi);
                }
                Map<String, Integer> spCodeToId = new HashMap<>();
                shippingPackageToPicklistItems.entrySet().forEach((e) -> {
                    List<ShippingPackageItemDTO> shippingPackageItemDTOs = new ArrayList<>();
                    ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(e.getKey());
                    SaleOrder saleOrder = saleOrderService.getSaleOrderById(shippingPackage.getSaleOrder().getId());

                    Map<String, SaleOrderItem> codeTOSaleOrderItem = new HashMap<>();
                    for (SaleOrderItem saleOrderItem : saleOrder.getSaleOrderItems()) {
                        codeTOSaleOrderItem.put(saleOrderItem.getCode(), saleOrderItem);
                    }

                    e.getValue().forEach(pi -> {
                        SaleOrderItem saleOrderItem = codeTOSaleOrderItem.get(pi.getSaleOrderItemCode());
                        if (StringUtils.equalsAny(pi.getStatusCode(), PicklistItem.StatusCode.PUTBACK_ACCEPTED, PicklistItem.StatusCode.PUTBACK_COMPLETE)
                                && PicklistItem.QCStatus.ACCEPTED.equals(pi.getQcStatusCode())) {
                            packlistDTO.getPutbackItems().add(new PacklistDTO.PacklistRemovedItemDTO(shippingPackage, saleOrderItem, pi));
                        } else if (PicklistItem.StatusCode.INVENTORY_NOT_FOUND.equals(pi.getStatusCode())) {
                            packlistDTO.getNotFoundItems().add(new PacklistDTO.PacklistNotFoundItemDTO(shippingPackage, saleOrderItem, pi));
                        } else if (StringUtils.equalsAny(pi.getStatusCode(), PicklistItem.StatusCode.PUTBACK_ACCEPTED, PicklistItem.StatusCode.PUTBACK_COMPLETE)
                                && PicklistItem.QCStatus.REJECTED.equals(pi.getQcStatusCode())) {
                            packlistDTO.getDamagedItems().add(new PacklistDTO.PacklistDamagedItemDTO(shippingPackage, saleOrderItem, pi));
                        } else {
                            shippingPackageItemDTOs.add(prepareShippingPackageItemDTO(saleOrderItem, pi));
                        }
                    });
                    if (shippingPackageItemDTOs.size() > 0) {
                        packlistDTO.getPacklistItems().add(prepareShippingPackageDTO(shippingPackage, shippingPackageItemDTOs));
                    }
                });
                //TODO : need to discuss this
                Collections.sort(packlistDTO.getPacklistItems(), (dto1, dto2) -> {
                    int score0 = 0, score1 = 0;
                    score0 += dto1.getInvoiceCode() == null ? 1 : dto1.getShippingProviderCode() == null ? 0 : 2;
                    score1 += dto2.getInvoiceCode() == null ? 1 : dto2.getShippingProviderCode() == null ? 0 : 2;

                    if (score0 - score1 == 0) {
                        return dto1.getCode().compareTo(dto2.getCode());
                    } else {
                        return score0 > score1 ? 1 : -1;
                    }
                });
                response.setPacklist(packlistDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private ShippingPackageItemDTO prepareShippingPackageItemDTO(SaleOrderItem saleOrderItem, PicklistItem pi) {
        ShippingPackageItemDTO shippingPackageItemDTO = new ShippingPackageItemDTO();
        ItemType itemType = saleOrderItem.getItemType(); //catalogService.getNonBundledItemTypeById(saleOrderItem.getItemType().getId());
        shippingPackageItemDTO.setItemSku(itemType.getSkuCode());
        shippingPackageItemDTO.setItemName(itemType.getName());
        shippingPackageItemDTO.setItemTypeImageUrl(itemType.getImageUrl());
        shippingPackageItemDTO.setItemTypePageUrl(itemType.getProductPageUrl());
        shippingPackageItemDTO.setColor(itemType.getColor());
        shippingPackageItemDTO.setSize(itemType.getSize());
        shippingPackageItemDTO.setBrand(itemType.getBrand());
        shippingPackageItemDTO.setScanIdentifier(itemType.getScanIdentifier());
        shippingPackageItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
        shippingPackageItemDTO.setStatusCode(saleOrderItem.getStatusCode());
        shippingPackageItemDTO.setShelfCode(saleOrderItem.getItemTypeInventory().getShelf().getCode());
        if (saleOrderItem.getItem() != null) {
            shippingPackageItemDTO.setItemCode(saleOrderItem.getItem().getCode());
        }
        shippingPackageItemDTO.setSoftAllocatedItemCode(pi.getItemCode());
        return shippingPackageItemDTO;
    }

    private ShippingPackageDTO prepareShippingPackageDTO(ShippingPackage shippingPackage, List<ShippingPackageItemDTO> shippingPackageItemDTOs) {
        ShippingPackageDTO shippingPackageDTO = new ShippingPackageDTO();
        shippingPackageDTO.setCode(shippingPackage.getCode());
        shippingPackageDTO.setSaleOrderCode(shippingPackage.getSaleOrder().getCode());
        shippingPackageDTO.setInvoiceCode(shippingPackage.getInvoice() != null ? shippingPackage.getInvoice().getCode() : null);
        shippingPackageDTO.setInvoiceDisplayCode(shippingPackage.getInvoice() != null ? shippingPackage.getInvoice().getDisplayCode() : null);
        shippingPackageDTO.setReturnInvoiceCode(shippingPackage.getReturnInvoice() != null ? shippingPackage.getReturnInvoice().getCode() : null);
        shippingPackageDTO.setReturnInvoiceDisplayCode(shippingPackage.getReturnInvoice() != null ? shippingPackage.getReturnInvoice().getDisplayCode() : null);
        shippingPackageDTO.setShippingLabelLink(shippingPackage.getShippingLabelLink());
        shippingPackageDTO.setShippingProviderCode(shippingPackage.getShippingProviderCode());
        shippingPackageDTO.setTrackingNumber(shippingPackage.getTrackingNumber());
        shippingPackageDTO.setRequiresCustomization(shippingPackage.isRequiresCustomization());
        shippingPackageDTO.setStatusCode(shippingPackage.getStatusCode());
        shippingPackageDTO.setTotalAmount(shippingPackage.getTotalPrice());
        shippingPackageDTO.setPackageType(shippingPackage.getShippingPackageType().getCode());
        shippingPackageDTO.setChannel(CacheManager.getInstance().getCache(ChannelCache.class).getChannelById(shippingPackage.getSaleOrder().getChannel().getId()).getName());
        shippingPackageDTO.getShippingPackageItems().addAll(shippingPackageItemDTOs);
        return shippingPackageDTO;
    }

    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public ReceivePicklistResponse receivePicklist(ReceivePicklistRequest request) {
        ReceivePicklistResponse response = new ReceivePicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, false);
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid picklist code");
            } else if (StringUtils.isNotBlank(request.getDestination()) && !picklist.getDestination().name().equals(request.getDestination())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_DESTINATION, "Picklist can not be received at " + request.getDestination());
            } else if (Picklist.StatusCode.RECEIVING.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.PICKLIST_ALREADY_RECEIVING, "Picklist can be received only in CREATED state, current state : " + picklist.getStatusCode());
            } else if (!Picklist.StatusCode.CREATED.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist can be received only in CREATED state, current state : " + picklist.getStatusCode());
            } else {
                Set<java.util.concurrent.locks.Lock> saleOrderLocks = new HashSet<>(picklist.getPicklistItems().size());
                List<String> saleOrderCodes = new ArrayList<>();
                try {
                    picklist.getPicklistItems().forEach(picklistItem -> {
                        if (!saleOrderCodes.contains(picklistItem.getSaleOrderCode())) {
                            saleOrderCodes.add(picklistItem.getSaleOrderCode());
                        }
                    });
                    Collections.sort(saleOrderCodes);
                    saleOrderCodes.forEach(saleOrderCode -> {
                        java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SALE_ORDER, saleOrderCode);
                        lock.lock();
                        saleOrderLocks.add(lock);
                    });
                    doReceivePicklist(request, context, response);
                    if (!context.hasErrors()) {
                        response.setSuccessful(true);
                    }
                } finally {
                    saleOrderLocks.forEach(java.util.concurrent.locks.Lock::unlock);
                }
            }
        }
        response.addErrors(context.getErrors());
        response.addWarnings(context.getWarnings());
        return response;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @LogActivity
    private void doReceivePicklist(ReceivePicklistRequest request, ValidationContext context, ReceivePicklistResponse response) {
        Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, false);
        receivePicklistInternal(picklist, context);
    }

    private void receivePicklistInternal(Picklist picklist, ValidationContext context) {
        boolean defaultPickBatch = false;
        if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled() && picklist.getPickBatches().size() == 0) {

            /* CREATE DEFAULT PICK BATCH & BUCKET */
            CreatePickBatchRequest createPickBatchRequest = new CreatePickBatchRequest();
            createPickBatchRequest.setPicklistCode(picklist.getCode());
            createPickBatchRequest.setPickBucketCode(Constants.DEFAULT);
            createPickBatchRequest.setUserId(usersService.getUserByUsername(UserContext.current().getUniwareUserName()).getId());
            CreatePickBatchResponse createPickBatchResponse = pickerService.createPickBatch(createPickBatchRequest);
            if (createPickBatchResponse.hasErrors()) {
                context.addErrors(createPickBatchResponse.getErrors());
            } else {
                defaultPickBatch = true;
            }
        }
        if (!context.hasErrors()) {
            receivePicklistItems(picklist, context, defaultPickBatch);
        }
        if (!context.hasErrors()) {
            picklist.setStatusCode(Picklist.StatusCode.RECEIVING.name());
        }
    }

    public void receivePicklistItems(Picklist picklist, ValidationContext context, boolean defaultPickBatch) {
        pickerDao.receivePicklistItems(picklist.getId());
        if (defaultPickBatch) {
            pickerDao.setDefaultPickBatch(picklist.getId());
        }
        /* MARK ALL PICKLIST ITEMS 'RECEIVING' */
        Map<String, ShippingPackage> codeToSP = new HashMap<>();
        if (Picklist.Destination.INVOICING.equals(picklist.getDestination())) {
            List<String> shippingPackageCodes = pickerDao.getShippingPackageCodesInPicklist(picklist.getId());
            shippingPackageCodes.forEach(shippingPackageCode -> {
                ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
                if (ShippingPackage.StatusCode.PICKING.name().equals(shippingPackage.getStatusCode())) {
                    shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
                    shippingService.updateShippingPackage(shippingPackage);
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} picklist {" + ActivityEntityEnum.PICKLIST
                                                + ":" + picklist.getCode() + "} received. Status " + shippingPackage.getStatusCode() }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                }
            });
        }
    }

    /* (non-Javadoc)
     * @see com.uniware.services.packer.IPackerService#allocateItem(com.uniware.core.api.packer.AllocateItemRequest)
     */
    //@Override
    @Locks({
            @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public AllocateItemInPackageResponse allocateItemInPackage(AllocateItemInPackageRequest request) {
        AllocateItemInPackageResponse response = new AllocateItemInPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            allocateItemInternal(request, response, context, request.getShippingPackageCode());
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!response.hasErrors());
        return response;
    }

    @Transactional
    private void allocateItemInternal(AbstractAllocateItemRequest request, AbstractAllocateItemResponse response, ValidationContext context, String shippingPackageCode) {
        Item item = inventoryService.getItemByCode(request.getItemCode(), true);
        Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
        if (item == null) {
            context.addError(WsResponseCode.INVALID_ITEM_ID);
        } else if (picklist == null) {
            context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
        }
        if (!context.hasErrors()) {
            PicklistItem existingPicklistItem = pickerService.getPicklistItemByItemCodeInPicklist(item.getCode(), picklist.getId());
            if (existingPicklistItem != null && existingPicklistItem.isRescannedPutbackItem()) {
                response.setPutbackItem(true);
                response.setSaleOrderCode(existingPicklistItem.getSaleOrderCode());
                response.setSaleOrderItemCode(existingPicklistItem.getSaleOrderItemCode());
                response.setShippingPackageCode(existingPicklistItem.getShippingPackageCode());
                return;
            }
            if (!isPicklistItemValidForRescan(picklist, existingPicklistItem)) {
                context.addError(WsResponseCode.INVALID_ITEM_ID, "Item code already scanned once in this picklist");
            }
            if (!context.hasErrors()) {
                boolean handheldEnabled = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled();
                final PickBatch pickBatch = handheldEnabled ? pickerService.getOpenPickBatchByPickBucketCode(request.getPickBucketCode()) : null;
                if (handheldEnabled && pickBatch == null) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE);
                }
                if (!context.hasErrors()) {
                    final Supplier<PicklistItem> picklistItemSupplier;
                    if (pickBatch != null) {
                        picklistItemSupplier = () -> pickerService.getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(item.getCode(), pickBatch);
                    } else {
                        picklistItemSupplier = () -> pickerService.getSoftAllocatedPicklistItemByItemCode(item.getCode(), picklist);
                    }
                    final Supplier<SaleOrderItem> saleOrderItemSupplier = () -> {
                        PicklistItem picklistItem = picklistItemSupplier.get();
                        if (handheldEnabled && picklistItem == null) {
                            context.addError(WsResponseCode.INVALID_REQUEST, "Scanned item does not belong to the bucket");
                            return null;
                        } else if (picklistItem != null) {
                            return saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
                        } else {
                            if (shippingPackageCode == null) {
                                return saleOrderService.getSaleOrderItemForAllocationInPicklist(request.getPicklistCode(), item);
                            } else {
                                return saleOrderService.getSaleOrderItemForAllocationInPicklist(request.getPicklistCode(), item, shippingPackageCode);
                            }
                        }
                    };
                    if (!context.hasErrors()) {
                        allocateItem(item, picklist, context, request, response, saleOrderItemSupplier);
                    }
                }
            }
        }
    }

    private void allocateItem(Item item, Picklist picklist, ValidationContext context, AbstractAllocateItemRequest request, AbstractAllocateItemResponse allocationResponse,
            Supplier<SaleOrderItem> saleOrderItemSupplier) {
        SaleOrderItem saleOrderItem;
        validateItemForAllocation(item, picklist, context);
        if (!context.hasErrors()) {
            if (!isItemAlreadyAllocated(item, context, allocationResponse) && !isItemExpired(item, context, allocationResponse, request.getPicklistCode())) {
                int retryCount = 10;
                do {
                    LOG.info("Attempt: {} to suggest saleOrderItem to item: {}", 11 - retryCount, item.getCode());
                    try {
                        saleOrderItem = saleOrderItemSupplier.get();
                    } catch (IllegalStateException e) {
                        allocationResponse.addError(new WsError(e.getMessage()));
                        break;
                    }
                    if (saleOrderItem == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
                        break;
                    } else {
                        if (!context.hasErrors() && doAllocateItemInternal(item, picklist, context, allocationResponse, saleOrderItem, saleOrderItemSupplier)) {
                            break;
                        }
                    }
                } while (--retryCount > 0 && !context.hasErrors());
            } else {
                context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
            }
        }
        allocationResponse.addErrors(context.getErrors());
    }

    @Transactional
    @LogActivity
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[4].saleOrder.code}") })
    private boolean doAllocateItemInternal(Item preLockItem, Picklist picklist, ValidationContext context, AbstractAllocateItemResponse response,
            SaleOrderItem preLockSaleOrderItem, Supplier<SaleOrderItem> saleOrderItemSupplier) {
        //sale order join fetched int soi for lock key
        Item item = inventoryService.getItemByCode(preLockItem.getCode());
        validateItemForAllocation(item, picklist, context);
        if (context.hasErrors()) {
            return false;
        }
        SaleOrderItem saleOrderItem;
        try {
            // check again if preLockSaleOrderItem is still the one to be which this item will be allocated
            saleOrderItem = saleOrderItemSupplier.get();
        } catch (IllegalStateException e) {
            response.addError(new WsError(e.getMessage()));
            return false;
        }
        if (saleOrderItem == null) {
            context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
            return false;
        } else {
            if (!saleOrderItem.getCode().equals(preLockSaleOrderItem.getCode())) {
                return false;
            } else {
                PicklistItem picklistItem = pickerService.getPicklistItemBySaleOrderItemCode(picklist.getId(), saleOrderItem.getCode(), saleOrderItem.getSaleOrder().getCode());
                picklistItem.setItemCode(item.getCode());
                picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
                item.setStatusCode(Item.StatusCode.PICKED.name());
                //                ActivityLogger.forEvent(Event.ITEM_PICKED).withIdentifier(item.getCode()).add(picklist.getCode()).log();
                if (PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
                    // SOI must be cancelled
                    response.setPutbackItem(true);
                    response.setSaleOrderItemCode(saleOrderItem.getCode());
                    response.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
                    response.setShippingPackageCode(picklistItem.getShippingPackageCode());
                } else {
                    allocateItemToSaleOrderItem(saleOrderItem, item, response);
                    picklistItem.setStatusCode(PicklistItem.StatusCode.COMPLETE);
                }
                pickerService.updatePicklistItem(picklistItem);
            }
        }
        return true;
    }

    private void allocateItemToSaleOrderItem(SaleOrderItem saleOrderItem, Item item, AbstractAllocateItemResponse allocationResponse) {
        if (saleOrderItem.getItemDetailingStatus() == ItemDetailingStatus.PENDING) {
            if (isItemDetailingComplete(item, saleOrderItem)) {
                saleOrderItem.setItemDetailingStatus(ItemDetailingStatus.COMPLETE);
            }
        }
        saleOrderItem.setItem(item);
        saleOrderItem.setItemDetails(item.getItemDetails());
        item.setStatusCode(Item.StatusCode.ALLOCATED.name());
        //        ActivityLogger.forEvent(Event.SALE_ORDER_ITEM_ADDED).withGroupIdentifier(saleOrderItem.getSaleOrder().getCode()).withIdentifier(saleOrderItem.getCode()).add(
        //                item.getCode()).log();
        //        ActivityLogger.forEvent(Event.ITEM_ALLOCATED).withIdentifier(item.getCode()).add(saleOrderItem.getCode()).log();
        allocationResponse.setSuccessful(true);
        allocationResponse.setSaleOrderItemCode(saleOrderItem.getCode());
        allocationResponse.setShippingPackageCode(saleOrderItem.getShippingPackage().getCode());
    }

    private boolean isItemDetailingComplete(Item item, SaleOrderItem saleOrderItem) {
        List<ItemDetailFieldDTO> pendingItemDetailFields = new ArrayList<>();
        Map<String, String> detailsAlreadyPresent = null;
        if (StringUtils.isNotBlank(item.getItemDetails())) {
            detailsAlreadyPresent = JsonUtils.jsonToMap(item.getItemDetails());
        }
        for (String itemDetailField : StringUtils.split(saleOrderItem.getItemDetailFields())) {
            if (detailsAlreadyPresent == null || !detailsAlreadyPresent.containsKey(itemDetailField)) {
                ItemDetailFieldDTO detailFieldDTO = CacheManager.getInstance().getCache(ItemTypeDetailCache.class).getItemDetailFieldByName(itemDetailField);
                pendingItemDetailFields.add(detailFieldDTO);
            }
        }
        return pendingItemDetailFields.isEmpty();
    }

    private boolean isItemExpired(Item item, ValidationContext context, AbstractAllocateItemResponse allocationResponse, String picklistCode) {
        ItemType itemType = catalogService.getNonBundledItemTypeById(item.getItemType().getId());
        if (itemService.isItemAboutToExpireForThisTolerance(item, itemType, itemType.getCategory().getDispatchExpiryTolerance())) {
            LOG.info("Expired item : marking bad inventory: " + item.getCode());
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemForDeallocationInPicklist(picklistCode, item);
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
            } else {
                int shelfLife = catalogService.getShelfLifeForItemType(itemType);
                if (!DateUtils.addDaysToDate(item.getManufacturingDate(), shelfLife).after(DateUtils.getCurrentTime())) {
                    allocationResponse.setAction(EditPicklistRequest.Action.EXPIRED_ITEM.name());
                } else {
                    allocationResponse.setAction(EditPicklistRequest.Action.ABOUT_TO_EXPIRE.name());
                }
                populateAllocationResponse(allocationResponse, item, saleOrderItem);
                context.addError(WsResponseCode.INVALID_ITEM_STATE,
                        "Item already or close to expiry: Expiry Date:" + format.format(DateUtils.addDaysToDate(item.getManufacturingDate(), shelfLife)));
            }
            return true;
        }
        return false;
    }

    @Transactional
    private void populateAllocationResponse(AbstractAllocateItemResponse abstractAllocateItemResponse, Item item, SaleOrderItem saleOrderItem) {
        saleOrderItem = saleOrderService.getSaleOrderItemById(saleOrderItem.getId());
        abstractAllocateItemResponse.setSkuCode(item.getItemType().getSkuCode());
        abstractAllocateItemResponse.setSaleOrderItemCode(saleOrderItem.getCode());
        abstractAllocateItemResponse.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
        if (saleOrderItem.getShippingPackage() != null) {
            abstractAllocateItemResponse.setShippingPackageCode(saleOrderItem.getShippingPackage().getCode());
        }
        abstractAllocateItemResponse.setProductName(item.getItemType().getName());
    }

    private boolean isItemAlreadyAllocated(Item item, ValidationContext context, AbstractAllocateItemResponse allocationResponse) {
        if (item.isAllocated()) {
            context.addError(WsResponseCode.ITEM_ALREADY_ALLOCATED, "Invalid item status : " + item.getStatusCode());
            for (SaleOrderItem soi : item.getSaleOrderItems()) {
                if (StringUtils.equalsAny(soi.getStatusCode(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.PICKING_FOR_STAGING.name(), STAGED.name(),
                        SaleOrderItem.StatusCode.PICKING_FOR_INVOICING.name())) {
                    allocationResponse.setSaleOrderItemCode(soi.getCode());
                    allocationResponse.setShippingPackageCode(soi.getShippingPackage().getCode());
                    break;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * If facility is enabled for handheld picking, then item status must be {@link Item.StatusCode#PICKED}. This would
     * have been done by If not handheld, then if picklist was created from STOCKING zone, then item should be
     * {@link Item.StatusCode#GOOD_INVENTORY}, else {@link Item.StatusCode#PICKED} (marked at STAGING).
     *
     * @param item - item which is scanned
     * @param picklist - picklist in which the item came
     * @param context - validation context
     */
    private boolean validateItemForAllocation(Item item, Picklist picklist, ValidationContext context) {
        PickSet.Type fromPickSetType = picksetService.getPickSetByName(picklist.getPickSetName()).getType();
        boolean handHeldPickingEnabled = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled();
        if (!handHeldPickingEnabled) {
            if (fromPickSetType.equals(PickSet.Type.STOCKING) && !item.isGoodInventory()) {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
            } else if (fromPickSetType.equals(PickSet.Type.STAGING) && !item.isPicked()) {
                context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
            }
        } else if (!item.isPicked()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
        }
        return !context.hasErrors();
    }

    private boolean isPicklistItemValidForRescan(Picklist picklist, PicklistItem picklistItem) {
        if (picklistItem != null) {
            PickSet pickSet = picksetService.getPickSetByName(picklist.getPickSetName());
            if (PickSet.Type.STAGING.equals(pickSet.getType()) && picklistItem.isAssessed()) {
                return false;
            } else if (PickSet.Type.STOCKING.equals(pickSet.getType())
                    && !StringUtils.equalsAny(picklistItem.getStatusCode(), PicklistItem.StatusCode.SUBMITTED, PicklistItem.StatusCode.RECEIVED, PUTBACK_PENDING)) {
                return false;
            }
        }
        return true;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}") })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    public PickShippingPackageResponse pickShippingPackage(PickShippingPackageRequest request) {
        PickShippingPackageResponse response = new PickShippingPackageResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(request.getSaleOrderCode());
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (saleOrder == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code");
            } else if (shippingPackage == null || !shippingPackage.getSaleOrder().getId().equals(saleOrder.getId())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code");
            } else if (!StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(),
                    ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(), ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name())) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package status");
            } else if (request.getItemCodes().size() != shippingPackage.getSaleOrderItems().size()) {
                context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION, "Item codes are required for all items in package");
            }
            if (!context.hasErrors()) {
                // Ignore item codes which are already allocated
                for (SaleOrderItem saleOrderItem : shippingPackage.getSaleOrderItems()) {
                    if (saleOrderItem.getItem() != null) {
                        request.getItemCodes().remove(saleOrderItem.getItem().getCode());
                    }
                }
                for (String itemCode : request.getItemCodes()) {
                    if (!response.hasErrors()) {
                        AllocateItemRequest itemRequest = new AllocateItemRequest();
                        itemRequest.setItemCode(itemCode);
                        itemRequest.setShippingPackageCode(request.getShippingPackageCode());
                        // TODO
                        //AllocateItemResponse itemResponse = allocateItem(itemRequest);
                        //response.addErrors(itemResponse.getErrors());
                    }
                }
                if (!response.hasErrors()) {
                    shippingPackage.setStatusCode(
                            shippingPackage.isRequiresCustomization() ? ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name() : ShippingPackage.StatusCode.PICKED.name());
                    if (ActivityContext.current().isEnable()) {
                        ActivityUtils.appendActivity(shippingPackage.getCode(), ActivityEntityEnum.SHIPPING_PACKAGE.getName(), shippingPackage.getSaleOrder().getCode(),
                                Arrays.asList(new String[] {
                                        "Shipping package {" + ActivityEntityEnum.SHIPPING_PACKAGE + ":" + shippingPackage.getCode() + "} moved to "
                                                + shippingPackage.getStatusCode() }),
                                ActivityTypeEnum.PROCESSING.name());
                    }
                    response.setSuccessful(true);
                }
            } else {
                response.setErrors(context.getErrors());
            }
        }
        return response;
    }

    @Override
    @Locks({
            @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public SuggestItemAllocationResponse suggestItemAllocation(SuggestItemAllocationRequest request) {
        SuggestItemAllocationResponse response = new SuggestItemAllocationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            allocateItemInternal(request, response, context, null);
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!response.hasErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.packer.IPackerService#acceptPutback(com.uniware.core.api.packer.AcceptPutbackRequest)
     */
    @Override
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}") })
    public AcceptPutbackResponse acceptPutback(AcceptPutbackRequest request) {
        AcceptPutbackResponse response = new AcceptPutbackResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, true);
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else if (!StringUtils.equalsAny(picklist.getStatusCode(), Picklist.StatusCode.RECEIVING.name(), Picklist.StatusCode.CREATED.name())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE);
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Operation not supported for ITEM traceability");
            } else {
                picklist.getPicklistItems().stream().filter(picklistItem -> (PUTBACK_PENDING.equals(picklistItem.getStatusCode()) && !context.hasErrors())).forEach(
                        picklistItem -> {
                            if (picklistItem.getStagingShelfCode() != null) {
                                acceptPutbackItemInternal(picklistItem, context, picklistItem.getStagingShelfCode());
                            } else {
                                acceptPutbackItemInternal(picklistItem, context);
                            }
                        });
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public String getWorkorderHtml(String shippingPackageCode, Template template) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(shippingPackageCode);
        if (shippingPackage != null) {
            return prepareWorkorderHtml(shippingPackage, template);
        } else {
            return null;
        }
    }

    private String prepareWorkorderHtml(ShippingPackage shippingPackage, Template template) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("shippingPackage", shippingPackage);
        return template.evaluate(params);
    }

    @Override
    @Transactional(readOnly = true)
    public PrintPicklistItemResponse printPicklistItem(PrintPicklistItemRequest request) {
        PrintPicklistItemResponse response = new PrintPicklistItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SaleOrder so = saleOrderService.getSaleOrderByCode(request.getSaleOrderCode());
            SaleOrderItem requestedSoi = null;
            int serialNumberInOrder = 0;
            int serialNumberInPackage = 0;
            for (SaleOrderItem soi : so.getSaleOrderItems()) {
                serialNumberInOrder++;
                if (soi.getCode().equals(request.getSaleOrderItemCode())) {
                    requestedSoi = soi;
                    break;
                }
            }
            if (requestedSoi == null) {
                context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_CODE, "SaleOrderItem does not belong to SaleOrder");
            } else {
                for (SaleOrderItem soi : requestedSoi.getShippingPackage().getSaleOrderItems()) {
                    serialNumberInPackage++;
                    if (soi.getCode().equals(requestedSoi.getCode())) {
                        break;
                    }
                }
            }
            if (!context.hasErrors()) {
                response.setSaleOrder(so);
                response.setSaleOrderItem(requestedSoi);
                response.setSerialNumberInOrder(serialNumberInOrder);
                response.setSerialNumberInPackage(serialNumberInPackage);
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    public AcceptPackagePutbackResponse acceptPackagePutback(AcceptPackagePutbackRequest request) {
        AcceptPackagePutbackResponse response = new AcceptPackagePutbackResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            request.getShippingPackageCodes().forEach(packageCode -> {
                List<PicklistItem> picklistItems = pickerService.getPicklistItemsByShippingPackageCode(packageCode, request.getDestination()).stream().filter(
                        picklistItem -> picklistItem.getStatusCode().equals(PUTBACK_PENDING)).collect(Collectors.toList());
                if (picklistItems.isEmpty()) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "No items to put back for package: " + packageCode);
                } else {
                    ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(packageCode);
                    picklistItems.forEach(picklistItem -> {
                        if (shippingPackage.getShelfCode() == null) {
                            acceptPutbackItemInternal(picklistItem, context);
                        } else {
                            acceptPutbackItemInternal(picklistItem, context, shippingPackage.getShelfCode());
                        }
                    });
                }
            });

        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    @Transactional
    @LogActivity
    public AcceptPutbackItemResponse acceptPutbackItem(AcceptPutbackItemRequest request) {
        AcceptPutbackItemResponse response = new AcceptPutbackItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else if (!StringUtils.equalsAny(picklist.getStatusCode(), Picklist.StatusCode.RECEIVING.name(), Picklist.StatusCode.CREATED.name())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE);
            } else {
                SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
                PicklistItem picklistItem = pickerService.getPicklistItemBySaleOrderItemCode(picklist.getId(), request.getSaleOrderItemCode(), request.getSaleOrderCode());
                if (picklistItem == null) {
                    context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "No picklist item found");
                } else if (picklistItem.isAssessed()) {
                    context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "Invalid picklist item state : " + picklistItem.getStatusCode().name());
                } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel) && StringUtils.isNotBlank(picklistItem.getItemCode())
                        && !picklistItem.getItemCode().equals(request.getItemCode())) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item does not belong to picklist item");
                } else {
                    ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(picklistItem.getShippingPackageCode());
                    if (shippingPackage.getShelfCode() == null) {
                        acceptPutbackItemInternal(picklistItem, context);
                    } else {
                        acceptPutbackItemInternal(picklistItem, context, shippingPackage.getShelfCode());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private void acceptPutbackForPicklistItem(PicklistItem picklistItem, SaleOrderItem saleOrderItem) {
        inventoryService.commitBlockedInventory(saleOrderItem.getItemTypeInventory().getId(), 1);
        if (PicklistItem.QCStatus.PENDING.equals(picklistItem.getQcStatusCode())) {
            picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
        }
        decrementShelfItemCountIfRequired(picklistItem);
        picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_ACCEPTED);
        if (StringUtils.isNotBlank(picklistItem.getItemCode())) {
            Item item = inventoryService.getItemByCode(picklistItem.getItemCode(), true);
            item.setStatusCode(Item.StatusCode.PUTBACK_PENDING.name());
        }
        if (saleOrderItem.getItem() != null) {
            //            ActivityLogger.forEvent(Event.ITEM_GOOD_INVENTORY).withIdentifier(saleOrderItem.getItem().getCode()).add(saleOrderItem.getCode()).log();
            saleOrderItem.setItem(null);
        }
        if (!SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
        }
        //        ActivityLogger.forEvent(Event.ITEM_PUTBACK_ACCEPTED).withIdentifier(picklistItem.getItemCode()).add(picklistItem.getPicklist().getCode()).log();
    }

    private void decrementShelfItemCountIfRequired(PicklistItem picklistItem) {
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(picklistItem.getShippingPackageCode());
        if (picklistItem.getStagingShelfCode() != null && picklistItem.getStagingShelfCode().equals(shippingPackage.getShelfCode())) {
            Shelf shelf = shelfService.getShelfByCode(shippingPackage.getShelfCode());
            shelf.setItemCount(shelf.getItemCount() > 1 ? shelf.getItemCount() - 1 : 0);
            if (shelf.getItemCount() == 0) {
                shippingPackage.setShelfCode(null);
            }
            shelfService.updateShelf(shelf);
        }
    }

    private void decrementShelfItemCountIfRequired(ShippingPackage shippingPackage, Integer qty) {
        if(StringUtils.isNotBlank(shippingPackage.getShelfCode())){
            Shelf shelf = shelfService.getShelfByCode(shippingPackage.getShelfCode());
            Integer qtyToDecrement = Math.min(shelf.getItemCount(), qty);
            shelf.setItemCount(shelf.getItemCount() - qtyToDecrement);
            if (shelf.getItemCount() == 0) {
                shippingPackage.setShelfCode(null);
            }
            shelfService.updateShelf(shelf);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @LogActivity
    private void doAcceptPutbackItemInternal(PicklistItem picklistItem, ValidationContext context) {
        picklistItem = pickerService.getPicklistItemById(picklistItem.getId());
        if (!PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "Invalid picklist item state : " + picklistItem.getStatusCode());
        } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())
                && picklistItem.getItemCode() == null) {
            context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "Can not accept putback before item is assigned");
        } else {
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
            acceptPutbackForPicklistItem(picklistItem, saleOrderItem);
            pickerService.updatePicklistItem(picklistItem);
        }
    }

    @Locks({
            @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}"),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklist.code}", level = Level.FACILITY),
            @Lock(ns = Namespace.SHELF, key = "#{#args[2]}") })
    private void acceptPutbackItemInternal(PicklistItem picklistItem, ValidationContext context, String shelfCode) {
        doAcceptPutbackItemInternal(picklistItem, context);
    }

    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrderCode}"), @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklist.code}", level = Level.FACILITY) })
    private void acceptPutbackItemInternal(PicklistItem picklistItem, ValidationContext context) {
        doAcceptPutbackItemInternal(picklistItem, context);
    }

    @Override
    public MarkPicklistItemsNotFoundResponse markPicklistItemsNotFound(MarkPicklistItemsNotFoundRequest request) {
        MarkPicklistItemsNotFoundResponse response = new MarkPicklistItemsNotFoundResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, false);
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid picklist code: " + request.getPicklistCode());
            } else if (!StringUtils.equalsAny(picklist.getStatusCode(), Picklist.StatusCode.CREATED.name(), Picklist.StatusCode.RECEIVING.name())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid picklist state: " + picklist.getStatusCode());
            } else {
                Map<String, List<EditPicklistRequest.WsEditPicklistItem>> soCodeToMissingPicklistItems = new HashMap<>();
                request.getMissingPicklistItems().forEach(missingPicklistItem -> {
                    soCodeToMissingPicklistItems.computeIfAbsent(missingPicklistItem.getSaleOrderCode(), k -> new ArrayList<>()).add(missingPicklistItem);
                });
                if (!context.hasErrors()) {
                    editPicklistItemsInternal(picklist.getCode(), soCodeToMissingPicklistItems, context, true);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0]}", level = Level.FACILITY) })
    private void editPicklistItemsInternal(String picklistCode, Map<String, List<EditPicklistRequest.WsEditPicklistItem>> soCodeToWsEditPicklistItems, ValidationContext context,
            boolean markNotFound) {
        List<java.util.concurrent.locks.Lock> saleOrderLocks = new ArrayList<>(soCodeToWsEditPicklistItems.size());
        try {
            soCodeToWsEditPicklistItems.keySet().forEach(soCode -> {
                java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SALE_ORDER, soCode);
                lock.lock();
                saleOrderLocks.add(lock);
            });
            soCodeToWsEditPicklistItems.entrySet().stream().filter(e -> !context.hasErrors()).forEach(
                    e -> doEditPicklistItems(picklistCode, e.getKey(), e.getValue(), context, markNotFound));
        } finally {
            saleOrderLocks.forEach(java.util.concurrent.locks.Lock::unlock);
        }
    }

    @Transactional
    private void doEditPicklistItems(String picklistCode, String saleOrderCode, List<EditPicklistRequest.WsEditPicklistItem> wsEditPicklistItems, ValidationContext context,
            boolean markNotFound) {
        Picklist picklist = pickerService.getPicklistByCode(picklistCode);
        SaleOrder saleOrder = saleOrderService.getSaleOrderForUpdate(saleOrderCode);
        if (saleOrder == null) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_CODE, "Invalid sale order code :" + saleOrderCode);
        } else {
            Map<String, SaleOrderItem> soiCodeToSoi = new HashMap<>();
            Map<String, PicklistItem> codeToPicklistItem = new HashMap<>();
            Map<Integer, Integer> spIdToNotFoundQty = new HashMap<>();
            Map<Integer, Integer> itemTypeInventoryToQty = new HashMap<>();
            Set<Integer> updatedPicklistItemIds = new HashSet<>(wsEditPicklistItems.size());
            Set<Integer> completelyNotFoundShelf = new HashSet<>();
            saleOrder.getSaleOrderItems().forEach(soi -> soiCodeToSoi.put(soi.getCode(), soi));
            picklist.getPicklistItems().forEach(pi -> codeToPicklistItem.put(pi.getSaleOrderCode() + "|" + pi.getSaleOrderItemCode(), pi));
            wsEditPicklistItems.stream().filter(k -> !context.hasErrors()).forEach(wsEditPicklistItem -> {
                SaleOrderItem saleOrderItem = soiCodeToSoi.get(wsEditPicklistItem.getSaleOrderItemCode());
                PicklistItem picklistItem = codeToPicklistItem.get(saleOrder.getCode() + "|" + saleOrderItem.getCode());
                updatedPicklistItemIds.add(picklistItem.getId());
                Item item = null;
                if (StringUtils.isNotBlank(wsEditPicklistItem.getItemCode())) {
                    item = inventoryService.getItemByCode(wsEditPicklistItem.getItemCode(), true);
                }
                validatePicklistItemForDeallocation(context, picklist, picklistItem, item, saleOrderItem, markNotFound);
                if (!context.hasErrors()) {
                    itemTypeInventoryToQty.compute(saleOrderItem.getItemTypeInventory().getId(), (k, v) -> v == null ? 1 : ++v);
                    if (markNotFound) {
                        picklistItem.setStatusCode(PicklistItem.StatusCode.INVENTORY_NOT_FOUND);
                        if (saleOrderItem.getShippingPackage() != null) {
                            spIdToNotFoundQty.compute(saleOrderItem.getShippingPackage().getId(), (k, v) -> v == null ? 1 : ++v);
                            saleOrderItem.setShippingPackage(null);
                        }
                        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel().equals(
                                SystemConfiguration.InventoryAccuracyLevel.SHELF) && StringUtils.isBlank(picklistItem.getItemCode())) {
                            completelyNotFoundShelf.add(saleOrderItem.getItemTypeInventory().getId());
                        }
                    } else {
                        if (item != null) {
                            picklistItem.setItemCode(item.getCode());
                            markItemBadInventoryInPicklist(item, saleOrderItem, wsEditPicklistItem.getRejectionReason());
                            item.setStatusCode(Item.StatusCode.PUTBACK_PENDING.name());
                            // TODO
                            //ActivityLogger.forEvent(Event.ITEM_PICKED).withIdentifier(item.getCode()).add(picklist.getCode()).log();
                        }
                        picklistItem.setQcStatusCode(PicklistItem.QCStatus.REJECTED);
                        picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_ACCEPTED);
                        if (!PUTBACK_PENDING.equals(picklistItem.getStatusCode()) && saleOrderItem.getShippingPackage() != null) {
                            spIdToNotFoundQty.compute(saleOrderItem.getShippingPackage().getId(), (k, v) -> v == null ? 1 : ++v);
                            saleOrderItem.setShippingPackage(null);
                        }
                        if (saleOrderItem.getItem() != null) {
                            saleOrderItem.setItem(null);
                        }
                    }
                    if (!SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                        saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
                    }
                    saleOrderService.updateSaleOrderItem(saleOrderItem);
                    pickerService.updatePicklistItem(picklistItem);
                }
            });
            itemTypeInventoryToQty.forEach((itemTypeInventoryId, qty) -> {
                ItemTypeInventory iti = inventoryService.getItemTypeInventoryById(itemTypeInventoryId);
                if (completelyNotFoundShelf.contains(iti.getId())) {
                    context.addErrors(inventoryService.markItemTypeInventoryNotFound(
                            new MarkItemTypeInventoryNotFoundRequest(iti.getItemType().getSkuCode(), iti.getShelf().getCode())).getErrors());
                }
                if(markNotFound){
                    inventoryService.markInventoryNotFound(iti.getId(), qty);
                }
                inventoryService.commitBlockedInventory(iti.getId(), qty);
            });
            spIdToNotFoundQty.forEach((spId, qty) -> {
                ShippingPackage shippingPackage = shippingService.getShippingPackageById(spId);
                if (!shippingPackage.isSplittable()) {
                    // All sale order items of shipping package may be in different picklist also so need to putback them.
                    List<PicklistItem> picklistItems = pickerDao.getPicklistItemsByShippingPackageCode(shippingPackage.getCode(), picklist.getDestination());
                    picklistItems.stream().filter(pi -> !updatedPicklistItemIds.contains(pi.getId())).forEach(pi -> doHandleAffectedPicklistItem(pi, shippingPackage, soiCodeToSoi.get(pi.getSaleOrderItemCode())));
                }
                decrementShelfItemCountIfRequired(shippingPackage, qty);
                shippingService.reassessShippingPackage(shippingPackage);
            });

        }
    }

    private void validatePicklistItemForDeallocation(ValidationContext context, Picklist picklist, PicklistItem picklistItem, Item item, SaleOrderItem saleOrderItem,
            boolean markNotFound) {
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (picklist.isComplete()) {
            context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "invalid picklist status : " + picklist.getStatusCode());
        } else if (picklistItem.isAssessed()) {
            context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "invalid picklist Item status : " + picklistItem.getStatusCode().name());
        } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel) && item == null && !markNotFound) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item code");
        } else if (item != null && picklistItem.getItemCode() != null && !picklistItem.getItemCode().equals(item.getCode())) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item scanned for picklist item");
        } else if (item != null && !(validateItemForAllocation(item, picklist, context) && ItemTypeInventory.Type.GOOD_INVENTORY.equals(item.getInventoryType()))) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Item is not good inventory:" + item.getStatusCode());
        } else if (!picklistItem.getSaleOrderItemCode().equals(saleOrderItem.getCode())) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_ID, "Sale Order item does not belong to picklist item");
        } else if (saleOrderItem.getItemTypeInventory().getShelf().isBlockedForCycleCount()) {
            context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT, "Shelf is blocked for Cycle Count");
        } else if (saleOrderItem.getItem() != null) {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, "Item already allocated to sale order item:" + saleOrderItem.getCode());
        } else if (item != null && !saleOrderItem.getItemType().getId().equals(item.getItemType().getId())) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item scanned for " + saleOrderItem.getCode());
        } else if (saleOrderItem.getShippingPackage() != null && !StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(),
                ShippingPackage.StatusCode.PICKING.name(), ShippingPackage.StatusCode.PICKED.name())) {
            context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Picklist can be edited for packages in PICKING/PICKED/CANCELLED statuses");
        }
    }

    @Override
    public EditPicklistResponse editPicklist(EditPicklistRequest request) {
        EditPicklistResponse response = new EditPicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else if (!Picklist.StatusCode.RECEIVING.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE);
            } else {
                Map<String, List<EditPicklistRequest.WsEditPicklistItem>> soCodeToMissingPicklistItems = new HashMap<>();
                Map<String, List<EditPicklistRequest.WsEditPicklistItem>> soCodeToDamagedPicklistItems = new HashMap<>();
                request.getPicklistItems().stream().filter(pi -> !context.hasErrors()).forEach(wsEditPicklistItem -> {
                    if (EditPicklistRequest.Action.INVENTORY_NOT_FOUND.equals(wsEditPicklistItem.getAction())) {
                        soCodeToMissingPicklistItems.computeIfAbsent(wsEditPicklistItem.getSaleOrderCode(), k -> new ArrayList<>()).add(wsEditPicklistItem);
                    } else {
                        soCodeToDamagedPicklistItems.computeIfAbsent(wsEditPicklistItem.getSaleOrderCode(), k -> new ArrayList<>()).add(wsEditPicklistItem);
                    }
                });
                if (!context.hasErrors() && soCodeToMissingPicklistItems.size() > 0) {
                    editPicklistItemsInternal(picklist.getCode(), soCodeToMissingPicklistItems, context, true);
                }
                if (!context.hasErrors() && soCodeToDamagedPicklistItems.size() > 0) {
                    editPicklistItemsInternal(picklist.getCode(), soCodeToDamagedPicklistItems, context, false);
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public CompletePicklistResponse completePicklist(CompletePicklistRequest request) {
        CompletePicklistResponse response = new CompletePicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, true);
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "No picklist found for given code");
            } else if (!picklist.isAllowedForCompletePicking()) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid picklist state: " + picklist.getStatusCode());
            } else {
                Optional<PicklistItem> picklistItem = picklist.getPicklistItems().stream().filter(
                        pi -> StringUtils.equalsAny(pi.getStatusCode(), PicklistItem.StatusCode.CREATED, PicklistItem.StatusCode.SUBMITTED)
                                || (PUTBACK_PENDING.equals(pi.getStatusCode()) && pi.getItemCode() != null)).findFirst();
                if (picklistItem.isPresent()) {
                    context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Cant close picklist, some actionable items still pending : " + picklistItem.get().getItemCode());
                } else {
                    CompletePickingResponse completePickingResponse = null;
                    if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                        CompletePickingRequest completePickingRequest = new CompletePickingRequest();
                        completePickingRequest.setPicklistCode(request.getPicklistCode());
                        completePickingRequest.setUserId(usersService.getUserByUsername(UserContext.current().getUniwareUserName()).getId());
                        completePickingRequest.getStatusCodes().add(PicklistItem.StatusCode.RECEIVED);
                        completePickingRequest.getStatusCodes().add(PicklistItem.StatusCode.SUBMITTED);
                        completePickingRequest.getStatusCodes().add(PUTBACK_PENDING);
                        completePickingResponse = pickerService.completePicking(completePickingRequest);
                    }
                    if (completePickingResponse != null && completePickingResponse.hasErrors()) {
                        context.addErrors(completePickingResponse.getErrors());
                    } else {
                        validateAndClosePicklist(context, picklist);
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Transactional
    private void validateAndClosePicklist(ValidationContext context, Picklist picklist) {
        picklist = pickerService.getPicklistByCode(picklist.getCode(), true, true);
        if (pickerService.isPicklistComplete(picklist)) {
            picklist.getPickBatches().stream().filter(pickBatch -> !PickBatch.StatusCode.CLOSED.equals(pickBatch.getStatusCode())).forEach(pickBatch -> {
                pickBatch.getPickBucket().setStatusCode(PickBucket.StatusCode.AVAILABLE);
                pickBatch.setStatusCode(PickBatch.StatusCode.CLOSED);
            });
            picklist.setStatusCode(Picklist.StatusCode.CLOSED.name());
        } else {
            if (Picklist.Destination.STAGING.equals(picklist.getDestination())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist is not complete, Putback might be Pending for some staged items");
            } else {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist is not complete");
            }
        }
    }

    @Override
    @Locks({
            @Lock(ns = Namespace.ITEM, key = "#{#args[0].itemCode}", level = Level.FACILITY),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public RemoveItemFromPicklistResponse removeItemFromPicklist(RemoveItemFromPicklistRequest request) {
        RemoveItemFromPicklistResponse response = new RemoveItemFromPicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Item item = inventoryService.getItemByCode(request.getItemCode(), true);
            if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code + " + request.getItemCode());
            } else if (!StringUtils.equalsAny(item.getStatusCode(), Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.PICKED.name())
                    || !ItemTypeInventory.Type.GOOD_INVENTORY.equals(item.getInventoryType())) {
                context.addError(WsResponseCode.ITEM_ALREADY_ALLOCATED, "Invalid item status : " + item.getStatusCode());
            } else {
                removeItemFromPicklistInternal(request, response, context);
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private void removeItemFromPicklistInternal(RemoveItemFromPicklistRequest request, RemoveItemFromPicklistResponse response, ValidationContext context) {
        Item item = inventoryService.getItemByCode(request.getItemCode(), true);
        Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
        if (picklist == null) {
            context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid picklist code : " + request.getPicklistCode());
        } else if (Picklist.StatusCode.CLOSED.name().equals(picklist.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid picklist status : " + picklist.getStatusCode());
        } else {
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemForDeallocationInPicklist(request.getPicklistCode(), item);
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Couldn't find any sale order item to remove");
            } else {
                deallocateItemInPicklist(item, picklist, context, request, response, saleOrderItem);
            }
        }
    }

    @Transactional
    @LogActivity
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[5].saleOrder.code}") })
    private boolean deallocateItemInPicklist(Item item, Picklist picklist, ValidationContext context, RemoveItemFromPicklistRequest request,
            RemoveItemFromPicklistResponse response, SaleOrderItem preLockSaleOrderItem) {
        item = inventoryService.getItemByCode(request.getItemCode(), true);
        picklist = pickerService.getPicklistByCode(request.getPicklistCode());
        validateItemForAllocation(item, picklist, context);
        SaleOrderItem saleOrderItem = null;
        try {
            // check again if preLockSaleOrderItem is still the one to be which this item will be allocated
            saleOrderItem = saleOrderService.getSaleOrderItemForDeallocationInPicklist(request.getPicklistCode(), item);
        } catch (IllegalStateException e) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, e.getMessage());
        }
        if (!context.hasErrors()) {
            if (saleOrderItem == null) {
                context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
            } else {
                if (saleOrderItem.getCode().equals(preLockSaleOrderItem.getCode())) {
                    PicklistItem picklistItem = pickerService.getPicklistItemBySaleOrderItemCode(picklist.getId(), saleOrderItem.getCode(), saleOrderItem.getSaleOrder().getCode());
                    validatePicklistItemForDeallocation(context, picklist, picklistItem, item, saleOrderItem, false);
                    if (!context.hasErrors()) {
                        picklistItem.setItemCode(item.getCode());
                        picklistItem.setQcStatusCode(PicklistItem.QCStatus.REJECTED);
                        item.setStatusCode(Item.StatusCode.PICKED.name());
                        // TODO
                        //ActivityLogger.forEvent(Event.ITEM_PICKED).withIdentifier(item.getCode()).add(picklist.getCode()).log();
                        switch (picklistItem.getStatusCode()) {
                            case SUBMITTED:
                            case RECEIVED:
                                handleAffectedPicklistItems(picklist, picklistItem, saleOrderItem);
                            case PUTBACK_PENDING:
                                markItemBadInventoryInPicklist(item, saleOrderItem, request.getRejectionReason());
                                acceptPutbackForPicklistItem(picklistItem, saleOrderItem);
                                break;
                            default:
                                context.addError(WsResponseCode.INVALID_PICKLIST_ITEM_STATE, "invalid picklist Item status : " + picklistItem.getStatusCode().name());
                        }
                        pickerService.updatePicklistItem(picklistItem);
                    }
                }
            }
        }
        return false;
    }

    private void handleAffectedPicklistItems(Picklist picklist, PicklistItem picklistItem, SaleOrderItem saleOrderItem) {
        if (saleOrderItem.getShippingPackage() != null) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(saleOrderItem.getShippingPackage().getCode());
            saleOrderItem.setShippingPackage(null);
            shippingPackage.getSaleOrderItems().remove(saleOrderItem);
            if (!SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
                saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
            }
            // Since package is not splittable all other picklist items need to be putback and new package is to be created
            if (!shippingPackage.isSplittable()) {
                // All sale order items of shipping package may be in different picklist also so need to putback them.
                List<PicklistItem> picklistItems = pickerDao.getPicklistItemsByShippingPackageCode(shippingPackage.getCode(), picklist.getDestination());
                picklistItems.stream().filter(pi -> !pi.getId().equals(picklistItem.getId()) && pi.getShippingPackageCode().equals(picklistItem.getShippingPackageCode())).forEach(
                        pi -> doHandleAffectedPicklistItem(pi, shippingPackage));
            }
            shippingService.reassessShippingPackage(shippingPackage);
        }
    }

    private void doHandleAffectedPicklistItem(PicklistItem picklistItem, ShippingPackage shippingPackage) {
        doHandleAffectedPicklistItem(picklistItem, shippingPackage, null);
    }

    private void doHandleAffectedPicklistItem(PicklistItem picklistItem, ShippingPackage shippingPackage, SaleOrderItem saleOrderItem) {
        if (saleOrderItem == null){
            saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
        }
        saleOrderItem.setShippingPackage(null);
        shippingPackage.getSaleOrderItems().remove(saleOrderItem);
        if (!SaleOrderItem.StatusCode.CANCELLED.name().equals(saleOrderItem.getStatusCode())) {
            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.CREATED.name());
        }
        if (!picklistItem.isAssessed()) {
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel().equals(SystemConfiguration.TraceabilityLevel.ITEM)) {
                picklistItem.setStatusCode(PUTBACK_PENDING);
            } else {
                acceptPutbackForPicklistItem(picklistItem, saleOrderItem);
            }
        } else if (PicklistItem.StatusCode.COMPLETE.equals(picklistItem.getStatusCode())) {
            if (Picklist.Destination.STAGING.equals(picklistItem.getPicklist().getDestination())) {
                picklistItem.setStatusCode(PUTBACK_PENDING);
            } else {
                acceptPutbackForPicklistItem(picklistItem, saleOrderItem);
            }
        }
    }

    private void markItemBadInventoryInPicklist(Item item, SaleOrderItem saleOrderItem, String rejectionReason) {
        item.setInventoryType(ItemTypeInventory.Type.BAD_INVENTORY);
        item.setBadInventoryMarkedTime(DateUtils.getCurrentTime());
        item.setRejectionReason(rejectionReason);
        // TODO
        //ActivityLogger.forEvent(Event.ITEM_BAD_INVENTORY).withIdentifier(item.getCode()).add(saleOrderItem.getCode()).log();
    }

    @Override
    public CompleteReceivedPicklistItemsForPackageResposne completePackagePicklistItems(CompleteReceivedPicklistItemsForPackageRequest request) {
        CompleteReceivedPicklistItemsForPackageResposne resposne = new CompleteReceivedPicklistItemsForPackageResposne();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE);
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "Feature not supported for ITEM traceability");
            } else {
                completePackagePicklistItemsInternal(shippingPackage, request.getDestination(), context);
            }
        }
        if (context.hasErrors()) {
            resposne.addErrors(context.getErrors());
        } else {
            resposne.setSuccessful(true);
        }
        return resposne;
    }

    @Transactional
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[0].saleOrder.code}", level = Level.TENANT) })
    private void completePackagePicklistItemsInternal(ShippingPackage shippingPackage, Picklist.Destination destination, ValidationContext validationContext) {
        List<PicklistItem> picklistItems = pickerService.getPicklistItemsByShippingPackageCode(shippingPackage.getCode(), destination);
        picklistItems.stream().filter(
                picklistItem -> StringUtils.equalsAny(picklistItem.getStatusCode(), PicklistItem.StatusCode.RECEIVED, PicklistItem.StatusCode.SUBMITTED)).forEach(picklistItem -> {
                    picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
                    picklistItem.setStatusCode(PicklistItem.StatusCode.COMPLETE);
                });
    }

    @Override
    public ReceivePickBucketForWebResponse receivePickBucketAtInvoicing(ReceivePickBucketForWebRequest request) {
        ReceivePickBucketForWebResponse response = new ReceivePickBucketForWebResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = pickerService.getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = pickerService.getOpenPickBatchByPickBucketCode(pickBucket.getCode(), true);
                if (pickBatch == null) {
                    throw new IllegalStateException("No open pick batch found for pick bucket");
                } else if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled()
                        || !Picklist.Destination.INVOICING.equals(pickBatch.getPicklist().getDestination())) {
                    context.addError(WsResponseCode.INVALID_API_REQUEST, "Not available for facilities with handheld picking");
                } else if (!PickBatch.StatusCode.SUBMITTED.equals(pickBatch.getStatusCode())) {
                    response.setPicklistCode(pickBatch.getPicklist().getCode());
                    response.setPickBatchStatus(pickBatch.getStatusCode());
                    context.addError(WsResponseCode.INVALID_PICK_BATCH_STATE, "Bucket cannot be received in " + pickBatch.getStatusCode() + " state");
                } else {
                    java.util.concurrent.locks.Lock picklistLock = lockingService.getLock(Namespace.PICKLIST, pickBatch.getPicklist().getCode(), Level.FACILITY);
                    picklistLock.lock();
                    List<java.util.concurrent.locks.Lock> saleOrderLocks = new ArrayList<>(pickBatch.getPicklistItems().size());
                    try {
                        pickBatch.getPicklistItems().forEach(picklistItem -> {
                            java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SALE_ORDER, picklistItem.getSaleOrderCode());
                            lock.lock();
                            saleOrderLocks.add(lock);
                        });
                        receivePickBucketAtInvoicingInternal(pickBatch, context, response);
                        response.setPicklistCode(pickBatch.getPicklist().getCode());
                        if (!context.hasErrors()) {
                            response.setSuccessful(true);
                        }
                    } finally {
                        saleOrderLocks.forEach(java.util.concurrent.locks.Lock::unlock);
                        picklistLock.unlock();
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Transactional
    @LogActivity
    public void receivePickBucketAtInvoicingInternal(PickBatch pickBatch, ValidationContext context, ReceivePickBucketForWebResponse response) {
        pickBatch = pickerService.getPickBatchById(pickBatch.getId());
        pickBatch.setStatusCode(PickBatch.StatusCode.RECEIVED);
        Map<String, List<PicklistItem>> shippingPackageToPicklistItems = getShippingPackageToPicklistItems(pickBatch.getPicklistItems());
        shippingPackageToPicklistItems.entrySet().stream().filter(e -> e.getValue().size() > 0).filter(
                se -> se.getValue().stream().anyMatch(pi -> pi.getStatusCode().equals(PicklistItem.StatusCode.SUBMITTED))).forEach(shipmentEntry -> {
                    shipmentEntry.getValue().forEach(pi -> receivePicklistItem(pi.getPicklist(), pi));
                });
        Picklist picklist = pickBatch.getPicklist();
        if (Picklist.StatusCode.CREATED.name().equals(picklist.getStatusCode())) {
            picklist.setStatusCode(Picklist.StatusCode.RECEIVING.name());
        }
    }

    public Map<String, List<PicklistItem>> getShippingPackageToPicklistItems(Set<PicklistItem> picklistItems) {
        Map<String, List<PicklistItem>> shippingPackageToPicklistItems = new HashMap<>();
        picklistItems.forEach(picklistItem -> shippingPackageToPicklistItems.computeIfAbsent(picklistItem.getShippingPackageCode(), k -> new ArrayList<>()).add(picklistItem));
        return shippingPackageToPicklistItems;
    }

    private PicklistItem receivePicklistItem(final Picklist picklist, PicklistItem picklistItem) {
        if (picklistItem.getStatusCode().equals(PicklistItem.StatusCode.CREATED)) {
            picklistItem.setStatusCode(PicklistItem.StatusCode.RECEIVED);
        }
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(picklistItem.getShippingPackageCode());
        if (ShippingPackage.StatusCode.PICKING.name().equals(shippingPackage.getStatusCode()) && Picklist.Destination.INVOICING.equals(picklist.getDestination())) {
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKED.name());
            shippingService.updateShippingPackage(shippingPackage);
            //            ActivityLogger.forEvent(Event.SHIPPING_PACKAGE_PICKLIST).withGroupIdentifier(shippingPackage.getSaleOrder().getCode()).withIdentifier(shippingPackage.getCode()).add(
            //                    picklist.getCode()).add(shippingPackage.getStatusCode()).log();
        }
        return picklistItem;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPacklistRemainingQuantityResponse getPacklistRemainingQuantity(GetPacklistRemainingQuantityRequest request) {
        GetPacklistRemainingQuantityResponse response = new GetPacklistRemainingQuantityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else if (!ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled()) {
                context.addError(WsResponseCode.INVALID_FACILITY_CONFIGURATION, "PickingViaHandledEnabled is disabled");
            } else {
                List<GetPacklistRemainingQuantityResponse.PickBucketRemainingQuantityDTO> pickBucketScannedQuantity = new ArrayList<>();
                picklist.getPickBatches().forEach(pickBatch -> {
                    pickBucketScannedQuantity.add(new GetPacklistRemainingQuantityResponse.PickBucketRemainingQuantityDTO(pickBatch.getPickBucket().getCode(),
                            Math.toIntExact(pickBatch.getPicklistItems().stream().filter(pi -> PicklistItem.StatusCode.SUBMITTED.equals(pi.getStatusCode())).count())));
                });
                response.setPickBucketRemainingQuantity(pickBucketScannedQuantity);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

}