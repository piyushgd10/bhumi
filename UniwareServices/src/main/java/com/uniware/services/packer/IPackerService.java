/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 12, 2012
 *  @author singla
 */
package com.uniware.services.packer;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.template.Template;
import com.unifier.services.aspect.RollbackOnFailure;
import com.uniware.core.api.packer.AcceptPackagePutbackRequest;
import com.uniware.core.api.packer.AcceptPackagePutbackResponse;
import com.uniware.core.api.packer.AcceptPutbackItemRequest;
import com.uniware.core.api.packer.AcceptPutbackItemResponse;
import com.uniware.core.api.packer.AcceptPutbackRequest;
import com.uniware.core.api.packer.AcceptPutbackResponse;
import com.uniware.core.api.packer.AllocateItemInPackageRequest;
import com.uniware.core.api.packer.AllocateItemInPackageResponse;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageRequest;
import com.uniware.core.api.packer.CompleteReceivedPicklistItemsForPackageResposne;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityRequest;
import com.uniware.core.api.packer.GetPacklistRemainingQuantityResponse;
import com.uniware.core.api.packer.GetPacklistRequest;
import com.uniware.core.api.packer.GetPacklistResponse;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.packer.PickShippingPackageRequest;
import com.uniware.core.api.packer.PickShippingPackageResponse;
import com.uniware.core.api.packer.PrintPicklistItemRequest;
import com.uniware.core.api.packer.PrintPicklistItemResponse;
import com.uniware.core.api.packer.ReceivePickBucketForWebRequest;
import com.uniware.core.api.packer.ReceivePickBucketForWebResponse;
import com.uniware.core.api.packer.ReceivePicklistRequest;
import com.uniware.core.api.packer.ReceivePicklistResponse;
import com.uniware.core.api.packer.RemoveItemFromPicklistRequest;
import com.uniware.core.api.packer.RemoveItemFromPicklistResponse;
import com.uniware.core.api.packer.SuggestItemAllocationRequest;
import com.uniware.core.api.packer.SuggestItemAllocationResponse;
import com.uniware.core.api.picker.CompletePicklistRequest;
import com.uniware.core.api.picker.CompletePicklistResponse;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.EditPicklistResponse;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author singla
 */
public interface IPackerService {

    /**
     * @param getPacklistRequest
     * @return
     */
    GetPacklistResponse getPacklist(GetPacklistRequest getPacklistRequest);

    @Transactional
    ReceivePicklistResponse receivePicklist(ReceivePicklistRequest request);

    /**
     * @param request
     * @return
     */
    AcceptPutbackResponse acceptPutback(AcceptPutbackRequest request);

    /**
     * @param request
     * @return
     */
    SuggestItemAllocationResponse suggestItemAllocation(SuggestItemAllocationRequest request);

    String getWorkorderHtml(String shippingPackageCode, Template template);

    AllocateItemInPackageResponse allocateItemInPackage(AllocateItemInPackageRequest request);

    PickShippingPackageResponse pickShippingPackage(PickShippingPackageRequest request);

    PrintPicklistItemResponse printPicklistItem(PrintPicklistItemRequest request);

    AcceptPackagePutbackResponse acceptPackagePutback(AcceptPackagePutbackRequest request);

    AcceptPutbackItemResponse acceptPutbackItem(AcceptPutbackItemRequest request);

    MarkPicklistItemsNotFoundResponse markPicklistItemsNotFound(MarkPicklistItemsNotFoundRequest request);

    EditPicklistResponse editPicklist(EditPicklistRequest request);

    CompletePicklistResponse completePicklist(CompletePicklistRequest request);

    RemoveItemFromPicklistResponse removeItemFromPicklist(RemoveItemFromPicklistRequest request);

    CompleteReceivedPicklistItemsForPackageResposne completePackagePicklistItems(CompleteReceivedPicklistItemsForPackageRequest request);

    ReceivePickBucketForWebResponse receivePickBucketAtInvoicing(ReceivePickBucketForWebRequest request);

    @Transactional(readOnly = true) GetPacklistRemainingQuantityResponse getPacklistRemainingQuantity(GetPacklistRemainingQuantityRequest request);
}
