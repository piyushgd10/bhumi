/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.services.picker.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.uniware.core.entity.SaleOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.codahale.metrics.annotation.Timed;
import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.template.Template;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.InMemoryEvictionCache;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.picker.AbstractCreatePicklistRequest;
import com.uniware.core.api.picker.AssignPicklistToUserRequest;
import com.uniware.core.api.picker.AssignPicklistToUserResponse;
import com.uniware.core.api.picker.CompletePickingRequest;
import com.uniware.core.api.picker.CompletePickingResponse;
import com.uniware.core.api.picker.CreateAutoPicklistRequest;
import com.uniware.core.api.picker.CreateBulkPickBucketRequest;
import com.uniware.core.api.picker.CreateBulkPickBucketResponse;
import com.uniware.core.api.picker.CreateManualPicklistRequest;
import com.uniware.core.api.picker.CreatePickBatchRequest;
import com.uniware.core.api.picker.CreatePickBatchResponse;
import com.uniware.core.api.picker.CreatePickBucketRequest;
import com.uniware.core.api.picker.CreatePickBucketResponse;
import com.uniware.core.api.picker.CreatePicklistResponse;
import com.uniware.core.api.picker.DisablePickBucketRequest;
import com.uniware.core.api.picker.DisablePickBucketResponse;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.EnablePickBucketRequest;
import com.uniware.core.api.picker.EnablePickBucketResponse;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsRequest;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsResponse;
import com.uniware.core.api.picker.GetPicklistDetailRequest;
import com.uniware.core.api.picker.GetPicklistDetailResponse;
import com.uniware.core.api.picker.GetPicklistRequest;
import com.uniware.core.api.picker.GetPicklistResponse;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountRequest;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountResponse;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountResponse.PicksetDTO;
import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistRequest;
import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistResponse;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuRequest;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuResponse;
import com.uniware.core.api.picker.PicklistDTO;
import com.uniware.core.api.picker.PicklistItemDTO;
import com.uniware.core.api.picker.PutbackItemDetailDTO;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.api.picker.SearchPicklistResponse;
import com.uniware.core.api.picker.SubmitPickBucketRequest;
import com.uniware.core.api.picker.SubmitPickBucketResponse;
import com.uniware.core.api.picker.UpdatePickBucketRequest;
import com.uniware.core.api.picker.UpdatePickBucketResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.concurrent.ContextAwareExecutor;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.PicklistStatus;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.ActivityContext;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.dao.picker.IPickerDao;
import com.uniware.services.admin.pickset.IPicksetService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.audit.impl.ActivityUtils;
import com.uniware.services.cache.PickerCache;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.configuration.PickConfiguration;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.item.IItemService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.picker.PickPlanner;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.warehouse.IShelfService;

/**
 * @author singla
 */
@Service
public class PickerServiceImpl implements IPickerService {

    private static final Logger            LOG                                       = LoggerFactory.getLogger(PickerServiceImpl.class);
    private static final String            SERVICE_NAME                              = "pickingService";

    @Autowired
    private IPickerDao                     pickerDao;

    @Autowired
    private ICatalogService                catalogService;

    @Autowired
    private IShippingService               shippingService;

    @Autowired
    private IInventoryService              inventoryService;

    @Autowired
    private ISaleOrderService              saleOrderService;

    @Autowired
    private IUsersService                  usersService;

    @Autowired
    private IPicksetService                picksetService;

    @Autowired
    private ILockingService                lockingService;

    @Autowired
    private IPackerService                 packerService;

    @Autowired
    private IShelfService                  shelfService;

    @Autowired
    private IItemService                   itemService;

    @Autowired
    private ContextAwareExecutorFactory    executorFactory;

    private InMemoryEvictionCache<Integer> picksetToPackageCountCache                = new InMemoryEvictionCache<>();

    private static final Integer           PICK_SET_PACKAGE_COUNT_CACHE_TIME_SECONDS = 180;

    @Transactional
    @Override
    public CreatePicklistResponse createManualPicklist(CreateManualPicklistRequest request) {
        ValidationContext context = request.validate();
        Picklist picklist = null;
        Supplier<List<ShippingPackage>> shippingPackagesSupplier = null;
        PickSet pickSet = validateAndGetPickSet(request, context, request.getShippingPackageCodes().size());
        if (!context.hasErrors()) {
            if (Picklist.Destination.STAGING.equals(request.getDestination())
                    && SystemConfiguration.TraceabilityLevel.NONE.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "Cannot create staging picklist for NONE traceability");
            } else {
                picklist = preparePicklist(request, pickSet);
                picklist.setType(Picklist.Type.MANUAL.name());
                shippingPackagesSupplier = () -> {
                    if (pickSet.getType().equals(PickSet.Type.STAGING)) {
                        return pickerDao.getPackagesForPickingFromStaging(request.getShippingPackageCodes(), pickSet.getId()).stream().filter(
                                this::isPackageReadyForPickingFromStaging).collect(Collectors.toList());
                    } else {
                        return pickerDao.getPackagesWithPickableSaleOrderItems(request.getShippingPackageCodes(), pickSet.getId());
                    }
                };
            }
        }
        return doCreatePicklist(picklist, shippingPackagesSupplier, context, pickSet);
    }

    private boolean isPackageReadyForPickingFromStaging(ShippingPackage shippingPackage) {
        return shippingPackage.getSaleOrderItems().stream().allMatch(soi -> soi.getStatusCode().equals(SaleOrderItem.StatusCode.STAGED.name()));
    }

    private PickSet validateAndGetPickSet(AbstractCreatePicklistRequest request, ValidationContext context, int numberOfItems) {
        PickSet pickSet = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSetById(request.getPickSetId());
        if (pickSet == null) {
            context.addError(WsResponseCode.INVALID_PICKSET_ID, "Invalid PickSet Id : " + request.getPickSetId());
        } else {
            SystemConfiguration systemConfiguration = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class);
            int maxAllowedShipmentsInPicklist = systemConfiguration.getMaxAllowedShipmentsInPicklist();
            if (systemConfiguration.isPickingViaHandheldEnabled() && maxAllowedShipmentsInPicklist < numberOfItems) {
                context.addError(WsResponseCode.INVALID_REQUEST, "More than " + maxAllowedShipmentsInPicklist + " items are not allowed in Picklist");
            }
        }
        return pickSet;
    }

    private Picklist preparePicklist(AbstractCreatePicklistRequest request, PickSet pickSet) {
        Picklist picklist = new Picklist();
        picklist.setUser(usersService.getUserWithDetailsById(request.getUserId()));
        picklist.setStatusCode(Picklist.StatusCode.CREATED.name());
        picklist.setPickSetName(pickSet.getName());
        picklist.setDestination(request.getDestination());
        Date now = DateUtils.getCurrentTime();
        picklist.setCreated(now);
        picklist.setUpdated(now);
        return picklist;
    }

    private CreatePicklistResponse doCreatePicklist(Picklist picklist, Supplier<List<ShippingPackage>> shippingPackagesListSupplier, ValidationContext context, PickSet pickSet) {
        if (!context.hasErrors()) {
            try {
                LOG.info("Getting Packages for picklist creation, picklist type : {}", picklist.getType());
                List<ShippingPackage> packages = shippingPackagesListSupplier.get();
                LOG.info("Found {}  packages for picklist", packages.size());
                if (packages.size() > 0) {
                    if (packages.stream().anyMatch(shippingPackage -> isShippingPackageValidForPicklistCreation(shippingPackage, pickSet))) {
                        picklist = addPicklist(picklist);
                        ForkJoinPool picklistItemPool = new ForkJoinPool(10);
                        Picklist finalPicklist = picklist;
                        UserContext userContext = UserContext.current();
                        picklistItemPool.submit(
                                () -> packages.parallelStream().filter(shippingPackage -> isShippingPackageValidForPicklistCreation(shippingPackage, pickSet)).forEach(
                                        shippingPackage -> {
                                            UserContext.setUserContext(userContext);
                                            createPicklistItemsForPackage(finalPicklist, context, shippingPackage, pickSet);
                                        })).get();
                    }
                    if (Picklist.Type.MANUAL.name().equals(picklist.getType())) {
                        scheduleReplanPicking(pickSet.getId());
                    }
                } else {
                    context.addError(WsResponseCode.NO_SHIPPING_PACKAGE_AVAILABLE_TO_PICK, "No shipping packages available to pick at this time");
                }
            } catch (Exception e) {
                LOG.error("Exception while create picklist", e);
                context.addError(WsResponseCode.INTERNAL_ERROR, "Internal error while creating picklist");
            }
        }
        CreatePicklistResponse response = new CreatePicklistResponse();
        if (!context.hasErrors()) {
            response.setSuccessful(true);
            PicklistDTO picklistDTO = new PicklistDTO();
            picklistDTO.setCode(picklist.getCode());
            response.setPicklist(picklistDTO);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private Picklist addPicklist(Picklist picklist) {
        return pickerDao.addPicklist(picklist);
    }

    private boolean isShippingPackageValidForPicklistCreation(ShippingPackage shippingPackage, PickSet pickSet) {
        return (PickSet.Type.STOCKING.equals(pickSet.getType())
                && StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name()))
                || (PickSet.Type.STAGING.equals(pickSet.getType()) && ShippingPackage.StatusCode.PICKING.name().equals(shippingPackage.getStatusCode()));
    }

    @Transactional
    private Picklist createPicklistItemsForPackage(Picklist picklist, ValidationContext context, ShippingPackage shippingPackage, PickSet pickSet) {
        List<java.util.concurrent.locks.Lock> locks = new ArrayList<>();
        // keep them sorted to avoid deadlock
        Set<String> shelfCodesToLock = new TreeSet<>();
        if (SystemConfiguration.InventoryAccuracyLevel.SHELF.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel())) {
            shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId());
            shippingPackage.getSaleOrderItems().forEach(soi -> shelfCodesToLock.add(soi.getItemTypeInventory().getShelf().getCode()));
        }
        try {
            shelfCodesToLock.forEach(shelfCode -> {
                java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SHELF, shelfCode, Level.FACILITY);
                lock.lock();
                locks.add(lock);
            });
            return createPicklistItemsForPackageInternal(picklist, context, shippingPackage, pickSet);
        } finally {
            locks.forEach(java.util.concurrent.locks.Lock::unlock);
        }
    }

    /**
     * Method is public so that {@link com.unifier.services.aspect.TransactionRollbackAspect} can catch it.
     *
     * @param picklist
     * @param context
     * @param shippingPackage
     * @param pickSet
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrder.code}") })
    @RollbackOnFailure
    private Picklist createPicklistItemsForPackageInternal(Picklist picklist, ValidationContext context, ShippingPackage shippingPackage, PickSet pickSet) {
        switch (picklist.getDestination()) {
            case STAGING:
                shippingPackage = shippingService.getShippingPackageWithSaleOrderItemsForPickSet(shippingPackage.getCode(), pickSet.getId());
                break;
            case INVOICING:
                shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId());
                break;
            default:
                break;
        }
        Set<Integer> alreadyCheckedShelf = new HashSet<>();
        Set<Integer> saleOrderItemIds = new HashSet<>(shippingPackage.getSaleOrderItems().size());
        Picklist finalPicklist = picklist;
        shippingPackage.getSaleOrderItems().stream().filter(soi -> !context.hasErrors()).forEach(soi -> {
            Integer shelfId = soi.getItemTypeInventory().getShelf().getId();
            if (!alreadyCheckedShelf.contains(shelfId)) {
                alreadyCheckedShelf.add(shelfId);
                Shelf shelf = shelfService.getShelfById(shelfId);
                if (shelf != null && shelf.isBlockedForCycleCount()) {
                    context.addError(WsResponseCode.SHELF_BLOCKED_FOR_CYCLE_COUNT,
                            soi.getCode() + " - could not be added, Shelf - " + shelf.getCode() + " is blocked for cycle count");
                }
            }
            createPicklistItem(finalPicklist, soi, context);
            saleOrderItemIds.add(soi.getId());
        });
        if (!context.hasErrors()) {
            picklist = savePicklistItemsForPackage(finalPicklist, shippingPackage, saleOrderItemIds);
        }
        return picklist;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.AUTOCREATE_PICKLIST, key = "#{#args[0].pickSetId}") })
    @Transactional
    public CreatePicklistResponse createAutoPicklist(CreateAutoPicklistRequest request) {
        ValidationContext context = request.validate();
        PickSet pickSet = validateAndGetPickSet(request, context, request.getNoOfItems());
        Picklist picklist = null;
        Supplier<List<ShippingPackage>> shippingPackagesSupplier = null;
        if (!context.hasErrors()) {
            Picklist.Destination destination = request.getDestination();
            request.setDestination(destination);
            picklist = preparePicklist(request, pickSet);
            picklist.setType(Picklist.Type.AUTO.name());
            shippingPackagesSupplier = () -> getShipmentsForAutoPicklist(pickSet.getId(), destination, request.getNoOfItems());
        }
        return doCreatePicklist(picklist, shippingPackagesSupplier, context, pickSet);
    }

    private Picklist savePicklistItemsForPackage(Picklist picklist, ShippingPackage shippingPackage, Set<Integer> saleOrderItemIds) {
        String soiStatusCodeToUpdate = null;
        switch (picklist.getDestination()) {
            case STAGING:
                soiStatusCodeToUpdate = SaleOrderItem.StatusCode.PICKING_FOR_STAGING.name();
                break;
            case INVOICING:
                soiStatusCodeToUpdate = SaleOrderItem.StatusCode.PICKING_FOR_INVOICING.name();
                break;
        }
        if (soiStatusCodeToUpdate != null) {
            saleOrderService.updateSaleOrderItemStatusForPicklistCreation(saleOrderItemIds, soiStatusCodeToUpdate);
        }
        if (ShippingPackage.StatusCode.CREATED.name().equals(shippingPackage.getStatusCode())) {
            shippingPackage.setStatusCode(ShippingPackage.StatusCode.PICKING.name());
        }
        return picklist;
    }

    private boolean createPicklistItem(Picklist picklist, SaleOrderItem saleOrderItem, ValidationContext context) {
        if (SaleOrderItem.StatusCode.FULFILLABLE.name().equals(saleOrderItem.getStatusCode())
                || (SaleOrderItem.StatusCode.STAGED.name().equals(saleOrderItem.getStatusCode()) && picklist.getDestination().equals(Picklist.Destination.INVOICING))) {
            PickSet pickSet = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSetByName(picklist.getPickSetName());
            PicklistItem picklistItem = new PicklistItem(picklist, saleOrderItem);
            if (PickSet.Type.STOCKING.equals(pickSet.getType())) {
                picklistItem.setShelfCode(saleOrderItem.getItemTypeInventory().getShelf().getCode());
            } else if (PickSet.Type.STAGING.equals(pickSet.getType()) && SaleOrderItem.StatusCode.STAGED.name().equals(saleOrderItem.getStatusCode())) {
                // copy item code and shelf code from last PicklistItem
                PicklistItem toStagingPicklistItem = pickerDao.getStockingToStagingPicklistItem(saleOrderItem.getCode(), saleOrderItem.getSaleOrder().getCode(),
                        saleOrderItem.getShippingPackage().getCode());
                picklistItem.setItemCode(toStagingPicklistItem.getItemCode());
                // this is source shelf code
                picklistItem.setShelfCode(toStagingPicklistItem.getShelfCode());
                picklistItem.setStagingShelfCode(saleOrderItem.getShippingPackage().getShelfCode());
            }
            pickerDao.addPicklistItem(picklistItem);
            picklist.getPicklistItems().add(picklistItem);
            //ActivityLogger.forEvent(Event.PICKLIST_CREATE).withGroupIdentifier(saleOrderItem.getSaleOrder().getCode()).withIdentifier(saleOrderItem.getCode()).add(
            //saleOrderItem.getShippingPackage().getCode()).add(picklist.getCode()).add(saleOrderItem.getShippingPackage().getStatusCode()).log();
            if (ActivityContext.current().isEnable()) {
                ActivityUtils.appendActivity(saleOrderItem.getCode(), ActivityEntityEnum.SALE_ORDER_ITEM.getName(), saleOrderItem.getSaleOrder().getCode(),
                        Arrays.asList(new String[] {
                                "Sale Order Item {" + ActivityEntityEnum.SALE_ORDER_ITEM + ":" + saleOrderItem.getCode() + "} items added to picklist {"
                                        + ActivityEntityEnum.PICKLIST + ":" + picklist.getCode() + "} and status :" + saleOrderItem.getStatusCode() }),
                        ActivityTypeEnum.PROCESSING.name());
            }
            return true;
        } else {
            context.addError(WsResponseCode.INVALID_SALE_ORDER_ITEM_STATE, saleOrderItem.getCode() + " - is not in a valid state : " + saleOrderItem.getStatusCode());
        }
        return false;
    }

    // Lock to ensure that if pick planner is loaded successfully
    @Locks({ @Lock(ns = Namespace.PICKER, key = "#{#args[0]}") })
    private List<ShippingPackage> getShipmentsForAutoPicklist(Integer pickSetId, Picklist.Destination destination, Integer noOfItems) {
        PickerCache pickerCache = CacheManager.getInstance().getCache(PickerCache.class);
        PickPlanner pickPlanner = pickerCache.getPickPlannerByPickSetId(pickSetId, destination);
        if (pickPlanner == null || pickPlanner.getPlannedItemsCount() == 0) {
            replanPicking(pickSetId);
        }
        pickPlanner = pickerCache.getPickPlannerByPickSetId(pickSetId, destination);
        List<ShippingPackage> packages = pickPlanner.getPicklist(noOfItems).stream().map(
                dto -> shippingService.getShippingPackageWithSaleOrderByCode(dto.getShippingPackageCode())).collect(Collectors.toList());
        pickerCache.updatePickplanner(pickSetId, pickPlanner, destination);
        return packages;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PicklistDTO getPicklistDTO(Integer picklistId) {
        return getPicklistDTO(pickerDao.getPicklistById(picklistId));
    }

    private PicklistDTO getPicklistDTO(Picklist picklist) {
        PicklistDTO picklistDTO = new PicklistDTO(picklist);
        Map<Integer, ItemType> idToItemType = new HashMap<>();
        Map<Integer, ItemTypeInventory> idToItemTypeInventory = new HashMap<>();
        Map<String, ShippingPackage> codeToShippingPackge = new HashMap<>();
        Map<String, SaleOrder> codeToSaleOrder = new HashMap<>();
        Map<String, Map<String, SaleOrderItem>> spCodeToSoiCodeToSoi = new HashMap<>();
        Map<String, Map<String, SaleOrderItem>> soCodeToSoiCodeToSoi = new HashMap<>();
        picklist.getPicklistItems().forEach(picklistItem -> {
            PicklistItemDTO picklistItemDTO = new PicklistItemDTO();
            ShippingPackage shippingPackage = codeToShippingPackge.computeIfAbsent(picklistItem.getShippingPackageCode(), k -> shippingService.getShippingPackageByCode(k));
            SaleOrderItem saleOrderItem = spCodeToSoiCodeToSoi.computeIfAbsent(shippingPackage.getCode(), k -> {
                Map<String, SaleOrderItem> soiCodeToSoi = new HashMap<>(shippingPackage.getSaleOrderItems().size());
                shippingPackage.getSaleOrderItems().forEach(soi -> soiCodeToSoi.put(soi.getCode(), soi));
                return soiCodeToSoi;
            }).get(picklistItem.getSaleOrderItemCode());
            // Might have been removed from shipping package, so fetching from SaleOrder
            if (saleOrderItem == null){
                SaleOrder saleOrder = codeToSaleOrder.computeIfAbsent(picklistItem.getSaleOrderCode(), k -> saleOrderService.getSaleOrderByCode(picklistItem.getSaleOrderCode()));
                saleOrderItem = soCodeToSoiCodeToSoi.computeIfAbsent(saleOrder.getCode(), k -> {
                    Map<String, SaleOrderItem> soiCodeToSoi = new HashMap<>(saleOrder.getSaleOrderItems().size());
                    saleOrder.getSaleOrderItems().forEach(soi -> soiCodeToSoi.put(soi.getCode(), soi));
                    return soiCodeToSoi;
                }).get(picklistItem.getSaleOrderItemCode());
            }
            if (saleOrderItem.getItemTypeInventory() != null) {
                SaleOrderItem finalSaleOrderItem = saleOrderItem;
                ItemTypeInventory itemTypeInventory = idToItemTypeInventory.computeIfAbsent(saleOrderItem.getItemTypeInventory().getId(),
                        k -> inventoryService.getItemTypeInventoryById(finalSaleOrderItem.getItemTypeInventory().getId()));
                picklistItemDTO.setShelfCode(itemTypeInventory.getShelf().getCode());
            }
            ItemType itemType = idToItemType.computeIfAbsent(saleOrderItem.getItemType().getId(), k -> catalogService.getNonBundledItemTypeById(k));
            picklistItemDTO.setItemSku(itemType.getSkuCode());
            picklistItemDTO.setItemName(itemType.getName());
            picklistItemDTO.setBrand(itemType.getBrand());
            picklistItemDTO.setColor(itemType.getColor());
            picklistItemDTO.setSize(itemType.getSize());
            picklistItemDTO.setMaxRetailPrice(itemType.getMaxRetailPrice());
            // TODO remove
            picklistItemDTO.setSlot(0);
            picklistItemDTO.setSaleOrderItemId(saleOrderItem.getId());
            picklistItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
            picklistItemDTO.setChannelName(saleOrderItem.getSaleOrder().getChannel().getName());
            picklistItemDTO.setStatusCode(picklistItem.getStatusCode().name());
            picklistItemDTO.setSaleOrderCode(saleOrderItem.getSaleOrder().getCode());
            picklistItemDTO.setDisplayOrderCode(saleOrderItem.getSaleOrder().getDisplayOrderCode());
            picklistItemDTO.setNumberOfItems(shippingPackage.getNoOfItems());
            picklistItemDTO.setShippingPackageStatusCode(shippingPackage.getStatusCode());
            picklistItemDTO.setItemTypeCustomFields(CustomFieldUtils.getCustomFieldValues(itemType));
            picklistItemDTO.setShippingPackageCode(shippingPackage.getCode());
            if (saleOrderItem.getItem() != null) {
                picklistItemDTO.setItemCode(saleOrderItem.getItem().getCode());
            }
            picklistDTO.getPicklistItems().add(picklistItemDTO);
        });
        (picklistDTO.getPicklistItems()).sort(new Comparator<PicklistItemDTO>() {
            @Override
            public int compare(PicklistItemDTO o1, PicklistItemDTO o2) {
                int compareTo = 0;
                if (StringUtils.isNotBlank(o1.getShelfCode()) && StringUtils.isNotBlank(o2.getShelfCode())) {
                    compareTo = o1.getShelfCode().compareTo(o2.getShelfCode());
                }
                compareTo = compareTo == 0 ? o1.getItemSku().compareTo(o2.getItemSku()) : compareTo;
                return compareTo == 0 ? ((Integer) o1.getSlot()).compareTo(o2.getSlot()) : compareTo;
            }
        });
        picklistDTO.setUsername(picklist.getUser().getUsername());
        return picklistDTO;
    }

    /**
     * @param pickSetId
     * @return
     */
    public List<ShippingPackage> regeneratePackageList(int pickSetId) {
        PickConfiguration pickConfiguration = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class);
        PickSet pickSet = pickConfiguration.getPickSetById(pickSetId);
        if (pickSet == null) {
            return Collections.emptyList();
        }
        return pickerDao.getShippingPackages(pickSetId, pickSet.getCapacity());
    }

    /* (non-Javadoc)
     * @see com.uniware.services.picker.IPickerService#getPicklist(com.uniware.core.api.picker.GetPicklistRequest)
     */
    @Override
    @Timed
    @Transactional
    public GetPicklistResponse getPicklist(GetPicklistRequest request) {
        GetPicklistResponse response = new GetPicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerDao.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE);
            } else {
                response.setSuccessful(true);
                response.setPicklist(getPicklistDTO(picklist));
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void scheduleReplanPicking(Integer pickSetId) {
        ContextAwareExecutor pickingExecutor = executorFactory.getExecutor(SERVICE_NAME);
        LOG.info("Submitting replan Picking for pickset {} to executor. Queue size: {}, active count: {}", pickSetId, pickingExecutor.getQueue().size(), pickingExecutor.getActiveCount());
        pickingExecutor.submit(new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                LOG.info("Starting replan picking for pickset: {} ", pickSetId);
                replanPicking(pickSetId);
                LOG.info("Done replan picking for pickset: {} in {} ms", pickSetId, (System.currentTimeMillis() - startTime));
            }
        });
    }

    /**
     * @param pickSetId
     */
    @Override
    @Timed
    @Locks({ @Lock(ns = Namespace.PICKER, key = "#{#args[0]}") })
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public void replanPicking(int pickSetId) {
        LOG.info("Replanning Picking for pickSetId : {}", pickSetId);
        PickerCache pickerCache = CacheManager.getInstance().getCache(PickerCache.class);
        pickerCache.resetShippingPackages(pickSetId, regeneratePackageList(pickSetId));
    }

    /* (non-Javadoc)
     * @see com.uniware.services.picker.IPickerService#handlePicklistItemCancellation(int)
     */
    @Override
    @Transactional
    public boolean handlePicklistItemCancellation(SaleOrderItem saleOrderItem) {
        if (StringUtils.equalsAny(saleOrderItem.getShippingPackage().getStatusCode(), ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.PICKING.name())) {
            PicklistItem picklistItem = getPicklistItemBySaleOrderItemCodeAndShippingPackageCodeAndDestination(saleOrderItem.getCode(), saleOrderItem.getSaleOrder().getCode(),
                    saleOrderItem.getShippingPackage().getCode(), null);
            if (picklistItem != null) {
                if (picklistItem.isAssessed() && Picklist.Destination.INVOICING.equals(picklistItem.getPicklist().getDestination())) {
                    return false;
                } else {
                    if (saleOrderItem.getItemTypeInventory() != null) {
                        picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_PENDING);
                    } else {
                        picklistItem.setStatusCode(PicklistItem.StatusCode.PUTBACK_COMPLETE);
                    }
                }
            }
        }
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getPicklistItemBySaleOrderItemCodeAndShippingPackageCodeAndDestination(String saleOrderItemCode, String SaleOrderCode, String shippingPackageCode,
            Picklist.Destination destination) {
        return pickerDao.getPicklistItemBySaleOrderItemCodeAndShippingPackageCode(saleOrderItemCode, SaleOrderCode, shippingPackageCode, destination);
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public SearchPicklistResponse searchPicklist(SearchPicklistRequest request) {
        SearchPicklistResponse response = new SearchPicklistResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<Picklist> picklists = pickerDao.searchPicklist(request);
            if (request.getSearchOptions() != null && request.getSearchOptions().isGetCount()) {
                Long count = pickerDao.getPicklistCount(request);
                response.setTotalRecords(count);
            }
            for (Picklist picklist : picklists) {
                response.getElements().add(new PicklistDTO(picklist));
            }
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public Picklist getPicklistForShippingPackage(String shippingPackageCode) {
        return pickerDao.getPicklistForShippingPackage(shippingPackageCode);
    }

    @Override
    @Transactional
    public List<PicklistItem> getPicklistItemsByShippingPackageCode(String shippingPackageCode, Picklist.Destination destination) {
        return pickerDao.getPicklistItemsByShippingPackageCode(shippingPackageCode, destination);
    }

    @Override
    @Timed
    public GetPicksetsWithPackageCountResponse getPicksetsWithPackageCount(GetPicksetsWithPackageCountRequest request) {
        GetPicksetsWithPackageCountResponse response = new GetPicksetsWithPackageCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<PickSet> pickSets = ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSets();
            List<PicksetDTO> picksets = new ArrayList<PicksetDTO>();
            for (PickSet pickSet : pickSets) {
                if (pickSet.isActive()) {
                    picksets.add(new PicksetDTO(pickSet.getId(), pickSet.getName(), getPendingShippingPackageCountInPickset(pickSet.getId())));
                }
            }
            response.setPicksets(picksets);
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private Integer getPendingShippingPackageCountInPickset(int picksetId) {
        Integer packageCount = picksetToPackageCountCache.get(String.valueOf(picksetId));
        if (packageCount == null) {
            synchronized (this) {
                packageCount = picksetToPackageCountCache.get(String.valueOf(picksetId));
                if (packageCount == null) {
                    packageCount = getPendingShippingPackageCountInPicksetFromDB(picksetId);
                    picksetToPackageCountCache.set(String.valueOf(picksetId), PICK_SET_PACKAGE_COUNT_CACHE_TIME_SECONDS, packageCount);
                }
            }
        }
        return packageCount;
    }

    @Override
    public GetPickSetWiseItemCountsResponse getPickSetWisePackageCounts(GetPickSetWiseItemCountsRequest request) {
        GetPickSetWiseItemCountsResponse response = new GetPickSetWiseItemCountsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (PickSet pickSet : ConfigurationManager.getInstance().getConfiguration(PickConfiguration.class).getPickSets()) {
                PickerCache pickerCache = CacheManager.getInstance().getCache(PickerCache.class);
                int plannedItemsCount = pickerCache.getPlannedItemsCount(pickSet.getId(), request.getDestination());
                response.getPickSetCountDTOs().add(new GetPickSetWiseItemCountsResponse.PickSetCountDTO(pickSet.getId(), pickSet.getName(), plannedItemsCount));
            }
        }
        if (context.hasWarnings()) {
            response.addWarnings(context.getWarnings());
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional(readOnly = true)
    private Integer getPendingShippingPackageCountInPicksetFromDB(int picksetId) {
        return pickerDao.getShippingPackages(picksetId).size();
    }

    @Override
    @Transactional
    public PickBucket getPickBucketByCode(String pickBucketCode) {
        return pickerDao.getPickBucketByCode(pickBucketCode);
    }

    @Override
    @Transactional(readOnly = true)
    public PickBatch getOpenPickBatchByPickBucketCode(String pickBucketCode) {
        return pickerDao.getOpenPickBatchByPickBucket(pickBucketCode, false);
    }

    @Override
    @Transactional(readOnly = true)
    public PickBatch getPickBatchById(Integer id) {
        return pickerDao.getPickBatchById(id);
    }

    @Override
    public PicklistItem getOpenPicklistItemByItemCode(String itemCode) {
        return pickerDao.getOpenPicklistItemByItemCode(itemCode);
    }

    @Override
    public PicklistItem pickAndAllocateItemToPicklistItem(ValidationContext context, Item item, Picklist picklist, SaleOrderItem saleOrderItem) {
        PicklistItem picklistItem = null;
        if (getOpenPicklistItemByItemCode(item.getCode()) != null) {
            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Item already present in an open picklist : " + item.getCode());
            LOG.info("Item already present in an open picklist: " + item.getCode());
        } else {
            item.setStatusCode(Item.StatusCode.PICKED.name());
            //            ActivityLogger.forEvent(Event.ITEM_PICKED).withIdentifier(item.getCode()).add(picklist.getCode()).log();
            // soft allocate saleOrderItem to item. Actual allocation will happen at the time of invoicing.
            if (saleOrderItem == null) {
                try {
                    saleOrderItem = saleOrderService.getSaleOrderItemForAllocationInPicklist(picklist.getCode(), item);
                } catch (IllegalStateException e) {
                    LOG.info("Item: {} not assigned to any shelf. Failed to soft allocate SaleOrderItem. Picklist: {}", item.getCode(), picklist.getCode());
                    context.addError(WsResponseCode.INTERNAL_ERROR, e.getMessage());
                }
            }
            if (!context.hasErrors()) {
                if (saleOrderItem == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "No valid picklist item found for item: " + item.getCode());
                    LOG.info("No valid picklist item found for item: {} in picklist: {}", item.getCode(), picklist.getCode());
                } else {
                    picklistItem = getPicklistItemBySaleOrderItemCode(picklist.getId(), saleOrderItem.getCode(), saleOrderItem.getSaleOrder().getCode());
                    picklistItem.setItemCode(item.getCode());
                }
            }
        }
        return picklistItem;
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getPicklistItemById(Integer picklistItemId) {
        return pickerDao.getPicklistItemById(picklistItemId);
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemOrSkuCode, PickBatch pickBatch) {
        return pickerDao.getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(itemOrSkuCode, pickBatch.getId(), PicklistItem.QCStatus.PENDING);
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemOrSkuCode, PickBatch pickBatch, PicklistItem.QCStatus qcStatusCode) {
        return pickerDao.getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(itemOrSkuCode, pickBatch.getId(), qcStatusCode);
    }

    @Override
    @Transactional
    public void updatePicklistItem(PicklistItem picklistItem) {
        pickerDao.updatePicklistItem(picklistItem);
    }

    @Override
    public SubmitPickBucketResponse submitPickBucket(SubmitPickBucketRequest request) {
        SubmitPickBucketResponse response = new SubmitPickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = getOpenPickBatchByPickBucketCode(pickBucket.getCode());
                if (pickBatch == null) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "No open pick batch found for pick bucket");
                } else {
                    submitPickBucketInternal(request, context, response, pickBatch);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Override
    @Transactional
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public CreatePickBatchResponse createPickBatch(CreatePickBatchRequest request) {
        CreatePickBatchResponse response = new CreatePickBatchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code : " + request.getPicklistCode());
            } else if (!StringUtils.equalsAny(picklist.getStatusCode(), Picklist.StatusCode.CREATED.name(), Picklist.StatusCode.RECEIVING.name())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Picklist State : " + picklist.getStatusCode());
            } else if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled() && picklist.getPicker() == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Picklist unassigned");
            } else if (picklist.getPicker() != null && !picklist.getPicker().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Picklist is assigned to different user");
            } else {
                PickBucket pickBucket = pickerDao.getPickBucketByCode(request.getPickBucketCode());
                if (pickBucket == null) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
                } else if (!pickBucket.getCode().equals(Constants.DEFAULT) && !PickBucket.StatusCode.AVAILABLE.equals(pickBucket.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
                } else if (Constants.DEFAULT.equals(pickBucket.getCode())
                        && ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled()) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "DEFAULT pick bucket not allowed in handheld enabled facility");
                } else {
                    picklist.getPickBatches().forEach(pickBatch -> {
                        if (PickBatch.StatusCode.PICKING.equals(pickBatch.getStatusCode())) {
                            context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist already has one pick bucket in PICKING ");
                        }
                    });
                    if (!context.hasErrors()) {
                        PickBatch pickBatch = new PickBatch();
                        pickBatch.setPicklist(picklist);
                        pickBatch.setPickBucket(pickBucket);
                        pickBatch.setCreated(DateUtils.getCurrentTime());
                        pickBatch.setStatusCode(PickBatch.StatusCode.PICKING);
                        pickBatch = pickerDao.addPickBatch(pickBatch);
                        if (!Constants.DEFAULT.equals(pickBucket.getCode())) {
                            pickBucket.setStatusCode(PickBucket.StatusCode.IN_USE);
                        }
                    }
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    private void submitPickBucketInternal(SubmitPickBucketRequest request, ValidationContext context, SubmitPickBucketResponse response, PickBatch pickBatch) {
        if (!CollectionUtils.isEmpty(request.getItemCodes())) {
            // Item to be marked as PICKED
            List<java.util.concurrent.locks.Lock> locks = new ArrayList<>(request.getItemCodes().size());
            try {
                request.getItemCodes().stream().sorted(String::compareTo).filter(itemCode -> !context.hasErrors()).forEach(itemCode -> {
                    java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.ITEM, itemCode, Level.FACILITY);
                    try {
                        lock.tryLock(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds(), TimeUnit.SECONDS);
                        locks.add(lock);
                    } catch (InterruptedException e) {
                        LOG.info("Failed to acquire lock on item: {}", itemCode);
                        context.addError(WsResponseCode.INTERNAL_ERROR, "Failed to acquire lock on item " + itemCode);
                    }
                });
                if (!context.hasErrors()) {
                    doSubmitPickBucketInternal(request, context, response, pickBatch);
                }
            } finally {
                locks.forEach(java.util.concurrent.locks.Lock::unlock);
            }
        } else {
            doSubmitPickBucketInternal(request, context, response, pickBatch);
        }
    }

    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[3].picklist.code}", level = Level.FACILITY) })
    @Transactional
    @RollbackOnFailure
    @LogActivity
    private void doSubmitPickBucketInternal(SubmitPickBucketRequest request, ValidationContext context, SubmitPickBucketResponse response, PickBatch pickBatch) {
        Picklist picklist = getPicklistByCode(pickBatch.getPicklist().getCode());
        if (Picklist.StatusCode.CLOSED.name().equals(picklist.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid picklist state: " + picklist.getStatusCode());
        } else if (picklist.getPicker() == null) {
            context.addError(WsResponseCode.INVALID_USER_ID, "Picklist unassigned");
        } else if (picklist.getPicker() != null && !picklist.getPicker().getId().equals(request.getUserId())) {
            context.addError(WsResponseCode.INVALID_USER_ID, "Picklist is assigned to different user");
        } else if (!PickBatch.StatusCode.PICKING.equals((pickBatch = getPickBatchById(pickBatch.getId())).getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "PickBatch not in PICKING state for this pick bucket");
        } else {
            PickSet pickSet = picksetService.getPickSetByName(picklist.getPickSetName());
            if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                if (!CollectionUtils.isEmpty(request.getItemCodes())) {
                    for (String itemCode : request.getItemCodes()) {
                        Item item = inventoryService.getItemByCode(itemCode);
                        if (item == null) {
                            context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item Code : " + itemCode);
                        } else {
                            validateItemInPickBucketSubmission(item, picklist, context);
                        }
                        if (!context.hasErrors()) {
                            PicklistItem picklistItem = null;
                            // Marking item as picked only in case of STOCKING as Item already picked when STAGED
                            if (PickSet.Type.STOCKING.equals(pickSet.getType())) {
                                picklistItem = pickAndAllocateItemToPicklistItem(new ValidationContext(), item, picklist, null);
                            } else if (PickSet.Type.STAGING.equals(pickSet.getType())) {
                                picklistItem = getSoftAllocatedPicklistItemByItemCode(item.getCode(), picklist);
                            }
                            if (picklistItem != null) {
                                picklistItem.setPickBatch(pickBatch);
                                if (PicklistItem.StatusCode.CREATED.equals(picklistItem.getStatusCode())) {
                                    picklistItem.setStatusCode(PicklistItem.StatusCode.SUBMITTED);
                                    //                            ActivityLogger.forEvent(Event.ITEM_SUBMITTED).withIdentifier(itemCode).add(picklist.getCode()).add(pickBatch.getPickBucket().getCode()).log();
                                }
                                pickBatch.getPicklistItems().add(picklistItem);
                            }
                        } else {
                            // transaction will be rolled back anyways
                            break;
                        }
                    }
                }
            } else {
                if (!CollectionUtils.isEmpty(request.getPickedItems())) {
                    PickBatch finalPickBatch = pickBatch;
                    request.getPickedItems().stream().filter(pickedItem -> !context.hasErrors()).forEach(pickedItem -> {
                        List<PicklistItem> picklistItems = getPicklistItemsForSoftAllocationBySkuCode(pickedItem.getSkuCode(), pickedItem.getQuantity(), picklist.getId());
                        if (picklistItems.size() == pickedItem.getQuantity()) {
                            picklistItems.forEach(picklistItem -> {
                                picklistItem.setPickBatch(finalPickBatch);
                                if (PicklistItem.StatusCode.CREATED.equals(picklistItem.getStatusCode())) {
                                    picklistItem.setStatusCode(PicklistItem.StatusCode.SUBMITTED);
                                }
                                //                            ActivityLogger.forEvent(Event.ITEM_SUBMITTED).withIdentifier(itemCode).add(picklist.getCode()).add(pickBatch.getPickBucket().getCode()).log();
                                finalPickBatch.getPicklistItems().add(picklistItem);
                            });
                        } else {
                            context.addError(WsResponseCode.INVALID_REQUEST, "Extra quantity scanned for skuCode : " + pickedItem.getSkuCode());
                        }
                    });
                }
            }

            if (!context.hasErrors()) {
                if (CollectionUtils.isEmpty(request.getItemCodes()) && CollectionUtils.isEmpty(request.getPickedItems())) {
                    // if not items submitted, free the bucket
                    pickBatch.getPickBucket().setStatusCode(PickBucket.StatusCode.AVAILABLE);
                    pickBatch.setStatusCode(PickBatch.StatusCode.CLOSED);
                } else {
                    pickBatch.setStatusCode(PickBatch.StatusCode.SUBMITTED);
                }
            }
            response.setPicklistCode(pickBatch.getPicklist().getCode());
        }
    }

    private List<PicklistItem> getPicklistItemsForSoftAllocationBySkuCode(String skuCode, Integer quantity, Integer picklistId) {
        return getPicklistItemsForSoftAllocationBySkuCode(skuCode, quantity, picklistId, Arrays.asList(PicklistItem.StatusCode.CREATED, PicklistItem.StatusCode.PUTBACK_PENDING), true);
    }

    @Override
    public PicklistItem getPicklistItemForSoftAllocationBySkuCode(String skuCode, Integer picklistId) {
        List<PicklistItem> picklistItems = getPicklistItemsForSoftAllocationBySkuCode(skuCode, 1, picklistId,
                Arrays.asList(PicklistItem.StatusCode.CREATED, PicklistItem.StatusCode.SUBMITTED, PicklistItem.StatusCode.RECEIVED), false);
        return !CollectionUtils.isEmpty(picklistItems) ? picklistItems.get(0) : null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PicklistItem> getPicklistItemsForSoftAllocationBySkuCode(String skuCode, Integer quantity, Integer picklistId, List<PicklistItem.StatusCode> statusCodes,
            boolean checkPickBatch) {
        return pickerDao.getPicklistItemsForSoftAllocationBySkuCode(skuCode, quantity, picklistId, statusCodes, checkPickBatch);
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getSoftAllocatedPicklistItemByItemCode(String itemCode, Picklist picklist) {
        return pickerDao.getSoftAllocatedPicklistItemByItemCode(itemCode, picklist.getId());
    }

    /**
     * Will be invoked only in handled picking enabled facilities. If picklist was created from STOCKING zone, then item
     * should be {@link Item.StatusCode#GOOD_INVENTORY}, else {@link Item.StatusCode#PICKED} (marked at STAGING).
     *
     * @param item - item which is scanned
     * @param picklist - picklist in which the item came
     * @param context - validation context
     */
    private void validateItemInPickBucketSubmission(Item item, Picklist picklist, ValidationContext context) {
        PickSet.Type fromPickSetType = picksetService.getPickSetByName(picklist.getPickSetName()).getType();
        if (fromPickSetType.equals(PickSet.Type.STOCKING) && !item.isGoodInventory()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
        } else if (fromPickSetType.equals(PickSet.Type.STAGING) && !item.isPicked()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getPicklistItemByItemCodeInPicklist(String itemCode, int picklistId) {
        return pickerDao.getPicklistItemByItemCodeInPicklist(itemCode, picklistId);
    }

    @Override
    @Transactional(readOnly = true)
    public PickBatch getOpenPickBatchByPickBucketCode(String pickBucketCode, boolean fetchItems) {
        return pickerDao.getOpenPickBatchByPickBucket(pickBucketCode, fetchItems);
    }

    @Timed
    @Transactional(readOnly = true)
    @Override
    public String preparePicklistPreviewHtml(List<String> shippingPackageCodes) {
        List<ShippingPackage> shippingPackages = new ArrayList<ShippingPackage>();
        for (String shippingPackageCode : shippingPackageCodes) {
            ShippingPackage shippingPackage = shippingService.getShippingPackageDetailedByCode(shippingPackageCode);
            if (shippingPackage != null) {
                shippingPackages.add(shippingPackage);
            }
        }
        if (shippingPackages.size() > 0) {
            Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PICKLIST_PREVIEW.name());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("shippingPackages", shippingPackages);
            return template.evaluate(params);
        } else {
            return null;
        }
    }

    @Override
    @Timed
    @Transactional(readOnly = true)
    public String preparePicklistHtml(List<String> picklistCodes) {
        List<Picklist> picklists = new ArrayList<Picklist>();
        for (String picklistCode : picklistCodes) {
            picklists.add(pickerDao.getPicklistByCode(picklistCode));
        }
        Template template = CacheManager.getInstance().getCache(PrintTemplateCache.class).getTemplateByType(PrintTemplateVO.Type.PICKLIST.name());
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("picklists", picklists);
        addPicklistTemplateParams(params, picklists.get(0));
        return template.evaluate(params);
    }

    private void addPicklistTemplateParams(Map<String, Object> params, Picklist picklist) {
        PicklistDTO picklistDTO = getPicklistDTO(picklist);
        if (picklist != null) {
            Map<String, List<PicklistItemDTO>> statusCodeToPicklistItem = new HashMap<String, List<PicklistItemDTO>>();
            for (PicklistItemDTO picklistItem : picklistDTO.getPicklistItems()) {
                if (StringUtils.equalsAny(picklistItem.getStatusCode(), PicklistItem.StatusCode.INVENTORY_NOT_FOUND.name(), PicklistItem.StatusCode.PUTBACK_COMPLETE.name())) {
                    List<PicklistItemDTO> picklistItems = statusCodeToPicklistItem.get(picklistItem.getStatusCode());
                    if (picklistItems == null) {
                        picklistItems = new ArrayList<PicklistItemDTO>();
                    }
                    picklistItems.add(picklistItem);
                    statusCodeToPicklistItem.put(picklistItem.getStatusCode(), picklistItems);
                } else {
                    List<PicklistItemDTO> picklistItems = statusCodeToPicklistItem.get("PICK_ITEMS");
                    if (picklistItems == null) {
                        picklistItems = new ArrayList<PicklistItemDTO>();
                    }
                    picklistItems.add(picklistItem);
                    statusCodeToPicklistItem.put("PICK_ITEMS", picklistItems);
                }
            }
            params.put("picklist", picklist);
            params.put("statusCodeToPicklistItem", statusCodeToPicklistItem);
            params.put("totalPicklistItemCount", picklistDTO.getPicklistItems().size());
        }
    }

    @Override
    @Timed
    @Transactional
    public List<PicklistStatus> getPicklistStatuses() {
        return pickerDao.getPicklistStatuses();
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isPicklistComplete(Picklist picklist) {
        for (PicklistItem picklistItem : picklist.getPicklistItems()) {
            if (!picklistItem.isAssessed() || (Picklist.Destination.INVOICING.equals(picklist.getDestination())
                    && StringUtils.equalsAny(shippingService.getShippingPackageByCode(picklistItem.getShippingPackageCode()).getStatusCode(),
                            ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(), ShippingPackage.StatusCode.PICKED.name(),
                            ShippingPackage.StatusCode.PENDING_CUSTOMIZATION.name(), ShippingPackage.StatusCode.CUSTOMIZATION_COMPLETE.name()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Picklist> getOpenPicklists() {
        return pickerDao.getOpenPicklists();
    }

    @Override
    @Transactional
    public PicklistItem getPicklistItemForPutbackByItemCode(String itemCode) {
        return pickerDao.getPicklistItemForPutbackByItemCode(itemCode);
    }

    @Override
    @Transactional(readOnly = true)
    public Picklist getPicklistByCode(String picklistCode, boolean refresh, boolean fetchItems) {
        return pickerDao.getPicklistByCode(picklistCode, refresh, fetchItems);
    }

    @Override
    @Transactional(readOnly = true)
    public Picklist getPicklistByCode(String picklistCode) {
        return pickerDao.getPicklistByCode(picklistCode);
    }

    @Override
    @Transactional
    @LogActivity
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public AssignPicklistToUserResponse assignPicklistToUser(AssignPicklistToUserRequest request) {
        AssignPicklistToUserResponse response = new AssignPicklistToUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code : " + request.getPicklistCode());
            } else if (!Picklist.StatusCode.CREATED.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Picklist State : " + picklist.getStatusCode());
            } else if (picklist.getPicker() != null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist is already assigned to : " + picklist.getPicker().getUsername());
            } else {
                User picker = usersService.getUserByUsername(request.getUsername());
                if (picker == null) {
                    context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid User : " + request.getUsername());
                } else {
                    picklist.setPicker(picker);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Override
    @Transactional
    @LogActivity
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    @RollbackOnFailure
    public CompletePickingResponse completePicking(CompletePickingRequest request) {
        CompletePickingResponse response = new CompletePickingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid picklist code");
            } else if (!picklist.isAllowedForCompletePicking()) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid picklist state: " + picklist.getStatusCode());
            } else {
                // sale order to sale order items
                Map<String, Set<String>> notFoundSaleOrderItems = new HashMap<>();
                for (PicklistItem picklistItem : picklist.getPicklistItems()) {
                    if (request.getStatusCodes().size() == 0) {
                        if (isPendingActionable(picklistItem)) {
                            notFoundSaleOrderItems.computeIfAbsent(picklistItem.getSaleOrderCode(), k -> new HashSet<>()).add(picklistItem.getSaleOrderItemCode());
                        }
                    } else {
                        if (StringUtils.equalsAny(picklistItem.getStatusCode(), request.getStatusCodes().toArray())) {
                            notFoundSaleOrderItems.computeIfAbsent(picklistItem.getSaleOrderCode(), k -> new HashSet<>()).add(picklistItem.getSaleOrderItemCode());
                        }
                    }
                }
                if (!context.hasErrors() && notFoundSaleOrderItems.size() > 0) {
                    MarkPicklistItemsNotFoundRequest markPicklistItemsNotFoundRequest = new MarkPicklistItemsNotFoundRequest();
                    markPicklistItemsNotFoundRequest.setPicklistCode(picklist.getCode());
                    markPicklistItemsNotFoundRequest.setUserId(request.getUserId());
                    List<EditPicklistRequest.WsEditPicklistItem> missingPicklistItems = new ArrayList<>(notFoundSaleOrderItems.size());
                    for (Map.Entry<String, Set<String>> notFoundSaleOrderEntry : notFoundSaleOrderItems.entrySet()) {
                        for (String saleOrderItemCode : notFoundSaleOrderEntry.getValue()) {
                            EditPicklistRequest.WsEditPicklistItem missingPicklistItem = new EditPicklistRequest.WsEditPicklistItem();
                            missingPicklistItem.setSaleOrderCode(notFoundSaleOrderEntry.getKey());
                            missingPicklistItem.setSaleOrderItemCode(saleOrderItemCode);
                            missingPicklistItems.add(missingPicklistItem);
                        }
                    }
                    markPicklistItemsNotFoundRequest.setMissingPicklistItems(missingPicklistItems);
                    MarkPicklistItemsNotFoundResponse markPicklistItemsNotFoundResponse = packerService.markPicklistItemsNotFound(markPicklistItemsNotFoundRequest);
                    if (!markPicklistItemsNotFoundResponse.isSuccessful()) {
                        context.addErrors(markPicklistItemsNotFoundResponse.getErrors());
                    }
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    // DEFAULT pick bucket check is to ensure that pick-batch is created from app and item is already submitted through app.
    private boolean isPendingActionable(PicklistItem picklistItem) {
        return PicklistItem.StatusCode.CREATED.equals(picklistItem.getStatusCode()) || (PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())
                && (picklistItem.getPickBatch() == null || Constants.DEFAULT.equals(picklistItem.getPickBatch().getPickBucket().getCode())));
    }

    @Override
    @Transactional(readOnly = true)
    public PicklistItem getPicklistItemBySaleOrderItemCode(Integer picklistId, String saleOrderItemCode, String saleOrderCode) {
        return pickerDao.getPicklistItemBySaleOrderItemCode(picklistId, saleOrderItemCode, saleOrderCode);
    }

    @Override
    @Transactional(readOnly = true)
    public GetPicklistDetailResponse getPicklistDetail(GetPicklistDetailRequest request) {
        GetPicklistDetailResponse response = new GetPicklistDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code : " + request.getPicklistCode());
            } else {
                PickSet pickSet = picksetService.getPickSetByName(picklist.getPickSetName());
                Map<String, Map<String, GetPicklistDetailResponse.ItemTypeShelfQuantity>> shelfToSkuCodeToSlotItems = new TreeMap<>();
                Map<String, Integer> pickBucketScannedQuantity = new HashMap<>();
                Set<String> alreadySoftAllocatedItemCodes = new HashSet<>();
                for (PicklistItem picklistItem : picklist.getPicklistItems()) {
                    if (picklistItem.getPickBatch() != null) {
                        alreadySoftAllocatedItemCodes.add(picklistItem.getItemCode());
                    }
                    if (PicklistItem.StatusCode.CREATED.equals(picklistItem.getStatusCode()) || (PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())) && picklistItem.getPickBatch() == null) {
                        SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
                        if (StringUtils.isBlank(picklistItem.getItemCode()) && PickSet.Type.STOCKING.equals(pickSet.getType())) {
                            Shelf shelf = saleOrderItem.getItemTypeInventory().getShelf();
                            shelfToSkuCodeToSlotItems.computeIfAbsent(shelf.getCode(), k -> new HashMap<>()).computeIfAbsent(saleOrderItem.getItemType().getSkuCode(),
                                    k -> createItemTypeShelfQuantity(shelf.getCode(), saleOrderItem.getItemType().getId(), pickSet.getType())).incrementQuantity();
                        } else if (PickSet.Type.STAGING.equals(pickSet.getType())) {
                            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(picklistItem.getShippingPackageCode());
                            Shelf shelf = shelfService.getShelfByCode(shippingPackage.getShelfCode());
                            shelfToSkuCodeToSlotItems.computeIfAbsent(shelf.getCode(), k -> new HashMap<>()).computeIfAbsent(saleOrderItem.getItemType().getSkuCode(),
                                    k -> createItemTypeShelfQuantity(shelf.getCode(), saleOrderItem.getItemType().getId(), pickSet.getType())).incrementQuantity();
                            shelfToSkuCodeToSlotItems.get(shelf.getCode()).get(saleOrderItem.getItemType().getSkuCode()).getValidItemCodes().add(picklistItem.getItemCode());
                        }
                    }
                }
                for (PickBatch pickBatch : picklist.getPickBatches()) {
                    pickBucketScannedQuantity.compute(pickBatch.getPickBucket().getCode(),
                            (k, v) -> v == null ? pickBatch.getPicklistItems().size() : v + pickBatch.getPicklistItems().size());
                }
                if (PickSet.Type.STOCKING.equals(pickSet.getType())) {
                    shelfToSkuCodeToSlotItems.forEach((k, v) -> v.forEach((a, b) -> b.getValidItemCodes().removeAll(alreadySoftAllocatedItemCodes)));
                }
                if (!context.hasErrors()) {
                    response.setPickInstructions(shelfToSkuCodeToSlotItems);
                    response.setPickSetName(picklist.getPickSetName());
                    response.setPickSetType(pickSet.getType());
                    response.setStatusCode(picklist.getStatusCode());
                    response.setPickBucketScannedQuantity(pickBucketScannedQuantity);
                    if (picklist.getPicker() != null) {
                        response.setPicker(picklist.getPicker().getUsername());
                    }
                }
            }
        }

        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private GetPicklistDetailResponse.ItemTypeShelfQuantity createItemTypeShelfQuantity(String shelfCode, Integer itemTypeId, PickSet.Type pickSetType) {
        GetPicklistDetailResponse.ItemTypeShelfQuantity itemTypeShelfQuantity = new GetPicklistDetailResponse.ItemTypeShelfQuantity();
        ItemType itemType = catalogService.getEnabledItemTypeById(itemTypeId);
        itemTypeShelfQuantity.setProductName(itemType.getName());
        SystemConfiguration.InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(
                SystemConfiguration.class).getInventoryAccuracyLevel();
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel) && inventoryAccuracyLevel == SystemConfiguration.InventoryAccuracyLevel.SHELF
                && PickSet.Type.STOCKING.equals(pickSetType)) {
            Shelf shelf = shelfService.getShelfByCode(shelfCode);
            List<Item> items = inventoryService.getItemsByItemTypeAndShelfAndStatus(itemTypeId, shelf.getId(), Item.StatusCode.GOOD_INVENTORY.name());
            itemTypeShelfQuantity.getValidItemCodes().addAll(items.stream().filter(item -> !(catalogService.isItemTypeExpirable(itemType)
                    && itemService.isItemAboutToExpireForThisTolerance(item, itemType, itemType.getCategory().getDispatchExpiryTolerance()))).map(Item::getCode).collect(
                            Collectors.toList()));
        }
        return itemTypeShelfQuantity;
    }

    @Override
    @Transactional
    public CreatePickBucketResponse createPickBucket(CreatePickBucketRequest request) {
        CreatePickBucketResponse response = new CreatePickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = getPickBucketByCode(request.getCode());
            if (pickBucket != null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Pick Bucket " + request.getCode() + " already exist");
            } else {
                pickBucket = new PickBucket();
                pickBucket.setCode(request.getCode());
                pickBucket.setLength(request.getLength());
                pickBucket.setWidth(request.getWidth());
                pickBucket.setHeight(request.getHeight());
                pickBucket = pickerDao.addPickBucket(pickBucket);
                response.setCode(pickBucket.getCode());
                response.setSuccessful(true);
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public UpdatePickBucketResponse updatePickBucket(UpdatePickBucketRequest request) {
        UpdatePickBucketResponse response = new UpdatePickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = getPickBucketByCode(request.getCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Pick Bucket " + request.getCode() + " doesn't exist");
            } else if (PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Pick Bucket " + request.getCode() + " is currently in use");
            } else {
                if (request.getLength() != null) {
                    pickBucket.setLength(request.getLength());
                }
                if (request.getWidth() != null) {
                    pickBucket.setWidth(request.getWidth());
                }
                if (request.getHeight() != null) {
                    pickBucket.setHeight(request.getHeight());
                }
                response.setSuccessful(true);
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public CreateBulkPickBucketResponse createBulkPickBuckets(CreateBulkPickBucketRequest request) {
        CreateBulkPickBucketResponse response = new CreateBulkPickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (int i = 0; i < request.getNoOfBucket(); i++) {
                PickBucket pickBucket = new PickBucket();
                pickBucket = pickerDao.addPickBucket(pickBucket);
                response.getPickBucketCodes().add(pickBucket.getCode());
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public EnablePickBucketResponse enablePickBucket(EnablePickBucketRequest request) {
        EnablePickBucketResponse response = new EnablePickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String pickBucketCode : request.getPickBucketCodes()) {
                PickBucket pickBucket = getPickBucketByCode(pickBucketCode);
                if (pickBucket == null) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Bucket Code : " + pickBucketCode);
                } else if (PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Pick bucket in use, code : " + pickBucketCode);
                } else {
                    pickBucket.setStatusCode(PickBucket.StatusCode.AVAILABLE);
                }
                //partial enabling is successful
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DisablePickBucketResponse disablePickBucket(DisablePickBucketRequest request) {
        DisablePickBucketResponse response = new DisablePickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            for (String pickBucketCode : request.getPickBucketCodes()) {
                PickBucket pickBucket = getPickBucketByCode(pickBucketCode);
                if (pickBucket == null) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Bucket Code : " + pickBucketCode);
                } else if (PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Pick bucket in use, code : " + pickBucketCode);
                } else {
                    pickBucket.setStatusCode(PickBucket.StatusCode.DISABLED);
                }
                //partial disabling is successful
                response.setSuccessful(true);
            }
        }
        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetPutbackAcceptedItemsForPicklistResponse getPutbackAcceptedItemsForPicklist(GetPutbackAcceptedItemsForPicklistRequest request) {
        GetPutbackAcceptedItemsForPicklistResponse response = new GetPutbackAcceptedItemsForPicklistResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerDao.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid picklist code");
            } else if (!picklist.isComplete()) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Picklist not closed");
            } else {
                response.setStatusCode(picklist.getStatusCode());
                response.setPicklistCode(picklist.getCode());
                response.setDestination(picklist.getDestination().name());
                List<PutbackItemDetailDTO> putbackItemDetailDTOList = new ArrayList<>();
                picklist.getPicklistItems().stream().filter(picklistItem -> PicklistItem.StatusCode.PUTBACK_ACCEPTED.equals(picklistItem.getStatusCode())).forEach(picklistItem -> {
                    putbackItemDetailDTOList.add(preparePutbackItemDetailDTO(picklistItem));
                });
                response.setPutbackItems(putbackItemDetailDTOList);
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    private PutbackItemDetailDTO preparePutbackItemDetailDTO(PicklistItem picklistItem) {
        PutbackItemDetailDTO putbackItemDetailDTO = new PutbackItemDetailDTO();
        SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
        ItemType itemType = saleOrderItem.getItemType();
        putbackItemDetailDTO.setPicklistItemId(picklistItem.getId());
        putbackItemDetailDTO.setSaleOrderCode(picklistItem.getSaleOrderCode());
        putbackItemDetailDTO.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
        putbackItemDetailDTO.setShippingPackageCode(picklistItem.getShippingPackageCode());
        putbackItemDetailDTO.setItemCode(picklistItem.getItemCode());
        putbackItemDetailDTO.setItemTypeName(itemType.getName());
        putbackItemDetailDTO.setSkuCode(itemType.getSkuCode());
        putbackItemDetailDTO.setInventoryType(
                PicklistItem.QCStatus.REJECTED.equals(picklistItem.getQcStatusCode()) ? ItemTypeInventory.Type.BAD_INVENTORY.name() : ItemTypeInventory.Type.GOOD_INVENTORY.name());
        return putbackItemDetailDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PickBucket> getPickBuckets(List<String> pickBucketCodes) {
        return pickerDao.getPickBuckets(pickBucketCodes);
    }

    @Override
    @Transactional(readOnly = true)
    public GetPutbackItemCountBySkuResponse getPutbackItemCountBySku(GetPutbackItemCountBySkuRequest request) {
        GetPutbackItemCountBySkuResponse response = new GetPutbackItemCountBySkuResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ItemType itemType = catalogService.getItemTypeBySkuCode(request.getSkuCode());
            if (itemType == null) {
                context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Invalid Sku Code");
            } else {
                List<PicklistItem> picklistItems = getPutbackAcceptedItemsBySkuCodeAndQCStatus(request.getSkuCode(), null, null);
                Map<ItemTypeInventory.Type, Integer> typeToQuantity = new HashMap<>();
                List<GetPutbackItemCountBySkuResponse.PutbackItemCountDTO> putbackItems = new ArrayList<>();
                picklistItems.forEach(picklistItem -> {
                    ItemTypeInventory.Type type = null;
                    if (PicklistItem.QCStatus.REJECTED.equals(picklistItem.getQcStatusCode())) {
                        type = ItemTypeInventory.Type.BAD_INVENTORY;
                    } else {
                        type = ItemTypeInventory.Type.GOOD_INVENTORY;
                    }
                    typeToQuantity.compute(type, (k, v) -> v == null ? 1 : ++v);
                });
                typeToQuantity.forEach(((type, quantity) -> putbackItems.add(new GetPutbackItemCountBySkuResponse.PutbackItemCountDTO(type, quantity))));
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public List<PicklistItem> getPutbackAcceptedItemsBySkuCodeAndQCStatus(String skuCode, PicklistItem.QCStatus qcStatus, Integer quantity) {
        return pickerDao.getPutbackAcceptedItemsBySkuCodeAndQCStatus(skuCode, qcStatus, quantity);
    }
}