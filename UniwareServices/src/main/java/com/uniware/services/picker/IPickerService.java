/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 14, 2011
 *  @author singla
 */
package com.uniware.services.picker;

import com.uniware.core.api.picker.CreateAutoPicklistRequest;
import java.util.List;

import com.uniware.core.api.picker.CreateBulkPickBucketRequest;
import com.uniware.core.api.picker.CreateBulkPickBucketResponse;
import com.uniware.core.api.picker.CreatePickBucketRequest;
import com.uniware.core.api.picker.CreatePickBucketResponse;
import com.uniware.core.api.picker.DisablePickBucketRequest;
import com.uniware.core.api.picker.DisablePickBucketResponse;
import com.uniware.core.api.picker.EnablePickBucketRequest;
import com.uniware.core.api.picker.EnablePickBucketResponse;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsRequest;
import com.uniware.core.api.picker.GetPickSetWiseItemCountsResponse;
import com.uniware.core.api.picker.GetPicklistDetailRequest;
import com.uniware.core.api.picker.GetPicklistDetailResponse;
import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistRequest;
import com.uniware.core.api.picker.GetPutbackAcceptedItemsForPicklistResponse;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuRequest;
import com.uniware.core.api.picker.GetPutbackItemCountBySkuResponse;
import com.uniware.core.api.picker.UpdatePickBucketRequest;
import com.uniware.core.api.picker.UpdatePickBucketResponse;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.uniware.core.api.picker.AssignPicklistToUserRequest;
import com.uniware.core.api.picker.AssignPicklistToUserResponse;
import com.uniware.core.api.picker.CompletePickingRequest;
import com.uniware.core.api.picker.CompletePickingResponse;
import com.uniware.core.api.picker.CreateManualPicklistRequest;
import com.uniware.core.api.picker.CreatePickBatchRequest;
import com.uniware.core.api.picker.CreatePickBatchResponse;
import com.uniware.core.api.picker.CreatePicklistResponse;
import com.uniware.core.api.picker.GetPicklistRequest;
import com.uniware.core.api.picker.GetPicklistResponse;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountRequest;
import com.uniware.core.api.picker.GetPicksetsWithPackageCountResponse;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.api.picker.SearchPicklistResponse;
import com.uniware.core.api.picker.SubmitPickBucketRequest;
import com.uniware.core.api.picker.SubmitPickBucketResponse;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.PicklistStatus;
import com.uniware.core.entity.SaleOrderItem;

/**
 * @author singla
 */
public interface IPickerService {
    CreatePicklistResponse createManualPicklist(CreateManualPicklistRequest request);

    CreatePicklistResponse createAutoPicklist(CreateAutoPicklistRequest request);

    /**
     * @param request
     * @return
     */
    GetPicklistResponse getPicklist(GetPicklistRequest request);

    /* (non-Javadoc)
     * @see com.uniware.services.picker.IPickerService#handlePicklistItemCancellation(int)
     */
    @Transactional
    boolean handlePicklistItemCancellation(SaleOrderItem saleOrderItem);

    @Transactional(readOnly = true)
    PicklistItem getPicklistItemBySaleOrderItemCodeAndShippingPackageCodeAndDestination(String saleOrderItemCode, String SaleOrderCode, String shippingPackageCode,
            Picklist.Destination destination);

    SearchPicklistResponse searchPicklist(SearchPicklistRequest request);

    /**
     * @param pickSetId
     */
    void replanPicking(int pickSetId);

    /**
     * @param shippingPackageCode
     * @return
     */
    Picklist getPicklistForShippingPackage(String shippingPackageCode);

    List<PicklistItem> getPicklistItemsForSoftAllocationBySkuCode(String skuCode, Integer quantity, Integer picklistId,
            List<PicklistItem.StatusCode> statusCodes, boolean checkPickBatch);

    PicklistItem getSoftAllocatedPicklistItemByItemCode(String itemCode, Picklist picklist);

    PicklistItem getPicklistItemByItemCodeInPicklist(String itemCode, int picklistId);

    PickBatch getOpenPickBatchByPickBucketCode(String pickBucketCode, boolean fetchItems);

    String preparePicklistPreviewHtml(List<String> shippingPackageCodes);

    String preparePicklistHtml(List<String> picklistCodes);

    List<PicklistStatus> getPicklistStatuses();

    List<PicklistItem> getPicklistItemsByShippingPackageCode(String shippingPackageCode, Picklist.Destination destination);

    GetPicksetsWithPackageCountResponse getPicksetsWithPackageCount(GetPicksetsWithPackageCountRequest request);

    GetPickSetWiseItemCountsResponse getPickSetWisePackageCounts(GetPickSetWiseItemCountsRequest request);

    PickBucket getPickBucketByCode(String pickBucketCode);

    PickBatch getOpenPickBatchByPickBucketCode(String code);

    PickBatch getPickBatchById(Integer id);

    Picklist getPicklistByCode(String picklistCode);

    Picklist getPicklistByCode(String picklistCode, boolean refresh, boolean fetchItems);

    PicklistItem getOpenPicklistItemByItemCode(String itemCode);

    PicklistItem getPicklistItemBySaleOrderItemCode(Integer picklistId, String saleOrderItemCode, String saleOrderCode);

    PicklistItem pickAndAllocateItemToPicklistItem(ValidationContext context, Item item, Picklist picklist, SaleOrderItem saleOrderItem);

    PicklistItem getPicklistItemById(Integer id);

    PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemCode, PickBatch pickBatch);

    PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemOrSkuCode, PickBatch pickBatch,
            PicklistItem.QCStatus qcStatusCode);

    void updatePicklistItem(PicklistItem picklistItem);

    SubmitPickBucketResponse submitPickBucket(SubmitPickBucketRequest request);

    CreatePickBatchResponse createPickBatch(CreatePickBatchRequest createPickBatchRequest);

    boolean isPicklistComplete(Picklist picklist);

    List<Picklist> getOpenPicklists();

    PicklistItem getPicklistItemForPutbackByItemCode(String itemCode);

    AssignPicklistToUserResponse assignPicklistToUser(AssignPicklistToUserRequest request);

    CompletePickingResponse completePicking(CompletePickingRequest request);

    GetPicklistDetailResponse getPicklistDetail(GetPicklistDetailRequest request);

    CreatePickBucketResponse createPickBucket(CreatePickBucketRequest request);

    UpdatePickBucketResponse updatePickBucket(UpdatePickBucketRequest request);

    CreateBulkPickBucketResponse createBulkPickBuckets(CreateBulkPickBucketRequest request);

    EnablePickBucketResponse enablePickBucket(EnablePickBucketRequest request);

    DisablePickBucketResponse disablePickBucket(DisablePickBucketRequest request);

    GetPutbackAcceptedItemsForPicklistResponse getPutbackAcceptedItemsForPicklist(GetPutbackAcceptedItemsForPicklistRequest request);

    List<PickBucket> getPickBuckets(List<String> pickBucketCodes);

    GetPutbackItemCountBySkuResponse getPutbackItemCountBySku(GetPutbackItemCountBySkuRequest request);

    List<PicklistItem> getPutbackAcceptedItemsBySkuCodeAndQCStatus(String skuCode, PicklistItem.QCStatus qcStatus, Integer quantity);

    PicklistItem getPicklistItemForSoftAllocationBySkuCode(String skuCode, Integer picklistId);
}
