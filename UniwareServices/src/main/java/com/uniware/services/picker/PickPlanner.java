/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 4, 2012
 *  @author singla
 */
package com.uniware.services.picker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.ShippingPackage;

/**
 * @author singla
 */
public class PickPlanner implements Serializable {

    private final List<PriorityWisePickPlanner>         priorityWisePickPlanners = new ArrayList<PriorityWisePickPlanner>();
    private final Map<Integer, PriorityWisePickPlanner> priorityToPickPlanners   = new HashMap<Integer, PickPlanner.PriorityWisePickPlanner>();
    private int                                         shippingPackagesCount;
    private int                                         plannedItemsCount;

    public PickPlanner(List<ShippingPackageDTO> shippingPackages) {
        shippingPackagesCount = shippingPackages.size();
        for (ShippingPackageDTO shippingPackage : shippingPackages) {
            PriorityWisePickPlanner pickPlanner = priorityToPickPlanners.get(shippingPackage.getPriority());
            if (pickPlanner == null) {
                pickPlanner = new PriorityWisePickPlanner(shippingPackage.getPriority());
                priorityWisePickPlanners.add(pickPlanner);
                priorityToPickPlanners.put(pickPlanner.getPriority(), pickPlanner);
            }
            pickPlanner.addShippingPackage(shippingPackage);
            plannedItemsCount += shippingPackage.getSaleOrderItemsCount();
        }

        Collections.sort(priorityWisePickPlanners, new Comparator<PriorityWisePickPlanner>() {
            @Override
            public int compare(PriorityWisePickPlanner arg0, PriorityWisePickPlanner arg1) {
                return arg0.getPriority() > arg1.getPriority() ? -1 : 1;
            }
        });
    }

    /**
     * @return
     */
    public int getPlannedItemsCount() {
        return plannedItemsCount;
    }

    public void reset() {
        for (PriorityWisePickPlanner pickPlanner : priorityWisePickPlanners) {
            pickPlanner.reset();
        }
        shippingPackagesCount = 0;
    }

    public List<ShippingPackageDTO> getPicklist(int noOfPackages) {
        List<ShippingPackageDTO> picklist = new ArrayList<ShippingPackageDTO>();
        Iterator<PriorityWisePickPlanner> iterator = priorityWisePickPlanners.iterator();
        while (iterator.hasNext() && picklist.size() < noOfPackages) {
            picklist.addAll(iterator.next().getPicklist(noOfPackages - picklist.size()));
        }
        shippingPackagesCount -= picklist.size();
        plannedItemsCount -= picklist.stream().mapToInt(ShippingPackageDTO::getSaleOrderItemsCount).sum();
        return picklist;
    }

    private class PriorityWisePickPlanner implements Serializable {
        private final int                    priority;
        private final LinkedPickPlanningList shippingPackageList;

        public PriorityWisePickPlanner(int priority) {
            this.priority = priority;
            shippingPackageList = new LinkedPickPlanningList();
        }

        public void addShippingPackage(ShippingPackageDTO shippingPackageDTO) {
            shippingPackageList.add(shippingPackageDTO);
        }

        public void reset() {
            shippingPackageList.clear();
        }

        public int getPriority() {
            return this.priority;
        }

        public List<ShippingPackageDTO> getPicklist(int noOfPackages) {
            List<ShippingPackageDTO> picklist = new ArrayList<ShippingPackageDTO>();
            LinkedNode rootPackageNode;
            do {
                rootPackageNode = shippingPackageList.getFirst();
                if (rootPackageNode != null) {
                    picklist.add(rootPackageNode.getShippingPackage());
                    if (!rootPackageNode.getShippingPackage().isMultiSku()) {
                        addSingleItemTypePackages(rootPackageNode, noOfPackages, picklist);
                    } else {
                        String[] itemSKUs = rootPackageNode.getShippingPackage().getPickItems().split("\\|");
                        for (String itemSKU : itemSKUs) {
                            LinkedNode nextItemType = shippingPackageList.getFirstItemType(itemSKU);
                            if (nextItemType != null) {
                                addSingleItemTypePackages(nextItemType, noOfPackages, picklist);
                            }
                        }
                        LinkedNode nextNode = shippingPackageList.getNext(rootPackageNode);
                        while (nextNode != null && picklist.size() < noOfPackages) {
                            if (isSameSection(rootPackageNode.getShippingPackage(), nextNode.getShippingPackage())) {
                                picklist.add(nextNode.getShippingPackage());
                                if (!nextNode.getShippingPackage().isMultiSku()) {
                                    addSingleItemTypePackages(nextNode, noOfPackages, picklist);
                                }
                                shippingPackageList.remove(nextNode);
                            }
                            nextNode = shippingPackageList.getNext(nextNode);
                        }
                    }
                    shippingPackageList.remove(rootPackageNode);
                }
            } while (rootPackageNode != null && picklist.size() < noOfPackages);
            return picklist;
        }

        private boolean isSameSection(ShippingPackageDTO rootPackage, ShippingPackageDTO shippingPackage) {
            return Pattern.matches("((" + rootPackage.getPickSections() + ")[|]?)+", shippingPackage.getPickSections());
        }

        private void addSingleItemTypePackages(LinkedNode node, int noOfPackages, List<ShippingPackageDTO> picklist) {
            LinkedNode nextItemTypeNode = shippingPackageList.getNextItemType(node);
            while (picklist.size() < noOfPackages && nextItemTypeNode != null) {
                picklist.add(nextItemTypeNode.getShippingPackage());
                shippingPackageList.remove(nextItemTypeNode);
                nextItemTypeNode = shippingPackageList.getNextItemType(nextItemTypeNode);
            }
            if (picklist.size() < noOfPackages) {
                LinkedNode nextSectionNode = shippingPackageList.getNextSection(node);
                if (nextSectionNode != null) {
                    picklist.add(nextSectionNode.getShippingPackage());
                    addSingleItemTypePackages(nextSectionNode, noOfPackages, picklist);
                    shippingPackageList.remove(nextSectionNode);
                }
            }
        }

    }

    private class LinkedPickPlanningList implements Serializable {

        private LinkedNode              header          = new LinkedNode(null, null, null, null, null, null, null);
        private Map<String, LinkedNode> itemTypeHeaders = new HashMap<String, PickPlanner.LinkedNode>();
        private Map<String, LinkedNode> sectionHeaders  = new HashMap<String, PickPlanner.LinkedNode>();

        /**
         * Constructs an empty list.
         */
        public LinkedPickPlanningList() {
            header.next = header.previous = header;
        }

        /**
         * @param node
         * @return
         */
        public LinkedNode getNextSection(LinkedNode node) {
            LinkedNode sectionHeader = sectionHeaders.get(node.getShippingPackage().getPickSections());
            return sectionHeader == null ? null : (node.nextSection == sectionHeader ? null : node.nextSection);
        }

        /**
         * @param node
         * @return
         */
        public LinkedNode getNextItemType(LinkedNode node) {
            LinkedNode itemTypeHeader = itemTypeHeaders.get(node.getShippingPackage().getPickItems());
            return itemTypeHeader == null ? null : (node.nextItemType == itemTypeHeader ? null : node.nextItemType);
        }

        public LinkedNode getFirstItemType(String itemSku) {
            LinkedNode itemTypeHeader = itemTypeHeaders.get(itemSku);
            return itemTypeHeader == null ? null : itemTypeHeader.nextItemType == itemTypeHeader ? null : itemTypeHeader.nextItemType;
        }

        /**
         * @param node
         * @return
         */
        public LinkedNode getNext(LinkedNode node) {
            return node.next == header ? null : node.next;
        }

        /**
         * @return
         */
        public LinkedNode getFirst() {
            return getNext(header);
        }

        public LinkedNode add(ShippingPackageDTO e) {
            LinkedNode itemTypeHeader = null, sectionHeader = null;
            if (!e.isMultiSku()) {
                itemTypeHeader = itemTypeHeaders.get(e.getPickItems());
                if (itemTypeHeader == null) {
                    itemTypeHeader = new LinkedNode(null, null, null, null, null, null, null);
                    itemTypeHeader.nextItemType = itemTypeHeader.previousItemType = itemTypeHeader;
                    itemTypeHeaders.put(e.getPickItems(), itemTypeHeader);
                }
                sectionHeader = sectionHeaders.get(e.getPickSections());
                if (sectionHeader == null) {
                    sectionHeader = new LinkedNode(null, null, null, null, null, null, null);
                    sectionHeader.nextSection = sectionHeader.previousSection = sectionHeader;
                    sectionHeaders.put(e.getPickSections(), sectionHeader);
                }
            }

            LinkedNode newNode = new LinkedNode(e, header, header.previous, itemTypeHeader, itemTypeHeader == null ? null : itemTypeHeader.previousItemType, sectionHeader,
                    sectionHeader == null ? null : sectionHeader.previousSection);
            newNode.previous.next = newNode;
            newNode.next.previous = newNode;
            if (itemTypeHeader != null) {
                newNode.previousItemType.nextItemType = newNode;
                newNode.nextItemType.previousItemType = newNode;
            }
            if (sectionHeader != null) {
                newNode.previousSection.nextSection = newNode;
                newNode.nextSection.previousSection = newNode;
            }
            return newNode;
        }

        public ShippingPackageDTO remove(LinkedNode e) {
            if (e == header)
                throw new NoSuchElementException();

            ShippingPackageDTO result = e.shippingPackage;
            e.previous.next = e.next;
            e.next.previous = e.previous;
            if (e.previousItemType != null) {
                e.previousItemType.nextItemType = e.nextItemType;
                e.nextItemType.previousItemType = e.previousItemType;
            }
            if (e.previousSection != null) {
                e.previousSection.nextSection = e.nextSection;
                e.nextSection.previousSection = e.previousSection;
            }
            return result;
        }

        public void clear() {
            LinkedNode e = header.next;
            while (e != header) {
                LinkedNode next = e.next;
                e.next = e.previous = e.nextItemType = e.previousItemType = e.nextSection = e.previousSection = null;
                e.shippingPackage = null;
                e = next;
            }
            itemTypeHeaders.clear();
            sectionHeaders.clear();
            header.next = header.previous = header;
        }

    }

    private class LinkedNode implements Serializable {
        ShippingPackageDTO shippingPackage;
        LinkedNode         next;
        LinkedNode         previous;
        LinkedNode         nextItemType;
        LinkedNode         previousItemType;
        LinkedNode         nextSection;
        LinkedNode         previousSection;

        private LinkedNode(ShippingPackageDTO shippingPackage, LinkedNode next, LinkedNode previous, LinkedNode nextItemType, LinkedNode previousItemType, LinkedNode nextSection,
                LinkedNode previousSection) {
            this.shippingPackage = shippingPackage;
            this.next = next;
            this.previous = previous;
            this.nextItemType = nextItemType;
            this.previousItemType = previousItemType;
            this.nextSection = nextSection;
            this.previousSection = previousSection;
        }

        public ShippingPackageDTO getShippingPackage() {
            return shippingPackage;
        }
    }

    public static class ShippingPackageDTO implements Serializable {

        public ShippingPackageDTO(String shippingPackageCode, String pickSections, String pickItems,int priority, int saleOrderItemsCount) {
            this.shippingPackageCode = shippingPackageCode;
            this.pickSections = pickSections;
            this.pickItems = pickItems;
            this.priority = priority;
            this.saleOrderItemsCount = saleOrderItemsCount;
        }

        private String shippingPackageCode;
        private String pickSections;
        private String pickItems;
        private int    priority;
        private int    saleOrderItemsCount;

        public String getShippingPackageCode() {
            return shippingPackageCode;
        }

        public void setShippingPackageCode(String shippingPackageCode) {
            this.shippingPackageCode = shippingPackageCode;
        }

        public String getPickSections() {
            return pickSections;
        }

        public void setPickSections(String pickSections) {
            this.pickSections = pickSections;
        }

        public String getPickItems() {
            return pickItems;
        }

        public void setPickItems(String pickItems) {
            this.pickItems = pickItems;
        }

        public int getSaleOrderItemsCount() {
            return saleOrderItemsCount;
        }

        public void setSaleOrderItemsCount(int saleOrderItemsCount) {
            this.saleOrderItemsCount = saleOrderItemsCount;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public boolean isMultiSku() {
            String pickItemTypes = StringUtils.getNotNullValue(pickItems);
            return pickItemTypes.split("\\|").length > 1;
        }
    }

}
