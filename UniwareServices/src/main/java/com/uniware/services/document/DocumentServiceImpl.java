/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-May-2013
 *  @author praveeng
 */
package com.uniware.services.document;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.unifier.core.cache.CacheManager;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;

import java.io.File;

@Service
public class DocumentServiceImpl implements IDocumentService {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentServiceImpl.class);

    @Override
    public String getFileUploadToken() {
        ScraperScript scraperScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName("GET_FILE_UPLOAD_TOKEN_SCRIPT");
        if (scraperScript == null) {
            throw new IllegalStateException("file upload token script is not configured");
        }
        try {
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServiceUsername());
            context.addVariable("password", CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServicePassword());
            context.addVariable("url", CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getDocumentServiceUrl());
            context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            scraperScript.execute();
            return context.getScriptOutput();
        } catch (Exception e) {
            LOG.error("unable to get file upload token" + e);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return null;
    }

    @Override
    public String uploadFile(File file, String bucketName) {
        try {
            AWSCredentials credentials = new BasicAWSCredentials("AKIAJUQKA3YXAY7TJF4Q", "e3P8O9S7GsXOV5kC21A7OaEnm1tCgws9FBDmgn8e");
            AmazonS3Client s3client = new AmazonS3Client(credentials);
            s3client.putObject(new PutObjectRequest(bucketName, file.getName(), file).withCannedAcl(CannedAccessControlList.PublicRead));
            return s3client.getResourceUrl(bucketName, file.getName());
        } catch (Throwable th) {
            LOG.error("error: ", th);
            throw new RuntimeException(th);
        }
    }
}
