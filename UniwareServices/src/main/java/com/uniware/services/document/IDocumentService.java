/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-May-2013
 *  @author praveeng
 */
package com.uniware.services.document;

import java.io.File;

public interface IDocumentService {

    String getFileUploadToken();

    String uploadFile(File file, String bucketName);
}
