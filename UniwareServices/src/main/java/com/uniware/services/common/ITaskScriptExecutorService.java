/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Aug-2013
 *  @author karunsingla
 */
package com.uniware.services.common;

import java.util.Map;

public interface ITaskScriptExecutorService {

    void executeScript(String scriptName, Map<String, Object> scriptParameters);

}
