/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 6, 2012
 *  @author singla
 */
package com.uniware.services.common;

import java.util.List;

import com.uniware.core.entity.Sequence;

public interface ISequenceGenerator {

    public String generateNext(Sequence.Name name);

    public void updateSequencePrefixes();

    void createSequence(Sequence sequence);

    String generateNext(Sequence.Name name, int randomSuffixLength);

    List<String> generateNextN(Sequence.Name name, int n, int randomSuffixLength);

    String generateNext(String sequenceName);

    public Sequence getSequenceByName(String retailInvoiceSequence, Integer facilityId);

    List<String> generateNextN(String sequenceName, int n);

    String generateNextInSameTransaction(String sequenceName);

    Sequence getSequenceByName(String name);

    Sequence updateSequence(String sequenceName, Integer facilityId, String newPrefix, int newValue, String nextYearPrefix, boolean resetCounterNextYear);
}
