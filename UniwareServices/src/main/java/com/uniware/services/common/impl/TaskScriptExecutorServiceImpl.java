/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Aug-2013
 *  @author karunsingla
 */
package com.uniware.services.common.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.queryParser.GenerateNoSqlQueryFromJsonRequest;
import com.unifier.core.queryParser.IQueryParser;
import com.unifier.core.queryParser.nosql.mongo.GenerateMongoQueryResponse;
import com.unifier.dao.report.IReportDao;
import com.unifier.mao.report.IReportMao;
import com.unifier.scraper.core.QueryType;
import com.unifier.scraper.sl.runtime.IEmailProvider;
import com.unifier.scraper.sl.runtime.IQueryProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.common.ITaskScriptExecutorService;

@Service
@Transactional
public class TaskScriptExecutorServiceImpl implements ITaskScriptExecutorService {

    @Autowired
    private IReportDao         reportDao;

    @Autowired
    private IEmailService      emailService;

    @Autowired
    private IReportMao         reportMao;

    @Autowired
    @Qualifier(value = "mongoQueryParser")
    private IQueryParser       mongoQueryParser;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void executeScript(String scriptName, Map<String, Object> scriptParameters) {
        ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(scriptName, true);
        try {
            ScriptExecutionContext executionContext = ScriptExecutionContext.current();
            executionContext.getScriptVariables().putAll(scriptParameters);
            executionContext.addVariable("__application", applicationContext);
            executionContext.getQueryProviderMap().put(QueryType.SQL, new IQueryProvider() {
                @Override
                public List<Map<String, Object>> executeQuery(String query, Map<String, Object> queryParams) {
                    queryParams.put("facilities", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds());
                    queryParams.put("facilityId", UserContext.current().getFacilityId());
                    queryParams.put("tenantId", UserContext.current().getTenantId());
                    return reportDao.executeAnonymousQuery(query, queryParams);
                }
            });
            executionContext.getQueryProviderMap().put(QueryType.MONGO, new IQueryProvider() {
                @SuppressWarnings("unchecked")
                @Override
                public List<Map<String, Object>> executeQuery(String query, Map<String, Object> queryParams) {
                    GenerateNoSqlQueryFromJsonRequest request = new GenerateNoSqlQueryFromJsonRequest();
                    request.setQueryString(query);
                    request.getQueryParams().put("tenantCode", UserContext.current().getTenant().getCode());
                    request.getQueryParams().putAll(queryParams);
                    GenerateMongoQueryResponse mongoQuery = (GenerateMongoQueryResponse) mongoQueryParser.generateQuery(request);
                    return reportMao.executeAnonymousQuery(mongoQuery.getQuery(), mongoQuery.getPagination(), mongoQuery.getClazz());
                }
            });
            executionContext.setEmailProvider(new IEmailProvider() {
                @Override
                public void sendEmail(EmailMessage emailMessage) {
                    emailMessage.setTemplateName(EmailTemplate.Type.SCRIPT_INSTRUCTION_EMAIL.name());
                    emailService.send(emailMessage);
                }
            });
            executionContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
            script.execute();
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

}
