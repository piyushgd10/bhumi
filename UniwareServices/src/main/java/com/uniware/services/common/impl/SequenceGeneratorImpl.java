/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Oct 6, 2012
 *  @author singla
 */
package com.uniware.services.common.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.expressions.Expression;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.RandomUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Sequence;
import com.uniware.dao.common.ISequenceGeneratorDao;
import com.uniware.services.common.ISequenceGenerator;

@Service
public class SequenceGeneratorImpl implements ISequenceGenerator {

    @Autowired
    private ISequenceGeneratorDao sequenceGeneratorDao;

    private static final Logger   LOG = LoggerFactory.getLogger(SequenceGeneratorImpl.class);

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String generateNext(Sequence.Name name) {
        return generateNext(name.name());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String generateNext(Sequence.Name name, int randomSuffixLength) {
        return generateNext(name) + RandomUtils.getNewString(randomSuffixLength);
    }

    @Override
    public List<String> generateNextN(Sequence.Name name, int n, int randomSuffixLength) {
        List<String> generatedCodes = generateNextN(name.name(), n);
        List<String> finalGeneratedCodes = new ArrayList<>(n);
        for (String code : generatedCodes) {
            finalGeneratedCodes.add(code + RandomUtils.getNewString(randomSuffixLength));
        }
        return finalGeneratedCodes;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String generateNext(String sequenceName) {
        return generateNextInternal(sequenceName);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<String> generateNextN(String sequenceName, int n) {
        return generateNextNInternal(sequenceName, n);
    }

    @Override
    @Transactional
    public String generateNextInSameTransaction(String sequenceName) {
        return generateNextInternal(sequenceName);
    }

    private String generateNextInternal(String sequenceName) {
        List<String> generated = generateNextNInternal(sequenceName, 1);
        return generated.get(0);
    }

    private List<String> generateNextNInternal(String sequenceName, int n) {
        Sequence sequence = sequenceGeneratorDao.getSequenceByName(sequenceName);
        if (sequence == null) {
            throw new IllegalArgumentException("invalid sequence name:" + sequenceName);
        }
        List<String> generatedSequences = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            sequence.setCurrentValue(sequence.getCurrentValue() + sequence.getIncrementBy());
            String nextSequence = String.valueOf(sequence.getCurrentValue());
            if (sequence.getPadToLength() > 0) {
                nextSequence = StringUtils.pad(nextSequence, sequence.getPadToLength(), '0');
            }
            if (StringUtils.isNotBlank(sequence.getPrefix())) {
                generatedSequences.add(sequence.getPrefix() + nextSequence);
            } else {
                generatedSequences.add(nextSequence);
            }
        }
        return generatedSequences;
    }

    @Override
    @Transactional
    public void updateSequencePrefixes() {
        List<Sequence> sequencesToUpdate = sequenceGeneratorDao.getSequencesForPrefixUpdation();
        Map<String, Object> contextParams = new HashMap<String, Object>();
        contextParams.put("currentDate", DateUtils.getCurrentDate());
        for (Sequence sequence : sequencesToUpdate) {
            updateSequencePrefix(sequence, contextParams);
        }
        /* ResetInvoicePrefix at custom time using env property */
        boolean resetInvoicePrefix = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isResetInvoicePrefixNow();
        if (resetInvoicePrefix || DateUtils.isFirstDayOfFinancialYear(DateUtils.getCurrentTime())) {
            sequencesToUpdate = sequenceGeneratorDao.getSequencesForPrefixUpdationOnFinancialYearChange();
            for (Sequence sequence : sequencesToUpdate) {
                LOG.info("Updating Sequence : " + sequence.getName());
                updateSequencePrefixOnFinancialYearChange(sequence);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void updateSequencePrefixOnFinancialYearChange(Sequence sequence) {
        sequence = sequenceGeneratorDao.lockSequenceById(sequence.getId());
        if (sequence.getNextYearPrefix() == null) {
            changeCurrentYearPrefix(sequence, true);
        } else if (!sequence.getNextYearPrefix().equalsIgnoreCase(sequence.getPrefix())) {
            changeCurrentYearPrefix(sequence, sequence.isResetCounterNextYear());
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateSequencePrefix(Sequence sequence, Map<String, Object> contextParams) {
        sequence = sequenceGeneratorDao.lockSequenceById(sequence.getId());
        Expression expression = Expression.compile(sequence.getPrefixExpression());
        String nextPrefix = expression.evaluate(contextParams, String.class);
        if (!nextPrefix.equalsIgnoreCase(sequence.getPrefix())) {
            sequence.setPrefix(nextPrefix);
            if (sequence.getResetInterval() == Sequence.ResetInterval.DAILY
                    || (sequence.getResetInterval() == Sequence.ResetInterval.MONTHLY && DateUtils.isFirstDayOfMonth(DateUtils.getCurrentTime()))) {
                sequence.setCurrentValue(0);
            }
        }
    }

    private void changeCurrentYearPrefix(Sequence sequence, boolean resetCurrentValue) {
        if (resetCurrentValue) {
            sequence.setCurrentValue(0);
        }
        sequence.setPrefix(sequence.getNextYearPrefix());
        sequence.setNextYearPrefix(null);
    }

    @Override
    public void createSequence(Sequence sequence) {
        sequenceGeneratorDao.createSequence(sequence);
    }

    @Override
    @Transactional(readOnly = true)
    public Sequence getSequenceByName(String name, Integer facilityId) {
        return sequenceGeneratorDao.getSequenceByName(name, facilityId, false);
    }

    @Override
    @Transactional(readOnly = true)
    public Sequence getSequenceByName(String name) {
        return sequenceGeneratorDao.getSequenceByName(name);
    }

    @Override
    @Transactional
    public Sequence updateSequence(String sequenceName, Integer facilityId, String newPrefix, int newValue, String nextYearPrefix, boolean resetCounterNextYear) {
        Sequence sequence = sequenceGeneratorDao.getSequenceByName(sequenceName, facilityId, true);
        if (sequence == null) {
            throw new IllegalArgumentException("invalid sequence name:" + sequenceName);
        } else if (newValue < sequence.getCurrentValue()) {
            throw new IllegalArgumentException("Sequence value can only be increased");
        }
        sequence.setCurrentValue(newValue);
        sequence.setPrefix(newPrefix);
        sequence.setNextYearPrefix(nextYearPrefix);
        sequence.setResetCounterNextYear(resetCounterNextYear);
        return sequenceGeneratorDao.updateSequence(sequence);
    }
}
