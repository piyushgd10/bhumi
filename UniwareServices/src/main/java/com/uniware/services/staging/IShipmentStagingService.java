/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/8/17 2:21 PM
 * @author digvijaysharma
 */

package com.uniware.services.staging;

import com.unifier.core.annotation.Level;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.picker.CompletePickBatchRequest;
import com.uniware.core.api.picker.CompletePickBatchResponse;
import com.uniware.core.api.picker.GetPickBucketDetailRequest;
import com.uniware.core.api.picker.GetPickBucketDetailResponse;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemRequest;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketItemRequest;
import com.uniware.core.api.picker.ReceivePickBucketItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketRequest;
import com.uniware.core.api.picker.ReceivePickBucketResponse;
import com.uniware.core.api.picker.ScanItemAtStagingRequest;
import com.uniware.core.api.picker.ScanItemAtStagingResponse;
import com.uniware.core.api.picker.ScanShipmentAtStagingRequest;
import com.uniware.core.api.picker.ScanShipmentAtStagingResponse;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhuvneshwarkumar on 08/09/16.
 */
public interface IShipmentStagingService {

    GetPickBucketDetailResponse getPickBucketDetail(GetPickBucketDetailRequest request);

    ReceivePickBucketResponse receivePickBucketAtStaging(ReceivePickBucketRequest request);

    ScanShipmentAtStagingResponse scanShipmentAtStaging(ScanShipmentAtStagingRequest request);

    ScanItemAtStagingResponse scanItemAtStaging(ScanItemAtStagingRequest request);

    ReceiveNonTraceablePicklistItemResponse receiveNonTraceablePicklistItem(ReceiveNonTraceablePicklistItemRequest request);

    ReceivePickBucketItemResponse receivePickBucketItem(ReceivePickBucketItemRequest request);

    CompletePickBatchResponse completePickBatch(CompletePickBatchRequest request);
}
