/*
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *    
 * @version     1.0, 8/8/17 12:32 PM
 * @author digvijaysharma
 */

package com.uniware.services.staging.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundRequest;
import com.uniware.core.api.packer.MarkPicklistItemsNotFoundResponse;
import com.uniware.core.api.packer.PutbackItemDTO;
import com.uniware.core.api.picker.CompletePickBatchRequest;
import com.uniware.core.api.picker.CompletePickBatchResponse;
import com.uniware.core.api.picker.EditPicklistRequest;
import com.uniware.core.api.picker.GetPickBucketDetailRequest;
import com.uniware.core.api.picker.GetPickBucketDetailResponse;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemRequest;
import com.uniware.core.api.picker.ReceiveNonTraceablePicklistItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketItemRequest;
import com.uniware.core.api.picker.ReceivePickBucketItemResponse;
import com.uniware.core.api.picker.ReceivePickBucketRequest;
import com.uniware.core.api.picker.ReceivePickBucketResponse;
import com.uniware.core.api.picker.ScanItemAtStagingRequest;
import com.uniware.core.api.picker.ScanItemAtStagingResponse;
import com.uniware.core.api.picker.ScanShipmentAtStagingRequest;
import com.uniware.core.api.picker.ScanShipmentAtStagingResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.picker.IPickerDao;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.staging.IShipmentStagingService;
import com.uniware.services.warehouse.IShelfService;

/**
 * Created by bhuvneshwarkumar on 08/09/16.
 */
@Service(value = "shipmentStagingService")
public class ShipmentStagingServiceImpl implements IShipmentStagingService {

    private static final Logger LOG = LoggerFactory.getLogger(ShipmentStagingServiceImpl.class);

    @Autowired
    private IPickerService      pickerService;

    @Autowired
    private IShelfService       shelfService;

    @Autowired
    private ISaleOrderService   saleOrderService;

    @Autowired
    private IUsersService       usersService;

    @Autowired
    private IShippingService    shippingService;

    @Autowired
    private IInventoryService   inventoryService;

    @Autowired
    private IPackerService      packerService;

    @Autowired
    private ILockingService     lockingService;

    @Autowired
    private IPickerDao          pickerDao;

    @Override
    @Transactional(readOnly = true)
    public GetPickBucketDetailResponse getPickBucketDetail(GetPickBucketDetailRequest request) {
        GetPickBucketDetailResponse response = new GetPickBucketDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = pickerService.getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = pickerService.getOpenPickBatchByPickBucketCode(pickBucket.getCode());
                if (pickBatch == null) {
                    throw new IllegalStateException("No open pick batch found for pick bucket");
                } else {
                    SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
                    Map<String, List<GetPickBucketDetailResponse.PickbucketItemDTO>> bucketItems = new HashMap<>();
                    if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
                        for (PicklistItem picklistItem : pickBatch.getPicklistItems()) {
                            boolean putbackComplete = StringUtils.equalsAny(picklistItem.getStatusCode(), PicklistItem.StatusCode.PUTBACK_ACCEPTED,
                                    PicklistItem.StatusCode.PUTBACK_COMPLETE);
                            String shelfCode = PicklistItem.StatusCode.COMPLETE.equals(picklistItem.getStatusCode()) ? picklistItem.getStagingShelfCode() : null;
                            bucketItems.computeIfAbsent(picklistItem.getSkuCode(), k -> new ArrayList<>()).add(
                                    new GetPickBucketDetailResponse.PickbucketItemDTO(picklistItem.getItemCode(), 1, shelfCode, putbackComplete));
                        }
                    } else {
                        Map<String, Map<String, Integer>> skuToStagingShelfToQuantity = new HashMap<>();
                        pickBatch.getPicklistItems().stream().filter(
                                pi -> !StringUtils.equalsAny(pi.getStatusCode(), PicklistItem.StatusCode.PUTBACK_ACCEPTED, PicklistItem.StatusCode.PUTBACK_COMPLETE)).forEach(
                                        picklistItem -> skuToStagingShelfToQuantity.computeIfAbsent(picklistItem.getSkuCode(), k -> new HashMap<>()).compute(
                                                picklistItem.getStagingShelfCode(), (k, v) -> v == null ? 1 : ++v));
                        skuToStagingShelfToQuantity.forEach((sku, shelfToQuantity) -> {
                            List<GetPickBucketDetailResponse.PickbucketItemDTO> pickbucketItems = new ArrayList<>(shelfToQuantity.size());
                            shelfToQuantity.forEach((shelf, quantity) -> pickbucketItems.add(new GetPickBucketDetailResponse.PickbucketItemDTO(null, quantity, shelf, false)));
                            bucketItems.put(sku, pickbucketItems);
                        });
                    }
                    response.setPicklistCode(pickBatch.getPicklist().getCode());
                    response.setBucketItems(bucketItems);
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public ReceivePickBucketResponse receivePickBucketAtStaging(ReceivePickBucketRequest request) {
        ReceivePickBucketResponse response = new ReceivePickBucketResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = pickerService.getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = pickerService.getOpenPickBatchByPickBucketCode(pickBucket.getCode());
                if (pickBatch == null) {
                    throw new IllegalStateException("No open pick batch found for pick bucket");
                } else {
                    receivePickBucketAtStagingInternal(context, response, pickBatch);
                }
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Transactional
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[2].picklist.code}", level = Level.FACILITY) })
    private void receivePickBucketAtStagingInternal(ValidationContext context, ReceivePickBucketResponse response, PickBatch pickBatch) {
        pickBatch = pickerService.getPickBatchById(pickBatch.getId());
        Picklist picklist = pickBatch.getPicklist();
        if (!PickBatch.StatusCode.SUBMITTED.equals(pickBatch.getStatusCode())) {
            context.addError(WsResponseCode.INVALID_PICK_BATCH_STATE, "Invalid Pick batch State : " + pickBatch.getStatusCode().toString());
        } else if (Picklist.Destination.INVOICING.equals(picklist.getDestination())) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Invoicing Destination Picklist cannot be received at Staging");
        } else {
            pickBatch.setStatusCode(PickBatch.StatusCode.RECEIVED);
            if (Picklist.StatusCode.CREATED.name().equals(picklist.getStatusCode())) {
                picklist.setStatusCode(Picklist.StatusCode.RECEIVING.name());
            }
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    public ScanShipmentAtStagingResponse scanShipmentAtStaging(ScanShipmentAtStagingRequest request) {
        ScanShipmentAtStagingResponse response = new ScanShipmentAtStagingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode(), false, true);
            ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(request.getShippingPackageCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code : " + request.getPicklistCode());
            } else if (!Picklist.StatusCode.RECEIVING.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Picklist State : " + picklist.getStatusCode());
            } else if (!Picklist.Destination.STAGING.equals(picklist.getDestination())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Picklist Destination : " + picklist.getDestination().name());
            } else if (!SystemConfiguration.TraceabilityLevel.ITEM_SKU.equals(
                    ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "Feature only available for ITEM_SKU traceability");
            } else if (shippingPackage == null) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "Invalid shipping package code");
            } else if (StringUtils.equalsAny(shippingPackage.getStatusCode(), ShippingPackage.StatusCode.PICKING, ShippingPackage.StatusCode.PICKED)) {
                context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_STATE, "Invalid shipping package state : " + shippingPackage.getStatusCode());
            } else {
                List<PicklistItem> cancelledPicklistItems = new ArrayList<>();
                List<PicklistItem> nonCancelledPicklistItems = new ArrayList<>();
                picklist.getPicklistItems().stream().filter(picklistItem -> picklistItem.getShippingPackageCode().equals(request.getShippingPackageCode())).forEach(
                        picklistItem -> {
                            if (PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
                                cancelledPicklistItems.add(picklistItem);
                            } else if (!picklistItem.isAssessed()) {
                                nonCancelledPicklistItems.add(picklistItem);
                            }
                        });
                if (!CollectionUtils.isEmpty(cancelledPicklistItems)) {
                    context.addError(WsResponseCode.INVALID_PACKAGE_STATE, "Putback Pending for cancelled items in picklist");
                    response.setPutbackItems(cancelledPicklistItems.stream().filter(picklistItem -> true).map(this::preparePutbackItemDTO).collect(Collectors.toList()));
                } else if (CollectionUtils.isEmpty(nonCancelledPicklistItems)) {
                    context.addError(WsResponseCode.INVALID_SHIPPING_PACKAGE_CODE, "No picklist items found for shipping package : " + shippingPackage.getCode());
                } else {
                    int retryCount = 10;
                    Shelf shelf = null;
                    do {
                        LOG.info("Attempt: {} to receive shipment : {} at staging", 11 - retryCount, shippingPackage.getCode());
                        shelf = StringUtils.isBlank(shippingPackage.getShelfCode()) ? shelfService.getNextAvailableShelfInStagingArea(picklist.getPickSetName())
                                : shelfService.getShelfByCode(shippingPackage.getShelfCode());
                        if (shelf == null) {
                            context.addError(WsResponseCode.NO_AVAILABLE_SHELF);
                        } else {
                            scanShipmentAtStagingInternal(picklist, shippingPackage, shelf, context, response);
                        }
                    } while (!response.isSuccessful() && !context.hasErrors() && --retryCount > 0);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Transactional
    private PutbackItemDTO preparePutbackItemDTO(PicklistItem picklistItem) {
        PutbackItemDTO putbackItemDTO = new PutbackItemDTO();
        SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
        putbackItemDTO.setSaleOrderItemCode(saleOrderItem.getCode());
        putbackItemDTO.setItemSku(saleOrderItem.getItemType().getSkuCode());
        putbackItemDTO.setItemName(saleOrderItem.getItemType().getName());
        putbackItemDTO.setShelfCode(saleOrderItem.getItemTypeInventory().getShelf().getCode());
        return putbackItemDTO;
    }

    @Locks({
            @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[1].saleOrder.code}", level = Level.TENANT),
            @Lock(ns = Namespace.SHELF, key = "#{#args[2].code}", level = Level.FACILITY) })
    @Transactional
    private void scanShipmentAtStagingInternal(Picklist picklist, ShippingPackage shippingPackage, Shelf preLockShelf, ValidationContext context,
            ScanShipmentAtStagingResponse response) {
        picklist = pickerService.getPicklistByCode(picklist.getCode());
        shippingPackage = shippingService.getShippingPackageByCode(shippingPackage.getCode());
        Shelf shelf = shelfService.getShelfByCode(preLockShelf.getCode());
        if (shelf.getItemCount() == preLockShelf.getItemCount()) {
            ShippingPackage finalShippingPackage = shippingPackage;
            picklist.getPicklistItems().stream().filter(
                    picklistItem -> picklistItem.getShippingPackageCode().equals(finalShippingPackage.getCode()) && !picklistItem.isAssessed()).forEach(picklistItem -> {
                        picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
                        if (!PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
                            picklistItem.setStatusCode(PicklistItem.StatusCode.COMPLETE);
                            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
                            saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.STAGED.name());
                            saleOrderService.updateSaleOrderItem(saleOrderItem);
                        }
                        picklistItem.setStagingShelfCode(shelf.getCode());
                        shelf.incrementItemCount();
                    });
            shippingPackage.setShelfCode(shelf.getCode());
            shippingService.updateShippingPackage(shippingPackage);
            response.setShelfCode(shelf.getCode());
            response.setSuccessful(true);
        }
    }

    @Override
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklistCode}", level = Level.FACILITY) })
    @Transactional
    public ScanItemAtStagingResponse scanItemAtStaging(ScanItemAtStagingRequest request) {
        ScanItemAtStagingResponse response = new ScanItemAtStagingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            Item item = inventoryService.getItemByCode(request.getItemCode());
            // TODO: 18/11/16 : scan for expirable/bad item
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code : " + request.getPicklistCode());
            } else if (!Picklist.StatusCode.RECEIVING.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Pick Bucket State : " + picklist.getStatusCode());
            } else if (item == null) {
                context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid item code : " + request.getItemCode());
            } else {
                PicklistItem existingPicklistItem = pickerService.getPicklistItemByItemCodeInPicklist(item.getCode(), picklist.getId());
                if (existingPicklistItem != null && existingPicklistItem.isRescannedPutbackItem()) {
                    response.setPutBackItem(true);
                    response.setSaleOrderCode(existingPicklistItem.getSaleOrderCode());
                    response.setSaleOrderItemCode(existingPicklistItem.getSaleOrderItemCode());
                } else {
                    if (existingPicklistItem != null && !StringUtils.equalsAny(existingPicklistItem.getStatusCode(), PicklistItem.StatusCode.SUBMITTED,
                            PicklistItem.StatusCode.RECEIVED, PicklistItem.StatusCode.PUTBACK_PENDING)) {
                        context.addError(WsResponseCode.INVALID_ITEM_ID, "Item code already scanned once in this picklist");
                    }
                    if (!context.hasErrors()) {
                        int retryCount = 10;
                        Shelf shelf = null;
                        do {
                            LOG.info("Attempt: {} to suggest saleOrderItem to item: {}", 11 - retryCount, item.getCode());
                            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemForAllocationInPicklist(picklist.getCode(), item);
                            if (saleOrderItem == null) {
                                context.addError(WsResponseCode.INVALID_ITEM_ALLOCATION);
                                break;
                            } else {
                                if (saleOrderItem.getShippingPackage() != null) {
                                    shelf = StringUtils.isBlank(saleOrderItem.getShippingPackage().getShelfCode())
                                            ? shelfService.getNextAvailableShelfInStagingArea(picklist.getPickSetName())
                                            : shelfService.getShelfByCode(saleOrderItem.getShippingPackage().getShelfCode());
                                    if (shelf == null) {
                                        context.addError(WsResponseCode.NO_AVAILABLE_SHELF);
                                        break;
                                    }
                                }
                            }
                            if (!context.hasErrors()) {
                                if (shelf == null)
                                    scanItemAtStagingInternal(response, context, item, picklist, saleOrderItem, null);
                                else {
                                    java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SHELF, shelf.getCode(), Level.FACILITY);
                                    boolean acquired = false;
                                    try {
                                        acquired = lock.tryLock(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds(),
                                                TimeUnit.SECONDS);
                                        scanItemAtStagingInternal(response, context, item, picklist, saleOrderItem, shelf);
                                    } catch (InterruptedException e) {
                                        LOG.info("Failed to acquire lock on shelf: {}", shelf.getCode());
                                        context.addError(WsResponseCode.INTERNAL_ERROR, "Failed to acquire lock on item " + shelf.getCode());
                                    } finally {
                                        if (acquired) {
                                            lock.unlock();
                                        }
                                    }
                                }
                            }
                        } while (!response.isSuccessful() && !context.hasErrors() && --retryCount > 0);
                    }
                }
            }
        }

        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @LogActivity
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({ @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[4].saleOrder.code}"), @Lock(ns = Namespace.ITEM, key = "#{#args[2].code}", level = Level.FACILITY) })
    private void scanItemAtStagingInternal(ScanItemAtStagingResponse response, ValidationContext context, Item item, Picklist picklist, SaleOrderItem saleOrderItem,
            Shelf preLockShelf) {
        saleOrderItem = saleOrderService.getSaleOrderItemById(saleOrderItem.getId());
        item = inventoryService.getItemById(item.getId());
        PicklistItem picklistItem = null;
        if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).isPickingViaHandheldEnabled() && !item.isPicked()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
        } else if (!item.isGoodInventory()) {
            context.addError(WsResponseCode.INVALID_ITEM_STATE, "Invalid item state: " + item.getStatusCode());
        } else {
            picklistItem = pickerService.pickAndAllocateItemToPicklistItem(context, item, picklist, saleOrderItem);
        }
        if (!context.hasErrors()) {
            picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
            if (SaleOrderItem.StatusCode.PICKING_FOR_STAGING.name().equals(saleOrderItem.getStatusCode())) {
                Shelf shelf = shelfService.getShelfByCode(preLockShelf.getCode());
                if (shelf.getItemCount() == preLockShelf.getItemCount()) {
                    picklistItem.setStatusCode(PicklistItem.StatusCode.COMPLETE);
                    picklistItem.setStagingShelfCode(shelf.getCode());
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.STAGED.name());
                    if (StringUtils.isBlank(saleOrderItem.getShippingPackage().getShelfCode())) {
                        saleOrderItem.getShippingPackage().setShelfCode(shelf.getCode());
                    }
                    shelf.incrementItemCount();
                    response.setShelfCode(shelf.getCode());
                    response.setSuccessful(true);
                    //                    ActivityLogger.forEvent(Event.ITEM_STAGING).withIdentifier(item.getCode()).add(shelf.getCode()).add(picklist.getCode()).log();
                }
            } else {
                response.setPutBackItem(true);
                response.setSuccessful(true);
                response.setSaleOrderCode(picklistItem.getSaleOrderCode());
                response.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
            }
        }
    }

    @Override
    @Transactional
    public ReceiveNonTraceablePicklistItemResponse receiveNonTraceablePicklistItem(ReceiveNonTraceablePicklistItemRequest request) {
        ReceiveNonTraceablePicklistItemResponse response = new ReceiveNonTraceablePicklistItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Picklist picklist = pickerService.getPicklistByCode(request.getPicklistCode());
            if (picklist == null) {
                context.addError(WsResponseCode.INVALID_PICKLIST_CODE, "Invalid Picklist Code");
            } else if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                context.addError(WsResponseCode.FEATURE_NOT_SUPPORTED, "Feature not supported for ITEM level Traceability");
            } else if (!Picklist.StatusCode.RECEIVING.name().equals(picklist.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICKLIST_STATE, "Invalid Picklist status : " + picklist.getStatusCode());
            } else {
                PicklistItem picklistItem = pickerService.getPicklistItemForSoftAllocationBySkuCode(request.getSkuCode(), picklist.getId());
                if (picklistItem == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_SKU_CODE, "Invalid Sku code scanned");
                } else if (picklistItem.isRescannedPutbackItem()) {
                    response.setPutBackItem(true);
                    response.setSaleOrderCode(picklistItem.getSaleOrderCode());
                    response.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
                } else {
                    doReceivePicklistItem(picklistItem.getSkuCode(), response, context, picklistItem);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    /*
     * This method should be called only from app and for staging area
     */
    @Override
    @Transactional
    public ReceivePickBucketItemResponse receivePickBucketItem(ReceivePickBucketItemRequest request) {
        ReceivePickBucketItemResponse response = new ReceivePickBucketItemResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = pickerService.getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = pickerService.getOpenPickBatchByPickBucketCode(pickBucket.getCode());
                PicklistItem picklistItem = pickerService.getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(request.getItemCodeOrSkuCode(), pickBatch);
                // This is to ensure that for SKU LEVEL TRACEABILITY we might have already scanned PUTBACK_PENDING picklist item and set qc_status as ACCEPTED. So to allow rescanning of that item
                if(!SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel()) && picklistItem == null){
                    picklistItem = pickerService.getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(request.getItemCodeOrSkuCode(), pickBatch, PicklistItem.QCStatus.ACCEPTED);
                }
                if (pickBatch == null) {
                    throw new IllegalStateException("No open pick batch found for pick bucket");
                } else if (!PickBatch.StatusCode.RECEIVED.equals(pickBatch.getStatusCode())) {
                    context.addError(WsResponseCode.INVALID_PICK_BATCH_STATE, "Receive Pick Bucket First");
                } else if (picklistItem == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_CODE, "Invalid Item code scanned");
                } else if (picklistItem.isRescannedPutbackItem()) {
                    response.setPutBackItem(true);
                    response.setSaleOrderCode(picklistItem.getSaleOrderCode());
                    response.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
                } else {
                    doReceivePicklistItem(request.getItemCodeOrSkuCode(), response, context, picklistItem);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    private void doReceivePicklistItem(String itemOrSkuCode, ReceivePickBucketItemResponse response, ValidationContext context, PicklistItem picklistItem) {
        int retryCount = 10;
        Shelf shelf = null;
        do {
            LOG.info("Attempt: {} to receive Pick Bucket Item : {}", 11 - retryCount, itemOrSkuCode);
            SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
            if (saleOrderItem.getShippingPackage() != null && !PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
                shelf = StringUtils.isBlank(saleOrderItem.getShippingPackage().getShelfCode())
                        ? shelfService.getNextAvailableShelfInStagingArea(picklistItem.getPicklist().getPickSetName())
                        : shelfService.getShelfByCode(saleOrderItem.getShippingPackage().getShelfCode());
                if (shelf == null) {
                    context.addError(WsResponseCode.NO_AVAILABLE_SHELF);
                    break;
                }
                if (!context.hasErrors()) {
                    java.util.concurrent.locks.Lock lock = lockingService.getLock(Namespace.SHELF, shelf.getCode(), Level.FACILITY);
                    boolean acquired = false;
                    try {
                        acquired = lock.tryLock(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getLockWaitTimeoutInSeconds(), TimeUnit.SECONDS);
                        receivePicklistItemInternal(context, response, picklistItem, shelf, itemOrSkuCode);
                    } catch (InterruptedException e) {
                        LOG.info("Failed to acquire lock on shelf: {}", shelf.getCode());
                        context.addError(WsResponseCode.INTERNAL_ERROR, "Failed to acquire lock on item " + shelf.getCode());
                    } finally {
                        if (acquired) {
                            lock.unlock();
                        }
                    }
                }
            } else {
                receivePicklistItemInternal(context, response, picklistItem, shelf, itemOrSkuCode);
            }

        } while (!response.isSuccessful() && !context.hasErrors() && --retryCount > 0);
    }

    @LogActivity
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Locks({
            @Lock(ns = Namespace.SALE_ORDER, key = "#{#args[2].saleOrderCode}"),
            @Lock(ns = Namespace.PICKLIST, key = "#{#args[2].picklist.code}", level = Level.FACILITY),
            @Lock(ns = Namespace.ITEM, key = "#{#args[4]}", level = Level.FACILITY) })
    private void receivePicklistItemInternal(ValidationContext context, ReceivePickBucketItemResponse response, PicklistItem picklistItem, Shelf preLockShelf, String itemCode) {
        picklistItem = pickerService.getPicklistItemById(picklistItem.getId());
        picklistItem.setQcStatusCode(PicklistItem.QCStatus.ACCEPTED);
        if (!context.hasErrors()) {
            if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
                Item item = inventoryService.getItemByCode(itemCode);
                item.setStatusCode(Item.StatusCode.PICKED.name());
            }
            //            ActivityLogger.forEvent(Event.ITEM_PICKED).withIdentifier(item.getCode()).add(picklistItem.getPicklist().getCode()).log();
            // if item was cancelled
            if (PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode())) {
                response.setSaleOrderCode(picklistItem.getSaleOrderCode());
                response.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
                response.setPutBackItem(true);
            } else {
                Shelf shelf = shelfService.getShelfByCode(preLockShelf.getCode());
                if (shelf.getItemCount() == preLockShelf.getItemCount()) {
                    SaleOrderItem saleOrderItem = saleOrderService.getSaleOrderItemByCode(picklistItem.getSaleOrderCode(), picklistItem.getSaleOrderItemCode());
                    saleOrderItem.setStatusCode(SaleOrderItem.StatusCode.STAGED.name());
                    picklistItem.setStatusCode(PicklistItem.StatusCode.COMPLETE);
                    picklistItem.setStagingShelfCode(shelf.getCode());
                    if (StringUtils.isBlank(saleOrderItem.getShippingPackage().getShelfCode())) {
                        saleOrderItem.getShippingPackage().setShelfCode(shelf.getCode());
                    }
                    shelf.incrementItemCount();
                    response.setShelfCode(shelf.getCode());
                    response.setSuccessful(true);
                    //                    ActivityLogger.forEvent(Event.ITEM_STAGING).withIdentifier(item.getCode()).add(shelf.getCode()).add(picklistItem.getPicklist().getCode()).log();
                }
            }
        }
    }

    @Override
    public CompletePickBatchResponse completePickBatch(CompletePickBatchRequest request) {
        CompletePickBatchResponse response = new CompletePickBatchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            PickBucket pickBucket = pickerService.getPickBucketByCode(request.getPickBucketCode());
            if (pickBucket == null) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_CODE, "Invalid Pick Bucket Code : " + request.getPickBucketCode());
            } else if (!PickBucket.StatusCode.IN_USE.equals(pickBucket.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_PICK_BUCKET_STATE, "Invalid Pick Bucket State : " + pickBucket.getStatusCode());
            } else {
                PickBatch pickBatch = pickerService.getOpenPickBatchByPickBucketCode(pickBucket.getCode());
                if (pickBatch == null) {
                    throw new IllegalStateException("No open pick batch found for pick bucket");
                } else {
                    completePickBatchInternal(pickBatch, context);
                }
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        } else {
            response.addErrors(context.getErrors());
            response.addWarnings(context.getWarnings());
        }
        return response;
    }

    @Transactional
    @RollbackOnFailure
    @Locks({ @Lock(ns = Namespace.PICKLIST, key = "#{#args[0].picklist.code}", level = Level.FACILITY) })
    private void completePickBatchInternal(PickBatch pickBatch, ValidationContext context) {
        pickBatch = pickerService.getPickBatchById(pickBatch.getId());
        for (PicklistItem picklistItem : pickBatch.getPicklistItems()) {
            // Only pick-batch items which are not scanned till now need to be mark as NOT_FOUND but we will stuck in one case if PUTBACK_PENDING item is scanned but not ACCEPTED at that time,
            // so we need to get it PUTBACK_ACCEPTED from user before marking the pick-batch as COMPLETE or mark this item as NOT_FOUND manually.
            if (PicklistItem.StatusCode.SUBMITTED.equals(picklistItem.getStatusCode()) || (PicklistItem.StatusCode.PUTBACK_PENDING.equals(picklistItem.getStatusCode()) && !PicklistItem.QCStatus.ACCEPTED.equals(picklistItem.getQcStatusCode()))) {
                MarkPicklistItemsNotFoundRequest markNotFoundRequest = new MarkPicklistItemsNotFoundRequest();
                markNotFoundRequest.setPicklistCode(pickBatch.getPicklist().getCode());
                markNotFoundRequest.setUserId(usersService.getUserByUsername(UserContext.current().getUniwareUserName()).getId());
                EditPicklistRequest.WsEditPicklistItem missingPicklistItem = new EditPicklistRequest.WsEditPicklistItem();
                missingPicklistItem.setSaleOrderCode(picklistItem.getSaleOrderCode());
                missingPicklistItem.setSaleOrderItemCode(picklistItem.getSaleOrderItemCode());
                markNotFoundRequest.setMissingPicklistItems(Collections.singletonList(missingPicklistItem));
                MarkPicklistItemsNotFoundResponse markNotFoundResponse = packerService.markPicklistItemsNotFound(markNotFoundRequest);
                if (markNotFoundResponse.hasErrors()) {
                    context.addErrors(markNotFoundResponse.getErrors());
                }
            }
        }
        if (!context.hasErrors()) {
            pickBatch.getPickBucket().setStatusCode(PickBucket.StatusCode.AVAILABLE);
            pickBatch.setStatusCode(PickBatch.StatusCode.CLOSED);
            if (pickerService.isPicklistComplete(pickBatch.getPicklist())) {
                Picklist picklist = pickBatch.getPicklist();
                picklist.getPickBatches().stream().filter(pickBatch1 -> !pickBatch1.isClosed()).forEach(pickBatch1 -> {
                    pickBatch1.getPickBucket().setStatusCode(PickBucket.StatusCode.AVAILABLE);
                    pickBatch1.setStatusCode(PickBatch.StatusCode.CLOSED);
                });
                picklist.setStatusCode(Picklist.StatusCode.CLOSED.name());
            }
        }
    }
}
