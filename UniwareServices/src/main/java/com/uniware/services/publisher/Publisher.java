/*
 *  Copyright 2014 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-May-2014
 *  @author sunny
 */
package com.uniware.services.publisher;

public interface Publisher {

    public abstract String getName();
    
    public abstract void publish(Message message);

}
