package com.uniware.services.publisher.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.unifier.core.jms.IActiveMQConnector;
import com.unifier.core.jms.MessagingConstants;
import com.uniware.services.publisher.IMessageBroadcaster;
import com.uniware.services.publisher.Message;
import com.uniware.services.publisher.Publisher;

@Component
public class MessageBroadcasterImpl implements IMessageBroadcaster {

    private Set<Publisher> publishers = new HashSet<>();

    @Autowired
    @Qualifier("activeMQConnector1")
    private IActiveMQConnector activeMQConnector;

    @Override
    public void broadcast(Message message) {
        activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.STOMP_MESSAGES_TOPIC, message);
    }

    @Override
    public void addPublisher(Publisher publisher) {
        publishers.remove(publisher);
        publishers.add(publisher);
    }

    @Override
    public void publish(Message message) {
        for (Publisher publisher : publishers) {
            publisher.publish(message);
        }
    }
}
