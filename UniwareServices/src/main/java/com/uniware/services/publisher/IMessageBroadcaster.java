package com.uniware.services.publisher;

public interface IMessageBroadcaster {

    void addPublisher(Publisher publisher);

    void broadcast(Message message);

    void publish(Message message);

}
