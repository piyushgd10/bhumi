package com.uniware.services.publisher;

import java.io.Serializable;

public class Message implements Serializable {

    private static final long serialVersionUID = 1287425619046912747L;

    public enum Type {
        TENANT,
        FACILITY
    }

    private String destination;
    private Type   type;
    private Object payload;

    public Message() {
        super();
    }

    public Message(String destination, Type type, Object payload) {
        super();
        this.destination = destination;
        this.type = type;
        this.payload = payload;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

}
