/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.services.vendor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.uniware.services.tax.ITaxService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.AddUserRolesResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.user.SignupVendorRequest;
import com.unifier.core.api.user.SignupVendorResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.users.IUsersService;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.catalog.dto.ItemTypeDTO;
import com.uniware.core.api.party.ActivateVendorAgreementRequest;
import com.uniware.core.api.party.ActivateVendorAgreementResponse;
import com.uniware.core.api.party.AddOrEditVendorAgreementRequest;
import com.uniware.core.api.party.AddOrEditVendorAgreementResponse;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeRequest;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeResponse;
import com.uniware.core.api.party.CreateOrEditVendorRequest;
import com.uniware.core.api.party.CreateOrEditVendorResponse;
import com.uniware.core.api.party.CreateVendorAgreementRequest;
import com.uniware.core.api.party.CreateVendorAgreementResponse;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.CreateVendorItemTypeResponse;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.GetVendorItemTypesRequest;
import com.uniware.core.api.party.GetVendorItemTypesResponse;
import com.uniware.core.api.party.GetVendorRequest;
import com.uniware.core.api.party.GetVendorResponse;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.api.party.SearchVendorItemTypesResponse;
import com.uniware.core.api.party.SearchVendorRequest;
import com.uniware.core.api.party.SearchVendorResponse;
import com.uniware.core.api.party.SearchVendorResponse.VendorSearchDTO;
import com.uniware.core.api.party.WsGenericVendor;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.party.WsVendorAgreement;
import com.uniware.core.api.party.WsVendorItemType;
import com.uniware.core.api.party.dto.VendorAgreementDTO;
import com.uniware.core.api.party.dto.VendorDTO;
import com.uniware.core.api.party.dto.VendorFullDTO;
import com.uniware.core.api.party.dto.VendorItemTypeDTO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.configuration.TaxConfiguration.TaxTypeVO;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorAgreementStatus;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.vo.VendorInventoryLogVO;
import com.uniware.dao.catalog.ICatalogDao;
import com.uniware.dao.vendor.IVendorDao;
import com.uniware.dao.vendor.IVendorInventoryLogMao;
import com.uniware.services.party.IGenericPartyService;
import com.uniware.services.party.IPartyService;
import com.uniware.services.vendor.IVendorService;

@Service
@Transactional
public class VendorServiceImpl implements IVendorService {
    @Autowired
    private IVendorDao             vendorDao;

    @Autowired
    private ICatalogDao            catalogDao;

    @Autowired
    private IUsersService          usersService;

    @Autowired
    private IPartyService          partyService;

    @Autowired
    private IVendorInventoryLogMao vendorInventoryLogMao;

    @Autowired
    private IGenericPartyService   genricPartyService;

    @Autowired
    private ITaxService             taxService;

    @Override
    public SearchVendorResponse searchVendor(SearchVendorRequest request) {
        SearchVendorResponse response = new SearchVendorResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<Vendor> vendors = vendorDao.searchVendor(request.getKeyword());
            for (Vendor vendor : vendors) {
                VendorSearchDTO vendorSearchDTO = new VendorSearchDTO();
                vendorSearchDTO.setId(vendor.getId());
                vendorSearchDTO.setCode(vendor.getCode());
                vendorSearchDTO.setName(vendor.getName());
                vendorSearchDTO.setWebsite(vendor.getWebsite());

                PartyContact partyContact = vendor.getPartyContactByType(PartyContactType.Code.PRIMARY.name());
                if (partyContact != null) {
                    vendorSearchDTO.setContactEmail(partyContact.getEmail());
                    vendorSearchDTO.setContactName(partyContact.getName());
                }

                PartyAddress partyAddress = vendor.getPartyAddressByType(PartyAddressType.Code.SHIPPING.name());
                if (partyAddress != null) {
                    vendorSearchDTO.setCity(partyAddress.getCity());
                    vendorSearchDTO.setPhone(partyAddress.getPhone());
                }
                response.getVendors().add(vendorSearchDTO);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public Vendor getVendorByCode(String code) {
        Vendor vendor = vendorDao.getVendorByCode(code);
        if (vendor != null) {
            for (PartyContact partyContact : vendor.getPartyContacts()) {
                Hibernate.initialize(partyContact);
                Hibernate.initialize(partyContact.getPartyContactType());
            }

        }
        return vendor;
    }

    @Override
    public Vendor getVendorById(int vendorId) {
        Vendor vendor = vendorDao.getVendorById(vendorId);
        return vendor;
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public CreateVendorResponse createVendor(CreateVendorRequest request) {
        CreateVendorResponse response = new CreateVendorResponse();
        // TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getVendor() != null) {
            updateRequestBeforeValidation(request.getVendor());
        }
        createVendorInternal(request, response, true);
        return response;
    }

    @Transactional
    private void createVendorInternal(CreateVendorRequest request, CreateVendorResponse response, boolean validateOptionalBlanks) {
        ValidationContext context = request.validate(validateOptionalBlanks);
        if (!context.hasErrors()) {
            Vendor vendor = vendorDao.getVendorByCode(request.getVendor().getCode());
            if (vendor != null) {
                context.addError(WsResponseCode.DUPLICATE_VENDOR_CODE, "Vendor Code already exists");
            } else {
                vendor = new Vendor();
                if (request.getVendor().getEnabled() == null) {
                    request.getVendor().setEnabled(Boolean.TRUE);
                }
                genricPartyService.prepareParty(vendor, request.getVendor(), context, true);
                if (!context.hasErrors()) {
                    vendor.setAcceptsCForm(request.getVendor().isAcceptsCForm());
                    vendor.setPurchaseExpiryPeriod(request.getVendor().getPurchaseExpiryPeriod());
                    Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, Vendor.class.getName(), request.getVendor().getCustomFieldValues());
                    CustomFieldUtils.setCustomFieldValues(vendor, customFieldValues, false);
                    vendor.setCreated(DateUtils.getCurrentTime());
                    vendor = vendorDao.createVendor(vendor);
                    if (request.getVendor().getVendorAgreements() != null) {
                        for (WsVendorAgreement vendorAgreement : request.getVendor().getVendorAgreements()) {
                            CreateVendorAgreementRequest createVendorAgreementRequest = new CreateVendorAgreementRequest();
                            createVendorAgreementRequest.setVendorAgreement(vendorAgreement);
                            CreateVendorAgreementResponse createVendorAgreementResponse = createAgreement(createVendorAgreementRequest);
                            if (!createVendorAgreementResponse.isSuccessful()) {
                                context.getErrors().addAll(createVendorAgreementResponse.getErrors());
                            }
                        }
                    }
                    if (!context.hasErrors()) {
                        response.setVendor(new VendorFullDTO(vendor));
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public EditVendorResponse editVendor(EditVendorRequest request) {
        EditVendorResponse response = new EditVendorResponse();
        // TODO - should be removed when all Party creation/editing are moved to new services
        if (request.getVendor() != null) {
            updateRequestBeforeValidation(request.getVendor());
        }
        ValidationContext context = request.validate(false);
        Map<String, WsVendorAgreement> vendorAgreements = new HashMap<>();
        if (!context.hasErrors()) {
            Vendor vendor = vendorDao.getVendorByCode(request.getVendor().getCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid vendor code");
            } else {
                if (!context.hasErrors()) {
                    genricPartyService.prepareParty(vendor, request.getVendor(), context);
                    vendor.setAcceptsCForm(ValidatorUtils.getOrDefaultValue(request.getVendor().isAcceptsCForm(), vendor.isAcceptsCForm()));
                    vendor.setPurchaseExpiryPeriod(ValidatorUtils.getOrDefaultValue(request.getVendor().getPurchaseExpiryPeriod(), vendor.getPurchaseExpiryPeriod()));
                    Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, Vendor.class.getName(), request.getVendor().getCustomFieldValues());
                    CustomFieldUtils.setCustomFieldValues(vendor, customFieldValues);
                    if (!context.hasErrors()) {
                        vendor = vendorDao.updateVendor(vendor);
                        if (request.getVendor().getVendorAgreements() != null) {
                            for (WsVendorAgreement vendorAgreement : request.getVendor().getVendorAgreements()) {
                                if (!context.hasErrors()) {
                                    vendorAgreements.put(vendorAgreement.getName(), vendorAgreement);
                                    AddOrEditVendorAgreementRequest editVendorAgreementRequest = new AddOrEditVendorAgreementRequest();
                                    editVendorAgreementRequest.setVendorAgreement(vendorAgreement);
                                    AddOrEditVendorAgreementResponse editVendorAgreementResponse = addOrEditVendorAgreementInternal(editVendorAgreementRequest, vendor, context);
                                    if (editVendorAgreementResponse.hasErrors()) {
                                        context.getErrors().addAll(editVendorAgreementResponse.getErrors());
                                    }
                                }
                            }
                        }
                        if (!context.hasErrors()) {
                            VendorFullDTO vendorFullDTO = new VendorFullDTO(vendor);
                            vendorFullDTO.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(vendor));
                            response.setVendor(vendorFullDTO);
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetVendorResponse getVendorByCode(GetVendorRequest request) {
        GetVendorResponse response = new GetVendorResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Vendor vendor = vendorDao.getVendorByCode(request.getCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid vendor code");
            } else {
                VendorFullDTO vendorDto = new VendorFullDTO(vendor);
                vendorDto.setCustomFieldValues(CustomFieldUtils.getCustomFieldValuesDTO(vendor));
                response.setVendor(vendorDto);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public CreateVendorAgreementResponse createAgreement(CreateVendorAgreementRequest request) {
        CreateVendorAgreementResponse response = new CreateVendorAgreementResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            WsVendorAgreement wsVendorAgreement = request.getVendorAgreement();
            Vendor vendor = getVendorByCode(wsVendorAgreement.getVendorCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_ID, "Invalid Vendor Id");
            } else {
                VendorAgreement vendorAgreement = vendorDao.getVendorAgreement(vendor.getId(), wsVendorAgreement.getName());
                if (vendorAgreement != null) {
                    context.addError(WsResponseCode.DUPLICATE_VENDOR_AGREEMENT, "Duplicate Vendor Agreement");
                } else {
                    if (request.getVendorAgreement().getEndTime().compareTo(request.getVendorAgreement().getStartTime()) <= 0) {
                        context.addError(WsResponseCode.INVALID_DATE, "Agreement end date should be greater then start date");
                    } else {
                        vendorAgreement = new VendorAgreement();
                        vendorAgreement.setAgreementText(wsVendorAgreement.getAgreementText());
                        vendorAgreement.setStartTime(wsVendorAgreement.getStartTime());
                        vendorAgreement.setEndTime(wsVendorAgreement.getEndTime());
                        vendorAgreement.setStatusCode(wsVendorAgreement.getVendorAgreementStatus());
                        vendorAgreement.setName(wsVendorAgreement.getName());
                        vendorAgreement.setVendor(vendor);
                        vendorAgreement = vendorDao.createAgreement(vendorAgreement);
                        response.setSuccessful(true);
                        response.setVendorAgreementDTO(new VendorAgreementDTO(vendorAgreement));
                    }
                }
            }
        }

        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddOrEditVendorAgreementResponse addOrEditAgreement(AddOrEditVendorAgreementRequest request) {
        AddOrEditVendorAgreementResponse response = new AddOrEditVendorAgreementResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Vendor vendor = vendorDao.getVendorByCode(request.getVendorAgreement().getVendorCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE, "Invalid Vendor Code");
            } else {
                response = addOrEditVendorAgreementInternal(request, vendor, context);
            }
        }
        if (!response.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private AddOrEditVendorAgreementResponse addOrEditVendorAgreementInternal(AddOrEditVendorAgreementRequest request, Vendor vendor, ValidationContext context) {
        AddOrEditVendorAgreementResponse response = new AddOrEditVendorAgreementResponse();
        if (request.getVendorAgreement().getEndTime().compareTo(request.getVendorAgreement().getStartTime()) <= 0) {
            context.addError(WsResponseCode.INVALID_DATE, "Agreement end date should be greater then start date");
        } else {
            WsVendorAgreement wsVendorAgreement = request.getVendorAgreement();
            VendorAgreement vendorAgreement = vendorDao.getVendorAgreement(vendor.getId(), wsVendorAgreement.getName());
            if (vendorAgreement == null) {
                vendorAgreement = new VendorAgreement();
                vendorAgreement.setStatusCode(request.getVendorAgreement().getVendorAgreementStatus());
                prepareVendorAgreement(vendorAgreement, wsVendorAgreement, vendor);
                vendorAgreement = vendorDao.save(vendorAgreement);
            } else {
                prepareVendorAgreement(vendorAgreement, wsVendorAgreement, vendor);
                vendorAgreement = vendorDao.updateAgreement(vendorAgreement);
            }
            for (VendorAgreement agreement : vendor.getVendorAgreements()) {
                if (agreement.getName().equals(vendorAgreement.getName())) {
                    vendor.getVendorAgreements().remove(agreement);
                    break;
                }
            }
            vendor.getVendorAgreements().add(vendorAgreement);
            response.setVendorAgreementDTO(new VendorAgreementDTO(vendorAgreement));
        }
        return response;
    }

    private void prepareVendorAgreement(VendorAgreement vendorAgreement, WsVendorAgreement wsVendorAgreement, Vendor vendor) {
        vendorAgreement.setAgreementText(wsVendorAgreement.getAgreementText());
        vendorAgreement.setStartTime(wsVendorAgreement.getStartTime());
        vendorAgreement.setEndTime(wsVendorAgreement.getEndTime());
        vendorAgreement.setName(wsVendorAgreement.getName());
        vendorAgreement.setVendor(vendor);
    }

    @Override
    public SearchVendorItemTypesResponse searchVendorItemType(SearchVendorItemTypesRequest request) {
        SearchVendorItemTypesResponse response = new SearchVendorItemTypesResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<VendorItemType> vendorItemTypes = vendorDao.getVendorItemTypes(request);
            for (VendorItemType vendorItemType : vendorItemTypes) {
                VendorItemTypeDTO vendorItemTypeDTO = prepareVendorItemTypeDTO(vendorItemType);
                response.getElements().add(vendorItemTypeDTO);
            }

            Long count = vendorDao.getVendorItemTypeCount(request);
            response.setTotalRecords(count);

            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public VendorAgreement getVendorAgreementById(int vendorAgreementId) {
        return vendorDao.getVendorAgreementById(vendorAgreementId);
    }

    @Override
    public List<VendorDTO> listVendors() {
        List<VendorDTO> vendors = new ArrayList<VendorDTO>();
        for (Vendor vendor : vendorDao.listVendors()) {
            vendors.add(new VendorDTO(vendor));
        }
        return vendors;
    }

    @Override
    public GetVendorItemTypesResponse getVendorItemTypes(GetVendorItemTypesRequest request) {
        GetVendorItemTypesResponse response = new GetVendorItemTypesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Vendor vendor = getVendorByCode(request.getVendorCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE);
            } else {
                List<VendorItemType> vendorItemTypes = vendorDao.getVendorItemTypes(vendor.getId(), (request.getPageNumber() - 1) * request.getPageSize(), request.getPageSize());
                List<VendorItemTypeDTO> vendorItemTypeDTOs = new ArrayList<VendorItemTypeDTO>(vendorItemTypes.size());
                for (VendorItemType vendorItemType : vendorItemTypes) {
                    vendorItemTypeDTOs.add(prepareVendorItemTypeDTO(vendorItemType));
                }
                response.setVendorItemTypes(vendorItemTypeDTOs);
                response.setTotalRecords((int) vendorDao.getVendorItemTypeCount(vendor.getId()));
                response.setPageNumber(request.getPageNumber());
                response.setPageSize(request.getPageSize());
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public EditVendorItemTypeResponse editVendorItemType(EditVendorItemTypeRequest request) {
        EditVendorItemTypeResponse response = new EditVendorItemTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            Vendor vendor = vendorDao.getVendorByCode(request.getVendorItemType().getVendorCode());
            if (vendor == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_ID, "Invalid vendor code");
            } else {
                ItemType itemType = catalogDao.getItemTypeBySkuCode(request.getVendorItemType().getItemTypeSkuCode());
                if (itemType == null) {
                    context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type skucode");
                } else {
                    VendorItemType vendorItemType = getVendorItemType(request.getVendorItemType().getVendorCode(), request.getVendorItemType().getItemTypeSkuCode());
                    if (vendorItemType == null) {
                        context.addError(WsResponseCode.INVALID_VENDOR_ITEM_TYPE, "Vendor Item Type does not exist");
                    } else if (itemType.getMaxRetailPrice() != null && request.getVendorItemType().getUnitPrice() != null
                            && itemType.getMaxRetailPrice().compareTo(request.getVendorItemType().getUnitPrice()) == -1) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Vendor Item master Unit price value can't be greater than Max Retail Price in Item Master");
                    } else {
                        int oldInventory = vendorItemType.getVendorInventory();
                        prepareVendorItemType(request.getVendorItemType(), vendorItemType, context);
                        vendorDao.updateVendorItemType(vendorItemType);
                        if (!context.hasErrors()) {
                            if (vendorItemType.getVendorInventory() != oldInventory) {
                                saveInventoryUpdateLog(request, vendorItemType, oldInventory);
                            }
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void saveInventoryUpdateLog(EditVendorItemTypeRequest request, VendorItemType vendorItemType, Integer oldInventory) {
        VendorInventoryLogVO inventoryLog = new VendorInventoryLogVO();
        inventoryLog.setItemSku(request.getVendorItemType().getItemTypeSkuCode());
        inventoryLog.setAction(request.getActionType());
        inventoryLog.setOldValue(oldInventory);
        inventoryLog.setNewValue(vendorItemType.getVendorInventory());
        User user = usersService.getUserWithDetailsById(request.getUserId());
        if (user != null) {
            inventoryLog.setUsername(user.getUsername());
        } else {
            inventoryLog.setUsername("UnKnown");
        }
        inventoryLog.setCreated(DateUtils.getCurrentTime());
        vendorInventoryLogMao.create(inventoryLog);
    }

    @Override
    public SearchItemTypesResponse searchItemTypesByKeyword(SearchItemTypesRequest request) {
        SearchItemTypesResponse response = new SearchItemTypesResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            List<ItemType> itemTypes = catalogDao.lookupItemTypes(request.getKeyword(), 10);
            response.setSuccessful(true);
            for (ItemType itemType : itemTypes) {
                ItemTypeDTO itemTypeDTO = new ItemTypeDTO();
                itemTypeDTO.setName(itemType.getName());
                itemTypeDTO.setSkuCode(itemType.getSkuCode());
                response.getElements().add(itemTypeDTO);
            }
        }
        return response;
    }

    @Override
    public CreateVendorItemTypeResponse createVendorItemType(CreateVendorItemTypeRequest request) {
        CreateVendorItemTypeResponse response = new CreateVendorItemTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            VendorItemType vendorItemType = getVendorItemType(request.getVendorItemType().getVendorCode(), request.getVendorItemType().getItemTypeSkuCode());
            if (vendorItemType != null) {
                context.addError(WsResponseCode.DUPLICATE_VENDOR_ITEM_TYPE, "Vendor Item Type already exists");
            } else {
                Vendor vendor = vendorDao.getVendorByCode(request.getVendorItemType().getVendorCode());
                if (vendor == null) {
                    context.addError(WsResponseCode.INVALID_VENDOR_ID, "Invalid vendor code");
                } else {
                    ItemType itemType = catalogDao.getItemTypeBySkuCode(request.getVendorItemType().getItemTypeSkuCode());
                    if (itemType == null) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Invalid item type skucode");
                    } else if (itemType.getMaxRetailPrice() != null && itemType.getMaxRetailPrice().compareTo(request.getVendorItemType().getUnitPrice()) == -1) {
                        context.addError(WsResponseCode.INVALID_ITEM_TYPE, "Vendor Item master Unit price value can't be greater than Max Retail Price in Item Master");
                    } else {
                        vendorItemType = new VendorItemType();
                        vendorItemType.setItemType(itemType);
                        vendorItemType.setVendor(vendor);
                        vendorItemType.setCreated(DateUtils.getCurrentTime());
                        prepareVendorItemType(request.getVendorItemType(), vendorItemType, context);
                        if (!context.hasErrors()) {
                            vendorItemType = vendorDao.createVendorItemType(vendorItemType);
                            response.setVendorItemType(prepareVendorItemTypeDTO(vendorItemType));
                            response.setSuccessful(true);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private VendorItemTypeDTO prepareVendorItemTypeDTO(VendorItemType vendorItemType) {
        VendorItemTypeDTO vendorItemTypeDTO = new VendorItemTypeDTO();
        vendorItemTypeDTO.setName(vendorItemType.getItemType().getName());
        vendorItemTypeDTO.setVendorItemTypeId(vendorItemType.getId());
        vendorItemTypeDTO.setVendorId(vendorItemType.getVendor().getId());
        vendorItemTypeDTO.setVendorCode(vendorItemType.getVendor().getCode());
        vendorItemTypeDTO.setVendorSkuCode(vendorItemType.getVendorSkuCode());
        vendorItemTypeDTO.setItemSKU(vendorItemType.getItemType().getSkuCode());
        vendorItemTypeDTO.setUnitPrice(vendorItemType.getUnitPrice());
        if (taxService.isGSTEnabled()){
            if (vendorItemType.getItemType().getGstTaxType() != null) {
                vendorItemTypeDTO.setTaxType(new TaxTypeVO(vendorItemType.getItemType().getGstTaxType()));
            } else if (vendorItemType.getItemType().getCategory().getGstTaxType() != null) {
                vendorItemTypeDTO.setTaxType(new TaxTypeVO(vendorItemType.getItemType().getCategory().getGstTaxType()));
            }
        } else {
            if (vendorItemType.getItemType().getTaxType() != null) {
                vendorItemTypeDTO.setTaxType(new TaxTypeVO(vendorItemType.getItemType().getTaxType()));
            } else if (vendorItemType.getItemType().getCategory().getTaxType() != null) {
                vendorItemTypeDTO.setTaxType(new TaxTypeVO(vendorItemType.getItemType().getCategory().getTaxType()));
            }
        }

        vendorItemTypeDTO.setInventory(vendorItemType.getVendorInventory());
        vendorItemTypeDTO.setPageUrl(vendorItemType.getItemType().getProductPageUrl());
        vendorItemTypeDTO.setImageUrl(vendorItemType.getItemType().getImageUrl());
        vendorItemTypeDTO.setMaxRetailPrice(vendorItemType.getItemType().getMaxRetailPrice());
        vendorItemTypeDTO.setColor(vendorItemType.getItemType().getColor());
        vendorItemTypeDTO.setBrand(vendorItemType.getItemType().getBrand());
        vendorItemTypeDTO.setSize(vendorItemType.getItemType().getSize());
        vendorItemTypeDTO.setEnabled(vendorItemType.isEnabled());
        return vendorItemTypeDTO;
    }

    private void prepareVendorItemType(WsVendorItemType wsVendorItemType, VendorItemType vendorItemType, ValidationContext context) {
        vendorItemType.setUnitPrice(ValidatorUtils.getOrDefaultValue(wsVendorItemType.getUnitPrice(), vendorItemType.getUnitPrice()));
        vendorItemType.setPriority(ValidatorUtils.getOrDefaultValue(wsVendorItemType.getPriority(), vendorItemType.getPriority()));
        vendorItemType.setEnabled(ValidatorUtils.getOrDefaultValue(wsVendorItemType.getEnabled(), vendorItemType.isEnabled()));
        if (vendorItemType.isEnabled()) {
            vendorItemType.setVendorInventory(ValidatorUtils.getOrDefaultValue(wsVendorItemType.getInventory(), vendorItemType.getVendorInventory()));
        } else {
            vendorItemType.setVendorInventory(0);
        }
        vendorItemType.setVendorSkuCode(ValidatorUtils.getOrDefaultValue(wsVendorItemType.getVendorSkuCode(), vendorItemType.getVendorSkuCode()));
        if (wsVendorItemType.getCustomFieldValues() != null) {
            Map<String, Object> customFieldValues = CustomFieldUtils.getCustomFieldValues(context, VendorItemType.class.getName(), wsVendorItemType.getCustomFieldValues());
            CustomFieldUtils.setCustomFieldValues(vendorItemType, customFieldValues, false);
        }
    }

    @Override
    public List<VendorAgreementDTO> getValidVendorAgreements(String vendorCode) {
        List<VendorAgreementDTO> agreements = new ArrayList<VendorAgreementDTO>();
        for (VendorAgreement agreement : vendorDao.getValidVendorAgreements(vendorCode)) {
            VendorAgreementDTO dto = new VendorAgreementDTO();
            dto.setId(agreement.getId());
            dto.setName(agreement.getName());
            agreements.add(dto);
        }
        return agreements;
    }

    @Override
    public List<VendorItemTypeDTO> lookupVendorItemTypes(String keyword, String vendorCode) {
        List<VendorItemTypeDTO> vendorItemTypes = new ArrayList<VendorItemTypeDTO>();
        for (VendorItemType vendorItemType : vendorDao.lookupVendorItemTypes(keyword, vendorCode)) {
            VendorItemTypeDTO vendorItemTypeDTO = prepareVendorItemTypeDTO(vendorItemType);
            vendorItemTypes.add(vendorItemTypeDTO);
        }
        return vendorItemTypes;
    }

    @Override
    public ActivateVendorAgreementResponse approveVendorAgreement(ActivateVendorAgreementRequest request) {
        ActivateVendorAgreementResponse response = new ActivateVendorAgreementResponse();
        ValidationContext context = request.validate();
        VendorAgreement vendorAgreement = null;
        if (!context.hasErrors()) {
            vendorAgreement = vendorDao.getVendorAgreementById(request.getVendorAgreementId());
            if (vendorAgreement == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_AGREEMENT_ID);
            } else if (!VendorAgreement.StatusCode.CREATED.name().equals(vendorAgreement.getStatusCode())) {
                context.addError(WsResponseCode.INVALID_VENDOR_AGREEMENT_STATUS);
            } else {
                vendorAgreement.setStatusCode(VendorAgreement.StatusCode.ACTIVE.name());
                vendorAgreement = vendorDao.updateAgreement(vendorAgreement);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.vendor.IVendorService#getBestVendorForItemTypeId(java.lang.Integer)
     */
    @Override
    public Vendor getBestVendorForItemTypeId(Integer itemTypeId) {
        VendorItemType vendorItemType = vendorDao.getBestVendorItemTypeByItemTypeId(itemTypeId);
        return vendorItemType != null ? vendorItemType.getVendor() : null;
    }

    @Override
    public VendorItemType getVendorItemType(String vendorCode, String itemTypeSkuCode) {
        return vendorDao.getVendorItemType(vendorCode, itemTypeSkuCode);
    }

    @Override
    @Transactional(readOnly = true)
    public VendorAgreement getVendorAgreement(Integer vendorId, String agreementName) {
        return vendorDao.getVendorAgreement(vendorId, agreementName);
    }

    @Override
    public Vendor getVendorByUserId(Integer userId) {
        return vendorDao.getVendorByUserId(userId);
    }

    @Override
    public List<Vendor> getVendors() {
        return vendorDao.getVendors();
    }

    @Override
    @Transactional
    public SignupVendorResponse signupVendor(SignupVendorRequest request) {
        SignupVendorResponse response = new SignupVendorResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Vendor vendor = getVendorByCode(request.getVendorCode());
            if (vendor.getUser() != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Vendor is already registered");
            } else {
                CreateUserRequest userRequest = new CreateUserRequest();
                userRequest.setUsername(request.getEmail());
                userRequest.setName(request.getName());
                userRequest.setMobile(request.getMobile());
                userRequest.setEmail(request.getEmail());
                CreateUserResponse userResponse = usersService.createUser(userRequest);
                if (userResponse.isSuccessful()) {
                    AddUserRolesRequest addUserRolesRequest = new AddUserRolesRequest();
                    addUserRolesRequest.getFacilityCodes().add(vendor.getFacility().getCode());
                    addUserRolesRequest.getFacilityRoleCodes().add("VENDOR");
                    addUserRolesRequest.setUsername(request.getEmail());
                    AddUserRolesResponse addUserRolesResponse = usersService.addUserRoles(addUserRolesRequest);
                    if (addUserRolesResponse.isSuccessful()) {
                        User user = usersService.getUserByUsername(request.getEmail());
                        vendor.setUser(user);
                        response.setSuccessful(true);
                    } else {
                        response.setErrors(addUserRolesResponse.getErrors());
                    }
                } else {
                    response.setErrors(userResponse.getErrors());
                }
            }
        }

        response.addErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<Vendor> getAllVendors() {
        return vendorDao.getAllVendors();
    }

    @Override
    @Transactional
    public List<Vendor> getAllVendorsForTenant() {
        return vendorDao.getAllVendorsForTenant();
    }

    @Override
    @Transactional
    public List<VendorAgreementStatus> getVendorsAgreementStatuses() {
        return vendorDao.getVendorsAgreementStatuses();
    }

    @Override
    public CreateOrEditVendorItemTypeResponse createOrEditVendorItemType(CreateOrEditVendorItemTypeRequest request) {
        CreateOrEditVendorItemTypeResponse response = new CreateOrEditVendorItemTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
            response.setSuccessful(false);
        } else {
            VendorItemType vendorItemType = vendorDao.getVendorItemType(request.getVendorItemType().getVendorCode(), request.getVendorItemType().getItemTypeSkuCode());
            if (vendorItemType == null) {
                CreateVendorItemTypeRequest createRequest = new CreateVendorItemTypeRequest();
                createRequest.setVendorItemType(request.getVendorItemType());
                CreateVendorItemTypeResponse createResponse = createVendorItemType(createRequest);
                response.setErrors(createResponse.getErrors());
                response.setSuccessful(createResponse.isSuccessful());
            } else {
                EditVendorItemTypeRequest editRequest = new EditVendorItemTypeRequest();
                editRequest.setVendorItemType(request.getVendorItemType());
                editRequest.setUserId(request.getUserId());
                editRequest.setActionType("CreateOrEditAPI");
                EditVendorItemTypeResponse editResponse = editVendorItemType(editRequest);
                response.addErrors(editResponse.getErrors());
                response.setSuccessful(editResponse.isSuccessful());
            }
        }
        return response;
    }

    @RollbackOnFailure
    @Override
    @Transactional
    public CreateOrEditVendorResponse createOrEditVendor(CreateOrEditVendorRequest request) {
        CreateOrEditVendorResponse response = new CreateOrEditVendorResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            WsGenericVendor genericVendor = new WsGenericVendor(request.getVendor());
            Vendor vendor = vendorDao.getVendorByCode(request.getVendor().getCode());
            if (vendor == null) {
                CreateVendorRequest createVendorRequest = new CreateVendorRequest();
                createVendorRequest.setVendor(genericVendor);
                CreateVendorResponse createVendorResponse = new CreateVendorResponse();
                createVendorInternal(createVendorRequest, createVendorResponse, false);
                response.setErrors(createVendorResponse.getErrors());
                response.setSuccessful(createVendorResponse.isSuccessful());
            } else {
                EditVendorRequest editVendorRequest = new EditVendorRequest();
                editVendorRequest.setVendor(genericVendor);
                EditVendorResponse editVendorResponse = editVendor(editVendorRequest);
                response.setErrors(editVendorResponse.getErrors());
                response.setSuccessful(editVendorResponse.isSuccessful());
            }
        }
        return response;
    }

    /**
     * adds partyCode, addressType to addresses before validating request, temporary, should be removed when all Party
     * creation/editing are moved to new services
     * 
     * @param customer
     */
    private void updateRequestBeforeValidation(WsGenericVendor vendor) {
        if (vendor.getCode() != null) {
            WsPartyAddress bAddress = vendor.getBillingAddress();
            if (bAddress != null) {
                bAddress.setPartyCode(vendor.getCode());
                bAddress.setAddressType(PartyAddressType.Code.BILLING.name());
            }
            WsPartyAddress sAddress = vendor.getShippingAddress();
            if (sAddress != null) {
                sAddress.setPartyCode(vendor.getCode());
                sAddress.setAddressType(PartyAddressType.Code.SHIPPING.name());
            }
            if (vendor.getPartyContacts() != null) {
                for (WsPartyContact contact : vendor.getPartyContacts()) {
                    contact.setPartyCode(vendor.getCode());
                }
            }
            if (vendor.getVendorAgreements() != null) {
                for (WsVendorAgreement vendorAgreement : vendor.getVendorAgreements()) {
                    vendorAgreement.setVendorCode(vendor.getCode());
                }
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<VendorItemType> getVendorItemTypesBySkuCodes(Integer vendorId, Set<String> skuCodes) {
        return vendorDao.getVendorItemTypes(vendorId, skuCodes);
    }
}
