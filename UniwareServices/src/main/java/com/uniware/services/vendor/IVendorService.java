/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.services.vendor;

import java.util.List;
import java.util.Set;

import com.unifier.core.api.user.SignupVendorRequest;
import com.unifier.core.api.user.SignupVendorResponse;
import com.uniware.core.api.party.ActivateVendorAgreementRequest;
import com.uniware.core.api.party.ActivateVendorAgreementResponse;
import com.uniware.core.api.party.AddOrEditVendorAgreementRequest;
import com.uniware.core.api.party.AddOrEditVendorAgreementResponse;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeRequest;
import com.uniware.core.api.party.CreateOrEditVendorItemTypeResponse;
import com.uniware.core.api.party.CreateOrEditVendorRequest;
import com.uniware.core.api.party.CreateOrEditVendorResponse;
import com.uniware.core.api.party.CreateVendorAgreementRequest;
import com.uniware.core.api.party.CreateVendorAgreementResponse;
import com.uniware.core.api.party.CreateVendorItemTypeRequest;
import com.uniware.core.api.party.CreateVendorItemTypeResponse;
import com.uniware.core.api.party.CreateVendorRequest;
import com.uniware.core.api.party.CreateVendorResponse;
import com.uniware.core.api.party.EditVendorItemTypeRequest;
import com.uniware.core.api.party.EditVendorItemTypeResponse;
import com.uniware.core.api.party.EditVendorRequest;
import com.uniware.core.api.party.EditVendorResponse;
import com.uniware.core.api.party.GetVendorItemTypesRequest;
import com.uniware.core.api.party.GetVendorItemTypesResponse;
import com.uniware.core.api.party.GetVendorRequest;
import com.uniware.core.api.party.GetVendorResponse;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.api.party.SearchVendorItemTypesResponse;
import com.uniware.core.api.party.SearchVendorRequest;
import com.uniware.core.api.party.SearchVendorResponse;
import com.uniware.core.api.party.dto.VendorAgreementDTO;
import com.uniware.core.api.party.dto.VendorDTO;
import com.uniware.core.api.party.dto.VendorItemTypeDTO;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.api.warehouse.SearchItemTypesResponse;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorAgreementStatus;
import com.uniware.core.entity.VendorItemType;

public interface IVendorService {

    SearchVendorResponse searchVendor(SearchVendorRequest request);

    Vendor getVendorByCode(String code);

    Vendor getVendorById(int vendorId);

    CreateVendorResponse createVendor(CreateVendorRequest request);

    EditVendorResponse editVendor(EditVendorRequest request);

    GetVendorResponse getVendorByCode(GetVendorRequest request);

    CreateVendorAgreementResponse createAgreement(CreateVendorAgreementRequest request);

    AddOrEditVendorAgreementResponse addOrEditAgreement(AddOrEditVendorAgreementRequest request);

    SearchVendorItemTypesResponse searchVendorItemType(SearchVendorItemTypesRequest request);

    List<VendorDTO> listVendors();

    EditVendorItemTypeResponse editVendorItemType(EditVendorItemTypeRequest request);

    /**
     * @param vendorAgreementId
     * @return
     */
    VendorAgreement getVendorAgreementById(int vendorAgreementId);

    SearchItemTypesResponse searchItemTypesByKeyword(SearchItemTypesRequest request);

    CreateVendorItemTypeResponse createVendorItemType(CreateVendorItemTypeRequest request);

    List<VendorAgreementDTO> getValidVendorAgreements(String vendorCode);

    List<VendorItemTypeDTO> lookupVendorItemTypes(String keyword, String vendorCode);

    ActivateVendorAgreementResponse approveVendorAgreement(ActivateVendorAgreementRequest request);

    /**
     * @param key
     * @return
     */
    Vendor getBestVendorForItemTypeId(Integer itemTypeId);

    VendorItemType getVendorItemType(String vendorCode, String vendorSkuCode);

    /**
     * @param id
     * @param agreementName
     * @return
     */
    VendorAgreement getVendorAgreement(Integer vendorId, String agreementName);

    /**
     * @param id
     * @return
     */
    Vendor getVendorByUserId(Integer id);

    /**
     * @param request
     * @return
     */
    SignupVendorResponse signupVendor(SignupVendorRequest request);

    /**
     * @param request
     * @return
     */

    List<Vendor> getVendors();

    List<Vendor> getAllVendors();

    List<VendorAgreementStatus> getVendorsAgreementStatuses();

    CreateOrEditVendorItemTypeResponse createOrEditVendorItemType(CreateOrEditVendorItemTypeRequest request);

    CreateOrEditVendorResponse createOrEditVendor(CreateOrEditVendorRequest request);

    GetVendorItemTypesResponse getVendorItemTypes(GetVendorItemTypesRequest request);

    List<Vendor> getAllVendorsForTenant();

    List<VendorItemType> getVendorItemTypesBySkuCodes(Integer vendorId, Set<String> skuCodes);
}
