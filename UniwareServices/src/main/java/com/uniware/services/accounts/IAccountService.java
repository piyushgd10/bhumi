/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2015
 *  @author akshay
 */
package com.uniware.services.accounts;

import com.unifier.core.api.myaccount.CreateAccountResponse;
import com.unifier.core.api.myaccount.CreateInviteRequest;
import com.unifier.core.api.myaccount.CreateInviteResponse;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderRequest;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderResponse;
import com.unifier.core.api.myaccount.GetAccountAlertsRequest;
import com.unifier.core.api.myaccount.GetAccountAlertsResponse;
import com.unifier.core.api.myaccount.GetAccountBalanceRequest;
import com.unifier.core.api.myaccount.GetAccountBalanceResponse;
import com.unifier.core.api.myaccount.GetInvitesByReferrerRequest;
import com.unifier.core.api.myaccount.GetInvitesByReferrerResponse;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryResponse;
import com.unifier.core.api.myaccount.GetPaymentHistoryRequest;
import com.unifier.core.api.myaccount.GetPaymentHistoryResponse;
import com.unifier.core.api.myaccount.GetProductApplicabilityRequest;
import com.unifier.core.api.myaccount.GetProductApplicabilityResponse;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsRequest;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsResponse;
import com.unifier.core.api.myaccount.GetSubscriptionsRequest;
import com.unifier.core.api.myaccount.GetSubscriptionsResponse;
import com.unifier.core.api.myaccount.GetUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetUsageHistoryResponse;
import com.unifier.core.api.myaccount.PipeToAccountsRequest;
import com.unifier.core.api.myaccount.ResendInviteRequest;
import com.unifier.core.api.myaccount.ResendInviteResponse;
import com.uniware.core.api.tenant.CreateTenantRequest;

public interface IAccountService {

    GetAccountBalanceResponse getAccountBalance(GetAccountBalanceRequest request);

    GetPaymentHistoryResponse getPaymentHistory(GetPaymentHistoryRequest request);

    GetUsageHistoryResponse getUsageHistory(GetUsageHistoryRequest request);

    GetAccountAlertsResponse getAccountAlerts(GetAccountAlertsRequest request);

    GetRecentUsageStatisticsResponse getRecentUsageStatistics(GetRecentUsageStatisticsRequest request);

    CreatePaymentSaleOrderResponse createSaleOrder(CreatePaymentSaleOrderRequest request);

    CreateAccountResponse createAccount(CreateTenantRequest request);

    CreateInviteResponse createInvite(CreateInviteRequest request);

    GetInvitesByReferrerResponse getAllInvitesByReferrer(GetInvitesByReferrerRequest request);

    ResendInviteResponse resendInvite(ResendInviteRequest request);

    GetProductApplicabilityResponse getProductApplicability(GetProductApplicabilityRequest request);

    String pipeToAccounts(PipeToAccountsRequest request);

    GetSubscriptionsResponse getSubscriptions(GetSubscriptionsRequest request);

    GetMonthlyUsageHistoryResponse getMonthlyUsageHistory(GetMonthlyUsageHistoryRequest request);
}
