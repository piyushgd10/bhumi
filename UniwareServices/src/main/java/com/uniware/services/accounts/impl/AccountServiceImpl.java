/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2015
 *  @author akshay
 */
package com.uniware.services.accounts.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unifier.core.api.myaccount.CreateAccountResponse;
import com.unifier.core.api.myaccount.CreateInviteRequest;
import com.unifier.core.api.myaccount.CreateInviteResponse;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderRequest;
import com.unifier.core.api.myaccount.CreatePaymentSaleOrderResponse;
import com.unifier.core.api.myaccount.GetAccountAlertsRequest;
import com.unifier.core.api.myaccount.GetAccountAlertsResponse;
import com.unifier.core.api.myaccount.GetAccountBalanceRequest;
import com.unifier.core.api.myaccount.GetAccountBalanceResponse;
import com.unifier.core.api.myaccount.GetInvitesByReferrerRequest;
import com.unifier.core.api.myaccount.GetInvitesByReferrerResponse;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetMonthlyUsageHistoryResponse;
import com.unifier.core.api.myaccount.GetPaymentHistoryRequest;
import com.unifier.core.api.myaccount.GetPaymentHistoryResponse;
import com.unifier.core.api.myaccount.GetProductApplicabilityRequest;
import com.unifier.core.api.myaccount.GetProductApplicabilityResponse;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsRequest;
import com.unifier.core.api.myaccount.GetRecentUsageStatisticsResponse;
import com.unifier.core.api.myaccount.GetSubscriptionsRequest;
import com.unifier.core.api.myaccount.GetSubscriptionsResponse;
import com.unifier.core.api.myaccount.GetUsageHistoryRequest;
import com.unifier.core.api.myaccount.GetUsageHistoryResponse;
import com.unifier.core.api.myaccount.PipeToAccountsRequest;
import com.unifier.core.api.myaccount.ResendInviteRequest;
import com.unifier.core.api.myaccount.ResendInviteResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Product.Type;
import com.uniware.services.accounts.IAccountService;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.tenant.ITenantService;

@Service("accountService")
public class AccountServiceImpl implements IAccountService {

    private static final Logger   LOG                            = LoggerFactory.getLogger(AccountServiceImpl.class);

    private static final String   ACCOUNT_DETAILS_SCRIPT         = "accountDetailsScript";
    private static final String   PAYMENT_HISTORY_SCRIPT         = "paymentHistoryScript";
    private static final String   USAGE_HISTORY_SCRIPT           = "usageHistoryScript";
    private static final String   ACCOUNT_ALERTS_SCRIPT          = "accountAlertsScript";
    private static final String   RECENT_USAGE_STATISTICS_SCRIPT = "recentUsageStatisticsScript";
    private static final String   CREATE_SALE_ORDER_SCRIPT       = "createSaleOrderScript";
    private static final String   CREATE_ACCOUNT_SCRIPT          = "createAccountScript";
    private static final String   CREATE_INVITE_SCRIPT           = "createInviteScript";
    private static final String   GET_REFERRER_INVITES_SCRIPT    = "getReferrerInvitesScript";
    private static final String   RESEND_INVITE_SCRIPT           = "resendInviteScript";
    private static final String   PRODUCT_APPLICABILITY_SCRIPT   = "productApplicabilityScript";
    private static final String   GET_SUBSCRIPTIONS_SCRIPT       = "getSubscriptionsScript";
    private static final String   PIPE_TO_ACCOUNTS_SCRIPT        = "pipeToAccountsScript";
    private static final String   MONTHLY_USAGE_HISTORY_SCRIPT   = "monthlyUsageHistoryScript";

    @Autowired
    private ITenantService        tenantService;

    @Autowired
    private ITenantProfileService tenantProfileService;

    @Autowired
    private IEmailService         emailService;

    @Override
    @Transactional
    public GetAccountBalanceResponse getAccountBalance(GetAccountBalanceRequest request) {
        GetAccountBalanceResponse response = new GetAccountBalanceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                ScraperScript accountBalanceScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(ACCOUNT_DETAILS_SCRIPT);
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                accountBalanceScript.execute();
                response = new GsonBuilder().setDateFormat("yyyy-MM-dd").create().fromJson(resultItems.get("response").toString(), GetAccountBalanceResponse.class);
                // response.setPackageType(resultItems.get("packageType").toString());
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetPaymentHistoryResponse getPaymentHistory(GetPaymentHistoryRequest request) {
        GetPaymentHistoryResponse response = new GetPaymentHistoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript paymentHistoryScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(PAYMENT_HISTORY_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                seContext.addVariable("start", request.getStart());
                seContext.addVariable("noOfResults", request.getNoOfResults());
                paymentHistoryScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetPaymentHistoryResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetUsageHistoryResponse getUsageHistory(GetUsageHistoryRequest request) {
        GetUsageHistoryResponse response = new GetUsageHistoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript usageHistoryScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(USAGE_HISTORY_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                seContext.addVariable("daterange", request.getDaterange());
                usageHistoryScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetUsageHistoryResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetMonthlyUsageHistoryResponse getMonthlyUsageHistory(GetMonthlyUsageHistoryRequest request) {
        GetMonthlyUsageHistoryResponse response = new GetMonthlyUsageHistoryResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript monthlyUsageHistoryScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(MONTHLY_USAGE_HISTORY_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                seContext.addVariable("daterange", request.getDaterange());
                seContext.addVariable("request", request);
                monthlyUsageHistoryScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetMonthlyUsageHistoryResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetAccountAlertsResponse getAccountAlerts(GetAccountAlertsRequest request) {
        GetAccountAlertsResponse response = new GetAccountAlertsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript accountAlertsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(ACCOUNT_ALERTS_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                accountAlertsScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetAccountAlertsResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetRecentUsageStatisticsResponse getRecentUsageStatistics(GetRecentUsageStatisticsRequest request) {
        GetRecentUsageStatisticsResponse response = new GetRecentUsageStatisticsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript accountAlertsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(RECENT_USAGE_STATISTICS_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                accountAlertsScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetRecentUsageStatisticsResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public CreatePaymentSaleOrderResponse createSaleOrder(CreatePaymentSaleOrderRequest request) {
        CreatePaymentSaleOrderResponse response = new CreatePaymentSaleOrderResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript accountAlertsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(CREATE_SALE_ORDER_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("saleOrder", request.getSaleOrder());
                seContext.addVariable("referrerCode", request.getReferrerCode());
                seContext.addVariable("resultItems", resultItems);
                accountAlertsScript.execute();
                response.setSuccessful(true);
                response.setRedirectUrl(resultItems.get("redirectUrl").toString());
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public CreateAccountResponse createAccount(CreateTenantRequest request) {
        ValidationContext context = new ValidationContext();
        CreateAccountResponse response = new CreateAccountResponse();
        String accountCode = null;
        try {
            ScraperScript createAccountScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(CREATE_ACCOUNT_SCRIPT);
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            Map<String, Object> resultItems = new HashMap<String, Object>();
            String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
            seContext.addVariable("accountsUrl", url);
            seContext.addVariable("resultItems", resultItems);
            seContext.addVariable("request", request);
            seContext.addVariable("profile", tenantProfileService.getTenantProfileByCode(request.getCode()));
            createAccountScript.execute();
            accountCode = (String) resultItems.get("accountCode");
        } catch (Exception e) {
            context.addError(WsResponseCode.ERROR_SETTING_UP_TENANT, e.getMessage());
            LOG.error("Error in creating account for tenant", e);
        }
        if (!context.hasErrors()) {
            if (request.getEmail() != null && request.getTenantType().equals(Type.LITE.name())) {
                sendWelcomeEmail(request);
            }
            response.setSuccessful(true);
            response.setAccountCode(accountCode);
        } else {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    private void sendWelcomeEmail(CreateTenantRequest request) {
        List<String> recipients = new ArrayList<String>();
        recipients.add(request.getEmail());
        try {
            LOG.info("Sending {} to {}", EmailTemplate.Type.WELCOME_EMAIL.name(), request.getEmail());
            EmailMessage message = new EmailMessage(recipients, EmailTemplate.Type.WELCOME_EMAIL.name());
            message.addTemplateParam("user", request.getCode());
            message.addTemplateParam("accessUrl", request.getAccessUrl());
            message.addTemplateParam("profile", tenantProfileService.getTenantProfileByCode(request.getCode()));
            message.addAttachmentUrl("https://mc.ucdn.in/documents/Unicommerce_Order_Processing_Manual.pdf");
            message.addAttachmentUrl("https://mc.ucdn.in/documents/standard_declaration_for_unicommerce_lite.pdf");
            emailService.send(message);
            LOG.info("Email successfully sent");
        } catch (Exception e) {
            LOG.error("Error sending WELCOME EMAIL to " + request.getEmail(), e);
        }
    }

    @Override
    public CreateInviteResponse createInvite(CreateInviteRequest request) {
        CreateInviteResponse response = new CreateInviteResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript createInviteScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(CREATE_INVITE_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("request", request);
                seContext.addVariable("resultItems", resultItems);
                createInviteScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), CreateInviteResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetInvitesByReferrerResponse getAllInvitesByReferrer(GetInvitesByReferrerRequest request) {
        GetInvitesByReferrerResponse response = new GetInvitesByReferrerResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript getReferrerInvitesScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(GET_REFERRER_INVITES_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("request", request);
                seContext.addVariable("resultItems", resultItems);
                getReferrerInvitesScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), GetInvitesByReferrerResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public ResendInviteResponse resendInvite(ResendInviteRequest request) {
        ResendInviteResponse response = new ResendInviteResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript getReferrerInvitesScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(RESEND_INVITE_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("request", request);
                seContext.addVariable("resultItems", resultItems);
                getReferrerInvitesScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), ResendInviteResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetProductApplicabilityResponse getProductApplicability(GetProductApplicabilityRequest request) {
        GetProductApplicabilityResponse response = new GetProductApplicabilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript productApplicabilityScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(PRODUCT_APPLICABILITY_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                productApplicabilityScript.execute();
                response = new GsonBuilder().setDateFormat("YYYY-mm-dd").create().fromJson(resultItems.get("response").toString(), GetProductApplicabilityResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    /**
     * @param request
     * @return Transfers request to accounts server and returns response
     */
    @Override
    public String pipeToAccounts(PipeToAccountsRequest request) {
        ValidationContext context = request.validate();
        String response = null;
        if (!context.hasErrors()) {
            try {
                ScraperScript pipeToAccountsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(PIPE_TO_ACCOUNTS_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<>();
                String accountsUrl = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", accountsUrl);
                seContext.addVariable("resultItems", resultItems);
                seContext.addVariable("request", request);
                pipeToAccountsScript.execute();
                response = resultItems.get("response").toString();
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        return response;
    }

    @Override
    public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsRequest request) {
        GetSubscriptionsResponse response = new GetSubscriptionsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScraperScript getSubscriptionsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(GET_SUBSCRIPTIONS_SCRIPT);
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                Map<String, Object> resultItems = new HashMap<String, Object>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAccountsServerUrl();
                seContext.addVariable("accountsUrl", url);
                seContext.addVariable("resultItems", resultItems);
                getSubscriptionsScript.execute();
                response = new GsonBuilder().setDateFormat("YYYY-mm-dd").create().fromJson(resultItems.get("response").toString(), GetSubscriptionsResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }
}
