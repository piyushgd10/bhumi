/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 13, 2015
 *  @author harsh
 */
package com.uniware.core.api.purchaseOrder.display;

import java.util.Date;
import java.util.List;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.uniware.core.api.party.PartyAddressDTO;

/**
 * @author harsh
 */
public class GetPurchaseOrderSummaryResponse extends ServiceResponse {

    private static final long serialVersionUID = 3042616268516452906L;

    PurchaseOrderSummaryDTO   purchaseOrderSummary;

    public PurchaseOrderSummaryDTO getPurchaseOrderSummary() {
        return purchaseOrderSummary;
    }

    public void setPurchaseOrderSummary(PurchaseOrderSummaryDTO purchaseOrderSummary) {
        this.purchaseOrderSummary = purchaseOrderSummary;
    }

    public static class PurchaseOrderSummaryDTO {

        private String                       purchaseOrderCode;
        private String                       vendorCode;
        private String                       vendorName;
        private String                       statusCode;
        private String                       type;
        private String                       fromParty;
        private String                       amendedFromPurchaseOrderCode;
        private String                       vendorAgreement;
        private Date                         created;
        private String                       createdBy;
        private Date                         expiryDate;
        private Date                         deliveryDate;
        private int                          totalQuantity;
        private int                          rejectedQuantity;
        private int                          pendingQuantity;
        private PartyAddressDTO              partyAddress;
        private List<CustomFieldMetadataDTO> customFieldValues;

        public String getPurchaseOrderCode() {
            return purchaseOrderCode;
        }

        public void setPurchaseOrderCode(String purchaseOrderCode) {
            this.purchaseOrderCode = purchaseOrderCode;
        }

        public String getVendorCode() {
            return vendorCode;
        }

        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getFromParty() {
            return fromParty;
        }

        public void setFromParty(String fromParty) {
            this.fromParty = fromParty;
        }

        public String getAmendedFromPurchaseOrderCode() {
            return amendedFromPurchaseOrderCode;
        }

        public void setAmendedFromPurchaseOrderCode(String amendedFromPurchaseOrderCode) {
            this.amendedFromPurchaseOrderCode = amendedFromPurchaseOrderCode;
        }

        public String getVendorAgreement() {
            return vendorAgreement;
        }

        public void setVendorAgreement(String vendorAgreement) {
            this.vendorAgreement = vendorAgreement;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public Date getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(Date expiryDate) {
            this.expiryDate = expiryDate;
        }

        public Date getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(Date deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public int getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(int totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        public int getRejectedQuantity() {
            return rejectedQuantity;
        }

        public void setRejectedQuantity(int rejectedQuantity) {
            this.rejectedQuantity = rejectedQuantity;
        }

        public int getPendingQuantity() {
            return pendingQuantity;
        }

        public void setPendingQuantity(int pendingQuantity) {
            this.pendingQuantity = pendingQuantity;
        }

        public PartyAddressDTO getPartyAddress() {
            return partyAddress;
        }

        public void setPartyAddress(PartyAddressDTO partyAddress) {
            this.partyAddress = partyAddress;
        }

        public List<CustomFieldMetadataDTO> getCustomFieldValues() {
            return customFieldValues;
        }

        public void setCustomFieldValues(List<CustomFieldMetadataDTO> customFieldValues) {
            this.customFieldValues = customFieldValues;
        }

    }

}
