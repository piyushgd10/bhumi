package com.uniware.events;

import org.springframework.context.ApplicationEvent;

/**
 * @author rachit.
 *         An event triggered whenever a billing party is disabled.
 *         BillingPartyService publishes this event to ChannelService which
 *         listens to this event and removes the disabled billing party
 *         from all the channels.
 */
public class BillingPartyDisableEvent extends ApplicationEvent {

    private String message;

    public BillingPartyDisableEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MessageEvent [message=").append(message).append("]");
        builder.append(" from " + source.getClass().getName());
        return builder.toString();
    }
}
