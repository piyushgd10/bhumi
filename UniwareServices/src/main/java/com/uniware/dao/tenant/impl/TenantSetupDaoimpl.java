/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Apr-2014
 *  @author harsh
 */
package com.uniware.dao.tenant.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.cache.InformationSchemaCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.TenantSetupState;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.tenant.ITenantSetupDao;

@Repository
@SuppressWarnings("unchecked")
public class TenantSetupDaoimpl implements ITenantSetupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<InformationSchemaDTO> getInformationSchema() {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
                "select TABLE_NAME, COLUMN_NAME, DATA_TYPE from information_schema.COLUMNS where TABLE_SCHEMA = 'uniware' order by ORDINAL_POSITION");
        List<InformationSchemaDTO> resultList = new ArrayList<InformationSchemaDTO>();
        ScrollableResults results = query.scroll();
        while (results.next()) {
            resultList.add(new InformationSchemaDTO((String) results.get(0), (String) results.get(1), (String) results.get(2)));

        }
        return resultList;
    }

    @Override
    public List<TenantSetupState> getSetupStatusMappings() {
        return sessionFactory.getCurrentSession().createQuery("from TenantSetupState where enabled = 1 order by position").list();
    }

    @Override
    public ApiUser getApiUserForTenantByUsername(Integer tenantId, String username) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ApiUser where tenant.id = :tenantId and username = :username");
        query.setParameter("username", username);
        query.setParameter("tenantId", tenantId);
        return (ApiUser) query.uniqueResult();
    }

    @Override
    public User getUserForTenantByUsername(Integer tenantId, String username) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User where tenant.id = :tenantId and username = :username");
        query.setParameter("username", username);
        query.setParameter("tenantId", tenantId);
        return (User) query.uniqueResult();
    }

    @Override
    public boolean tenantSetupUpdate(String table, Integer tenantId, String condition) {
        table = table.toLowerCase();
        StringBuilder insert = new StringBuilder();
        StringBuilder select = new StringBuilder();
        select.append("select ");
        insert.append("insert into ").append(table).append(" ( ");

        InformationSchemaCache cache = CacheManager.getInstance().getCache(InformationSchemaCache.class);
        List<String> columns = cache.getColumns(table);
        if (columns.size() == 0)
            return false;
        String prefix = "";
        for (String column : columns) {
            //don't include id
            if (column.equals("id"))
                continue;
            else {
                insert.append(prefix).append(column);
                //updated and created set to now()
                if ("updated".equals(column) || "created".equals(column)) {
                    select.append(prefix).append("now()");
                } else if ("tenant_id".equals(column)) {
                    select.append(prefix).append(":toTenant");
                } else {
                    select.append(prefix).append(column);
                }
                prefix = ", ";
            }

        }
        select.append(" from ").append(table).append(" where tenant_id = :fromTenant");
        if (StringUtils.isNotBlank(condition)) {
            select.append(" and ").append(condition);
        }
        return executeUpdate(insert.append(") ").append(select).toString(), tenantId);

    }

    @Override
    public boolean tenantSetupUpdate(String table, String referencedTable, String joinKey, Integer tenantId) {
        table = table.toLowerCase();
        StringBuilder insert = new StringBuilder();
        StringBuilder select = new StringBuilder();
        String alias1 = " t1";
        String alias21 = " t21";
        String alias22 = " t22";
        select.append("select ");
        insert.append("insert into ").append(table).append(" ( ");
        String prefix = "";
        InformationSchemaCache cache = CacheManager.getInstance().getCache(InformationSchemaCache.class);
        List<String> columns = cache.getColumns(table);
        for (String column : columns) {
            if (column.equals("id"))
                continue;
            else {
                insert.append(prefix).append(column);
                if ("updated".equals(column) || "created".equals(column)) {
                    select.append(prefix).append("now()");
                } else if ("tenant_id".equals(column)) {
                    select.append(prefix).append(":toTenant");
                } else if ((referencedTable + "_id").equals(column)) {
                    select.append(prefix).append(alias22).append(".id");
                } else {
                    select.append(prefix).append(alias1).append(".").append(column);
                }
                prefix = ", ";
            }

        }
        insert.append(") ");
        select.append(" from ").append(table).append(alias1).append(", ").append(referencedTable).append(alias21).append(", ").append(referencedTable).append(alias22).append(
                " where ").append(alias21).append(".tenant_id = :fromTenant and ").append(alias1).append(".").append(referencedTable).append("_id = ").append(alias21).append(
                ".id and ").append(alias21).append(".").append(joinKey).append(" = ").append(alias22).append(".").append(joinKey).append(" and ").append(alias22).append(
                ".tenant_id = :toTenant");
        return executeUpdate(insert.append(select).toString(), tenantId);
    }

    @Override
    public boolean executeUpdate(String sql, Integer tenantId) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        if (sql.contains(":toTenant")) {
            query.setInteger("toTenant", tenantId);
        }
        if (sql.contains(":fromTenant")) {
            query.setInteger("fromTenant", UserContext.current().getTenant().getId());
        }
        return query.executeUpdate() > 0;
    }

    @Override
    @Transactional
    public Tenant updateTenant(Tenant tenant) {
        return (Tenant) sessionFactory.getCurrentSession().merge(tenant);
    }

}
