/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.tenant.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.TenantSource;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.TenantSetupLog;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.Tenant.StatusCode;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.tenant.ITenantDao;

@Repository
@SuppressWarnings("unchecked")
public class TenantDaoImpl implements ITenantDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public Tenant createTenant(Tenant tenant) {
        sessionFactory.getCurrentSession().persist(tenant);
        return tenant;
    }

    @Override
    public Tenant getTenantByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Tenant t where t.code = :code");
        query.setParameter("code", code);
        return (Tenant) query.uniqueResult();
    }

    @Override
    public boolean cleanupTenantData(Integer tenantId, boolean clearAWBs) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("CALL tenant_go_live(:tenant_id, :clear_awb)");
        query.setParameter("tenant_id", tenantId);
        query.setParameter("clear_awb", clearAWBs);
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean updateTenantStatus(StatusCode status) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("update tenant set status_code = :statusCode where code = :code");
        query.setParameter("code", UserContext.current().getTenant().getCode());
        query.setParameter("statusCode", status.name());
        return query.executeUpdate() > 0;
    }

    @Override
    public TenantSource addTenantSource(TenantSource ts) {
        sessionFactory.getCurrentSession().persist(ts);
        return ts;
    }

    @Override
    public TenantSource getTenantSource(String sourceCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from TenantSource ts where ts.sourceCode = :sourceCode and ts.tenant.id = :tenantId");
        q.setParameter("sourceCode", sourceCode);
        q.setParameter("tenantId", UserContext.current().getTenantId());
        return (TenantSource) q.uniqueResult();
    }

    @Override
    public List<TenantSource> getAllTenantSources() {
        Query q = sessionFactory.getCurrentSession().createQuery("from TenantSource ts where ts.tenant.id = :tenantId");
        q.setParameter("tenantId", UserContext.current().getTenantId());
        return q.list();
    }

    @Override
    public void addTenantSetupLog(TenantSetupLog tenantSetupLog){
        Criteria criteria = Criteria.where("request.code").is(tenantSetupLog.getRequest().getCode());
        mongoOperations.remove(new org.springframework.data.mongodb.core.query.Query(criteria), TenantSetupLog.class);
        mongoOperations.save(tenantSetupLog);
    }

    @Override
    public void updateTenantSetupLog(CreateTenantRequest request, CreateTenantResponse response){
        org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query(Criteria.where("request.code").is(request.getCode()));
        Update update = new Update().set("response", response);
        mongoOperations.updateFirst(query, update, TenantSetupLog.class);
    }

    @Override
    public List<TenantSetupLog> getTenantSetupLogStatus(String tenantCode){
        Criteria criteria = Criteria.where("request.code").is(tenantCode);
        org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query(criteria);
        return mongoOperations.find(query, TenantSetupLog.class);
    }

    @Override
    public void updateTenantReactivationDate(Date reactivation_date){
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("update tenant set last_reactivation_date = :reactivation_date where code = :code");
        query.setParameter("code", UserContext.current().getTenant().getCode());
        query.setParameter("reactivation_date", reactivation_date);
        query.executeUpdate();
    }




}
