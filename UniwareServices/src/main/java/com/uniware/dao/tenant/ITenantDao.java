/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.tenant;

import com.unifier.core.entity.TenantSource;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.api.tenant.TenantSetupLog;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.Tenant.StatusCode;

import java.util.Date;
import java.util.List;

public interface ITenantDao {

    Tenant createTenant(Tenant tenant);

    boolean cleanupTenantData(Integer tenantId, boolean clearAWBs);

    Tenant getTenantByCode(String code);

    TenantSource addTenantSource(TenantSource ts);

    TenantSource getTenantSource(String code);

    List<TenantSource> getAllTenantSources();

    boolean updateTenantStatus(StatusCode status);

    void addTenantSetupLog(TenantSetupLog request);

    void updateTenantSetupLog(CreateTenantRequest request, CreateTenantResponse response);

    List<TenantSetupLog> getTenantSetupLogStatus(String tenantCode);

    void updateTenantReactivationDate(Date reactivation_date);
}
