package com.uniware.dao.tenant;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.unifier.core.utils.DateUtils.DateRange;

public interface ITenantInvoiceDao {

    BigDecimal getGroupedReturnShipmentsCount(DateRange daterange, Integer perOrderItems);

    BigDecimal getGroupedDispatchedShipmentsCount(DateRange daterange, Integer perOrderItems);

    BigInteger getReversePickupShipmentsCount(DateRange daterange);

    BigInteger getReversePickupItemsCount(DateRange daterange);

    BigInteger getReturnedShipmentsCount(DateRange daterange);

    BigInteger getReturnedItemsCount(DateRange daterange);

    BigInteger getDispatchedShipmentsCount(DateRange daterange);

    BigInteger getDispatchedItemsCount(DateRange daterange);

    BigInteger getTenantActiveWarehouses();

    BigDecimal getGroupedReversePickupItemsCount(DateRange daterange, Integer perOrderItems);

}
