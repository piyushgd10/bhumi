/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */
package com.uniware.dao.tenant.impl;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.tenant.ITenantInvoiceDao;

@Repository
public class TenantInvoiceDaoImpl implements ITenantInvoiceDao {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public BigInteger getDispatchedItemsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(soi.id) dispatched_items from sale_order so, shipping_package sp, sale_order_item soi, tenant t where soi.shipping_package_id = sp.id and sp.dispatch_time > :startDate and sp.dispatch_time < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigInteger getDispatchedShipmentsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(distinct(sp.id)) dispatched_shipment from sale_order so, shipping_package sp, sale_order_item soi, tenant t where soi.shipping_package_id = sp.id and sp.dispatch_time > :startDate and sp.dispatch_time < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigInteger getReturnedItemsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(soi.id) as returned_items from sale_order so, return_manifest_item rmi, sale_order_item soi, tenant t where soi.sale_order_id = so.id and soi.shipping_package_id = rmi.shipping_package_id and rmi.created > :startDate and rmi.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigInteger getReturnedShipmentsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(distinct(rmi.id)) return_shipment from sale_order so, return_manifest_item rmi, sale_order_item soi, tenant t where soi.sale_order_id = so.id and soi.shipping_package_id = rmi.shipping_package_id and rmi.created > :startDate and rmi.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigInteger getReversePickupItemsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(soi.id) as reverse_pickup_items from sale_order so, sale_order_item soi, reverse_pickup rp, tenant t where soi.sale_order_id = so.id and soi.reverse_pickup_id is not null and soi.reverse_pickup_id = rp.id and rp.status_code = 'COMPLETE' and rp.created > :startDate and rp.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigInteger getReversePickupShipmentsCount(DateRange daterange) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(distinct(rp.id)) as reverse_pickup_shipment from sale_order so, sale_order_item soi, reverse_pickup rp, tenant t where soi.sale_order_id = so.id and soi.reverse_pickup_id is not null and soi.reverse_pickup_id = rp.id and rp.status_code = 'COMPLETE' and rp.created > :startDate and rp.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        return (BigInteger) query.uniqueResult();
    }
    
    @Override
    public BigDecimal getGroupedDispatchedShipmentsCount(DateRange daterange, Integer perOrderItems) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select sum(t.dispatched_shipment) from (select (floor(count(soi.id)/:perOrderItems)+1) as dispatched_shipment from sale_order so, shipping_package sp, sale_order_item soi, tenant t where soi.shipping_package_id = sp.id and sp.dispatch_time > :startDate and sp.dispatch_time < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId) t");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        query.setParameter("perOrderItems", perOrderItems);
        return (BigDecimal) query.uniqueResult();
    }
    
    @Override
    public BigDecimal getGroupedReturnShipmentsCount(DateRange daterange, Integer perOrderItems) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select sum(t.return_shipment) from (select count(soi.id) as returned_items, (floor(count(soi.id)/:perOrderItems)+1) return_shipment, t.code tenant from sale_order so, return_manifest_item rmi, sale_order_item soi, tenant t where soi.sale_order_id = so.id and soi.shipping_package_id = rmi.shipping_package_id and rmi.created > :startDate and rmi.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId) t");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        query.setParameter("perOrderItems", perOrderItems);
        return (BigDecimal) query.uniqueResult();
    }
    
    @Override
    public BigDecimal getGroupedReversePickupItemsCount(DateRange daterange, Integer perOrderItems) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery("select (floor(count(soi.id)/:perOrderItems)+1) as reverse_pickup_items from sale_order so, sale_order_item soi, reverse_pickup rp, tenant t where soi.sale_order_id = so.id and soi.reverse_pickup_id is not null and soi.reverse_pickup_id = rp.id and rp.status_code = 'COMPLETE' and rp.created > :startDate and rp.created < :endDate and soi.sale_order_id = so.id and so.tenant_id = t.id and t.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("startDate", daterange.getStart());
        query.setParameter("endDate", daterange.getEnd());
        query.setParameter("perOrderItems", perOrderItems);
        return (BigDecimal) query.uniqueResult();
    }
    
    @Override
    public BigInteger getTenantActiveWarehouses() {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(" select count(f.id) from facility f join party p on f.id = p.id where p.tenant_id = :tenantId and p.enabled = 1");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (BigInteger) query.uniqueResult();
    }
}
