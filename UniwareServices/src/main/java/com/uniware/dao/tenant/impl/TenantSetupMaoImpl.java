/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2014
 *  @author harsh
 */
package com.uniware.dao.tenant.impl;

import com.unifier.services.vo.TenantSetupPropertyVO;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.ScriptVersionVO;
import com.uniware.dao.tenant.ITenantSetupMao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class TenantSetupMaoImpl implements ITenantSetupMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    MongoOperations commonMongoOperations;

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    MongoOperations mongoOperations;

    @Override
    public TenantSetupPropertyVO getTenantSetupProperty() {
        return commonMongoOperations.findOne(new Query(), TenantSetupPropertyVO.class);
    }

    @Override
    public void insertSamplePrintTemplate(SamplePrintTemplateVO template) {
        if (template.isGlobal()) commonMongoOperations.insert(template);
        else mongoOperations.insert(template);
    }

    @Override
    public void insertPrintTemplate(PrintTemplateVO template) {
        mongoOperations.save(template);
    }

    @Override
    public List<SamplePrintTemplateVO> getSamplePrintTemplates() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),SamplePrintTemplateVO.class);
    }

    @Override
    public List<SamplePrintTemplateVO> getDefaultSamplePrintTemplates() {
        return mongoOperations.find(new Query(Criteria.where("useAsDefault").is(Boolean.TRUE).and("tenantCode").is(UserContext.current().getTenant().getCode())),SamplePrintTemplateVO.class);
    }

    @Override
    public List<SamplePrintTemplateVO> getDefaultGlobalSamplePrintTemplates() {
        return commonMongoOperations.find(new Query(Criteria.where("enabled").is(Boolean.TRUE).and("useAsDefault").is(Boolean.TRUE).and("global").is(Boolean.TRUE)),SamplePrintTemplateVO.class);
    }

    @Override
    public void insertScriptVersion(ScriptVersionVO scriptVersion) {
        commonMongoOperations.insert(scriptVersion);
    }

    @Override
    public void removeSamplePrintTemplates(String tenantCode) {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(tenantCode)), SamplePrintTemplateVO.class);
    }

    @Override
    public void removePrintTemplates(String tenantCode) {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(tenantCode)), PrintTemplateVO.class);
    }

}
