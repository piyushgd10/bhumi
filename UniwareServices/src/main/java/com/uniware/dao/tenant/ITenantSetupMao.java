/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Feb-2014
 *  @author harsh
 */
package com.uniware.dao.tenant;

import java.util.List;

import com.unifier.services.vo.TenantSetupPropertyVO;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.core.vo.ScriptVersionVO;

public interface ITenantSetupMao {

    void insertSamplePrintTemplate(SamplePrintTemplateVO template);

    void insertPrintTemplate(PrintTemplateVO template);

    List<SamplePrintTemplateVO> getDefaultSamplePrintTemplates();

    List<SamplePrintTemplateVO> getSamplePrintTemplates();

    TenantSetupPropertyVO getTenantSetupProperty();

    void removeSamplePrintTemplates(String tenantCode);

    void removePrintTemplates(String tenantCode);

    List<SamplePrintTemplateVO> getDefaultGlobalSamplePrintTemplates();

    void insertScriptVersion(ScriptVersionVO scriptVersion);

}
