/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Apr-2014
 *  @author harsh
 */
package com.uniware.dao.tenant;

import java.util.List;

import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.uniware.core.api.metadata.InformationSchemaDTO;
import com.uniware.core.entity.Tenant;
import com.uniware.core.entity.TenantSetupState;

public interface ITenantSetupDao {

    ApiUser getApiUserForTenantByUsername(Integer tenantId, String username);

    List<InformationSchemaDTO> getInformationSchema();

    List<TenantSetupState> getSetupStatusMappings();

    boolean executeUpdate(String sql, Integer tenantId);

    /**
     * Selects rows from "table" for base tenant and copies them for the new tenant with id "tenantId". Excludes column
     * "id" from INSERT and sets columns "created" and "updated" to "now()"
     *
     * @param table Table to insert updates into
     * @param tenantId id for the tenant for which data is to be inserted
     * @param condition any extra condition to be applied
     * @return boolean true if some rows were inserted, else false
     */
    boolean tenantSetupUpdate(String table, Integer tenantId, String condition);

    /**
     * Selects rows joining "table" and "referencedTable" having foreign key reference, for base tenant and copies them
     * for the new tenant with id "tenantId". Excludes column "id" from INSERT and sets columns "created" and "updated"
     * to "now()"
     *
     * @param table Table to insert updates into
     * @param referencedTable
     * @param joinKey
     * @param tenantId id for the tenant for which data is to be inserted
     * @return boolean true if some rows were inserted, else false
     */
    boolean tenantSetupUpdate(String table, String referencedTable, String joinKey, Integer tenantId);

    public Tenant updateTenant(Tenant tenant);

    /**
     * @param tenantId
     * @param username
     * @return
     */
    User getUserForTenantByUsername(Integer tenantId, String username);

}
