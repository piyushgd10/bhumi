/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.customer.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Customer;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.customer.ICustomerDao;

@Repository
public class CustomerDaoImpl implements ICustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Customer getCustomerByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Customer where code = :code and tenant.id = :tenantId");
        query.setParameter("code", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (Customer) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Customer> getCustomerByCodeEmailOrMobile(String key) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select c from Customer as c left join c.partyContacts as pc left join c.partyAddresses as pa where "
                        + "(c.code like :key or c.name like :key or pc.email like :key or pc.phone like :key or pa.phone like :key) and c.tenant.id = :tenantId");
        query.setParameter("key", "%" + key + "%");
        query.setMaxResults(10);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public Customer createCustomer(Customer customer) {
        customer.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(customer);
        return customer;
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        sessionFactory.getCurrentSession().merge(customer);
        return customer;
    }

    @Override
    public Customer getCustomerById(int customerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Customer where id = :customerId and tenant.id = :tenantId");
        query.setParameter("customerId", customerId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (Customer) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Customer> getCustomersByCode(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Customer where tenant.id = :tenantId and code like :keyword");
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

}
