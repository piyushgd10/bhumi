/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.customer;

import java.util.List;

import com.uniware.core.entity.Customer;

public interface ICustomerDao {

    Customer getCustomerByCode(String code);

    Customer createCustomer(Customer vendor);

    Customer updateCustomer(Customer vendor);

    Customer getCustomerById(int vendorId);

    List<Customer> getCustomersByCode(String code);

	List<Customer> getCustomerByCodeEmailOrMobile(String key);
}
