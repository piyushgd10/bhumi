/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.catalog;

import java.util.Collection;
import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeRequest;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.ItemDetailField;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Tag;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;

/**
 * @author singla
 */
public interface ICatalogDao {

    ItemType getItemTypeBySkuCode(String skuCode);

    ItemType getItemTypeById(int itemTypeId);

    Category getCategoryById(int categoryId);

    Section getSectionById(int sectionId);

    ItemType createItemType(ItemType itemType);

    Vendor createVendor(Vendor vendor);

    VendorItemType createVendorItemType(VendorItemType vendorItemType);

    Category createCategory(Category category);

    ItemTypeInventory createItemTypeInventory(ItemTypeInventory itemInventoryType);

    /**
     * @param vendorId
     * @param itemTypeId
     * @return
     */
    VendorItemType getVendorItemType(int vendorId, int itemTypeId);

    /**
     * @param name
     * @return
     */
    Tag addTag(Tag tag);

    List<Tag> getAllTags();

    int removeTag(String name);

    ItemType updateItemType(ItemType itemType);

    Section getSectionByCode(String sectionCode);

    Category getCategoryByCode(String code);

    Category updateCategory(Category category);

    List<Category> listCategories();

    List<Section> listSections();

    /**
     * @param keyword
     * @return
     */
    Long getItemTypeCount(SearchItemTypesRequest request);

    List<ItemType> searchItemTypes(SearchItemTypesRequest request);

    Tag getTagByName(String name);

    List<Tag> getTagByNames(Collection<String> names);

    /**
     * @param id
     * @return
     */
    List<VendorItemType> getVendorItemTypeByItemTypeId(Integer id);

    /**
     * @param keyword
     * @return
     */
    List<ItemType> lookupItemTypes(String keyword, int noOfResults);

    List<ItemType> lookupAllItemTypes(String keyword, int noOfResults);

    List<ItemType> lookupExactItemTypes(String keyword, int noOfResults);

    List<ItemType> getItemTypes(int start, int noOfRecords, DateRange updatedIn);

    VendorItemType getVendorItemType(String vendorCode, Integer itemTypeId);

    List<VendorItemType> getVendorItemTypeByItemTypeIdByTenant(Integer itemTypeId);

    List<FacilityItemType> getFacilityItemTypesByItemTypeId(Integer itemTypeId);

    void addFacilityItemType(FacilityItemType FacilityItemType);

    List<FacilityItemType> searchFacilityItemType(SearchFacilityItemTypeRequest request);

    Long getFacilityItemTypeCount(SearchFacilityItemTypeRequest request);

    FacilityItemType getFacilityItemType(String itemSku, Integer facilityId);

    void createFacilityItemType(FacilityItemType facilityItemType);

    List<Category> getCategories();

    List<ItemDetailField> getItemDetailFields();

    ItemType getItemTypeByScanIdentifier(String scanIdentifier);

    ItemType getAllItemTypeBySkuCode(String skuCode);

    ItemType getAllItemTypeById(int itemTypeId);

    ItemType getAllEnabledItemTypeById(int itemTypeId);

    ItemType getEnabledItemTypeById(int itemTypeId);

    ItemType getEnabledItemTypeBySkuCode(String skuCode);

    ItemType getAllEnabledItemTypeBySkuCode(String skuCode);

    FacilityItemType updateFacilityItemType(FacilityItemType facilityItemType);

    List<Category> lookupCategories(String keyword);

    long getItemTypeCount();

    List<ItemType> getItemTypesByExpiryStatus(boolean expirable, int start, int pageSize);

    List<ItemType> getAllItemTypes(int start, int batchSize);
}
