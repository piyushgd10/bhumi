/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.catalog.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.warehouse.SearchFacilityItemTypeRequest;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.entity.Category;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityItemType;
import com.uniware.core.entity.ItemDetailField;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Tag;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.catalog.ICatalogDao;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class CatalogDaoImpl implements ICatalogDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ItemType getAllItemTypeBySkuCode(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select it from ItemType it join fetch it.category where it.tenant.id = :tenantId and it.skuCode = :skuCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("skuCode", skuCode);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public long getItemTypeCount() {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(it) from ItemType it  where it.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return ((Long) query.uniqueResult());
    }

    @Override
    public List<ItemType> getItemTypesByExpiryStatus(boolean expirable, int start, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category c where it.tenant.id = :tenantId and (it.expirable =:expirable or (it.expirable is null and c.expirable =:expirable))");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("expirable", expirable);
        query.setFirstResult(start);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @Override
    public List<ItemType> getAllItemTypes(int start, int batchSize) {
        Query q = sessionFactory.getCurrentSession().createQuery("from ItemType where tenant.id = :tenantId order by id");
        q.setParameter("tenantId", UserContext.current().getTenantId());
        q.setFirstResult(start);
        q.setMaxResults(batchSize);
        return q.list();
    }

    @Override
    public ItemType getAllEnabledItemTypeBySkuCode(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category where it.tenant.id = :tenantId and it.skuCode = :skuCode and it.enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("enabled", true);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getItemTypeBySkuCode(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category where it.tenant.id = :tenantId and it.skuCode = :skuCode and it.type != :type");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("type", ItemType.Type.BUNDLE);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getEnabledItemTypeBySkuCode(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category where it.tenant.id = :tenantId and it.skuCode = :skuCode and it.type != :type and it.enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("enabled", true);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getAllItemTypeById(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select it from ItemType it join fetch it.category c where  it.tenant.id = :tenantId and it.id = :itemTypeId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getAllEnabledItemTypeById(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category c where  it.tenant.id = :tenantId and it.id = :itemTypeId and it.enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("enabled", true);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getItemTypeById(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category c where it.tenant.id = :tenantId and it.id = :itemTypeId and it.type != :type");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("type", ItemType.Type.BUNDLE);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public ItemType getEnabledItemTypeById(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category c where  it.tenant.id = :tenantId and it.id = :itemTypeId and it.type != :type and it.enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("enabled", true);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public Category getCategoryById(int categoryId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Category where tenant.id = :tenantId and id = :categoryId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("categoryId", categoryId);
        return (Category) query.uniqueResult();
    }

    @Override
    public Section getSectionById(int sectionId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where facility.id = :facilityId and id = :sectionId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("sectionId", sectionId);
        return (Section) query.uniqueResult();
    }

    @Override
    public ItemType createItemType(ItemType itemType) {
        itemType.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(itemType);
        return itemType;
    }

    @Override
    public ItemType updateItemType(ItemType itemType) {
        sessionFactory.getCurrentSession().merge(itemType);
        return itemType;
    }

    @Override
    public Vendor createVendor(Vendor vendor) {
        vendor.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(vendor);
        return vendor;
    }

    @Override
    public VendorItemType createVendorItemType(VendorItemType vendorItemType) {
        sessionFactory.getCurrentSession().persist(vendorItemType);
        return vendorItemType;
    }

    @Override
    public VendorItemType getVendorItemType(int vendorId, int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorItemType where vendor.facility.id = :facilityId and enabled = 1 and  itemType.id = :itemTypeId and vendor.id = :vendorId order by priority desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("vendorId", vendorId);
        query.setParameter("itemTypeId", itemTypeId);
        return (VendorItemType) query.uniqueResult();
    }

    @Override
    public Category createCategory(Category category) {
        category.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(category);
        return category;
    }

    @Override
    public ItemTypeInventory createItemTypeInventory(ItemTypeInventory itemInventoryType) {
        itemInventoryType.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(itemInventoryType);
        return itemInventoryType;
    }

    @Override
    public Tag addTag(Tag tag) {
        tag.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(tag);
        return tag;
    }

    @Override
    public List<Tag> getAllTags() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Tag where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public int removeTag(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from Tag where tenant.id = :tenantId and name = :name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name);
        return query.executeUpdate();
    }

    @Override
    public Long getItemTypeCount(SearchItemTypesRequest request) {
        Criteria criteria = getSearchItemTypeCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public Section getSectionByCode(String sectionCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where facility.id = :facilityId and code = :sectionCode");
        query.setParameter("sectionCode", sectionCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Section) query.uniqueResult();
    }

    @Override
    public Category getCategoryByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Category c where tenant.id = :tenantId and c.code = :code");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", code);
        return (Category) query.uniqueResult();
    }

    @Override
    public Category updateCategory(Category category) {
        category.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().merge(category);
        return category;
    }

    @Override
    public List<Category> listCategories() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Category where tenant.id = :tenantId order by name asc");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<Section> listSections() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ItemType> searchItemTypes(SearchItemTypesRequest request) {
        Criteria criteria = getSearchItemTypeCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    public Criteria getSearchItemTypeCriteria(SearchItemTypesRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemType.class);
        criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
        criteria.createCriteria("category", "c");
        if (StringUtils.isNotEmpty(request.getKeyword())) {
            SimpleExpression se1 = Restrictions.like("name", "%" + request.getKeyword() + "%");
            SimpleExpression se2 = Restrictions.like("color", "%" + request.getKeyword() + "%");
            SimpleExpression se3 = Restrictions.like("description", "%" + request.getKeyword() + "%");
            SimpleExpression se4 = Restrictions.like("brand", "%" + request.getKeyword() + "%");
            SimpleExpression se5 = Restrictions.like("c.name", "%" + request.getKeyword() + "%");
            criteria.add(Restrictions.or(se1, Restrictions.or(se2, Restrictions.or(se3, Restrictions.or(se4, se5)))));
        }
        if (StringUtils.isNotBlank(request.getCategoryCode())) {
            criteria.add(Restrictions.eq("c.code", request.getCategoryCode()));
        }
        if (StringUtils.isNotBlank(request.getProductCode())) {
            criteria.add(Restrictions.eq("skuCode", request.getProductCode()));
        }

        if (request.getGetInventorySnapshot() != null && request.getGetInventorySnapshot()) {
            criteria.createCriteria("itemTypeInventorySnapshots");
        }

        return criteria;
    }

    @Override
    public Tag getTagByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Tag where tenant.id = :tenantId and name = :name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name);
        return (Tag) query.uniqueResult();
    }

    @Override
    public List<Tag> getTagByNames(Collection<String> names) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Tag where tenant.id = :tenantId and name in (:names)");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("names", names);
        return query.list();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.dao.catalog.ICatalogDao#getVendorItemTypeByItemTypeId(java.
     * lang.Integer)
     */
    @Override
    public List<VendorItemType> getVendorItemTypeByItemTypeId(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorItemType where vendor.facility.id = :facilityId and enabled = 1 and  itemType.id = :itemTypeId and vendor.enabled=1 order by priority desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public List<ItemType> lookupItemTypes(String keyword, int noOfResults) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemType where tenant.id = :tenantId and (name like :keyword or skuCode like :keyword or scanIdentifier like :keyword) and type != :type and enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("enabled", true);
        query.setMaxResults(noOfResults);
        return query.list();
    }

    @Override
    public List<ItemType> lookupAllItemTypes(String keyword, int noOfResults) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemType where tenant.id = :tenantId and (name like :keyword or skuCode like :keyword or scanIdentifier like :keyword) and enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("enabled", true);
        query.setMaxResults(noOfResults);
        return query.list();
    }

    @Override
    public List<ItemType> lookupExactItemTypes(String keyword, int noOfResults) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemType where tenant.id = :tenantId and (skuCode = :keyword or scanIdentifier = :keyword) and enabled = :enabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("keyword", keyword);
        query.setParameter("enabled", true);
        query.setMaxResults(noOfResults);
        return query.list();
    }

    @Override
    public List<ItemType> getItemTypes(int start, int noOfRecords, DateRange updatedIn) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemType it join fetch it.category left join fetch it.taxType where it.tenant.id = :tenantId and it.updated > :updatedInFrom and it.updated < :updatedInTo");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("updatedInFrom", updatedIn.getStart());
        query.setParameter("updatedInTo", updatedIn.getEnd());
        query.setFirstResult(start);
        query.setMaxResults(noOfRecords);
        return query.list();
    }

    @Override
    public VendorItemType getVendorItemType(String vendorCode, Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorItemType where vendor.facility.id = :facilityId and enabled = 1 and  itemType.id = :itemTypeId and vendor.code = :vendorCode order by priority desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("vendorCode", vendorCode);
        query.setParameter("itemTypeId", itemTypeId);
        return (VendorItemType) query.uniqueResult();
    }

    @Override
    public List<VendorItemType> getVendorItemTypeByItemTypeIdByTenant(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vit from VendorItemType vit join fetch vit.vendor v where v.facility.tenant.id = :tenantId and vit.enabled = 1 and  vit.itemType.id = :itemTypeId and v.enabled=1 order by priority desc, vit.leadTime, v.leadTime");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public List<FacilityItemType> getFacilityItemTypesByItemTypeId(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from FacilityItemType fit where fit.enabled = 1 and fit.itemType.id = :itemTypeId and (inventory = -1 or inventory > 0) order by priority desc");
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public void addFacilityItemType(FacilityItemType FacilityItemType) {
        sessionFactory.getCurrentSession().persist(FacilityItemType);
    }

    @Override
    public FacilityItemType updateFacilityItemType(FacilityItemType facilityItemType) {
        return (FacilityItemType) sessionFactory.getCurrentSession().merge(facilityItemType);
    }

    @Override
    public List<FacilityItemType> searchFacilityItemType(SearchFacilityItemTypeRequest request) {
        Criteria criteria = getSearchFacilityItemTypeCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }

        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public Long getFacilityItemTypeCount(SearchFacilityItemTypeRequest request) {
        Criteria criteria = getSearchFacilityItemTypeCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    public Criteria getSearchFacilityItemTypeCriteria(SearchFacilityItemTypeRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(FacilityItemType.class);
        Criteria itemTypeCriteria = criteria.createCriteria("itemType");
        Criteria facilityCriteria = criteria.createCriteria("facility");

        criteria.addOrder(Order.desc("created"));
        facilityCriteria.add(Restrictions.eq("type", Facility.Type.DROPSHIP.name()));
        itemTypeCriteria.createCriteria("tenant").add(Restrictions.eq("id", UserContext.current().getTenantId()));

        if (StringUtils.isNotBlank(request.getItemTypeKeyword())) {
            String keyword = request.getItemTypeKeyword().trim();
            SimpleExpression se1 = Restrictions.like("name", "%" + keyword + "%");
            SimpleExpression se2 = Restrictions.like("skuCode", "%" + keyword + "%");
            SimpleExpression se3 = Restrictions.like("description", "%" + keyword + "%");
            itemTypeCriteria.add(Restrictions.or(se1, Restrictions.or(se2, se3)));
        }

        if (StringUtils.isNotBlank(request.getFacilityCode())) {
            facilityCriteria.add(Restrictions.eq("code", request.getFacilityCode()));
        }

        return criteria;
    }

    @Override
    public FacilityItemType getFacilityItemType(String itemSku, Integer facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from FacilityItemType fit where fit.itemType.skuCode = :itemSku and fit.facility.id = :facilityId and fit.itemType.tenant.id = :tenantId");
        query.setParameter("itemSku", itemSku);
        query.setParameter("facilityId", facilityId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (FacilityItemType) query.uniqueResult();
    }

    @Override
    public void createFacilityItemType(FacilityItemType facilityItemType) {
        sessionFactory.getCurrentSession().persist(facilityItemType);
    }

    @Override
    public List<Category> getCategories() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Category where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<ItemDetailField> getItemDetailFields() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemDetailField where enabled = 1 and tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public ItemType getItemTypeByScanIdentifier(String scanIdentifier) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select it from ItemType it join fetch it.category left join fetch it.facilityItemTypes where it.tenant.id = :tenantId and it.scanIdentifier = :scanIdentifier");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("scanIdentifier", scanIdentifier);
        return (ItemType) query.uniqueResult();
    }

    @Override
    public List<Category> lookupCategories(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Category where tenant.id = :tenantId and name like :keyword");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("keyword", "%" + keyword + "%");
        return query.list();
    }
}
