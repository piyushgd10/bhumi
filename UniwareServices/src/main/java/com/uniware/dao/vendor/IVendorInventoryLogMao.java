/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Apr-2014
 *  @author harsh
 */
package com.uniware.dao.vendor;

import java.util.Collection;
import java.util.List;

import com.uniware.core.vo.VendorInventoryLogVO;

public interface IVendorInventoryLogMao {
    public List<VendorInventoryLogVO> getVendorInventoryLogBySkuCode(String itemSku);

    public void create(VendorInventoryLogVO log);

    void createNew(Collection<VendorInventoryLogVO> inventoryLogs);
}
