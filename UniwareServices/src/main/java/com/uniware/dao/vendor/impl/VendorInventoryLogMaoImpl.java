/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Apr-2014
 *  @author harsh
 */
package com.uniware.dao.vendor.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.VendorInventoryLogVO;
import com.uniware.dao.vendor.IVendorInventoryLogMao;

@Repository
public class VendorInventoryLogMaoImpl implements IVendorInventoryLogMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<VendorInventoryLogVO> getVendorInventoryLogBySkuCode(String itemSku) {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("itemSku").is(itemSku)), VendorInventoryLogVO.class);
    }

    @Override
    public void create(VendorInventoryLogVO inventoryLog) {
        inventoryLog.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(inventoryLog);
    }

    @Override
    public void createNew(Collection<VendorInventoryLogVO> inventoryLogs) {
        for (VendorInventoryLogVO inventoryLog : inventoryLogs) {
            inventoryLog.setTenantCode(UserContext.current().getTenant().getCode());
        }
        mongoOperations.insertAll(inventoryLogs);
    }

}
