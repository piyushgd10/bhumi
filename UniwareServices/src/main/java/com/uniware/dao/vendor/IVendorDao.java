/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.vendor;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorAgreementStatus;
import com.uniware.core.entity.VendorItemType;

public interface IVendorDao {

    List<Vendor> searchVendor(String keyword);

    Vendor getVendorByCode(String code);

    Vendor createVendor(Vendor vendor);

    Vendor updateVendor(Vendor vendor);

    Vendor getVendorById(int vendorId);

    VendorAgreement createAgreement(VendorAgreement vendorAgreement);

    VendorAgreement updateAgreement(VendorAgreement vendorAgreement);

    VendorAgreement getVendorAgreementById(int id);

    List<VendorItemType> getVendorItemTypes(SearchVendorItemTypesRequest request);

    List<Vendor> listVendors();

    VendorItemType getVendorItemTypeById(Integer integer);

    VendorItemType createVendorItemType(VendorItemType itemType);

    List<VendorAgreement> getValidVendorAgreements(String vendorCode);

    List<VendorItemType> lookupVendorItemTypes(String keyword, String vendorCode);

    /**
     * @param itemTypeId
     * @return
     */
    VendorItemType getBestVendorItemTypeByItemTypeId(Integer itemTypeId);

    Long getVendorItemTypeCount(SearchVendorItemTypesRequest request);

    VendorItemType getVendorItemType(String vendorCode, String itemTypeSkuCode);

    List<VendorItemType> getVendorItemTypes(Integer vendorId, Collection<Integer> itemTypeIds, boolean lock);

    VendorAgreement getVendorAgreement(Integer vendorId, String agreementName);

    Vendor getVendorByUserId(Integer userId);

    List<Vendor> getVendors();

    List<Vendor> getAllVendors();

    List<VendorAgreementStatus> getVendorsAgreementStatuses();

    VendorItemType getVendorItemType(Integer vendorId, Integer itemTypeId, boolean lock);

    VendorItemType updateVendorItemType(VendorItemType vendorItemType);

    List<VendorItemType> getVendorItemTypes(Integer vendorId, int start, int pageSize);

    long getVendorItemTypeCount(Integer vendorId);

    VendorAgreement save(VendorAgreement vendorAgreement);

    List<Vendor> getAllVendorsForTenant();

    List getVendorItemTypes(Integer vendorId, Set<String> skuCodes);
}
