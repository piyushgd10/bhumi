/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.vendor.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.party.SearchVendorItemTypesRequest;
import com.uniware.core.entity.Vendor;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorAgreementStatus;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.vendor.IVendorDao;

@Repository
public class VendorDaoImpl implements IVendorDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Vendor> searchVendor(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Vendor where facility.id = :facilityId and name like :keyword order by name asc");
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public Vendor getVendorByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Vendor v left join fetch v.vendorAgreements va left join fetch v.partyAddresses pa left join fetch pa.partyAddressType where v.facility.id = :facilityId and v.code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Vendor) query.uniqueResult();
    }

    @Override
    public Vendor createVendor(Vendor vendor) {
        vendor.setFacility(UserContext.current().getFacility());
        vendor.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(vendor);
        return vendor;
    }

    @Override
    public Vendor updateVendor(Vendor vendor) {
        vendor.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().merge(vendor);
        return vendor;
    }

    @Override
    public Vendor getVendorById(int vendorId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Vendor v left join fetch v.facility f left join fetch v.vendorAgreements va left join fetch v.partyAddresses pa left join fetch pa.partyAddressType where v.id = :vendorId");
        query.setParameter("vendorId", vendorId);
        return (Vendor) query.uniqueResult();
    }

    @Override
    public VendorAgreement createAgreement(VendorAgreement vendorAgreement) {
        sessionFactory.getCurrentSession().persist(vendorAgreement);
        return vendorAgreement;
    }

    @Override
    public VendorAgreement updateAgreement(VendorAgreement vendorAgreement) {
        sessionFactory.getCurrentSession().merge(vendorAgreement);
        return vendorAgreement;
    }

    @Override
    public VendorAgreement getVendorAgreementById(int vendorAgreementId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VendorAgreement where vendor.facility.id = :facilityId and id = :vendorAgreementId");
        query.setParameter("vendorAgreementId", vendorAgreementId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (VendorAgreement) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorItemType> getVendorItemTypes(SearchVendorItemTypesRequest request) {
        Criteria criteria = prepareVendorItemTypeCriteria(request);
        criteria.addOrder(Order.asc("priority"));
        criteria.setMaxResults(500);
        return criteria.list();
    }

    @Override
    public Long getVendorItemTypeCount(SearchVendorItemTypesRequest request) {
        Criteria criteria = prepareVendorItemTypeCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    private Criteria prepareVendorItemTypeCriteria(SearchVendorItemTypesRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(VendorItemType.class, "vit");
        criteria.createCriteria("vit.itemType", "it", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("vit.enabled", true));

        String name = request.getItemTypeName();
        if (StringUtils.isNotEmpty(name)) {
            criteria.add(Restrictions.or(Restrictions.like("vit.vendorSkuCode", "%" + name + "%"),
                    Restrictions.or(Restrictions.like("it.name", "%" + name + "%"), Restrictions.like("it.skuCode", "%" + name + "%"))));
        }

        Criteria vendorCriteria = criteria.createCriteria("vit.vendor");
        vendorCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            vendorCriteria.add(Restrictions.idEq(vendorId));
        }

        String vendorName = request.getVendorName();
        if (StringUtils.isNotEmpty(vendorName)) {
            vendorCriteria.add(Restrictions.like("name", "%" + vendorName + "%"));
        }
        return criteria;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Vendor> listVendors() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Vendor and facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public VendorItemType getVendorItemTypeById(Integer id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VendorItemType where facility.id = :facilityId and id=:id and enabled = 1");
        query.setParameter("id", id);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (VendorItemType) query.uniqueResult();
    }

    @Override
    public VendorItemType createVendorItemType(VendorItemType vendorItemType) {
        vendorItemType.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(vendorItemType);
        return vendorItemType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorAgreement> getValidVendorAgreements(String vendorCode) {
        Date currentTime = DateUtils.getCurrentTime();
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(VendorAgreement.class);
        Criteria vendorCriteria = criteria.createCriteria("vendor");
        vendorCriteria.add(Restrictions.eq("code", vendorCode));
        vendorCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.add(Restrictions.le("startTime", currentTime)).add(Restrictions.ge("endTime", currentTime));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorItemType> lookupVendorItemTypes(String keyword, String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorItemType where enabled = :enabled and facility.id = :facilityId and vendor.code=:vendorCode and itemType.enabled = :enabled and (itemType.name like :keyword or itemType.skuCode like :keyword) and vendor.enabled = :enabled");
        query.setParameter("enabled", true);
        query.setParameter("vendorCode", vendorCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("keyword", "%" + keyword + "%");
        query.setMaxResults(10);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.vendor.IVendorDao#getBestVendorItemTypeByItemTypeId(java.lang.Integer)
     */
    @Override
    public VendorItemType getBestVendorItemTypeByItemTypeId(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vit from VendorItemType vit where vit.facility.id = :facilityId and vit.enabled = 1 and vit.itemType.id = :itemTypeId order by vit.priority");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (VendorItemType) query.uniqueResult();
    }

    @Override
    public VendorItemType getVendorItemType(String vendorCode, String itemTypeSkuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorItemType where facility.id = :facilityId and vendor.code = :vendorCode and itemType.skuCode = :itemTypeSkuCode");
        query.setParameter("vendorCode", vendorCode);
        query.setParameter("itemTypeSkuCode", itemTypeSkuCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (VendorItemType) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorItemType> getVendorItemTypes(Integer vendorId, int start, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VendorItemType where facility.id = :facilityId and vendor.id = :vendorId");
        query.setParameter("vendorId", vendorId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(pageSize);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public long getVendorItemTypeCount(Integer vendorId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(*) from VendorItemType where facility.id = :facilityId and vendor.id = :vendorId");
        query.setParameter("vendorId", vendorId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Long) query.uniqueResult();
    }

    @Override
    public VendorItemType getVendorItemType(Integer vendorId, Integer itemTypeId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vit from VendorItemType vit where vit.facility.id = :facilityId and vit.vendor.id = :vendorId and vit.itemType.id = :itemTypeId");
        query.setParameter("vendorId", vendorId);
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("vit", LockMode.PESSIMISTIC_WRITE);
        }
        return (VendorItemType) query.uniqueResult();
    }

    @Override
    public List<VendorItemType> getVendorItemTypes(Integer vendorId, Collection<Integer> itemTypeIds, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vit from VendorItemType vit where vit.facility.id = :facilityId and vit.vendor.id = :vendorId and vit.itemType.id in (:itemTypeIds)");
        query.setParameter("vendorId", vendorId);
        query.setParameterList("itemTypeIds", itemTypeIds);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("vit", LockMode.PESSIMISTIC_WRITE);
        }
        return query.list();
    }

    @Override
    public VendorAgreement getVendorAgreement(Integer vendorId, String agreementName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from VendorAgreement where vendor.facility.id = :facilityId and vendor.id = :vendorId and name = :agreementName");
        query.setParameter("vendorId", vendorId);
        query.setParameter("agreementName", agreementName);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (VendorAgreement) query.uniqueResult();
    }

    @Override
    public Vendor getVendorByUserId(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Vendor where facility.id = :facilityId and  user.id = :userId");
        query.setParameter("userId", userId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Vendor) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Vendor> getVendors() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Vendor where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Vendor> getAllVendors() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct v from Vendor v left join fetch v.vendorAgreements va left join fetch v.partyAddresses pa left join fetch pa.partyAddressType where v.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Vendor> getAllVendorsForTenant() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct v from Vendor v left join fetch v.vendorAgreements va left join fetch v.partyAddresses pa left join fetch pa.partyAddressType where v.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorAgreementStatus> getVendorsAgreementStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from VendorAgreementStatus").list();
    }

    @Override
    public VendorItemType updateVendorItemType(VendorItemType vendorItemType) {
        return (VendorItemType) sessionFactory.getCurrentSession().merge(vendorItemType);
    }

    @Override
    public VendorAgreement save(VendorAgreement vendorAgreement) {
        sessionFactory.getCurrentSession().persist(vendorAgreement);
        return vendorAgreement;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VendorItemType> getVendorItemTypes(Integer vendorId, Set<String> skuCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vit from VendorItemType vit where vit.facility.id = :facilityId and vit.vendor.id = :vendorId and vit.itemType.skuCode in (:skuCodes)");
        query.setParameter("vendorId", vendorId);
        query.setParameterList("skuCodes", skuCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }
}
