package com.uniware.dao.taxtype;

import java.util.List;

import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;

public interface ITaxTypeDao {

    TaxTypeConfiguration createTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration);

    TaxType getTaxTypeByCode(String taxTypeCode);

    TaxTypeConfiguration getTaxTypeConfigurationByCode(String taxTypeCode, String stateCode);

    TaxTypeConfiguration editTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration);

    TaxType addTaxType(TaxType taxType);

    List<TaxType> getAllTaxTypes();

    @SuppressWarnings("unchecked") List<TaxType> getTaxTypes(boolean fetchGstOnly);

    List<TaxTypeConfiguration> getTaxtypeConfigurationsByTaxTypeCode(String taxTypeCode);

    TaxTypeConfiguration addTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration);

    int deleteTaxConfigurations(TaxType taxType);

    TaxType update(TaxType taxType);

}
