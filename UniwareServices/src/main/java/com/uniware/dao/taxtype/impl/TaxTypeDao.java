package com.uniware.dao.taxtype.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.TaxType;
import com.uniware.core.entity.TaxTypeConfiguration;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.taxtype.ITaxTypeDao;

@Repository
public class TaxTypeDao implements ITaxTypeDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public TaxTypeConfiguration createTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration) {
        sessionFactory.getCurrentSession().persist(taxTypeConfiguration);
        return taxTypeConfiguration;
    }

    @Override
    public TaxType getTaxTypeByCode(String taxTypeCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select taxType from TaxType taxType where tenant.id = :tenantId and taxType.code = :taxTypeCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("taxTypeCode", taxTypeCode);
        query.setMaxResults(1);
        return (TaxType) query.uniqueResult();
    }

    @Override
    public TaxTypeConfiguration getTaxTypeConfigurationByCode(String taxTypeCode, String stateCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ttc from TaxTypeConfiguration ttc join fetch ttc.taxType tt where tt.tenant.id = :tenantId and tt.code = :taxTypeCode and ttc.stateCode = :stateCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("taxTypeCode", taxTypeCode);
        query.setParameter("stateCode", stateCode);
        query.setMaxResults(1);
        return (TaxTypeConfiguration) query.uniqueResult();
    }

    @Override
    public TaxTypeConfiguration editTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration) {
        sessionFactory.getCurrentSession().merge(taxTypeConfiguration);
        return taxTypeConfiguration;
    }

    @Override
    public int deleteTaxConfigurations(TaxType taxType) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from TaxTypeConfiguration taxTypeConfiguration where taxTypeConfiguration.taxType.id = :taxTypeId");
        query.setParameter("taxTypeId", taxType.getId());
        return query.executeUpdate();
    }

    @Override
    public TaxType addTaxType(TaxType taxType) {
        taxType.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(taxType);
        return taxType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TaxType> getAllTaxTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct taxType from TaxType taxType left join fetch taxType.taxTypeConfigurations ttc left join fetch ttc.ranges where taxType.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TaxType> getTaxTypes(boolean fetchGstOnly) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct taxType from TaxType taxType left join fetch taxType.taxTypeConfigurations ttc left join fetch ttc.ranges where taxType.tenant.id = :tenantId and taxType.gst = :fetchGstOnly");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("fetchGstOnly", fetchGstOnly);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TaxTypeConfiguration> getTaxtypeConfigurationsByTaxTypeCode(String taxTypeCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from TaxTypeConfiguration where taxType.tenant.id = :tenantId and taxType.code = :taxTypeCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("taxTypeCode", taxTypeCode);
        return query.list();
    }

    @Override
    public TaxTypeConfiguration addTaxTypeConfiguration(TaxTypeConfiguration taxTypeConfiguration) {
        sessionFactory.getCurrentSession().persist(taxTypeConfiguration);
        return taxTypeConfiguration;
    }
    
    @Override
    public TaxType update(TaxType taxType) {
        return (TaxType) sessionFactory.getCurrentSession().merge(taxType);
    }

}
