/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.dao.broadcast.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.BroadcastMessageVO;
import com.uniware.dao.broadcast.IBroadcastMao;

@Repository
public class BroadcastMaoImpl implements IBroadcastMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<BroadcastMessageVO> getTenantBroadcastedMessages(Direction sortType, String sortColumn) {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
        query.with(new Sort(sortType, sortColumn));
        return mongoOperations.find(query, BroadcastMessageVO.class);
    }

    @Override
    public void deleteMessages(List<String> messages) {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).andOperator(Criteria.where("message").in(messages))),
                BroadcastMessageVO.class);

    }

    @Override
    public void save(BroadcastMessageVO broadcastMessageVO) {
        mongoOperations.save(broadcastMessageVO);
    }

}
