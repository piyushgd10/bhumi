/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Jan-2014
 *  @author akshay
 */
package com.uniware.dao.broadcast;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.uniware.core.vo.BroadcastMessageVO;


public interface IBroadcastMao {

    void deleteMessages(List<String> messages);

    List<BroadcastMessageVO> getTenantBroadcastedMessages(Direction sortType, String sortColumn);

    void save(BroadcastMessageVO broadcastMessageVO);

}
