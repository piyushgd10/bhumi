/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.dao.cms.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.annotation.Level;
import com.uniware.core.locking.Namespace;
import com.uniware.core.locking.annotation.Lock;
import com.uniware.core.locking.annotation.Locks;
import com.uniware.core.vo.ContentNamespaceVO;
import com.uniware.core.vo.ContentVO;
import com.uniware.dao.cms.IContentMao;
import com.uniware.services.exception.InvalidContentNamespaceException;

@Repository(value = "contentMao")
public class ContentMaoImpl implements IContentMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<ContentVO> getContent(String nsIdentifier, ContentVO.LanguageCode langCode) {
        List<ContentVO> voList = null;
        if (getContentNamespace(nsIdentifier) != null) {
            voList = mongoOperations.find(new Query(Criteria.where("nsIdentifier").is(nsIdentifier).and("languageCode").is(langCode)), ContentVO.class);
        }
        return voList;
    }

    @Override
    public ContentNamespaceVO getContentNamespace(String nsIdentifier) {
        return mongoOperations.findOne(new Query(Criteria.where("identifier").is(nsIdentifier)), ContentNamespaceVO.class);
    }

    @Override
    public ContentVO getContent(String nsIdentifier, ContentVO.LanguageCode langCode, String key) {
        ContentVO vo = null;
        if (getContentNamespace(nsIdentifier) != null) {
            Query q = new Query(Criteria.where("nsIdentifier").is(nsIdentifier).and("languageCode").is(langCode).and("key").is(key));
            vo = mongoOperations.findOne(q, ContentVO.class);
        }
        return vo;
    }

    @Override
    @Locks({ @Lock(ns = Namespace.CONTENT, key = "#{#args[0] + #args[1].name()}", level = Level.GLOBAL) })
    public void saveContent(String nsIdentifier, ContentVO.LanguageCode langCode, String key, String content) throws InvalidContentNamespaceException {
        if (getContentNamespace(nsIdentifier) == null) {
            throw new InvalidContentNamespaceException("Invalid Namespace: " + nsIdentifier);
        } else {
            ContentNamespaceVO nsVO = getContentNamespace(nsIdentifier);
            Integer currentVersion = nsVO.getVersionMap().get(langCode);
            if (currentVersion == null) {
                nsVO.getVersionMap().put(langCode, new Integer(1));
            } else {
                nsVO.getVersionMap().put(langCode, ++currentVersion);
                mongoOperations.save(nsVO);
            }
            ContentVO vo = getContent(nsIdentifier, langCode, key);
            if (vo == null) {
                vo = new ContentVO();
            }
            vo.setNsIdentifier(nsIdentifier);
            vo.setLanguageCode(langCode);
            vo.setKey(key);
            vo.setContent(content);
            mongoOperations.save(vo);
        }
    }
}
