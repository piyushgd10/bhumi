/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.uniware.dao.cms;

import java.util.List;

import com.uniware.core.vo.ContentNamespaceVO;
import com.uniware.core.vo.ContentVO;
import com.uniware.services.exception.InvalidContentNamespaceException;

public interface IContentMao {

    ContentNamespaceVO getContentNamespace(String nsIdentifier);

    List<ContentVO> getContent(String nsIdentifier, ContentVO.LanguageCode langCode);

    ContentVO getContent(String nsIdentifier, ContentVO.LanguageCode langCode, String key);

    void saveContent(String nsIdentifier, ContentVO.LanguageCode langCode, String key, String value) throws InvalidContentNamespaceException;
}
