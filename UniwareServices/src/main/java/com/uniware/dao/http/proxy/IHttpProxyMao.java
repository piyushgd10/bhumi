/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.dao.http.proxy;

import com.uniware.core.vo.HttpProxyVO;

public interface IHttpProxyMao {

    HttpProxyVO getNextAvailableProxy(String sourceCode, Integer threshold);

    void updateProxy(HttpProxyVO httpProxy);
}
