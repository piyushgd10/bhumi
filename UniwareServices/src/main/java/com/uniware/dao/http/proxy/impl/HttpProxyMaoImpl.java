/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.dao.http.proxy.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.uniware.core.vo.HttpProxyVO;
import com.uniware.dao.http.proxy.IHttpProxyMao;

@Repository
public class HttpProxyMaoImpl implements IHttpProxyMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public HttpProxyVO getNextAvailableProxy(String sourceCode,Integer threshold) {
        return mongoOperations.findAndModify(new Query(Criteria.where("sourceCode").is(sourceCode).and("used").is(false).and("usedCount").lt(threshold)), Update.update("threshold", threshold).inc("usedCount", 1), HttpProxyVO.class);
    }
    
    @Override
    public void updateProxy(HttpProxyVO httpProxy){
        mongoOperations.save(httpProxy);
    }

}
