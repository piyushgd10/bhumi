package com.uniware.dao.location;

import java.util.List;

import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResults;

import com.uniware.core.entity.Location;

/**
 * Created by sunny on 05/05/15.
 */
public interface ILocationMao {
//    void save(Location location);

    List<Location> getAllLocations();

    List<Location> getLocations(int skipCount, int pageSize);

    Location getLocationbyPincode(String pincode);

    long getTotalCount();

    GeoResults<Location> getAllPincodesWithinRange(Location location, double radius);

    String getNearestPincode(double[] position);

    Distance getDistanceBetweenLocationAndPincode(double[] position, String pincode);
}
