/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.dao.location.impl;

import com.uniware.core.entity.Location;
import com.uniware.dao.location.ILocationMao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoResult;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;

@Repository
public class LocationMaoImpl implements ILocationMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

//    @Override
//    public void save(Location location) {
//        mongoOperations.save(location);
//    }

    @Override
    public List<Location> getAllLocations() {
        return mongoOperations.findAll(Location.class);
    }

    @Override
    public Location getLocationbyPincode(String pincode) {
        Query query = new Query(Criteria.where("pincode").is(pincode));
        return mongoOperations.findOne(query, Location.class);
    }

    @Override
    public List<Location> getLocations(int skipCount, int pageSize) {
        Query query = new Query();
        query.skip(skipCount);
        query.limit(pageSize);
        return mongoOperations.find(query, Location.class);
    }

    @Override
    public long getTotalCount() {
        Query query = new Query();
        return mongoOperations.count(query, Location.class);
    }

    @Override
    public GeoResults<Location> getAllPincodesWithinRange(Location location, double radius) {
        Point origin = new Point(location.getPosition()[0], location.getPosition()[1]);
        Distance distance = new Distance(radius, Metrics.KILOMETERS);
        NearQuery query = NearQuery.near(origin).spherical(true).maxDistance(distance).num(1000);
        GeoResults<Location> locations = mongoOperations.geoNear(query, Location.class);
        return locations;
    }
    
    @Override
    public String getNearestPincode(double[] position) {
        Point origin = new Point(position[0], position[1]);
        NearQuery query = NearQuery.near(origin).num(1);
        GeoResults<Location> locations = mongoOperations.geoNear(query, Location.class);
        Iterator<GeoResult<Location>> iterator = locations.iterator();
        if (iterator.hasNext()) {
            return iterator.next().getContent().getPincode();
        }
        return null;
        
    }

    @Override
    public Distance getDistanceBetweenLocationAndPincode(double[] position, String pincode){
        Point origin = new Point(position[0], position[1]);
        NearQuery query = NearQuery.near(origin,Metrics.KILOMETERS).spherical(true).query(new Query(Criteria.where("pincode").is(pincode))).num(1);
        GeoResults<Location> locations = mongoOperations.geoNear(query, Location.class);
        Iterator<GeoResult<Location>> iterator = locations.iterator();
        if (iterator.hasNext()) {
            return iterator.next().getDistance();
        }
        return null;
    }
}
