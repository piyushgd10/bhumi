/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 6, 2012
 *  @author singla
 */
package com.uniware.dao.common;

import java.util.List;

import com.uniware.core.entity.Sequence;

public interface ISequenceGeneratorDao {

    public Sequence getSequenceByName(String name);

    public Sequence lockSequenceById(Integer sequenceId);

    public List<Sequence> getSequencesForPrefixUpdation();

    void createSequence(Sequence sequence);

    List<Sequence> getSequences();

    Sequence getSequenceByName(String name, Integer facilityId, boolean lock);

    Sequence updateSequence(Sequence sequence);

    List<Sequence> getSequencesForPrefixUpdationOnFinancialYearChange();
}
