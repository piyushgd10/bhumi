/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 6, 2012
 *  @author singla
 */
package com.uniware.dao.common.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.Sequence.Level;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.common.ISequenceGeneratorDao;

@Repository
@SuppressWarnings("unchecked")
public class SequenceGeneratorDaoImpl implements ISequenceGeneratorDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Sequence getSequenceByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select s from Sequence s where s.name = :name and (s.facility.id = :facilityId or (s.level = :tenantLevel and s.tenant.id = :tenantId))");
        query.setParameter("tenantLevel", Level.TENANT.name());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("name", name);
        query.setLockMode("s", LockMode.PESSIMISTIC_WRITE);
        return (Sequence) query.uniqueResult();
    }

    @Override
    public Sequence getSequenceByName(String name, Integer facilityId, boolean lock) {
        Query query;
        if (facilityId == null) {
            query = sessionFactory.getCurrentSession().createQuery("select s from Sequence s where s.name = :name and s.level = :level and s.tenant.id = :tenantId))");
            query.setParameter("level", Level.TENANT.name());
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select s from Sequence s where s.name = :name and s.facility.id = :facilityId and s.level = :level and s.tenant.id = :tenantId");
            query.setParameter("facilityId", facilityId);
            query.setParameter("level", Level.FACILITY.name());
        }
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name);
        if (lock) {
            query.setLockMode("s", LockMode.PESSIMISTIC_WRITE);
        }
        return (Sequence) query.uniqueResult();
    }

    @Override
    public List<Sequence> getSequencesForPrefixUpdation() {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct s from Sequence s where s.tenant.id = :tenantId and s.prefixExpression is not null");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public Sequence lockSequenceById(Integer sequenceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select s from Sequence s where s.id = :sequenceId");
        query.setParameter("sequenceId", sequenceId);
        query.setLockMode("s", LockMode.PESSIMISTIC_WRITE);
        return (Sequence) query.uniqueResult();
    }

    @Override
    public void createSequence(Sequence sequence) {
        sessionFactory.getCurrentSession().persist(sequence);
    }

    @Override
    public List<Sequence> getSequences() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Sequence where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public Sequence updateSequence(Sequence sequence) {
        return (Sequence) sessionFactory.getCurrentSession().merge(sequence);
    }

    @Override
    public List<Sequence> getSequencesForPrefixUpdationOnFinancialYearChange() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct s from Sequence s where s.tenant.id = :tenantId and name like '%INVOICE'");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }
}
