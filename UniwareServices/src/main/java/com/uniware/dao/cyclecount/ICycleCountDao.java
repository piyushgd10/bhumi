package com.uniware.dao.cyclecount;

import java.util.List;

import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.MissingInventoryDTO;
import com.uniware.core.api.cyclecount.SubCycleCountDTO;
import com.uniware.core.api.cyclecount.ZoneDTO;
import com.uniware.core.api.inventory.FacilityInventoryTypeDTO;
import com.uniware.core.entity.CycleCount;
import com.uniware.core.entity.CycleCountErrorItem;
import com.uniware.core.entity.CycleCountItem;
import com.uniware.core.entity.CycleCountShelfSummary;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.SubCycleCount;

/**
 * Created by harshpal on 2/16/16.
 */
public interface ICycleCountDao {
    CycleCount addCycleCount(CycleCount cycleCount);

    CycleCount updateCycleCount(CycleCount cycleCount);

    CycleCount getActiveCycleCount();

    CycleCount getCycleCountByCode(String cycleCountCode);

    SubCycleCount getSubCycleCountByCode(String subCycleCountCode);

    Shelf getLastCountedShelf(int cycleCountId);

    CycleCountItem addCycleCountItem(CycleCountItem cycleCountItem);

    SubCycleCount addSubCycleCount(SubCycleCount subCycleCount);

    SubCycleCount updateSubCycleCount(SubCycleCount subCycleCount);

    List<CycleCountItem> getCycleCountItemsForShelf(int cycleCountId, int shelfId);

    List<CycleCountItem> getUnreconciledCycleCountItemsForShelf(int shelfId);

    CycleCountItem getCycleCountItemForUpdate(int cycleCountItemId);

    List<CycleCountItem> getUnreconciledCycleCountItems(int cycleCountId, String skuCode,
            ItemTypeInventory.Type inventoryType, boolean missing);

    List<Shelf> getShelvesPendingForCycleCount(int cycleCountId, Integer pickSetId, Integer numberOfShelves);

    Long getNumberOfShelvesPendingCycleCount(int cycleCountId);

    List<ZoneDTO> fetchZonesForCycleCount(int cycleCountId);

    List<SubCycleCount> getSubCycleCountsForCycleCount(GetActiveCycleCountRequest request, int cycleCountId);

    Long getTotalSubcycleCountsForCycleCount(GetActiveCycleCountRequest request, int cycleCountId);

    CycleCountShelfSummary addCycleCountShelfSummary(CycleCountShelfSummary shelfSummary);

    CycleCountShelfSummary getActiveCycleCountShelfSummary(int shelfId);

    CycleCountShelfSummary getShelfSummaryForCycleCount(int shelfId, int cycleCountId);

    CycleCountShelfSummary getShelfSummaryForSubCycleCount(int shelfId, int subCycleCountId);

    void deleteCycleCountShelfSummary(CycleCountShelfSummary shelfSummary);

    CycleCountShelfSummary updateCycleCountShelfSummary(CycleCountShelfSummary shelfSummary);

    List<CycleCountShelfSummary> getSubCycleCountShelfSummaries(int subCycleCountId);

    SubCycleCountDTO getSubCycleCountSummary(int subCycleCountId);

    List<CycleCountShelfSummary> getSubCycleCountShelfSummaries(GetSubCycleCountShelvesRequest request,
            int subCycleCountId);

    Long getSubCycleCountShelfSummariesCount(GetSubCycleCountShelvesRequest request, int subCycleCountId);

    List<CycleCountShelfSummary> getActiveSubCycleCountShelfSummaries(int subCycleCountId);

    CycleCountErrorItem addCycleCountErrorItem(CycleCountErrorItem ccbi);

    List<GetErrorItemsForCycleCountResponse.ShelfErrorItemsDTO> getCycleCountErrorItemsPerShelf(int cycleCountId);

    List<CycleCountErrorItem> getCycleCountErrorItemsForShelf(int shelfId, int cycleCountId);

    void discardExistingCycleCountItemsIfRecount(SubCycleCount subCycleCount, Shelf shelf);

    List<FacilityInventoryTypeDTO> fetchMissingItemTypeInventory(Integer facility_id, String skuCode);

    List<MissingInventoryDTO> getMissingInventoryPerShelf(int cycleCountId);

    List<SubCycleCount> getSubCycleCountByCycleCountId(int id);

    Long getCompletedShelves(int cycleCountId);
}
