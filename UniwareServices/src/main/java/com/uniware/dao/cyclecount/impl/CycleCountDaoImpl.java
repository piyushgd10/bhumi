package com.uniware.dao.cyclecount.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.CollectionUtils;
import com.uniware.core.api.cyclecount.GetActiveCycleCountRequest;
import com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse;
import com.uniware.core.api.cyclecount.GetSubCycleCountShelvesRequest;
import com.uniware.core.api.cyclecount.MissingInventoryDTO;
import com.uniware.core.api.cyclecount.SubCycleCountDTO;
import com.uniware.core.api.cyclecount.ZoneDTO;
import com.uniware.core.api.inventory.FacilityInventoryTypeDTO;
import com.uniware.core.entity.CycleCount;
import com.uniware.core.entity.CycleCountErrorItem;
import com.uniware.core.entity.CycleCountItem;
import com.uniware.core.entity.CycleCountShelfSummary;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Sequence;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.Shelf.StatusCode;
import com.uniware.core.entity.SubCycleCount;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.cyclecount.ICycleCountDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * Created by harshpal on 2/16/16.
 */
@SuppressWarnings("JpaQueryApiInspection")
@Repository
public class CycleCountDaoImpl implements ICycleCountDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    @Override
    public CycleCount addCycleCount(CycleCount cycleCount) {
        cycleCount.setCode(sequenceGenerator.generateNext(Sequence.Name.CYCLE_COUNT));
        cycleCount.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(cycleCount);
        return cycleCount;
    }

    @Override
    public CycleCount updateCycleCount(CycleCount cycleCount) {
        return (CycleCount) sessionFactory.getCurrentSession().merge(cycleCount);
    }

    @Override
    public CycleCount getActiveCycleCount() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCount where statusCode in (:statusCodes) and facility.id = :facilityId");
        query.setParameterList("statusCodes", Arrays.asList(CycleCount.StatusCode.IN_PROGRESS.name(), CycleCount.StatusCode.CREATED.name()));
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (CycleCount) query.uniqueResult();
    }

    @Override
    public CycleCount getCycleCountByCode(String cycleCountCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCount where code = :code and facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("code", cycleCountCode);
        return (CycleCount) query.uniqueResult();
    }

    @Override
    public SubCycleCount getSubCycleCountByCode(String subCycleCountCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubCycleCount where code = :code and cycleCount.facility.id = :facilityId");
        query.setParameter("code", subCycleCountCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (SubCycleCount) query.uniqueResult();
    }

    @Override
    public Shelf getLastCountedShelf(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cci.shelf from CycleCountItem cci where cci.cycleCountShelfSummary.subCycleCount.cycleCount.id = :cycleCountId order by cci.created");
        query.setParameter("cycleCountId", cycleCountId);
        query.setMaxResults(1);
        return (Shelf) query.uniqueResult();
    }

    @Override
    public CycleCountItem addCycleCountItem(CycleCountItem cycleCountItem) {
        sessionFactory.getCurrentSession().persist(cycleCountItem);
        return cycleCountItem;
    }

    @Override
    public SubCycleCount addSubCycleCount(SubCycleCount subCycleCount) {
        subCycleCount.setCode(sequenceGenerator.generateNext(Sequence.Name.SUB_CYCLE_COUNT));
        sessionFactory.getCurrentSession().persist(subCycleCount);
        return subCycleCount;
    }

    @Override
    public SubCycleCount updateSubCycleCount(SubCycleCount subCycleCount) {
        return (SubCycleCount) sessionFactory.getCurrentSession().merge(subCycleCount);
    }

    @Override
    public List<CycleCountItem> getCycleCountItemsForShelf(int activeShelfSummaryId, int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCountItem where cycleCountShelfSummary.id = :activeShelfSummaryId and shelf.id = :shelfId");
        query.setParameter("activeShelfSummaryId", activeShelfSummaryId);
        query.setParameter("shelfId", shelfId);
        return query.list();
    }

    @Override
    public List<CycleCountItem> getUnreconciledCycleCountItemsForShelf(int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cci from CycleCountItem cci join cci.cycleCountShelfSummary ccss join ccss.subCycleCount scc join scc.cycleCount cc where cc.facility.id = :facilityId and cc.statusCode = :statusCode and cci.shelf.id = :shelfId and cci.pendingReconciliation > 0");
        query.setParameter("shelfId", shelfId);
        query.setParameter("statusCode", CycleCount.StatusCode.CREATED.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public CycleCountItem getCycleCountItemForUpdate(int cycleCountItemId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select cci from CycleCountItem where cci.id = :cycleCountItemId");
        query.setParameter("cycleCountItemId", cycleCountItemId);
        query.setLockMode("cci", LockMode.PESSIMISTIC_WRITE);
        return (CycleCountItem) query.uniqueResult();
    }

    @Override
    public List<CycleCountItem> getUnreconciledCycleCountItems(int cycleCountId, String skuCode, ItemTypeInventory.Type inventoryType, boolean missing) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CycleCountItem.class);
        criteria.add(Restrictions.eq("discarded", false));
        criteria.add(missing ? Restrictions.lt("inventoryDifference", 0) : Restrictions.gt("inventoryDifference", 0));
        criteria.createCriteria("cycleCountShelfSummary").createCriteria("subCycleCount").createCriteria("cycleCount").add(Restrictions.idEq(cycleCountId));
        criteria.add(Restrictions.gt("pendingReconciliation", 0));
        criteria.add(Restrictions.eq("skuCode", skuCode));
        criteria.add(Restrictions.eq("inventoryType", inventoryType));
        criteria.addOrder(Order.desc("pendingReconciliation"));
        criteria.setLockMode(LockMode.PESSIMISTIC_WRITE);
        return criteria.list();
    }

    @Override
    public List<Shelf> getShelvesPendingForCycleCount(int cycleCountId, Integer pickSetId, Integer numberOfShelves) {
        StringBuilder queryStringBuilder = new StringBuilder("select distinct s from Shelf s ");
        if (pickSetId != null) {
            queryStringBuilder.append(" join fetch s.section ss join fetch ss.pickSet ps ");
        }
        queryStringBuilder.append(
                " where not exists (select ccss.id from CycleCountShelfSummary ccss join ccss.subCycleCount scc join scc.cycleCount cc where cc.id = :cycleCountId and ccss.shelf.id = s.id) and s.facility.id = :facilityId and s.statusCode = :statusCode ");
        if (pickSetId != null) {
            queryStringBuilder.append(" and ps.id = :pickSetId ");
        }
        queryStringBuilder.append(" order by s.code");
        Query query = sessionFactory.getCurrentSession().createQuery(queryStringBuilder.toString());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("cycleCountId", cycleCountId);
        query.setParameter("statusCode", StatusCode.ACTIVE.name());
        if (pickSetId != null) {
            query.setParameter("pickSetId", pickSetId);
        }
        if (numberOfShelves != null) {
            query.setMaxResults(numberOfShelves);
        }
        //TODO - crappy solution :/ do better if you can
        return query.list();
    }

    /**
     * Pending shelves are those ACTIVE shelves for which cycle count shelf summary has not been created yet.
     * 
     * @param cycleCountId the cycle count id, used for joing sub cycle count and cycle count shelf summary.
     * @return number of pending shelves
     */
    @Override
    public Long getNumberOfShelvesPendingCycleCount(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct s) from CycleCountShelfSummary ccss right join ccss.shelf s left join ccss.subCycleCount scc left join scc.cycleCount cc where s.shelfType.facility.id = :facilityId and (ccss is null or cc.id != :cycleCountId) and s.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("cycleCountId", cycleCountId);
        query.setParameter("statusCode", StatusCode.ACTIVE.name());
        return ((Long) query.uniqueResult());
    }

    @Override
    public List<ZoneDTO> fetchZonesForCycleCount(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "select ps.name, count(distinct s.id), count(distinct s.id) - count(distinct (if(cc.id= :cycleCountId, s.id,null))) from cycle_count_shelf_summary ccss right join shelf s on ccss.shelf_id =s.id left join sub_cycle_count scc on ccss.sub_cycle_count_id=scc.id left join cycle_count cc on scc.cycle_count_id=cc.id join section ss on s.section_id=ss.id join pick_set ps on ss.pick_set_id=ps.id where ps.facility_id= :facilityId and s.status_code != :disabledStatus group by ps.id");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("cycleCountId", cycleCountId);
        query.setParameter("disabledStatus", StatusCode.INACTIVE.name());
        List<ZoneDTO> zones = new ArrayList<>();
        query.list().forEach(obj -> {
            Object[] row = (Object[]) obj;
            zones.add(new ZoneDTO((String) row[0], ((BigInteger) row[1]).longValue(), ((BigInteger) row[2]).longValue()));
        });
        //TODO - crappy solution :( need to do a lot better
        return zones;
    }

    @Override
    public List<SubCycleCount> getSubCycleCountsForCycleCount(GetActiveCycleCountRequest request, int cycleCountId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SubCycleCount.class, "scc");
        criteria.createCriteria("cycleCount").add(Restrictions.idEq(cycleCountId));
        if (request.getPageStartIndex() != null) {
            int maxResults = request.getPageSize() != null ? request.getPageSize() : 1;
            criteria.setFirstResult(request.getPageStartIndex() - 1);
            criteria.setMaxResults(maxResults);
        }
        if (!CollectionUtils.isEmpty(request.getStatusCodes())) {
            criteria.add(Restrictions.in("statusCode", request.getStatusCodes()));
        }
        criteria.addOrder(Order.desc("code"));
        return criteria.list();
    }

    @Override
    public Long getTotalSubcycleCountsForCycleCount(GetActiveCycleCountRequest request, int cycleCountId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SubCycleCount.class, "scc");
        criteria.createCriteria("cycleCount").add(Restrictions.idEq(cycleCountId));
        if (!CollectionUtils.isEmpty(request.getStatusCodes())) {
            criteria.add(Restrictions.in("statusCode", request.getStatusCodes()));
        }
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    @Override
    public CycleCountShelfSummary addCycleCountShelfSummary(CycleCountShelfSummary shelfSummary) {
        sessionFactory.getCurrentSession().persist(shelfSummary);
        return shelfSummary;
    }

    @Override
    public CycleCountShelfSummary getActiveCycleCountShelfSummary(int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ccss from CycleCountShelfSummary ccss join ccss.subCycleCount scc join scc.cycleCount cc where ccss.shelf.id = :shelfId and ccss.active = 1 and cc.statusCode = :statusCode");
        query.setParameter("statusCode", CycleCount.StatusCode.CREATED.name());
        query.setParameter("shelfId", shelfId);
        List cycleCountShelfSummaryList = query.list();
        if (cycleCountShelfSummaryList.size() != 1) {
            throw new RuntimeException("Multiple cycle count shelf summaries are active in db!");
        } else {
            return (CycleCountShelfSummary) cycleCountShelfSummaryList.get(0);
        }
    }

    /**
     * @param shelfId id of shelf
     * @param cycleCountId id of cycleCount
     * @return Fetches the {@link CycleCountShelfSummary} of {@code shelf} in {@code cycle count}. Fetches the one which
     *         was created last.
     */
    @Override
    public CycleCountShelfSummary getShelfSummaryForCycleCount(int shelfId, int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from CycleCountShelfSummary where shelf.id = :shelfId and subCycleCount.cycleCount.id= :cycleCountId order by created desc");
        query.setParameter("shelfId", shelfId);
        query.setParameter("cycleCountId", cycleCountId);
        query.setMaxResults(1);
        return (CycleCountShelfSummary) query.uniqueResult();
    }

    @Override
    public CycleCountShelfSummary getShelfSummaryForSubCycleCount(int shelfId, int subCycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCountShelfSummary where shelf.id = :shelfId and subCycleCount.id= :subCycleCountId");
        query.setParameter("shelfId", shelfId);
        query.setParameter("subCycleCountId", subCycleCountId);
        query.setMaxResults(1);
        return (CycleCountShelfSummary) query.uniqueResult();
    }

    @Override
    public void deleteCycleCountShelfSummary(CycleCountShelfSummary shelfSummary) {
        sessionFactory.getCurrentSession().delete(shelfSummary);
    }

    @Override
    public CycleCountShelfSummary updateCycleCountShelfSummary(CycleCountShelfSummary shelfSummary) {
        return (CycleCountShelfSummary) sessionFactory.getCurrentSession().merge(shelfSummary);
    }

    @Override
    public List<CycleCountShelfSummary> getSubCycleCountShelfSummaries(int subCycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCountShelfSummary where subCycleCount.id = :subCycleCountId ");
        query.setParameter("subCycleCountId", subCycleCountId);
        return query.list();
    }

    @Override
    public SubCycleCountDTO getSubCycleCountSummary(int subCycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.cyclecount.SubCycleCountDTO(sum(case when (ccss.active = true and s.statusCode= :requested) then 1 else 0 end), sum(case when (ccss.active = true and s.statusCode= :queued) then 1 else 0 end), sum(case when (ccss.active = true and s.statusCode= :ready) then 1 else 0 end), sum(case when (ccss.active = true and s.statusCode= :inProgress) then 1 else 0 end),sum(case when ccss.active = false then 1 else 0 end)) from CycleCountShelfSummary ccss right join ccss.subCycleCount scc left join ccss.shelf s where scc.id = :subCycleCountId group by scc.id");
        query.setParameter("subCycleCountId", subCycleCountId);
        query.setParameter("requested", StatusCode.COUNT_REQUESTED.name());
        query.setParameter("queued", StatusCode.QUEUED_FOR_COUNT.name());
        query.setParameter("ready", StatusCode.READY_FOR_COUNT.name());
        query.setParameter("inProgress", StatusCode.COUNT_IN_PROGRESS.name());
        return (SubCycleCountDTO) query.uniqueResult();
    }

    @Override
    public List<CycleCountShelfSummary> getSubCycleCountShelfSummaries(GetSubCycleCountShelvesRequest request, int subCycleCountId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CycleCountShelfSummary.class, "ccss");
        criteria.createCriteria("subCycleCount").add(Restrictions.idEq(subCycleCountId));
        if (request.getUserId() != null) {
            criteria.createCriteria("user").add(Restrictions.idEq(request.getUserId()));
        }
        Criteria shelfCriteria = criteria.createCriteria("shelf", "s");
        if (!CollectionUtils.isEmpty(request.getStatusCodes())) {
            if (request.getStatusCodes().contains(StatusCode.ACTIVE.name()) && request.getStatusCodes().size() == 1) {
                criteria.add(Restrictions.eq("active", false));
            } else if (!request.getStatusCodes().contains(StatusCode.ACTIVE.name())) {
                criteria.add(Restrictions.eq("active", true));
            }
            shelfCriteria.add(Restrictions.in("statusCode", request.getStatusCodes()));
        }
        if (!CollectionUtils.isEmpty(request.getPickSetIds())) {
            shelfCriteria.createCriteria("section").createCriteria("pickSet", "ps").add(Restrictions.in("id", request.getPickSetIds()));
        }
        shelfCriteria.addOrder(Order.desc("statusCode"));
        if (request.getPageStartIndex() != null) {
            int numberOfRecords = request.getPageSize() != null ? request.getPageSize() : 1;
            criteria.setFirstResult(request.getPageStartIndex() - 1);
            criteria.setMaxResults(numberOfRecords);
        }
        return criteria.list();
    }

    @Override
    public Long getSubCycleCountShelfSummariesCount(GetSubCycleCountShelvesRequest request, int subCycleCountId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CycleCountShelfSummary.class, "ccss");
        criteria.createCriteria("subCycleCount").add(Restrictions.idEq(subCycleCountId));
        if (request.getUserId() != null) {
            criteria.createCriteria("user").add(Restrictions.idEq(request.getUserId()));
        }
        Criteria shelfCriteria = null;
        if (!CollectionUtils.isEmpty(request.getStatusCodes())) {
            if (request.getStatusCodes().contains(StatusCode.ACTIVE.name()) && request.getStatusCodes().size() == 1) {
                criteria.add(Restrictions.eq("active", false));
            } else if (!request.getStatusCodes().contains(StatusCode.ACTIVE.name())) {
                criteria.add(Restrictions.eq("active", true));
            }
            shelfCriteria = criteria.createCriteria("shelf", "s");
            shelfCriteria.add(Restrictions.in("statusCode", request.getStatusCodes()));
        }
        if (!CollectionUtils.isEmpty(request.getPickSetIds())) {
            (shelfCriteria != null ? shelfCriteria : criteria.createCriteria("shelf", "s")).createCriteria("section").createCriteria("pickSet", "ps").add(
                    Restrictions.in("id", request.getPickSetIds()));
        }
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<CycleCountShelfSummary> getActiveSubCycleCountShelfSummaries(int subCycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CycleCountShelfSummary where subCycleCount.id = :subCycleCountId and active = 1");
        query.setParameter("subCycleCountId", subCycleCountId);
        return query.list();

    }

    @Override
    public CycleCountErrorItem addCycleCountErrorItem(CycleCountErrorItem ccei) {
        sessionFactory.getCurrentSession().persist(ccei);
        return ccei;
    }

    @Override
    public List<GetErrorItemsForCycleCountResponse.ShelfErrorItemsDTO> getCycleCountErrorItemsPerShelf(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.cyclecount.GetErrorItemsForCycleCountResponse$ShelfErrorItemsDTO(s.code, count(ccei)) from CycleCountErrorItem ccei join ccei.shelf s where ccei.subCycleCount.cycleCount.id = :cycleCountId group by s");
        query.setParameter("cycleCountId", cycleCountId);
        return query.list();
    }

    @Override
    public List<CycleCountErrorItem> getCycleCountErrorItemsForShelf(int shelfId, int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from CycleCountErrorItem ccei where ccei.shelf.id= :shelfId and ccei.subCycleCount.cycleCount.id = :cycleCountId");
        query.setParameter("cycleCountId", cycleCountId);
        query.setParameter("shelfId", shelfId);
        return query.list();
    }

    /**
     * <p>
     * Discarding cycle count item(<b>cci</b> is done only when inventory difference > 0 and pending Reconciliation > 0.
     * (Meaning that inventory was found to be extra, and we are hoping that a recount will fix it. Hence, discard the
     * cci). Note that cci is not discarded when inventory difference < 0 (i.e., inventory is missing). Reason is that
     * when inventory is found to be missing, the item type inventory is updated right there and then (Cause we can
     * always take orders with extra inventory, but not when inventory is missing). So, we can not discard then.
     * <p>
     * Note that pending reconciliation cannot be negative.
     * 
     * @param subCycleCount
     * @param shelf
     */
    @Override
    public void discardExistingCycleCountItemsIfRecount(SubCycleCount subCycleCount, Shelf shelf) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "update cycle_count_item cci, cycle_count_shelf_summary ccss ,sub_cycle_count scc set discarded = 1 where cci.cycle_count_shelf_summary_id = ccss.id and scc.id = ccss.sub_cycle_count_id and scc.id = :subCycleCountId and cci.shelf_id = :shelfId and cci.inventory_difference > 0 and cci.pending_reconciliation > 0");
        query.setParameter("subCycleCountId", subCycleCount.getId());
        query.setParameter("shelfId", shelf.getId());
        query.executeUpdate();
    }

    @Override
    public List<FacilityInventoryTypeDTO> fetchMissingItemTypeInventory(Integer facilityId, String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.inventory.FacilityInventoryTypeDTO(cc.facility.id, cci.ageingStartDate, sum(cci.pendingReconciliation)) from CycleCountItem cci join cci.cycleCountShelfSummary ccss join ccss.subCycleCount scc join scc.cycleCount cc where cc.facility.id=:facilityId and cc.statusCode = :cycleCountStatusCode and cci.skuCode =:skuCode and cci.inventoryDifference < 0 group by cci.ageingStartDate");
        query.setParameter("skuCode", skuCode);
        query.setParameter("facilityId", facilityId);
        query.setParameter("cycleCountStatusCode", CycleCount.StatusCode.CREATED.name());
        return query.list();
    }

    /**
     * <p>
     * Basically, gets sum of pendingReconciliation in those (shelf, sku) pairs from cycleCountItem where
     * inventoryDifference<0 and pendingReconciliation>0
     * </p>
     * 
     * @param cycleCountId
     * @return List of missing inventory which contains SkuCode, the quantity of inventory not found, ageingStartDate
     *         and shelf id.
     */
    @Override
    public List<MissingInventoryDTO> getMissingInventoryPerShelf(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.cyclecount.MissingInventoryDTO(cci.skuCode, cci.inventoryType, sum(cci.pendingReconciliation), cci.ageingStartDate, cci.shelf.id) "
                        + "from CycleCountItem cci join cci.cycleCountShelfSummary ccss join ccss.subCycleCount scc join scc.cycleCount cc "
                        + "where cc.id = :cycleCountId and cci.inventoryDifference<0 and cci.pendingReconciliation>0 "
                        + "group by cci.skuCode, cci.shelf,cci.inventoryType, cci.ageingStartDate");

        query.setParameter("cycleCountId", cycleCountId);
        return query.list();
    }

    @Override
    public List<SubCycleCount> getSubCycleCountByCycleCountId(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubCycleCount scc where scc.cycleCount.id = :id");
        query.setParameter("id", cycleCountId);
        return query.list();
    }

    /**
     * Counts the number of shelves processed in a cycle count. Does not count shelves which are {@code INACTIVE} or
     * whose shelf summary processing is incomplete (i.e., ccss.active = 1)
     * 
     * @param cycleCountId cycle count id
     * @return number of shelves processed.
     */
    @Override
    public Long getCompletedShelves(int cycleCountId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct ccss.shelf) from CycleCountShelfSummary ccss join ccss.shelf s join ccss.subCycleCount scc where scc.cycleCount.id = :cycleCountId "
                        + "and ccss.active = 0 and s.statusCode != :shelfStatusCode");
        query.setParameter("shelfStatusCode", StatusCode.INACTIVE.name());
        query.setParameter("cycleCountId", cycleCountId);
        return (Long) query.uniqueResult();
    }
}
