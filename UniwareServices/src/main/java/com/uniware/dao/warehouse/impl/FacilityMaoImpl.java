/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.warehouse.impl;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.uniware.core.entity.AnnualHoliday;
import com.uniware.core.entity.FacilityProfile;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.warehouse.IFacilityMao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.geo.GeoResults;
import org.springframework.data.mongodb.core.geo.Metrics;
import org.springframework.data.mongodb.core.geo.Point;
import org.springframework.data.mongodb.core.index.IndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Sunny
 */
@Repository
public class FacilityMaoImpl implements IFacilityMao {

	@Autowired
    @Qualifier(value = "tenantSpecificMongo")
	private MongoOperations mongoOperations;

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commonMongo;

    @Override
    public FacilityProfile getFacilityProfileByCode(String facilityCode) {
        return mongoOperations.findOne(new Query(Criteria.where("facilityCode").is(facilityCode).and("tenantCode").is(UserContext.current().getTenant().getCode())), FacilityProfile.class);
    }

    @Override
    public GeoResults<FacilityProfile> getPickupFacilitiesWithinDistance(double[] position, double distance) {
        Point point = new Point(position[0], position[1]);
        NearQuery query = NearQuery.near(point, Metrics.KILOMETERS).spherical(true).maxDistance(distance).query(
                new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("storePickupEnabled").is(true)));
        return mongoOperations.geoNear(query, FacilityProfile.class);
    }

    @Override
    public void save(FacilityProfile facilityProfile) {
        facilityProfile.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(facilityProfile);
    }

    @Override
    public List<AnnualHoliday> getAllHolidaysByCountryCode(String countryCode) {
        return commonMongo.find(new Query(Criteria.where("locale").is(countryCode)), AnnualHoliday.class);
    }

    @Override
    public void ensureIndexOnFacilityProfile() {
        IndexDefinition uniqueFacility = new IndexDefinition() {
            @Override
            public DBObject getIndexOptions() {
                return (DBObject) JSON.parse("{\"unique\" : true, \"name\" : \"facility_unique\"}");
            }
            
            @Override
            public DBObject getIndexKeys() {
                return (DBObject) JSON.parse("{\"tenantCode\" : 1, \"facilityCode\" : 1}");
            }
        };
        
        IndexDefinition positionIndex = new IndexDefinition() {
            @Override
            public DBObject getIndexOptions() {
                return (DBObject) JSON.parse("{\"name\" : \"position_2d\",\"min\" : -180, \"max\" : 180}");
            }
            
            @Override
            public DBObject getIndexKeys() {
                return (DBObject) JSON.parse("{\"position\" : \"2d\"}");
            }
        };
        mongoOperations.indexOps(FacilityProfile.class).ensureIndex(uniqueFacility);
        mongoOperations.indexOps(FacilityProfile.class).ensureIndex(positionIndex);
        
    }
}
