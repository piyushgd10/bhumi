/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.warehouse.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShelfType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.warehouse.IShelfDao;
import com.uniware.core.entity.Shelf.StatusCode;

@Repository
public class ShelfDaoImpl implements IShelfDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Shelf getShelfByCode(String code) {
        return getShelfByCode(code, true);
    }

    @Override
    public Shelf getShelfByCode(String code, boolean fetchActiveOnly) {
        Query query;
        if (fetchActiveOnly) {
            query = sessionFactory.getCurrentSession().createQuery("from Shelf where facility.id = :facilityId and code = :code and statusCode != :statusCode");
            query.setParameter("statusCode", StatusCode.INACTIVE.name());
        } else {
            query = sessionFactory.getCurrentSession().createQuery("from Shelf where facility.id = :facilityId and code = :code");
        }
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Shelf) query.uniqueResult();
    }

    @Override
    public ShelfType getShelfTypeByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShelfType st join fetch st.facility f where f.id = :facilityId and st.code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ShelfType) query.uniqueResult();
    }

    @Override
    public ShelfType createShelfType(ShelfType shelfType) {
        shelfType.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(shelfType);
        return shelfType;
    }

    @Override
    public ShelfType updateShelfType(ShelfType shelfType) {
        ShelfType tempShelfType = getShelfTypeByCode(shelfType.getCode());
        shelfType.setId(tempShelfType.getId());
        shelfType.setFacility(UserContext.current().getFacility());
        shelfType = (ShelfType) sessionFactory.getCurrentSession().merge(shelfType);
        return shelfType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShelfType> getShelfTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShelfType where facility.id = :facilityId order by enabled desc, code");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Shelf> searchShelfs(int sectionId, Integer shelfTypeId, String codeContains) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Shelf.class);
        criteria.createCriteria("section").add(Restrictions.idEq(sectionId));
        Criteria shelfTypeCriteria = criteria.createCriteria("shelfType");
        shelfTypeCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (shelfTypeId != null) {
            shelfTypeCriteria.add(Restrictions.idEq(shelfTypeId));
        }
        if (StringUtils.isNotEmpty(codeContains)) {
            criteria.add(Restrictions.like("code", "%" + codeContains + "%"));
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Shelf> getShelfs(List<String> shelfCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Shelf where shelfType.facility.id = :facilityId and code in (:codes)");
        query.setParameterList("codes", shelfCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public Shelf createShelf(Shelf shelf) {
        sessionFactory.getCurrentSession().persist(shelf);
        return shelf;
    }

    @Override
    public List<Shelf> getAllShelfs() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Shelf where shelfType.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public void updateShelf(Shelf shelf) {
        sessionFactory.getCurrentSession().merge(shelf);
    }

    @Override
    public List<Shelf> getShelvesForCycleCountPreProcessing() {
        return getAllShelvesByStatus(Arrays.asList(StatusCode.COUNT_REQUESTED, StatusCode.READY_FOR_COUNT));
    }

    @Override
    public List<Shelf> getAllShelvesPendingInCycleCount() {
        return getAllShelvesByStatus(Arrays.asList(StatusCode.COUNT_REQUESTED, StatusCode.QUEUED_FOR_COUNT, StatusCode.READY_FOR_COUNT, StatusCode.COUNT_IN_PROGRESS));
    }

    private List<Shelf> getAllShelvesByStatus(List<StatusCode> statusCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Shelf where facility.id = :facilityId and statusCode in (:statusCodes)");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("statusCodes", statusCodes.stream().map(Enum::name).collect(Collectors.toList()));
        return query.list();

    }

    @Override
    public Shelf getShelfById(int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Shelf where id = :id");
        query.setParameter("id", shelfId);
        return (Shelf) query.uniqueResult();
    }

    @Override
    public Long getTotalStockableActiveShelfCount() {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(distinct s) from Shelf s where s.facility.id = :facilityId and s.statusCode != :statusCode");
        query.setParameter("statusCode", StatusCode.INACTIVE.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Long) query.uniqueResult();
    }

    @Override
    public Shelf getNextShelfByPickSet(Integer pickSetId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select s from PickSet p join p.sections sec join sec.shelves s where p.id = :pickSetId and s.itemCount = :itemCount and s.statusCode = :active order by s.code");
        query.setParameter("pickSetId", pickSetId);
        query.setParameter("itemCount", 0);
        query.setParameter("active", StatusCode.ACTIVE.name());
        query.setMaxResults(1);
        return (Shelf) query.uniqueResult();
    }
}
