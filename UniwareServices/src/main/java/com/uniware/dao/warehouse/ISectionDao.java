/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.warehouse;

import com.uniware.core.entity.Section;

import java.util.List;

public interface ISectionDao {

    Section createSection(Section section);

    Section getSectionByCode(String code);

    List<Section> getSections();

    Section editSection(Section section);

    Section getSectionById(Integer sectionId);
}