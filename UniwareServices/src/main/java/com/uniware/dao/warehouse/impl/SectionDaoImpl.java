/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.warehouse.impl;

import com.uniware.core.entity.Section;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.warehouse.ISectionDao;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SectionDaoImpl implements ISectionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Section> getSections() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct s from Section s where s.facility.id = :facilityId order by s.editable, s.enabled desc, s.name");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public Section createSection(Section section) {
        section.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(section);
        sessionFactory.getCurrentSession().flush();
        return section;
    }

    @Override
    public Section editSection(Section section) {
        section.setFacility(UserContext.current().getFacility());
        section = (Section) sessionFactory.getCurrentSession().merge(section);
        return section;
    }

    @Override
    public Section getSectionByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where facility.id = :facilityId and code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Section) query.uniqueResult();
    }

    @Override
    public Section getSectionById(Integer sectionId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where id = :sectionId");
        query.setParameter("sectionId", sectionId);
        return (Section) query.uniqueResult();
    }
}
