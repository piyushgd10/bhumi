/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-May-2014
 *  @author sunny
 */
package com.uniware.dao.warehouse;

import java.util.List;

import com.uniware.core.entity.AnnualHoliday;
import com.uniware.core.entity.FacilityProfile;
import org.springframework.data.mongodb.core.geo.GeoResults;

public interface IFacilityMao {

    FacilityProfile getFacilityProfileByCode(String facilityCode);

    GeoResults<FacilityProfile> getPickupFacilitiesWithinDistance(double[] position, double distance);

    void save(FacilityProfile facilityProfile);

    List<AnnualHoliday> getAllHolidaysByCountryCode(String countryCode);

    void ensureIndexOnFacilityProfile();
}
