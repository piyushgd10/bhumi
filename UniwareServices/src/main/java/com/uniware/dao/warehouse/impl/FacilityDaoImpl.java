/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.dao.warehouse.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.unifier.core.utils.DateUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.warehouse.IFacilityDao;

/**
 * @author singla
 */
@Repository
public class FacilityDaoImpl implements IFacilityDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Facility editFacility(Facility facility) {
        facility = (Facility) sessionFactory.getCurrentSession().merge(facility);
        return facility;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> getAllEnabledFacilities() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility facility where facility.enabled = :enabled and facility.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("enabled", true);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> getAllEnabledFacilitiesWithGstNumber() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility facility where facility.enabled = :enabled and facility.tenant.id = :tenantId and facility.gstNumber is not null");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("enabled", true);
        return query.list();
    }

    @Override
    public Facility getFacilityByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Facility facility where (facility.code = :code or facility.alternateCode = :code) and facility.enabled = :enabled and facility.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", code);
        query.setParameter("enabled", true);
        return (Facility) query.uniqueResult();
    }

    @Override
    public Facility getAllFacilityByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility facility where (facility.code = :code or facility.alternateCode = :code) and facility.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", code);
        return (Facility) query.uniqueResult();
    }

    @Override
    public FacilityAllocationRule createAllocationRule(FacilityAllocationRule facilityAllocationRule) {
        facilityAllocationRule.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(facilityAllocationRule);
        return facilityAllocationRule;
    }

    @Override
    public FacilityAllocationRule editAllocationRule(FacilityAllocationRule facilityAllocationRule) {
        sessionFactory.getCurrentSession().merge(facilityAllocationRule);
        return facilityAllocationRule;
    }

    @Override
    public int deleteAllocationRule(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from FacilityAllocationRule far where far.tenant.id = :tenantId and far.name = :name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name);
        return query.executeUpdate();
    }

    @Override
    public FacilityAllocationRule getAllocationRuleByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from FacilityAllocationRule far where far.name = :name and far.tenant.id = :tenantId");
        query.setParameter("name", name);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (FacilityAllocationRule) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FacilityAllocationRule> getAllocationRules() {
        Query query = sessionFactory.getCurrentSession().createQuery("from FacilityAllocationRule far where far.tenant.id = :tenantId order by far.preference");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public Facility createFacility(Facility facility) {
        facility.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(facility);
        sessionFactory.getCurrentSession().flush();
        return facility;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> getFacilities() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct facility from Facility facility left join fetch facility.partyAddresses pa left join fetch facility.partyContacts pc left join fetch pa.partyAddressType where facility.tenant.id = :tenantId and facility.enabled = :enabled order by facility.code");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("enabled", true);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FacilityAllocationRule> getFacilityAllocationRules() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from FacilityAllocationRule far join fetch far.facility f where far.enabled = 1 and far.tenant.id = :tenantId order by far.preference");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Integer> getFacilityIdsWithPendingSaleOrderItems() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct soi.facility.id from SaleOrderItem soi left join soi.facility f where soi.statusCode in ('CREATED','FULFILLABLE','UNFULFILLABLE') and f.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> lookUpFacilities(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Facility facility where facility.code like :code and facility.enabled = :enabled and facility.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", "%" + keyword + "%");
        query.setParameter("enabled", true);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> getFacilitiesByTenantId(Integer tenantId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility f where f.enabled = :enabled and f.tenant.id = :tenantId");
        query.setParameter("tenantId", tenantId);
        query.setParameter("enabled", true);
        return query.list();
    }

    @Override
    public boolean setupFacilityResources(Facility facilityToClone, Facility currentFacility) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
                "Insert ignore into system_configuration select null,null,:currentFacilityId,:tenantId,sc.name,sc.display_name,sc.value,sc.type,null,sc.access_resource_name,sc.group_name,sc.sequence,now(),now() from system_configuration sc where sc.facility_id = :facilityToCloneId");
        query.setParameter("tenantId", UserContext.current().getTenant().getId());
        query.setParameter("currentFacilityId", currentFacility.getId());
        query.setParameter("facilityToCloneId", facilityToClone.getId());
        return query.executeUpdate() > 0;
    }

    @Override
    public Facility getFacilityById(Integer id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility where id = :id");
        query.setParameter("id", id);
        return (Facility) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> getAllFacilities() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Facility facility where facility.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public void resetFacilityUsers(Facility facility) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update User u set u.currentFacility = null where u.currentFacility.id = :facilityId and u.tenant.id = :tenantId");
        query.setParameter("facilityId", facility.getId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.executeUpdate();
    }
    
    @Override
    public Facility save(Facility facility) {
       return (Facility) sessionFactory.getCurrentSession().merge(facility);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Integer> getFacilityIdsWithPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct soi.facility.id from SaleOrderItem soi join soi.saleOrder so where soi.statusCode in ('CREATED','FULFILLABLE','UNFULFILLABLE') and so.tenant.id = :tenantId and so.statusCode in (:pendingOrderStatuses) and so.created > :dateStart");
        DateUtils.DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new DateUtils.Interval(TimeUnit.HOURS, pendingSinceInHours));
        query.setParameter("dateStart", dateRange.getStart());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("pendingOrderStatuses", pendingOrderStatuses);
        return query.list();
    }

}
