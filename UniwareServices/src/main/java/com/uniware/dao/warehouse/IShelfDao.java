/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.warehouse;

import java.util.List;

import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.ShelfType;

public interface IShelfDao {

    Shelf getShelfByCode(String code);

    ShelfType getShelfTypeByCode(String code);

    ShelfType createShelfType(ShelfType shelfType);

    ShelfType updateShelfType(ShelfType shelfType);

    List<ShelfType> getShelfTypes();

    List<Shelf> searchShelfs(int sectionId, Integer shelfTypeId, String codeContains);

    List<Shelf> getShelfs(List<String> shelfCodes);

    Shelf createShelf(Shelf shelf);

    List<Shelf> getAllShelfs();

    Shelf getNextShelfByPickSet(Integer id);

    void updateShelf(Shelf shelf);

    List<Shelf> getShelvesForCycleCountPreProcessing();

    List<Shelf> getAllShelvesPendingInCycleCount();

    Shelf getShelfById(int shelfId);

    Long getTotalStockableActiveShelfCount();

    Shelf getShelfByCode(String code, boolean fetchActiveOnly);
}