/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.dao.warehouse;

import java.util.List;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.FacilityAllocationRule;

/**
 * @author singla
 */
public interface IFacilityDao {

    Facility editFacility(Facility Facility);

    List<Facility> getAllEnabledFacilities();

    @SuppressWarnings("unchecked") List<Facility> getAllEnabledFacilitiesWithGstNumber();

    Facility getFacilityByCode(String code);

    Facility getAllFacilityByCode(String code);

    FacilityAllocationRule createAllocationRule(FacilityAllocationRule facilityAllocationRule);

    FacilityAllocationRule editAllocationRule(FacilityAllocationRule facilityAllocationRule);

    int deleteAllocationRule(String name);

    FacilityAllocationRule getAllocationRuleByName(String name);

    List<FacilityAllocationRule> getAllocationRules();

    Facility createFacility(Facility facility);

    List<Facility> getFacilities();

    List<FacilityAllocationRule> getFacilityAllocationRules();

    List<Integer> getFacilityIdsWithPendingSaleOrderItems();

    List<Facility> lookUpFacilities(String keyword);

    List<Facility> getFacilitiesByTenantId(Integer tenantId);

    boolean setupFacilityResources(Facility facilityToClone, Facility currentFacility);

    Facility getFacilityById(Integer id);

    List<Facility> getAllFacilities();

    void resetFacilityUsers(Facility facility);

    Facility save(Facility facility);

    List<Integer> getFacilityIdsWithPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses);
}
