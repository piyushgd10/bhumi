/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.admin.pickset.impl;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PickArea;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.admin.pickset.IPicksetDao;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PicksetDaoImpl implements IPicksetDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public PickSet getPicksetByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PickSet where facility.id = :facilityId and name = :name");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("name", name);
        return (PickSet) query.uniqueResult();
    }

    @Override
    public PickSet getPicksetById(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PickSet where facility.id = :facilityId and id = :id");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("id", id);
        return (PickSet) query.uniqueResult();
    }

    @Override
    public PickSet createPickset(PickSet pickset) {
        pickset.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(pickset);
        return pickset;
    }

    @Override
    public PickSet editPickset(PickSet pickset) {
        pickset.setFacility(UserContext.current().getFacility());
        pickset = (PickSet) sessionFactory.getCurrentSession().merge(pickset);
        return pickset;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PickSet> getPicksets() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ps from PickSet ps left join fetch ps.sections s left join fetch ps.pickArea pa where ps.facility.id = :facilityId order by ps.editable, ps.active desc, ps.name");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public Section getDefaultSection(Facility facility) {
        Query query = sessionFactory.getCurrentSession().createQuery("select s from Section s where s.facility.id = :facilityId and s.editable = 0");
        query.setParameter("facilityId", facility.getId());

        return (Section) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PickSet> getAllPickSets() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ps from PickSet ps join fetch ps.sections s where ps.facility.id = :facilityId and s.enabled = 1 and ps.active = 1");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public PickSet getActivePickSetByNameAndType(String zoneName) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PickSet where facility.id = :facilityId and name = :pickSetName and active = 1");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pickSetName", zoneName);
        return (PickSet) query.uniqueResult();
    }

    @Override
    public PickArea getPickAreaByName(String name) {
        Query q = sessionFactory.getCurrentSession().createQuery("from PickArea where name=:name and facility.id = :facilityId");
        q.setParameter("name", name);
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        return (PickArea) q.uniqueResult();
    }

    @Override
    public List<PickSet> getPicksetByTypeInPickArea(String pickAreaName, PickSet.Type type) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "from PickSet where facility.id = :facilityId and pickArea.name = :pickAreaName and type= :type and active = :active");
        q.setParameter("pickAreaName", pickAreaName);
        q.setParameter("type", type);
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("active", true);
        return q.list();
    }

    @Override
    public PickArea createPickArea(PickArea pickArea) {
        pickArea.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(pickArea);
        return pickArea;
    }

    @Override
    public List<PickArea> getPickAreas() {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct pa from PickArea pa where pa.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PickSet> getPickSetsByType(PickSet.Type type) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "from PickSet ps join fetch ps.pickArea where ps.facility.id = :facilityId and ps.type = :type order by ps.editable, ps.active desc, ps.name");
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("type", type);
        return q.list();
    }
}
