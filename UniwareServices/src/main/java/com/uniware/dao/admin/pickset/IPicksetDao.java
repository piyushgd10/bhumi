/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.admin.pickset;

import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PickArea;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Section;

import java.util.List;

public interface IPicksetDao {

    PickSet getPicksetByName(String name);

    PickSet createPickset(PickSet pickset);

    PickSet getPicksetById(int id);

    PickSet editPickset(PickSet pickset);

    List<PickSet> getPicksets();

    Section getDefaultSection(Facility facility);

    List<PickSet> getAllPickSets();

    PickSet getActivePickSetByNameAndType(String zoneName);

    PickArea getPickAreaByName(String pickAreaName);

    List<PickSet> getPicksetByTypeInPickArea(String name, PickSet.Type staging);

    PickArea createPickArea(PickArea pickArea);

    List<PickArea> getPickAreas();

    List<PickSet> getPickSetsByType(PickSet.Type type);
}
