/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Feb-2014
 *  @author parijat
 */
package com.uniware.dao.reconciliation.impl;

import com.unifier.core.entity.SaleOrderReconciliation;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelReconciliationInvoice;
import com.uniware.core.vo.ChannelReconciliationInvoiceItem;
import com.uniware.dao.reconciliation.IReconciliationMao;
import edu.umd.cs.findbugs.annotations.Nullable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ReconciliationMaoImpl implements IReconciliationMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public SaleOrderReconciliation update(SaleOrderReconciliation itemReconciliation) {
        itemReconciliation.setTenantCode(UserContext.current().getTenant().getCode());
        itemReconciliation.setUpdated(DateUtils.getCurrentTime());
        mongoOperations.save(itemReconciliation);
        return itemReconciliation;
    }

    @Override
    @Nullable
    public ChannelReconciliationInvoice getReconciliationInvoiceByCode(String invoiceCode, String channelCode) {
        return mongoOperations.findOne(
                new Query(Criteria.where("invoiceCode").is(invoiceCode).and("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelReconciliationInvoice.class);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.reconciliation.IReconciliationMao#createOrUpdate(com.uniware.core.vo.ChannelReconciliationInvoiceVO)
     */
    @Override
    public ChannelReconciliationInvoice createOrUpdate(ChannelReconciliationInvoice invoiceVO) {
        invoiceVO.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(invoiceVO);
        return invoiceVO;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.reconciliation.IReconciliationMao#create(com.uniware.core.vo.ChannelReconciliationInvoiceItemVO)
     */
    @Override
    public void createOrUpdateInvoiceItem(ChannelReconciliationInvoiceItem invoiceItem) {
        invoiceItem.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(invoiceItem);
    }

    @Override
    public List<ChannelReconciliationInvoiceItem> getReconciliationInvoiceItemsByChannelOrderCode(String channelSaleOrderCode, String reconciliationIdentifier,
            String channelCode) {
        //        return mongoOperations.find(
        //                new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("channelSaleOrderCode").is(channelSaleOrderCode).and("channelCode").is(
        //                        channelCode).and("reconciliationIdentifier").is(reconciliationIdentifier)), ChannelReconciliationInvoiceItem.class);
        return mongoOperations.find(
                new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("channelCode").is(channelCode).and("reconciliationIdentifier").is(
                        reconciliationIdentifier)),
                ChannelReconciliationInvoiceItem.class);
    }

    @Override
    public List<ChannelReconciliationInvoiceItem> getReconciliationInvoiceItemsbyInvoiceCode(String invoiceCode, String channelCode) {
        return mongoOperations.find(
                new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("invoiceCode").is(invoiceCode).and("channelCode").is(channelCode)),
                ChannelReconciliationInvoiceItem.class);
    }

    @Override
    public SaleOrderReconciliation getSaleOrderReconciliationByIdentifier(String reconciliationIdentifier, String channelCode) {
        Query query = new Query(Criteria.where("reconciliationIdentifier").is(reconciliationIdentifier).and("channelCode").is(channelCode).and("tenantCode").is(
                UserContext.current().getTenant().getCode()));
        return mongoOperations.findOne(query, SaleOrderReconciliation.class);
    }

    @Override
    public ChannelReconciliationInvoiceItem getReconciliationInvoiceItemByIdentifier(String reconciliationIdentifier, String channelCode, String invoiceCode,
            String invoiceItemOrderStatus) {
        Query query = new Query(Criteria.where("reconciliationIdentifier").is(reconciliationIdentifier).and("channelCode").is(channelCode).and("tenantCode").is(
                UserContext.current().getTenant().getCode()).and("invoiceCode").is(invoiceCode).and("invoiceItemOrderStatus").is(invoiceItemOrderStatus));
        return mongoOperations.findOne(query, ChannelReconciliationInvoiceItem.class);
    }

    @Override
    public List<ChannelReconciliationInvoiceItem> getUnprocessedReconciliationInvoiceItemByReconciliationIdentifier(String reconciliationIdentifier, String channelCode) {
        Query query = new Query(Criteria.where("reconciliationIdentifier").is(reconciliationIdentifier).and("statusCode").in(
                Arrays.asList(ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.UNPROCESSED.name(),
                        ChannelReconciliationInvoiceItem.InvoiceItemProcessedStatus.ORDER_NOT_FOUND.name())).and("channelCode").is(channelCode).and("tenantCode").is(
                                UserContext.current().getTenant().getCode()));
        return mongoOperations.find(query, ChannelReconciliationInvoiceItem.class);
    }

    @Override
    public List<SaleOrderReconciliation> getSaleOrderReconciliationsWithInvoiceMissing(int numberOfDays, int start, int pageSize) {
        Query query = new Query(
                Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("invoicingStatus").is(SaleOrderReconciliation.InvoicingStatus.AWAITING.name()).and(
                        "created").gt(DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, -numberOfDays)));
        query.skip(start);
        query.limit(pageSize);
        return mongoOperations.find(query, SaleOrderReconciliation.class);
    }

    @Override
    public void removeSaleOrderReconciliationsBySaleOrderCode(String saleOrderCode,
            String channelCode) {
        Query query = new Query(Criteria.where("saleOrderCode").is(saleOrderCode).and("channelCode").is(channelCode).and("tenantCode").is(
                UserContext.current().getTenant().getCode()));
        mongoOperations.remove(query, SaleOrderReconciliation.class);
    }
}
