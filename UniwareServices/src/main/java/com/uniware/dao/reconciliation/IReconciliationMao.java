/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Feb-2014
 *  @author parijat
 */
package com.uniware.dao.reconciliation;

import java.util.List;

import com.unifier.core.entity.SaleOrderReconciliation;
import com.uniware.core.vo.ChannelReconciliationInvoice;
import com.uniware.core.vo.ChannelReconciliationInvoiceItem;

public interface IReconciliationMao {

    SaleOrderReconciliation update(SaleOrderReconciliation itemReconciliation);

    ChannelReconciliationInvoice getReconciliationInvoiceByCode(String invoiceCode, String channelCode);

    ChannelReconciliationInvoice createOrUpdate(ChannelReconciliationInvoice invoiceVO);

    void createOrUpdateInvoiceItem(ChannelReconciliationInvoiceItem invoiceItem);

    List<ChannelReconciliationInvoiceItem> getReconciliationInvoiceItemsByChannelOrderCode(String channelSaleOrderCode, String channelSaleOrderItemCode, String channelCode);

    List<ChannelReconciliationInvoiceItem> getReconciliationInvoiceItemsbyInvoiceCode(String reconciliationIdentifier, String channelCode);

    SaleOrderReconciliation getSaleOrderReconciliationByIdentifier(String reconciliationIdentifier, String channelCode);

    ChannelReconciliationInvoiceItem getReconciliationInvoiceItemByIdentifier(String reconciliationIdentifier, String channelCode, String invoiceCode,
            String invoiceItemOrderStatus);

    List<ChannelReconciliationInvoiceItem> getUnprocessedReconciliationInvoiceItemByReconciliationIdentifier(String reconciliationIdentifier, String channelCode);

    List<SaleOrderReconciliation> getSaleOrderReconciliationsWithInvoiceMissing(int numberOfDays, int start, int pageSize);

    void removeSaleOrderReconciliationsBySaleOrderCode(String saleOrderCode, String channelCode);
}
