package com.uniware.dao.channelWarehouse;

import com.uniware.core.entity.ChannelWarehouseInventory;
import com.uniware.core.entity.Facility;

/**
 * Created by digvijaysharma on 24/01/17.
 */
public interface IChannelWarehouseInventoryDao {
    void addChannelWarehouseInventory(Facility facility, ChannelWarehouseInventory channelWarehouseInventory);
}
