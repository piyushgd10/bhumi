package com.uniware.dao.channelWarehouse.impl;

import com.uniware.core.entity.ChannelWarehouseInventory;
import com.uniware.core.entity.Facility;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.channelWarehouse.IChannelWarehouseInventoryDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by digvijaysharma on 24/01/17.
 */
@Repository
public class ChannelWarehouseInventoryDaoImpl implements IChannelWarehouseInventoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addChannelWarehouseInventory(Facility facility, ChannelWarehouseInventory channelWarehouseInventory) {
        channelWarehouseInventory.setTenant(UserContext.current().getTenant());
        channelWarehouseInventory.setFacility(facility);
        sessionFactory.getCurrentSession().persist(channelWarehouseInventory);
    }
}

