/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.dao.purchase.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.SearchInflowReceiptRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.entity.AdvanceShippingNotice;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.PurchaseCart;
import com.uniware.core.entity.PurchaseCartItem;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.PurchaseOrderStatus;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.purchase.IPurchaseDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class PurchaseDaoImpl implements IPurchaseDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#createPurchaseOrder(com.uniware.core.entity.PurchaseOrder)
     */
    @Override
    public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) {
        purchaseOrder.setFacility(UserContext.current().getFacility());
        if (purchaseOrder.getCode() == null) {
            purchaseOrder.setCode(sequenceGenerator.generateNext(Name.PURCHASE_ORDER));
        }
        sessionFactory.getCurrentSession().persist(purchaseOrder);
        return purchaseOrder;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseOrderById(java.lang.Integer)
     */
    @Override
    public PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId) {
        return getPurchaseOrderById(purchaseOrderId, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseOrderById(java.lang.Integer, boolean)
     */
    @Override
    public PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct p from PurchaseOrder p left join fetch p.purchaseOrderItems where p.id = :purchaseOrderId");
        query.setParameter("purchaseOrderId", purchaseOrderId);
        if (lock) {
            query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        }
        return (PurchaseOrder) query.uniqueResult();
    }

    @Override
    public PurchaseOrder lockOnPurchaseOrder(Integer purchaseOrderId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select p from PurchaseOrder p where p.id = :purchaseOrderId and p.created < :created");
        query.setParameter("purchaseOrderId", purchaseOrderId);
        query.setParameter("created", DateUtils.getCurrentTime());
        query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        return (PurchaseOrder) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseOrderByCode(java.lang.String)
     */
    @Override
    public PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode) {
        return getPurchaseOrderByCode(purchaseOrderCode, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseOrderByCode(java.lang.String, boolean)
     */
    @Override
    public PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct p from PurchaseOrder p left join fetch p.purchaseOrderItems left join fetch p.amendedPurchaseOrder where p.facility.id = :facilityId and p.code = :purchaseOrderCode");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        }
        return (PurchaseOrder) query.uniqueResult();
    }

    @Override
    public PurchaseOrderItem addPuchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
        sessionFactory.getCurrentSession().persist(purchaseOrderItem);
        return purchaseOrderItem;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#updatePuchaseOrderItem(com.uniware.core.entity.PurchaseOrderItem)
     */
    @Override
    public PurchaseOrderItem updatePuchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
        return (PurchaseOrderItem) sessionFactory.getCurrentSession().merge(purchaseOrderItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#updatePuchaseOrder(com.uniware.core.entity.PurchaseOrder)
     */
    @Override
    public PurchaseOrder updatePuchaseOrder(PurchaseOrder purchaseOrder) {
        return (PurchaseOrder) sessionFactory.getCurrentSession().merge(purchaseOrder);
    }

    @Override
    public List<PurchaseOrderItem> getPurchaseOrderItems(Integer purchaseOrderId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrderItem where purchaseOrder.id = :purchaseOrderId");
        query.setParameter("purchaseOrderId", purchaseOrderId);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseOrdersByVendorId(java.lang.Integer)
     */
    @Override
    public List<PurchaseOrder> getPurchaseOrdersByVendorCode(String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrder where facility.id = :facilityId and vendor.code = :vendorCode order by created desc");
        query.setParameter("vendorCode", vendorCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrdersWaitingForApproval() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrder where facility.id = :facilityId and statusCode = :statusCode");
        query.setParameter("statusCode", PurchaseOrder.StatusCode.WAITING_FOR_APPROVAL.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PurchaseOrder> getActivePurchaseOrders() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrder where facility.id = :facilityId and statusCode = :statusCode");
        query.setParameter("statusCode", PurchaseOrder.StatusCode.APPROVED.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#getPurchaseCartByUserId(java.lang.Integer)
     */
    @Override
    public PurchaseCart getPurchaseCartByUserId(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct pc from PurchaseCart pc where pc.facility.id = :facilityId and pc.user.id = :userId");
        query.setParameter("userId", userId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setLockMode("pc", LockMode.PESSIMISTIC_WRITE);
        return (PurchaseCart) query.uniqueResult();
    }

    @Override
    public PurchaseCartItem getPurchaseCartItem(Integer userId, Integer vendorId, Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct pci from PurchaseCartItem pci where pci.purchaseCart.facility.id = :facilityId and pci.purchaseCart.user.id = :userId and pci.vendor.id = :vendorId and pci.itemType.id = :itemTypeId");
        query.setParameter("userId", userId);
        query.setParameter("vendorId", vendorId);
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (PurchaseCartItem) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#addPurchaseCart(com.uniware.core.entity.PurchaseCart)
     */
    @Override
    public PurchaseCart addPurchaseCart(PurchaseCart purchaseCart) {
        purchaseCart.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(purchaseCart);
        return purchaseCart;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#updatePurchaseCartItem(com.uniware.core.entity.PurchaseCartItem)
     */
    @Override
    public PurchaseCartItem updatePurchaseCartItem(PurchaseCartItem purchaseCartItem) {
        return (PurchaseCartItem) sessionFactory.getCurrentSession().merge(purchaseCartItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#addPurchaseCartItem(com.uniware.core.entity.PurchaseCartItem)
     */
    @Override
    public PurchaseCartItem addPurchaseCartItem(PurchaseCartItem purchaseCartItem) {
        sessionFactory.getCurrentSession().persist(purchaseCartItem);
        return purchaseCartItem;
    }

    @Override
    public void removePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
        sessionFactory.getCurrentSession().delete(purchaseOrderItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#removePurchaseCartItem(com.uniware.core.entity.PurchaseCartItem)
     */
    @Override
    public void removePurchaseCartItem(PurchaseCartItem purchaseCartItem) {
        sessionFactory.getCurrentSession().delete(purchaseCartItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#removePurchaseCart(com.uniware.core.entity.PurchaseCart)
     */
    @Override
    public void removePurchaseCart(PurchaseCart purchaseCart) {
        sessionFactory.getCurrentSession().delete(purchaseCart);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.purchase.IPurchaseDao#searchItemType(com.uniware.core.api.purchase.SearchItemTypeForCartRequest)
     */
    @Override
    public List<Object> searchItemType(SearchItemTypeForCartRequest request) {
        Criteria criteria = prepareSearchItemTypeQuery(request);
        criteria.setMaxResults(500);
        criteria.setProjection(Projections.distinct(Projections.id()));
        criteria.addOrder(Order.desc("it.created"));
        return criteria.list();
    }

    @Override
    public Long getItemTypeCount(SearchItemTypeForCartRequest request) {
        Criteria criteria = prepareSearchItemTypeQuery(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    private Criteria prepareSearchItemTypeQuery(SearchItemTypeForCartRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemType.class, "it");

        Criteria vendorItemTypeCriteria = criteria.createCriteria("vendorItemTypes", "vit");
        vendorItemTypeCriteria.add(Restrictions.eq("enabled", true));

        String name = request.getItemTypeName();
        if (StringUtils.isNotEmpty(name)) {
            criteria.add(Restrictions.or(Restrictions.like("vit.vendorSkuCode", "%" + name + "%"),
                    Restrictions.or(Restrictions.like("it.name", "%" + name + "%"), Restrictions.like("it.skuCode", "%" + name + "%"))));
        }

        String categoryCode = request.getCategoryCode();
        if (StringUtils.isNotEmpty(categoryCode)) {
            criteria.createCriteria("category").add(Restrictions.eq("code", categoryCode));
        }

        Criteria vendorCriteria = vendorItemTypeCriteria.createCriteria("vendor");
        vendorCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            vendorCriteria.add(Restrictions.eq("id", vendorId));
            vendorCriteria.add(Restrictions.eq("enabled", true));
        }

        return criteria;
    }

    /* (non-Javadoc) 
     * @see com.uniware.dao.purchase.IPurchaseDao#searchPurchaseOrders(com.uniware.core.api.purchase.SearchPurchaseOrderRequest)
     */
    @Override
    public List<Object> searchPurchaseOrders(SearchPurchaseOrderRequest request) {
        Criteria criteria = preparePurchaseOrderCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }

        criteria.setProjection(Projections.distinct(Projections.id()));
        return criteria.list();
    }

    @Override
    public Long getPurchaseOrderCount(SearchPurchaseOrderRequest request) {
        Criteria criteria = preparePurchaseOrderCriteria(request);

        criteria.setProjection(Projections.countDistinct("po.id"));
        return (Long) criteria.uniqueResult();
    }

    private Criteria preparePurchaseOrderCriteria(SearchPurchaseOrderRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PurchaseOrder.class, "po");

        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        String purchaseOrderCode = request.getPurchaseOrderCode();
        if (StringUtils.isNotBlank(purchaseOrderCode)) {
            criteria.add(Restrictions.like("code", "%" + purchaseOrderCode + "%"));
        }

        String name = request.getItemTypeName();
        Criteria itemTypeCriteria = criteria.createCriteria("purchaseOrderItems", "poi", JoinType.LEFT_OUTER_JOIN).createCriteria("itemType", "it", JoinType.LEFT_OUTER_JOIN);
        if (StringUtils.isNotEmpty(name)) {
            criteria.add(Restrictions.or(Restrictions.like("poi.vendorSkuCode", "%" + name + "%"),
                    Restrictions.or(Restrictions.like("it.name", "%" + name + "%"), Restrictions.like("it.skuCode", "%" + name + "%"))));
        }

        Integer categoryId = request.getCategoryId();
        if (categoryId != null) {
            itemTypeCriteria.createCriteria("category").add(Restrictions.eq("id", categoryId));
        }

        String status = request.getStatus();
        if (StringUtils.isNotEmpty(status)) {
            criteria.add(Restrictions.eq("statusCode", status));
        }

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            criteria.createCriteria("vendor").add(Restrictions.eq("id", vendorId));
        }

        if (request.getFromDate() != null && request.getToDate() != null) {
            criteria.add(Restrictions.between("created", request.getFromDate(), request.getToDate()));
        }

        if (request.getApprovedFromDate() != null && request.getApprovedToDate() != null) {
            criteria.add(Restrictions.between("approveTime", request.getApprovedFromDate(), request.getApprovedToDate()));
        }
        return criteria;
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrders(List<Object> ids) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PurchaseOrder po join fetch po.fromParty join fetch po.vendor left join fetch po.vendorAgreement where po.facility.id = :facilityId and po.id in (:ids) order by po.created desc");
        query.setParameterList("ids", ids);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PurchaseOrder> getExpiredPurchaseOrders() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PurchaseOrder where facility.id = :facilityId and statusCode = :statusCode and expiryDate < :currentDate");
        query.setParameter("statusCode", PurchaseOrder.StatusCode.APPROVED.name());
        query.setParameter("currentDate", DateUtils.getCurrentDate());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public PurchaseOrderItem getPurchaseOrderItemById(Integer purchaseOrderItemId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrderItem where id = :purchaseOrderItemId");
        query.setParameter("purchaseOrderItemId", purchaseOrderItemId);
        return (PurchaseOrderItem) query.uniqueResult();
    }

    @Override
    public PurchaseOrderItem getPurchaseOrderItemBySkuCode(Integer purchaseOrderId, String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrderItem where purchaseOrder.id = :purchaseOrderId and itemType.skuCode = :skuCode");
        query.setParameter("purchaseOrderId", purchaseOrderId);
        query.setParameter("skuCode", skuCode);
        return (PurchaseOrderItem) query.uniqueResult();
    }

    @Override
    public AdvanceShippingNotice addASN(AdvanceShippingNotice shippingNotice) {
        shippingNotice.setFacility(UserContext.current().getFacility());
        if (shippingNotice.getCode() == null) {
            shippingNotice.setCode(sequenceGenerator.generateNext(Name.ADVANCED_SHIPPING_NOTICE));
        }
        sessionFactory.getCurrentSession().persist(shippingNotice);
        return shippingNotice;
    }

    @Override
    public List<AdvanceShippingNotice> getAdvanceShippingNotices(String purchaseOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from AdvanceShippingNotice where purchaseOrder.facility.id = :facilityId and purchaseOrder.code = :purchaseOrderCode");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public AdvanceShippingNotice getASNByCode(String asnCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from AdvanceShippingNotice asn left join fetch asn.advanceShippingNoticeItems it left join fetch it.purchaseOrderItem where asn.facility.id = :facilityId and asn.code = :asnCode");
        query.setParameter("asnCode", asnCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (AdvanceShippingNotice) query.uniqueResult();
    }

    public Criteria prepareBackOrdersCriteria(GetBackOrderItemsRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemTypeInventorySnapshot.class, "itis");

        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        criteria.add(Restrictions.sqlRestriction("open_sale > (inventory + putaway_pending + open_purchase)"));

        String name = request.getItemTypeName();
        Criteria itemTypeCriteria = criteria.createCriteria("itis.itemType");
        if (StringUtils.isNotEmpty(name)) {
            SimpleExpression se1 = Restrictions.like("name", "%" + name.trim() + "%");
            SimpleExpression se2 = Restrictions.like("skuCode", "%" + name.trim() + "%");
            itemTypeCriteria.add(Restrictions.or(se1, se2));
        }

        String categoryCode = request.getCategoryCode();
        if (StringUtils.isNotEmpty(categoryCode)) {
            itemTypeCriteria.createCriteria("category").add(Restrictions.eq("code", categoryCode));
        }
        itemTypeCriteria.add(Restrictions.eq("enabled", true));
        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            Criteria vendorItemTypeCriteria = itemTypeCriteria.createCriteria("vendorItemTypes");
            vendorItemTypeCriteria.add(Restrictions.eq("enabled", true));
            Criteria vendorCriteria = vendorItemTypeCriteria.createCriteria("vendor");
            vendorCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
            vendorCriteria.add(Restrictions.eq("id", vendorId));
            vendorCriteria.add(Restrictions.eq("enabled", true));
        }

        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.property("itis.itemType.id"));
        projectionList.add(Projections.sqlProjection("open_sale - (inventory + putaway_pending + open_purchase) as backorderQuantity", new String[] { "backorderQuantity" },
                new Type[] { StandardBasicTypes.INTEGER }));
        criteria.setProjection(projectionList);
        criteria.addOrder(Order.desc("itis.created"));
        return criteria;

    }

    @Override
    public List<Object[]> getBackOrders(GetBackOrderItemsRequest request) {
        Criteria criteria = prepareBackOrdersCriteria(request);
        if (request.getSearchOptions() != null && request.getNoVendors() == null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        return criteria.list();
    }

    @Override
    public Long getBackOrdersCount(GetBackOrderItemsRequest request) {
        Criteria criteria = prepareBackOrdersCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public List<ItemType> getItemTypes(List<Object> itemTypeIds) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemType where tenant.id = :tenantId and id in (:itemTypeIds)");
        query.setParameterList("itemTypeIds", itemTypeIds);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<Object> searchInflowReceipt(SearchInflowReceiptRequest request) {
        Criteria criteria = prepareSearchInflowReceiptCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        criteria.setProjection(Projections.distinct(Projections.id()));
        return criteria.list();
    }

    private Criteria prepareSearchInflowReceiptCriteria(SearchInflowReceiptRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InflowReceipt.class, "ir");
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        String inflowReceiptCode = request.getInflowReceiptCode();
        if (StringUtils.isNotBlank(inflowReceiptCode)) {
            criteria.add(Restrictions.eq("code", inflowReceiptCode));
        }

        String vendorInvoiceNumber = request.getVendorInvoiceNumber();
        if (StringUtils.isNotBlank(vendorInvoiceNumber)) {
            criteria.add(Restrictions.eq("vendorInvoiceNumber", vendorInvoiceNumber));
        }

        String itemTypeName = request.getItemTypeName().trim();
        if (StringUtils.isNotBlank(itemTypeName)) {
            criteria.createCriteria("inflowReceiptItems").createCriteria("itemType", "it");
            criteria.add(Restrictions.or(Restrictions.like("it.name", "%" + itemTypeName + "%"), Restrictions.like("it.skuCode", "%" + itemTypeName + "%")));
        }

        String statusCode = request.getStatusCode();
        if (StringUtils.isNotEmpty(statusCode)) {
            criteria.add(Restrictions.eq("statusCode", statusCode));
        }

        Criteria purchaseOrderCriteria = criteria.createCriteria("purchaseOrder");

        String purchaseOrderCode = request.getPurchaseOrderCode();
        if (StringUtils.isNotBlank(purchaseOrderCode)) {
            purchaseOrderCriteria.add(Restrictions.like("code", "%" + purchaseOrderCode + "%"));
        }

        String vendorName = request.getVendorName();
        if (StringUtils.isNotEmpty(vendorName)) {
            purchaseOrderCriteria.createCriteria("vendor").add(Restrictions.like("name", "%" + vendorName + "%"));
        }

        if (request.getFromDate() != null && request.getToDate() != null) {
            criteria.add(Restrictions.between("created", request.getFromDate(), request.getToDate()));
        }
        return criteria;
    }

    @Override
    public Long getInflowReceiptCount(SearchInflowReceiptRequest request) {
        Criteria criteria = prepareSearchInflowReceiptCriteria(request);

        criteria.setProjection(Projections.countDistinct("ir.id"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public List<InflowReceipt> getInflowReceipts(List<Object> ids) {
        Query query = sessionFactory.getCurrentSession().createQuery("from InflowReceipt ir where ir.facility.id = :facilityId and ir.id in (:ids) order by ir.created desc");
        query.setParameterList("ids", ids);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<SaleOrderItem> getUnfulfillableSaleOrderItems(String itemSKU, int limit) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from SaleOrderItem soi join fetch soi.itemType it where soi.facility.id = :facilityId and soi.statusCode = :statusCode and it.skuCode = :itemSKU order by soi.created desc");
        query.setParameter("itemSKU", itemSKU);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (limit > 0) {
            query.setMaxResults(limit);
        }
        return query.list();
    }

    @Override
    public int getItemTypeBackOrderQuantity(String itemSKU) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itis.openSale - (itis.inventory + itis.putawayPending + itis.openPurchase) from ItemTypeInventorySnapshot itis where itis.facility.id = :facilityId and itis.itemType.skuCode = :itemSKU and itis.openSale > (itis.inventory + itis.putawayPending + itis.openPurchase)");
        query.setParameter("itemSKU", itemSKU);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (query.uniqueResult() != null) {
            return (Integer) query.uniqueResult();
        } else {
            return 0;
        }
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrderByStatusInRange(String statusCode, DateRange dateRange, String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct p from PurchaseOrder p join fetch p.vendor v join fetch v.partyContacts pc join fetch pc.partyContactType join fetch p.purchaseOrderItems poi join fetch poi.itemType where p.facility.id = :facilityId and p.statusCode = :statusCode and p.approveTime < :pastDate and v.code = :vendorCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", PurchaseOrder.StatusCode.APPROVED.name());
        query.setParameter("pastDate", dateRange.getStart());
        query.setParameter("vendorCode", vendorCode);
        return query.list();
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrderByStatus(String statusCode, String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct p from PurchaseOrder p join fetch p.vendor v join fetch v.partyContacts pc join fetch pc.partyContactType join fetch p.purchaseOrderItems poi join fetch poi.itemType where p.facility.id = :facilityId and p.statusCode = :statusCode and v.code = :vendorCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", PurchaseOrder.StatusCode.APPROVED.name());
        query.setParameter("vendorCode", vendorCode);
        return query.list();
    }

    @Override
    public List<Object[]> getReordersNew(GetReorderItemsRequest request) {

        String sql = "select rc.reorderQuantity, it.id, it.skuCode, it.name, it.minOrderSize, "
                + "case when rc.reorderThreshold>(itis.inventory + itis.putawayPending + itis.openPurchase) then (rc.reorderThreshold - (itis.inventory + itis.putawayPending + itis.openPurchase)) else 0 end as quantityDeficit "
                + "from ReorderConfig rc join rc.itemType it " + (StringUtils.isNotBlank(request.getCategoryCode()) ? "join it.category c " : "")
                + "join it.vendorItemTypes vit " + "join it.itemTypeInventorySnapshots itis "
                + "where rc.reorderQuantity>0 and rc.reorderThreshold>0  " + "and rc.reorderThreshold > (itis.inventory + itis.putawayPending + itis.openPurchase) "
                + (StringUtils.isNotBlank(request.getItemTypeName()) ? "and it.name like :itemName or it.skuCode like :itemName " : "")
                + (StringUtils.isNotBlank(request.getCategoryCode()) ? "and c.code = :categoryCode " : "") + ((request.getVendorId() != null && request.getVendorId() > 0)
                        ? "and vit.facility.id = :facilityId and vit.vendor.id = :vendorId and vit.enabled = true " : "and vit.facility.id = :facilityId and vit.enabled = true ")
                + "and itis.facility.id = :facilityId " + "and rc.facility.id = :facilityId";

        Query query = sessionFactory.getCurrentSession().createQuery(sql);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (StringUtils.isNotBlank(request.getItemTypeName())) {
            query.setParameter("itemName", "%" + request.getItemTypeName() + "%");
        }
        if (StringUtils.isNotBlank(request.getCategoryCode())) {
            query.setParameter("categoryCode", request.getCategoryCode());
        }
        if (request.getVendorId() != null && request.getVendorId() > 0) {
            query.setParameter("vendorId", request.getVendorId());
        }

        return query.list();
    }

    @Override
    public List<Object[]> getReorders(GetReorderItemsRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemTypeInventorySnapshot.class, "itis");

        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        criteria.add(Restrictions.sqlRestriction("min_bin_quantity > inventory"));

        String name = request.getItemTypeName();
        Criteria itemTypeCriteria = criteria.createCriteria("itis.itemType");
        if (StringUtils.isNotEmpty(name)) {
            SimpleExpression se1 = Restrictions.like("name", "%" + name + "%");
            SimpleExpression se2 = Restrictions.like("skuCode", "%" + name + "%");
            itemTypeCriteria.add(Restrictions.or(se1, se2));
        }

        String categoryCode = request.getCategoryCode();
        if (StringUtils.isNotEmpty(categoryCode)) {
            itemTypeCriteria.createCriteria("category").add(Restrictions.eq("code", categoryCode));
        }

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            Criteria vendorItemTypeCriteria = itemTypeCriteria.createCriteria("vendorItemTypes");
            vendorItemTypeCriteria.add(Restrictions.eq("enabled", true));
            Criteria vendorCriteria = vendorItemTypeCriteria.createCriteria("vendor");
            vendorCriteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
            vendorCriteria.add(Restrictions.eq("id", vendorId));
            vendorCriteria.add(Restrictions.eq("enabled", true));
        }

        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.property("itis.itemType.id"));
        projectionList.add(
                Projections.sqlProjection("min_bin_quantity - inventory as reorderQuantity", new String[] { "reorderQuantity" }, new Type[] { StandardBasicTypes.INTEGER }));
        criteria.setProjection(projectionList);
        criteria.addOrder(Order.desc("itis.created"));
        return criteria.list();

    }

    @Override
    public void removeVendorItemsFromCart(PurchaseCart purchaseCart, Vendor vendor) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from PurchaseCartItem where purchaseCart.id = :purchaseCartId and vendor.id = :vendorId");
        query.setParameter("purchaseCartId", purchaseCart.getId());
        query.setParameter("vendorId", vendor.getId());
        query.executeUpdate();
    }

    @Override
    public List<String> getPurchaseOrderCodes(String statusCode, List<Integer> vendorIds, DateRange approvedBetween) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select po.code from PurchaseOrder po where po.statusCode = :statusCode and po.approveTime >= :startTime and po.approveTime <= :endTime and po.vendor.id in (:vendorIds)");
        query.setParameter("statusCode", statusCode);
        query.setParameter("startTime", approvedBetween.getStart());
        query.setParameter("endTime", approvedBetween.getEnd());
        query.setParameterList("vendorIds", vendorIds);
        return query.list();
    }

    @Override
    public PurchaseOrder getVendorPurchaseOrderByCode(String purchaseOrderCode, List<Integer> vendorIds, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct po from PurchaseOrder po left join fetch po.purchaseOrderItems left join fetch po.amendedPurchaseOrder where po.code = :purchaseOrderCode and po.vendor.id in (:vendorIds)");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameterList("vendorIds", vendorIds);
        if (lock) {
            query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        }
        return (PurchaseOrder) query.uniqueResult();
    }

    @Override
    public List<PurchaseOrderStatus> getPurchaseOrderStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from PurchaseOrderStatus").list();
    }

    @Override
    public List<PurchaseOrder> searchPurchaseOrder(String text) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrder po where po.code = :text and po.facility.id = :facilityId");
        query.setParameter("text", text);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrderCodes(GetPurchaseOrdersRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PurchaseOrder.class);
        if (request.getApprovedBetween() != null) {
            criteria.add(Restrictions.between("approveTime", request.getApprovedBetween().getStart(), request.getApprovedBetween().getEnd()));
        }
        if (request.getCreatedBetween() != null) {
            criteria.add(Restrictions.between("created", request.getCreatedBetween().getStart(), request.getCreatedBetween().getEnd()));
        }
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (StringUtils.isNotBlank(request.getVendorName())) {
            criteria.createCriteria("vendor.name").add(Restrictions.idEq(request.getVendorName()));
        }
        return criteria.list();
    }
}
