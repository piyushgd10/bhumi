/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 29, 2012
 *  @author singla
 */
package com.uniware.dao.purchase;

import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.purchase.GetBackOrderItemsRequest;
import com.uniware.core.api.purchase.GetPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.SearchInflowReceiptRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.entity.AdvanceShippingNotice;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.PurchaseCart;
import com.uniware.core.entity.PurchaseCartItem;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.PurchaseOrderStatus;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Vendor;

/**
 * @author singla
 */
public interface IPurchaseDao {

    public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * @param purchaseOrderCode
     * @param lock
     * @return
     */
    PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode, boolean lock);

    /**
     * @param purchaseOrderItem
     * @return
     */
    public PurchaseOrderItem addPuchaseOrderItem(PurchaseOrderItem purchaseOrderItem);

    /**
     * @param purchaseOrderItem
     * @return
     */
    public PurchaseOrderItem updatePuchaseOrderItem(PurchaseOrderItem purchaseOrderItem);

    /**
     * @param purchaseOrder
     * @return
     */
    public PurchaseOrder updatePuchaseOrder(PurchaseOrder purchaseOrder);

    public List<PurchaseOrderItem> getPurchaseOrderItems(Integer id);

    /**
     * @param vendorCode
     * @return
     */
    public List<PurchaseOrder> getPurchaseOrdersByVendorCode(String vendorCode);

    public List<PurchaseOrder> getPurchaseOrdersWaitingForApproval();

    public List<PurchaseOrder> getActivePurchaseOrders();

    /**
     * @param userId
     * @return
     */
    public PurchaseCart getPurchaseCartByUserId(Integer userId);

    /**
     * @param purchaseCart
     * @return
     */
    public PurchaseCart addPurchaseCart(PurchaseCart purchaseCart);

    /**
     * @param purchaseCartItem
     */
    public PurchaseCartItem updatePurchaseCartItem(PurchaseCartItem purchaseCartItem);

    /**
     * @param purchaseCartItem
     * @return
     */
    public PurchaseCartItem addPurchaseCartItem(PurchaseCartItem purchaseCartItem);

    void removePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem);

    /**
     * @param purchaseCartItem
     */
    public void removePurchaseCartItem(PurchaseCartItem purchaseCartItem);

    /**
     * @param purchaseCart
     */
    public void removePurchaseCart(PurchaseCart purchaseCart);

    /**
     * @param request
     * @return
     */
    public List<Object> searchItemType(SearchItemTypeForCartRequest request);

    /**
     * @param request
     * @return
     */
    public List<Object> searchPurchaseOrders(SearchPurchaseOrderRequest request);

    /**
     * @param request
     * @return
     */
    public Long getItemTypeCount(SearchItemTypeForCartRequest request);

    /**
     * @param ids
     * @return
     */
    public List<PurchaseOrder> getPurchaseOrders(List<Object> ids);

    /**
     * @return
     */
    public List<PurchaseOrder> getExpiredPurchaseOrders();

    /**
     * @param purchaseOrderItemId
     * @return
     */
    public PurchaseOrderItem getPurchaseOrderItemById(Integer purchaseOrderItemId);

    /**
     * @param shippingNotice
     * @return
     */
    public AdvanceShippingNotice addASN(AdvanceShippingNotice shippingNotice);

    /**
     * @param purchaseOrderCode
     * @return
     */
    public List<AdvanceShippingNotice> getAdvanceShippingNotices(String purchaseOrderCode);

    /**
     * @param asnCode
     * @return
     */
    public AdvanceShippingNotice getASNByCode(String asnCode);

    /**
     * @param request
     * @return
     */
    List<Object[]> getBackOrders(GetBackOrderItemsRequest request);

    /**
     * @param itemTypeIds
     * @return
     */
    public List<ItemType> getItemTypes(List<Object> itemTypeIds);

    /**
     * @param request
     * @return
     */
    public List<Object> searchInflowReceipt(SearchInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    public Long getInflowReceiptCount(SearchInflowReceiptRequest request);

    /**
     * @param request
     * @return
     */
    Long getPurchaseOrderCount(SearchPurchaseOrderRequest request);

    /**
     * @param purchaseOrderCode
     * @return
     */
    PurchaseOrder getPurchaseOrderByCode(String purchaseOrderCode);

    /**
     * @param itemSKU
     * @return
     */
    public int getItemTypeBackOrderQuantity(String itemSKU);

    /**
     * @param itemSKU
     * @param limit
     * @return
     */
    List<SaleOrderItem> getUnfulfillableSaleOrderItems(String itemSKU, int limit);

    PurchaseCartItem getPurchaseCartItem(Integer userId, Integer vendorId, Integer itemTypeId);

    /**
     * @param purchaseOrderId
     * @return
     */
    PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId);

    /**
     * @param purchaseOrderId
     * @param lock
     * @return
     */
    PurchaseOrder getPurchaseOrderById(Integer purchaseOrderId, boolean lock);

    /**
     * @param vendorCode
     * @param pastDate
     * @return
     */
    public List<PurchaseOrder> getPurchaseOrderByStatusInRange(String statusCode, DateRange dateRange, String vendorCode);

    /**
     * @param request
     * @return
     */
    public List<Object[]> getReorders(GetReorderItemsRequest request);

    /**
     * @param purchaseCart
     * @param vendor
     */
    void removeVendorItemsFromCart(PurchaseCart purchaseCart, Vendor vendor);

    public List<InflowReceipt> getInflowReceipts(List<Object> ids);

    List<String> getPurchaseOrderCodes(String statusCode, List<Integer> vendorIds, DateRange approvedBetween);

    List<PurchaseOrderStatus> getPurchaseOrderStatuses();

    Long getBackOrdersCount(GetBackOrderItemsRequest request);

    List<PurchaseOrder> searchPurchaseOrder(String text);

    List<Object[]> getReordersNew(GetReorderItemsRequest request);

    List<PurchaseOrder> getPurchaseOrderCodes(GetPurchaseOrdersRequest request);

    PurchaseOrder getVendorPurchaseOrderByCode(String purchaseOrderCode, List<Integer> vendorIds, boolean lock);

    List<PurchaseOrder> getPurchaseOrderByStatus(String statusCode, String vendorCode);

    PurchaseOrderItem getPurchaseOrderItemBySkuCode(Integer purchaseOrderId, String skuCode);

    PurchaseOrder lockOnPurchaseOrder(Integer purchaseOrderId);
}
