/**
 * 
 */
package com.uniware.dao.reorder;

import java.util.List;

import com.unifier.core.entity.ReorderConfig;

/**
 * @author parijat
 */
public interface IReorderConfigDAO {

    ReorderConfig create(ReorderConfig reorderConfig);
    
    ReorderConfig update(ReorderConfig reorderConfig);
    
    ReorderConfig getReorderConfigForItemID(int itemTypeId);
    
    List<ReorderConfig> getAllReorderConfigs();
    
    void delete(ReorderConfig reorderConfig);
}
