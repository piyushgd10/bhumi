/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author parijat
 */
package com.uniware.dao.reorder.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.ReorderConfig;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.reorder.IReorderConfigDAO;

@Repository
public class ReorderConfigDAOImpl implements IReorderConfigDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ReorderConfig create(ReorderConfig reorderConfig) {
        sessionFactory.getCurrentSession().persist(reorderConfig);
        return reorderConfig;
    }

    @Override
    public ReorderConfig update(ReorderConfig reorderConfig) {
        return (ReorderConfig) sessionFactory.getCurrentSession().merge(reorderConfig);
    }

    @Override
    public ReorderConfig getReorderConfigForItemID(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select rc from ReorderConfig rc where rc.itemType.id = :itemTypeId and rc.facility.id = :facilityId");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ReorderConfig) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReorderConfig> getAllReorderConfigs() {
        Query query = sessionFactory.getCurrentSession().createQuery("select rc from ReorderConfig rc where rc.facility.id is null or rc.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public void delete(ReorderConfig reorderConfig) {
        sessionFactory.getCurrentSession().delete(reorderConfig);
    }

}
