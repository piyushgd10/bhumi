/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Mar-2012
 *  @author vibhu
 */
package com.uniware.dao.shipping.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderTrackingNumber;
import com.uniware.core.entity.ShippingSourceColor;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.shipping.IShippingAdminDao;

@Repository
public class ShippingAdminDaoImpl implements IShippingAdminDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingPackageType> getShippingPackageTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackageType where facility.id = :facilityId order by editable, enabled desc, code");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public ShippingPackageType updateShippingPackageType(ShippingPackageType sPackageType) {
        return (ShippingPackageType) sessionFactory.getCurrentSession().merge(sPackageType);
    }

    @Override
    public ShippingPackageType getShippingPackageTypeByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackageType where facility.id = :facilityId and code = :code");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("code", code);
        return (ShippingPackageType) query.uniqueResult();
    }

    @Override
    public ShippingPackageType createShippingPackageType(ShippingPackageType sPackageType) {
        sPackageType.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(sPackageType);
        return sPackageType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingProvider> getShippingProviders(boolean fetchHidden) {
        Query query;
        if (fetchHidden) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select distinct sp from ShippingProvider sp where sp.tenant.id = :tenantId order by sp.enabled desc");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select distinct sp from ShippingProvider sp where sp.tenant.id = :tenantId and hidden = false order by sp.enabled desc");
        }
        query.setParameter("tenantId", UserContext.current().getTenantId());
        List<ShippingProvider> shippingProviderList = query.list();
        for (ShippingProvider sp : shippingProviderList) {
            for (ShippingProviderConnector spc : sp.getShippingProviderConnectors()) {
                Hibernate.initialize(spc.getShippingProviderConnectorParameters());
            }
        }
        return shippingProviderList;
    }

    @Override
    public ShippingProvider getShippingProviderByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProvider sp left join fetch sp.shippingProviderConnectors spc left join fetch spc.shippingProviderConnectorParameters where sp.code = :shippingProviderCode and sp.tenant.id = :tenantId order by sp.enabled desc");
        query.setParameter("shippingProviderCode", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (ShippingProvider) query.uniqueResult();
    }

    @Override
    public ShippingProvider editShippingProvider(ShippingProvider shippingProvider) {
        return (ShippingProvider) sessionFactory.getCurrentSession().merge(shippingProvider);
    }

    @Override
    public ShippingProviderMethod updateShippingProviderMethod(ShippingProviderMethod shippingProviderMethod) {
        if (shippingProviderMethod.getFacility() == null) {
            shippingProviderMethod.setFacility(UserContext.current().getFacility());
        }
        return (ShippingProviderMethod) sessionFactory.getCurrentSession().merge(shippingProviderMethod);
    }

    @Override
    public Long getAvailableAwbNumbers(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal) {
        Query query = null;
        if (isGlobal) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select count(*) from ShippingProviderTrackingNumber sptm where sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0 and sptm.facility.id is null");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select count(*) from ShippingProviderTrackingNumber sptm where sptm.facility.id = :facilityId and sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0");
            query.setParameter("facilityId", UserContext.current().getFacilityId());
        }
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        return (Long) query.uniqueResult();
    }

    @Override
    public ShippingProviderMethod getShippingProviderMethodById(Integer shippingProviderMethodId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProviderMethod where facility.id = :facilityId and id = :shippingProviderMethodId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingProviderMethodId", shippingProviderMethodId);
        return (ShippingProviderMethod) query.uniqueResult();
    }

    @Override
    public ShippingProviderTrackingNumber createShippingProviderTrackingNumber(ShippingProviderTrackingNumber trackingNumber, boolean isGlobal) {
        if (!isGlobal) {
            trackingNumber.setFacility(UserContext.current().getFacility());
        }
        sessionFactory.getCurrentSession().persist(trackingNumber);
        return trackingNumber;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingProviderAllocationRule> getShippingProviderAllocationRules() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderAllocationRule spar join fetch spar.shippingProvider where spar.facility.id = :facilityId order by spar.preference");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public ShippingProviderAllocationRule getShippingProviderAllocationRuleByName(String providerAllocationRuleName) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProviderAllocationRule where facility.id = :facilityId and name = :providerAllocationRuleName");
        query.setParameter("providerAllocationRuleName", providerAllocationRuleName);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ShippingProviderAllocationRule) query.uniqueResult();
    }

    @Override
    public ShippingProviderAllocationRule createShippingProviderAllocationRule(ShippingProviderAllocationRule allocationRule) {
        allocationRule.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(allocationRule);
        return allocationRule;
    }

    @Override
    public void deleteShippingProviderAllocationRule(ShippingProviderAllocationRule allocationRule) {
        allocationRule.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().delete(allocationRule);
    }

    /* @Override
     public ShippingProviderParameter getShippingProviderParameterById(Integer id) {
         Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProviderParameter where id = :id");
         query.setParameter("id", id);
         return (ShippingProviderParameter) query.uniqueResult();
     }

     @Override
     public ShippingProviderParameter updateShippingProviderParameter(ShippingProviderParameter parameter) {
         return (ShippingProviderParameter) sessionFactory.getCurrentSession().merge(parameter);
     }*/

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getShippingProviderTrackingNumbers(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal) {
        Query query = null;
        if (isGlobal) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select trackingNumber from ShippingProviderTrackingNumber sptm where sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0 and sptm.facility.id is null");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select trackingNumber from ShippingProviderTrackingNumber sptm where sptm.facility.id = :facilityId and sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0");
            query.setParameter("facilityId", UserContext.current().getFacilityId());
        }
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        return query.list();
    }

    @Override
    public Integer deleteShippingProviderTrackingNumbersList(Integer shippingProviderId, Integer shippingMethodId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "delete from ShippingProviderTrackingNumber sptm where sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0 and sptm.facility.id = :facilityId");
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.executeUpdate();
    }

    @Override
    public Integer deleteShippingProviderTrackingNumbersGlobal(Integer shippingProviderId, Integer shippingMethodId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "delete from ShippingProviderTrackingNumber sptm where sptm.shippingProvider.id = :shippingProviderId and sptm.shippingMethod.id = :shippingMethodId and sptm.used = 0 and sptm.facility.id is null");
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        return query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spm from ShippingProviderMethod spm join fetch spm.shippingProvider sp join fetch spm.shippingMethod where sp.id = :shippingProviderId and spm.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingProviderId", shippingProviderId);
        return query.list();
    }

    @Override
    public ShippingProvider addShippingProvider(ShippingProvider shippingProvider) {
        shippingProvider.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(shippingProvider);
        return shippingProvider;
    }

    @Override
    public ShippingProvider updateShippingProvider(ShippingProvider shippingProvider) {
        return (ShippingProvider) sessionFactory.getCurrentSession().merge(shippingProvider);
    }

    @Override
    public ShippingProvider getShippingProviderByName(String shippingProviderName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProvider sp where sp.name = :shippingProviderName and sp.tenant.id = :tenantId");
        query.setParameter("shippingProviderName", shippingProviderName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (ShippingProvider) query.uniqueResult();
    }

    @Override
    public ShippingProviderConnector addShippingProviderConnector(ShippingProviderConnector shippingProviderConnector) {
        return (ShippingProviderConnector) sessionFactory.getCurrentSession().merge(shippingProviderConnector);
    }

    @Override
    public ShippingProviderConnector updateShippingProviderConnector(ShippingProviderConnector shippingProviderConnector) {
        return (ShippingProviderConnector) sessionFactory.getCurrentSession().merge(shippingProviderConnector);
    }

    @Override
    public ShippingSourceColor getShippingProviderColorBySourceAndCount(String shippingSourceCode, int count) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingSourceColor where shippingSourceCode = :sourceCode and priority = :priority");
        query.setParameter("priority", count);
        query.setParameter("sourceCode", shippingSourceCode);
        return (ShippingSourceColor) query.uniqueResult();
    }

}
