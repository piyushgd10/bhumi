/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-May-2014
 *  @author sunny
 */
package com.uniware.dao.shipping;

import java.util.List;

import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.vo.ShippingZoneVO;

public interface IShippingMao {


    ShippingZoneVO getShippingZoneVO(String pincode);

    List<ShippingZoneVO> getAllShippingZones();

    List<ShipmentTrackingSyncStatusDTO> getAllTrackingSyncStatusDtos();

    void save(ShipmentTrackingSyncStatusDTO statusVO);

    ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByShippingProvider(String shippingProviderCode);

}
