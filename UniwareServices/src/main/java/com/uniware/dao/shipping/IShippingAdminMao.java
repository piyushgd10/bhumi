/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-Mar-2015
 *  @author parijat
 */
package com.uniware.dao.shipping;

import java.util.List;

import com.uniware.core.entity.ShippingProviderSource;

/**
 * @author parijat
 *
 */
public interface IShippingAdminMao {

    public List<ShippingProviderSource> getAllShippingProviderSources();
}
