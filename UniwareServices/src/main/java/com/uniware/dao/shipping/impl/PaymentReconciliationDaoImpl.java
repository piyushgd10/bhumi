/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.shipping.impl;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PaymentReconciliation;
import com.uniware.core.utils.UserContext;
import com.uniware.services.shipping.IPaymentReconciliationDao;

/**
 * @author singla
 */
@Repository
public class PaymentReconciliationDaoImpl implements IPaymentReconciliationDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<PaymentReconciliation> getPaymentReconciliations() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PaymentReconciliation where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public PaymentReconciliation getPaymentReconciliationByPaymentMethodCode(String paymentMethodCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PaymentReconciliation where paymentMethod.code = :paymentMethodCode and tenant.id = :tenantId");
        query.setParameter("paymentMethodCode", paymentMethodCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (PaymentReconciliation) query.uniqueResult();
    }

    @Override
    public void markPaymentReconciled(String paymentMethodCode, BigDecimal tolerance) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "update shipping_package sp, sale_order so set payment_reconciled = 1 where so.tenant_id = :tenantId and sp.sale_order_id = so.id "
                        + "and so.payment_method_code = :paymentMethodCode and sp.collected_amount is not null and abs(sp.collectable_amount - sp.collected_amount) > :tolerance"
                        + " and sp.payment_reconciled = 0");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("paymentMethodCode", paymentMethodCode);
        query.setParameter("tolerance", tolerance);
        query.executeUpdate();
    }

    @Override
    public PaymentMethod getPaymentMethodByCode(String paymentMethodCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PaymentMethod where code = :paymentMethodCode");
        query.setParameter("paymentMethodCode", paymentMethodCode);
        return (PaymentMethod) query.uniqueResult();
    }

    @Override
    public void markPackagesReconciled(List<String> shippingPackageCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ShippingPackage set paymentReconciled = 1 where facility.id = :facilityId and code in (:shippingPackageCodes)");
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.executeUpdate();
    }
}
