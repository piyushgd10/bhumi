/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.shipping.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.api.shipping.ShipmentTrackingSyncStatusDTO;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ShippingZoneVO;
import com.uniware.dao.shipping.IShippingMao;

/**
 * @author singla
 */
@Repository
public class ShippingMaoImpl implements IShippingMao {

	@Autowired
    @Qualifier(value = "commonMongo")
	private MongoOperations mongoOperations;


    @Override
    public ShippingZoneVO getShippingZoneVO(String pincode) {
        return mongoOperations.findOne(new Query(Criteria.where("pincode").is(pincode)), ShippingZoneVO.class);
    }

    @Override
    public List<ShippingZoneVO> getAllShippingZones() {
        return mongoOperations.findAll(ShippingZoneVO.class);
    }

    @Override
    public List<ShipmentTrackingSyncStatusDTO> getAllTrackingSyncStatusDtos() {
        return mongoOperations.findAll(ShipmentTrackingSyncStatusDTO.class);
    }

    @Override
    public void save(ShipmentTrackingSyncStatusDTO statusVO) {
        statusVO.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(statusVO);
    }

    @Override
    public ShipmentTrackingSyncStatusDTO getTrackingSyncStatusByShippingProvider(String shippingProviderCode) {
        return mongoOperations.findOne(
                new Query(Criteria.where("shippingProviderCode").is(shippingProviderCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ShipmentTrackingSyncStatusDTO.class);
    }

}
