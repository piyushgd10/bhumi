/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Mar-2012
 *  @author vibhu
 */
package com.uniware.dao.shipping;

import java.util.List;

import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderConnector;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderTrackingNumber;
import com.uniware.core.entity.ShippingSourceColor;

public interface IShippingAdminDao {

    List<ShippingPackageType> getShippingPackageTypes();

    ShippingPackageType updateShippingPackageType(ShippingPackageType sPackageType);

    ShippingPackageType getShippingPackageTypeByCode(String code);

    ShippingPackageType createShippingPackageType(ShippingPackageType sPackageType);

    ShippingProvider editShippingProvider(ShippingProvider shippingProvider);

    ShippingProviderMethod updateShippingProviderMethod(ShippingProviderMethod shippingProviderMethod);

    ShippingProviderMethod getShippingProviderMethodById(Integer shippingProviderMethodId);

    ShippingProviderTrackingNumber createShippingProviderTrackingNumber(ShippingProviderTrackingNumber trackingNumber, boolean isGlobal);

    List<ShippingProviderAllocationRule> getShippingProviderAllocationRules();

    ShippingProviderAllocationRule getShippingProviderAllocationRuleByName(String providerAllocationRuleName);

    ShippingProviderAllocationRule createShippingProviderAllocationRule(ShippingProviderAllocationRule allocationRule);

    void deleteShippingProviderAllocationRule(ShippingProviderAllocationRule allocationRule);

    /*ShippingProviderParameter getShippingProviderParameterById(Integer id);

    ShippingProviderParameter updateShippingProviderParameter(ShippingProviderParameter parameter);*/

    List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId);

    Long getAvailableAwbNumbers(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal);

    List<String> getShippingProviderTrackingNumbers(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal);

    Integer deleteShippingProviderTrackingNumbersList(Integer shippingProviderId, Integer shippingMethodId);

    Integer deleteShippingProviderTrackingNumbersGlobal(Integer shippingProviderId, Integer shippingMethodId);

    ShippingProvider getShippingProviderByCode(String shippingProviderCode);

    ShippingProvider addShippingProvider(ShippingProvider shippingProvider);

    ShippingProvider updateShippingProvider(ShippingProvider shippingProvider);

    List<ShippingProvider> getShippingProviders(boolean fetchHidden);

    ShippingProvider getShippingProviderByName(String shippingProviderName);

    ShippingProviderConnector addShippingProviderConnector(ShippingProviderConnector shippingProviderConnector);

    ShippingProviderConnector updateShippingProviderConnector(ShippingProviderConnector shippingProviderConnector);

    ShippingSourceColor getShippingProviderColorBySourceAndCount(String sourceCode, int count);
}