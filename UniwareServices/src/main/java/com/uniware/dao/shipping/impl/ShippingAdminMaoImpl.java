/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 31-Mar-2015
 *  @author parijat
 */
package com.uniware.dao.shipping.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.ShippingProviderSource;
import com.uniware.dao.shipping.IShippingAdminMao;

/**
 * @author parijat
 *
 */
@Repository
public class ShippingAdminMaoImpl implements IShippingAdminMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingAdminMao#getAllShippingProviderSources()
     */
    @Override
    public List<ShippingProviderSource> getAllShippingProviderSources() {
        return mongoOperations.findAll(ShippingProviderSource.class);
    }

}
