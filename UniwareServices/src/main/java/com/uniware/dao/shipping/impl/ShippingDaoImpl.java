/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.shipping.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.SearchManifestRequest;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItem.ItemDetailingStatus;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.ShipmentBatch;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingManifestItem;
import com.uniware.core.entity.ShippingManifestStatus;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackage.StatusCode;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProvider.ShippingServiceablity;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderTrackingNumber;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.dao.shipping.IShippingDao;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.configuration.SystemConfiguration;

/**
 * @author singla
 */
@SuppressWarnings("unchecked")
@Repository
public class ShippingDaoImpl implements IShippingDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    @Autowired
    private ISaleOrderDao      saleOrderDao;

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShippingPackageTypeByCode(java.lang.String)
     */
    @Override
    public ShippingPackageType getShippingPackageTypeByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackageType where facility.id = :facilityId and code = :code");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("code", code);
        return (ShippingPackageType) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#addShippingPackage(com.uniware.core.entity.ShippingPackage)
     */
    @Override
    public ShippingPackage addShippingPackage(ShippingPackage shippingPackage) {
        shippingPackage.setTenant(UserContext.current().getTenant());
        shippingPackage.setFacility(UserContext.current().getFacility());
        if (shippingPackage.getCode() == null) {
            shippingPackage.setCode(sequenceGenerator.generateNext(Name.SHIPPING_PACKAGE));
        }
        sessionFactory.getCurrentSession().persist(shippingPackage);
        return shippingPackage;
    }

    @Override
    public ShippingProvider getShippingProviderByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProvider where tenant.id = :tenantId and code = :code");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", code);
        return (ShippingProvider) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getAvailableShippingProviders(java.lang.Integer, java.lang.String)
     */
    @Override
    public List<ShippingProviderLocation> getAvailableShippingProviders(Integer shippingMethodId, String pincode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct spl from ShippingProviderLocation spl join fetch spl.shippingProviderMethods spm join fetch spm.shippingMethod where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.id = :facilityId) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.enabled = 1 and spm.facility.id = :facilityId and spm.shippingProvider.configured = 1 and spl.shippingProvider.enabled = 1 and spm.enabled = 1 and spl.shippingMethod.id = :shippingMethodId and spl.pincode = :pincode order by priority");
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pincode", pincode);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        return query.list();
    }

    @Override
    public ShippingProviderLocation getShippingProviderLocation(Integer shippingMethodId, Integer shippingProviderId, String pincode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct spl from ShippingProviderLocation spl join fetch spl.shippingProviderMethods spm where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility   and spl.facility.id = :facilityId) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spm.facility.id = :facilityId and spl.shippingMethod.id = :shippingMethodId and spl.shippingProvider.id = :shippingProviderId and spl.pincode = :pincode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("pincode", pincode);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        query.setMaxResults(1);
        return (ShippingProviderLocation) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#updateShippingPackage(com.uniware.core.entity.ShippingPackage)
     */
    @Override
    public ShippingPackage updateShippingPackage(ShippingPackage shippingPackage) {
        saleOrderDao.markSaleOrderDirty(shippingPackage.getSaleOrder());
        return (ShippingPackage) sessionFactory.getCurrentSession().merge(shippingPackage);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShippingPackageById(int)
     */
    @Override
    public ShippingPackage getShippingPackageById(int shippingPackageId) {
        return getShippingPackageById(shippingPackageId, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShippingPackageById(int, boolean)
     */
    @Override
    public ShippingPackage getShippingPackageById(int shippingPackageId, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp left join fetch sp.saleOrderItems left join fetch sp.shippingAddress where sp.id = :shippingPackageId");
        query.setParameter("shippingPackageId", shippingPackageId);
        ShippingPackage shippingPackage = (ShippingPackage) query.uniqueResult();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(shippingPackage);
        }
        return shippingPackage;
    }

    @Override
    public ShippingProviderMethod getShippingProviderMethod(String providerCode, String methodName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderMethod spm join fetch spm.shippingMethod sm where spm.facility.id = :facilityId and spm.shippingProvider.code = :providerCode and sm.name = :methodName");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("providerCode", providerCode);
        //        query.setParameter("paymentMethodCode", cod ? PaymentMethod.Code.COD.name() : PaymentMethod.Code.PREPAID.name());
        query.setParameter("methodName", methodName);
        return (ShippingProviderMethod) query.uniqueResult();
    }

    @Override
    public ShippingProviderMethod getShippingProviderMethod(String providerCode, String methodName, Integer facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderMethod spm join fetch spm.shippingMethod sm where spm.facility.id = :facilityId and spm.shippingProvider.code = :providerCode and sm.name = :methodName");
        query.setParameter("facilityId", facilityId);
        query.setParameter("providerCode", providerCode);
        //        query.setParameter("paymentMethodCode", cod ? PaymentMethod.Code.COD.name() : PaymentMethod.Code.PREPAID.name());
        query.setParameter("methodName", methodName);
        return (ShippingProviderMethod) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getAvailableTrackingNumber(java.lang.Integer, java.lang.Integer)
     */
    @Override
    public ShippingProviderTrackingNumber getAvailableTrackingNumber(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal) {
        Query query = null;
        if (isGlobal) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select trackingNumber from ShippingProviderTrackingNumber trackingNumber where trackingNumber.shippingMethod.id = :shippingMethodId and trackingNumber.shippingProvider.id = :shippingProviderId and trackingNumber.used = false");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select trackingNumber from ShippingProviderTrackingNumber trackingNumber where trackingNumber.facility.id = :facilityId and trackingNumber.shippingMethod.id = :shippingMethodId and trackingNumber.shippingProvider.id = :shippingProviderId and trackingNumber.used = false");
            query.setParameter("facilityId", UserContext.current().getFacilityId());
        }
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setLockMode("trackingNumber", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        return (ShippingProviderTrackingNumber) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#updateShippingProviderTrackingNumber(com.uniware.core.entity.ShippingProviderTrackingNumber)
     */
    @Override
    public ShippingProviderTrackingNumber updateShippingProviderTrackingNumber(ShippingProviderTrackingNumber trackingNumber) {
        return (ShippingProviderTrackingNumber) sessionFactory.getCurrentSession().merge(trackingNumber);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShipmentsForStatusTracking(int)
     */
    @Override
    public List<ShipmentTracking> getShipmentsForTracking(int shippingProviderId, List<String> statusCodes, int start, int pageSize) {
        int daysCount = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getShipmentTrackingDaysCount();
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShipmentTracking st where st.shippingProvider.id = :shippingProviderId and st.statusCode in (:statusCodes) and st.disableStatusUpdate = 0 and st.created > :startTime order by st.id");
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("startTime", DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, -daysCount));
        query.setParameterList("statusCodes", statusCodes);
        query.setFirstResult(start);
        query.setMaxResults(pageSize);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#addShipmentTracking(com.uniware.core.entity.ShipmentTracking)
     */
    @Override
    public ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking) {
        shipmentTracking.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(shipmentTracking);
        return shipmentTracking;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#updateShipmentTracking(com.uniware.core.entity.ShipmentTracking)
     */
    @Override
    public ShipmentTracking updateShipmentTracking(ShipmentTracking shipmentTracking) {
        return (ShipmentTracking) sessionFactory.getCurrentSession().merge(shipmentTracking);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#addShippingManifest(com.uniware.core.entity.ShippingManifest)
     */
    @Override
    public ShippingManifest addShippingManifest(ShippingManifest shippingManifest) {
        shippingManifest.setFacility(UserContext.current().getFacility());
        shippingManifest.setCode(sequenceGenerator.generateNext(Name.SHIPPING_MANIFEST));
        sessionFactory.getCurrentSession().persist(shippingManifest);
        return shippingManifest;
    }

    @Override
    public ShippingManifest getShippingManifestById(int shippingManifestId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sm from ShippingManifest sm left join fetch sm.shippingManifestItems smi left join fetch smi.shippingPackage sp left join fetch sp.saleOrderItems  where sm.id = :shippingManifestId");
        query.setParameter("shippingManifestId", shippingManifestId);
        ShippingManifest sm = (ShippingManifest) query.uniqueResult();
        return sm;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#addShippingManifestItem(com.uniware.core.entity.ShippingManifestItem)
     */
    @Override
    public ShippingManifestItem addShippingManifestItem(ShippingManifestItem shippingManifestItem) {
        sessionFactory.getCurrentSession().persist(shippingManifestItem);
        return shippingManifestItem;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShippingMethodById(int)
     */
    @Override
    public ShippingMethod getShippingMethodById(int shippingMethodId) {
        return (ShippingMethod) sessionFactory.getCurrentSession().createCriteria(ShippingMethod.class).add(Restrictions.idEq(shippingMethodId)).uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getRecentManifests(com.uniware.core.entity.User, int)
     */
    @Override
    public List<ShippingManifest> getRecentManifests(User user, int count) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingManifest.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (user != null) {
            criteria.createCriteria("user").add(Restrictions.idEq(user.getId()));
        }
        criteria.setFetchMode("user", FetchMode.JOIN);
        criteria.addOrder(Order.desc("created"));
        criteria.setMaxResults(count);
        return criteria.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#updateShippingManifest(com.uniware.core.entity.ShippingManifest)
     */
    @Override
    public ShippingManifest updateShippingManifest(ShippingManifest shippingManifest) {
        return (ShippingManifest) sessionFactory.getCurrentSession().merge(shippingManifest);
    }

    @Override
    public ShippingPackage searchShipmentById(Integer shipmentId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage sp left join fetch sp.saleOrderItems where sp.id = :shipmentId");
        query.setParameter("shipmentId", shipmentId);
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public List<ShippingPackage> getShippingPackageChildren(Integer shipmentId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage sp left join fetch sp.parentShippingPackage psp where psp.id = :shipmentId");
        query.setParameter("shipmentId", shipmentId);
        return query.list();
    }

    @Override
    public ShippingProviderLocation createShippingProviderLocation(ShippingProviderLocation location) {
        if (!ShippingServiceablity.LOCATION_AGNOSTIC.equals(location.getShippingProvider().getShippingServiceablity()) && location.getFacility() == null) {
            location.setFacility(UserContext.current().getFacility());
        }
        sessionFactory.getCurrentSession().persist(location);
        return location;
    }

    @Override
    public ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderLocation spl where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.id = :facilityId) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.shippingMethod.name = :shippingMethodName and spl.shippingProvider.code = :shippingProviderCode and spl.pincode = :pincode and spl.shippingProvider.tenant.id = :tenantId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("shippingMethodName", shippingMethodName);
        query.setParameter("shippingProviderCode", shippingProviderCode);
        //        query.setParameter("paymentMethodCode", cod ? PaymentMethod.Code.COD.name() : PaymentMethod.Code.PREPAID.name());
        query.setParameter("pincode", pincode);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        query.setMaxResults(1);
        return (ShippingProviderLocation) query.uniqueResult();
    }

    @Override
    public ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode, String facilityCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderLocation spl where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.code = :facilityCode) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.shippingMethod.name = :shippingMethodName and spl.shippingProvider.code = :shippingProviderCode and spl.pincode = :pincode and spl.shippingProvider.tenant.id = :tenantId");
        query.setParameter("facilityCode", facilityCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("shippingMethodName", shippingMethodName);
        query.setParameter("shippingProviderCode", shippingProviderCode);
        //        query.setParameter("paymentMethodCode", cod ? PaymentMethod.Code.COD.name() : PaymentMethod.Code.PREPAID.name());
        query.setParameter("pincode", pincode);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        query.setMaxResults(1);
        return (ShippingProviderLocation) query.uniqueResult();
    }

    @Override
    public List<ShippingProviderLocation> getLocationforShippingProvider(String shippingProviderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProviderLocation spl where spl.shippingProvider.code = :shippingProviderCode");
        query.setParameter("shippingProviderCode", shippingProviderCode);
        return query.list();
    }

    @Override
    public int deleteLocationsforShippingProvider(int shippingProviderId) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from ShippingProviderLocation where shippingProvider.id = :shippingProviderId");
        query.setParameter("shippingProviderId", shippingProviderId);
        return query.executeUpdate();
    }

    @Override
    public List<Object> searchShippingPackages(SearchShippingPackageRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.asc("created"));
        criteria.setProjection(Projections.distinct(Projections.id()));
        return criteria.list();
    }

    @Override
    public Long getShippingPackageCount(SearchShippingPackageRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        criteria.setProjection(Projections.countDistinct("sp.id"));
        return (Long) criteria.uniqueResult();
    }

    private Criteria getCriteriaFromRequest(SearchShippingPackageRequest request) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingPackage.class, "sp");
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        if (StringUtils.isNotEmpty(request.getShippingPackageCode())) {
            criteria.add(Restrictions.eq("code", request.getShippingPackageCode().trim()));
        }

        if (request.isContainsCancelledItems() || request.isOnHold()) {
            Criteria saleOrderItemCriteria = criteria.createCriteria("saleOrderItems");
            if (request.isContainsCancelledItems()) {
                saleOrderItemCriteria.add(Restrictions.eq("statusCode", SaleOrderItem.StatusCode.CANCELLED.name()));
            }
            if (request.isOnHold()) {
                saleOrderItemCriteria.add(Restrictions.eq("onHold", true));
            }
        }

        String skuCode = request.getItemTypeSkuCode();
        if (StringUtils.isNotBlank(skuCode)) {
            criteria.createCriteria("saleOrderItems", "soi").createCriteria("itemType", "it");
            criteria.add(Restrictions.eq("it.skuCode", skuCode));
        }

        List<String> statuses = request.getStatuses();
        if (!CollectionUtils.isEmpty(statuses)) {
            List<Criterion> predicates = new ArrayList<Criterion>();
            for (String status : statuses) {
                predicates.add(Restrictions.eq("statusCode", status));
            }
            criteria.add(Restrictions.or(predicates.toArray(new Criterion[predicates.size()])));
        }

        if (StringUtils.isNotBlank(request.getShippingProvider())) {
            Criteria shippingProviderCriteria = criteria.createCriteria("shippingProvider");
            shippingProviderCriteria.add(Restrictions.eq("name", request.getShippingProvider()));
        }
        if (StringUtils.isNotBlank(request.getShippingMethod())) {
            criteria.createCriteria("shippingMethod").add(Restrictions.eq("name", request.getShippingMethod()));
        }
        if (StringUtils.isNotBlank(request.getTrackingNumber())) {
            criteria.add(Restrictions.eq("trackingNumber", request.getTrackingNumber().trim()));
        }

        if (StringUtils.isNotBlank(request.getInvoiceCode())) {
            criteria.createCriteria("invoice").add(Restrictions.eq("code", request.getInvoiceCode()));
        }

        if (request.getCreateTime() != null) {
            criteria.add(Restrictions.ge("created", request.getCreateTime().getStart()));
            criteria.add(Restrictions.le("created", request.getCreateTime().getEnd()));
        }
        if (request.getDispatchTime() != null) {
            criteria.add(Restrictions.ge("dispatchTime", request.getDispatchTime().getStart()));
            criteria.add(Restrictions.le("dispatchTime", request.getDispatchTime().getEnd()));
        }

        if (request.getPaymentReconciled() != null) {
            criteria.add(Restrictions.eq("paymentReconciled", request.getPaymentReconciled()));
        }

        Boolean cashOnDelivery = request.getCashOnDelivery();
        Criteria saleOrderCriteria = criteria.createCriteria("saleOrder");
        if (cashOnDelivery != null) {
            saleOrderCriteria.createCriteria("paymentMethod").add(Restrictions.eq("code", cashOnDelivery ? PaymentMethod.Code.COD.name() : PaymentMethod.Code.PREPAID.name()));
        }
        if (StringUtils.isNotBlank(request.getSaleOrderCode())) {
            saleOrderCriteria.add(Restrictions.eq("code", request.getSaleOrderCode()));
        }

        if (StringUtils.isNotBlank(request.getChannelCode())) {
            saleOrderCriteria.createCriteria("channel").add(Restrictions.eq("code", request.getChannelCode()));
        }

        if (request.getUpdatedSinceInMinutes() != null) {
            DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new DateUtils.Interval(TimeUnit.MINUTES, request.getUpdatedSinceInMinutes()));
            criteria.add(Restrictions.ge("updated", dateRange.getStart()));
        }
        return criteria;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShippingPackagesByStatusCode(java.lang.String)
     */
    @Override
    public List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrder join fetch sp.saleOrderItems join fetch sp.shippingAddress join fetch sp.shippingMethod where sp.facility.id = :facilityId and sp.statusCode = :statusCode");
        query.setParameter("statusCode", statusCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ShippingPackage> getAllShippingPackagesByStatusCode(String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrder join fetch sp.saleOrderItems join fetch sp.shippingAddress join fetch sp.shippingMethod where sp.statusCode = :statusCode and sp.tenant.id = :tenantId and sp.facility.id = :facilityId");
        query.setParameter("statusCode", statusCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ShippingPackage> getPaginatedShippingPackagesByStatusCode(String statusCode, PageRequest request) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrder join fetch sp.saleOrderItems join fetch sp.shippingAddress join fetch sp.shippingMethod where sp.statusCode = :statusCode and sp.tenant.id = :tenantId and sp.facility.id = :facilityId");
        query.setParameter("statusCode", statusCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setFirstResult((request.getPage() - 1) * request.getSize());
        query.setMaxResults(request.getSize());
        return query.list();
    }

    @Override
    public List<ShippingProviderLocation> getShippingProviderLocationsByFacilityCodeAndUpdatedTime(Integer facilityId, Date updatedAfter) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderLocation spl where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.id = :facilityId) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.shippingProvider.tenant.id = :tenantId and spl.updated > :updated");
        query.setParameter("facilityId", facilityId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("updated", updatedAfter);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        return query.list();
    }

    @Override
    public ShippingPackage getShippingPackageByReturnInvoiceId(Integer returnInvoiceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage where returnInvoice.id=:returnInvoiceId");
        query.setParameter("returnInvoiceId", returnInvoiceId);
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public Long getPicklistPendingForShelfCount(String shelfCode) {
        Set<Long> pendingPicklists = new HashSet<>();
        String query = "select distinct pl.id from picklist_item pi, shipping_package sp, picklist pl where pi.picklist_id = pl.id and (pi.shipping_package_code = sp.code and sp.facility_id = :facilityId) and pi.shelf_code = :shelfCode and pl.facility_id = sp.facility_id and";

        Query q1 = sessionFactory.getCurrentSession().createSQLQuery(query + " sp.status_code in (:pickingPendingStatuses)");
        q1.setParameter("shelfCode", shelfCode);
        q1.setParameter("facilityId", UserContext.current().getFacilityId());
        q1.setParameterList("pickingPendingStatuses", Arrays.asList(StatusCode.PICKED.name(), StatusCode.PICKING.name()));
        pendingPicklists.addAll(q1.list());

        Query q2 = sessionFactory.getCurrentSession().createSQLQuery(query + " pi.status_code = :putbackPending");
        q2.setParameter("shelfCode", shelfCode);
        q2.setParameter("facilityId", UserContext.current().getFacilityId());
        q2.setParameter("putbackPending", PicklistItem.StatusCode.PUTBACK_PENDING);
        pendingPicklists.addAll(q2.list());

        return (long) pendingPicklists.size();
    }

    @Override
    public ShippingPackage getShippingPackageByInvoiceId(Integer invoiceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage where invoice.id=:invoiceId");
        query.setParameter("invoiceId", invoiceId);
        return (ShippingPackage) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getConfiguredShippingProviders()
     */
    @Override
    public List<ShippingProvider> getConfiguredShippingProviders() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingProvider sp where sp.tenant.id = :tenantId and sp.enabled = 1 and sp.configured = 1");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getNoOfAvailableTrackingNumberList(com.uniware.core.entity.ShippingProviderMethod)
     */
    @Override
    public Integer getNoOfAvailableTrackingNumberList(ShippingProviderMethod shippingProviderMethod) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(s) from ShippingProviderTrackingNumber s where s.facility.id = :facilityId and s.used = 0 and s.shippingMethod.id =:shippingMethodId and s.shippingProvider.id = :shippingProviderId");
        query.setParameter("shippingProviderId", shippingProviderMethod.getShippingProvider().getId());
        query.setParameter("shippingMethodId", shippingProviderMethod.getShippingMethod().getId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public ShippingPackage getShippingPackageByTrackingNumberOrShippingProvider(String trackingNumber, String shippingProviderCode) {
        Query query;
        if (shippingProviderCode != null) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems where sp.facility.id = :facilityId and sp.trackingNumber = :trackingNumber and sp.shippingProviderCode = :shippingProviderCode");
            query.setParameter("shippingProviderCode", shippingProviderCode);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems where sp.facility.id = :facilityId and sp.trackingNumber = :trackingNumber");
        }
        query.setParameter("trackingNumber", trackingNumber);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode, Date updatedBefore, Date createdBefore) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingPackage.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.add(Restrictions.eq("statusCode", statusCode));
        if (updatedBefore != null) {
            criteria.add(Restrictions.lt("updated", updatedBefore));
        }
        if (createdBefore != null) {
            criteria.add(Restrictions.lt("created", createdBefore));
        }
        return criteria.list();
    }

    @Override
    public List<ShippingManifest> searchManifest(SearchManifestRequest request) {
        Criteria criteria = prepareSearchShippingManifestCriteria(request);
        criteria.addOrder(Order.desc("created"));
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();

    }

    private Criteria prepareSearchShippingManifestCriteria(SearchManifestRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingManifest.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        String manifestCode = request.getManifestCode();
        if (StringUtils.isNotBlank(manifestCode)) {
            criteria.add(Restrictions.eq("code", manifestCode));
        }

        Integer shippingProvider = request.getShippingProvider();
        if (shippingProvider != null) {
            criteria.createCriteria("shippingProvider").add(Restrictions.eq("id", shippingProvider));
        }

        String generatedBy = request.getGeneratedBy();
        if (StringUtils.isNotEmpty(generatedBy)) {
            criteria.createCriteria("user").add(Restrictions.eq("username", generatedBy));
        }

        String statusCode = request.getStatusCode();
        if (StringUtils.isNotEmpty(statusCode)) {
            criteria.add(Restrictions.eq("statusCode", statusCode));
        }

        if (request.getFromDate() != null && request.getToDate() != null) {
            criteria.add(Restrictions.between("created", request.getFromDate(), request.getToDate()));
        }

        return criteria;
    }

    @Override
    public Long getManifestCount(SearchManifestRequest request) {
        Criteria criteria = prepareSearchShippingManifestCriteria(request);

        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public ShippingPackage searchShipmentByCode(String shipmentCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingPackage sp left join fetch sp.saleOrderItems where sp.facility.id = :facilityId and sp.code = :shipmentCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shipmentCode", shipmentCode);
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public ShippingPackage getShippingPackageByCode(String shippingPackageCode) {
        return getShippingPackageByCode(shippingPackageCode, false);
    }

    @Override
    public ShippingManifest getShippingManifestByCode(String shippingManifestCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sm from ShippingManifest sm left join fetch sm.shippingManifestItems smi where sm.facility.id = :facilityId and sm.code = :shippingManifestCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingManifestCode", shippingManifestCode);
        return (ShippingManifest) query.uniqueResult();
    }

    @Override
    public ShippingManifest getShippingManifestDetailedByCode(String shippingManifestCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sm from ShippingManifest sm left join fetch sm.shippingManifestItems smi left join fetch smi.shippingPackage sp left join fetch sp.saleOrderItems left join fetch sp.saleOrder so where sm.facility.id = :facilityId and sm.code = :shippingManifestCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingManifestCode", shippingManifestCode);
        return (ShippingManifest) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getServiceablePincodesByShippingProviderMethod(java.lang.Integer)
     */
    @Override
    public List<String> getServiceablePincodesByShippingProviderMethod(Integer shippingProviderId, Integer shippingMethodId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pincode from ShippingProviderLocation spl where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.id = :facilityId) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.shippingMethod.id = :shippingMethodId and spl.shippingProvider.id = :shippingProviderId and enabled = :enabled");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        query.setParameter("enabled", true);
        return query.list();
    }

    @Override
    public List<String> getServiceablePincodesByShippingProviderMethod(Integer shippingProviderId, Integer shippingMethodId, String facilityCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pincode from ShippingProviderLocation spl where ((spl.shippingProvider.shippingServiceablity = :limitedServiceAbility and spl.facility.code = :facilityCode) or spl.shippingProvider.shippingServiceablity = :locationAgnostic) and spl.shippingMethod.id = :shippingMethodId and spl.shippingProvider.id = :shippingProviderId and enabled = :enabled");
        query.setParameter("facilityCode", facilityCode);
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("limitedServiceAbility", ShippingServiceablity.LIMITED_SERVICEABILITY);
        query.setParameter("locationAgnostic", ShippingServiceablity.LOCATION_AGNOSTIC);
        query.setParameter("enabled", true);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#updateNonServiceablePincodes(java.lang.Integer, java.util.Set)
     */
    @Override
    public void updateNonServiceablePincodes(Integer shippingProviderId, Integer shippingMethodId, Set<String> pincodes) {
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderById(shippingProviderId);
        Query query;
        if (ShippingServiceablity.LOCATION_AGNOSTIC.equals(shippingProvider.getShippingServiceablity())) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "update ShippingProviderLocation spl set spl.enabled = 0 where spl.shippingProvider.id = :shippingProviderId and spl.shippingMethod.id = :shippingMethodId and spl.pincode in (:pincodes)");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "update ShippingProviderLocation spl set spl.enabled = 0 where spl.facility.id = :facilityId and spl.shippingProvider.id = :shippingProviderId and spl.shippingMethod.id = :shippingMethodId and spl.pincode in (:pincodes)");
            query.setParameter("facilityId", UserContext.current().getFacilityId());
        }
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameterList("pincodes", pincodes);
        query.executeUpdate();
    }

    @Override
    public void updateNonServiceablePincodes(Integer shippingProviderId, Integer shippingMethodId, Set<String> pincodes, int facilityId) {
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderById(shippingProviderId);
        Query query;
        if (ShippingServiceablity.LOCATION_AGNOSTIC.equals(shippingProvider.getShippingServiceablity())) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "update ShippingProviderLocation spl set spl.enabled = 0 where spl.shippingProvider.id = :shippingProviderId and spl.shippingMethod.id = :shippingMethodId and spl.pincode in (:pincodes)");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "update ShippingProviderLocation spl set spl.enabled = 0 where spl.facility.id = :facilityId and spl.shippingProvider.id = :shippingProviderId and spl.shippingMethod.id = :shippingMethodId and spl.pincode in (:pincodes)");
            query.setParameter("facilityId", facilityId);
        }
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameterList("pincodes", pincodes);
        query.executeUpdate();
    }

    @Override
    public ShippingManifest getShippingManifestForShippingPackage(ShippingPackage shippingPackage) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select smi.shippingManifest from ShippingManifestItem smi where smi.shippingManifest.facility.id = :facilityId and smi.shippingPackage.id = :shippingPackageId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageId", shippingPackage.getId());
        return (ShippingManifest) query.uniqueResult();
    }

    @Override
    public ReturnManifestItem getReturnManifestForShippingPackage(ShippingPackage shippingPackage) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select rmi from ReturnManifestItem rmi where rmi.returnManifest.facility.id = :facilityId and rmi.shippingPackage.id = :shippingPackageId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageId", shippingPackage.getId());
        return (ReturnManifestItem) query.uniqueResult();
    }

    @Override
    public ShipmentTracking getShipmentTracking(String trackingNumber, Integer shippingProviderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShipmentTracking where facility.id = :facilityId and type = :shipmentType and trackingNumber=:trackingNumber and shippingProvider.id = :shippingProviderId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("trackingNumber", trackingNumber);
        query.setParameter("shipmentType", ShipmentTracking.Type.SHIP_TO_CUSTOMER.name());
        query.setParameter("shippingProviderId", shippingProviderId);
        return (ShipmentTracking) query.uniqueResult();
    }

    @Override
    public List<ShippingPackage> getShippingPackagesDispatchedInDateRange(DateRange range) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sp from ShippingManifestItem smi, ShippingPackage sp join fetch sp.saleOrder join fetch sp.invoice where sp.facility.id = :facilityId and smi.shippingPackage.id = sp.id and smi.created >= :start and smi.created < :end");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("start", range.getStart());
        query.setParameter("end", range.getEnd());
        return query.list();
    }

    @Override
    public List<ShippingProviderLocation> searchShippingProviderLocation(SearchShippingProviderLocationRequest request) {
        Criteria criteria = getSearchShippingProviderLocationCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("enabled")).addOrder(Order.asc("priority"));
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public Long getShippingProviderLocationCount(SearchShippingProviderLocationRequest request) {
        Criteria criteria = getSearchShippingProviderLocationCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    private Criteria getSearchShippingProviderLocationCriteria(SearchShippingProviderLocationRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingProviderLocation.class);

        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        Criteria providerCriteria = criteria.createCriteria("shippingProvider");
        providerCriteria.add(Restrictions.eq("enabled", true));
        if (StringUtils.isNotBlank(request.getShippingProvider())) {
            providerCriteria.add(Restrictions.eq("code", request.getShippingProvider()));
        }

        String state = request.getState();
        if (StringUtils.isNotBlank(state)) {
            criteria.createCriteria("state").add(Restrictions.like("name", "%" + state + "%"));
        }

        Criteria providerMethodCriteria = criteria.createCriteria("shippingProviderMethods");
        providerMethodCriteria.add(Restrictions.eq("enabled", true));

        String shippingMethod = request.getShippingMethod();
        if (StringUtils.isNotBlank(shippingMethod)) {
            criteria.createCriteria("shippingMethod").add(Restrictions.eq("name", shippingMethod));
        }

        String shippingProviderRegion = request.getShippingProviderRegion();
        if (StringUtils.isNotBlank(shippingProviderRegion)) {
            criteria.createCriteria("shippingProviderRegion").add(Restrictions.like("name", "%" + shippingProviderRegion + "%"));
        }

        String pincode = request.getPincode();
        if (StringUtils.isNotBlank(pincode)) {
            criteria.add(Restrictions.eq("pincode", pincode));
        }

        String routingCode = request.getRoutingCode();
        if (StringUtils.isNotBlank(routingCode)) {
            criteria.add(Restrictions.like("routingCode", "%" + routingCode + "%"));
        }

        if (request.isEnabled()) {
            criteria.add(Restrictions.eq("enabled", request.isEnabled()));
        }

        return criteria;
    }

    @Override
    public Set<Integer> getServiceableByFacilities(Integer shippingMethodId, String pincode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spl from ShippingProviderLocation spl where spl.enabled = 1 and spl.shippingProvider.tenant.id = :tenantId and spl.shippingProvider.configured = 1 and spl.shippingProvider.enabled = 1 and spl.shippingMethod.id = :shippingMethodId and spl.pincode = :pincode order by priority");
        query.setParameter("shippingMethodId", shippingMethodId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("pincode", pincode);
        List<ShippingProviderLocation> locations = query.list();
        Set<Integer> facilities = new HashSet<>();
        for (ShippingProviderLocation location : locations) {
            if (location.getFacility() == null) {
                if (ShippingServiceablity.LOCATION_AGNOSTIC.equals(location.getShippingProvider().getShippingServiceablity())) {
                    List<ShippingProviderMethod> providerMethods = getShippingProviderMethods(location.getShippingProvider().getId(), location.getShippingMethod().getId());
                    for (ShippingProviderMethod providerMethod : providerMethods) {
                        if (providerMethod.getFacility().isEnabled()) {
                            facilities.add(providerMethod.getFacility().getId());
                        }
                    }
                }
            } else {
                if (!facilities.contains(location.getFacility().getId())) {
                    ShippingProviderMethod providerMethod = getShippingProviderMethod(location.getShippingProvider().getId(), location.getShippingMethod().getId(),
                            location.getFacility().getId());
                    Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(location.getFacility().getId());
                    if (facility != null && facility.isEnabled() && providerMethod != null && providerMethod.isEnabled()) {
                        facilities.add(location.getFacility().getId());
                    }
                }
            }
        }
        for (ShippingProvider shippingProvider : ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getGlobalServiceabilityShippingProviders()) {
            List<ShippingProviderMethod> providerMethods = getShippingProviderMethods(shippingProvider.getId(), shippingMethodId);
            for (ShippingProviderMethod providerMethod : providerMethods) {
                if (providerMethod.getFacility().isEnabled()) {
                    facilities.add(providerMethod.getFacility().getId());
                }
            }
        }
        return facilities;
    }

    public ShippingProviderMethod getShippingProviderMethod(Integer shippingProviderId, Integer shippingMethodId, Integer facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderMethod where shippingProvider.id = :shippingProviderId and shippingMethod.id = :shippingMethodId and facility.id = :facilityId and enabled = 1");
        query.setParameter("facilityId", facilityId);
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        return (ShippingProviderMethod) query.uniqueResult();
    }

    public List<ShippingProviderMethod> getShippingProviderMethods(Integer shippingProviderId, Integer shippingMethodId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingProviderMethod where enabled = 1 and shippingProvider.id = :shippingProviderId and shippingMethod.id = :shippingMethodId");
        query.setParameter("shippingProviderId", shippingProviderId);
        query.setParameter("shippingMethodId", shippingMethodId);
        return query.list();
    }

    @Override
    public List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spm from ShippingProviderMethod spm join fetch spm.shippingProvider sp join fetch spm.shippingMethod where sp.id = :shippingProviderId and spm.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingProviderId", shippingProviderId);
        return query.list();
    }

    @Override
    public List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId, int facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spm from ShippingProviderMethod spm join fetch spm.shippingProvider sp join fetch spm.shippingMethod where sp.id = :shippingProviderId and spm.facility.id = :facilityId");
        query.setParameter("facilityId", facilityId);
        query.setParameter("shippingProviderId", shippingProviderId);
        return query.list();
    }

    @Override
    public List<ShippingProviderMethod> getEnabledShippingProviderMethods(int shippingProviderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spm from ShippingProviderMethod spm join fetch spm.shippingProvider sp join fetch spm.shippingMethod where spm.enabled = 1 and sp.id = :shippingProviderId and spm.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingProviderId", shippingProviderId);
        return query.list();
    }

    @Override
    public List<ShippingPackage> getShippingPackagesBySaleOrder(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp where sp.saleOrder.tenant.id = :tenantId and sp.facility.id = :facilityId and sp.saleOrder.code = :saleOrderCode order by sp.updated desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        return query.list();
    }

    @Override
    public List<ShippingPackage> getAllShippingPackagesBySaleOrder(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp where sp.saleOrder.tenant.id = :tenantId and sp.saleOrder.code = :saleOrderCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        return query.list();
    }

    @Override
    public ShippingPackage getShippingPackageByCode(String shippingPackageCode, boolean markSaleOrderDirty) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp left join fetch sp.saleOrder left join fetch sp.saleOrderItems left join fetch sp.shippingAddress left join fetch sp.shippingPackageType where sp.facility.id = :facilityId and sp.code = :shippingPackageCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageCode", shippingPackageCode);
        ShippingPackage shippingPackage = (ShippingPackage) query.uniqueResult();
        if (markSaleOrderDirty) {
            saleOrderDao.markSaleOrderDirty(shippingPackage.getSaleOrder());
        }
        return shippingPackage;
    }

    @Override
    public ShippingPackage getShippingPackageWithSaleOrderByCode(String shippingPackageCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp left join fetch sp.saleOrder where sp.facility.id = :facilityId and sp.code = :shippingPackageCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageCode", shippingPackageCode);
        return (ShippingPackage) query.uniqueResult();
    }

    public int getReturnsCountBySaleOrder(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct sp) from ShippingPackage sp where sp.saleOrder.tenant.id = :tenantId and sp.facility.id = :facilityId and sp.saleOrder.code = :saleOrderCode and ( sp.statusCode = :st1 or sp.statusCode = :st2 or sp.statusCode = :st3 ) ");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("st1", ShippingPackage.StatusCode.RETURN_EXPECTED.name());
        query.setParameter("st2", ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name());
        query.setParameter("st3", ShippingPackage.StatusCode.RETURNED.name());
        return ((Long) query.uniqueResult()).intValue();
    }

    public int getInvoiceCountBySaleOrder(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct sp) from ShippingPackage sp join sp.invoice where sp.saleOrder.tenant.id = :tenantId and sp.facility.id = :facilityId and sp.saleOrder.code = :saleOrderCode ");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        return ((Long) query.uniqueResult()).intValue();
    }

    public int getShipmentCountBySaleOrder(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct sp) from ShippingPackage sp where sp.saleOrder.tenant.id = :tenantId and sp.facility.id = :facilityId and sp.saleOrder.code = :saleOrderCode and not(sp = null) ");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public ShippingPackage getShippingPackage(String shippingPackageCode, String shippingProviderCode, String trackingNumber) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sp from ShippingPackage sp join fetch sp.saleOrder where sp.tenant.id = :tenantId and sp.code = :shippingPackageCode and sp.trackingNumber = :trackingNumber and sp.shippingProviderCode = :shippingProviderCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("shippingPackageCode", shippingPackageCode);
        query.setParameter("shippingProviderCode", shippingProviderCode);
        query.setParameter("trackingNumber", trackingNumber);
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public List<ShippingPackage> getShippingPackages(String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ShippingPackage where saleOrder.tenant.id = :tenantId and facility.id = :facilityId and saleOrder.code = :saleOrderCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("saleOrderCode", saleOrderCode);
        return query.list();
    }

    @Override
    public List<ShippingPackage> getShippingPackagesByIds(List<Object> ids) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage where  facility.id = :facilityId and id in (:ids)");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("ids", ids);
        return query.list();
    }

    @Override
    public List<ShippingManifestStatus> getShippingManifestStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from ShippingManifestStatus").list();
    }

    @Override
    public List<ShippingProvider> getShippingProviders() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingProvider sp left join fetch sp.shippingProviderParameters spp where sp.tenant.id = :tenantId and sp.configured = 1 order by sp.preference");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<ShippingProviderAllocationRule> getShippingProviderAllocationRules() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingProviderAllocationRule where facility.id = :facilityId and enabled = 1 order by preference");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();

    }

    @Override
    public List<ShippingProviderMethod> getShippingProviderMethods() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select spm from ShippingProviderMethod spm join spm.shippingProvider sp join fetch spm.shippingMethod sm where spm.enabled = 1 and sp.enabled = 1 and spm.facility.id = :facilityId order by sp.preference");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ShippingMethod> getShippingMethods() {
        return sessionFactory.getCurrentSession().createQuery("from ShippingMethod where enabled = 1").list();
    }

    @Override
    public List<ShippingPackageType> getShippingPackageTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackageType where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ShippingPackageStatus> getShippingPackageStatuses() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackageStatus");
        return query.list();
    }

    @Override
    public List<ShipmentTrackingStatus> getShipmentTrackingStatuses() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct status from ShipmentTrackingStatus status join fetch status.shippingPackageStatus left join fetch status.shipmentTrackingStatusMappings stsm");
        //        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public void removeManifestItem(ShippingManifestItem shippingManifestItem) {
        sessionFactory.getCurrentSession().delete(shippingManifestItem);
    }

    @Override
    public List<ShippingProvider> lookupShippingProviders(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingProvider sp where sp.tenant.id = :tenantId and sp.configured = 1 and sp.name like :keyword");
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<ShippingPackage> getShippingPackages(GetShippingPackagesRequest request) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShippingPackage where facility.id = :facilityId and statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", request.getStatusCode());
        return query.list();
    }

    @Override
    public ShippingProviderLocation updateShippingProviderLocation(ShippingProviderLocation shippingProviderLocation) {
        return (ShippingProviderLocation) sessionFactory.getCurrentSession().merge(shippingProviderLocation);
    }

    @Override
    public List<ShippingPackage> getShippingPackagesByTrackingNumber(String trackingNumber) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems where sp.facility.id = :facilityId and sp.trackingNumber = :trackingNumber");
        query.setParameter("trackingNumber", trackingNumber);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public ShippingPackage getShippingPackageByShippingProviderAndTrackingNumber(String shippingProviderCode, String trackingNumber) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sp from ShippingPackage sp join fetch sp.saleOrder where sp.trackingNumber = :trackingNumber and sp.shippingProviderCode = :shippingProviderCode and sp.tenant.id = :tenantId");
        query.setParameter("trackingNumber", trackingNumber);
        query.setParameter("shippingProviderCode", shippingProviderCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (ShippingPackage) query.uniqueResult();
    }

    @Override
    public ShipmentBatch getShipmentBatchByCode(String shipmentBatchCode) {
        return getShipmentBatchByCode(shipmentBatchCode, false);
    }

    @Override
    public ShipmentBatch getShipmentBatchByCode(String shipmentBatchCode, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select sb from ShipmentBatch sb where sb.facility.id = :facilityId and sb.code = :shipmentBatchCode");
        query.setParameter("shipmentBatchCode", shipmentBatchCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("sb", LockMode.PESSIMISTIC_WRITE);
        }
        return (ShipmentBatch) query.uniqueResult();
    }

    @Override
    public ShipmentBatch addShipmentBatch(ShipmentBatch shipmentBatch) {
        shipmentBatch.setCode(sequenceGenerator.generateNext(Name.SHIPMENT_BATCH));
        shipmentBatch.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(shipmentBatch);
        return shipmentBatch;
    }

    @Override
    public ShipmentBatch createShipmentBatch(ShipmentBatch shipmentBatch) {
        shipmentBatch.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(shipmentBatch);
        return shipmentBatch;
    }

    @Override
    public int updateShipmentBatch(List<String> shippingPackageCodes, String shipmentBatchCode) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "update shipping_package sp, sale_order so set sp.shipment_batch_code = :shipmentBatchCode where sp.sale_order_id = so.id and sp.facility_id = :facilityId and sp.code in (:shippingPackageCodes)");
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("shipmentBatchCode", shipmentBatchCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        for (String shippingPackageCode : shippingPackageCodes) {
            saleOrderDao.markSaleOrderDirty(getShippingPackageByCode(shippingPackageCode).getSaleOrder());
        }
        return query.executeUpdate();
    }

    @Override
    public List<ShipmentBatch> getShipmentBatchesByCode(String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ShipmentBatch where facility.id = :facilityId and statusCode = :statusCode");
        query.setParameter("statusCode", statusCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemsForDetaling(List<String> shippingPackageCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct soi from SaleOrderItem soi where soi.shippingPackage.facility.id = :facilityId and soi.shippingPackage.code in (:shippingPackageCodes) and soi.itemDetailingStatus = :itemDetailingStatus");
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("itemDetailingStatus", ItemDetailingStatus.PENDING);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<Object[]> getEligibleShippingProvidersForManifest(String channelCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp.shippingProviderCode, sp.shippingProviderName, sp.shippingMethod.id, sp.shippingManager from ShippingPackage sp where sp.saleOrder.channel.code = :channelCode and sp.saleOrder.channel.tenant.id = :tenantId and (sp.statusCode = :readyToShipCode or (sp.statusCode = :packedCode and sp.shippingProviderCode is not null)) and sp.facility.id = :facilityId");
        query.setParameter("channelCode", channelCode);
        query.setParameter("readyToShipCode", ShippingPackage.StatusCode.READY_TO_SHIP.name());
        query.setParameter("packedCode", ShippingPackage.StatusCode.PACKED.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<Channel> getChannelsForManifest() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp.saleOrder.channel from ShippingPackage sp where (sp.statusCode = :readyToShipCode or (sp.statusCode = :packedCode and sp.shippingProviderCode is not null)) and sp.facility.id = :facilityId");
        query.setParameter("readyToShipCode", ShippingPackage.StatusCode.READY_TO_SHIP.name());
        query.setParameter("packedCode", ShippingPackage.StatusCode.PACKED.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ShippingPackage> getEligibleShippingPackagesForShippingManifest(ShippingManifest shippingManifest) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingPackage.class, "sp");
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.add(Restrictions.eq("statusCode", StatusCode.READY_TO_SHIP.name()));
        Criteria channelCriteria = criteria.createCriteria("saleOrder").createCriteria("channel");
        channelCriteria.add(Restrictions.idEq(shippingManifest.getChannel().getId()));

        if (shippingManifest.getShippingProviderCode() != null) {
            criteria.add(Restrictions.eq("shippingProviderCode", shippingManifest.getShippingProviderCode()));
        }

        if (shippingManifest.getShippingMethod() != null) {
            criteria.createCriteria("shippingMethod").add(Restrictions.idEq(shippingManifest.getShippingMethod().getId()));
        }
        return criteria.list();
    }

    //    @Override
    //    public ShippingManifest getShippingManifestSummaryByCode(String shippingManifestCode) {
    //        return getShippingManifestSummaryByCode(shippingManifestCode, false);
    //    }
    //
    //    @Override
    //    public ShippingManifest getShippingManifestSummaryByCode(String shippingManifestCode, boolean lock) {
    //        Query query = sessionFactory.getCurrentSession().createQuery(
    //                "select distinct sm from ShippingManifest sm left join fetch sm.shippingManifestItems smi left join fetch smi.shippingPackage sp where sm.facility.id = :facilityId and sm.code = :shippingManifestCode");
    //        query.setParameter("facilityId", UserContext.current().getFacilityId());
    //        query.setParameter("shippingManifestCode", shippingManifestCode);
    //        if (lock) {
    //            query.setLockMode("sm", LockMode.PESSIMISTIC_WRITE);
    //        }
    //        return (ShippingManifest) query.uniqueResult();
    //    }

    /*
    join fetch SOI mandatory to get saleOrderItems of given pickset only
     */
    @Override
    public ShippingPackage getShippingPackageWithSaleOrderItemsForPickSet(String packageCode, int pickSetId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sp from ShippingPackage sp join fetch sp.saleOrderItems soi where sp.facility.id = :facilityId and sp.code = :shippingPackageCode and soi.section.pickSet.id = :pickSetId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageCode", packageCode);
        query.setParameter("pickSetId", pickSetId);
        return (ShippingPackage) query.uniqueResult();
    }

}
