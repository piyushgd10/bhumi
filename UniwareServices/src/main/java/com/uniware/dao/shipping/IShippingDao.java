/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.shipping;

import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.shipping.GetShippingPackagesRequest;
import com.uniware.core.api.shipping.SearchManifestRequest;
import com.uniware.core.api.shipping.SearchShippingPackageRequest;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShipmentBatch;
import com.uniware.core.entity.ShipmentTracking;
import com.uniware.core.entity.ShipmentTrackingStatus;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingManifestItem;
import com.uniware.core.entity.ShippingManifestStatus;
import com.uniware.core.entity.ShippingMethod;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.ShippingPackageType;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.ShippingProviderAllocationRule;
import com.uniware.core.entity.ShippingProviderLocation;
import com.uniware.core.entity.ShippingProviderMethod;
import com.uniware.core.entity.ShippingProviderTrackingNumber;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author singla
 */
public interface IShippingDao {
    /**
     * @param code
     * @return
     */
    ShippingPackageType getShippingPackageTypeByCode(String code);

    /**
     * @param shippingPackage
     * @return
     */
    ShippingPackage addShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param shippingMethodId
     * @param pincode
     * @return
     */
    List<ShippingProviderLocation> getAvailableShippingProviders(Integer shippingMethodId, String pincode);

    /**
     * @param shippingPackage
     * @return
     */
    ShippingPackage updateShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param shippingPackageId
     * @return
     */
    ShippingPackage getShippingPackageById(int shippingPackageId);

    /**
     * @param code
     * @return
     */
    ShippingProvider getShippingProviderByCode(String code);

    ShippingProviderTrackingNumber getAvailableTrackingNumber(Integer shippingProviderId, Integer shippingMethodId, boolean isGlobal);

    /**
     * @param trackingNumber
     * @return
     */
    ShippingProviderTrackingNumber updateShippingProviderTrackingNumber(ShippingProviderTrackingNumber trackingNumber);

    /**
     * @param shipmentTracking
     * @return
     */
    ShipmentTracking addShipmentTracking(ShipmentTracking shipmentTracking);

    /**
     * @param shipmentTracking
     * @return
     */
    ShipmentTracking updateShipmentTracking(ShipmentTracking shipmentTracking);

    /**
     * @param shippingManifest
     * @return
     */
    ShippingManifest addShippingManifest(ShippingManifest shippingManifest);

    /**
     * @param shippingManifestId
     * @return
     */
    ShippingManifest getShippingManifestById(int shippingManifestId);

    /**
     * @param shippingPackageId
     * @param refresh
     * @return
     */
    ShippingPackage getShippingPackageById(int shippingPackageId, boolean refresh);

    /**
     * @param shippingManifestItem
     * @return
     */
    ShippingManifestItem addShippingManifestItem(ShippingManifestItem shippingManifestItem);

    /**
     * @param shippingMethodId
     * @return
     */
    ShippingMethod getShippingMethodById(int shippingMethodId);

    /**
     * @param user
     * @param count
     * @return
     */
    List<ShippingManifest> getRecentManifests(User user, int count);

    /**
     * @param shippingManifest
     */
    ShippingManifest updateShippingManifest(ShippingManifest shippingManifest);

    /**
     * @param shippingMethodId
     * @param shippingProviderId
     * @param pincode
     * @return
     */
    ShippingProviderLocation getShippingProviderLocation(Integer shippingMethodId, Integer shippingProviderId, String pincode);

    /**
     * @param shipmentId
     * @return
     */
    ShippingPackage searchShipmentById(Integer shipmentId);

    List<ShippingPackage> getShippingPackageChildren(Integer shipmentId);

    ShippingProviderLocation createShippingProviderLocation(ShippingProviderLocation location);

    ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode);

    List<Object> searchShippingPackages(SearchShippingPackageRequest request);

    Long getShippingPackageCount(SearchShippingPackageRequest request);

    /**
     * @param statusCode
     */
    List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode);

    ShippingPackage getShippingPackageByInvoiceId(Integer invoiceId);

    List<ShippingPackage> getAllShippingPackagesByStatusCode(String statusCode);

    /**
     * @return
     */
    List<ShippingProvider> getConfiguredShippingProviders();

    /**
     * @param shippingProviderMethod
     */
    Integer getNoOfAvailableTrackingNumberList(ShippingProviderMethod shippingProviderMethod);

    /**
     * @param trackingNumber
     * @param shippingProviderCode
     * @return
     */
    ShippingPackage getShippingPackageByTrackingNumberOrShippingProvider(String trackingNumber, String shippingProviderCode);

    List<ShippingPackage> getShippingPackagesByStatusCode(String statusCode, Date updatedBefore, Date createdBefore);

    /**
     * @param request
     * @return
     */
    List<ShippingManifest> searchManifest(SearchManifestRequest request);

    /**
     * @param request
     * @return
     */
    Long getManifestCount(SearchManifestRequest request);

    /**
     * @param shipmentCode
     * @return
     */
    ShippingPackage searchShipmentByCode(String shipmentCode);

    /**
     * @param code
     * @return
     */
    ShippingPackage getShippingPackageByCode(String code);

    /**
     * @param shippingManifestCode
     * @return
     */
    ShippingManifest getShippingManifestDetailedByCode(String shippingManifestCode);

    /**
     * @param shippingPackage
     * @return
     */
    ShippingManifest getShippingManifestForShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param shippingPackage
     * @return
     */
    ReturnManifestItem getReturnManifestForShippingPackage(ShippingPackage shippingPackage);

    /**
     * @param trackingNumber
     * @param shippingProviderId
     * @return
     */
    ShipmentTracking getShipmentTracking(String trackingNumber, Integer shippingProviderId);

    List<ShippingPackage> getShippingPackagesDispatchedInDateRange(DateRange range);

    /**
     * @param request
     * @return
     */
    List<ShippingProviderLocation> searchShippingProviderLocation(SearchShippingProviderLocationRequest request);

    /**
     * @param request
     * @return
     */
    Long getShippingProviderLocationCount(SearchShippingProviderLocationRequest request);

    Set<Integer> getServiceableByFacilities(Integer shippingMethodId, String pincode);

    List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId);

    List<ShippingProviderMethod> getEnabledShippingProviderMethods(int shippingProviderId);

    List<String> getServiceablePincodesByShippingProviderMethod(Integer shippingProviderId, Integer shippingMethodId);

    void updateNonServiceablePincodes(Integer shippingProviderId, Integer shippingMethodId, Set<String> pincodes);

    /**
     * @param saleOrderCode
     * @return
     */
    List<ShippingPackage> getShippingPackages(String saleOrderCode);

    List<ShippingPackage> getShippingPackagesByIds(List<Object> ids);

    ShippingProviderMethod getShippingProviderMethod(String providerCode, String methodName);

    ShippingProviderMethod getShippingProviderMethod(String providerCode, String methodName, Integer facilityId);

    List<ShippingManifestStatus> getShippingManifestStatuses();

    List<ShippingProvider> getShippingProviders();

    List<ShippingProviderAllocationRule> getShippingProviderAllocationRules();

    List<ShippingProviderMethod> getShippingProviderMethods();

    List<ShippingMethod> getShippingMethods();

    List<ShippingPackageType> getShippingPackageTypes();

    List<ShippingPackageStatus> getShippingPackageStatuses();

    List<ShipmentTrackingStatus> getShipmentTrackingStatuses();

    public void removeManifestItem(ShippingManifestItem shippingManifestItem);

    List<ShippingProvider> lookupShippingProviders(String keyword);

    List<ShippingPackage> getShippingPackages(GetShippingPackagesRequest request);

    ShippingProviderLocation updateShippingProviderLocation(ShippingProviderLocation shippingProviderLocation);

    ShippingManifest getShippingManifestByCode(String shippingManifestCode);

    List<ShippingPackage> getShippingPackagesByTrackingNumber(String trackingNumber);

    /* (non-Javadoc)
     * @see com.uniware.dao.shipping.IShippingDao#getShipmentsForStatusTracking(int)
     */
    List<ShipmentTracking> getShipmentsForTracking(int shippingProviderId, List<String> statusCodes, int start, int pageSize);

    ShipmentBatch getShipmentBatchByCode(String shipmentBatchCode);

    ShipmentBatch addShipmentBatch(ShipmentBatch shipmentBatch);

    int updateShipmentBatch(List<String> shippingPackageCodes, String code);

    ShipmentBatch getShipmentBatchByCode(String shipmentBatchCode, boolean lock);

    List<ShipmentBatch> getShipmentBatchesByCode(String statusCode);

    List<SaleOrderItem> getSaleOrderItemsForDetaling(List<String> shippingPackageCodes);

    List<Channel> getChannelsForManifest();

    ShipmentBatch createShipmentBatch(ShipmentBatch shipmentBatch);

    List<ShippingPackage> getShippingPackagesBySaleOrder(String saleOrderCode);

    List<ShippingPackage> getAllShippingPackagesBySaleOrder(String saleOrderCode);

    ShippingPackage getShippingPackageWithSaleOrderByCode(String shippingPackageCode);

    int getReturnsCountBySaleOrder(String saleOrderCode);

    int getInvoiceCountBySaleOrder(String saleOrderCode);

    int getShipmentCountBySaleOrder(String saleOrderCode);

    ShippingPackage getShippingPackageByShippingProviderAndTrackingNumber(String shippingProviderCode, String trackingNumber);

    List<ShippingPackage> getEligibleShippingPackagesForShippingManifest(ShippingManifest shippingManifest);

    List<Object[]> getEligibleShippingProvidersForManifest(String channelCode);

    ShippingPackage getShippingPackage(String shippingPackageCode, String shippingProviderCode, String trackingNumber);

    ShippingPackage getShippingPackageByCode(String shippingPackageCode, boolean markSaleOrderDirty);

    List<ShippingProviderLocation> getLocationforShippingProvider(String shippingProviderCode);

    int deleteLocationsforShippingProvider(int shippingProviderId);

    List<ShippingProviderMethod> getShippingProviderMethods(int shippingProviderId, int facilityId);

    List<String> getServiceablePincodesByShippingProviderMethod(Integer shippingProviderId, Integer shippingMethodId, String facilityCode);

    void updateNonServiceablePincodes(Integer shippingProviderId, Integer shippingMethodId, Set<String> pincodes, int facilityId);

    ShippingProviderLocation getShippingProviderLocation(String shippingMethodName, String shippingProviderCode, String pincode, String facilityCode);

    List<ShippingPackage> getPaginatedShippingPackagesByStatusCode(String statusCode, PageRequest request);

    List<ShippingProviderLocation> getShippingProviderLocationsByFacilityCodeAndUpdatedTime(Integer facilityId, Date updatedAfter);

    Long getPicklistPendingForShelfCount(String shelfCode);

    ShippingPackage getShippingPackageByReturnInvoiceId(Integer returnInvoiceId);

    ShippingPackage getShippingPackageWithSaleOrderItemsForPickSet(String packageCode, int pickSetId);
}
