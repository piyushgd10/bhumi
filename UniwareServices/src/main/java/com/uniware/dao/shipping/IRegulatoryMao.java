/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Mar-2012
 *  @author vibhu
 */
package com.uniware.dao.shipping;

import java.util.List;

import com.uniware.core.entity.RegulatoryForm;

public interface IRegulatoryMao {

    List<RegulatoryForm> getAllStateRegulatoryForms();

}