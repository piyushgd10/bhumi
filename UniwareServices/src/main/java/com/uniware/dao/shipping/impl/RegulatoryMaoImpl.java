/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 01-Mar-2012
 *  @author vibhu
 */
package com.uniware.dao.shipping.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.RegulatoryForm;
import com.uniware.dao.shipping.IRegulatoryMao;

@Repository
public class RegulatoryMaoImpl implements IRegulatoryMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<RegulatoryForm> getAllStateRegulatoryForms() {
        return mongoOperations.findAll(RegulatoryForm.class);
    }
}
