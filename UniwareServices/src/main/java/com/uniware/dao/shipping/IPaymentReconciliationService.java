/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 18, 2011
 *  @author singla
 */
package com.uniware.dao.shipping;

import java.util.List;

import com.uniware.core.api.admin.shipping.EditPaymentReconciliationRequest;
import com.uniware.core.api.admin.shipping.EditPaymentReconciliationResponse;
import com.uniware.core.api.admin.shipping.PaymentReconciliationDTO;
import com.uniware.core.api.shipping.MarkPackagesReconciledRequest;
import com.uniware.core.api.shipping.MarkPackagesReconciledResponse;
import com.uniware.core.api.shipping.PaymentReconciliationRequest;
import com.uniware.core.api.shipping.PaymentReconciliationResponse;

/**
 * @author singla
 */
public interface IPaymentReconciliationService {

    /**
     * @param paymentMethodCode
     */
    void reconcileShipmentPayments(String paymentMethodCode);

    /**
     * @return
     */
    List<PaymentReconciliationDTO> getPaymentReconciliations();

    /**
     * @param request
     * @return
     */
    PaymentReconciliationResponse reconcileShipmentPayment(PaymentReconciliationRequest request);

    EditPaymentReconciliationResponse editPaymentReconciliation(EditPaymentReconciliationRequest request);

    MarkPackagesReconciledResponse markPackagesReconciled(MarkPackagesReconciledRequest request);

}
