/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.channel;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;

import java.util.Date;
import java.util.List;

public interface IChannelCatalogDao {

    ChannelItemType addChannelItemType(ChannelItemType channelItemType);

    boolean deleteChannelItemType(int channelId, String channelProductId);

    long getChannelItemTypeCount(int channelId, ListingStatus status);

    List<ChannelItemType> getChannelItemTypesByItemTypeId(int itemTypeId);

    boolean markChannelItemTypeUnlinked(int channelId, String channelProductId);

    ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId, boolean lock);

    ChannelItemType getChannelItemTypeByChannelAndSellerSku(int channelId, String skuCode);

    ChannelItemType update(ChannelItemType channelItemType);

    long getLinkedChannelItemTypeCount(int channelId);

    long getUnlinkedChannelItemTypeCount(int channelId);

    List<ChannelItemType> getChannelItemTypesByChannelAndItemTypeId(int channelId, int itemTypeId);

    long getIgnoredChannelItemTypeCount(int channelId);

    List<ChannelItemType> getDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated, ChannelItemType.Status statusCode);

    List<ChannelItemType> getDirtyStockoutChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated, ChannelItemType.Status statusCode);

    long getDirtyChannelItemTypeCount(int channelId, Date lastUpdated, ChannelItemType.Status statusCode);

    long getDirtyStockoutChannelItemTypeCount(int channelId, Date lastUpdated, ChannelItemType.Status statusCode);

    boolean markChannelItemTypesForRecalculation(Integer channelId);

    boolean resetFailedChannelItemTypes(Integer channelId);

    List<ChannelItemType> getPendingChannelItemTypeForRecalculation(int start, int pageSize);

    void resetChannelPendency(Integer channelId);

    boolean markAllChannelItemTypesForRecalculation(Integer channelId);

    List<ChannelItemType> getUnlinkedChannelItemTypesBySellerSku(String skuCode);

    List<ChannelItemType> getPossiblyDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated);

    long getPossiblyDirtyChannelItemTypeCount(int channelId, Date lastUpdated);

    boolean resetFailedOrderInventory();

    boolean updateChannelItemTypeIfFresh(ChannelItemType cit);

    Date getLatestDirtyTimestamp(int channelId, ChannelItemType.Status statusCode);

    Date getLatestPossiblyDirtyTimestamp(int channelId);

    Date getLatestStockoutDirtyTimestamp(int channelId, ChannelItemType.Status statusCode);

    List<ChannelItemType> lookupLinkedChannelItemTypes(Integer channelId, String keyword);

    long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus);

    long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus, DateRange dateRange);

    ChannelItemType getChannelItemTypeById(int channelItemTypeId, boolean lock);

    List<ChannelItemType> getChannelItemTypesWithPendency(int channelId);

    List<ChannelItemType> getChannelItemTypeBySellerSkuCode(String sellerSkuCode);

    int disableStaleCatalog(String syncId, Integer channelId);
}
