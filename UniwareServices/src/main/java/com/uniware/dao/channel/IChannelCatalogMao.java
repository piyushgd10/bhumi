/**
 * 
 */
package com.uniware.dao.channel;

import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.core.vo.ChannelItemTypeVO;

/**
 * @author Sunny
 */
public interface IChannelCatalogMao {

    ChannelItemTypeVO getChannelItemTypeByChannelAndChannelProductId(String channelCode, String productCode);

    ChannelItemTypeVO update(ChannelItemTypeVO channelItemTypeVO);

    void deleteChannelItemType(String channelCode, String channelProductId);

    ChannelCatalogSyncMetadataVO save(ChannelCatalogSyncMetadataVO channelCatalogSyncMetadata);

    ChannelCatalogSyncMetadataVO getChannelCatalogSyncMetadata(String sourceCode);

    ChannelItemTypeTaxVO getChannelItemTypeTax(String channelCode, String channelProductId);

    ChannelItemTypeTaxVO update(ChannelItemTypeTaxVO channelItemTypeTax);

}
