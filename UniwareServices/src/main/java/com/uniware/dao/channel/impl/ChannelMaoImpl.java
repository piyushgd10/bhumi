/**
 * 
 */
package com.uniware.dao.channel.impl;

import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.ChannelOrderStatusDTO;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.entity.Channel;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelCaptchaVO;
import com.uniware.core.vo.ChannelWarehouseInventorySyncStatusVO;
import com.uniware.core.vo.SourceSaleOrderImportInstructionsVO;
import com.uniware.dao.channel.IChannelMao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author parijat
 */
@Repository
public class ChannelMaoImpl implements IChannelMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public ChannelOrderSyncStatusDTO save(ChannelOrderSyncStatusDTO channelOrderSyncStatus) {
        channelOrderSyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelOrderSyncStatus);
        return channelOrderSyncStatus;
    }
    
    @Override
    public ChannelPricingPushStatusDTO save(ChannelPricingPushStatusDTO channelPricingSyncStatus) {
        channelPricingSyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelPricingSyncStatus);
        return channelPricingSyncStatus;
    }


    @Override
    public ChannelOrderSyncStatusDTO getChannelOrderSyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelOrderSyncStatusDTO.class);
    }

    @Override
    public void resetChannelOrderSyncStatus() {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),
                Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0).set("syncExecutionStatus", Channel.SyncExecutionStatus.IDLE), ChannelOrderSyncStatusDTO.class);
    }

    @Override
    public List<ChannelOrderSyncStatusDTO> getChannelOrderSyncStatuses() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelOrderSyncStatusDTO.class);
    }

    @Override
    public void clearChannelOrderSyncStatuses() {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelOrderSyncStatusDTO.class);
    }

    @Override
    public ChannelInventorySyncStatusDTO save(ChannelInventorySyncStatusDTO channelInventorySyncStatus) {
        channelInventorySyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelInventorySyncStatus);
        return channelInventorySyncStatus;
    }

    @Override
    public ChannelInventorySyncStatusDTO getChannelInventorySyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelInventorySyncStatusDTO.class);
    }

    @Override
    public ChannelReconciliationInvoiceSyncStatusVO getChannelReconciliationSyncStatus(final String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelReconciliationInvoiceSyncStatusVO.class);
    }

    @Override
    public void clearChannelInventorySyncStatuses() {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelInventorySyncStatusDTO.class);
    }

    @Override
    public List<ChannelInventorySyncStatusDTO> getChannelInventorySyncStatuses() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelInventorySyncStatusDTO.class);
    }

    @Override
    public ChannelCatalogSyncStatusDTO save(ChannelCatalogSyncStatusDTO channelCatalogSyncStatus) {
        channelCatalogSyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelCatalogSyncStatus);
        return channelCatalogSyncStatus;
    }

    @Override
    public ChannelWarehouseInventorySyncStatusVO save(ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus) {
        channelWarehouseInventorySyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelWarehouseInventorySyncStatus);
        return channelWarehouseInventorySyncStatus;
    }

    @Override
    public ChannelReconciliationInvoiceSyncStatusVO save(ChannelReconciliationInvoiceSyncStatusVO channelStatus) {
        channelStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelStatus);
        return channelStatus;
    }

    @Override
    public ChannelCatalogSyncStatusDTO getChannelCatalogSyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelCatalogSyncStatusDTO.class);
    }

    @Override
    public ChannelWarehouseInventorySyncStatusVO getChannelWarehouseInventorySyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelWarehouseInventorySyncStatusVO.class);
    }

    @Override
    public ChannelOrderStatusDTO getOrderSyncStatusOnChannel(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelOrderStatusDTO.class);
    }

    @Override
    public void resetChannelCatalogSyncStatus() {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),
                Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0).set("syncExecutionStatus", Channel.SyncExecutionStatus.IDLE), ChannelCatalogSyncStatusDTO.class);
    }

    @Override
    public void resetStatusSyncOnchannel() {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),
                Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0), ChannelOrderStatusDTO.class);
    }

    @Override
    public void resetChannelReconciliationSyncStatus() {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),
                Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0), ChannelReconciliationInvoiceSyncStatusVO.class);
    }

    @Override
    public List<ChannelCatalogSyncStatusDTO> getChannelCatalogSyncStatuses() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelCatalogSyncStatusDTO.class);
    }

    @Override
    public void clearChannelCatalogSyncStatuses() {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelCatalogSyncStatusDTO.class);
    }

    @Override
    public ChannelOrderStatusDTO save(ChannelOrderStatusDTO channelShipmentSyncStatus) {
        channelShipmentSyncStatus.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelShipmentSyncStatus);
        return channelShipmentSyncStatus;
    }

    @Override
    public ChannelOrderStatusDTO getChannelShipmentSyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelOrderStatusDTO.class);
    }

    @Override
    public List<ChannelOrderStatusDTO> getChannelShipmentSyncStatuses() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelOrderStatusDTO.class);
    }

    @Override
    public void clearChannelShipmentSyncStatuses() {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelOrderStatusDTO.class);
    }

    @Override
    public List<SourceSaleOrderImportInstructionsVO> getSourceSaleOrderImportInstructions() {
        return mongoOperations.findAll(SourceSaleOrderImportInstructionsVO.class);
    }

    @Override
    public List<SourceSaleOrderImportInstructionsVO> getSourceImportInstructionsByType(String importType) {
        Query query = new Query(Criteria.where("importType").is(importType));
        return mongoOperations.find(query, SourceSaleOrderImportInstructionsVO.class);
    }

    @Override
    public void addSourceSaleOrderImportInstructionsVO(SourceSaleOrderImportInstructionsVO sourceSaleOrderImportInstructionsVO) {
        mongoOperations.save(sourceSaleOrderImportInstructionsVO);
    }

    @Override
    public ChannelCatalogSyncMetadataVO save(ChannelCatalogSyncMetadataVO channelCatalogSyncPreprocessor) {
        channelCatalogSyncPreprocessor.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelCatalogSyncPreprocessor);
        return channelCatalogSyncPreprocessor;
    }

    @Override
    public ChannelCatalogSyncMetadataVO getChannelCatalogSyncPreprocessor(String sourceCode) {
        return mongoOperations.findOne(new Query(Criteria.where("sourceCode").is(sourceCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelCatalogSyncMetadataVO.class);
    }

    public ChannelCaptchaVO getChannelCaptcha(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelCaptchaVO.class);
    }

    @Override
    public void removeChannelCaptcha(String channelCode) {
        mongoOperations.remove(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelCaptchaVO.class);
    }

    @Override
    public void save(ChannelCaptchaVO channelCaptcha) {
        channelCaptcha.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelCaptcha);
    }
    
    @Override
    public ChannelPricingPushStatusDTO getChannelPriceSyncStatus(String channelCode) {
        return mongoOperations.findOne(new Query(Criteria.where("channelCode").is(channelCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelPricingPushStatusDTO.class);
    }

    @Override
    public List<ChannelPricingPushStatusDTO> getChannelPriceSyncStatuses() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), ChannelPricingPushStatusDTO.class);
    }
}
