/**
 * 
 */
package com.uniware.dao.channel.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelItemTypeTaxVO;
import com.uniware.core.vo.ChannelItemTypeVO;
import com.uniware.dao.channel.IChannelCatalogMao;

/**
 * @author Sunny
 */
@Repository
public class ChannelCatalogMaoImpl implements IChannelCatalogMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public ChannelItemTypeVO getChannelItemTypeByChannelAndChannelProductId(String channelCode, String channelProductId) {
        return mongoOperations.findOne(
                new Query(Criteria.where("channelCode").is(channelCode).and("channelProductId").is(channelProductId).and("tenantCode").is(
                        UserContext.current().getTenant().getCode())), ChannelItemTypeVO.class);
    }

    @Override
    public void deleteChannelItemType(String channelCode, String channelProductId) {
        mongoOperations.remove(
                new Query(Criteria.where("channelCode").is(channelCode).and("channelProductId").is(channelProductId).and("tenantCode").is(
                        UserContext.current().getTenant().getCode())), ChannelItemTypeVO.class);
    }

    @Override
    public ChannelItemTypeVO update(ChannelItemTypeVO channelItemTypeVO) {
        channelItemTypeVO.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.remove(
                new Query(
                        Criteria.where("channelCode").is(channelItemTypeVO.getChannelCode()).and("channelProductId").is(channelItemTypeVO.getChannelProductId()).and("tenantCode").is(
                                UserContext.current().getTenant().getCode())), ChannelItemTypeVO.class);
        mongoOperations.save(channelItemTypeVO);
        return channelItemTypeVO;
    }

    @Override
    public ChannelCatalogSyncMetadataVO save(ChannelCatalogSyncMetadataVO channelCatalogSyncMetadata) {
        channelCatalogSyncMetadata.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.remove(
                new Query(Criteria.where("sourceCode").is(channelCatalogSyncMetadata.getSourceCode()).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelCatalogSyncMetadataVO.class);
        mongoOperations.save(channelCatalogSyncMetadata);
        return channelCatalogSyncMetadata;
    }

    @Override
    public ChannelCatalogSyncMetadataVO getChannelCatalogSyncMetadata(String sourceCode) {
        return mongoOperations.findOne(new Query(Criteria.where("sourceCode").is(sourceCode).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                ChannelCatalogSyncMetadataVO.class);
    }

    @Override
    public ChannelItemTypeTaxVO getChannelItemTypeTax(String channelCode, String channelProductId) {
        return mongoOperations.findOne(
                new Query(Criteria.where("channelCode").is(channelCode).and("channelProductId").is(channelProductId).and("tenantCode").is(
                        UserContext.current().getTenant().getCode())), ChannelItemTypeTaxVO.class);
    }

    @Override
    public ChannelItemTypeTaxVO update(ChannelItemTypeTaxVO channelItemTypeTax) {
        channelItemTypeTax.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelItemTypeTax);
        return channelItemTypeTax;
    }

}
