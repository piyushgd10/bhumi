/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.channel.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemType.ListingStatus;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.channel.IChannelCatalogDao;

@Repository
public class ChannelCatalogDaoImpl implements IChannelCatalogDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ChannelItemType addChannelItemType(ChannelItemType channelItemType) {
        channelItemType.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().merge(channelItemType);
        return channelItemType;
    }

    @Override
    public boolean deleteChannelItemType(int channelId, String channelProductId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "delete from ChannelItemType where tenant.id = :tenantId and channelProductId = :channelProductId and channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelProductId", channelProductId);
        query.setParameter("channelId", channelId);
        return query.executeUpdate() > 0;
    }

    @Override
    public long getChannelItemTypeCount(int channelId, ListingStatus listingStatus) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.listingStatus = :listingStatus");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("listingStatus", listingStatus);
        return ((Long) query.uniqueResult());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getChannelItemTypesByChannelAndItemTypeId(int channelId, int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.itemType.id = :itemTypeId and cit.channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("channelId", channelId);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getChannelItemTypesByItemTypeId(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.itemType.id = :itemTypeId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public boolean markChannelItemTypeUnlinked(int channelId, String channelProductId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType cit set cit.statusCode = :statusCode, cit.itemType = null where cit.tenant.id = :tenantId and cit.channelProductId = :channelProductId and cit.channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.UNLINKED);
        query.setParameter("channelProductId", channelProductId);
        query.setParameter("channelId", channelId);
        return query.executeUpdate() > 0;
    }

    @Override
    public ChannelItemType getChannelItemTypeByChannelAndChannelProductId(int channelId, String channelProductId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channelProductId = :channelProductId and cit.channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelProductId", channelProductId);
        query.setParameter("channelId", channelId);
        if (lock) {
            query.setLockMode("cit", LockMode.PESSIMISTIC_WRITE);
        }
        return (ChannelItemType) query.uniqueResult();
    }

    @Override
    public ChannelItemType update(ChannelItemType channelItemType) {
        channelItemType.setTenant(UserContext.current().getTenant());
        return (ChannelItemType) sessionFactory.getCurrentSession().merge(channelItemType);
    }

    @Override
    public boolean resetFailedOrderInventory() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType cit set cit.failedOrderInventory = :failedOrderInventory where cit.tenant.id = :tenantId and cit.failedOrderInventory > 0");
        query.setParameter("failedOrderInventory", 0);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean updateChannelItemTypeIfFresh(ChannelItemType cit) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType cit set cit.listingStatus = :listingStatus, cit.dirty = :dirty, cit.possiblyDirty = :possiblyDirty, cit.failedCount = :failedCount, cit.disabledDueToErrors = :disabledDueToErrors, cit.lastInventoryUpdate = :lastInventoryUpdate, cit.lastInventoryUpdateStatus = :lastInventoryUpdateStatus, cit.lastMessage = :lastMessage, cit.lastInventoryUpdateAt = :lastInventoryUpdateAt where cit.tenant.id = :tenantId and cit.channelProductId = :channelProductId and cit.channel.id = :channelId and cit.updated = :updated");
        query.setParameter("listingStatus", cit.getListingStatus());
        query.setParameter("dirty", cit.isDirty());
        query.setParameter("possiblyDirty", cit.isPossiblyDirty());
        query.setParameter("failedCount", cit.getFailedCount());
        query.setParameter("disabledDueToErrors", cit.isDisabledDueToErrors());
        query.setParameter("lastInventoryUpdate", cit.getLastInventoryUpdate());
        query.setParameter("lastInventoryUpdateStatus", cit.getLastInventoryUpdateStatus());
        query.setParameter("lastMessage", cit.getLastMessage());
        query.setParameter("lastInventoryUpdateAt", cit.getLastInventoryUpdateAt());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelProductId", cit.getChannelProductId());
        query.setParameter("channelId", cit.getChannel().getId());
        query.setParameter("updated", cit.getUpdated());
        return query.executeUpdate() > 0;
    }

    @Override
    public long getLinkedChannelItemTypeCount(int channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.statusCode = :statusCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        return ((Long) query.uniqueResult());
    }

    @Override
    public long getUnlinkedChannelItemTypeCount(int channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.statusCode = :statusCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("statusCode", ChannelItemType.Status.UNLINKED);
        return ((Long) query.uniqueResult());
    }

    @Override
    public long getIgnoredChannelItemTypeCount(int channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.statusCode = :statusCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("statusCode", ChannelItemType.Status.IGNORED);
        return ((Long) query.uniqueResult());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId  and cit.channel.id = :channelId and cit.disabledDueToErrors = :disabledDueToErrors and disabled = :disabled and cit.dirty = :dirty and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("disabled", false);
        query.setParameter("lastUpdated", lastUpdated);
        query.setMaxResults(batchSize);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public Date getLatestDirtyTimestamp(int channelId, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select max(cit.updated) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.disabledDueToErrors = :disabledDueToErrors and cit.channel.id = :channelId and cit.dirty = :dirty");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        return ((Date) query.uniqueResult());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getPossiblyDirtyChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.disabledDueToErrors = :disabledDueToErrors and cit.channel.id = :channelId and cit.possiblyDirty = :possiblyDirty  and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("channelId", channelId);
        query.setParameter("possiblyDirty", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("lastUpdated", lastUpdated);
        query.setMaxResults(batchSize);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public Date getLatestPossiblyDirtyTimestamp(int channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select max(cit.updated) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.disabledDueToErrors = :disabledDueToErrors and cit.channel.id = :channelId and cit.possiblyDirty = :possiblyDirty");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("channelId", channelId);
        query.setParameter("possiblyDirty", true);
        query.setParameter("disabledDueToErrors", false);
        return ((Date) query.uniqueResult());
    }

    @Override
    public long getPossiblyDirtyChannelItemTypeCount(int channelId, Date lastUpdated) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit.id) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.possiblyDirty = :possiblyDirty and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("channelId", channelId);
        query.setParameter("possiblyDirty", true);
        query.setParameter("lastUpdated", lastUpdated);
        return (Long) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getDirtyStockoutChannelItemTypes(int channelId, int start, int batchSize, Date lastUpdated, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.dirty = :dirty  and cit.disabledDueToErrors = :disabledDueToErrors and cit.nextInventoryUpdate = 0 and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("lastUpdated", lastUpdated);
        query.setMaxResults(batchSize);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public Date getLatestStockoutDirtyTimestamp(int channelId, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select max(cit.updated) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.disabledDueToErrors = :disabledDueToErrors and cit.channel.id = :channelId and cit.dirty = :dirty and cit.nextInventoryUpdate = 0");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        return ((Date) query.uniqueResult());
    }

    @Override
    public long getDirtyChannelItemTypeCount(int channelId, Date lastUpdated, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit.id) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.disabledDueToErrors = :disabledDueToErrors and cit.dirty = :dirty and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("lastUpdated", lastUpdated);
        return (Long) query.uniqueResult();
    }

    @Override
    public long getDirtyStockoutChannelItemTypeCount(int channelId, Date lastUpdated, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit.id) from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.dirty = :dirty and cit.channel.id = :channelId and cit.disabledDueToErrors = :disabledDueToErrors and cit.nextInventoryUpdate = 0  and cit.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("lastUpdated", lastUpdated);
        return (Long) query.uniqueResult();
    }

    @Override
    public boolean markChannelItemTypesForRecalculation(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType set recalculateInventory = :recalculateInventory, disabledDueToErrors = :disabledDueToErrors, failedCount = :failedCount where statusCode = :statusCode and tenant.id = :tenantId and channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("recalculateInventory", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("failedCount", 0);
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean markAllChannelItemTypesForRecalculation(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType set lastInventoryUpdate = null, lastMessage = null, recalculateInventory = :recalculateInventory, disabledDueToErrors = :disabledDueToErrors, failedCount = :failedCount where statusCode = :statusCode and tenant.id = :tenantId and channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("recalculateInventory", true);
        query.setParameter("disabledDueToErrors", false);
        query.setParameter("failedCount", 0);
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean resetFailedChannelItemTypes(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType set dirty = :dirty where tenant.id = :tenantId and channel.id = :channelId and failedCount > 0 and disabledDueToErrors = :disabledDueToErrors");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToErrors", false);
        return query.executeUpdate() > 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getPendingChannelItemTypeForRecalculation(int start, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.recalculateInventory = :recalculateInventory");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("recalculateInventory", true);
        query.setFirstResult(start);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @Override
    public void resetChannelPendency(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType set pendency = 0 where tenant.id = :tenantId and channel.id = :channelId and pendency > 0");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> getUnlinkedChannelItemTypesBySellerSku(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.sellerSkuCode = :skuCode and cit.statusCode = :statusCode and cit.tenant.id = :tenantId");
        query.setParameter("skuCode", skuCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.UNLINKED);
        return query.list();
    }

    @Override
    public ChannelItemType getChannelItemTypeByChannelAndSellerSku(int channelId, String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.sellerSkuCode = :skuCode and cit.channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("channelId", channelId);
        List<ChannelItemType> channelItemTypes = query.list();
        if (!channelItemTypes.isEmpty()) {
            return (ChannelItemType) channelItemTypes.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemType> lookupLinkedChannelItemTypes(Integer channelId, String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.statusCode = :statusCode and (cit.itemType.skuCode like :skuCode or cit.channelProductId like :channelProductId)");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("channelProductId", "%" + keyword + "%");
        query.setParameter("skuCode", "%" + keyword + "%");
        return query.list();
    }

    @Override
    public long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.lastInventoryUpdateStatus = :lastInventoryUpdateStatus");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("lastInventoryUpdateStatus", lastInventoryUpdateStatus);
        return ((Long) query.uniqueResult());
    }

    @Override
    public long getChannelItemTypeCount(int channelId, String lastInventoryUpdateStatus, DateRange lastInventoryUpdateDateRange) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(cit) from ChannelItemType cit where cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.lastInventoryUpdateStatus = :lastInventoryUpdateStatus and cit.lastInventoryUpdateAt between :start and :end");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("lastInventoryUpdateStatus", lastInventoryUpdateStatus);
        query.setParameter("start", lastInventoryUpdateDateRange.getStart());
        query.setParameter("end", lastInventoryUpdateDateRange.getEnd());
        return ((Long) query.uniqueResult());
    }

    @Override
    public ChannelItemType getChannelItemTypeById(int channelItemTypeId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select cit from ChannelItemType cit where id = :id");
        query.setParameter("id", channelItemTypeId);
        if (lock) {
            query.setLockMode("cit", LockMode.PESSIMISTIC_WRITE);
        }
        return (ChannelItemType) query.uniqueResult();
    }

    @Override
    public List<ChannelItemType> getChannelItemTypesWithPendency(int channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cit from ChannelItemType cit where cit.statusCode = :statusCode and cit.tenant.id = :tenantId and cit.channel.id = :channelId and cit.pendency > 0");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", ChannelItemType.Status.LINKED);
        query.setParameter("channelId", channelId);
        return query.list();
    }

    @Override
    public List<ChannelItemType> getChannelItemTypeBySellerSkuCode(String sellerSkuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select cit from ChannelItemType cit where cit.tenant.id = :tenantId and cit.sellerSkuCode = :sellerSkuCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("sellerSkuCode", sellerSkuCode);
        return query.list();
    }

    @Override
    public int disableStaleCatalog(String syncId, Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ChannelItemType cit set cit.catalogSyncId = :newCatalogSyncStatus, cit.listingStatus = :statusCode, cit.disabled = true where cit.tenant.id = :tenantId and cit.channel.id = :channelId and (cit.catalogSyncId not in (:catalogSyncIds) or cit.catalogSyncId is null) and cit.verified = 1");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("statusCode", ListingStatus.INACTIVE);
        query.setParameter("newCatalogSyncStatus", ChannelItemType.SyncedBy.AUTO_DISABLED.name());
        query.setParameterList("catalogSyncIds", Arrays.asList(syncId, ChannelItemType.SyncedBy.MANUAL.name(), ChannelItemType.SyncedBy.SYSTEM.name()));
        return query.executeUpdate();
    }
}
