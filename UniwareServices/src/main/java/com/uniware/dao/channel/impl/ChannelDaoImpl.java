/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.channel.impl;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelAssociatedFacility;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.Party;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.channel.IChannelDao;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class ChannelDaoImpl implements IChannelDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Channel getChannelByCode(String channelCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Channel c left join fetch c.channelConnectors cc left join fetch cc.channelConnectorParameters left join fetch c.customer where c.code = :channelCode and c.tenant.id = :tenantId order by c.enabled desc");
        query.setParameter("channelCode", channelCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (Channel) query.uniqueResult();
    }

    @Override
    public Channel addChannel(Channel channel) {
        channel.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(channel);
        return channel;
    }

    @Override
    public ChannelConnector addChannelConnector(ChannelConnector channelConnector) {
        return (ChannelConnector) sessionFactory.getCurrentSession().merge(channelConnector);
    }

    @Override
    public Channel updateChannel(Channel channel) {
        return (Channel) sessionFactory.getCurrentSession().merge(channel);
    }

    @Override
    public Channel getChannelByName(String channelName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Channel c left join fetch c.channelParameters where c.name = :channelName and c.tenant.id = :tenantId");
        query.setParameter("channelName", channelName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (Channel) query.uniqueResult();
    }

    @Override
    public List<Channel> getChannels() {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct c from Channel c where c.tenant.id = :tenantId order by c.enabled desc");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        List<Channel> channelList = query.list();
        for (Channel c : channelList) {
            Hibernate.initialize(c.getCustomer());
            Hibernate.initialize(c.getAssociatedFacility());
            for (ChannelConnector cc : c.getChannelConnectors()) {
                Hibernate.initialize(cc.getChannelConnectorParameters());
            }
        }
        return channelList;
    }

    @Override
    public long getChannelRevenue(Integer channelId, Date created) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sum(soi.totalPrice) from SaleOrderItem soi where soi.saleOrder.tenant.id = :tenantId and soi.saleOrder.channel.id = :channelId and soi.created >= :createdStart and soi.created < :createdEnd");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        DateUtils.DateRange dateRange = DateUtils.getDayRange(created);
        query.setParameter("createdStart", dateRange.getStart());
        query.setParameter("createdEnd", dateRange.getEnd());
        Object result = query.uniqueResult();
        return result == null ? 0L : ((BigDecimal) result).longValue();
    }

    @Override
    public List<Party> lookupCustomer(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Customer where tenant.id = :tenantId and (code=:code or name like :name)");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", keyword);
        query.setParameter("name", "%" + keyword + "%");
        query.setMaxResults(10);
        return query.list();
    }

    @Override
    public ChannelConnector updateChannelConnector(ChannelConnector channelConnector) {
        return (ChannelConnector) sessionFactory.getCurrentSession().merge(channelConnector);
    }

    @Override
    public List<Channel> getChannelConnectorStatus(){
        Query query = sessionFactory.getCurrentSession().createQuery("from Channel c left join fetch c.channelConnectors cc where c.tenant.id = :tenantId");
        query.setParameter("tenantId",UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<ChannelAssociatedFacility> getChannelAssociatedFacilities(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ChannelAssociatedFacility caf where caf.channel.id = :channelId");
        query.setParameter("channelId",channelId);
        return query.list();
    }

    @Override
    public ChannelAssociatedFacility addChannelAssociatedFacility(ChannelAssociatedFacility channelAssociatedFacility) {
        sessionFactory.getCurrentSession().persist(channelAssociatedFacility);
        return channelAssociatedFacility;
    }

    @Override
    public void removeChannelAssociatedFacilities(List<Integer> channelAssociatedFacilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from ChannelAssociatedFacility caf where caf.id in (:channelAssociatedFacilityIds)");
        query.setParameterList("channelAssociatedFacilityIds", channelAssociatedFacilityIds);
        query.executeUpdate();
    }
}
