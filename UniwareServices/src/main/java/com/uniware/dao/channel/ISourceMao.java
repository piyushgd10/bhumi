package com.uniware.dao.channel;

import com.uniware.core.entity.SourceColor;
import com.uniware.core.entity.Source;

import java.util.List;

/**
 * Created by sunny on 19/03/15.
 */
public interface ISourceMao {

    List<Source> getAllSources();

    SourceColor getColorBySourceCodeAndCount(String sourceCode, int count);

    List<Source> lookupSources(String keyword);
}
