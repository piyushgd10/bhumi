/**
 * 
 */
package com.uniware.dao.channel;

import com.uniware.core.vo.ChannelWarehouseInventorySyncStatusVO;
import java.util.List;

import com.uniware.core.api.channel.ChannelCatalogSyncMetadataVO;
import com.uniware.core.api.channel.ChannelCatalogSyncStatusDTO;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.api.channel.ChannelOrderStatusDTO;
import com.uniware.core.api.channel.ChannelOrderSyncStatusDTO;
import com.uniware.core.api.channel.ChannelPricingPushStatusDTO;
import com.uniware.core.api.channel.ChannelReconciliationInvoiceSyncStatusVO;
import com.uniware.core.vo.ChannelCaptchaVO;
import com.uniware.core.vo.SourceSaleOrderImportInstructionsVO;

/**
 * @author parijat
 */
public interface IChannelMao {

    ChannelOrderSyncStatusDTO save(ChannelOrderSyncStatusDTO channelOrderSyncStatus);

    ChannelOrderSyncStatusDTO getChannelOrderSyncStatus(String channelCode);

    List<ChannelOrderSyncStatusDTO> getChannelOrderSyncStatuses();

    ChannelInventorySyncStatusDTO save(ChannelInventorySyncStatusDTO channelInventorySyncStatus);

    ChannelInventorySyncStatusDTO getChannelInventorySyncStatus(String channelCode);

    List<ChannelInventorySyncStatusDTO> getChannelInventorySyncStatuses();

    ChannelCatalogSyncStatusDTO save(ChannelCatalogSyncStatusDTO channelCatalogSyncStatus);

    ChannelCatalogSyncStatusDTO getChannelCatalogSyncStatus(String channelCode);

    void resetStatusSyncOnchannel();

    void resetChannelReconciliationSyncStatus();

    List<ChannelCatalogSyncStatusDTO> getChannelCatalogSyncStatuses();

    void clearChannelOrderSyncStatuses();

    ChannelReconciliationInvoiceSyncStatusVO getChannelReconciliationSyncStatus(String channelCode);

    void clearChannelInventorySyncStatuses();

    void clearChannelCatalogSyncStatuses();

    List<SourceSaleOrderImportInstructionsVO> getSourceSaleOrderImportInstructions();

    void addSourceSaleOrderImportInstructionsVO(SourceSaleOrderImportInstructionsVO sourceSaleOrderImportInstructionsVO);

    ChannelOrderStatusDTO save(ChannelOrderStatusDTO channelShipmentSyncStatus);

    ChannelOrderStatusDTO getChannelShipmentSyncStatus(String channelCode);

    List<ChannelOrderStatusDTO> getChannelShipmentSyncStatuses();

    void clearChannelShipmentSyncStatuses();

    List<SourceSaleOrderImportInstructionsVO> getSourceImportInstructionsByType(String importType);

    ChannelCatalogSyncMetadataVO save(ChannelCatalogSyncMetadataVO channelCatalogSyncPreprocessor);

    ChannelCatalogSyncMetadataVO getChannelCatalogSyncPreprocessor(String sourceCode);

    ChannelCaptchaVO getChannelCaptcha(String channelCode);

    void removeChannelCaptcha(String channelCode);

    void save(ChannelCaptchaVO channelCaptcha);

    ChannelWarehouseInventorySyncStatusVO save(
            ChannelWarehouseInventorySyncStatusVO channelWarehouseInventorySyncStatus);

    ChannelReconciliationInvoiceSyncStatusVO save(ChannelReconciliationInvoiceSyncStatusVO channelStatus);

    ChannelWarehouseInventorySyncStatusVO getChannelWarehouseInventorySyncStatus(String channelCode);

    ChannelOrderStatusDTO getOrderSyncStatusOnChannel(String channelCode);

    void resetChannelCatalogSyncStatus();

    void resetChannelOrderSyncStatus();

    ChannelPricingPushStatusDTO getChannelPriceSyncStatus(String channelCode);

    List<ChannelPricingPushStatusDTO> getChannelPriceSyncStatuses();

    ChannelPricingPushStatusDTO save(ChannelPricingPushStatusDTO channelPricingSyncStatus);

}
