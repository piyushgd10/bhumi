/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.channel;

import com.uniware.core.entity.Channel;
import com.uniware.core.entity.ChannelAssociatedFacility;
import com.uniware.core.entity.ChannelConnector;
import com.uniware.core.entity.Party;

import java.util.Date;
import java.util.List;

public interface IChannelDao {

    Channel getChannelByCode(String channelCode);

    Channel addChannel(Channel channel);

    Channel updateChannel(Channel channel);

    List<Channel> getChannels();

    Channel getChannelByName(String channelName);

    List<Party> lookupCustomer(String keyword);

    ChannelConnector addChannelConnector(ChannelConnector channelConnector);

    long getChannelRevenue(Integer channelId, Date date);

    ChannelConnector updateChannelConnector(ChannelConnector channelConnector);

    List<Channel> getChannelConnectorStatus();

    List<ChannelAssociatedFacility> getChannelAssociatedFacilities(Integer channelId);

    ChannelAssociatedFacility addChannelAssociatedFacility(ChannelAssociatedFacility channelAssociatedFacility);

    void removeChannelAssociatedFacilities(List<Integer> channelAssociatedFacilityIds);
}
