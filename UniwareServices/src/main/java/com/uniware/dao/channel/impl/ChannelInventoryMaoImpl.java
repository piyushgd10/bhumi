/**
 * 
 */
package com.uniware.dao.channel.impl;

import java.util.List;

import com.uniware.core.entity.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.api.channel.ChannelInventorySyncStatusDTO;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelInventoryFormulaVO;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;
import com.uniware.core.vo.ChannelInventoryUpdateVO;
import com.uniware.dao.channel.IChannelInventoryMao;

/**
 * @author Sunny
 */
@Repository
public class ChannelInventoryMaoImpl implements IChannelInventoryMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commonMongoOperations;

    @Override
    public void create(ChannelInventoryUpdateVO channelItemTypeInventorySnapshot) {
        channelItemTypeInventorySnapshot.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelItemTypeInventorySnapshot);
    }

    @Override
    public ChannelInventoryUpdateSnapshotVO create(ChannelInventoryUpdateSnapshotVO channelInventoryUpdateSnapshot) {
        channelInventoryUpdateSnapshot.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(channelInventoryUpdateSnapshot);
        return channelInventoryUpdateSnapshot;
    }

    @Override
    public ChannelInventoryUpdateSnapshotVO getInventoryUpdateSnapshotById(String snapshotId) {
        boolean checkInBoth = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isCheckSnapshotInCommonMongoEnabled();
        ChannelInventoryUpdateSnapshotVO snapshot = mongoOperations.findById(snapshotId, ChannelInventoryUpdateSnapshotVO.class);
        if(snapshot == null && checkInBoth){
            snapshot = commonMongoOperations.findById(snapshotId, ChannelInventoryUpdateSnapshotVO.class);
        }
        return snapshot;
    }

    @Override
    public void resetChannelInventorySyncStatus(List<String> tenantCodes) {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").in(tenantCodes)), Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0),
                ChannelInventorySyncStatusDTO.class);
    }

    @Override
    public void resetChannelInventorySyncStatus() {
        mongoOperations.updateMulti(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())),
                Update.update("running", false).set("totalMileStones", 0).set("currentMileStone", 0).set("syncExecutionStatus", Channel.SyncExecutionStatus.IDLE), ChannelInventorySyncStatusDTO.class);
    }

    @Override
    public void create(ChannelInventoryFormulaVO channelInventoryFormula) {
        commonMongoOperations.save(channelInventoryFormula);
    }

    @Override
    public ChannelInventoryFormulaVO getChannelInventoryFormulaByCode(String code) {
        return commonMongoOperations.findOne(new Query(Criteria.where("code").is(code)), ChannelInventoryFormulaVO.class);
    }

    @Override
    public ChannelInventoryFormulaVO getChannelInventoryFormulaByChecksum(String checksum) {
        return commonMongoOperations.findOne(new Query(Criteria.where("checksum").is(checksum)), ChannelInventoryFormulaVO.class);
    }
}
