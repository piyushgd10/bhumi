/**
 *
 */
package com.uniware.dao.channel.impl;

import com.uniware.core.entity.SourceColor;
import com.uniware.core.entity.Source;
import com.uniware.dao.channel.ISourceMao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Sunny
 */
@Repository
public class SourceMaoImpl implements ISourceMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<Source> getAllSources() {
        Query query = new Query(Criteria.where("enabled").is(true));
        return mongoOperations.find(query, Source.class);
    }

    @Override
    public SourceColor getColorBySourceCodeAndCount(String sourceCode, int count) {
        Query query = new Query(Criteria.where("sourceCode").is(sourceCode).and("priority").is(count));
        return mongoOperations.findOne(query, SourceColor.class);
    }

    @Override
    public List<Source> lookupSources(String keyword) {
        Query query = new Query(Criteria.where("sourceCode").regex(keyword, "i").and("enabled").is(true));
        return mongoOperations.find(query, Source.class);
    }
}
