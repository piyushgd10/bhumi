/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.channel;

import java.util.List;

import com.uniware.core.vo.ChannelInventoryFormulaVO;
import com.uniware.core.vo.ChannelInventoryUpdateSnapshotVO;
import com.uniware.core.vo.ChannelInventoryUpdateVO;

public interface IChannelInventoryMao {

    void create(ChannelInventoryUpdateVO citis);

    ChannelInventoryUpdateSnapshotVO create(ChannelInventoryUpdateSnapshotVO citis);

    ChannelInventoryUpdateSnapshotVO getInventoryUpdateSnapshotById(String snapshotId);

    void resetChannelInventorySyncStatus(List<String> tenantCodes);

    void resetChannelInventorySyncStatus();

    void create(ChannelInventoryFormulaVO channelInventoryFormula);

    ChannelInventoryFormulaVO getChannelInventoryFormulaByCode(String code);

    ChannelInventoryFormulaVO getChannelInventoryFormulaByChecksum(String checksum);
}
