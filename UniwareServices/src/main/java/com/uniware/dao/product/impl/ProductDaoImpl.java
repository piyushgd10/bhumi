/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Apr-2013
 *  @author unicom
 */
package com.uniware.dao.product.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Product;
import com.uniware.dao.product.IProductDao;

@Repository
public class ProductDaoImpl implements IProductDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Product getProductByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Product where code = :code");
        query.setParameter("code", code);
        return (Product) query.uniqueResult();
    }
}
