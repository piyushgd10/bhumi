/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 21/09/17
 * @author aditya
 */
package com.uniware.dao.ledger.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.CollectionUtils;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.InventoryLedger;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.ledger.IInventoryLedgerDao;

@Repository(value = "inventoryLedgerDao")
public class InventoryLedgerDaoImpl implements IInventoryLedgerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public InventoryLedger getInventoryLedger(int inventoryLedgerId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InventoryLedger.class).add(Restrictions.idEq(inventoryLedgerId));
        return (InventoryLedger) criteria.uniqueResult();
    }

    @Override
    public List<InventoryLedger> getInventoryLedgerEntries(Date from, int start, int pageSize) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InventoryLedger.class).add(Restrictions.le("created", from)).add(
                Restrictions.eq("statusCode", InventoryLedger.StatusCode.LIVE)).setFirstResult(start).setMaxResults(pageSize);
        //noinspection unchecked
        return criteria.list();

    }

    @Override
    public List<Integer> getFacilitiesForItemType(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct il.facility.id from InventoryLedger where il.skuCode = :skuCode");
        query.setParameter("skuCode", skuCode);
        //noinspection unchecked
        return query.list();
    }

    @Override
    public InventoryLedger addInventoryLedgerEntry(InventoryLedger ledgerEntry) {
        ledgerEntry.setFacility(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(UserContext.current().getFacilityId()));
        sessionFactory.getCurrentSession().persist(ledgerEntry);
        return ledgerEntry;
    }

    @Override
    public InventoryLedger getLastInventoryLedgerEntry(String skuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from InventoryLedger il where il.skuCode = :skuCode and il.facility.id = :facilityId order by il.created desc, il.id desc");
        query.setParameter("skuCode", skuCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (InventoryLedger) query.uniqueResult();
    }

    @Override
    public InventoryLedger getLastInventoryLedgerEntry(String skuCode, List<String> facilityCodes, Date from) {
        Query query;
        if (!CollectionUtils.isEmpty(facilityCodes)) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode and il.facility.code in (:codes) and il.created < :fromDate order by il.created desc");
            query.setParameterList("codes", facilityCodes);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode  and il.created < :fromDate order by il.created desc");
        }
        query.setParameter("fromDate", from);
        query.setParameter("skuCode", skuCode);
        query.setMaxResults(1);
        return (InventoryLedger) query.uniqueResult();
    }

    @Override
    public Integer getOpeningBalance(String skuCode, Date fromDate) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select il.oldBalance from InventoryLedger il where il.skuCode = :skuCode  and il.created > :fromDate order by il.created asc ");
        query.setParameter("skuCode", skuCode);
        query.setParameter("fromDate", fromDate);
        query.setMaxResults(1);
        return (Integer) query.uniqueResult();
    }

    @Override
    public List<InventoryLedger> fetchInventoryLedgerSummary(String skuCode, List<String> facilityCodes, Date from, Date to) {
        Query query;
        if (!CollectionUtils.isEmpty(facilityCodes)) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode and il.created < :to and il.created > :from and il.facility.code in (:codes) order by il.id asc ");
            query.setParameterList("codes", facilityCodes);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode and il.created < :to and il.created > :from order by il.id asc ");
        }
        query.setParameter("skuCode", skuCode);
        query.setParameter("from", from);
        query.setParameter("to", to);
        return query.list();
    }

    @Override
    public List<InventoryLedger> fetchInventoryLedger(String skuCode, List<String> facilityCodes, Date from, Date to, int start, int pageSize) {
        Query query;
        if (!CollectionUtils.isEmpty(facilityCodes)) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode and il.facility.code in (:codes) and il.created < :to and il.created > :from order by il.id asc ");
            query.setParameterList("codes", facilityCodes);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select il from InventoryLedger il where il.skuCode = :skuCode and il.created < :to and il.created > :from order by il.id asc ");
        }
        query.setParameter("skuCode", skuCode);
        query.setParameter("from", from);
        query.setParameter("to", to);
        query.setFirstResult(start);
        query.setMaxResults(pageSize);
        return query.list();


    }

    @Override
    public Long getInventoryLedgerEntriesCountInDateRange(String skuCode, List<String> facilityCodes, Date from, Date to) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InventoryLedger.class);
        criteria.add(Restrictions.eq("skuCode", skuCode));
        criteria.createCriteria("facility", "f");
        if (!CollectionUtils.isEmpty(facilityCodes)) {
            criteria.add(Restrictions.in("f.code", facilityCodes));
        }
        if (from != null && to != null) {
            criteria.add(Restrictions.between("created", from, to));
        }        criteria.add(Restrictions.ne("transactionType", InventoryLedger.TransactionType.OPENING_BALANCE));
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }
}
