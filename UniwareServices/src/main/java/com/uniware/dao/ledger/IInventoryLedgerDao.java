/**
 * Copyright 2017 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version 1.0, 19/09/17
 * @author aditya
 */
package com.uniware.dao.ledger;

import java.util.Date;
import java.util.List;

import com.uniware.core.entity.InventoryLedger;

public interface IInventoryLedgerDao {

    InventoryLedger getInventoryLedger(int inventoryLedgerId);

    List<InventoryLedger> getInventoryLedgerEntries(Date from, int start, int pageSize);

    List getFacilitiesForItemType(String skuCode);

    InventoryLedger addInventoryLedgerEntry(InventoryLedger ledgerEntry);

    InventoryLedger getLastInventoryLedgerEntry(String skuCode);

    InventoryLedger getLastInventoryLedgerEntry(String skuCode, List<String> facilityCodes, Date from);

    Integer getOpeningBalance(String skuCode, Date from);

    List<InventoryLedger> fetchInventoryLedgerSummary(String skuCode, List<String> facilityCodes, Date from, Date to);

    List<InventoryLedger> fetchInventoryLedger(String skuCode, List<String> facilityCodes, Date from, Date to, int start, int pageSize);

    Long getInventoryLedgerEntriesCountInDateRange(String skuCode, List<String> facilityCodes, Date from, Date to);


}
