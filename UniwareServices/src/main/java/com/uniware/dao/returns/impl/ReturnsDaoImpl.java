/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 10, 2012
 *  @author singla
 */
package com.uniware.dao.returns.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.returns.SearchAwaitingShippingPackageRequest;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.ReturnManifestStatus;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.ReversePickupAction;
import com.uniware.core.entity.ReversePickupAlternate;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.returns.IReturnsDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * @author singla
 */
@Repository
public class ReturnsDaoImpl implements IReturnsDao {
    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#addReversePickup(com.uniware.core.entity.ReversePickup)
     */
    @Override
    public ReversePickup addReversePickup(ReversePickup reversePickup) {
        reversePickup.setFacility(UserContext.current().getFacility());
        if (reversePickup.getCode() == null) {
            reversePickup.setCode(sequenceGenerator.generateNext(Name.REVERSE_PICKUP));
        }
        sessionFactory.getCurrentSession().persist(reversePickup);
        return reversePickup;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#updateReversePickup(com.uniware.core.entity.ReversePickup)
     */
    @Override
    public ReversePickup updateReversePickup(ReversePickup reversePickup) {
        reversePickup.setFacility(UserContext.current().getFacility());
        return (ReversePickup) sessionFactory.getCurrentSession().merge(reversePickup);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReversePickupById(int)
     */
    @Override
    public ReversePickup getReversePickupById(int reversePickupId) {
        return getReversePickupById(reversePickupId, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReversePickupById(java.lang.Integer, boolean)
     */
    @Override
    public ReversePickup getReversePickupById(Integer reversePickupId, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct r from ReversePickup r left join fetch r.saleOrderItems left join fetch r.shipmentTracking where r.id = :reversePickupId");
        query.setParameter("reversePickupId", reversePickupId);
        ReversePickup reversePickup = (ReversePickup) query.uniqueResult();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(reversePickup);
        }
        return reversePickup;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#addReshipment(com.uniware.core.entity.Reshipment)
     */
    @Override
    public Reshipment addReshipment(Reshipment reshipment) {
        reshipment.setFacility(UserContext.current().getFacility());
        reshipment.setCode(sequenceGenerator.generateNext(Name.RESHIPMENT));
        sessionFactory.getCurrentSession().persist(reshipment);
        return reshipment;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#updateReshipment(com.uniware.core.entity.Reshipment)
     */
    @Override
    public Reshipment updateReshipment(Reshipment reshipment) {
        return (Reshipment) sessionFactory.getCurrentSession().merge(reshipment);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReshipmentById(java.lang.Integer)
     */
    @Override
    public Reshipment getReshipmentById(Integer reshipmentId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Reshipment.class);
        criteria.add(Restrictions.idEq(reshipmentId));
        return (Reshipment) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingPackage> getReturnsAwaitingAction() {
        List<String> statusCodes = new ArrayList<String>();
        statusCodes.add(ShippingPackage.StatusCode.RETURN_EXPECTED.name());
        statusCodes.add(ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name());
        statusCodes.add(ShippingPackage.StatusCode.RETURNED.name());

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingPackage.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.createCriteria("reshipment", JoinType.LEFT_OUTER_JOIN).add(Restrictions.isNull("id"));
        criteria.add(Restrictions.in("statusCode", statusCodes));
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Reshipment> getPendingReshipments() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Reshipment.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.add(Restrictions.ne("statusCode", Reshipment.StatusCode.COMPLETE.name()));
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReversePickup> getUnassignedPendingReversePickups() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReversePickup.class);
        criteria.add(Restrictions.isNull("shipmentTracking")).add(Restrictions.eq("statusCode", ReversePickup.StatusCode.CREATED.name()));
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public ReversePickup searchReversePickupById(Integer reversePickupId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReversePickup.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.add(Restrictions.eq("id", reversePickupId));
        return (ReversePickup) criteria.uniqueResult();
    }

    @Override
    public ReversePickup getReversePickupByTrackingNumber(String trackingNumber) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReversePickup.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        criteria.createCriteria("shipmentTracking").add(Restrictions.eq("trackingNumber", trackingNumber));
        return (ReversePickup) criteria.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReversePickupById(int)
     */
    @Override
    public ReversePickup getReversePickupByCode(String reversePickupCode) {
        return getReversePickupByCode(reversePickupCode, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReversePickupById(java.lang.Integer, boolean)
     */
    @Override
    public ReversePickup getReversePickupByCode(String reversePickupCode, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct r from ReversePickup r left join fetch r.saleOrder left join fetch r.saleOrderItems left join fetch r.shipmentTracking where r.facility.id = :facilityId and r.code = :reversePickupCode");
        query.setParameter("reversePickupCode", reversePickupCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        ReversePickup reversePickup = (ReversePickup) query.uniqueResult();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(reversePickup);
        }
        return reversePickup;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#addReturnManifest(com.uniware.core.entity.ReturnManifest)
     */
    @Override
    public ReturnManifest addReturnManifest(ReturnManifest returnManifest) {
        returnManifest.setFacility(UserContext.current().getFacility());
        if (StringUtils.isBlank(returnManifest.getCode())) {
            returnManifest.setCode(sequenceGenerator.generateNext(Name.RETURN_MANIFEST));
        }
        sessionFactory.getCurrentSession().persist(returnManifest);
        return returnManifest;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#getReturnManifestById(java.lang.Integer)
     */
    @Override
    public ReturnManifest getReturnManifestById(Integer returnManifestId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct rm from ReturnManifest rm left join fetch rm.returnManifestItems rmi left join fetch rmi.shippingPackage sp left join fetch sp.saleOrderItems  where rm.id = :returnManifestId");
        query.setParameter("returnManifestId", returnManifestId);
        return (ReturnManifest) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.returns.IReturnsDao#addReturnManifestItem(com.uniware.core.entity.ReturnManifestItem)
     */
    @Override
    public ReturnManifestItem addReturnManifestItem(ReturnManifestItem returnManifestItem) {
        sessionFactory.getCurrentSession().persist(returnManifestItem);
        return returnManifestItem;
    }

    @Override
    public ReturnManifestItem updateReturnManifestItem(ReturnManifestItem returnManifestItem) {
        return (ReturnManifestItem) sessionFactory.getCurrentSession().merge(returnManifestItem);
    }

    @Override
    public ReturnManifest getReturnManifestDetailedByCode(String returnManifestCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct rm from ReturnManifest rm left join fetch rm.returnManifestItems rmi left join fetch rmi.shippingPackage sp left join fetch sp.saleOrderItems  where rm.facility.id = :facilityId and rm.code = :returnManifestCode");
        query.setParameter("returnManifestCode", returnManifestCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ReturnManifest) query.uniqueResult();
    }

    @Override
    public ReturnManifest getReturnManifestByCode(String returnManifestCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select rm from ReturnManifest rm where rm.facility.id = :facilityId and rm.code = :returnManifestCode");
        query.setParameter("returnManifestCode", returnManifestCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ReturnManifest) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShippingPackage> searchAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request) {
        Criteria criteria = getCriteriaForAwaitingShippingPackages(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    private Criteria getCriteriaForAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ShippingPackage.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        List<String> statuses = new ArrayList<String>();
        if (StringUtils.isNotBlank(request.getShippingPackageStatus())) {
            statuses.add(request.getShippingPackageStatus());
        } else {
            statuses.add(ShippingPackage.StatusCode.RETURN_EXPECTED.name());
            statuses.add(ShippingPackage.StatusCode.RETURN_ACKNOWLEDGED.name());
            statuses.add(ShippingPackage.StatusCode.RETURNED.name());
        }
        criteria.add(Restrictions.in("statusCode", statuses));
        criteria.add(Restrictions.isNull("reshipment"));
        if (StringUtils.isNotEmpty(request.getShippingPackageCode())) {
            criteria.add(Restrictions.eq("code", request.getShippingPackageCode().trim()));
        }
        if (StringUtils.isNotEmpty(request.getShippingProvider())) {
            criteria.createCriteria("shippingProvider").add(Restrictions.eq("name", request.getShippingProvider()));
        }
        if (StringUtils.isNotEmpty(request.getTrackingNumber())) {
            criteria.add(Restrictions.eq("trackingNumber", request.getTrackingNumber().trim()));
        }
        if (StringUtils.isNotEmpty(request.getSaleOrderCode())) {
            Criteria saleOrderCriteria = criteria.createCriteria("saleOrder");
            saleOrderCriteria.add(Restrictions.like("displayOrderCode", "%" + request.getSaleOrderCode() + "%"));
        }
        return criteria;
    }

    @Override
    public Long getAwaitingShippingPackageCount(SearchAwaitingShippingPackageRequest request) {
        Criteria criteria = getCriteriaForAwaitingShippingPackages(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public void addReversePickupAlternate(ReversePickupAlternate reversePickupAlternate) {
        sessionFactory.getCurrentSession().persist(reversePickupAlternate);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReturnManifestStatus> getReturnManifestStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from ReturnManifestStatus").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReshipmentAction> getReshipmentActions() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ReshipmentAction");
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ReversePickupAction> getReversePickupActions() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ReversePickupAction");
        return query.list();
    }

    @Override
    public Reshipment getReshipmentByCode(String reshipmentCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Reshipment where facility.id = :facilityId and code = :reshipmentCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("reshipmentCode", reshipmentCode);
        return (Reshipment) query.uniqueResult();
    }

    @Override
    public void removeReturnManifestItem(ReturnManifestItem returnManifestItem) {
        sessionFactory.getCurrentSession().delete(returnManifestItem);
    }

    @Override
    public ReversePickup getReversePickupByReturnInvoice(Integer invoiceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ReversePickup where returnInvoice.id = :returnInvoiceId and facility.id = :facilityId");
        query.setParameter("returnInvoiceId" , invoiceId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (ReversePickup) query.uniqueResult();
    }
}
