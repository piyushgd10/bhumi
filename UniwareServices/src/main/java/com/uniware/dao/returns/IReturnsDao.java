/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 10, 2012
 *  @author singla
 */
package com.uniware.dao.returns;

import java.util.List;

import com.uniware.core.api.returns.SearchAwaitingShippingPackageRequest;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.ReshipmentAction;
import com.uniware.core.entity.ReturnManifest;
import com.uniware.core.entity.ReturnManifestItem;
import com.uniware.core.entity.ReturnManifestStatus;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.ReversePickupAction;
import com.uniware.core.entity.ReversePickupAlternate;
import com.uniware.core.entity.ShippingPackage;

/**
 * @author singla
 */
public interface IReturnsDao {

    /**
     * @param reversePickup
     * @return
     */
    ReversePickup addReversePickup(ReversePickup reversePickup);

    /**
     * @param reversePickup
     * @return
     */
    ReversePickup updateReversePickup(ReversePickup reversePickup);

    /**
     * @param reversePickupId
     * @return
     */
    ReversePickup getReversePickupById(int reversePickupId);

    /**
     * @param reversePickupId
     * @param refresh
     * @return
     */
    ReversePickup getReversePickupById(Integer reversePickupId, boolean refresh);

    /**
     * @param reshipment
     */
    Reshipment addReshipment(Reshipment reshipment);

    /**
     * @param reshipment
     * @return
     */
    Reshipment updateReshipment(Reshipment reshipment);

    /**
     * @param reshipmentId
     * @return
     */
    Reshipment getReshipmentById(Integer reshipmentId);

    Reshipment getReshipmentByCode(String reshipmentCode);

    List<ShippingPackage> getReturnsAwaitingAction();

    List<Reshipment> getPendingReshipments();

    List<ReversePickup> getUnassignedPendingReversePickups();

    ReversePickup searchReversePickupById(Integer reversePickupId);

    ReversePickup getReversePickupByTrackingNumber(String trackingNumber);

    ReversePickup getReversePickupByCode(String reversePickupCode);

    ReversePickup getReversePickupByCode(String reversePickupCode, boolean refresh);

    /**
     * @param returnManifest
     */
    ReturnManifest addReturnManifest(ReturnManifest returnManifest);

    /**
     * @param returnManifestId
     * @return
     */
    ReturnManifest getReturnManifestById(Integer returnManifestId);

    /**
     * @param returnManifestItem
     * @return
     */
    ReturnManifestItem addReturnManifestItem(ReturnManifestItem returnManifestItem);

    ReturnManifestItem updateReturnManifestItem(ReturnManifestItem returnManifestItem);

    /**
     * @param returnManifestCode
     * @return
     */
    ReturnManifest getReturnManifestDetailedByCode(String returnManifestCode);

    List<ShippingPackage> searchAwaitingShippingPackages(SearchAwaitingShippingPackageRequest request);

    Long getAwaitingShippingPackageCount(SearchAwaitingShippingPackageRequest request);

    void addReversePickupAlternate(ReversePickupAlternate reversePickupAlternate);

    List<ReturnManifestStatus> getReturnManifestStatuses();

    List<ReshipmentAction> getReshipmentActions();

    List<ReversePickupAction> getReversePickupActions();

    ReturnManifest getReturnManifestByCode(String returnManifestCode);

    void removeReturnManifestItem(ReturnManifestItem returnManifestItem);

    ReversePickup getReversePickupByReturnInvoice(Integer invoiceId);
}
