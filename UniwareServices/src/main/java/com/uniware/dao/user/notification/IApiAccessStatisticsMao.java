/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.dao.user.notification;

import java.util.List;

import com.uniware.core.vo.ApiAccessStatisticsVO;
import com.uniware.core.vo.ApiLimitsVO;

public interface IApiAccessStatisticsMao {

    List<ApiAccessStatisticsVO> getAccessStatisticsForUser(String apiUserName);

    ApiLimitsVO getAPILimitByAPIName(String apiName);

    void save(ApiAccessStatisticsVO vo, boolean successful);

    List<ApiAccessStatisticsVO> getAllApiAccessStatistics();
}
