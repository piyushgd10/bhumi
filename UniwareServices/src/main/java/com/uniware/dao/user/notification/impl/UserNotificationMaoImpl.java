/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 11, 2012
 *  @author singla
 */
package com.uniware.dao.user.notification.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserNotificationTypeVO;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import com.uniware.core.vo.UserNotificationVO;
import com.uniware.dao.user.notification.IUserNotificationMao;

/**
 * @author Sunny
 */
@Repository
public class UserNotificationMaoImpl implements IUserNotificationMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations tenantSpecificMongoOps;

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commonMongoOps;

    @Override
    public List<UserNotificationTypeVO> getAllUserNotificationTypes() {
        return commonMongoOps.findAll(UserNotificationTypeVO.class);
    }

    @Override
    public void save(UserNotificationVO userNotificationVO) {
        Query query = new Query(Criteria.where("identifier").is(userNotificationVO.getIdentifier()).and("tenantCode").is(UserContext.current().getTenant().getCode()));
        if (UserContext.current().getFacility() != null) {
            query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()));
        } else {
            query.addCriteria(Criteria.where("facilityCode").is(null));
        }
        Update update = new Update();
        update.set("notificationType", userNotificationVO.getNotificationType());
        update.set("message", userNotificationVO.getMessage());
        update.set("actionUrl", userNotificationVO.getActionUrl());
        update.set("title", userNotificationVO.getTitle());
        update.set("created", userNotificationVO.getCreated());
        tenantSpecificMongoOps.upsert(query, update, UserNotificationVO.class);
    }

    @Override
    public UserNotificationVO getNotificationById(String notificationId) {
        return tenantSpecificMongoOps.findById(notificationId, UserNotificationVO.class);
    }

    @Override
    public List<UserNotificationVO> getUserNotifications(List<NotificationType> notificationTypes, int start, int pageSize) {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("notificationType").in(notificationTypes));
        if (UserContext.current().getFacility() != null) {
            query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
        }
        Pageable pagination = new PageRequest(start / pageSize, pageSize, new Sort(new Sort.Order(Direction.DESC, "created")));
        return tenantSpecificMongoOps.find(query.with(pagination), UserNotificationVO.class);
    }

    @Override
    public void remove(String identifier) {
        tenantSpecificMongoOps.remove(new Query(Criteria.where("identifier").is(identifier).and("tenantCode").is(UserContext.current().getTenant().getCode())),
                UserNotificationVO.class);
    }

    @Override
    public void removeAll() {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
        query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
        tenantSpecificMongoOps.remove(query, UserNotificationVO.class);
    }

    @Override
    public long getUnreadCount(Date userReadTime) {
        Date queryDate = new Date(userReadTime.getTime() + 999); // mongo compares up to milliseconds.
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("created").gt(queryDate));
        if (UserContext.current().getFacility() != null) {
            query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
        }
        return tenantSpecificMongoOps.count(query, UserNotificationVO.class);
    }
}
