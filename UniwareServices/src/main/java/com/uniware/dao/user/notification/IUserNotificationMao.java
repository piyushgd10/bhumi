/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 11, 2012
 *  @author singla
 */
package com.uniware.dao.user.notification;

import java.util.Date;
import java.util.List;

import com.uniware.core.vo.UserNotificationTypeVO;
import com.uniware.core.vo.UserNotificationVO;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;

/**
 * @author Sunny
 */
public interface IUserNotificationMao {

    public void save(UserNotificationVO userNotificationVO);

    public List<UserNotificationVO> getUserNotifications(List<NotificationType> notificationTypes, int start, int pageSize);

    UserNotificationVO getNotificationById(String notificationId);

    void remove(String identifier);

    void removeAll();

    long getUnreadCount(Date userReadTime);

    List<UserNotificationTypeVO> getAllUserNotificationTypes();
}