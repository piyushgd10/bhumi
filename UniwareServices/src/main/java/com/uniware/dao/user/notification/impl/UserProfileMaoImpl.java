/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, July, 2015
 *  @author akshay
 */
package com.uniware.dao.user.notification.impl;

import com.uniware.core.vo.UserProfileVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.dao.user.notification.IUserProfileMao;

@Repository
public class UserProfileMaoImpl implements IUserProfileMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;
    
    @Override
    public void save(com.uniware.core.vo.UserProfileVO userProfile) {
        userProfile.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(userProfile);
    }
    
    @Override
    public UserProfileVO getUserProfileByUsername(String username) {
       return mongoOperations.findOne(new Query(Criteria.where("username").is(username).and("tenantCode").is(UserContext.current().getTenant().getCode())),
               UserProfileVO.class);
    }
    
    @Override
    public UserProfileVO getUserProfileByUsernameAndTenant(String username, String tenantCode) {
       return mongoOperations.findOne(new Query(Criteria.where("username").is(username).and("tenantCode").is(tenantCode)),
                UserProfileVO.class);
    }

    @Override
    public UserProfileVO getUserDigestResponse(){
        return mongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), UserProfileVO.class);

    }


}
