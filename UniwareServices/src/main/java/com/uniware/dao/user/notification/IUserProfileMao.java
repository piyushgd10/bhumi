/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, July 02, 2015
 *  @author akshay
 */
package com.uniware.dao.user.notification;

import com.uniware.core.vo.UserProfileVO;

public interface IUserProfileMao {

    void save(com.uniware.core.vo.UserProfileVO userProfile);

    UserProfileVO getUserProfileByUsername(String username);

    UserProfileVO getUserProfileByUsernameAndTenant(String username, String tenantCode);

    public UserProfileVO getUserDigestResponse();

}
