/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Mar-2014
 *  @author parijat
 */
package com.uniware.dao.user.notification.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ApiAccessStatisticsVO;
import com.uniware.core.vo.ApiLimitsVO;
import com.uniware.dao.user.notification.IApiAccessStatisticsMao;

@Repository
public class ApiAccessStatisticsMaoImpl implements IApiAccessStatisticsMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public void save(ApiAccessStatisticsVO vo, boolean successful) {
        Date timestamp = new Date((System.currentTimeMillis() / 1000)*1000);
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("apiUsername").is(UserContext.current().getApiUsername()).and(
                "apiName").is(vo.getApiName()).and("apiVersion").is(UserContext.current().getApiVersion()).and("timestamp").is(timestamp));
        Update update = new Update();
        if (successful) {
            update.inc("successCount", 1);
        } else {
            update.inc("failedCount", 1);
        }
        update.inc("totalResponseTime", vo.getTotalResponseTime());
        upsert(query,update);
    }

    @Async
    private void upsert(Query query, Update update){
        mongoOperations.upsert(query, update, ApiAccessStatisticsVO.class);
    }

    @Override
    public List<ApiAccessStatisticsVO> getAccessStatisticsForUser(String apiUsername) {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("apiUsername").is(apiUsername));
        return mongoOperations.find(query, ApiAccessStatisticsVO.class);
    }

    @Override
    public List<ApiAccessStatisticsVO> getAllApiAccessStatistics() {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
        return mongoOperations.find(query, ApiAccessStatisticsVO.class);
    }

    @Override
    public ApiLimitsVO getAPILimitByAPIName(String apiName) {
        Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("apiName").is(apiName));
        return mongoOperations.findOne(query, ApiLimitsVO.class);
    }

}
