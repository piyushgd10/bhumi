/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.dao.lookup;

import java.util.List;

import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventorySnapshotRequest;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventorySnapshot;

public interface ILookupDao {

    List<ItemTypeInventory> searchInventory(SearchInventoryRequest request);

    Long getInventoryCount(SearchInventoryRequest request);

    Long getPendingGRNCountByItemTypeId(Integer itemTypeId);

    Integer getItemTypeInventoryCountInPicking(Integer itemTypeInventoryId);

    List<ItemTypeInventorySnapshot> searchInventorySnapshot(SearchInventorySnapshotRequest request);

    Long getInventorySnapshotCount(SearchInventorySnapshotRequest request);

}
