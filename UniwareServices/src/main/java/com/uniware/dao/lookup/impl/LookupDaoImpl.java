/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.uniware.dao.lookup.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventorySnapshotRequest;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.lookup.ILookupDao;

@Repository
public class LookupDaoImpl implements ILookupDao {
    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<ItemTypeInventory> searchInventory(SearchInventoryRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public Long getInventoryCount(SearchInventoryRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    private Criteria getCriteriaFromRequest(SearchInventoryRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemTypeInventory.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (StringUtils.isNotEmpty(request.getShelf())) {
            criteria.createCriteria("shelf").add(Restrictions.like("code", "%" + request.getShelf().trim() + "%"));
        }
        if (StringUtils.isNotEmpty(request.getItemType())) {
            criteria.createCriteria("itemType").add(
                    Restrictions.or(Restrictions.like("name", "%" + request.getItemType().trim() + "%"), Restrictions.like("skuCode", "%" + request.getItemType().trim() + "%")));
        }
        criteria.add(Restrictions.gt("quantity", 0));
        return criteria;
    }

    @Override
    public Long getPendingGRNCountByItemTypeId(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sum(iri.quantity) - sum(iri.rejectedQuantity) from InflowReceiptItem iri where iri.inflowReceipt.facility.id = :facilityId and iri.itemType.id = :itemTypeId and iri.statusCode != :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("statusCode", InflowReceiptItem.StatusCode.COMPLETE.name());
        return (Long) query.uniqueResult();
    }

    @Override
    public Integer getItemTypeInventoryCountInPicking(Integer itemTypeInventoryId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(soi) from SaleOrderItem soi where soi.facility.id = :facilityId and soi.itemTypeInventory.id = :itemTypeInventoryId and soi.statusCode = :statusCode and soi.item.id is null");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return ((Long) query.uniqueResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ItemTypeInventorySnapshot> searchInventorySnapshot(SearchInventorySnapshotRequest request) {
        Criteria criteria = getSearchInventorySnapshotCriteria(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("updated"));
        return criteria.list();
    }

    @Override
    public Long getInventorySnapshotCount(SearchInventorySnapshotRequest request) {
        Criteria criteria = getSearchInventorySnapshotCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    public Criteria getSearchInventorySnapshotCriteria(SearchInventorySnapshotRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ItemTypeInventorySnapshot.class);
        Criteria itemTypeCriteria = criteria.createCriteria("itemType");
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (StringUtils.isNotBlank(request.getItemTypeKeyword())) {
            String keyword = request.getItemTypeKeyword().trim();
            SimpleExpression se1 = Restrictions.like("name", "%" + keyword + "%");
            SimpleExpression se2 = Restrictions.like("skuCode", "%" + keyword + "%");
            SimpleExpression se3 = Restrictions.like("description", "%" + keyword + "%");
            itemTypeCriteria.add(Restrictions.or(se1, Restrictions.or(se2, se3)));
        }
        if (StringUtils.isNotBlank(request.getCategory())) {
            itemTypeCriteria.createCriteria("category").add(Restrictions.eq("code", request.getCategory()));
        }
        if (request.isEnabled()) {
            criteria.add(Restrictions.gt("inventory", 0));
        }
        return criteria;
    }

}
