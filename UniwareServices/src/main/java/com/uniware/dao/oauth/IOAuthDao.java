/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 29/08/14
*  @author sunny
*/

package com.uniware.dao.oauth;

import com.uniware.core.entity.OAuthAccessToken;
import com.uniware.core.entity.OAuthRefreshToken;

public interface IOAuthDao {

    public void save(OAuthAccessToken oAuthAccessToken);

    public void save(OAuthRefreshToken oAuthRefreshToken);

    public OAuthAccessToken getOAuthAccessTokenByTokenId(String tokenId);

    public OAuthRefreshToken getOAUthRefreshTokenByTokenId(String tokenId);

    OAuthAccessToken getOAuthAccessTokenByAuthenticationId(String authenticationId);

    boolean deleteOAuthAccessTokenByTokenId(String tokenId);

    boolean deleteOAuthAccessTokenByRefreshToken(String refreshToken);

    boolean deleteOAuthRefreshTokenByTokenId(String tokenId);
}
