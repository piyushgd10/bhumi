/*
*  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 29/08/14
*  @author sunny
*/

package com.uniware.dao.oauth.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.OAuthAccessToken;
import com.uniware.core.entity.OAuthRefreshToken;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.oauth.IOAuthDao;

@Repository
public class OAuthDaoImpl implements IOAuthDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(OAuthAccessToken oAuthAccessToken) {
        oAuthAccessToken.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().save(oAuthAccessToken);
    }

    @Override
    public void save(OAuthRefreshToken oAuthRefreshToken) {
        oAuthRefreshToken.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().save(oAuthRefreshToken);
    }

    @Override
    public OAuthAccessToken getOAuthAccessTokenByTokenId(String tokenId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from OAuthAccessToken where tokenId = :tokenId and tenant.id = :tenantId");
        query.setParameter("tokenId", tokenId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (OAuthAccessToken) query.uniqueResult();
    }

    @Override
    public OAuthRefreshToken getOAUthRefreshTokenByTokenId(String tokenId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from OAuthRefreshToken where tokenId = :tokenId and tenant.id = :tenantId");
        query.setParameter("tokenId", tokenId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (OAuthRefreshToken) query.uniqueResult();
    }

    @Override public OAuthAccessToken getOAuthAccessTokenByAuthenticationId(String authenticationId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from OAuthAccessToken where authenticationId = :authenticationId and tenant.id = :tenantId");
        query.setParameter("authenticationId", authenticationId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (OAuthAccessToken) query.uniqueResult();
    }

    @Override
    public boolean deleteOAuthAccessTokenByTokenId(String tokenId) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from OAuthAccessToken oaat where oaat.tokenId = :tokenId and oaat.tenant.id = :tenantId");
        query.setParameter("tokenId", tokenId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean deleteOAuthAccessTokenByRefreshToken(String refreshToken) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from OAuthAccessToken oaat where oaat.refreshToken = :refreshToken and oaat.tenant.id = :tenantId");
        query.setParameter("refreshToken", refreshToken);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean deleteOAuthRefreshTokenByTokenId(String tokenId) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from OAuthRefreshToken oart where oart.tokenId = :tokenId and oart.tenant.id = :tenantId");
        query.setParameter("tokenId", tokenId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.executeUpdate() > 0;
    }
}
