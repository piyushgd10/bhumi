/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2011
 *  @author singla
 */
package com.uniware.dao.picker;

import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import java.util.List;

import com.unifier.core.entity.User;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.PicklistStatus;
import com.uniware.core.entity.ShippingPackage;

/**
 * @author singla
 */
public interface IPickerDao {
    /**
     * @param pickSetId
     * @param maxCount
     * @return
     */
    List<ShippingPackage> getShippingPackages(int pickSetId, int maxCount);

    List<ShippingPackage> getShippingPackages(int pickSetId);

    /**
     * @param picklist
     * @return
     */
    Picklist addPicklist(Picklist picklist);

    /**
     * @param picklist
     * @return
     */
    Picklist updatePicklist(Picklist picklist);

    /**
     * @param picklistCode
     * @return
     */
    Picklist getPicklistByCode(String picklistCode);

    Picklist getPicklistByCode(String picklistCode, boolean refresh, boolean fetchItems);

    Picklist getPicklistById(Integer picklistId);

    /**
     * @param user
     * @param count
     */
    List<Picklist> getRecentPicklists(User user, int count);

    /**
     * @param request
     * @return
     */
    List<Picklist> searchPicklist(SearchPicklistRequest request);

    /**
     * @param request
     * @return
     */
    Long getPicklistCount(SearchPicklistRequest request);

    /**
     * @return
     */
    List<Object[]> getShippingPackagesCountGroupByPickset();

    /**
     * @param shippingPackageCode
     * @return
     */
    Picklist getPicklistForShippingPackage(String shippingPackageCode);

    List<ShippingPackage> getPickablePackagesByCode(List<String> shippingPackageCodes);

    List<PicklistStatus> getPicklistStatuses();

    /*
        join fetch SOI mandatory to get saleOrderItems of given pickset only
         */
    List<ShippingPackage> getPackagesWithPickableSaleOrderItems(List<String> shippingPackageCodes, int pickSetId);

    List<ShippingPackage> getPackagesForPickingFromStaging(List<String> shippingPackageCodes, int pickSetId);

    PicklistItem getStockingToStagingPicklistItem(String saleOrderItemCode, String saleOrderCode, String shippingPackageCode);

    PicklistItem addPicklistItem(PicklistItem picklistItem);

    PicklistItem updatePicklistItem(PicklistItem picklistItem);

    PickBucket getPickBucketByCode(String pickBucketCode);

    PickBatch getOpenPickBatchByPickBucket(String pickBucketCode, boolean fetchItems);

    PickBatch getPickBatchById(Integer id);

    PicklistItem getPicklistItemByItemCodeInPicklist(String itemCode, int picklistId);

    PicklistItem getOpenPicklistItemByItemCode(String itemCode);

    PicklistItem getPicklistItemById(int picklistItemId);

    PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemOrSkuCode, Integer pickBatchId, PicklistItem.QCStatus qcStatusCode);

    PicklistItem getSoftAllocatedPicklistItemByItemCode(String itemCode, Integer id);

    PickBatch addPickBatch(PickBatch pickBatch);

    PicklistItem getPicklistItemBySaleOrderItemCodeAndShippingPackageCode(String saleOrderItemCode, String saleOrderCode, String shippingPackageCode,
            Picklist.Destination destination);

    List<PicklistItem> getPicklistItemsByShippingPackageCode(String shippingPackageCode, Picklist.Destination destination);

    List<Picklist> getOpenPicklists();

    PicklistItem getPicklistItemForPutbackByItemCode(String itemCode);

    PicklistItem getPicklistItemBySaleOrderItemCode(Integer picklistId, String saleOrderItemCode, String saleOrderCode);

    List<PickBucket> getPickBuckets(List<String> pickBucketCodes);

    PickBucket addPickBucket(PickBucket pickBucket);

    List<PicklistItem> getPicklistItemsForSoftAllocationBySkuCode(String skuCode, Integer quantity, Integer picklistId, List<PicklistItem.StatusCode> statusCodes,
            boolean checkPickBatch);

    List<PicklistItem> getPutbackAcceptedItemsBySkuCodeAndQCStatus(String skuCode, PicklistItem.QCStatus qcStatus, Integer quantity);

    int receivePicklistItems(Integer picklistId);

    int setDefaultPickBatch(Integer picklistId);

    List<String> getShippingPackageCodesInPicklist(Integer picklistId);
}
