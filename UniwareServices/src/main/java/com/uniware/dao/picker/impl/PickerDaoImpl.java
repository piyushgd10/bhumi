/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 28, 2011
 *  @author singla
 */
package com.uniware.dao.picker.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.entity.PickBatch;
import com.uniware.core.entity.PickBucket;
import com.uniware.core.entity.Picklist;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.PicklistStatus;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.picker.IPickerDao;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.SystemConfiguration;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class PickerDaoImpl implements IPickerDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#getShippingPackages(java.util.List, int)
     */

    @Override
    public List<ShippingPackage> getShippingPackages(int pickSetId, int maxCount) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join sp.saleOrderItems soi where sp.facility.id = :facilityId and sp.statusCode in (:spStatusCodes) and soi.section.pickSet.id = :pickSetId and soi.statusCode = :statusCode and soi.onHold = :onHold order by sp.saleOrder.priority desc,  sp.saleOrder.fulfillmentTat");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pickSetId", pickSetId);
        //        Getting custom channel id from cache as it will always be there
        //        query.setParameter("channelId", CacheManager.getInstance().getCache(ChannelCache.class).getChannelByCode(Source.Code.CUSTOM.name()).getId());
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        query.setParameterList("spStatusCodes", Arrays.asList(ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name()));
        query.setParameter("onHold", false);
        query.setMaxResults(maxCount);
        return query.list();
    }

    @Override
    public List<ShippingPackage> getShippingPackages(int pickSetId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join sp.saleOrderItems soi where sp.facility.id = :facilityId and soi.section.pickSet.id = :pickSetId and soi.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pickSetId", pickSetId);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#addPicklist(com.uniware.core.entity.Picklist)
     */
    @Override
    public Picklist addPicklist(Picklist picklist) {
        picklist.setCode(sequenceGenerator.generateNext(Name.PICKLIST));
        picklist.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(picklist);
        return picklist;
    }

    @Override
    public Picklist getPicklistByCode(String picklistCode, boolean refresh, boolean fetchItems) {
        Query query = null;
        if (fetchItems) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select p from Picklist p join fetch p.picklistItems pi where p.facility.id = :facilityId and p.code = :picklistCode");
        } else {
            query = sessionFactory.getCurrentSession().createQuery("select p from Picklist p where p.facility.id = :facilityId and p.code = :picklistCode");
        }
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("picklistCode", picklistCode);
        Picklist picklist = (Picklist) query.uniqueResult();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(picklist);
        }
        return picklist;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#getPicklistByCode(java.lang.String, boolean)
     */
    @Override
    public Picklist getPicklistByCode(String picklistCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select p from Picklist p where p.facility.id = :facilityId and p.code = :picklistCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("picklistCode", picklistCode);
        Picklist picklist = (Picklist) query.uniqueResult();
        if (picklist != null) {
            Hibernate.initialize(picklist.getPicklistItems());
        }
        return picklist;
    }

    @Override
    public Picklist getPicklistById(Integer picklistId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select p from Picklist p join fetch p.picklistItems pi where p.facility.id = :facilityId and p.id = :picklistId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("picklistId", picklistId);
        return (Picklist) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#getRecentPicklists(java.lang.Integer, int)
     */
    @Override
    public List<Picklist> getRecentPicklists(User user, int count) {
        Query query;
        if (user == null) {
            query = sessionFactory.getCurrentSession().createQuery("select p from Picklist p join fetch p.user u where p.facility.id = :facilityId order by p.created desc");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select p from Picklist p join fetch p.user u where p.facility.id = :facilityId and u.id = :userId order by p.created desc");
            query.setParameter("userId", user.getId());
            query.setParameter("facilityId", UserContext.current().getFacilityId());
        }
        query.setMaxResults(count);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#updatePicklist(com.uniware.core.entity.Picklist)
     */
    @Override
    public Picklist updatePicklist(Picklist picklist) {
        picklist.setFacility(UserContext.current().getFacility());
        return (Picklist) sessionFactory.getCurrentSession().merge(picklist);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#searchPicklist(com.uniware.core.api.picker.SearchPicklistRequest)
     */
    @Override
    public List<Picklist> searchPicklist(SearchPicklistRequest request) {
        Criteria criteria = getSearchPicklistCriteria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();

    }

    /* (non-Javadoc)
     * @see com.uniware.dao.picker.IPickerDao#getPicklistCount(com.uniware.core.api.picker.SearchPicklistRequest)
     */
    @Override
    public Long getPicklistCount(SearchPicklistRequest request) {
        Criteria criteria = getSearchPicklistCriteria(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    private Criteria getSearchPicklistCriteria(SearchPicklistRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Picklist.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (!request.getStatusCodes().isEmpty()) {
            criteria.add(Restrictions.in("statusCode", request.getStatusCodes()));
        }
        if (request.getFromDate() != null && request.getToDate() != null) {
            criteria.add(Restrictions.between("created", request.getFromDate(), request.getToDate()));
        }
        return criteria;
    }

    @Override
    public List<Object[]> getShippingPackagesCountGroupByPickset() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sp.pickSet.name, count(sp) from ShippingPackage sp where sp.facility.id = :facilityId and sp.statusCode=:statusCode group by sp.pickSet.id order by count(sp)");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", ShippingPackage.StatusCode.CREATED.name());
        return query.list();
    }

    @Override
    public Picklist getPicklistForShippingPackage(String shippingPackageCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct p from Picklist p join fetch p.picklistItems pi where p.destination = :invoicing and p.facility.id = :facilityId and pi.shippingPackageCode = :shippingPackageCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shippingPackageCode", shippingPackageCode);
        query.setParameter("invoicing", Picklist.Destination.INVOICING);
        return (Picklist) query.uniqueResult();
    }

    @Override
    public List<ShippingPackage> getPickablePackagesByCode(List<String> shippingPackageCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems where sp.code in (:shippingPackageCodes) and sp.facility.id = :facilityId and sp.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("statusCode", ShippingPackage.StatusCode.CREATED.name());
        return query.list();
    }

    @Override
    public List<PicklistStatus> getPicklistStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from PicklistStatus").list();
    }

    /*
    join fetch SOI mandatory to get saleOrderItems of given pickset only
     */
    @Override
    public List<ShippingPackage> getPackagesWithPickableSaleOrderItems(List<String> shippingPackageCodes, int pickSetId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems soi where sp.code in (:shippingPackageCodes) and sp.statusCode in (:spStatusCodes) and soi.section.pickSet.id = :pickSetId and sp.facility.id = :facilityId and soi.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        List<String> spStatusCodes = new ArrayList<>(3);
        spStatusCodes.add(ShippingPackage.StatusCode.CREATED.name());
        spStatusCodes.add(ShippingPackage.StatusCode.PICKING.name());
        spStatusCodes.add(ShippingPackage.StatusCode.PICKED.name());
        query.setParameterList("spStatusCodes", spStatusCodes);
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("pickSetId", pickSetId);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());

        return query.list();
    }

    @Override
    public List<ShippingPackage> getPackagesForPickingFromStaging(List<String> shippingPackageCodes, int pickSetId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct sp from ShippingPackage sp join fetch sp.saleOrderItems soi, Shelf s where sp.code in (:shippingPackageCodes) and sp.facility.id = :facilityId and soi.statusCode = :statusCode and sp.shelfCode = s.code and s.facility.id = :facilityId and s.section.pickSet.id = :pickSetId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("shippingPackageCodes", shippingPackageCodes);
        query.setParameter("pickSetId", pickSetId);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.STAGED.name());
        return query.list();
    }

    @Override
    public PicklistItem getStockingToStagingPicklistItem(String saleOrderItemCode, String saleOrderCode, String shippingPackageCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pi from PicklistItem pi join pi.picklist p where p.facility.id = :facilityId and pi.saleOrderItemCode = :saleOrderItemCode and pi.saleOrderCode = :saleOrderCode and pi.shippingPackageCode = :shippingPackageCode");
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("shippingPackageCode", shippingPackageCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (PicklistItem) query.uniqueResult();
    }

    @Override
    public PicklistItem addPicklistItem(PicklistItem picklistItem) {
        sessionFactory.getCurrentSession().persist(picklistItem);
        return picklistItem;
    }

    @Override
    public PicklistItem getPicklistItemBySaleOrderItemCodeAndShippingPackageCode(String saleOrderItemCode, String saleOrderCode, String shippingPackageCode,
            Picklist.Destination destination) {
        Query query;
        if (destination != null) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select pli from PicklistItem pli join fetch pli.picklist pl where pli.saleOrderItemCode = :saleOrderItemCode and pli.saleOrderCode = :saleOrderCode and pli.shippingPackageCode = :shippingPackageCode and pl.destination = :destination");
            query.setParameter("destination", destination);
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select pli from PicklistItem pli join fetch pli.picklist pl where pli.saleOrderItemCode = :saleOrderItemCode and pli.saleOrderCode = :saleOrderCode and pli.shippingPackageCode = :shippingPackageCode order by pli.id desc");
            query.setMaxResults(1);
        }
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("shippingPackageCode", shippingPackageCode);
        return (PicklistItem) query.uniqueResult();
    }

    @Override
    public List<Picklist> getOpenPicklists() {
        Query q = sessionFactory.getCurrentSession().createQuery("from Picklist where facility.id = :facilityId and statusCode != :closed");
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("closed", Picklist.StatusCode.CLOSED.name());
        return q.list();
    }

    @Override
    public PicklistItem updatePicklistItem(PicklistItem picklistItem) {
        sessionFactory.getCurrentSession().save(picklistItem);
        return picklistItem;
    }

    @Override
    public PickBucket getPickBucketByCode(String pickBucketCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select p from PickBucket p where p.facility.id = :facilityId and p.code = :pickBucketCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pickBucketCode", pickBucketCode);
        return (PickBucket) query.uniqueResult();
    }

    @Override
    public PickBatch getOpenPickBatchByPickBucket(String pickBucketCode, boolean fetchItems) {
        Query q = null;
        if (fetchItems) {
            q = sessionFactory.getCurrentSession().createQuery(
                    "from PickBatch pb join fetch pb.picklist p left join fetch pb.picklistItems pi where pb.pickBucket.code = :pickBucketCode and pb.statusCode != :closed and p.facility.id = :facilityId");
        } else {
            q = sessionFactory.getCurrentSession().createQuery(
                    "from PickBatch pb join fetch pb.picklist p where pb.pickBucket.code = :pickBucketCode and pb.statusCode != :closed and p.facility.id = :facilityId");
        }
        q.setParameter("pickBucketCode", pickBucketCode);
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("closed", PickBatch.StatusCode.CLOSED);
        return (PickBatch) q.uniqueResult();
    }

    @Override
    public PickBatch getPickBatchById(Integer id) {
        Query q = sessionFactory.getCurrentSession().createQuery("from PickBatch pb join fetch pb.picklist where pb.id = :id");
        q.setParameter("id", id);
        return (PickBatch) q.uniqueResult();
    }

    @Override
    public PicklistItem getPicklistItemByItemCodeInPicklist(String itemCode, int picklistId) {
        Query q = sessionFactory.getCurrentSession().createQuery("from PicklistItem where itemCode = :itemCode and picklist.id= :picklistId");
        q.setParameter("picklistId", picklistId);
        q.setParameter("itemCode", itemCode);
        return (PicklistItem) q.uniqueResult();
    }

    @Override
    public PicklistItem getOpenPicklistItemByItemCode(String itemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pli from PicklistItem pli join pli.picklist pl where pli.itemCode = :itemCode and pli.statusCode not in (:terminalStatusCodes) and pl.facility.id = :facilityId");
        query.setParameter("itemCode", itemCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("terminalStatusCodes", Arrays.asList(PicklistItem.StatusCode.COMPLETE, PicklistItem.StatusCode.PUTBACK_COMPLETE,
                PicklistItem.StatusCode.PUTBACK_ACCEPTED, PicklistItem.StatusCode.INVENTORY_NOT_FOUND));
        query.setMaxResults(1);
        return (PicklistItem) query.uniqueResult();
    }

    @Override
    public PicklistItem getPicklistItemById(int picklistItemId) {
        Query q = sessionFactory.getCurrentSession().createQuery("from PicklistItem where id = :picklistItemId");
        q.setParameter("picklistItemId", picklistItemId);
        return (PicklistItem) q.uniqueResult();
    }

    @Override
    public PicklistItem getPicklistItemBySaleOrderItemCode(Integer picklistId, String saleOrderItemCode, String saleOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pli from PicklistItem pli join pli.picklist pl where pl.id = :picklistId and pli.saleOrderItemCode = :saleOrderItemCode and pli.saleOrderCode = :saleOrderCode");
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("picklistId", picklistId);
        return (PicklistItem) query.uniqueResult();
    }

    @Override
    public PicklistItem getSoftAllocatedPicklistItemInPickBatchByItemOrSkuCode(String itemOrSkuCode, Integer pickBatchId, PicklistItem.QCStatus qcStatusCode) {
        StringBuilder queryBuilder = new StringBuilder(
                "select pli from PicklistItem pli join fetch pli.picklist pl join pli.pickBatch pb where pb.id = :pickBatchId and pli.statusCode in (:statusCodes)");
        SystemConfiguration.TraceabilityLevel traceabilityLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel();
        if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
            queryBuilder.append(" and pli.itemCode = :itemOrSkuCode");
        } else {
            queryBuilder.append(" and pli.skuCode = :itemOrSkuCode and pli.qcStatusCode = :qcStatusCode");
        }
        Query query = sessionFactory.getCurrentSession().createQuery(queryBuilder.toString());
        query.setParameter("pickBatchId", pickBatchId);
        query.setParameter("itemOrSkuCode", itemOrSkuCode);
        query.setParameterList("statusCodes", Arrays.asList(PicklistItem.StatusCode.SUBMITTED, PicklistItem.StatusCode.PUTBACK_PENDING));
        PicklistItem picklistItem = null;
        if (SystemConfiguration.TraceabilityLevel.ITEM.equals(traceabilityLevel)) {
            picklistItem = (PicklistItem) query.uniqueResult();
        } else {
            query.setParameter("qcStatusCode", qcStatusCode);
            query.setMaxResults(1);
            List<PicklistItem> picklistItems = query.list();
            picklistItem = picklistItems.isEmpty() ? null : picklistItems.get(0);
        }
        return picklistItem;
    }

    @Override
    public PicklistItem getSoftAllocatedPicklistItemByItemCode(String itemCode, Integer picklistId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select pli from PicklistItem pli join pli.picklist pl where pl.id = :picklistId and pli.itemCode = :itemCode and pli.statusCode in (:statusCodes)");
        query.setParameter("picklistId", picklistId);
        query.setParameter("itemCode", itemCode);
        query.setParameterList("statusCodes", Arrays.asList(PicklistItem.StatusCode.RECEIVED, PicklistItem.StatusCode.CREATED, PicklistItem.StatusCode.PUTBACK_PENDING));
        return (PicklistItem) query.uniqueResult();
    }

    @Override
    public PicklistItem getPicklistItemForPutbackByItemCode(String itemCode) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select pi from PicklistItem pi join fetch pi.picklist p  where pi.statusCode in (:statusCodes) and pi.itemCode = :itemCode and pi.picklist.facility.id= :facilityId");
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("itemCode", itemCode);
        q.setParameterList("statusCodes", Arrays.asList(PicklistItem.StatusCode.PUTBACK_ACCEPTED, PicklistItem.StatusCode.INVENTORY_NOT_FOUND));
        return (PicklistItem) q.uniqueResult();
    }

    @Override
    public PickBatch addPickBatch(PickBatch pickBatch) {
        sessionFactory.getCurrentSession().persist(pickBatch);
        return pickBatch;
    }

    @Override
    public List<PicklistItem> getPicklistItemsByShippingPackageCode(String shippingPackageCode, Picklist.Destination destination) {
        Query q = null;
        if (SystemConfiguration.TraceabilityLevel.ITEM.equals(ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel())) {
            q = sessionFactory.getCurrentSession().createQuery(
                    "select distinct pi from PicklistItem pi join fetch pi.picklist p where pi.shippingPackageCode = :shippingPackageCode and pi.itemCode is not null and p.destination = :destination and p.facility.id= :facilityId");
        } else {
            q = sessionFactory.getCurrentSession().createQuery(
                    "select distinct pi from PicklistItem pi join fetch pi.picklist p where pi.shippingPackageCode = :shippingPackageCode and p.destination = :destination and p.facility.id= :facilityId");
        }
        q.setParameter("destination", destination);
        q.setParameter("shippingPackageCode", shippingPackageCode);
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        return q.list();
    }

    @Override
    public PickBucket addPickBucket(PickBucket pickBucket) {
        if (StringUtils.isEmpty(pickBucket.getCode())) {
            pickBucket.setCode(sequenceGenerator.generateNext(Name.PICK_BUCKET));
        }
        pickBucket.setFacility(UserContext.current().getFacility());
        if (pickBucket.getCreated() == null) {
            pickBucket.setCreated(DateUtils.getCurrentTime());
        }
        sessionFactory.getCurrentSession().persist(pickBucket);
        return pickBucket;
    }

    @Override
    public List<PickBucket> getPickBuckets(List<String> pickBucketCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PickBucket where facility.id = :facilityId and code in (:pickBucketCodes)");
        query.setParameterList("pickBucketCodes", pickBucketCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<PicklistItem> getPicklistItemsForSoftAllocationBySkuCode(String skuCode, Integer quantity, Integer picklistId, List<PicklistItem.StatusCode> statusCodes,
            boolean checkPickBatch) {
        Query query = null;
        if (checkPickBatch) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select pi from PicklistItem pi, SaleOrderItem soi where pi.statusCode in (:statusCodes) and pi.picklist.id = :picklistId and soi.code = pi.saleOrderItemCode and soi.saleOrder.code = pi.saleOrderCode and  soi.itemType.skuCode = :skuCode and pi.pickBatch.id is null");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select pi from PicklistItem pi, SaleOrderItem soi where pi.statusCode in (:statusCodes) and pi.picklist.id = :picklistId and soi.code = pi.saleOrderItemCode and soi.saleOrder.code = pi.saleOrderCode and  soi.itemType.skuCode = :skuCode");
        }
        query.setParameter("skuCode", skuCode);
        query.setParameter("picklistId", picklistId);
        query.setParameterList("statusCodes", statusCodes);
        query.setMaxResults(quantity);
        return query.list();
    }

    @Override
    public List<PicklistItem> getPutbackAcceptedItemsBySkuCodeAndQCStatus(String skuCode, PicklistItem.QCStatus qcStatus, Integer quantity) {
        Query query = null;
        if (qcStatus == null) {
            query = sessionFactory.getCurrentSession().createQuery("from PicklistItem where statusCode = :putbackAcceptedStatus and skuCode = :skuCode");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "from PicklistItem where statusCode = :putbackAcceptedStatus and skuCode = :skuCode and qcStatusCode = :qcStatusCode");
            query.setParameter("qcStatusCode", qcStatus);
        }
        query.setParameter("skuCode", skuCode);
        query.setParameter("putbackAcceptedStatus", PicklistItem.StatusCode.PUTBACK_ACCEPTED);
        if (quantity != null) {
            query.setMaxResults(quantity);
        }
        return query.list();
    }

    @Override
    public int receivePicklistItems(Integer picklistId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "UPDATE PicklistItem set statusCode = :toStatusCode where picklist.id = :picklistId and statusCode = :createdStatusCode");
        query.setParameter("toStatusCode", PicklistItem.StatusCode.RECEIVED);
        query.setParameter("createdStatusCode", PicklistItem.StatusCode.CREATED);
        query.setParameter("picklistId", picklistId);
        return query.executeUpdate();
    }

    @Override
    public int setDefaultPickBatch(Integer picklistId) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "UPDATE picklist_item pi, pick_batch pb set pi.pick_batch_id = pb.id where pi.picklist_id = :picklistId and pb.picklist_id = :picklistId");
        query.setParameter("picklistId", picklistId);
        return query.executeUpdate();
    }

    @Override
    public List<String> getShippingPackageCodesInPicklist(Integer picklistId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT distinct pi.shippingPackageCode from PicklistItem pi where pi.picklist.id = :picklistId");
        query.setParameter("picklistId", picklistId);
        return query.list();
    }
}
