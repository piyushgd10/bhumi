/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.dao.inventory.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.CacheMode;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Repository;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.entity.InventoryAdjustment;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inventory.IInventoryDao;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.ItemTypeDTO;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class InventoryDaoImpl implements IInventoryDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#blockInventory(com.uniware.core.entity.ItemType)
     */
    @Override
    public ItemTypeInventory getItemTypeInventory(ItemType itemType, Type inventoryType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.type = :inventoryType and itemTypeInventory.itemType.skuCode = :skuCode and itemTypeInventory.quantity > 0 order by itemTypeInventory.priority desc, itemTypeInventory.ageingStartDate, itemTypeInventory.created");
        query.setParameter("skuCode", itemType.getSkuCode());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        return (ItemTypeInventory) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#updateItemTypeInventory(com.uniware.core.entity.ItemTypeInventory)
     */
    @Override
    public ItemTypeInventory updateItemTypeInventory(ItemTypeInventory itemTypeInventory) {
        itemTypeInventory.setFacility(UserContext.current().getFacility());
        return (ItemTypeInventory) sessionFactory.getCurrentSession().merge(itemTypeInventory);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#addItemTypeInventory(com.uniware.core.entity.ItemTypeInventory)
     */
    @Override
    public ItemTypeInventory addItemTypeInventory(ItemTypeInventory itemTypeInventory) {
        itemTypeInventory.setFacility(UserContext.current().getFacility());
        if (!itemTypeInventory.getFacility().getId().equals(itemTypeInventory.getShelf().getFacility().getId())) {
            throw new RuntimeException("Trying to add shelf with different facility than current facility:[" + itemTypeInventory.getShelf() + "], [" + itemTypeInventory + "]");
        }
        sessionFactory.getCurrentSession().persist(itemTypeInventory);
        return itemTypeInventory;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#getItemTypeInventoryById(int)
     */
    @Override
    public ItemTypeInventory getItemTypeInventoryById(int itemTypeInventoryId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select iti from ItemTypeInventory iti join fetch iti.shelf where iti.id = :itemTypeInventoryId");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        return (ItemTypeInventory) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#getInventoryForUpdate(com.uniware.core.entity.ItemType, com.uniware.core.entity.Shelf)
     */
    @Override
    public ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.id = :itemTypeId and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemType.getId());
        query.setParameter("shelfId", shelf.getId());
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        query.setCacheMode(CacheMode.REFRESH);
        return (ItemTypeInventory) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#markInventoryNotFound(int)
     */
    @Override
    public void markInventoryNotFound(int itemTypeInventoryId) {
        markInventoryNotFound(itemTypeInventoryId, 1);
    }

    @Override
    public void markInventoryNotFound(int itemTypeInventoryId, int qty) {
        Query query = sessionFactory.getCurrentSession().createQuery("select itemTypeInventory from ItemTypeInventory itemTypeInventory where id = :itemTypeInventoryId");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        ItemTypeInventory itemTypeInventory = (ItemTypeInventory) query.uniqueResult();
        itemTypeInventory.setQuantityNotFound(itemTypeInventory.getQuantityNotFound() + qty);
        sessionFactory.getCurrentSession().merge(itemTypeInventory);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#releaseInventory(int)
     */
    @Override
    public ItemTypeInventory releaseInventory(int itemTypeInventoryId, int quantity) {
        Query query = sessionFactory.getCurrentSession().createQuery("select itemTypeInventory from ItemTypeInventory itemTypeInventory where id = :itemTypeInventoryId");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        ItemTypeInventory itemTypeInventory = (ItemTypeInventory) query.uniqueResult();
        itemTypeInventory.setQuantity(itemTypeInventory.getQuantity() + quantity);
        itemTypeInventory.setQuantityBlocked(itemTypeInventory.getQuantityBlocked() - quantity);
        return (ItemTypeInventory) sessionFactory.getCurrentSession().merge(itemTypeInventory);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.dao.inventory.IInventoryDao#addItem(com.uniware.core.entity.Item)
     */
    @Override
    public Item addItem(Item item) {
        item.setFacility(UserContext.current().getFacility());
        return addItem(item, true);
    }

    @Override
    public Item addItem(Item item, boolean generateCode) {
        item.setFacility(UserContext.current().getFacility());
        if (generateCode) {
            item.setCode(sequenceGenerator.generateNext(Name.ITEM, ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getItemSequenceSuffixLength()));
        }
        sessionFactory.getCurrentSession().persist(item);
        return item;
    }

    @Override
    public List<Item> addItems(List<Item> items) {
        List<String> generatedCodes = sequenceGenerator.generateNextN(Name.ITEM, items.size(),
                ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getItemSequenceSuffixLength());
        for (Item item : items) {
            item.setFacility(UserContext.current().getFacility());
            if (StringUtils.isBlank(item.getCode())) {
                item.setCode(generatedCodes.remove(0));
            }
            sessionFactory.getCurrentSession().persist(item);
        }
        return items;
    }

    @Override
    public List<ItemTypeInventory> getNotFoundInventories(Integer itemTypeId, Integer shelfId, Type inventoryType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.id = :itemTypeId and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType and itemTypeInventory.quantityNotFound > 0 order by itemTypeInventory.quantityNotFound desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("shelfId", shelfId);
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        return query.list();
    }

    @Override
    public List<ItemTypeInventory> getNotFoundInventories(String skuCode, Type inventoryType, Shelf shelf) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.skuCode = :skuCode and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType and itemTypeInventory.quantityNotFound > 0 order by itemTypeInventory.quantityNotFound desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("shelfId", shelf.getId());
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        return query.list();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.uniware.dao.inventory.IInventoryDao#getItemById(int)
     */
    @Override
    public Item getItemById(int itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select i from Item i left join fetch i.vendor join fetch i.itemType where i.id = :itemId");
        query.setParameter("itemId", itemId);
        return (Item) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#getItemByCode(java.lang.String)
     */
    @Override
    public Item getItemByCode(String itemCode) {
        return getItemByCode(itemCode, false);
    }

    @Override
    public Item getItemByCode(String itemCode, boolean fetchSaleOrderItems) {
        StringBuilder queryBuilder = new StringBuilder("select i from Item i join fetch i.itemType ");
        if (fetchSaleOrderItems) {
            queryBuilder.append("left join fetch i.saleOrderItems soi left join fetch soi.shippingPackage sp ");
        }
        queryBuilder.append("left join fetch i.vendor where i.facility.id = :facilityId and i.code = :itemCode");
        Query query = sessionFactory.getCurrentSession().createQuery(queryBuilder.toString());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemCode", itemCode);
        return (Item) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#updateItem(com.uniware.core.entity.Item)
     */
    @Override
    public Item updateItem(Item item) {
        item.setFacility(UserContext.current().getFacility());
        return (Item) sessionFactory.getCurrentSession().merge(item);
    }

    @Override
    public List<Item> getItems(List<String> itemCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i join fetch i.itemType it join fetch it.category left join fetch i.inflowReceiptItem iri left join fetch iri.purchaseOrderItem left join fetch i.vendor where i.facility.id = :facilityId and i.code in (:codes)");
        query.setParameterList("codes", itemCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<Item> getItemsByCodes(List<String> itemCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i where i.facility.id = :facilityId and i.code in (:codes)");
        query.setParameterList("codes", itemCodes);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(Integer itemTypeId, List<Integer> facilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis where itis.facility.id in (:facilityIds) and itis.itemType.id = :itemTypeId");
        query.setParameterList("facilityIds", facilityIds);
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public ItemTypeInventorySnapshot getItemTypeInventorySnapshot(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemTypeInventorySnapshot itis where itis.facility.id = :facilityId and itis.itemType.id = :itemTypeId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        return (ItemTypeInventorySnapshot) query.uniqueResult();
    }

    @Override
    public List<ItemTypeInventorySnapshot> getAllItemTypeInventorySnapshots(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemTypeInventorySnapshot itis where itis.itemType.id = :itemTypeId");
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public List<Item> createItemLabels(Integer itemTypeId, String vendorSkuCode, Integer vendorId, Integer qty, Date ageingStartDate, boolean expirable) {
        List<Item> items = new ArrayList<Item>();
        for (int i = 0; i < qty; i++) {
            Item item = new Item();
            item.setFacility(UserContext.current().getFacility());
            item.setItemType(new ItemType(itemTypeId));
            item.setStatusCode(Item.StatusCode.UNPROCESSED.name());
            item.setCreated(DateUtils.getCurrentTime());
            item.setUpdated(DateUtils.getCurrentTime());
            if (ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getTraceabilityLevel().equals(SystemConfiguration.TraceabilityLevel.ITEM)) {
                if (expirable) {
                    item.setManufacturingDate(ageingStartDate);
                    item.setAgeingStartDate(ageingStartDate);
                }
                // else, leave it as null.
            } else {
                item.setAgeingStartDate(Constants.ITEM_SKU_OR_NONE_TRACEABILITY_AGEING_DATE);
            }
            if (StringUtils.isNotBlank(vendorSkuCode)) {
                item.setVendorItemCode(vendorSkuCode);
            }
            if (vendorId != null) {
                item.setVendor(new Vendor(vendorId));
            }
            items.add(item);
        }
        return addItems(items);
    }

    @Override
    public List<Item> createItemLabels(Integer itemTypeId, List<String> itemCodes) {
        List<Item> items = new ArrayList<>();
        for (String itemCode : itemCodes) {
            Item item = new Item();
            item.setFacility(UserContext.current().getFacility());
            item.setItemType(new ItemType(itemTypeId));
            item.setCode(itemCode);
            item.setStatusCode(Item.StatusCode.UNPROCESSED.name());
            item.setCreated(DateUtils.getCurrentTime());
            item.setUpdated(DateUtils.getCurrentTime());
            items.add(addItem(item, false));
        }
        return items;
    }

    @Override
    public List<Item> getItemsByInflowReceiptIdByStatus(Integer inflowReceiptItemId, Item.StatusCode status) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i join fetch i.itemType it join fetch it.category left join fetch i.inflowReceiptItem iri left join fetch iri.purchaseOrderItem where i.facility.id = :facilityId and i.inflowReceiptItem.id = :inflowReceiptItemId and i.statusCode = :statusCode");
        query.setParameter("inflowReceiptItemId", inflowReceiptItemId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", status.name());
        return query.list();
    }

    @Override
    public List<Item> getItemsByItemTypeByStatus(String skuCode, String status, Integer offset, Integer limit) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i where i.facility.id = :facilityId and i.itemType.skuCode = :skuCode and i.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("statusCode", status);
        if (limit != null) {
            query.setMaxResults(limit);
        }
        if (offset != null) {
            query.setFirstResult(offset);
        }
        return query.list();
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotByTenant(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis where itis.facility.tenant.id = :tenantId and itis.itemType.id = :itemTypeId and itis.itemType.type != :type order by inventory desc");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("type", ItemType.Type.BUNDLE);
        return query.list();
    }

    @Override
    public List<InventorySnapshotDTO> getPendingNonBundleItemTypeInventoryNotifications(List<Integer> facilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.inventory.SearchInventorySnapshotResponse$InventorySnapshotDTO(itis.itemType.id, itis.itemType.skuCode, sum(itis.pendingInventoryAssessment), sum(itis.openSale), sum(itis.inventory), sum(itis.openPurchase), sum(itis.putawayPending), sum(itis.pendingStockTransfer), sum(itis.vendorInventory), sum(itis.inventoryBlocked), sum(itis.virtualInventory)) from ItemTypeInventorySnapshot itis where itis.itemType.tenant.id = :tenantId and itis.itemType.type != :type and itis.facility.id in (:facilityIds) group by itis.itemType.id having min(acknowledged) = 0");
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("facilityIds", facilityIds);
        return query.list();
    }

    @Override
    public List<InventorySnapshotDTO> getPendingBundleItemTypeInventoryNotifications(List<Integer> facilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.inventory.SearchInventorySnapshotResponse$InventorySnapshotDTO(itis.itemType.id, itis.itemType.skuCode, sum(itis.pendingInventoryAssessment), sum(itis.openSale), sum(itis.inventory), sum(itis.openPurchase), sum(itis.putawayPending), sum(itis.pendingStockTransfer), sum(itis.vendorInventory), sum(itis.inventoryBlocked), sum(itis.virtualInventory)) from ItemTypeInventorySnapshot itis where itis.itemType.tenant.id = :tenantId and itis.itemType.type = :type and itis.facility.id in (:facilityIds) group by itis.itemType.id having min(acknowledged) = 0");
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("facilityIds", facilityIds);
        return query.list();
    }

    @Override
    public InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId, List<Integer> facilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.core.api.inventory.SearchInventorySnapshotResponse$InventorySnapshotDTO(itis.itemType.id, itis.itemType.skuCode, sum(itis.pendingInventoryAssessment), sum(itis.openSale), sum(itis.inventory), sum(itis.openPurchase), sum(itis.putawayPending), sum(itis.pendingStockTransfer), sum(itis.vendorInventory), sum(itis.inventoryBlocked), sum(itis.virtualInventory)) from ItemTypeInventorySnapshot itis where itis.itemType.tenant.id = :tenantId and itis.itemType.id = :itemTypeId and itis.facility.id in (:facilityIds) group by itis.itemType.id");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameterList("facilityIds", facilityIds);
        return (InventorySnapshotDTO) query.uniqueResult();
    }

    @Override
    public void markInventoryUpdateAcknowledged(Integer itemTypeId, Date lastAcknowledgedTime) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ItemTypeInventorySnapshot set acknowledged = 1 where item_type_id = :itemTypeId and updated <= :lastAcknowledgedTime");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("lastAcknowledgedTime", lastAcknowledgedTime);
        query.executeUpdate();
    }

    @Override
    public InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment) {
        inventoryAdjustment.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(inventoryAdjustment);
        return inventoryAdjustment;
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(List<String> itemTypeSKUs) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis join fetch itis.itemType it where it.skuCode in (:itemTypeSKUs) and itis.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("itemTypeSKUs", itemTypeSKUs);
        return query.list();

    }

    @Override
    public ItemTypeInventory getLatestItemTypeInventoryByItemType(int itemTypeId, Type inventoryType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iti from ItemTypeInventory iti join fetch iti.shelf where iti.type = :inventoryType and iti.facility.id = :facilityId and iti.itemType.id = :itemTypeId order by iti.updated desc");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("inventoryType", inventoryType);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (ItemTypeInventory) query.uniqueResult();
    }

    @Override
    public ItemTypeInventorySnapshot getItemTypeInventorySnapshot(String itemSKU) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis join fetch itis.itemType it where it.skuCode = :itemSKU and itis.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemSKU", itemSKU);
        return (ItemTypeInventorySnapshot) query.uniqueResult();
    }

    @Override
    public int reSyncAllItemTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("update ItemTypeInventorySnapshot set acknowledged = 0 where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.executeUpdate();
    }

    @Override
    public List<ItemTypeInventory> getItemTypeInventoryByItemType(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemTypeInventory iti where iti.itemType.id = :itemTypeId and iti.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public List<ItemTypeInventory> getItemTypeInventoryByItemType(Integer itemTypeId, List<Integer> facilityIds) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemTypeInventory iti where iti.itemType.id = :itemTypeId and iti.facility.id in (:facilityIds)");
        query.setParameterList("facilityIds", facilityIds);
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    @Override
    public List<Item> getItemsByShelfAndStatus(int shelfId, List<Item.StatusCode> statuses) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Item i where i.facility.id = :facilityId and i.statusCode in ( :statusCodes ) and i.shelf.id= :shelfId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("statusCodes", statuses.stream().map(Enum::name).collect(Collectors.toList()));
        query.setParameter("shelfId", shelfId);
        return query.list();
    }

    @Override
    public Item updateItemOptimistic(Item item, Integer shelfId, Integer cycleCountId) {
        //Query query = sessionFactory.getCurrentSession().createSQLQuery("update item set shelf_id = :shelfId, last_cycle_count_id = :cycleCountId where code= :itemCode and updated = :updated");
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "update item set shelf_id = :shelfId, last_cycle_count_id = :cycleCountId where code = :itemCode and updated = :updated");
        query.setParameter("shelfId", shelfId);
        query.setParameter("cycleCountId", cycleCountId);
        query.setParameter("itemCode", item.getCode());
        query.setParameter("updated", item.getUpdated());
        if (query.executeUpdate() == 0) {
            throw new HibernateOptimisticLockingFailureException(new StaleObjectStateException(Item.class.getCanonicalName(), item.getId()));
        }
        return getItemById(item.getId());
    }

    /**
     * Gets all inventory for shelf
     *
     * @param shelfId Id of shelf
     * @param lock whether to prevent other transactions from reading while we are getting inventory.
     * @return list of itemTypeInventories
     */
    @Override
    public List<ItemTypeInventory> getAllItemTypeInventoryForShelf(Integer shelfId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select iti from ItemTypeInventory iti where facility.id = :facilityId and shelf.id = :shelfId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shelfId", shelfId);
        if (lock) {
            query.setLockMode("iti", LockMode.PESSIMISTIC_WRITE);
        }
        return query.list();
    }

    @Override
    public ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf, Date ageingStartDate) {
        return getInventory(itemType.getSkuCode(), inventoryType, shelf, ageingStartDate);
    }

    @Override
    public ItemTypeInventory getInventory(String skuCode, Type inventoryType, Shelf shelf, Date ageingStartDate) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.skuCode = :skuCode and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType and ageingStartDate = :ageingStartDate");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("shelfId", shelf.getId());
        query.setParameter("inventoryType", inventoryType);
        query.setParameter("ageingStartDate", ageingStartDate);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        return (ItemTypeInventory) query.uniqueResult();
    }

    @Override
    public ItemTypeInventory getInventoryBySkuCodeAndShelf(String skuCode, Type inventoryType, Shelf shelf, boolean quantityGreaterThanZero) {
        String queryString = "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.skuCode = :skuCode and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType";
        if (quantityGreaterThanZero) {
            queryString += " and quantity > 0";
        }
        Query query = sessionFactory.getCurrentSession().createQuery(queryString);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("shelfId", shelf.getId());
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        return (ItemTypeInventory) query.uniqueResult();
    }

    @Override
    public List<Item> getItemsForAutoAging(String skuCode, int idToProcessFrom, int batchSize, int allowedShelfLife) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i join fetch i.shelf where i.facility.id = :facilityId and i.id > :idToProcessFrom and i.itemType.skuCode = :skuCode and i.manufacturingDate < :allowedMinMfgDate and ((i.statusCode = :goodInventory) or (i.statusCode = :badInventory and i.rejectionReason = :aboutToExpire)) order by i.id");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("skuCode", skuCode);
        query.setParameter("goodInventory", Item.StatusCode.GOOD_INVENTORY.name());
        query.setParameter("badInventory", Item.StatusCode.BAD_INVENTORY.name());
        query.setParameter("aboutToExpire", Item.RejectionReason.ABOUT_TO_EXPIRE.name());
        query.setParameter("allowedMinMfgDate", DateUtils.addDaysToDate(DateUtils.getCurrentDate(), -allowedShelfLife));
        query.setParameter("idToProcessFrom", idToProcessFrom);
        query.setFetchSize(batchSize);
        return query.list();
    }

    @Override
    public Item getItemByCode(String itemCode, boolean fetchSaleOrderItems, boolean fetchShelf) {
        StringBuilder queryBuilder = new StringBuilder("select i from Item i left join fetch");
        if (fetchSaleOrderItems) {
            queryBuilder.append(" i.saleOrderItems soi left join fetch soi.shippingPackage sp left join fetch");
        }
        if (fetchShelf) {
            queryBuilder.append(" i.shelf s left join fetch");
        }
        queryBuilder.append(" i.vendor where i.facility.id = :facilityId and i.code = :itemCode");
        Query query = sessionFactory.getCurrentSession().createQuery(queryBuilder.toString());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemCode", itemCode);
        return (Item) query.uniqueResult();
    }

    @Override
    public ItemTypeInventory getNotFoundInventory(Integer itemTypeId, Type inventoryType, Shelf shelf) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select itemTypeInventory from ItemTypeInventory itemTypeInventory where facility.id = :facilityId and itemTypeInventory.itemType.id = :itemTypeId and itemTypeInventory.shelf.id = :shelfId and itemTypeInventory.type=:inventoryType and itemTypeInventory.quantityNotFound > 0 order by itemTypeInventory.quantityNotFound desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("shelfId", shelf.getId());
        query.setParameter("inventoryType", inventoryType);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        return (ItemTypeInventory) query.uniqueResult();
    }

    @Override
    public ItemTypeInventory commitBlockedInventory(int itemTypeInventoryId, int quantity) {
        Query query = sessionFactory.getCurrentSession().createQuery("select itemTypeInventory from ItemTypeInventory itemTypeInventory where id = :itemTypeInventoryId");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        ItemTypeInventory itemTypeInventory = (ItemTypeInventory) query.uniqueResult();
        itemTypeInventory.setQuantityBlocked(itemTypeInventory.getQuantityBlocked() - quantity);
        return (ItemTypeInventory) sessionFactory.getCurrentSession().merge(itemTypeInventory);
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsInDateRange(DateRange dateRange) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis join fetch itis.itemType it where itis.updated > :pastDate and itis.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("pastDate", dateRange.getStart());
        return query.list();
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsBySKUsInDateRange(DateRange dateRange, List<String> itemTypeSKUs) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventorySnapshot itis join fetch itis.itemType it where it.skuCode in (:itemTypeSKUs) and itis.updated > :pastDate and itis.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("itemTypeSKUs", itemTypeSKUs);
        query.setParameter("pastDate", dateRange.getStart());
        return query.list();

    }

    @Override
    public Item getItemByCodeAcrossFacility(String itemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select i from Item i join i.facility f left join fetch i.vendor v where f.tenant.id = :tenantId and i.code = :itemCode");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemCode", itemCode);
        return (Item) query.uniqueResult();
    }

    @Override
    public void persist(ItemTypeInventorySnapshot itis) {
        sessionFactory.getCurrentSession().persist(itis);
    }

    @Override
    public ItemTypeInventorySnapshot update(ItemTypeInventorySnapshot itis) {
        return (ItemTypeInventorySnapshot) sessionFactory.getCurrentSession().merge(itis);
    }

    @Override
    public boolean markInventorySnapshotDirty(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ItemTypeInventorySnapshot itis set itis.acknowledged = :unacknowldged where itis.itemType.id = :itemTypeId");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("unacknowldged", false);
        return query.executeUpdate() > 0;
    }

    @Override
    public List<ItemTypeDTO> getItemTypesToBeReassessed() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select new com.uniware.services.tasks.saleorder.SaleOrderProcessor$ItemTypeDTO(itis.itemType.id, itis.reassessVersion) from ItemTypeInventorySnapshot itis where itis.inventory > 0 and itis.reassessVersion != 0 and itis.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public boolean resetReassessVersionIfLatest(Integer itemTypeId, int reassessVersion) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update ItemTypeInventorySnapshot itis set itis.reassessVersion = 0 where itis.itemType.id = :itemTypeId and itis.reassessVersion = :reassessVersion and itis.facility.id = :facilityId");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("reassessVersion", reassessVersion);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.executeUpdate() > 0;
    }

    @Override
    public ItemTypeInventory getItemTypeInventoryFromActiveShelf(String skuCode, Type inventoryType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ItemTypeInventory where itemType.skuCode = :skuCode and facility.id = :facilityId and shelf.statusCode in (:shelfStatuses) and type = :inventoryType and quantity > 0");
        query.setParameter("skuCode", skuCode);
        query.setParameter("inventoryType", inventoryType);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameterList("shelfStatuses", Arrays.asList(Shelf.StatusCode.ACTIVE.name(), Shelf.StatusCode.QUEUED_FOR_COUNT.name()));
        query.setLockMode("itemTypeInventory", LockMode.PESSIMISTIC_WRITE);
        query.setMaxResults(1);
        return (ItemTypeInventory) query.uniqueResult();
    }

    /**
     * @param skuCode
     * @param ageingStartDate
     * @param type
     * @param shelfId
     * @return true if {@code ItemTypeInventory} of {@code skuCode, type, type} with ageing different than
     *         {@code ageingStartDate} is present.
     */
    @Override
    public boolean isItemPresentWithDifferentAgeingOnShelf(String skuCode, Date ageingStartDate, Type type, int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(iti) from ItemTypeInventory iti where iti.shelf.id= :shelfId and iti.itemType.skuCode = :skuCode and iti.ageingStartDate != :ageingStartDate and iti.type = :inventoryType and (iti.quantity > 0 or iti.quantityBlocked>0 or iti.quantityNotFound>0 or iti.quantityDamaged>0 or iti.quantityLost>0) and iti.facility.id = :facilityId");
        query.setParameter("skuCode", skuCode);
        query.setParameter("ageingStartDate", ageingStartDate);
        query.setParameter("shelfId", shelfId);
        query.setParameter("inventoryType", type);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Long) query.uniqueResult() > 0;
    }

    @Override
    public Item getItemByCodeAndFacility(String itemCode, String facilityCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select i from Item i where i.facility.code = :facilityCode and i.code = :itemCode");
        query.setParameter("facilityCode", facilityCode);
        query.setParameter("itemCode", itemCode);
        return (Item) query.uniqueResult();
    }

    @Override
    public Item updateItemForFacility(Item item) {
        return (Item) sessionFactory.getCurrentSession().merge(item);
    }

    @Override
    public Long getInventoryCountByShelfId(Integer shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select (sum(quantity) + sum(quantityBlocked) + sum(quantityNotFound) + sum(quantityDamaged) + sum(quantityLost)) from ItemTypeInventory iti where iti.shelf.id = :shelfId");
        query.setParameter("shelfId", shelfId);
        return (Long) query.uniqueResult();
    }

    @Override
    public List<Item> getItemsByItemTypeAndShelfAndStatus(Integer itemTypeId, Integer shelfId, String status) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i where i.facility.id = :facilityId and i.statusCode = :statusCode and i.shelf.id= :shelfId and i.itemType.id = :itemTypeId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("statusCode", status);
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("shelfId", shelfId);
        return query.list();
    }

    @Override
    public Long getNumberOfItemsWithoutManufacturingDateForCategory(String categoryCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(i) from Item i join i.itemType it where it.category.code = :categoryCode and i.manufacturingDate is null and i.statusCode in (:statusCodes)");
        List<String> statusCodes = new ArrayList<>(Arrays.asList(Item.StatusCode.GOOD_INVENTORY.name(), Item.StatusCode.BAD_INVENTORY.name(), Item.StatusCode.CREATED.name(),
                Item.StatusCode.QC_COMPLETE.name(), Item.StatusCode.QC_REJECTED.name(), Item.StatusCode.QC_PENDING.name()));
        query.setParameterList("statusCodes", statusCodes);
        query.setParameter("categoryCode", categoryCode);
        return (Long) query.uniqueResult();
    }

    @Override
    public List<ItemTypeInventory> getItemTypeInventoryByIdAndShelf(Integer itemTypeId, Integer shelfId, Type inventoryType, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iti from ItemTypeInventory iti where facility.id = :facilityId and itemType.id = :itemTypeId and shelf.id = :shelfId and type= :inventoryType");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("shelfId", shelfId);
        query.setParameter("inventoryType", inventoryType);
        if (lock) {
            query.setLockMode("iti", LockMode.PESSIMISTIC_WRITE);
        }
        return query.list();
    }

    @Override
    public List<Integer> getPendingNonBundleItemTypesForInventoryNotifications(List<Integer> facilityIds, int start, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct itis.itemType.id from ItemTypeInventorySnapshot itis where itis.itemType.tenant.id = :tenantId and itis.facility.id in (:facilityIds) and itis.itemType.type != :type and itis.acknowledged = 0");
        query.setParameter("type", ItemType.Type.BUNDLE);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("facilityIds", facilityIds);
        query.setFirstResult(start);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @Override
    public List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsForItemTypes(List<Integer> itemTypeIds) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ItemTypeInventorySnapshot itis where itis.itemType.id in (:itemTypeIds)");
        query.setParameterList("itemTypeIds", itemTypeIds);
        return query.list();
    }
}
