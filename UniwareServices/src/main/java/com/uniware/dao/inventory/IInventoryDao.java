/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 22, 2011
 *  @author singla
 */
package com.uniware.dao.inventory;

import java.util.Date;
import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.inventory.SearchInventorySnapshotResponse.InventorySnapshotDTO;
import com.uniware.core.entity.InventoryAdjustment;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.ItemTypeInventorySnapshot;
import com.uniware.core.entity.Shelf;
import com.uniware.services.exception.InventoryNotAvailableException;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.ItemTypeDTO;

/**
 * @author singla
 */
public interface IInventoryDao {

    /**
     * @param itemType
     * @return
     * @throws InventoryNotAvailableException
     */
    ItemTypeInventory getItemTypeInventory(ItemType itemType, Type inventoryType);

    /**
     * @param itemType
     * @param inventoryType
     * @param shelf
     * @return
     */
    ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf);

    /**
     * @param itemTypeInventory
     * @return
     */
    ItemTypeInventory updateItemTypeInventory(ItemTypeInventory itemTypeInventory);

    /**
     * @param itemTypeInventoryId
     * @return
     */
    ItemTypeInventory getItemTypeInventoryById(int itemTypeInventoryId);

    /**
     * @param itemTypeId
     * @param inventoryType
     * @return
     */
    ItemTypeInventory getLatestItemTypeInventoryByItemType(int itemTypeId, Type inventoryType);

    /**
     * @param itemTypeInventory
     * @return
     */
    ItemTypeInventory addItemTypeInventory(ItemTypeInventory itemTypeInventory);

    /**
     * @param itemTypeId
     */
    void markInventoryNotFound(int itemTypeId);

    void markInventoryNotFound(int itemTypeInventoryId, int qty);

    /**
     * @param itemTypeInventoryId
     */
    ItemTypeInventory releaseInventory(int itemTypeInventoryId, int quantity);

    /**
     * @param item
     * @return
     */
    Item addItem(Item item);

    /**
     * @param itemId
     * @return
     */
    Item getItemById(int itemId);

    /**
     * @param itemCode
     * @return
     */
    Item getItemByCode(String itemCode);

    /**
     * @param item
     * @return
     */
    Item updateItem(Item item);

    /**
     * @param itemCodes
     * @return
     */
    List<Item> getItems(List<String> itemCodes);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(Integer itemTypeId, List<Integer> facilityIds);

    ItemTypeInventorySnapshot getItemTypeInventorySnapshot(Integer itemTypeId);

    List<Item> createItemLabels(Integer itemTypeId, String vendorSkuCode, Integer vendorId, Integer qty, Date ageingStartDate, boolean expirable);

    /**
     * @param item
     * @param generateCode
     * @return
     */
    Item addItem(Item item, boolean generateCode);

    /**
     * @param itemTypeId
     * @param itemCodes
     * @return
     */
    List<Item> createItemLabels(Integer itemTypeId, List<String> itemCodes);

    List<Item> getItemsByInflowReceiptIdByStatus(Integer inflowReceiptItemId, Item.StatusCode statusCode);

    List<Item> getItemsByItemTypeByStatus(String skuCode, String status, Integer offset, Integer limit);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotByTenant(Integer itemTypeId);

    /**
     * @return
     */
    List<InventorySnapshotDTO> getPendingNonBundleItemTypeInventoryNotifications(List<Integer> facilityIds);

    /**
     * @param lastAcknowledgedTime
     * @param itemTypeId
     */
    void markInventoryUpdateAcknowledged(Integer itemTypeId, Date lastAcknowledgedTime);

    boolean markInventorySnapshotDirty(Integer itemTypeId);

    /**
     * @param inventoryAdjustment
     * @return
     */
    InventoryAdjustment addInventoryAdjustment(InventoryAdjustment inventoryAdjustment);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshots(List<String> itemTypeSKUs);

    /**
     * @param skuCode
     * @return
     */
    ItemTypeInventorySnapshot getItemTypeInventorySnapshot(String skuCode);

    Item getItemByCode(String itemCode, boolean lock);

    List<ItemTypeInventory> getItemTypeInventoryByItemType(Integer itemTypeId);

    ItemTypeInventory commitBlockedInventory(int itemTypeInventoryId, int quantity);

    int reSyncAllItemTypes();

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsInDateRange(DateRange dateRange);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsBySKUsInDateRange(DateRange dateRange, List<String> itemTypeSKUs);

    Item getItemByCodeAcrossFacility(String itemCode);

    InventorySnapshotDTO getItemTypeInventoryNotification(int itemTypeId, List<Integer> facilityIds);

    List<ItemTypeInventorySnapshot> getAllItemTypeInventorySnapshots(Integer itemTypeId);

    void persist(ItemTypeInventorySnapshot itis);

    List<InventorySnapshotDTO> getPendingBundleItemTypeInventoryNotifications(List<Integer> facilityIds);

    ItemTypeInventorySnapshot update(ItemTypeInventorySnapshot itis);

    List<ItemTypeDTO> getItemTypesToBeReassessed();

    boolean resetReassessVersionIfLatest(Integer itemTypeId, int reassessVersion);

    List<ItemTypeInventory> getItemTypeInventoryByItemType(Integer itemTypeId, List<Integer> facilityIds);

    Item getItemByCode(String itemCode, boolean fetchSaleOrderItems, boolean fetchShelf);

    ItemTypeInventory getNotFoundInventory(Integer itemTypeId, Type inventoryType, Shelf shelf);

    List<Item> getItemsByShelfAndStatus(int shelfId, List<Item.StatusCode> statuses);

    Item updateItemOptimistic(Item item, Integer shelfId, Integer cycleCountId);

    List<ItemTypeInventory> getAllItemTypeInventoryForShelf(Integer shelfId, boolean lock);

    ItemTypeInventory getInventory(ItemType itemType, Type inventoryType, Shelf shelf, Date ageingStartDate);

    ItemTypeInventory getInventory(String skuCode, Type inventoryType, Shelf shelf, Date ageingStartDate);

    ItemTypeInventory getInventoryBySkuCodeAndShelf(String skuCode, Type inventoryType, Shelf shelf, boolean quantityGreaterThanZero);

    List<Item> getItemsForAutoAging(String skuCode, int idToProcessFrom, int batchSize, int shelfLife);

    ItemTypeInventory getItemTypeInventoryFromActiveShelf(String skuCode, Type inventoryType);

    boolean isItemPresentWithDifferentAgeingOnShelf(String skuCode, Date ageingStartDate, Type type, int shelfId);

    List<Item> getItemsByItemTypeAndShelfAndStatus(Integer itemTypeId, Integer shelfId, String status);

    Item getItemByCodeAndFacility(String itemCode, String facilityCode);

    Item updateItemForFacility(Item item);

    Long getInventoryCountByShelfId(Integer shelfId);

    Long getNumberOfItemsWithoutManufacturingDateForCategory(String categoryCode);

    List<ItemTypeInventory> getItemTypeInventoryByIdAndShelf(Integer itemTypeId, Integer shelfId, Type inventoryType, boolean lock);

    List<Item> addItems(List<Item> items);

    List<ItemTypeInventory> getNotFoundInventories(Integer itemTypeId, Integer shelfId, Type inventoryType);

    List<Integer> getPendingNonBundleItemTypesForInventoryNotifications(List<Integer> facilityIds, int start, int pageSize);

    List<ItemTypeInventorySnapshot> getItemTypeInventorySnapshotsForItemTypes(List<Integer> itemTypeIds);

    List<ItemTypeInventory> getNotFoundInventories(String skuCode, Type inventoryType, Shelf shelf);

    List<Item> getItemsByCodes(List<String> itemCodes);
}
