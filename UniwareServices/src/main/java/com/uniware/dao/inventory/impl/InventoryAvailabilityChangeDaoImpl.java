package com.uniware.dao.inventory.impl;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.InventoryAvailabilityChangeLedger;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inventory.IInventoryAvailabilityChangeDao;
import com.uniware.services.tasks.notifications.AcknowledgeInventoryLedgerTask;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by piyush on 1/6/16.
 */
@Repository
public class InventoryAvailabilityChangeDaoImpl implements IInventoryAvailabilityChangeDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<InventoryAvailabilityChangeLedger> getAllPendingStockoutLedgers(){
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sil from InventoryAvailabilityChangeLedger sil where acknowledged = :acknowledged and (facility.id in :facilityIds or tenant.id = :tenantId) order by id");
        query.setParameter("acknowledged", 0);
        List<Integer> facilityIds = new ArrayList<Integer>();
        List<Facility> facilities = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities();
        for (Facility facility : facilities) {
            facilityIds.add(facility.getId());
        }
        query.setParameterList("facilityIds", facilityIds);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setFirstResult(0);
        return query.list();
    }

    @Override
    public InventoryAvailabilityChangeLedger updateInventoryStockoutLedger(InventoryAvailabilityChangeLedger inventoryAvailabilityChangeLedger) {
        return (InventoryAvailabilityChangeLedger) sessionFactory.getCurrentSession().merge(inventoryAvailabilityChangeLedger);
    }

    @Override
    public AcknowledgeInventoryLedgerTask.UnacknowledgedInventoryLedgerStatus getUnacknowledgedInventoryLedgerStatus() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select max(sil.id), count(sil.id) from InventoryAvailabilityChangeLedger sil where acknowledged = :acknowledged and (facility.id in :facilityIds or tenant.id = :tenantId) order by id");
        query.setParameter("acknowledged", 0);
        List<Integer> facilityIds = new ArrayList<Integer>();
        List<Facility> facilities = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities();
        for (Facility facility : facilities) {
            facilityIds.add(facility.getId());
        }
        query.setParameterList("facilityIds", facilityIds);
        query.setParameter("tenantId", UserContext.current().getTenantId());

        Object[] unacknowledgedInventoryLedgerStatus = (Object[]) query.uniqueResult();
        return new AcknowledgeInventoryLedgerTask.UnacknowledgedInventoryLedgerStatus((unacknowledgedInventoryLedgerStatus[0] != null ? ((Number) unacknowledgedInventoryLedgerStatus[0]).intValue() : Integer.MAX_VALUE),
                ((Number) unacknowledgedInventoryLedgerStatus[1]).intValue());
    }

    @Override
    public List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgers(int batchSize, int maxId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sil from InventoryAvailabilityChangeLedger sil where acknowledged = :acknowledged and (facility.id in :facilityIds or tenant.id = :tenantId) and id <= :maxId order by id");
        query.setParameter("acknowledged", 0);
        List<Integer> facilityIds = new ArrayList<Integer>();
        List<Facility> facilities = CacheManager.getInstance().getCache(FacilityCache.class).getFacilities();
        for (Facility facility : facilities) {
            facilityIds.add(facility.getId());
        }
        query.setParameterList("facilityIds", facilityIds);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("maxId", maxId);
        query.setFirstResult(0);
        query.setMaxResults(batchSize);
        return query.list();
    }

    @Override
    public List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgersByFacilityId(int batchSize, int maxId, int facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select sil from InventoryAvailabilityChangeLedger sil where acknowledged = :acknowledged and (facility.id = :facilityId and tenant.id = :tenantId) and id <= :maxId order by id");
        query.setParameter("acknowledged", 1);
        query.setParameter("facilityId", facilityId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("maxId", maxId);
        query.setFirstResult(0);
        query.setMaxResults(batchSize);
        return query.list();
    }

    @Override
    public int markStockoutInventoryLedgersAcknowledged(List<Integer> processedInventoryLedgerIds) {
        Query query = sessionFactory.getCurrentSession().createQuery("update InventoryAvailabilityChangeLedger set acknowledged = 1 where id in :processedInventoryLedgerIds");
        query.setParameterList("processedInventoryLedgerIds", processedInventoryLedgerIds);
        return query.executeUpdate();
    }
}
