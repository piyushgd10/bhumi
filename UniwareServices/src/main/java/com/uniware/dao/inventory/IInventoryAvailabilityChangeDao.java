package com.uniware.dao.inventory;

import com.uniware.core.entity.InventoryAvailabilityChangeLedger;
import com.uniware.services.tasks.notifications.AcknowledgeInventoryLedgerTask;

import java.util.List;

/**
 * Created by piyush on 1/6/16.
 */
public interface IInventoryAvailabilityChangeDao {

    List<InventoryAvailabilityChangeLedger> getAllPendingStockoutLedgers();

    InventoryAvailabilityChangeLedger updateInventoryStockoutLedger(InventoryAvailabilityChangeLedger inventoryAvailabilityChangeLedger);

    AcknowledgeInventoryLedgerTask.UnacknowledgedInventoryLedgerStatus getUnacknowledgedInventoryLedgerStatus();

    List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgers(int batchSize, int maxId);

    List<InventoryAvailabilityChangeLedger> getUnacknowledgedInventoryLedgersByFacilityId(int batchSize, int maxId, int facilityId);

    int markStockoutInventoryLedgersAcknowledged(List<Integer> processedInventoryLedgerIds);
}
