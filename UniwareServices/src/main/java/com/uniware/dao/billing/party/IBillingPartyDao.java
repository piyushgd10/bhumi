/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.billing.party;

import java.util.List;

import com.uniware.core.entity.BillingParty;

public interface IBillingPartyDao {

    List<BillingParty> searchBillingParty(String keyword);

    BillingParty getBillingPartyByCode(String code);

    BillingParty createBillingParty(BillingParty billingParty);

    BillingParty updateBillingParty(BillingParty billingParty);

    BillingParty getBillingPartyById(int billingPartyId);

    List<BillingParty> getAllBillingParties();

    List<BillingParty> getAllBillingPartiesWithGstNumber();
}
