/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.billing.party.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.BillingParty;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.billing.party.IBillingPartyDao;

@Repository
public class BillingPartyDaoImpl implements IBillingPartyDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<BillingParty> searchBillingParty(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from BillingParty where tenant.id = :tenantId and (name like :keyword or code like :keyword) and enabled = 1 order by name asc");
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public BillingParty getBillingPartyByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from BillingParty bp left join fetch bp.partyAddresses pa left join fetch pa.partyAddressType where bp.tenant.id = :tenantId and bp.code = :code");
        query.setParameter("code", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (BillingParty) query.uniqueResult();
    }

    @Override
    public BillingParty createBillingParty(BillingParty billingParty) {
        billingParty.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(billingParty);
        return billingParty;
    }

    @Override
    public BillingParty updateBillingParty(BillingParty billingParty) {
        billingParty.setTenant(UserContext.current().getTenant());
        return (BillingParty) sessionFactory.getCurrentSession().merge(billingParty);
    }

    @Override
    public BillingParty getBillingPartyById(int billingPartyId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from BillingParty bp left join fetch bp.partyAddresses pa left join fetch pa.partyAddressType where bp.id = :billingPartyId");
        query.setParameter("billingPartyId", billingPartyId);
        return (BillingParty) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BillingParty> getAllBillingParties() {
        Query query = sessionFactory.getCurrentSession().createQuery("from BillingParty where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<BillingParty> getAllBillingPartiesWithGstNumber() {
        Query query = sessionFactory.getCurrentSession().createQuery("from BillingParty where tenant.id = :tenantId and gstNumber is not null");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

}
