/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2012
 *  @author praveeng
 */
package com.uniware.dao.transition;

import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorItemType;

import java.util.List;

public interface ITransitionDao {

    /**
     * @param code
     * @return
     */
    PurchaseOrder getPurchaseOrderByCode(String code);

    /**
     * @param purchaseOrder
     * @return
     */
    PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder);

    /**
     * @param vendorAgreement
     * @return
     */
    VendorAgreement addVendorAgreement(VendorAgreement vendorAgreement);

    /**
     * @param code
     * @param itemTypeSkuCode
     * @return
     */
    PurchaseOrderItem getPurchaseOrderItem(String purchaseOrderCode, String itemTypeSkuCode);

    /**
     * @param purchaseOrderItem
     * @return
     */
    PurchaseOrderItem addPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem);

    /**
     * @param purchaseOrderItem
     * @return
     */
    PurchaseOrderItem updatePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem);

    /**
     * @param inflowReceipt
     * @return
     */
    InflowReceipt addInflowReceipt(InflowReceipt inflowReceipt);

    /**
     * @param inflowReceiptId
     * @param purchaseOrderItemId
     * @return
     */
    InflowReceiptItem getInflowReceiptItem(Integer inflowReceiptId, Integer purchaseOrderItemId);

    /**
     * @param inflowReceiptItem
     * @return
     */
    InflowReceiptItem addInflowReceiptItem(InflowReceiptItem inflowReceiptItem);

    /**
     * @param inflowReceiptItem
     * @return
     */
    InflowReceiptItem updateInflowReceiptItem(InflowReceiptItem inflowReceiptItem);

    /**
     * @param itemTypeId
     * @return
     */
    Integer getGoodInventoryCount(Integer itemTypeId);

    /**
     * @param string
     * @return
     */
    Putaway getPutawayByCode(String string);

    /**
     * @param putaway
     * @return
     */
    Putaway addPutaway(Putaway putaway);

    /**
     * @param id
     * @param id2
     * @return
     */
    PutawayItem getPutawayItem(int putawayId, int itemTypeId);

    /**
     * @param putawayItem
     * @return
     */
    PutawayItem addPutawayItem(PutawayItem putawayItem);

    /**
     * @param putawayItem
     * @return
     */
    PutawayItem updatePutawayItem(PutawayItem putawayItem);

    /**
     * @param itemTypeId
     * @return
     */
    List<Object[]> getShelfToItemTypeCount(Integer itemTypeId);

    /**
     * @param itemTypeId
     * @return
     */
    List<VendorItemType> getVendorItemTypeByItemTypeId(Integer itemTypeId);

    /**
     * @param item
     * @return
     */
    int removeItem(String code);

    /**
     * @param itemSKU
     * @return
     */
    List<Object[]> getItemCountGroupByShelfAndAgeing(String itemSKU);

    /**
     * @param shelfId
     * @param itemTypeId
     */
    void updateItemsStatus(Integer shelfId, Integer itemTypeId);

    /**
     * @param itemTypeId
     * @return
     */
    List<Item> getItemsInTransition(Integer itemTypeId);

    /**
     * @param itemCode
     * @return
     */
    Item getItemByCode(String itemCode);

}
