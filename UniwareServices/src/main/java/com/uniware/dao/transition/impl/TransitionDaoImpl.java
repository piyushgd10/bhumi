/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-May-2012
 *  @author praveeng
 */
package com.uniware.dao.transition.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.PurchaseOrderItem;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.VendorAgreement;
import com.uniware.core.entity.VendorItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.transition.ITransitionDao;

@Repository
public class TransitionDaoImpl implements ITransitionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public PurchaseOrder getPurchaseOrderByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PurchaseOrder where facility.id = :facilityId and code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (PurchaseOrder) query.uniqueResult();
    }

    @Override
    public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) {
        purchaseOrder.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(purchaseOrder);
        return purchaseOrder;
    }

    @Override
    public VendorAgreement addVendorAgreement(VendorAgreement vendorAgreement) {
        sessionFactory.getCurrentSession().persist(vendorAgreement);
        return vendorAgreement;
    }

    @Override
    public PurchaseOrderItem getPurchaseOrderItem(String purchaseOrderCode, String itemTypeSkuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PurchaseOrderItem where purchaseOrder.facility.id = :facilityId and purchaseOrder.code = :purchaseOrderCode and itemType.skuCode = :itemTypeSkuCode");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameter("itemTypeSkuCode", itemTypeSkuCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (PurchaseOrderItem) query.uniqueResult();
    }

    @Override
    public PurchaseOrderItem addPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
        sessionFactory.getCurrentSession().persist(purchaseOrderItem);
        return purchaseOrderItem;
    }

    @Override
    public PurchaseOrderItem updatePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
        return (PurchaseOrderItem) sessionFactory.getCurrentSession().merge(purchaseOrderItem);
    }

    @Override
    public InflowReceipt addInflowReceipt(InflowReceipt inflowReceipt) {
        inflowReceipt.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(inflowReceipt);
        sessionFactory.getCurrentSession().flush();
        inflowReceipt.setCode("TRANSIT-" + inflowReceipt.getId());
        return (InflowReceipt) sessionFactory.getCurrentSession().merge(inflowReceipt);
    }

    @Override
    public InflowReceiptItem getInflowReceiptItem(Integer inflowReceiptId, Integer purchaseOrderItemId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from InflowReceiptItem where inflowReceipt.facility.id = :facilityId and inflowReceipt.id = :inflowReceiptId and purchaseOrderItem.id = :purchaseOrderItemId");
        query.setParameter("inflowReceiptId", inflowReceiptId);
        query.setParameter("purchaseOrderItemId", purchaseOrderItemId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (InflowReceiptItem) query.uniqueResult();
    }

    @Override
    public InflowReceiptItem addInflowReceiptItem(InflowReceiptItem inflowReceiptItem) {
        sessionFactory.getCurrentSession().persist(inflowReceiptItem);
        return inflowReceiptItem;
    }

    @Override
    public InflowReceiptItem updateInflowReceiptItem(InflowReceiptItem inflowReceiptItem) {
        return (InflowReceiptItem) sessionFactory.getCurrentSession().merge(inflowReceiptItem);
    }

    @Override
    public Integer getGoodInventoryCount(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(i) from Item i where i.facility.id = :facilityId and i.itemType.id = :itemTypeId and i.statusCode = :statusCode and i.putaway.code = 'TRANSITION'");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("statusCode", Item.StatusCode.GOOD_INVENTORY.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return ((Long) query.uniqueResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Item> getItemsInTransition(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Item i where i.facility.id = :facilityId and i.itemType.id = :itemTypeId and i.statusCode = :statusCode and i.putaway.code = 'TRANSITION'");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("statusCode", Item.StatusCode.IN_TRANSITION.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.transition.ITransitionDao#getPutawayByCode(java.lang.String)
     */
    @Override
    public Putaway getPutawayByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Putaway where facility.id = :facilityId and code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Putaway) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.transition.ITransitionDao#addPutaway(com.uniware.core.entity.Putaway)
     */
    @Override
    public Putaway addPutaway(Putaway putaway) {
        putaway.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(putaway);
        return putaway;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.transition.ITransitionDao#getPutawayItem(int, int)
     */
    @Override
    public PutawayItem getPutawayItem(int putawayId, int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PutawayItem where putaway.facility.id = :facilityId and itemType.id = :itemTypeId and putaway.id = :putawayId");
        query.setParameter("putawayId", putawayId);
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (PutawayItem) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.transition.ITransitionDao#addPutawayItem(com.uniware.core.entity.PutawayItem)
     */
    @Override
    public PutawayItem addPutawayItem(PutawayItem putawayItem) {
        sessionFactory.getCurrentSession().persist(putawayItem);
        return putawayItem;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.transition.ITransitionDao#updatePutawayItem(com.uniware.core.entity.PutawayItem)
     */
    @Override
    public PutawayItem updatePutawayItem(PutawayItem putawayItem) {
        return (PutawayItem) sessionFactory.getCurrentSession().merge(putawayItem);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> getShelfToItemTypeCount(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iti.shelf.code, iti.quantity from ItemTypeInventory iti where iti.facility.id = :facilityId and iti.type=:inventoryType and iti.itemType.id = :itemTypeId");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("inventoryType", Type.GOOD_INVENTORY);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorItemType> getVendorItemTypeByItemTypeId(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VendorItemType where vendor.facility.id = :facilityId and enabled = 1 and itemType.id = :itemTypeId");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public int removeItem(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from Item where facility.id = :facilityId and code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> getItemCountGroupByShelfAndAgeing(String itemSKU) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(i), i.shelf, i.ageingStartDate from Item i where i.facility.id = :facilityId and i.itemType.skuCode = :itemSKU and i.statusCode = :statusCode group by i.shelf.id, i.ageingStartDate");
        query.setParameter("itemSKU", itemSKU);
        query.setParameter("statusCode", Item.StatusCode.IN_TRANSITION.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public void updateItemsStatus(Integer shelfId, Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set statusCode = :toStatus where facility.id = :facilityId and shelf.id = :shelfId and statusCode = :fromStatus and itemType.id = :itemTypeId");
        query.setParameter("shelfId", shelfId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("toStatus", Item.StatusCode.GOOD_INVENTORY.name());
        query.setParameter("fromStatus", Item.StatusCode.IN_TRANSITION.name());
        query.setParameter("itemTypeId", itemTypeId);
        query.executeUpdate();
    }

    @Override
    public Item getItemByCode(String itemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct i from Item i join fetch i.itemType where i.facility.id = :facilityId and i.code = :itemCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemCode", itemCode);
        return (Item) query.uniqueResult();
    }
}
