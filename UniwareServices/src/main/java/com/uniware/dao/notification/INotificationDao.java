/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 29, 2012
 *  @author singla
 */
package com.uniware.dao.notification;

import java.util.List;

import com.uniware.core.entity.Notification;
import com.uniware.services.tasks.notifications.NotificationsProcessor.PendingNotificationStatus;

/**
 * @author singla
 */
public interface INotificationDao {

    List<Notification> getPendingNotifications(int batchSize, int maxId);

    Notification updateNotification(Notification notification);

    Notification getFailedNotification(String groupIdentifier);

    List<Notification> getNotificationsByGroupIdentifier(String groupIdentifier);

    Integer updateFailedNotificationsStatusByIds(List<Integer> notificationIds, String action);

    List<String> getGroupIdentifiers(List<Integer> notificationIds);

    void updateNotificationsStatusByGroupIdentifiers(List<String> groupIdentifiers, String action);

    PendingNotificationStatus getPendingNotificationsStatus();

}
