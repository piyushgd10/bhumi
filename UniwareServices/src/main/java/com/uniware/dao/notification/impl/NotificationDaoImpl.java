/*
 * Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 * UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license
 * terms.
 * 
 * @version 1.0, Apr 29, 2012
 * 
 * @author singla
 */
package com.uniware.dao.notification.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Notification;
import com.uniware.core.entity.Notification.Status;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.notification.INotificationDao;
import com.uniware.services.tasks.notifications.NotificationsProcessor.PendingNotificationStatus;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class NotificationDaoImpl implements INotificationDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Notification> getPendingNotifications(int batchSize, int maxId) {
        Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "select n from Notification n where statusCode = :statusCode and tenant.id = :tenantId and id <= :maxId order by id");
        query.setParameter("statusCode", Status.NEW.name());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("maxId", maxId);
        query.setFirstResult(0);
        query.setMaxResults(batchSize);
        return query.list();
    }

    @Override
    public PendingNotificationStatus getPendingNotificationsStatus() {
        Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "select max(n.id), count(n.id) from Notification n where statusCode = :statusCode and tenant.id = :tenantId order by id");
        query.setParameter("statusCode", Status.NEW.name());
        query.setParameter("tenantId", UserContext.current().getTenantId());

        Object[] pendingNotificationStatus = (Object[]) query.uniqueResult();
        return new PendingNotificationStatus(
                (pendingNotificationStatus[0] != null ? ((Number) pendingNotificationStatus[0]).intValue()
                        : Integer.MAX_VALUE), ((Number) pendingNotificationStatus[1]).intValue());

    }

    @Override
    public Notification updateNotification(Notification notification) {
        return (Notification) sessionFactory.getCurrentSession().merge(notification);
    }

    @Override
    public Notification getFailedNotification(String groupIdentifier) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select n from Notification n where groupIdentifier = :groupIdentifier and statusCode = :statusCode");
        query.setParameter("groupIdentifier", groupIdentifier);
        query.setParameter("statusCode", Status.FAILED.name());
        query.setMaxResults(1);
        return (Notification) query.uniqueResult();
    }

    @Override
    public List<Notification> getNotificationsByGroupIdentifier(String groupIdentifier) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select n from Notification n where tenant.id = :tenantId and groupIdentifier = :groupIdentifier");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("groupIdentifier", groupIdentifier);
        return query.list();
    }

    @Override
    public Integer updateFailedNotificationsStatusByIds(List<Integer> notificationIds, String action) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Notification set statusCode = :action where id in :notificationIds and statusCode = 'FAILED'");
        query.setParameter("action", action);
        query.setParameterList("notificationIds", notificationIds);
        return query.executeUpdate();
    }

    @Override
    public List<String> getGroupIdentifiers(List<Integer> notificationIds) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select groupIdentifier from Notification where tenant.id = :tenantId and id in (:notificationIds)");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("notificationIds", notificationIds);
        return query.list();
    }

    @Override
    public void updateNotificationsStatusByGroupIdentifiers(List<String> groupIdentifiers, String action) {
        Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "update Notification set statusCode = :action where groupIdentifier in :groupIdentifiers and statusCode = 'WAITING'");
        query.setParameter("action", action);
        query.setParameterList("groupIdentifiers", groupIdentifiers);
        query.executeUpdate();
    }

}
