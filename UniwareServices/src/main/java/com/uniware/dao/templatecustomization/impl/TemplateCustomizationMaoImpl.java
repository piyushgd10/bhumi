/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21/05/14
 *  @author amit
 */

package com.uniware.dao.templatecustomization.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.TemplateCustomizationFieldsVO;
import com.uniware.dao.templatecustomization.ITemplateCustomizationMao;

@Repository("templateCustomizationMao")
public class TemplateCustomizationMaoImpl implements ITemplateCustomizationMao {

	@Autowired
	@Qualifier(value = "commonMongo")
	private MongoOperations mongoOperations;

	@Override
	public TemplateCustomizationFieldsVO getTemplateCustomizationFields(PrintTemplateVO.Type templateType) {
		return mongoOperations.findOne(new Query(Criteria.where("templateType").is(templateType)), TemplateCustomizationFieldsVO.class);
	}
}
