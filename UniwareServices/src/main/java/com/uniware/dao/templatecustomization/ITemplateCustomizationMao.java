/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20/05/14
 *  @author amit
 */

package com.uniware.dao.templatecustomization;

import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.TemplateCustomizationFieldsVO;

public interface ITemplateCustomizationMao {

    TemplateCustomizationFieldsVO getTemplateCustomizationFields(PrintTemplateVO.Type templateType);
}
