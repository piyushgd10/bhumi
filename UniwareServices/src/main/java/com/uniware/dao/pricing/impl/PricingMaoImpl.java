/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.dao.pricing.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ChannelPriceUpdateVO;
import com.uniware.dao.pricing.IPricingMao;

@Repository
public class PricingMaoImpl implements IPricingMao {
    
    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void createPriceSnapshot(final ChannelPriceUpdateVO channelPriceUpdateVO) {
        channelPriceUpdateVO.setTenantCode(UserContext.current().getTenant().getCode());
        String who = "";
        if (StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
            who = UserContext.current().getUniwareUserName();
        } else if (StringUtils.isNotBlank(UserContext.current().getApiUsername())) {
            who = UserContext.current().getApiUsername();
        }
        channelPriceUpdateVO.setWho(who);
        channelPriceUpdateVO.setCreated(DateUtils.getCurrentTime());
        mongoOperations.save(channelPriceUpdateVO);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public ChannelPriceUpdateVO getPriceSnapshot(
        final String channelCode, final String channelProductId, final Date when) 
    {
        // Last price snapshot that was successfully created in uniware, on or before the given time.
        return getLastSuccesssfullPriceSnapshot(channelCode, channelProductId, when);
    }
    
    /**
     * Find the last successful price snapshot created for the given channel item type on or before the given
     * time.
     */
    private ChannelPriceUpdateVO getLastSuccesssfullPriceSnapshot(
        final String channelCode, final String channelProductId, final Date when) 
    {
        // Find latest price snapshot
        Query query = new Query();
        query.limit(1);
        query.with(new Sort(Sort.Direction.DESC, "created"));
        // .. for the given product
        Criteria channelProducIdCriteria = Criteria
            .where("tenantCode").is(UserContext.current().getTenant().getCode())
            .and("channelCode").is(channelCode)
            .and("channelProductId").is(channelProductId);
        // .. that is a successfully created on or before the given date
        Criteria successfulPriceEditsCriteria = Criteria
            .where("statusCode").is(ChannelPriceUpdateVO.Status.SUCCESS)
            .and("created").lte(when);
        query.addCriteria(channelProducIdCriteria).addCriteria(successfulPriceEditsCriteria);
        return mongoOperations.findOne(query, ChannelPriceUpdateVO.class);
    }
}
