/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.dao.pricing;

import java.util.Date;

import com.uniware.core.vo.ChannelPriceUpdateVO;

public interface IPricingMao {

    void createPriceSnapshot(final ChannelPriceUpdateVO channelPriceUpdateVO);

    ChannelPriceUpdateVO getPriceSnapshot(final String channelCode, final String channelProductId, final Date when);
}
