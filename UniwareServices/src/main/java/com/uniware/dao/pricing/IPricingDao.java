/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.dao.pricing;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.entity.ChannelItemType.Status;
import com.uniware.core.entity.ChannelItemTypePrice;

import java.util.Date;
import java.util.List;

public interface IPricingDao {
    
    /**
     * Dao that manages seller prices across various channels.
     * <ul>
     * The responsibilities include:
     * <li>Updating prices for a given channel item type from uniware to the channel in database.
     * <li>Fetching list or an item from database.
     *
     * </ul>
     */
    
    
    /**
     * Get entity for channel item type price conditionally acquiring pessimistic write lock
     */
    ChannelItemTypePrice getChannelItemTypePriceByChannelAndChannelProductId(int channelId, String channelProductId);

    /**
     * Get dirty channel item type prices with start and batch size updated before lastUpdated
     */
    List<ChannelItemTypePrice> getDirtyChannelItemTypePrices(int channelId, int start, int batchSize, Date lastUpdated, Status statusCode);

    /**
     * Get dirty channel item type price from updated timestamp
     */
    long getDirtyChannelItemTypePriceCount(int channelId, Date lastUpdated, Status statusCode);

    boolean updateChannelItemTypePriceIfFresh(final ChannelItemTypePrice channelItemTypePrice, final Date maxVersion);

    ChannelItemTypePrice update(ChannelItemTypePrice channelItemTypePrice);

    long getChannelItemTypePriceCount(int channelId, String lastPriceUpdateStatus);

    long getChannelItemTypePriceCount(int channelId, String lastPriceUpdateStatus, DateRange lastPriceUpdateDateRange);

}
