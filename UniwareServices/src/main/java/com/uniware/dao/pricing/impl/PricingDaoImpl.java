/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.dao.pricing.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.entity.ChannelItemType;
import com.uniware.core.entity.ChannelItemTypePrice;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.pricing.IPricingDao;

@Repository
public class PricingDaoImpl implements IPricingDao {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ChannelItemTypePrice getChannelItemTypePriceByChannelAndChannelProductId(int channelId, String channelProductId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
            "select citp from ChannelItemTypePrice citp join fetch citp.channelItemType cit left join cit.itemType it where citp.tenant.id = :tenantId and citp.channelProductId = :channelProductId and citp.channel.id = :channelId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelProductId", channelProductId);
        query.setParameter("channelId", channelId);
        return (ChannelItemTypePrice) query.uniqueResult();
    }

    @Override
    public long getDirtyChannelItemTypePriceCount(int channelId, Date lastUpdated, ChannelItemType.Status statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
            "select count(citp.id) from ChannelItemTypePrice citp where citp.channelItemType.statusCode = :statusCode and citp.tenant.id = :tenantId and citp.channel.id = :channelId and citp.disabledDueToPushErrors = :disabledDueToPushErrors and citp.dirty = :dirty and citp.updated <= :lastUpdated and citp.disabled = :disabled");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabled",false);
        query.setParameter("disabledDueToPushErrors", false);
        query.setParameter("lastUpdated", lastUpdated);
        return (Long) query.uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<ChannelItemTypePrice> getDirtyChannelItemTypePrices(
        int channelId, int start, int batchSize, Date lastUpdated, ChannelItemType.Status statusCode) 
    {
        Query query = sessionFactory.getCurrentSession().createQuery(
            "select citp from ChannelItemTypePrice citp join fetch citp.channelItemType cit left join cit.itemType it where citp.channelItemType.statusCode = :statusCode and citp.tenant.id = :tenantId  and citp.channel.id = :channelId and citp.disabledDueToPushErrors = :disabledDueToPushErrors and citp.disabled = :disabled and citp.dirty = :dirty and citp.updated <= :lastUpdated");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("statusCode", statusCode);
        query.setParameter("channelId", channelId);
        query.setParameter("dirty", true);
        query.setParameter("disabledDueToPushErrors", false);
        query.setParameter("disabled",false);
        query.setParameter("lastUpdated", lastUpdated);
        query.setMaxResults(batchSize);
        query.setFirstResult(start);
        return query.list();
    }
    
    @Override
    public long getChannelItemTypePriceCount(int channelId, String lastPushStatus) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(citp) from ChannelItemTypePrice citp where citp.tenant.id = :tenantId and citp.channel.id = :channelId and citp.lastPushStatus = :lastPushStatus");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("lastPushStatus", lastPushStatus);
        return ((Long) query.uniqueResult());
    }
    

    @Override
    public long getChannelItemTypePriceCount(int channelId, String lastPushStatus, DateRange lastPriceUpdateDateRange) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(citp) from ChannelItemTypePrice citp where citp.tenant.id = :tenantId and citp.channel.id = :channelId and citp.lastPushStatus = :lastPushStatus and citp.lastUpdateAttemptAt between :start and :end");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        query.setParameter("lastPushStatus", lastPushStatus);
        query.setParameter("start", lastPriceUpdateDateRange.getStart());
        query.setParameter("end", lastPriceUpdateDateRange.getEnd());
        return ((Long) query.uniqueResult());
    }

    @Override
    public boolean updateChannelItemTypePriceIfFresh(
        final ChannelItemTypePrice channelItemTypePrice, final Date maxVersion) {
        Query query = sessionFactory.getCurrentSession().createQuery(
            "update ChannelItemTypePrice citp set citp.sellingPrice = :sellingPrice, citp.msp = :msp, citp.mrp = :mrp, citp.transferPrice = :transferPrice, citp.competitivePrice = :competitivePrice, "
            + "citp.channelSellingPrice = :channelSellingPrice, citp.channelMsp = :channelMsp, citp.channelMrp = :channelMrp, citp.channelTransferPrice = :channelTransferPrice, citp.channelCompetitivePrice = :channelCompetitivePrice, citp.dirty = :dirty, citp.pushFailedCount = :pushFailedCount, citp.disabledDueToPushErrors = :disabledDueToPushErrors, citp.lastPushStatus = :lastPushStatus, citp.lastPushMessage = :lastPushMessage, citp.lastUpdateAttemptAt = :lastUpdateAttemptAt where citp.tenant.id = :tenantId and citp.channelProductId = :channelProductId and citp.channel.id = :channelId and citp.updated <= :updated");
        query.setParameter("sellingPrice", channelItemTypePrice.getSellingPrice());
        query.setParameter("msp", channelItemTypePrice.getMsp());
        query.setParameter("mrp", channelItemTypePrice.getMrp());
        query.setParameter("transferPrice", channelItemTypePrice.getTransferPrice());
        query.setParameter("competitivePrice", channelItemTypePrice.getCompetitivePrice());
        query.setParameter("channelSellingPrice", channelItemTypePrice.getChannelSellingPrice());
        query.setParameter("channelMsp", channelItemTypePrice.getChannelMsp());
        query.setParameter("channelMrp", channelItemTypePrice.getChannelMrp());
        query.setParameter("channelTransferPrice", channelItemTypePrice.getChannelTransferPrice());
        query.setParameter("channelCompetitivePrice", channelItemTypePrice.getChannelCompetitivePrice());
        query.setParameter("dirty", channelItemTypePrice.isDirty());
        query.setParameter("pushFailedCount", channelItemTypePrice.getPushFailedCount());
        query.setParameter("disabledDueToPushErrors", channelItemTypePrice.isDisabledDueToPushErrors());
        query.setParameter("lastPushStatus", channelItemTypePrice.getLastPushStatus());
        query.setParameter("lastUpdateAttemptAt", channelItemTypePrice.getLastUpdateAttemptAt());
        query.setParameter("lastPushMessage", channelItemTypePrice.getLastPushMessage());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelProductId", channelItemTypePrice.getChannelProductId());
        query.setParameter("channelId", channelItemTypePrice.getChannel().getId());
        query.setParameter("updated", maxVersion);
        return query.executeUpdate() > 0;
    }

    @Override
    public ChannelItemTypePrice update(ChannelItemTypePrice channelItemTypePrice) {
        channelItemTypePrice.setTenant(UserContext.current().getTenant());
        return (ChannelItemTypePrice) sessionFactory.getCurrentSession().merge(channelItemTypePrice);
    }
}
