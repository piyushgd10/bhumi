/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author vibhu
 */
package com.uniware.dao.material;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.entity.InboundGatePass;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.OutboundGatePassItem;
import com.uniware.core.entity.OutboundGatePassStatus;

import java.util.List;

public interface IMaterialDao {

    OutboundGatePass addGatePass(OutboundGatePass gatePass);

    OutboundGatePass getGatePassByCode(String gassPassCode, boolean lock);

    OutboundGatePass getGatePassByCode(String gatePassCode);

    OutboundGatePassItem addGatePassItem(OutboundGatePassItem gatePassItem);

    List<OutboundGatePass> searchGatePass(SearchGatePassRequest request);

    Long getSearchGatePassCount(SearchGatePassRequest request);

    OutboundGatePass updateGatePass(OutboundGatePass gatePass);

    InboundGatePass addInboundGatePass(InboundGatePass gatePass);

    InboundGatePass getInboundGatePassByCode(String code);

    InboundGatePass updateInboundGatePass(InboundGatePass inboundGatePass);

    List<OutboundGatePass> getOutboundGatepassByRange(DateRange dateRange, String vendorCode);

    List<OutboundGatePassStatus> getOutboundGatePassStatuses();

    List<OutboundGatePass> getOutboundGatepassAcrossFacilityByItemId(Integer itemId);

    List<OutboundGatePass> getOutboundGatepassByItemId(Integer itemId);

    OutboundGatePass getGatePassById(int outboundGatepassId);

    void removeGatePassItem(OutboundGatePassItem gatePassItem);

    OutboundGatePassItem updateGatePassItem(OutboundGatePassItem gatePassItem);

    OutboundGatePassItem getOutboundGatepassItemByGatepassAndItemCode(String gatepassCode, String itemCode);

    OutboundGatePass getGatePassByInvoiceId(Integer invoiceId);

    List<OutboundGatePassItem> getOutboundGatepassItemByGatepass(Integer gatePassId);
}
