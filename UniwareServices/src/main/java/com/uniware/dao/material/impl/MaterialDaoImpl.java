/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Jun-2012
 *  @author vibhu
 */
package com.uniware.dao.material.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.material.SearchGatePassRequest;
import com.uniware.core.entity.InboundGatePass;
import com.uniware.core.entity.OutboundGatePass;
import com.uniware.core.entity.OutboundGatePassItem;
import com.uniware.core.entity.OutboundGatePassStatus;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.material.IMaterialDao;
import com.uniware.services.common.ISequenceGenerator;

@Repository
public class MaterialDaoImpl implements IMaterialDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    @Override
    public OutboundGatePass addGatePass(OutboundGatePass gatePass) {
        gatePass.setFacility(UserContext.current().getFacility());
        if(StringUtils.isBlank(gatePass.getCode())) {
            gatePass.setCode(sequenceGenerator.generateNext(Name.OUTBOUND_GATE_PASS));
        }
        sessionFactory.getCurrentSession().persist(gatePass);
        return gatePass;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.material.IMaterialDao#updateGatePass(com.uniware.core.entity.OutboundGatePass)
     */
    @Override
    public OutboundGatePass updateGatePass(OutboundGatePass gatePass) {
        return (OutboundGatePass) sessionFactory.getCurrentSession().merge(gatePass);
    }

    @Override
    public OutboundGatePass getGatePassByCode(String gatePassCode, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select gp from OutboundGatePass gp where (gp.facility.id = :facilityId or (gp.type = :typeStockTrasfer and gp.toParty.id = :facilityId)) and gp.code = :gatePassCode");
        query.setParameter("gatePassCode", gatePassCode);
        query.setParameter("typeStockTrasfer", OutboundGatePass.Type.STOCK_TRANSFER.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("gp", LockMode.PESSIMISTIC_WRITE);
        }
        return (OutboundGatePass) query.uniqueResult();
    }

    @Override
    public OutboundGatePass getGatePassByCode(String gatePassCode) {
        return getGatePassByCode(gatePassCode, false);
    }

    @Override
    public OutboundGatePassItem addGatePassItem(OutboundGatePassItem gatePassItem) {
        sessionFactory.getCurrentSession().persist(gatePassItem);
        return gatePassItem;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OutboundGatePass> searchGatePass(SearchGatePassRequest request) {
        Criteria criteria = getCriteriaForSearchGatePass(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    private Criteria getCriteriaForSearchGatePass(SearchGatePassRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OutboundGatePass.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (StringUtils.isNotBlank(request.getGatePassCode())) {
            criteria.add(Restrictions.eq("code", request.getGatePassCode()));
        }
        if (StringUtils.isNotBlank(request.getStatusCode())) {
            criteria.add(Restrictions.eq("statusCode", request.getStatusCode()));
        }
        if (request.getFromDate() != null) {
            criteria.add(Restrictions.ge("created", request.getFromDate()));
        }
        if (request.getToDate() != null) {
            criteria.add(Restrictions.le("created", request.getToDate()));
        }
        String username = request.getUsernameContains();
        if (StringUtils.isNotBlank(username)) {
            criteria.createCriteria("user").add(Restrictions.like("username", "%" + username + "%"));
        }

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            criteria.createCriteria("toParty").add(Restrictions.idEq(vendorId));
        }
        if (StringUtils.isNotBlank(request.getToParty())) {
            String toParty = request.getToParty().trim();
            criteria.createCriteria("toParty").add(Restrictions.like("name", "%" + toParty + "%"));
        }

        return criteria;
    }

    @Override
    public Long getSearchGatePassCount(SearchGatePassRequest request) {
        Criteria criteria = getCriteriaForSearchGatePass(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    @Override
    public InboundGatePass addInboundGatePass(InboundGatePass gatePass) {
        gatePass.setFacility(UserContext.current().getFacility());
        gatePass.setCode(sequenceGenerator.generateNext(Name.INBOUND_GATE_PASS));
        sessionFactory.getCurrentSession().persist(gatePass);
        return gatePass;
    }

    @Override
    public InboundGatePass getInboundGatePassByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from InboundGatePass where facility.id = :facilityId and code = :code");
        query.setParameter("code", code);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (InboundGatePass) query.uniqueResult();
    }

    @Override
    public InboundGatePass updateInboundGatePass(InboundGatePass inboundGatePass) {
        return (InboundGatePass) sessionFactory.getCurrentSession().merge(inboundGatePass);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OutboundGatePass> getOutboundGatepassByRange(DateRange dateRange, String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ogp from OutboundGatePass ogp join fetch ogp.outboundGatePassItems ogpi join fetch ogpi.item i join fetch ogpi.itemType it join fetch ogp.toParty p join fetch ogp.user u left join fetch i.inflowReceiptItem iri left join iri.inflowReceipt ir left join fetch iri.purchaseOrderItem poi left join fetch poi.purchaseOrder po where ogp.facility.id = :facilityId and ogp.created > :startDate and ogp.created < :endDate and p.code = :vendorCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("startDate", dateRange.getStart());
        query.setParameter("endDate", dateRange.getEnd());
        query.setParameter("vendorCode", vendorCode);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OutboundGatePassStatus> getOutboundGatePassStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from OutboundGatePassStatus").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OutboundGatePass> getOutboundGatepassAcrossFacilityByItemId(Integer itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ogp from OutboundGatePassItem ogpi join ogpi.outboundGatePass ogp join ogp.facility f where ogpi.item.id = :itemId and f.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("itemId", itemId);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OutboundGatePass> getOutboundGatepassByItemId(Integer itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ogp from OutboundGatePassItem ogpi join ogpi.outboundGatePass ogp where ogpi.item.id = :itemId");
        query.setParameter("itemId", itemId);
        return query.list();
    }

    @Override
    public OutboundGatePass getGatePassById(int outboundGatepassId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select gp from OutboundGatePass gp where gp.id = :outboundGatepassId");
        query.setParameter("outboundGatepassId", outboundGatepassId);
        return (OutboundGatePass) query.uniqueResult();
    }

    @Override
    public void removeGatePassItem(OutboundGatePassItem gatePassItem) {
        sessionFactory.getCurrentSession().delete(gatePassItem);
    }

    @Override
    public OutboundGatePassItem updateGatePassItem(OutboundGatePassItem gatePassItem) {
        gatePassItem = (OutboundGatePassItem) sessionFactory.getCurrentSession().merge(gatePassItem);
        sessionFactory.getCurrentSession().flush();
        return gatePassItem;
    }
    
    @Override
    public OutboundGatePassItem getOutboundGatepassItemByGatepassAndItemCode(String gatepassCode, String itemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ogpi from OutboundGatePassItem ogpi join ogpi.outboundGatePass ogp join ogpi.item i where ogp.code = :gatepassCode and i.code = :itemCode and ogp.facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("gatepassCode", gatepassCode);
        query.setParameter("itemCode", itemCode);
        return (OutboundGatePassItem) query.uniqueResult();
    }

    @Override public OutboundGatePass getGatePassByInvoiceId(Integer invoiceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from OutboundGatePass where facility.id = :facilityId and invoice.id= :invoiceId");
        query.setParameter("invoiceId", invoiceId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (OutboundGatePass) query.uniqueResult();
    }

    @Override
    public List<OutboundGatePassItem> getOutboundGatepassItemByGatepass(Integer gatePassId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select ogpi from OutboundGatePassItem ogpi left join fetch ogpi.item i where ogpi.outboundGatePass.id = :gatePassId");
        query.setParameter("gatePassId", gatePassId);
        return query.list();
    }
}
