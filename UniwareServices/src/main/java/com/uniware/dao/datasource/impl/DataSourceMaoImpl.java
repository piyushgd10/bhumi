/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.uniware.dao.datasource.impl;

import com.uniware.core.vo.DataSourceConfigurationVO;
import com.uniware.dao.datasource.IDataSourceMao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class DataSourceMaoImpl implements IDataSourceMao {

    @Autowired
    @Qualifier(value = "configMongo")
    private MongoOperations mongoOperations;

    @Override public List<DataSourceConfigurationVO> getDataSourcesByServerNames(List<String> serverNames, DataSourceConfigurationVO.Type type) {
        return mongoOperations.find(new Query(Criteria.where("type").is(type).and("serverName").in(serverNames)), DataSourceConfigurationVO.class);
    }

    @Override
    public DataSourceConfigurationVO getDataSourceByServerName(String serverName, DataSourceConfigurationVO.Type type) {
        return mongoOperations.findOne(new Query(Criteria.where("type").is(type).and("serverName").is(serverName)), DataSourceConfigurationVO.class);
    }

    @Override public void save(DataSourceConfigurationVO dataSourceConfiguration) {
        mongoOperations.save(dataSourceConfiguration);
    }

    public static void main(String[] args) {
        String a = "surface";
        String b = "surface";
        String c = "air";
        String d = "AIR";
        String e = "";
        System.out.println(Math.abs(a.hashCode()));
        System.out.println(b.hashCode());
        System.out.println(c.hashCode());
        System.out.println(d.hashCode());
        System.out.println(e.hashCode());

    }
}
