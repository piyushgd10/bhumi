package com.uniware.dao.datasource;

import com.uniware.core.vo.DataSourceConfigurationVO;

import java.util.List;

/**
 * Created by sunny on 17/03/15.
 */
public interface IDataSourceMao {

    List<DataSourceConfigurationVO> getDataSourcesByServerNames(List<String> serverNames, DataSourceConfigurationVO.Type type);

    DataSourceConfigurationVO getDataSourceByServerName(String serverName, DataSourceConfigurationVO.Type type);

    void save(DataSourceConfigurationVO dataSourceConfiguration);
}
