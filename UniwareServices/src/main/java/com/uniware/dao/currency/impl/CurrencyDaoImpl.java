/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2013
 *  @author karunsingla
 */
package com.uniware.dao.currency.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Currency;
import com.uniware.dao.currency.ICurrencyDao;

@Repository
@SuppressWarnings("unchecked")
public class CurrencyDaoImpl implements ICurrencyDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Currency> getAllCurrencies() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Currency");
        return query.list();
    }

}
