/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2013
 *  @author karunsingla
 */
package com.uniware.dao.currency.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.vo.CurrencyConversionRateVO;
import com.uniware.dao.currency.ICurrencyMao;

@Repository
public class CurrencyMaoImpl implements ICurrencyMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public CurrencyConversionRateVO getCurrencyConversionRate(String fromCurrencyCode, String toCurrencyCode) {
        return mongoOperations.findOne(new Query(Criteria.where("from").is(fromCurrencyCode).and("to").is(toCurrencyCode).and("date").is(DateUtils.getCurrentDate())),
                CurrencyConversionRateVO.class);
    }
}
