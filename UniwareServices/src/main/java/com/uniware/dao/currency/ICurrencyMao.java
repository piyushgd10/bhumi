/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2013
 *  @author karunsingla
 */
package com.uniware.dao.currency;

import com.unifier.core.vo.CurrencyConversionRateVO;

public interface ICurrencyMao {

    CurrencyConversionRateVO getCurrencyConversionRate(String fromCurrencyCode, String toCurrencyCode);
}
