/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Aug-2013
 *  @author sunny
 */
package com.uniware.dao.saleorder.impl;

import java.util.List;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.vo.SaleOrderFlowSummaryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.FacilityAllocationVO;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.dao.saleorder.ISaleOrderMao;

@Repository
public class SaleOrderMaoImpl implements ISaleOrderMao {

	@Autowired
	@Qualifier(value = "tenantSpecificMongo")
	private MongoOperations mongoOperations;

	@Override
	public void save(SaleOrderVO saleOrderVO) {
		saleOrderVO.setTenantCode(UserContext.current().getTenant().getCode());
		mongoOperations.remove(new Query(Criteria.where("code").is(saleOrderVO.getCode()).and("tenantCode").is(UserContext.current().getTenant().getCode())), SaleOrderVO.class);
		mongoOperations.save(saleOrderVO);
	}

	@Override
	public void remove(String saleOrderCode) {
		mongoOperations.remove(new Query(Criteria.where("code").is(saleOrderCode).and("tenantCode").is(UserContext.current().getTenant().getCode())), SaleOrderVO.class);
	}

	@Override
	public SaleOrderVO getSaleOrderVOByCode(String saleOrderCode) {
		return mongoOperations.findOne(new Query(Criteria.where("code").is(saleOrderCode).and("tenantCode").is(UserContext.current().getTenant().getCode())), SaleOrderVO.class);
    }

    @Override
    public List<SaleOrderVO> getAllSaleOrders() {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), SaleOrderVO.class);
    }

    @Override
    public void removeAllSaleOrders() {
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), SaleOrderVO.class);
    }

    @Override
    public List<SaleOrderVO> searchFailedOrPartialOrders(String text) {
        //UNT-766 - search in failed orders for both code and displayOrderCode
        return mongoOperations.find(
                new Query(new Criteria().orOperator(Criteria.where("request.saleOrder.displayOrderCode").is(text), Criteria.where("code").is(text)).and("tenantCode").is(
                        UserContext.current().getTenant().getCode())), SaleOrderVO.class);

    }

    @Override
    public List<SaleOrderVO> getFailedOrdersByChannelProductId(String channelProductId) {
        return mongoOperations.find(
                new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("response.errors").elemMatch(
                        Criteria.where("errorParams.channelItemType.channelProductId").is(channelProductId))), SaleOrderVO.class);
    }

    @Override
    public List<SaleOrderVO> getFailedOrdersForSaleOrderCodes(List<String> saleOrderCodes) {
        return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("fixable").is(true).and("code").in(saleOrderCodes)),
                SaleOrderVO.class);
    }
    
    @Override
    public void save(FacilityAllocationVO facilityAllocationVO) {
        facilityAllocationVO.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.remove(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("saleOrderItemCode").is(facilityAllocationVO.getSaleOrderItemCode()).and("saleOrderCode").is(facilityAllocationVO.getSaleOrderCode())), FacilityAllocationVO.class);
        mongoOperations.save(facilityAllocationVO);
    }
    
    @Override
    public FacilityAllocationVO getFacilityAllocation(String saleOrderItemCode,String saleOrderCode) {
        return mongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("saleOrderItemCode").is(saleOrderItemCode).and("saleOrderCode").is(saleOrderCode)), FacilityAllocationVO.class);
    }

    @Override
    public SaleOrderFlowSummaryVO createSaleOrderFlowSummary(SaleOrderFlowSummaryVO summary) {
        summary.setTenantCode(UserContext.current().getTenant().getCode());
        summary.setFacilityCode( CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(UserContext.current().getFacilityId()).getCode());
        mongoOperations.save(summary);
        return summary;
    }
}
