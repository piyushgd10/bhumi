/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.dao.saleorder;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderItemRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItemAlternate;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.SaleOrderDTO;

/**
 * @author singla
 */
public interface ISaleOrderDao {

    /**
     * @param shippingAddress
     */
    AddressDetail addAddressDetail(AddressDetail shippingAddress);

    /**
     * @param saleOrder
     * @return
     */
    SaleOrder addSaleOrder(SaleOrder saleOrder);

    /**
     * @param statusCode
     * @return
     */
    List<SaleOrder> getSaleOrdersByStatus(String statusCode);

    /**
     * @param saleOrder
     * @return
     */
    SaleOrder updateSaleOrder(SaleOrder saleOrder);

    /**
     * @param saleOrderItem
     * @return
     */
    SaleOrderItem updateSaleOrderItem(SaleOrderItem saleOrderItem);

    /**
     * @param saleOrderItemId
     * @return
     */
    SaleOrderItem getSaleOrderItemById(int saleOrderItemId, boolean refresh);

    /**
     * @param saleOrderItemId
     * @return
     */
    SaleOrderItem getSaleOrderItemById(int saleOrderItemId);

    List<SaleOrderItem> getSaleOrderItemByCodeList(String saleOrderCode, List<String> saleOrderItemCodeList);

    /**
     * @param itemId
     * @return
     */
    SaleOrderItem getSaleOrderItemByAllocatedItem(int itemId);

    /**
     * @param shippingPackageCode
     * @param item
     * @return
     */
    SaleOrderItem getSaleOrderItemForAllocation(String shippingPackageCode, Item item);

    List<SaleOrder> searchSaleOrders(SearchSaleOrderRequest request);

    SaleOrder getSaleOrderByCode(String code);

    /**
     * @param saleOrderId
     * @return
     */
    SaleOrder getSaleOrderById(int saleOrderId);

    Long getItemCountNotDispatched();

    Long getItemCountDispatchedToday();

    /**
     * @return
     */
    List<SaleOrderDTO> getSaleOrdersForProcessing();

    /**
     * @param itemTypeId
     * @param statusCode
     * @return
     */
    List<SaleOrderItem> getSaleOrderItemByItemTypeByStatus(int itemTypeId, String statusCode);

    Long getSaleOrderCount(SearchSaleOrderRequest request);

    /**
     * @param statusCode
     * @return
     */
    List<SaleOrderItem> getSaleOrderItemsByStatus(String statusCode);

    List<SaleOrderItem> searchSaleOrderItems(SearchSaleOrderItemRequest request);

    Long getSearchSaleOrderItemRequestCount(SearchSaleOrderItemRequest request);

    /**
     * @param picklistCode
     * @param item
     * @return
     */
    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item);

    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, boolean fetchCancelled);

    List<SaleOrderItem> getSaleOrderItemsByItemId(Integer itemId);

    AddressDetail updateAddressDetail(AddressDetail shippingAddress);

    AddressDetail getAddressDetail(Integer addressId);

    SaleOrderItemAlternate addSaleOrderItemAlternate(SaleOrderItemAlternate saleOrderItemAlternate);

    SaleOrderItemAlternate getSaleOrderItemAlternateById(Integer alternateId);

    SaleOrderItem addSaleOrderItem(SaleOrderItem saleOrderItem);

    List<Integer> getSaleOrdersForFacilityAllocation();

    List<String> getSaleOrderCodesForFacilityAllocation();

    List<SaleOrder> getSaleOrders(List<Integer> ids);

    SaleOrderItem getSaleOrderItemByCode(String saleOrderCode, String saleOrderItemCode);

    List<SaleOrderStatus> getOrderStatuses();

    List<SaleOrderItemStatus> getOrderItemStatuses();

    List<SaleOrderItem> getPendingSaleOrderItemsByFacilityId(Integer facilityId);

    List<SaleOrderItem> getSaleOrderItemsByChannelOrderAndItemCode(String channelSaleOrderCode, String channelSaleOrderItemCode, Integer channelId);

    List<SaleOrder> searchSaleOrder(String text);

    SaleOrder getSaleOrderByAdditionalInfo(String additionalInfo);

    List<SaleOrderItem> getSaleOrderItemsByItemIdForTenant(Integer itemId);

    List<SaleOrder> getReconciliationPendingOrders(int start, int pageSize);

    List<SaleOrder> getSaleOrderByDisplayOrderCode(String displayOrderCode, Integer channelId);

    SaleOrderItem getSaleOrderItemByItemCode(String saleOrderCode, String itemCode);

    List<SaleOrder> getSaleOrdersForStatusSync(List<String> statusCodes, int numberOfDays, Integer channelId, int maxId, int pageSize);

    Long getSaleOrderCountForStatusSync(List<String> statusCodes, int numOfDays, Integer channelId);

    Long getSaleOrderItemCount(String channelId, List<String> statusCodes);

    Long getUnprocessedSaleOrderItemCount(int channelId, String channelProductId);

    Map<String, Long> getUnprocessedSaleOrderItemsCount(int channelId);

    void deleteSaleOrder(SaleOrder saleOrder);

    long getOrderCountCreatedTodayForChannel(Integer channelId);

    SaleOrder getSaleOrderByCode(String code, Integer channelId);

    List<SaleOrderDTO> getSaleOrdersForProcessing(Integer itemTypeId);

    List<SaleOrder> searchSaleOrderByDisplayOrderCode(String keyword);

    List<SaleOrder> searchSaleOrderByOrderCode(String keyword);

    SaleOrder getSaleOrderForUpdate(String saleOrderCode);

    SaleOrder getSaleOrderForUpdate(String code, boolean markViewDirty);

    SaleOrder getSaleOrderForUpdate(int saleOrderId);

    void markSaleOrderDirty(SaleOrder saleOrder);

    List<SaleOrder> getDirtySaleOrders(int start, int pageSize);

    List<SaleOrder> getPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses, Integer facilityId);

    List<SaleOrder> getPaginatedSaleOrdersByStatus(List<String> pendingOrderStatuses, PageRequest request);

    List<SaleOrder> getSaleOrdersByStatus(Integer openSinceInHours, List<String> saleOrderStatusCodes);

    List<String> getBundleItemsByCode(String saleOrderCode, String saleOrderItemCode);

    SaleOrderItem getSoftAllocatedPicklistItemForDeallocationInPicklist(String picklistCode, Item item);

    SaleOrderItem getCancelledSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item);

    Long getSaleOrderItemsCountByChannelProductIdAndStatus(String channelProductId, List<String> statusCodes);

    SaleOrderItem getSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item);

    List<SaleOrderItem> getFulfillableSaleOrderItemsForItemTypeInventory(int itemTypeInventoryId, int numberOfItems);

    SaleOrderItem getSaleOrderItemForDeallocation(Item item);

    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode);

    SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode, boolean fetchCancelled);

    void updateSaleOrderItemStatusForPicklistCreation(Set<Integer> saleOrderItemIds, String statusCode);
}
