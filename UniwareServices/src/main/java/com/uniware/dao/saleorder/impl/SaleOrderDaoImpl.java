/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 15, 2011
 *  @author singla
 */
package com.uniware.dao.saleorder.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.Interval;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.saleorder.PageRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderItemRequest;
import com.uniware.core.api.saleorder.SearchSaleOrderRequest;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.PicklistItem;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.SaleOrderItemAlternate;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.UserContext;
import com.uniware.core.utils.ViewContext;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.configuration.SystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration.InventoryAccuracyLevel;
import com.uniware.services.saleorder.impl.ViewDirtyPostCommitHandler;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor.SaleOrderDTO;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
public class SaleOrderDaoImpl implements ISaleOrderDao {

    @Autowired
    private SessionFactory             sessionFactory;

    @Autowired
    private ViewDirtyPostCommitHandler viewDirtyPostCommitHandler;

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#addAddressDetail(com.uniware.core.entity.AddressDetail)
     */
    @Override
    public AddressDetail addAddressDetail(AddressDetail addressDetail) {
        addressDetail.setTenant(UserContext.current().getTenant());
        return (AddressDetail) sessionFactory.getCurrentSession().merge(addressDetail);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#addSaleOrder(com.uniware.core.entity.SaleOrder)
     */
    @Override
    public SaleOrder addSaleOrder(SaleOrder saleOrder) {
        saleOrder.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(saleOrder);
        return saleOrder;
    }

    @Override
    public SaleOrderItem updateSaleOrderItem(SaleOrderItem saleOrderItem) {
        markSaleOrderDirty(saleOrderItem.getSaleOrder());
        return (SaleOrderItem) sessionFactory.getCurrentSession().merge(saleOrderItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrdersByStatus(java.lang.String)
     */
    @Override
    public List<SaleOrder> getSaleOrdersByStatus(String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrder so join fetch so.saleOrderItems soi join fetch soi.shippingAddress where so.tenant.id = :tenantId and so.statusCode = :statusCode");
        query.setParameter("statusCode", statusCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#updateSaleOrder(com.uniware.core.entity.SaleOrder)
     */
    @Override
    public SaleOrder updateSaleOrder(SaleOrder saleOrder) {
        markSaleOrderDirty(saleOrder);
        return (SaleOrder) sessionFactory.getCurrentSession().merge(saleOrder);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemById(int)
     */
    @Override
    public SaleOrderItem getSaleOrderItemById(int saleOrderItemId, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi join fetch soi.saleOrder so left join fetch soi.itemTypeInventory where soi.id = :saleOrderItemId");
        query.setParameter("saleOrderItemId", saleOrderItemId);
        SaleOrderItem saleOrderItem = (SaleOrderItem) query.uniqueResult();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(saleOrderItem);
        }
        return saleOrderItem;
    }

    @Override
    public SaleOrderItem getSaleOrderItemByCode(String saleOrderCode, String saleOrderItemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.saleOrder.code = :saleOrderCode and soi.code = :saleOrderItemCode and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrderItem) query.uniqueResult();
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemByCodeList(String saleOrderCode, List<String> saleOrderItemCodeList) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.saleOrder.code = :saleOrderCode and soi.code in (:saleOrderItemCodeList) and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameterList("saleOrderItemCodeList", saleOrderItemCodeList);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemByAllocatedItem(int)
     */
    @Override
    public SaleOrderItem getSaleOrderItemByAllocatedItem(int itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select soi from SaleOrderItem soi where soi.item.id = :itemId");
        query.setParameter("itemId", itemId);
        return (SaleOrderItem) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemById(int)
     */
    @Override
    public SaleOrderItem getSaleOrderItemById(int saleOrderItemId) {
        return getSaleOrderItemById(saleOrderItemId, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemForAllocation(int, int)
     */
    @Override
    public SaleOrderItem getSaleOrderItemForAllocation(String shippingPackageCode, Item item) {
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        Query query;
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.ITEM_TYPE) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from SaleOrderItem soi where soi.shippingPackage.code = :shippingPackageCode and soi.shippingPackage.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null");
        } else {
            if (item.getShelf() == null) {
                throw new IllegalStateException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from SaleOrderItem soi where soi.shippingPackage.code = :shippingPackageCode and soi.shippingPackage.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and soi.itemTypeInventory.shelf.id = :shelfId");
            query.setParameter("shelfId", item.getShelf().getId());
        }
        query.setParameter("itemTypeId", item.getItemType().getId());
        query.setParameter("shippingPackageCode", shippingPackageCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        SaleOrderItem soi = (SaleOrderItem) query.uniqueResult();
        if (soi != null) {
            markSaleOrderDirty(soi.getSaleOrder());
        }
        return soi;
    }

    @Override
    public List<SaleOrder> searchSaleOrders(SearchSaleOrderRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        //criteria.setProjection(Projections.distinct(Projections.id()));
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public Long getSaleOrderCount(SearchSaleOrderRequest request) {
        Criteria criteria = getCriteriaFromRequest(request);
        criteria.setProjection(Projections.countDistinct("so.id"));
        return (Long) criteria.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getOrderByCode(java.lang.String)
     */
    @Override
    public SaleOrder getSaleOrderByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct so from SaleOrder so where so.tenant.id = :tenantId and so.code = :code");
        query.setParameter("code", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrder) query.uniqueResult();
    }

    @Override
    public SaleOrder getSaleOrderForUpdate(String code) {
        return getSaleOrderForUpdate(code, true);
    }

    @Override
    public SaleOrder getSaleOrderForUpdate(String code, boolean markViewDirty) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct so from SaleOrder so where so.tenant.id = :tenantId and so.code = :code");
        query.setParameter("code", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        SaleOrder so = (SaleOrder) query.uniqueResult();
        if (markViewDirty && so != null) {
            markSaleOrderDirty(so);
        }
        return so;
    }

    @Override
    public SaleOrder getSaleOrderForUpdate(int saleOrderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select saleOrder from SaleOrder saleOrder join fetch saleOrder.saleOrderItems soi where saleOrder.tenant.id = :tenantId and saleOrder.id = :saleOrderId");
        query.setParameter("saleOrderId", saleOrderId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setMaxResults(1);
        SaleOrder so = (SaleOrder) query.uniqueResult();
        markSaleOrderDirty(so);
        return so;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderById(int)
     */
    @Override
    public SaleOrder getSaleOrderById(int saleOrderId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select so from SaleOrder so join fetch so.saleOrderItems soi join fetch so.billingAddress where so.tenant.id = :tenantId and so.id = :saleOrderId");
        query.setParameter("saleOrderId", saleOrderId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrder) query.uniqueResult();
    }

    @Override
    public Long getItemCountNotDispatched() {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(*) from SaleOrderItem where facility.id = :facilityId and statusCode in (:statusCodes)");
        ArrayList<String> statusCodes = new ArrayList<String>();
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        statusCodes.add(SaleOrderItem.StatusCode.CREATED.name());
        statusCodes.add(SaleOrderItem.StatusCode.FULFILLABLE.name());
        statusCodes.add(SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        query.setParameterList("statusCodes", statusCodes);
        return (Long) query.uniqueResult();
    }

    @Override
    public Long getItemCountDispatchedToday() {

        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(*) from SaleOrderItem where facility.id = :facilityId and shippingPackage.dispatchTime >= :dateStart and shippingPackage.dispatchTime < :dateEnd");
        DateRange dateRange = DateUtils.getDayRange(DateUtils.getCurrentTime());
        query.setParameter("dateStart", dateRange.getStart());
        query.setParameter("dateEnd", dateRange.getEnd());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (Long) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemsByStatus(java.lang.String)
     */
    @Override
    public List<SaleOrderItem> getSaleOrderItemsByStatus(String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrderItem where facility.id = :facilityId and statusCode = :statusCode");
        query.setParameter("statusCode", statusCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemsByStatus(java.lang.String)
     */
    @Override
    public List<SaleOrderDTO> getSaleOrdersForProcessing() {
        int autoSplitHours = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getAutoSplitWaitHours();
        Query query1 = sessionFactory.getCurrentSession().createQuery(
                "select distinct new com.uniware.services.tasks.saleorder.SaleOrderProcessor$SaleOrderDTO(so.id, so.priority, so.fulfillmentTat) from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:saleOrderCreatedStatusCodes) and soi.facility.id = :facilityId and soi.onHold = 0 and so.reassessInventory = 1 and soi.statusCode = :unfulfillableStatusCode");
        Query query2 = sessionFactory.getCurrentSession().createQuery(
                "select distinct new com.uniware.services.tasks.saleorder.SaleOrderProcessor$SaleOrderDTO(so.id, so.priority, so.fulfillmentTat) from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:saleOrderCreatedStatusCodes) and soi.facility.id = :facilityId and soi.onHold = 0 and soi.statusCode in (:createdAndLocationNotServiceableStatusCode)");
        Query query3 = sessionFactory.getCurrentSession().createQuery(
                "select distinct new com.uniware.services.tasks.saleorder.SaleOrderProcessor$SaleOrderDTO(so.id, so.priority, so.fulfillmentTat) from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:saleOrderCreatedStatusCodes) and soi.facility.id = :facilityId and soi.onHold = 0 and soi.statusCode = :fulfillableStatusCode and (soi.updated < :updatedTime or so.reassessInventory = 1) and soi.shippingPackage.id is null");

        List<String> saleOrderCreatedStatusCodes = new ArrayList<String>();
        saleOrderCreatedStatusCodes.add(SaleOrder.StatusCode.CREATED.name());
        saleOrderCreatedStatusCodes.add(SaleOrder.StatusCode.PROCESSING.name());

        query1.setParameter("facilityId", UserContext.current().getFacilityId());
        query1.setParameterList("saleOrderCreatedStatusCodes", saleOrderCreatedStatusCodes);
        query1.setParameter("unfulfillableStatusCode", SaleOrderItem.StatusCode.UNFULFILLABLE.name());

        query2.setParameter("facilityId", UserContext.current().getFacilityId());
        query2.setParameterList("saleOrderCreatedStatusCodes", saleOrderCreatedStatusCodes);
        query2.setParameterList("createdAndLocationNotServiceableStatusCode",
                Arrays.asList(SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.LOCATION_NOT_SERVICEABLE.name()));

        query3.setParameter("facilityId", UserContext.current().getFacilityId());
        query3.setParameterList("saleOrderCreatedStatusCodes", saleOrderCreatedStatusCodes);
        query3.setParameter("fulfillableStatusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        query3.setParameter("updatedTime", DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.HOUR, -autoSplitHours));

        List<SaleOrderDTO> saleOrderDTOs = new ArrayList<>();
        saleOrderDTOs.addAll(query1.list());
        saleOrderDTOs.addAll(query2.list());
        saleOrderDTOs.addAll(query3.list());

        return saleOrderDTOs;
    }

    @Override
    public List<SaleOrderDTO> getSaleOrdersForProcessing(Integer itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct new com.uniware.services.tasks.saleorder.SaleOrderProcessor$SaleOrderDTO(so.id, so.priority, so.fulfillmentTat) from SaleOrderItem soi join soi.saleOrder so where soi.itemType.id = :itemTypeId and soi.facility.id = :facilityId and soi.statusCode = :unfulfillableStatusCode and soi.onHold = 0");
        query.setParameter("unfulfillableStatusCode", SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemByItemTypeByStatus(int, java.lang.String)
     */
    @Override
    public List<SaleOrderItem> getSaleOrderItemByItemTypeByStatus(int itemTypeId, String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select s from SaleOrderItem s where s.facility.id = :facilityId and s.statusCode = :statusCode and s.itemType.id = :itemTypeId");
        query.setParameter("statusCode", SaleOrderItem.StatusCode.UNFULFILLABLE.name());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    //String displayOrderCode, notificationEmail, notificationMobile, statusCode, Date fromDate, Date toDate, boolean onHold
    private Criteria getCriteriaFromRequest(SearchSaleOrderRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SaleOrder.class, "so");
        criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
        if (StringUtils.isNotEmpty(request.getDisplayOrderCode())) {
            SimpleExpression se1 = Restrictions.eq("code", request.getDisplayOrderCode().trim());
            SimpleExpression se2 = Restrictions.eq("displayOrderCode", request.getDisplayOrderCode().trim());
            criteria.add(Restrictions.or(se1, se2));
        }
        if (StringUtils.isNotEmpty(request.getStatus())) {
            criteria.add(Restrictions.eq("statusCode", request.getStatus()));
        }
        if (request.getFacilityCodes() != null && !request.getFacilityCodes().isEmpty()) {
            Criteria saleOrderItemCriteria = criteria.createCriteria("saleOrderItems", "soi");
            saleOrderItemCriteria.add(Restrictions.in("facility.id", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds(request.getFacilityCodes())));
        }
        String customerEmailOrMobile = request.getCustomerEmailOrMobile();
        if (StringUtils.isNotBlank(customerEmailOrMobile)) {
            SimpleExpression se1 = Restrictions.like("notificationMobile", "%" + customerEmailOrMobile + "%");
            SimpleExpression se2 = Restrictions.like("notificationEmail", "%" + customerEmailOrMobile + "%");
            criteria.add(Restrictions.or(se1, se2));
        }

        if (request.getFromDate() != null) {
            criteria.add(Restrictions.ge("created", request.getFromDate()));
        }
        if (request.getToDate() != null) {
            criteria.add(Restrictions.le("created", request.getToDate()));
        }
        if (request.getUpdatedSinceInMinutes() != null) {
            DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.MINUTES, request.getUpdatedSinceInMinutes()));
            criteria.add(Restrictions.ge("updated", dateRange.getStart()));
        }

        Boolean cashOnDelivery = request.getCashOnDelivery();
        if (cashOnDelivery != null) {
            if (cashOnDelivery) {
                criteria.createCriteria("paymentMethod").add(Restrictions.eq("code", PaymentMethod.Code.COD.name()));
            } else {
                criteria.createCriteria("paymentMethod").add(Restrictions.eq("code", PaymentMethod.Code.PREPAID.name()));
            }
        }

        String customerName = request.getCustomerName();
        if (StringUtils.isNotBlank(customerName)) {
            criteria.createCriteria("billingAddress").add(Restrictions.like("name", "%" + customerName.trim() + "%"));
        }

        String channel = request.getChannel();
        if (StringUtils.isNotBlank(channel)) {
            criteria.createCriteria("channel").add(Restrictions.like("code", channel));
        }

        if (request.getOnHold() != null) {
            Criteria soiCriteria = criteria.createCriteria("saleOrderItems", "soi");
            soiCriteria.add(Restrictions.eq("onHold", request.getOnHold().booleanValue()));
        }

        criteria.addOrder(Order.desc("created"));
        return criteria;
    }

    private Criteria getCriteriaFromSearchSaleOrderItemRequest(SearchSaleOrderItemRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SaleOrderItem.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        String saleOrderItemCode = request.getSaleOrderItemCode();
        if (StringUtils.isNotBlank(saleOrderItemCode)) {
            criteria.add(Restrictions.eq("code", saleOrderItemCode));
        }

        if (StringUtils.isNotEmpty(request.getStatus())) {
            criteria.add(Restrictions.eq("statusCode", request.getStatus()));
        }

        if (StringUtils.isNotEmpty(request.getItemNameContains()) || StringUtils.isNotEmpty(request.getItemSkuContains())) {
            Criteria itemCriteria;
            itemCriteria = criteria.createCriteria("itemType");
            if (StringUtils.isNotEmpty(request.getItemNameContains())) {
                itemCriteria.add(Restrictions.like("name", "%" + request.getItemNameContains().trim() + "%"));
            }
            if (StringUtils.isNotEmpty(request.getItemSkuContains())) {
                itemCriteria.add(Restrictions.like("skuCode", "%" + request.getItemSkuContains().trim() + "%"));
            }
        }
        if (request.getFromDate() != null) {
            criteria.add(Restrictions.ge("created", request.getFromDate()));
        }
        if (request.getToDate() != null) {
            criteria.add(Restrictions.le("created", request.getToDate()));
        }

        if (request.isOnHold()) {
            criteria.add(Restrictions.eq("onHold", request.isOnHold()));
        }
        Criteria saleOrderCriteria = criteria.createCriteria("saleOrder");
        Boolean cashOnDelivery = request.getCashOnDelivery();
        if (cashOnDelivery != null) {
            if (cashOnDelivery) {
                saleOrderCriteria.createCriteria("paymentMethod").add(Restrictions.eq("code", PaymentMethod.Code.COD.name()));
            } else {
                saleOrderCriteria.createCriteria("paymentMethod").add(Restrictions.eq("code", PaymentMethod.Code.PREPAID.name()));
            }
        }

        String saleOrderCode = request.getSaleOrderCode();
        if (StringUtils.isNotBlank(saleOrderCode)) {
            saleOrderCriteria.add(Restrictions.eq("code", saleOrderCode));
        }

        return criteria;
    }

    @Override
    public List<SaleOrderItem> searchSaleOrderItems(SearchSaleOrderItemRequest request) {
        Criteria criteria = getCriteriaFromSearchSaleOrderItemRequest(request);
        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        return criteria.list();
    }

    @Override
    public Long getSearchSaleOrderItemRequestCount(SearchSaleOrderItemRequest request) {
        Criteria criteria = getCriteriaFromSearchSaleOrderItemRequest(request);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.list().get(0);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.saleorder.ISaleOrderDao#getSaleOrderItemForAllocationInPicklist(java.lang.Integer, java.lang.Integer)
     */
    @Override
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item) {
        return getSaleOrderItemForAllocationInPicklist(picklistCode, item, null);
    }

    @Override
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, boolean fetchCancelled) {
        return getSaleOrderItemForAllocationInPicklist(picklistCode, item, null, fetchCancelled);
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemsByItemId(Integer itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select s from SaleOrderItem s where s.facility.id = :facilityId and s.item.id = :itemId order by s.id desc");
        query.setParameter("itemId", itemId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public AddressDetail updateAddressDetail(AddressDetail address) {
        sessionFactory.getCurrentSession().merge(address);
        return address;
    }

    @Override
    public AddressDetail getAddressDetail(Integer addressId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from AddressDetail ad where ad.id = :addressId");
        query.setParameter("addressId", addressId);
        return (AddressDetail) query.uniqueResult();
    }

    @Override
    public SaleOrderItemAlternate addSaleOrderItemAlternate(SaleOrderItemAlternate saleOrderItemAlternate) {
        sessionFactory.getCurrentSession().persist(saleOrderItemAlternate);
        return saleOrderItemAlternate;
    }

    @Override
    public SaleOrderItemAlternate getSaleOrderItemAlternateById(Integer alternateId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrderItemAlternate a where a.id = :alternateId");
        query.setParameter("alternateId", alternateId);
        return (SaleOrderItemAlternate) query.uniqueResult();
    }

    @Override
    public SaleOrderItem addSaleOrderItem(SaleOrderItem saleOrderItem) {
        sessionFactory.getCurrentSession().persist(saleOrderItem);
        return saleOrderItem;
    }

    @Override
    public List<Integer> getSaleOrdersForFacilityAllocation() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so.id from SaleOrderItem soi join soi.saleOrder so where so.tenant.id = :tenantId and soi.facility.id is null and soi.statusCode = :createdStatusCode and soi.onHold = 0  order by so.priority desc, so.fulfillmentTat, soi.id");
        query.setParameter("createdStatusCode", SaleOrderItem.StatusCode.CREATED.name());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<String> getSaleOrderCodesForFacilityAllocation() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so.code from SaleOrderItem soi join soi.saleOrder so where so.tenant.id = :tenantId and soi.facility.id is null and soi.statusCode = :createdStatusCode and soi.onHold = 0  order by so.priority desc, so.fulfillmentTat, soi.id");
        query.setParameter("createdStatusCode", SaleOrderItem.StatusCode.CREATED.name());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<SaleOrder> getSaleOrders(List<Integer> ids) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrder so join fetch so.channel c where so.id in (:ids) order by so.created desc");
        query.setParameterList("ids", ids);
        return query.list();
    }

    @Override
    public List<SaleOrderStatus> getOrderStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from SaleOrderStatus").list();
    }

    @Override
    public List<SaleOrderItemStatus> getOrderItemStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from SaleOrderItemStatus").list();
    }

    @Override
    public List<SaleOrderItem> getPendingSaleOrderItemsByFacilityId(Integer facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from SaleOrderItem soi join fetch soi.saleOrder so join fetch soi.itemType it where soi.statusCode in ('CREATED','FULFILLABLE','UNFULFILLABLE') and soi.facility.id = :facilityId");
        query.setParameter("facilityId", facilityId);
        return query.list();
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemsByChannelOrderAndItemCode(String channelSaleOrderCode, String channelSaleOrderItemCode, Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from SaleOrderItem soi where soi.channelSaleOrderItemCode = :channelSaleOrderItemCode and soi.saleOrder.displayOrderCode = :channelSaleOrderCode and soi.saleOrder.channel.id = :channelId and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("channelSaleOrderItemCode", channelSaleOrderItemCode);
        query.setParameter("channelSaleOrderCode", channelSaleOrderCode);
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<SaleOrder> searchSaleOrder(String text) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrder so where (so.code = :text or so.displayOrderCode = :text) and so.tenant.id = :tenantId");
        query.setParameter("text", text);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public SaleOrder getSaleOrderByAdditionalInfo(String additionalInfo) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrder so where so.additionalInfo = :additionalInfo and so.tenant.id = :tenantId");
        query.setParameter("additionalInfo", additionalInfo);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrder) query.uniqueResult();
    }

    @Override
    public List<SaleOrderItem> getSaleOrderItemsByItemIdForTenant(Integer itemId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select s from SaleOrderItem s join s.facility f where f.tenant.id = :tenantId and s.item.id = :itemId order by s.id desc");
        query.setParameter("itemId", itemId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<SaleOrder> getSaleOrderByDisplayOrderCode(String displayOrderCode, Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrder so join fetch so.saleOrderItems soi where so.tenant.id = :tenantId and so.displayOrderCode = :displayOrderCode and so.channel.id = :channelId");
        query.setParameter("displayOrderCode", displayOrderCode);
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public SaleOrderItem getSaleOrderItemByItemCode(String saleOrderCode, String itemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.saleOrder.code = :saleOrderCode and soi.item.code = :itemCode and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("itemCode", itemCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrderItem) query.uniqueResult();
    }

    @Override
    public List<SaleOrder> getReconciliationPendingOrders(int start, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select so from SaleOrder so where so.tenant.id  = :tenantId and so.reconciliationPending = :reconciliationPending and so.reconciliationStatus != :reconciliationStatus");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("reconciliationPending", true);
        query.setParameter("reconciliationStatus", SaleOrder.ReconciliationStatus.IRRECONCILIABLE.name());
        query.setMaxResults(pageSize);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public List<SaleOrder> getSaleOrdersForStatusSync(List<String> statusCodes, int numberOfDays, Integer channelId, int maxId, int pageSize) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrder so join so.saleOrderItems soi where so.tenant.id = :tenantId and so.channel.id = :channelId and soi.statusCode in (:statusCodes) and so.id > :maxId and so.created > :startTime order by so.id");
        query.setParameter("startTime", DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, -numberOfDays));
        query.setParameter("maxId", maxId);
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("statusCodes", statusCodes);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @Override
    public Long getSaleOrderCountForStatusSync(List<String> statusCodes, int numOfDays, Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct so.id) from SaleOrder so join so.saleOrderItems soi where so.tenant.id = :tenantId and so.channel.id = :channelId and soi.statusCode in (:statusCodes) and so.created > :startTime");
        query.setParameter("startTime", DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, -numOfDays));
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("statusCodes", statusCodes);
        return (Long) query.uniqueResult();
    }

    @Override
    public Long getSaleOrderItemCount(String channelId, List<String> statusCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(*) from SaleOrderItem where saleOrder.tenant.id = :tenantId and saleOrder.channel.id = :channelId and soi.statusCode in (:statusCodes)");
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("statusCodes", statusCodes);
        return (Long) query.uniqueResult();
    }

    @Override
    public Long getUnprocessedSaleOrderItemCount(int channelId, String channelProductId) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "select count(*) from sale_order_item soi join sale_order so on soi.sale_order_id = so.id left join shipping_package sp on soi.shipping_package_id = sp.id where so.tenant_id = :tenantId and so.channel_id = :channelId and soi.status_code in (:soiStatusCodes) and soi.channel_product_id = :channelProductId and (sp.id is null or sp.status_code in (:spStatusCodes))");
        query.setParameter("channelId", channelId);
        query.setParameter("channelProductId", channelProductId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("spStatusCodes", Arrays.asList(ShippingPackage.StatusCode.CREATED.name(), ShippingPackage.StatusCode.PICKING.name(),
                ShippingPackage.StatusCode.PICKED.name(), ShippingPackage.StatusCode.LOCATION_NOT_SERVICEABLE.name()));
        query.setParameterList("soiStatusCodes",
                Arrays.asList(SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name()));
        return ((BigInteger) query.uniqueResult()).longValue();
    }

    @Override
    public Map<String, Long> getUnprocessedSaleOrderItemsCount(int channelId) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(
                "select channel_product_id, count(*) from sale_order_item soi join sale_order so on soi.sale_order_id = so.id left join shipping_package sp on soi.shipping_package_id = sp.id where so.tenant_id = :tenantId and so.channel_id = :channelId and soi.status_code in (:soiStatusCodes) and (sp.id is null or sp.status_code in (:spStatusCodes)) group by 1");
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("spStatusCodes", ShippingPackage.StatusCode.getInProcessStatusCodes());
        query.setParameterList("soiStatusCodes",
                Arrays.asList(SaleOrderItem.StatusCode.CREATED.name(), SaleOrderItem.StatusCode.FULFILLABLE.name(), SaleOrderItem.StatusCode.UNFULFILLABLE.name()));
        List<Object[]> rows = query.list();
        Map<String, Long> channelProductIdToSaleOrderItemCount = new HashMap<>(rows.size());
        for (Object[] row : rows) {
            channelProductIdToSaleOrderItemCount.put((String) row[0], ((BigInteger) row[1]).longValue());
        }
        return channelProductIdToSaleOrderItemCount;
    }

    @Override
    public void deleteSaleOrder(SaleOrder saleOrder) {
        sessionFactory.getCurrentSession().delete(saleOrder);
    }

    @Override
    @Transactional(readOnly = true)
    public long getOrderCountCreatedTodayForChannel(Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(*) from SaleOrder where tenant.id = :tenantId and channel.id = :channelId and created >= :dateStart and created < :dateEnd");
        DateRange dateRange = DateUtils.getDayRange(DateUtils.getCurrentTime());
        query.setParameter("dateStart", dateRange.getStart());
        query.setParameter("dateEnd", dateRange.getEnd());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("channelId", channelId);
        return (long) query.uniqueResult();
    }

    @Override
    public SaleOrder getSaleOrderByCode(String code, Integer channelId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrder so where so.tenant.id = :tenantId and so.code = :code and so.channel.id = :channelId");
        query.setParameter("code", code);
        query.setParameter("channelId", channelId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SaleOrder) query.uniqueResult();
    }

    @Override
    public List<SaleOrder> searchSaleOrderByDisplayOrderCode(String text) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrder so where so.displayOrderCode = :text and so.tenant.id = :tenantId");
        query.setParameter("text", text);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<SaleOrder> searchSaleOrderByOrderCode(String text) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SaleOrder so where so.code = :text and so.tenant.id = :tenantId");
        query.setParameter("text", text);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /**
     * Marks a given {@link SaleOrder} dirty.
     * 
     * @param saleOrder
     */
    @Override
    public void markSaleOrderDirty(SaleOrder saleOrder) {
        saleOrder.setViewDirty(true);
        ViewContext.current().addDirtySaleOrderCode(saleOrder.getCode());
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronizationManager.registerSynchronization(viewDirtyPostCommitHandler);
        }
    }

    @Override
    public List<SaleOrder> getDirtySaleOrders(int start, int pageSize) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrderItem soi join soi.saleOrder so where so.viewDirty = :viewDirty and so.tenant.id = :tenantId and soi.facility.id = :facilityId");
        q.setParameter("tenantId", UserContext.current().getTenantId());
        q.setParameter("facilityId", UserContext.current().getFacilityId());
        q.setParameter("viewDirty", true);
        q.setFirstResult(start);
        q.setMaxResults(pageSize);
        return q.list();
    }

    @Override
    public List<SaleOrder> getPendingSaleOrders(Integer pendingSinceInHours, List<String> pendingOrderStatuses, Integer facilityId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:pendingOrderStatuses) and so.tenant.id = :tenantId and so.created >= :dateStart and soi.facility.id = :facilityId");
        DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.HOURS, pendingSinceInHours));
        query.setParameter("dateStart", dateRange.getStart());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("facilityId", facilityId);
        query.setParameterList("pendingOrderStatuses", pendingOrderStatuses);
        return query.list();
    }

    @Override
    public List<SaleOrder> getPaginatedSaleOrdersByStatus(List<String> saleOrderStatusCodes, PageRequest request) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:saleOrderStatusCodes) and so.tenant.id = :tenantId");
        query.setFirstResult((request.getPage() - 1) * request.getSize());
        query.setMaxResults(request.getSize());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("saleOrderStatusCodes", saleOrderStatusCodes);
        return query.list();
    }

    @Override
    public List<SaleOrder> getSaleOrdersByStatus(Integer openSinceInDays, List<String> saleOrderStatusCodes) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct so from SaleOrderItem soi join soi.saleOrder so where so.statusCode in (:saleOrderStatusCodes) and so.tenant.id = :tenantId and so.created >= :dateStart");
        DateRange dateRange = DateUtils.getPastInterval(DateUtils.getCurrentTime(), new Interval(TimeUnit.DAYS, openSinceInDays));
        query.setParameter("dateStart", dateRange.getStart());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameterList("saleOrderStatusCodes", saleOrderStatusCodes);
        return query.list();
    }

    @Override
    public List<String> getBundleItemsByCode(String saleOrderCode, String saleOrderItemCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi.combinationIdentifier from SaleOrderItem soi where soi.saleOrder.code = :saleOrderCode and soi.code = :saleOrderItemCode and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        String combinationIdentifier = (String) query.uniqueResult();
        query = sessionFactory.getCurrentSession().createQuery(
                "select soi.code from SaleOrderItem soi where soi.combinationIdentifier = :combinationIdentifier and soi.saleOrder.code = :saleOrderCode and soi.saleOrder.tenant.id = :tenantId");
        query.setParameter("saleOrderCode", saleOrderCode);
        query.setParameter("combinationIdentifier", combinationIdentifier);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public SaleOrderItem getSoftAllocatedPicklistItemForDeallocationInPicklist(String picklistCode, Item item) {
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        Query query;
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.ITEM_TYPE) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.itemCode = :itemCode and p.facility.id = soi.facility.id and  p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null order by so.priority");
        } else {
            if (item.getShelf() == null) {
                throw new IllegalStateException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.itemCode = :itemCode and p.facility.id = soi.facility.id and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and soi.itemTypeInventory.shelf.id = :shelfId order by so.priority");
            query.setParameter("shelfId", item.getShelf().getId());
        }
        query.setParameter("itemTypeId", item.getItemType().getId());
        query.setParameter("picklistCode", picklistCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemCode", item.getCode()); // look for soft-allocated sale order item
        query.setMaxResults(1);
        return (SaleOrderItem) query.uniqueResult();
    }

    @Override
    public SaleOrderItem getCancelledSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item) {
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        Query query;
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.ITEM_TYPE) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.statusCode = :putbackPendingStatus and p.facility.id = soi.facility.id and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.statusCode = :soiCancelledStatus and soi.item.id is null");
        } else {
            if (item.getShelf() == null) {
                throw new IllegalStateException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.statusCode = :putbackPendingStatus and p.facility.id = soi.facility.id and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.statusCode = :soiCancelledStatus and soi.item.id is null and soi.itemTypeInventory.shelf.id = :shelfId");
            query.setParameter("shelfId", item.getShelf().getId());
        }
        query.setParameter("itemTypeId", item.getItemType().getId());
        query.setParameter("picklistCode", picklistCode);
        query.setParameter("soiCancelledStatus", SaleOrderItem.StatusCode.CANCELLED.name());
        query.setParameter("putbackPendingStatus", PicklistItem.StatusCode.PUTBACK_PENDING);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (SaleOrderItem) query.uniqueResult();
    }

    @Override
    public SaleOrderItem getSaleOrderItemForDeallocationInPicklist(String picklistCode, Item item) {
        SaleOrderItem saleOrderItem;
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        Query query;
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.ITEM_TYPE) {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.itemCode is null and p.facility.id = soi.facility.id and  p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null order by so.priority");
        } else {
            if (item.getShelf() == null) {
                throw new IllegalStateException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
            query = sessionFactory.getCurrentSession().createQuery(
                    "select soi from Picklist p join p.picklistItems pl , SaleOrderItem soi join fetch soi.saleOrder so where so.code = pl.saleOrderCode and pl.saleOrderItemCode = soi.code and pl.itemCode is null and p.facility.id = soi.facility.id and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and soi.itemTypeInventory.shelf.id = :shelfId order by so.priority");
            query.setParameter("shelfId", item.getShelf().getId());
        }
        query.setParameter("itemTypeId", item.getItemType().getId());
        query.setParameter("picklistCode", picklistCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        saleOrderItem = (SaleOrderItem) query.uniqueResult();
        if (saleOrderItem != null) {
            markSaleOrderDirty(saleOrderItem.getSaleOrder());
        }
        return saleOrderItem;
    }
    @Override
    public Long getSaleOrderItemsCountByChannelProductIdAndStatus(String channelProductId, List<String> statusCodes) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select count(soi.id) from SaleOrderItem soi where soi.channelProductId = :channelProductId and soi.statusCode in (:statusCodes) and soi.saleOrder.tenant.id = :tenantId");
        q.setParameterList("statusCodes", statusCodes);
        q.setParameter("channelProductId", channelProductId);
        q.setParameter("tenantId", UserContext.current().getTenantId());
        return (long) q.uniqueResult();
    }

    @Override
    public List<SaleOrderItem> getFulfillableSaleOrderItemsForItemTypeInventory(int itemTypeInventoryId, int numberOfItems) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi join soi.saleOrder so join soi.shippingPackage sp where soi.itemTypeInventory.id = :itemTypeInventoryId and sp.statusCode = :packageCreated and soi.statusCode = :statusCode order by so.priority");
        query.setParameter("itemTypeInventoryId", itemTypeInventoryId);
        query.setParameter("statusCode", SaleOrderItem.StatusCode.FULFILLABLE.name());
        query.setParameter("packageCreated", ShippingPackage.StatusCode.CREATED.name());
        query.setMaxResults(numberOfItems);
        return query.list();
    }

    @Override
    public SaleOrderItem getSaleOrderItemForDeallocation(Item item) {
        Query query;
        query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi join soi.saleOrder so left join soi.shippingPackage sp join soi.itemType it where soi.facility.id = :facilityId and it.skuCode = :skuCode and soi.item.id is null and soi.itemTypeInventory.shelf.id = :shelfId and soi.itemTypeInventory.ageingStartDate = :ageingStartDate and soi.statusCode = :fulfillableStatus and (sp.id is null or sp.statusCode = :createdStatus) order by so.priority");
        query.setParameter("shelfId", item.getShelf().getId());
        query.setParameter("skuCode", item.getItemType().getSkuCode());
        query.setParameter("ageingStartDate", item.getAgeingStartDate());
        query.setParameter("fulfillableStatus", SaleOrderItem.StatusCode.FULFILLABLE.name());
        query.setParameter("createdStatus", ShippingPackage.StatusCode.CREATED.name());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (SaleOrderItem) query.uniqueResult();
    }

    @Override
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode) {
        return getSaleOrderItemForAllocationInPicklist(picklistCode, item, shippingPackageCode, false);
    }

    @Override
    public SaleOrderItem getSaleOrderItemForAllocationInPicklist(String picklistCode, Item item, String shippingPackageCode, boolean fetchCancelled) {
        InventoryAccuracyLevel inventoryAccuracyLevel = ConfigurationManager.getInstance().getConfiguration(SystemConfiguration.class).getInventoryAccuracyLevel();
        Query query;
        //ShippingPackage join required to get only non cancelled items
        if (inventoryAccuracyLevel == InventoryAccuracyLevel.ITEM_TYPE) {
            if (shippingPackageCode == null) {
                query = sessionFactory.getCurrentSession().createQuery(
                        "select soi from Picklist p join p.picklistItems pi, SaleOrderItem soi join fetch soi.saleOrder so left join soi.shippingPackage sp where pi.itemCode is null and soi.code = pi.saleOrderItemCode and pi.saleOrderCode = so.code and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and pi.statusCode in (:picklistItemStatus) order by pi.id");
            } else {
                query = sessionFactory.getCurrentSession().createQuery(
                        "select soi from Picklist p join p.picklistItems pi, SaleOrderItem soi join fetch soi.saleOrder so left join soi.shippingPackage sp where pi.itemCode is null and soi.code = pi.saleOrderItemCode and pi.saleOrderCode = so.code and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and pi.statusCode in (:picklistItemStatus) and pi.shippingPackageCode = :shippingPackageCode order by pi.id");
                query.setParameter("shippingPackageCode", shippingPackageCode);
            }
        } else {
            if (item.getShelf() == null) {
                throw new IllegalStateException("Invalid inventory state, item should be assigned to a shelf in configuration InventoryAccuracyLevel.SHELF");
            }
            if (shippingPackageCode == null) {
                query = sessionFactory.getCurrentSession().createQuery(
                        "select soi from Picklist p join p.picklistItems pi ,SaleOrderItem soi join fetch soi.saleOrder so left join soi.shippingPackage sp join soi.itemTypeInventory iti where pi.itemCode is null and soi.code=pi.saleOrderItemCode and pi.saleOrderCode = so.code and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and iti.shelf.id = :shelfId and pi.statusCode in (:picklistItemStatus) order by pi.id");
            } else {
                query = sessionFactory.getCurrentSession().createQuery(
                        "select soi from Picklist p join p.picklistItems pi ,SaleOrderItem soi join fetch soi.saleOrder so left join soi.shippingPackage sp join soi.itemTypeInventory iti where pi.itemCode is null and soi.code=pi.saleOrderItemCode and pi.saleOrderCode = so.code and p.code = :picklistCode and p.facility.id = :facilityId and soi.itemType.id = :itemTypeId and soi.item.id is null and iti.shelf.id = :shelfId and pi.statusCode in (:picklistItemStatus) and pi.shippingPackageCode = :shippingPackageCode order by pi.id");
                query.setParameter("shippingPackageCode", shippingPackageCode);
            }
            query.setParameter("shelfId", item.getShelf().getId());
        }
        query.setParameter("itemTypeId", item.getItemType().getId());
        query.setParameter("picklistCode", picklistCode);
        List<PicklistItem.StatusCode> picklistItemStatus = new ArrayList<>();
        picklistItemStatus.add(PicklistItem.StatusCode.CREATED);
        picklistItemStatus.add(PicklistItem.StatusCode.RECEIVED);
        if (fetchCancelled) {
            picklistItemStatus.add(PicklistItem.StatusCode.PUTBACK_PENDING);
        }
        query.setParameterList("picklistItemStatus", picklistItemStatus);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        SaleOrderItem soi = (SaleOrderItem) query.uniqueResult();
        if (soi != null) {
            markSaleOrderDirty(soi.getSaleOrder());
        }
        return soi;
    }

    @Override
    public void updateSaleOrderItemStatusForPicklistCreation(Set<Integer> saleOrderItemIds, String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update SaleOrderItem set statusCode = :statusCode where id in (:saleOrderItemIds)");
        query.setParameterList("saleOrderItemIds", saleOrderItemIds);
        query.setParameter("statusCode", statusCode);
        query.executeUpdate();
    }
}
