/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Aug-2013
 *  @author sunny
 */
package com.uniware.dao.saleorder;

import java.util.List;

import com.uniware.core.vo.FacilityAllocationVO;
import com.uniware.core.vo.SaleOrderFlowSummaryVO;
import com.uniware.core.vo.SaleOrderVO;

public interface ISaleOrderMao {

    void save(SaleOrderVO saleOrderVO);

    SaleOrderVO getSaleOrderVOByCode(String code);

    void remove(String saleOrderCode);

    List<SaleOrderVO> getAllSaleOrders();

    List<SaleOrderVO> searchFailedOrPartialOrders(String keyword);

    void removeAllSaleOrders();

    List<SaleOrderVO> getFailedOrdersByChannelProductId(String skuCode);

    List<SaleOrderVO> getFailedOrdersForSaleOrderCodes(List<String> saleOrderCodes);

    void save(FacilityAllocationVO facilityAllocationVO);

    FacilityAllocationVO getFacilityAllocation(String saleOrderItemCode, String saleOrderCode);

    SaleOrderFlowSummaryVO createSaleOrderFlowSummary(SaleOrderFlowSummaryVO summary);

}
