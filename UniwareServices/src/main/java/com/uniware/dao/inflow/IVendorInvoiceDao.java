/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.dao.inflow;

import com.uniware.core.entity.VendorInvoice;
import com.uniware.core.entity.VendorInvoiceItem;

import java.util.List;

public interface IVendorInvoiceDao {

    VendorInvoice getVendorInvoiceByNumber(Integer vendorId, String vendorInvoiceNumber);

    VendorInvoice addVendorCreditInvoice(VendorInvoice vendorInvoice);

    VendorInvoice addVendorDebitInvoice(VendorInvoice vendorInvoice);

    VendorInvoice getVendorInvoiceByCode(String vendorInvoiceCode);

    VendorInvoice getVendorInvoiceByCode(String vendorInvoiceCode, boolean lock);

    VendorInvoice updateVendorInvoice(VendorInvoice vendorInvoice);

    VendorInvoiceItem updateVendorInvoiceItem(VendorInvoiceItem vendorInvoiceItem);

    void removeVendorInvoiceItem(VendorInvoiceItem vendorInvoiceItem);

    List<VendorInvoice> getDebitVendorInvoicesByCreditInvoice(String creditVendorInvoiceCode);

	VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId);

	VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId, boolean lock);

    List<VendorInvoice> getVendorInvoicesByPurchaseOrderCode(String purchaseOrderCode);

}
