/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Mar-2014
 *  @author karunsingla
 */
package com.uniware.dao.inflow.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.VendorInvoice;
import com.uniware.core.entity.VendorInvoiceItem;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inflow.IVendorInvoiceDao;
import com.uniware.services.common.ISequenceGenerator;

@Repository
public class VendorInvoiceDaoImpl implements IVendorInvoiceDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    @Override
    public VendorInvoice getVendorInvoiceByNumber(Integer vendorId, String vendorInvoiceNumber) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vi from VendorInvoice vi where vi.facility.id = :facilityId and vi.toParty.id = :vendorId and vi.vendorInvoiceNumber = :vendorInvoiceNumber");
        query.setParameter("vendorId", vendorId);
        query.setParameter("vendorInvoiceNumber", vendorInvoiceNumber);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (VendorInvoice) query.uniqueResult();
    }

    @Override
    public VendorInvoice addVendorCreditInvoice(VendorInvoice vendorInvoice) {
        vendorInvoice.setFacility(UserContext.current().getFacility());
        vendorInvoice.setCode(sequenceGenerator.generateNext(Name.VENDOR_CREDIT_INVOICE));
        sessionFactory.getCurrentSession().persist(vendorInvoice);
        return vendorInvoice;
    }

    @Override
    public VendorInvoice addVendorDebitInvoice(VendorInvoice vendorInvoice) {
        vendorInvoice.setFacility(UserContext.current().getFacility());
        vendorInvoice.setCode(sequenceGenerator.generateNext(Name.VENDOR_DEBIT_INVOICE));
        sessionFactory.getCurrentSession().persist(vendorInvoice);
        return vendorInvoice;
    }

    @Override
    public VendorInvoice getVendorInvoiceByCode(String vendorInvoiceCode) {
        return getVendorInvoiceByCode(vendorInvoiceCode, false);
    }

    @Override
    public VendorInvoice getVendorInvoiceByCode(String vendorInvoiceCode, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select vi from VendorInvoice vi where vi.facility.id = :facilityId and vi.code = :vendorInvoiceCode");
        query.setParameter("vendorInvoiceCode", vendorInvoiceCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("vi", LockMode.PESSIMISTIC_WRITE);
        }
        return (VendorInvoice) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<VendorInvoice> getDebitVendorInvoicesByCreditInvoice(String creditVendorInvoiceCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select vi from VendorInvoice vi where vi.facility.id = :facilityId and vi.creditVendorInvoice.code = :creditVendorInvoiceCode");
        query.setParameter("creditVendorInvoiceCode", creditVendorInvoiceCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public VendorInvoice updateVendorInvoice(VendorInvoice vendorInvoice) {
        return (VendorInvoice) sessionFactory.getCurrentSession().merge(vendorInvoice);
    }

    @Override
    public VendorInvoiceItem updateVendorInvoiceItem(VendorInvoiceItem vendorInvoiceItem) {
        return (VendorInvoiceItem) sessionFactory.getCurrentSession().merge(vendorInvoiceItem);
    }

    @Override
    public void removeVendorInvoiceItem(VendorInvoiceItem vendorInvoiceItem) {
        sessionFactory.getCurrentSession().delete(vendorInvoiceItem);
    }

	@Override	
	public VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId) {
        return getVendorInvoiceById(vendorInvoiceId, false);
    }   
    @Override
    public VendorInvoice getVendorInvoiceById(Integer vendorInvoiceId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select v from VendorInvoice v left join fetch v.vendorInvoiceItems where v.id = :vendorInvoiceId");
        query.setParameter("vendorInvoiceId", vendorInvoiceId);
        if (lock) {
            query.setLockMode("v", LockMode.PESSIMISTIC_WRITE);
        }
        return (VendorInvoice) query.uniqueResult();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<VendorInvoice> getVendorInvoicesByPurchaseOrderCode(String purchaseOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select vi from VendorInvoice vi join fetch vi.purchaseOrder po where vi.facility.id = :facilityId and po.code = :purchaseOrderCode");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
        
    }
}
