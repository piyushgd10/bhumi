/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.dao.inflow;

import java.util.Date;
import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InflowReceiptStatus;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Shelf;

/**
 * @author singla
 */
public interface IInflowDao {

    /**
     * @param inflowReceipt
     * @return
     */
    InflowReceipt addInflowReceipt(InflowReceipt inflowReceipt);

    List<Item> getItemsByReceiptItemId(int receiptItemId, boolean refresh);

    /**
     * @param inflowReceiptItem
     * @return
     */
    InflowReceiptItem addInflowReceiptItem(InflowReceiptItem inflowReceiptItem);

    /**
     * @param inflowReceiptItemId
     * @param lock
     * @return
     */
    InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId, boolean lock);

    /**
     * @param inflowReceiptItemId
     * @return
     */
    InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId);

    /**
     * @param inflowReceiptItem
     * @return
     */
    InflowReceiptItem updateInflowReceiptItem(InflowReceiptItem inflowReceiptItem);

    /**
     * @param inflowReceiptId
     * @param lock
     * @return
     */
    PurchaseOrder getPurchaseOrderByInflowReceiptCode(String inflowReceiptCode, boolean lock);

    /**
     * @param inflowReceiptId
     * @return
     */
    PurchaseOrder getPurchaseOrderByInflowReceiptCode(String inflowReceiptCode);

    /**
     * @param itemTypeId
     * @return
     */
    Shelf getShelfSuggestionForItemType(int itemTypeId);

    /**
     * @param inflowReceipt
     * @return
     */
    InflowReceipt updateInflowReceipt(InflowReceipt inflowReceipt);

    /**
     * @param inflowReceiptId
     * @return
     */
    InflowReceipt getInflowReceiptById(int inflowReceiptId);

    /**
     * @param inflowReceiptId
     * @return
     */
    List<InflowReceiptItem> getInflowReceiptItems(String inflowReceiptCode);

    /**
     * @param purchaseOrderCode
     * @return
     */
    List<InflowReceipt> getPOInflowReceipts(String purchaseOrderCode);

    /**
     * @param request
     * @return
     */
    List<Object> searchInflowReceipt(SearchReceiptRequest request);

    /**
     * @param request
     * @return
     */
    Long getInflowReceiptCount(SearchReceiptRequest request);

    /**
     * @param receiptItemId
     * @return
     */
    List<Item> getItemsByReceiptItemId(int receiptItemId);

    /**
     * @param statusCode
     * @param beforeTime
     * @return
     */
    List<InflowReceipt> getInflowReceiptsByStatus(String statusCode, Date pastHours);

    /**
     * @param inflowReceiptId
     * @param lock
     * @return
     */
    InflowReceipt getInflowReceiptById(int inflowReceiptId, boolean lock);

    /**
     * @param inflowReceiptId
     * @param fromStatus
     * @param toStatus
     */
    void updateItemStatuses(int inflowReceiptItemId, String fromStatus, String toStatus);

    InflowReceipt getInflowReceiptByCode(String inflowReceiptCode);

    public List<InflowReceipt> getInflowReceipts(List<Object> ids);

    List<InflowReceipt> getPreviousDayGrnSummary(DateRange dateRange, String vendorCode);

    List<InflowReceiptStatus> getInflowReceiptStatuses();

    List<InflowReceipt> getInflowReceipts(GetInflowReceiptsRequest request);

    InflowReceipt getInflowReceiptById(int inflowReceiptId, boolean lock, boolean refresh);

}
