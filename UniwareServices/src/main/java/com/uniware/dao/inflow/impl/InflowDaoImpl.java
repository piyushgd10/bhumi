/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.dao.inflow.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.pagination.SearchOptions;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.api.inflow.GetInflowReceiptsRequest;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.entity.InflowReceipt;
import com.uniware.core.entity.InflowReceiptItem;
import com.uniware.core.entity.InflowReceiptStatus;
import com.uniware.core.entity.Item;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.Shelf;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.inflow.IInflowDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * @author singla
 */
@Repository
public class InflowDaoImpl implements IInflowDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#addInflowReceipt(com.uniware.core.entity.InflowReceipt)
     */
    @Override
    public InflowReceipt addInflowReceipt(InflowReceipt inflowReceipt) {
        inflowReceipt.setFacility(UserContext.current().getFacility());
        inflowReceipt.setCode(sequenceGenerator.generateNext(Name.INFLOW_RECEIPT));
        sessionFactory.getCurrentSession().persist(inflowReceipt);
        return inflowReceipt;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getInflowReceiptItemById(int, boolean)
     */
    @Override
    public InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iri from InflowReceiptItem iri join fetch iri.itemType it join fetch it.category where iri.id = :inflowReceiptItemId");
        query.setParameter("inflowReceiptItemId", inflowReceiptItemId);
        if (lock) {
            query.setLockMode("iri", LockMode.PESSIMISTIC_WRITE);
        }
        return (InflowReceiptItem) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getInflowReceiptItemById(int)
     */
    @Override
    public InflowReceiptItem getInflowReceiptItemById(int inflowReceiptItemId) {
        return getInflowReceiptItemById(inflowReceiptItemId, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#updateInflowReceiptItem(com.uniware.core.entity.InflowReceiptItem)
     */
    @Override
    public InflowReceiptItem updateInflowReceiptItem(InflowReceiptItem inflowReceiptItem) {
        return (InflowReceiptItem) sessionFactory.getCurrentSession().merge(inflowReceiptItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getPurchaseOrderByInflowReceiptId(int, boolean)
     */
    @Override
    public PurchaseOrder getPurchaseOrderByInflowReceiptCode(String inflowReceiptCode, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select p from PurchaseOrder p join fetch p.inflowReceipts ir where ir.facility.id = :facilityId and ir.code = :inflowReceiptCode");
        query.setParameter("inflowReceiptCode", inflowReceiptCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        if (lock) {
            query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        }
        return (PurchaseOrder) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getPurchaseOrderByInflowReceiptId(int)
     */
    @Override
    public PurchaseOrder getPurchaseOrderByInflowReceiptCode(String inflowReceiptCode) {
        return getPurchaseOrderByInflowReceiptCode(inflowReceiptCode, false);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getShelfForItemType(int)
     */
    @Override
    public Shelf getShelfSuggestionForItemType(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iti.shelf from ItemTypeInventory iti where iti.facility.id = :facilityId and iti.itemType.id = :itemTypeId order by iti.created desc");
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setMaxResults(1);
        return (Shelf) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#updateInflowReceipt(com.uniware.core.entity.InflowReceipt)
     */
    @Override
    public InflowReceipt updateInflowReceipt(InflowReceipt inflowReceipt) {
        return (InflowReceipt) sessionFactory.getCurrentSession().merge(inflowReceipt);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getInflowReceiptById(int)
     */
    @Override
    public InflowReceipt getInflowReceiptById(int inflowReceiptId) {
        return getInflowReceiptById(inflowReceiptId, false);
    }

    @Override
    public InflowReceipt getInflowReceiptById(int inflowReceiptId, boolean lock) {
        return getInflowReceiptById(inflowReceiptId, false, false);
    }

    @Override
    public InflowReceipt getInflowReceiptById(int inflowReceiptId, boolean lock, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ir from InflowReceipt ir join fetch ir.purchaseOrder left join fetch ir.inflowReceiptItems iri left join fetch iri.purchaseOrderItem where ir.id = :inflowReceiptId");
        query.setParameter("inflowReceiptId", inflowReceiptId);
        InflowReceipt inflowReceipt = (InflowReceipt) query.uniqueResult();
        if (lock) {
            query.setLockMode("ir", LockMode.PESSIMISTIC_WRITE);
        }
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(inflowReceipt);
        }
        return inflowReceipt;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceiptItem> getInflowReceiptItems(String inflowReceiptCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from InflowReceiptItem iri join fetch iri.purchaseOrderItem where iri.inflowReceipt.facility.id = :facilityId and iri.inflowReceipt.code= :inflowReceiptCode");
        query.setParameter("inflowReceiptCode", inflowReceiptCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceipt> getPOInflowReceipts(String purchaseOrderCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from InflowReceipt where facility.id = :facilityId and purchaseOrder.code= :purchaseOrderCode");
        query.setParameter("purchaseOrderCode", purchaseOrderCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> searchInflowReceipt(SearchReceiptRequest request) {
        Criteria criteria = prepareSearchInflowReceiptCritieria(request);

        if (request.getSearchOptions() != null) {
            SearchOptions options = request.getSearchOptions();
            criteria.setFirstResult(options.getDisplayStart());
            criteria.setMaxResults(options.getDisplayLength());
        }
        criteria.addOrder(Order.desc("created"));
        criteria.setProjection(Projections.distinct(Projections.id()));
        return criteria.list();
    }

    private Criteria prepareSearchInflowReceiptCritieria(SearchReceiptRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InflowReceipt.class, "ir");

        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));

        Integer userId = request.getUserId();
        if (userId != null) {
            criteria.createCriteria("user").add(Restrictions.idEq(userId));
        }

        String invoiceNumber = request.getInvoiceNumber();
        if (StringUtils.isNotBlank(invoiceNumber)) {
            criteria.add(Restrictions.eq("vendorInvoiceNumber", invoiceNumber));
        }
        String inflowReceiptCode = request.getInflowReceiptCode();
        if (StringUtils.isNotBlank(inflowReceiptCode)) {
            criteria.add(Restrictions.eq("code", inflowReceiptCode));
        }

        String itemTypeName = request.getItemTypeName();
        if (StringUtils.isNotBlank(request.getItemTypeName())) {
            criteria.createCriteria("inflowReceiptItems").createCriteria("itemType", "it");
            criteria.add(Restrictions.or(Restrictions.like("it.name", "%" + itemTypeName + "%"), Restrictions.like("it.skuCode", "%" + itemTypeName + "%")));
        }

        String statusCode = request.getStatusCode();
        if (StringUtils.isNotEmpty(statusCode)) {
            criteria.add(Restrictions.eq("statusCode", statusCode));
        }

        Criteria purchaseOrderCriteria = criteria.createCriteria("purchaseOrder");
        String purchaseOrderCode = request.getPurchaseOrderCode();
        if (StringUtils.isNotBlank(purchaseOrderCode)) {
            purchaseOrderCriteria.add(Restrictions.like("code", "%" + purchaseOrderCode + "%"));
        }

        Integer vendorId = request.getVendorId();
        if (vendorId != null) {
            purchaseOrderCriteria.createCriteria("vendor").add(Restrictions.idEq(vendorId));
        }
        String vendorName = request.getVendorName();
        if (StringUtils.isNotEmpty(vendorName)) {
            purchaseOrderCriteria.createCriteria("vendor").add(Restrictions.like("name", "%" + vendorName + "%"));
        }

        if (request.getFromDate() != null && request.getToDate() != null) {
            criteria.add(Restrictions.between("created", request.getFromDate(), request.getToDate()));
        }
        return criteria;
    }

    @Override
    public Long getInflowReceiptCount(SearchReceiptRequest request) {
        Criteria criteria = prepareSearchInflowReceiptCritieria(request);
        criteria.setProjection(Projections.countDistinct("ir.id"));
        return (Long) criteria.list().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceipt> getInflowReceipts(List<Object> ids) {
        Query query = sessionFactory.getCurrentSession().createQuery("from InflowReceipt ir where ir.facility.id = :facilityId and ir.id in (:ids) order by ir.created desc");
        query.setParameterList("ids", ids);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#getItemsByReceiptItemId(int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Item> getItemsByReceiptItemId(int receiptItemId) {
        return getItemsByReceiptItemId(receiptItemId, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Item> getItemsByReceiptItemId(int receiptItemId, boolean refresh) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct i from Item i join fetch i.itemType it join fetch it.category join fetch i.inflowReceiptItem iri join fetch iri.inflowReceipt ir join fetch iri.purchaseOrderItem join fetch i.vendor where i.inflowReceiptItem.id =:receiptItemId");
        query.setParameter("receiptItemId", receiptItemId);
        List<Item> items = query.list();
        if (refresh) {
            sessionFactory.getCurrentSession().refresh(items);
        }
        return items;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inflow.IInflowDao#addInflowReceiptItem(com.uniware.core.entity.InflowReceiptItem)
     */
    @Override
    public InflowReceiptItem addInflowReceiptItem(InflowReceiptItem inflowReceiptItem) {
        sessionFactory.getCurrentSession().persist(inflowReceiptItem);
        return inflowReceiptItem;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceipt> getInflowReceiptsByStatus(String statusCode, Date pastHours) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InflowReceipt.class);
        criteria.add(Restrictions.eq("statusCode", statusCode));
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (pastHours != null) {
            criteria.add(Restrictions.lt("updated", pastHours));
        }
        return criteria.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.inventory.IInventoryDao#updateItemStatuses(java.lang.String, java.lang.String)
     */
    @Override
    public void updateItemStatuses(int inflowReceiptItemId, String fromStatus, String toStatus) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set statusCode = :toStatus where facility.id = :facilityId and inflowReceiptItem.id = :inflowReceiptItemId and statusCode = :fromStatus");
        query.setParameter("inflowReceiptItemId", inflowReceiptItemId);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("fromStatus", fromStatus);
        query.setParameter("toStatus", toStatus);
        query.executeUpdate();
    }

    @Override
    public InflowReceipt getInflowReceiptByCode(String inflowReceiptCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ir from InflowReceipt ir join fetch ir.purchaseOrder left join fetch ir.inflowReceiptItems iri left join fetch iri.purchaseOrderItem where ir.facility.id = :facilityId and ir.code = :inflowReceiptCode");
        query.setParameter("inflowReceiptCode", inflowReceiptCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return (InflowReceipt) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceipt> getPreviousDayGrnSummary(DateRange dateRange, String vendorCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ir from InflowReceipt ir join fetch ir.inflowReceiptItems iri join fetch ir.user u join fetch ir.purchaseOrder po join fetch po.vendor v join fetch iri.itemType where ir.facility.id = :facilityId and ir.created > :startDate and ir.created < :endDate and v.code = :vendorCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("startDate", dateRange.getStart());
        query.setParameter("endDate", dateRange.getEnd());
        query.setParameter("vendorCode", vendorCode);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceiptStatus> getInflowReceiptStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from InflowReceiptStatus").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<InflowReceipt> getInflowReceipts(GetInflowReceiptsRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(InflowReceipt.class);
        criteria.createCriteria("facility").add(Restrictions.idEq(UserContext.current().getFacilityId()));
        if (request.getPurchaseOrderCode() != null) {
            criteria.createCriteria("purchaseOrder").add(Restrictions.eq("code", request.getPurchaseOrderCode()));
        }
        if (request.getCreatedBetween() != null) {
            criteria.add(Restrictions.between("created", request.getCreatedBetween().getStart(), request.getCreatedBetween().getEnd()));
        }
        return criteria.list();
    }
}
