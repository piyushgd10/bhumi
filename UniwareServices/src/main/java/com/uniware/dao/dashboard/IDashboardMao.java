/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 10/23/15 12:01 PM
 * @author harshpal
 */

package com.uniware.dao.dashboard;

import com.uniware.core.vo.DashboardWidgetVO;

import java.util.Date;

/**
 * Created by harshpal on 10/23/15.
 */
public interface IDashboardMao {

    public void insertDashboardWidgetData(DashboardWidgetVO widgetVO);

    public DashboardWidgetVO getWidgetData(String cacheWidgetIdentifier);
}
