/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 10/23/15 12:02 PM
 * @author harshpal
 */

package com.uniware.dao.dashboard.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.DashboardWidgetVO;
import com.uniware.dao.dashboard.IDashboardMao;
import org.springframework.stereotype.Repository;

/**
 * Created by harshpal on 10/23/15.
 */
@Repository
public class DashboardMaoImpl implements IDashboardMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public void insertDashboardWidgetData(DashboardWidgetVO widgetVO) {
        widgetVO.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.insert(widgetVO);
    }

    @Override
    public DashboardWidgetVO getWidgetData(String cacheWidgetIdentifier) {
        return mongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("cacheWidgetIdentifier").is(cacheWidgetIdentifier)), DashboardWidgetVO.class);
    }
}
