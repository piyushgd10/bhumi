/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.bundle;

import java.util.List;

import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;

public interface IBundleDao {

    public Bundle create(Bundle bundle);

    public Bundle getBundleByCode(String code);

    public Bundle update(Bundle bundle);

    BundleItemType create(BundleItemType bundleItemType);

    public List<Bundle> lookupBundle(String keyword);

    void deleteBundleItemType(BundleItemType bundleItemType);

    public List<Bundle> getBundlesByComponentItemType(String itemSku);

    Bundle getBundleBySkuCode(String bundleSkuCode);
}
