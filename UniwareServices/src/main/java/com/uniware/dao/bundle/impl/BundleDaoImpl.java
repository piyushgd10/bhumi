/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 20, 2012
 *  @author praveeng
 */
package com.uniware.dao.bundle.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.BundleItemType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.bundle.IBundleDao;

@Repository
public class BundleDaoImpl implements IBundleDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Bundle create(Bundle bundle) {
        bundle.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(bundle);
        return bundle;
    }

    @Override
    public Bundle getBundleByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Bundle b join fetch b.itemType it where b.tenant.id = :tenantId and b.code = :code");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", code);
        return (Bundle) query.uniqueResult();
    }

    @Override
    public Bundle update(Bundle bundle) {
        bundle.setTenant(UserContext.current().getTenant());
        return (Bundle) sessionFactory.getCurrentSession().merge(bundle);
    }

    @Override
    public BundleItemType create(BundleItemType bundleItemType) {
        sessionFactory.getCurrentSession().persist(bundleItemType);
        return bundleItemType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Bundle> lookupBundle(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Bundle b join fetch b.itemType it where b.tenant.id = :tenantId and code like :keyword");
        query.setParameter("keyword", "%" + keyword + "%");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setMaxResults(10);
        return query.list();
    }

    @Override
    public void deleteBundleItemType(BundleItemType bundleItemType) {
        sessionFactory.getCurrentSession().delete(bundleItemType);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Bundle> getBundlesByComponentItemType(String itemSku) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select b from Bundle b join b.bundleItemTypes bit where bit.itemType.skuCode = :itemSku and b.tenant.id = :tenantId");
        query.setParameter("itemSku", itemSku);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /**
     * This is a method used to get bundle (along with its itemType), which has the item type with the sku code that is
     * supplied in the request.
     * 
     * @apiNote: Note that the current method fetches only 1 Bundle, where there can be in fact many bundles of the same
     *           sku code. Thus, it can be used to fetch a particular bundle. This is due to design in product, where
     *           currently one bundle sku has typically only one bundle. <b>However</b>, this is not enforced anywhere
     *           in the code, and hence may give wrong results if one expects it to work properly.<br/>
     *           If you are fixing this flaw, please make appropriate changes to
     *           {@link com.uniware.core.entity.SaleOrderItem}, which currently contains bundleSkuCode, instead of the
     *           bundleCode, which it should contain. Also, change the method {@link #getBundleByCode(String)}, which
     *           was formerly called <b>getBundleByItemSkuCode</b>. This misnomer caused it to be used it many places
     *           with item sku as argument (hence the requirement for refactoring). Please fix the bugs where it was
     *           used incorrectly.
     * @param bundleSkuCode @
     */
    @Override
    public Bundle getBundleBySkuCode(String bundleSkuCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Bundle b join fetch b.itemType it where b.tenant.id = :tenantId and b.itemType.skuCode = :code");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", bundleSkuCode);
        query.setMaxResults(1);
        return (Bundle) query.uniqueResult();
    }
}
