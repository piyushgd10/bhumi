/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2012
 *  @author singla
 */
package com.uniware.dao.putaway.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.ItemTypeInventory.Type;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.PutawayStatus;
import com.uniware.core.entity.Sequence.Name;
import com.uniware.core.entity.Shelf;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.putaway.IPutawayDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * @author singla
 */
@Repository
public class PutawayDaoImpl implements IPutawayDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#getLastShelfForPutaway(int)
     */
    @Override
    public Shelf getLastShelfForPutaway(int itemTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select iti from ItemTypeInventory iti join fetch iti.shelf s where iti.type=:inventoryType and iti.facility.id = :facilityId and iti.itemType.id = :itemTypeId and s.statusCode != :inactiveStatus order by iti.created desc");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("inventoryType", Type.GOOD_INVENTORY);
        query.setParameter("inactiveStatus", Shelf.StatusCode.INACTIVE.name());
        query.setMaxResults(1);
        ItemTypeInventory itemTypeInventory = (ItemTypeInventory) query.uniqueResult();
        if (itemTypeInventory != null) {
            return itemTypeInventory.getShelf();
        }
        return null;
    }

    @Override
    @Transactional
    public Putaway addPutaway(Putaway putaway) {
        putaway.setFacility(UserContext.current().getFacility());
        putaway.setCode(sequenceGenerator.generateNext(Name.PUTAWAY));
        sessionFactory.getCurrentSession().persist(putaway);
        return putaway;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#getPutawayItemById(java.lang.Integer)
     */
    @Override
    public PutawayItem getPutawayItemById(Integer putawayItemId) {
        return getPutawayItemById(putawayItemId, false);
    }

    @Override
    public PutawayItem getPutawayItemById(Integer putawayItemId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select pi from PutawayItem pi where pi.putaway.facility.id = :facilityId and pi.id = :putawayItemId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("putawayItemId", putawayItemId);
        if (lock) {
            query.setLockMode("pi", LockMode.PESSIMISTIC_WRITE);
        }
        return (PutawayItem) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#addPutawayItem(com.uniware.core.entity.PutawayItem)
     */
    @Override
    public PutawayItem addPutawayItem(PutawayItem putawayItem) {
        sessionFactory.getCurrentSession().persist(putawayItem);
        return putawayItem;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#updatePutawayItem(com.uniware.core.entity.PutawayItem)
     */
    @Override
    public PutawayItem updatePutawayItem(PutawayItem putawayItem) {
        return (PutawayItem) sessionFactory.getCurrentSession().merge(putawayItem);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#getPutawayById(java.lang.Integer, boolean)
     */
    @Override
    public Putaway getPutawayById(Integer putawayId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select p from Putaway p left join fetch p.putawayItems where p.id = :putawayId");
        query.setParameter("putawayId", putawayId);
        return (Putaway) query.uniqueResult();
    }

    @Override
    public Putaway getPutawayByCode(String putawayCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select p from Putaway p left join fetch p.putawayItems where p.facility.id = :facilityId and p.code = :putawayCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("putawayCode", putawayCode);
        return (Putaway) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#updatePutaway(com.uniware.core.entity.Putaway)
     */
    @Override
    public Putaway updatePutaway(Putaway putaway) {
        return (Putaway) sessionFactory.getCurrentSession().merge(putaway);
    }

    @Override
    public void addReceiptItemsToPutaway(int inflowReceiptItemId, Putaway putaway) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set putaway.id = :putawayId, statusCode = :toStatus where facility.id = :facilityId and inflowReceiptItem.id = :inflowReceiptItemId and statusCode = :fromStatus");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("inflowReceiptItemId", inflowReceiptItemId);
        query.setParameter("putawayId", putaway.getId());
        query.setParameter("fromStatus", Item.StatusCode.QC_COMPLETE.name());
        query.setParameter("toStatus", Item.StatusCode.PUTAWAY_PENDING.name());
        query.executeUpdate();
    }

    @Override
    public void addItemsToInventory(int putawayId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set statusCode = inventoryType where facility.id = :facilityId and putaway.id = :putawayId and statusCode = :fromStatus");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("putawayId", putawayId);
        query.setParameter("fromStatus", Item.StatusCode.PUTAWAY_PENDING.name());
        query.executeUpdate();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.putaway.IPutawayDao#updateItemShelf(int, java.lang.Integer, java.lang.Integer)
     */
    @Override
    public void updateItemShelf(int putawayId, Integer itemTypeId, Integer shelfId, Type inventoryType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set shelf.id = :shelfId where facility.id = :facilityId and putaway.id = :putawayId and itemType.id = :itemTypeId and statusCode = :status and inventoryType = :inventoryType");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("putawayId", putawayId);
        query.setParameter("shelfId", shelfId);
        query.setParameter("itemTypeId", itemTypeId);
        query.setParameter("status", Item.StatusCode.PUTAWAY_PENDING.name());
        query.setParameter("inventoryType", inventoryType);
        query.executeUpdate();
    }

    /**
     * Updates {@link Item} with matching {@code skuCode, putawayId, shelfId}. Used where traceability level is ITEM.
     * 
     * @see #updateItemShelf(int, Integer, Integer, Type)
     */
    @Override
    public void updateItemShelf(int putawayId, String skuCode, Integer shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update Item set shelf.id = :shelfId where facility.id = :facilityId and putaway.id = :putawayId and itemType.skuCode = :skuCode and statusCode = :status");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("putawayId", putawayId);
        query.setParameter("shelfId", shelfId);
        query.setParameter("skuCode", skuCode);
        query.setParameter("status", Item.StatusCode.PUTAWAY_PENDING.name());
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PutawayStatus> getPutawayStatuses() {
        return sessionFactory.getCurrentSession().createQuery("from PutawayStatus").list();

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Item> getItemsByPutawayId(int putawayId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct i from Item i join fetch i.itemType it join fetch it.category join fetch i.inflowReceiptItem iri join fetch iri.inflowReceipt ir join fetch ir.purchaseOrder po join fetch i.vendor v join fetch i.putaway p where i.putaway.id =:putawayId");
        query.setParameter("putawayId", putawayId);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Item> getPendingItemsByPutawayId(int putawayId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Item where putaway.id= :putawayId and statusCode= :putawayPending");
        query.setParameter("putawayId", putawayId);
        query.setParameter("putawayPending", Item.StatusCode.PUTAWAY_PENDING.name());
        return query.list();
    }

    @Override
    public Long getPutawaysPendingForShelfCount(int shelfId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(distinct p) from Putaway p join p.putawayItems pi where pi.shelf.id= :shelfId and p.statusCode in (:statusCodes)");
        query.setParameter("shelfId", shelfId);
        List<String> statusCodes = new ArrayList<>();
        statusCodes.add(Putaway.StatusCode.CREATED.name());
        statusCodes.add(Putaway.StatusCode.PENDING.name());
        query.setParameterList("statusCodes", statusCodes);
        return (Long) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getPendingShelvesForPutaway(String putawayCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct s.code from Putaway p join p.putawayItems pi join pi.shelf s where p.code = :putawayCode and pi.statusCode != :statusCode");
        query.setParameter("putawayCode", putawayCode);
        query.setParameter("statusCode", PutawayItem.StatusCode.COMPLETE.name());
        return query.list();
    }
}