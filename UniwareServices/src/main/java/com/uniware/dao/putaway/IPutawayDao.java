/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 5, 2012
 *  @author singla
 */
package com.uniware.dao.putaway;

import java.util.List;

import com.uniware.core.entity.Item;
import com.uniware.core.entity.ItemTypeInventory;
import com.uniware.core.entity.Putaway;
import com.uniware.core.entity.PutawayItem;
import com.uniware.core.entity.PutawayStatus;
import com.uniware.core.entity.Shelf;

/**
 * @author singla
 */
public interface IPutawayDao {

    Shelf getLastShelfForPutaway(int itemTypeId);

    /**
     * @param putaway
     * @return
     */
    Putaway addPutaway(Putaway putaway);

    /**
     * @param putawayItemId
     * @return
     */
    PutawayItem getPutawayItemById(Integer putawayItemId);

    /**
     * @param putawayItem
     * @return
     */
    PutawayItem addPutawayItem(PutawayItem putawayItem);

    /**
     * @param putawayItem
     * @return
     */
    PutawayItem updatePutawayItem(PutawayItem putawayItem);

    /**
     * @param putawayId
     * @return
     */
    Putaway getPutawayById(Integer putawayId);

    /**
     * @param putaway
     * @return
     */
    Putaway updatePutaway(Putaway putaway);

    /**
     * @param inflowReceiptItemId
     * @param putaway
     */
    void addReceiptItemsToPutaway(int inflowReceiptItemId, Putaway putaway);

    /**
     * @param putawayId
     */
    void addItemsToInventory(int putawayId);

    /**
     * @param putawayId
     * @param itemTypeId
     */
    void updateItemShelf(int putawayId, Integer itemTypeId, Integer shelfId, ItemTypeInventory.Type inventoryType);

    /**
     * @param putawayCode
     * @return
     */
    Putaway getPutawayByCode(String putawayCode);

    void updateItemShelf(int putawayId, String skuCode, Integer shelfId);

    List<PutawayStatus> getPutawayStatuses();

    PutawayItem getPutawayItemById(Integer putawayItemId, boolean lock);

    List<Item> getItemsByPutawayId(int putawayId);

    List<Item> getPendingItemsByPutawayId(int putawayId);

    Long getPutawaysPendingForShelfCount(int shelfId);

    List<String> getPendingShelvesForPutaway(String putawayCode);
}
