/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.dao.invoice.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.StringUtils;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.invoice.IInvoiceDao;
import com.uniware.services.common.ISequenceGenerator;

/**
 * @author singla
 */
@Repository
public class InvoiceDaoImpl implements IInvoiceDao {

    @Autowired
    private SessionFactory     sessionFactory;

    @Autowired
    private ISequenceGenerator sequenceGenerator;

    /* (non-Javadoc)
     * @see com.uniware.dao.invoice.IInvoiceDao#addInvoice(com.uniware.core.entity.Invoice)
     */
    @Override
    public Invoice addInvoice(Invoice invoice) {
        invoice.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(invoice);
        return invoice;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.invoice.IInvoiceDao#getInvoiceById(int)
     */
    @Override
    public Invoice getInvoiceById(int invoiceId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select i from Invoice i where i.facility.id = :facilityId and i.id = :invoiceId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("invoiceId", invoiceId);
        return (Invoice) query.uniqueResult();
    }

    @Override
    public Invoice getInvoiceByCode(String invoiceCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select i from Invoice i where i.facility.id = :facilityId and i.code = :invoiceCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("invoiceCode", invoiceCode);
        return (Invoice) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.invoice.IInvoiceDao#updateInvoice(com.uniware.core.entity.Invoice)
     */
    @Override
    public Invoice updateInvoice(Invoice invoice) {
        invoice.setFacility(UserContext.current().getFacility());
        return (Invoice) sessionFactory.getCurrentSession().merge(invoice);
    }

    @Override
    public void removeInvoiceItem(InvoiceItem invoiceItem) {
        sessionFactory.getCurrentSession().delete(invoiceItem);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Invoice> searchInvoice(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Invoice where code like :text and facility.id = :facilityId");
        query.setParameter("text", keyword);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }
}
