/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 20, 2012
 *  @author singla
 */
package com.uniware.dao.invoice;

import java.util.List;

import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.InvoiceItem;

/**
 * @author singla
 */
public interface IInvoiceDao {

    /**
     * @param invoice
     * @return
     */
    Invoice addInvoice(Invoice invoice);

    /**
     * @param invoiceId
     * @return
     */
    Invoice getInvoiceById(int invoiceId);

    Invoice getInvoiceByCode(String invoiceCode);

    /**
     * @param invoice
     * @return
     */
    Invoice updateInvoice(Invoice invoice);

    void removeInvoiceItem(InvoiceItem invoiceItem);

    List<Invoice> searchInvoice(String keyword);
}
