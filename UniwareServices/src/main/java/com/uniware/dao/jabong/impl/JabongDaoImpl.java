/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 4, 2012
 *  @author singla
 */
package com.uniware.dao.jabong.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.jabong.IJabongDao;
import com.uniware.dao.saleorder.ISaleOrderDao;
import com.uniware.services.jabong.impl.JabongServiceImpl;

@Repository
public class JabongDaoImpl implements IJabongDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ISaleOrderDao  saleOrderDao;

    @Override
    public SaleOrderItem getSaleOrderItemByCodeByStatus(String saleOrderItemCode, String statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.facility.id = :facilityId and soi.code = :saleOrderItemCode and soi.statusCode = :statusCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("saleOrderItemCode", saleOrderItemCode);
        query.setParameter("statusCode", statusCode);
        SaleOrderItem soi = (SaleOrderItem) query.uniqueResult();
        if (soi != null) {
            saleOrderDao.markSaleOrderDirty(soi.getSaleOrder());
        }
        return soi;
    }

    @Override
    public SaleOrderItem getSaleOrderItemForAllocation(ItemType itemType, SaleOrderItem.StatusCode statusCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi join soi.saleOrder so where soi.onHold = 0 and soi.facility.id = :facilityId and soi.item.id is null and soi.itemType.id = :itemTypeId and soi.statusCode in (:statusCodes) order by so.priority desc, so.displayOrderDateTime");
        query.setParameter("itemTypeId", itemType.getId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        List<String> statusCodes = new ArrayList<String>();
        statusCodes.add(statusCode.name());
        query.setParameterList("statusCodes", statusCodes);
        query.setMaxResults(1);
        SaleOrderItem soi = (SaleOrderItem) query.uniqueResult();
        if (soi != null) {
            saleOrderDao.markSaleOrderDirty(soi.getSaleOrder());
        }
        return soi;
    }

    @Override
    public SaleOrderItem getSaleOrderItemForInventoryDeallocation(ItemType itemType) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select soi from SaleOrderItem soi where soi.facility.id = :facilityId and soi.item.id is null and soi.itemType.id = :itemTypeId and soi.statusCode in (:statusCodes) order by soi.created");
        query.setParameter("itemTypeId", itemType.getId());
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        List<String> statusCodes = new ArrayList<String>();
        statusCodes.add(SaleOrderItem.StatusCode.FULFILLABLE.name());
        query.setParameterList("statusCodes", statusCodes);
        query.setMaxResults(1);
        SaleOrderItem soi = (SaleOrderItem) query.uniqueResult();
        if (soi != null) {
            saleOrderDao.markSaleOrderDirty(soi.getSaleOrder());
        }
        return soi;
    }

    @Override
    public Shelf getShelfForPutaway(int itemCount) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select s from Shelf s join fetch s.shelfType st where s.section.facility.id = :facilityId and s.itemCount = 0 and s.code != :standardShelfCode and s.code != :defaultShelfCode and s.statusCode != :inactiveStatusCode and st.length >= :itemCount order by st.length,s.code");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("standardShelfCode", JabongServiceImpl.SHELF_CODE);
        query.setParameter("defaultShelfCode", "DEFAULT");
        query.setParameter("itemCount", itemCount > 2 ? 2 : 0);
        query.setParameter("inactiveStatusCode", Shelf.StatusCode.INACTIVE.name());
        query.setMaxResults(1);
        return (Shelf) query.uniqueResult();
    }

    @Override
    public Shelf getShelfByCode(String shelfCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select s from Shelf s  where s.section.facility.id = :facilityId and s.code = :shelfCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("shelfCode", shelfCode);
        query.setMaxResults(1);
        return (Shelf) query.uniqueResult();
    }
}
