/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jul 4, 2012
 *  @author singla
 */
package com.uniware.dao.jabong;

import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.Shelf;

public interface IJabongDao {

    SaleOrderItem getSaleOrderItemByCodeByStatus(String saleOrderItemCode, String statusCode);

    SaleOrderItem getSaleOrderItemForAllocation(ItemType itemType, SaleOrderItem.StatusCode statusCode);

    SaleOrderItem getSaleOrderItemForInventoryDeallocation(ItemType itemType);

    Shelf getShelfForPutaway(int itemCount);

    Shelf getShelfByCode(String shelfCode);

}
