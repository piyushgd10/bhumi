/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.party.impl;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;
import com.uniware.core.utils.UserContext;
import com.uniware.dao.party.IPartyDao;

@Repository
public class PartyDaoImpl implements IPartyDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public PartyAddress getPartyAddress(String partyCode, int partyAddressTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PartyAddress pa where ((pa.party.facility.id is null and pa.party.tenant.id = :tenantId) or pa.party.facility.id = :facilityId) and pa.party.code = :partyCode and pa.partyAddressType.id = :partyAddressTypeId");
        query.setParameter("partyCode", partyCode);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("partyAddressTypeId", partyAddressTypeId);

        return (PartyAddress) query.uniqueResult();
    }

    @Override
    public PartyAddress createAddress(PartyAddress partyAddress) {
        sessionFactory.getCurrentSession().persist(partyAddress);
        return partyAddress;
    }

    @Override
    public PartyAddress updateAddress(PartyAddress partyAddress) {
        sessionFactory.getCurrentSession().merge(partyAddress);
        return partyAddress;
    }

    @Override
    public Party getPartyById(int partyId) {
        return getPartyById(partyId, false);
    }

    @Override
    public Party getPartyById(int partyId, boolean lock) {
        Query query = sessionFactory.getCurrentSession().createQuery("select p from Party p where p.id=:partyId");
        query.setParameter("partyId", partyId);
        if (lock) {
            query.setLockMode("p", LockMode.PESSIMISTIC_WRITE);
        }
        return (Party) query.uniqueResult();
    }

    @Override
    public PartyContact getPartyContact(String partyCode, Integer partyContactTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PartyContact pc where ((pc.party.facility.id is null and pc.party.tenant.id = :tenantId) or pc.party.facility.id = :facilityId) and pc.party.code = :partyCode and pc.partyContactType.id = :partyContactTypeId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("partyCode", partyCode);
        query.setParameter("partyContactTypeId", partyContactTypeId);
        return (PartyContact) query.uniqueResult();
    }

    @Override
    public PartyContact createContact(PartyContact partyContact) {
        sessionFactory.getCurrentSession().persist(partyContact);
        return partyContact;
    }

    @Override
    public PartyContact updateContact(PartyContact partyContact) {
        sessionFactory.getCurrentSession().merge(partyContact);
        return partyContact;
    }

    @Override
    public void deletePartyContact(PartyContact partyContact) {
        sessionFactory.getCurrentSession().delete(partyContact);
    }

    @Override
    public Party getPartyByCode(String partyCode) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Party where ((facility.id is null and tenant.id = :tenantId) or facility.id = :facilityId) and code=:partyCode");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("partyCode", partyCode);
        return (Party) query.uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.party.IPartyDao#updateParty(com.uniware.core.entity.Party)
     */
    @Override
    public Party updateParty(Party party) {
        party.setFacility(UserContext.current().getFacility());
        return (Party) sessionFactory.getCurrentSession().merge(party);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Party> lookupParty(String keyword) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from Party where ((facility.id is null and facility.tenant.id = :tenantId) or facility.id = :facilityId) and (code=:code or name like :name)");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("code", keyword);
        query.setParameter("name", "%" + keyword + "%");
        query.setMaxResults(10);
        return query.list();
    }

    @Override
    public PartyAddress getPartyAddress(Integer partyId, int partyAddressTypeId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PartyAddress pa where pa.party.id = :partyId and pa.partyAddressType.id = :partyAddressTypeId");
        query.setParameter("partyId", partyId);
        query.setParameter("partyAddressTypeId", partyAddressTypeId);
        return (PartyAddress) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PartyAddressType> getPartyAddressTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PartyAddressType where tenant.id is null or tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PartyContactType> getPartyContactTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PartyContactType where tenant.id is null or tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }
}
