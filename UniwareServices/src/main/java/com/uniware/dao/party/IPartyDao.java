/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Feb-2012
 *  @author vibhu
 */
package com.uniware.dao.party;

import java.util.List;

import com.uniware.core.entity.Party;
import com.uniware.core.entity.PartyAddress;
import com.uniware.core.entity.PartyAddressType;
import com.uniware.core.entity.PartyContact;
import com.uniware.core.entity.PartyContactType;

public interface IPartyDao {

    PartyAddress getPartyAddress(String partyCode, int partyAddressTypeId);

    PartyAddress getPartyAddress(Integer partyId, int partyAddressTypeId);

    PartyAddress createAddress(PartyAddress partyAddress);

    PartyAddress updateAddress(PartyAddress partyAddress);

    Party getPartyById(int partyId);

    PartyContact getPartyContact(String partyCode, Integer id);

    PartyContact createContact(PartyContact partyContact);

    PartyContact updateContact(PartyContact partyContact);

    Party getPartyByCode(String partyCode);

    /**
     * @param partyId
     * @param lock
     * @return
     */
    Party getPartyById(int partyId, boolean lock);

    Party updateParty(Party party);

    List<Party> lookupParty(String keyword);

    void deletePartyContact(PartyContact partyContact);

    List<PartyAddressType> getPartyAddressTypes();

    List<PartyContactType> getPartyContactTypes();

}
