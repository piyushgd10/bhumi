/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.mao.recommendation;

import java.util.List;

import com.unifier.core.utils.DateUtils.DateRange;
import com.uniware.core.vo.PriceRecommendationVO;
import com.uniware.core.vo.RecommendationProviderVO;
import com.uniware.core.vo.RecommendationSubscriptionVO;
import com.uniware.core.vo.RecommendationTypeVO;

/**
 * 
 * <ul>
 * The responsibilities include:
 * <li>Get all recommendation types
 * <li>Get all recommendation providers
 * <li>Get recommendation provider by provider filters i..e type and provider
 * <li>Search recommendation by code
 * <li>Update or create a recommendation
 * <li>Filtered list of recommendations
 * <li>Update recommendation status and reason
 * </ul>
 */

public interface IRecommendationMao {
    
    /** Get all recommendation types */
    List<RecommendationTypeVO> getRecommendationTypes();

    /** Get all recommendation providers*/
    List<RecommendationProviderVO> getRecommendationProviders();

    /** Get all subscribed to recommendations*/
    List<RecommendationSubscriptionVO> getRecommendationSubscriptions();

    /** Get recommendation provider by provider filters*/
    List<RecommendationProviderVO> getRecommendationProvidersByProviderFilters(String recommendationType, String provider);
    
    /** Recommendation by code*/
    PriceRecommendationVO getRecommendationByIdentifier(String code);

    /** Update or create a recommendation*/
    void create(PriceRecommendationVO recommendation);

    /** Update ACTIVE recommendation status and reason*/
    boolean updateActiveRecommendation(PriceRecommendationVO recommendation, String recommendationStatus);

    /** List all recommendation based on provided filters 
     * @param noOfResults 
     * @param start 
     * @param identifiers */
    List<PriceRecommendationVO> getFilteredRecommendations(String channelCode, String channelProductId, String recommendationStatus,
            String recommendationProvider, DateRange createdDateRange, DateRange expiryTime, int start, int noOfResults);

    /** Get subscriptions by recommendation type 
     * @param recommendationProvider */
    List<RecommendationSubscriptionVO> getFilteredRecommendationSubscriptions(String recommendationProvider);

    /** Add subscription */
    void addSubscription(RecommendationSubscriptionVO subscription);

    /** Remove subscription */
    void removeSubscription(String recommendationType, String recommendationProvider);

    /** Update recommendation status and reason*/
    boolean updateRecommendation(String recommendationIdentifier, String recommendationStatus, String oldRecommendationStatus, String reason);

    List<PriceRecommendationVO> getRecommendationsByIdentifiers(List<String> identifiers);

    /** Count of active subscriptions for tenant */
    int getActiveRecommendationsCount();

}
