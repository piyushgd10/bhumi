/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 19, 2015
 *  @author akshay
 */
package com.uniware.mao.recommendation.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.WriteResult;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.RecommendationProviderVO;
import com.uniware.core.vo.RecommendationSubscriptionVO;
import com.uniware.core.vo.RecommendationTypeVO;
import com.uniware.core.vo.PriceRecommendationVO;
import com.uniware.core.vo.PriceRecommendationVO.RecommendationStatus;
import com.uniware.mao.recommendation.IRecommendationMao;

@Repository
public class RecommendationMaoImpl implements IRecommendationMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commomMongo;
    
    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations tenantSpecificMongo;

    @Override
    public List<RecommendationTypeVO> getRecommendationTypes() {
        return commomMongo.find(new Query(), RecommendationTypeVO.class);
    }

    @Override
    public List<RecommendationProviderVO> getRecommendationProviders() {
        return commomMongo.find(new Query(), RecommendationProviderVO.class);
    }

    @Override
    public List<RecommendationProviderVO> getRecommendationProvidersByProviderFilters(String recommendationType, String provider) {
        Criteria criteria = new Criteria();
        if (StringUtils.isNotBlank(recommendationType)) {
            criteria.and("recommendationType").is(recommendationType);
        }
        if (StringUtils.isNotBlank(provider)) {
            criteria.and("provider").is(provider);
        }
        return commomMongo.find(new Query(criteria), RecommendationProviderVO.class);
    }

    @Override
    public List<RecommendationSubscriptionVO> getRecommendationSubscriptions() {
        return tenantSpecificMongo.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), RecommendationSubscriptionVO.class);
    }

    @Override
    public PriceRecommendationVO getRecommendationByIdentifier(String identifier) {
        return tenantSpecificMongo.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("_id").is(identifier)), PriceRecommendationVO.class);
    }

    @Override
    public boolean updateActiveRecommendation(PriceRecommendationVO recommendation, String recommendationStatus) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("_id").is(recommendation.getId()).and("recommendationStatus").
                is(RecommendationStatus.ACTIVE.name()).and("expiryTime").gt(DateUtils.getCurrentTime());
        Update recommendationUpdate = new Update();
        recommendationUpdate.set("recommendationStatus", recommendationStatus);
        recommendationUpdate.set("closureReason", recommendation.getClosureReason());
        recommendationUpdate.set("closedBy", recommendation.getClosedBy());
        recommendationUpdate.set("updated", DateUtils.getCurrentTime());
        WriteResult updateResult = tenantSpecificMongo.updateFirst(new Query(criteria), recommendationUpdate, PriceRecommendationVO.class);
        return updateResult.getN() > 0;
    }
    
    @Override
    public boolean updateRecommendation(String recommendationIdentifier, String recommendationStatus, String oldRecommendationStatus, String reason) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("recommendationStatus").
                is(oldRecommendationStatus).and("_id").is(recommendationIdentifier);
        Update recommendationUpdate = new Update();
        recommendationUpdate.set("recommendationStatus", PriceRecommendationVO.RecommendationStatus.valueOf(recommendationStatus).name());
        recommendationUpdate.set("closureReason", reason);
        recommendationUpdate.set("updated", DateUtils.getCurrentTime());
        recommendationUpdate.set("closedBy", "SYSTEM");
        WriteResult updateResult = tenantSpecificMongo.updateFirst(new Query(criteria), recommendationUpdate, PriceRecommendationVO.class);
        return updateResult.getN() > 0;
    }
    
    @Override
    public List<PriceRecommendationVO> getRecommendationsByIdentifiers(List<String> identifiers) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("_id").in(identifiers);
        return tenantSpecificMongo.find(new Query(criteria), PriceRecommendationVO.class);
    }

    @Override
    public List<PriceRecommendationVO> getFilteredRecommendations(String channelCode, String channelProductId, String recommendationStatus,
             String recommendationProvider, DateRange createdDateRange, DateRange expiryTime, int start, int noOfResults) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode());
        if (StringUtils.isNotBlank(channelCode)) {
            criteria.and("channelCode").is(channelCode);
        }
        if (StringUtils.isNotBlank(channelProductId)) {
            criteria.and("channelProductId").is(channelProductId);
        }
        if (StringUtils.isNotBlank(recommendationStatus)) {
            criteria.and("recommendationStatus").is(recommendationStatus);
        }
        if (StringUtils.isNotBlank(recommendationProvider)) {
            criteria.and("recommendationProvider").is(recommendationProvider);
        }
        if (createdDateRange != null) {
            criteria.and("created").gte(createdDateRange.getStart()).lte(createdDateRange.getEnd());
        }
        if (expiryTime != null) {
            criteria.and("expiryTime").gte(expiryTime.getStart()).lte(expiryTime.getEnd());
        }
        Query recommendationQuery = new Query(criteria).with(new Sort(Direction.DESC, "created")).skip(start).limit(noOfResults);
        return tenantSpecificMongo.find(recommendationQuery, PriceRecommendationVO.class);
    }

    @Override
    public void create(PriceRecommendationVO recommendation) {
        recommendation.setTenantCode(UserContext.current().getTenant().getCode());
        tenantSpecificMongo.save(recommendation);
    }

    @Override
    public int getActiveRecommendationsCount() {
        return (int) tenantSpecificMongo.count(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("recommendationStatus").is(RecommendationStatus.ACTIVE)), PriceRecommendationVO.class);
    }

    @Override
    public List<RecommendationSubscriptionVO> getFilteredRecommendationSubscriptions(String recommendationProvider) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode());
        if (StringUtils.isNotBlank(recommendationProvider)) {
            criteria.and("recommendationProvider").is(recommendationProvider);
        }
        return tenantSpecificMongo.find(new Query(criteria), RecommendationSubscriptionVO.class);
    }

    @Override
    public void addSubscription(RecommendationSubscriptionVO subscription) {
        subscription.setTenantCode(UserContext.current().getTenant().getCode()); //TODO
        tenantSpecificMongo.save(subscription);
    }

    @Override
    public void removeSubscription(String recommendationType, String recommendationProvider) {
        tenantSpecificMongo.remove(new Query(
                Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("recommendationType").is(recommendationType).and("recommendationProvider").is(
                        recommendationProvider)
        ), RecommendationSubscriptionVO.class);
    }

}
