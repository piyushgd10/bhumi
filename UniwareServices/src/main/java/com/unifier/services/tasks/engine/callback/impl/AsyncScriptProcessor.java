/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 14-Oct-2014
 *  @author harsh
 */
package com.unifier.services.tasks.engine.callback.impl;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.google.gson.Gson;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.JobResult;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.ThreadContextUtils;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.IResumable;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

public class AsyncScriptProcessor implements IJobWorker {

    private static final Logger LOG = LoggerFactory.getLogger(AsyncScriptProcessor.class);

    @Override
    public JobResult execute(final ApplicationContext applicationContext, JobDataMap jobDataMap, final JobResult jobResult) {
        final IAsyncScriptService asyncScriptTaskService = applicationContext.getBean(IAsyncScriptService.class);
        for (final AsyncScriptInstruction task : asyncScriptTaskService.getAllScheduledAsyncScriptInstructions()) {
            if (task.getScheduledTime().before(DateUtils.getCurrentTime())) {
                Tenant tenant = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenantByCode(task.getTenantCode());
                UserContext.current().setTenant(tenant);
                UserContext.current().setThreadInfo(tenant.getCode());
                long startTime = System.currentTimeMillis();
                LOG.info("Started Task:{}", task.getName());
                JobResult result = new JobResult();
                try {
                    task.setStatusCode(AsyncScriptInstruction.StatusCode.RUNNING.name());
                    asyncScriptTaskService.updateInstruction(task);
                    Class klass = null;
                    try {
                        klass = Class.forName(task.getCallback());
                    } catch (ClassNotFoundException cnfe) {

                    }
                    IResumable service = (IResumable) applicationContext.getBean(klass);
                    service.resume(task);
                    jobResult.setMessage("Instruction completed successfully");
                } catch (Throwable e) {
                    jobResult.setMessage("Exception while executing task - " + e.getMessage());
                    LOG.error("Error while executing task:" + task.getName(), e);
                } finally {
                    task.setStatusCode(AsyncScriptInstruction.StatusCode.COMPLETE.name());
                    task.setResult(new Gson().toJson(result));
                    asyncScriptTaskService.updateInstruction(task);
                }
                LOG.info("Completed Task:{} in {} secs", task.getName(), (System.currentTimeMillis() - startTime) / 1000);
            }
        }
        jobResult.setMessage("Task completed successfully");
        return jobResult;
    }
}
