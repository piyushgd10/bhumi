/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Oct-2014
 *  @author harsh
 */
package com.unifier.services.tasks.engine.callback;

import com.unifier.core.entity.AsyncScriptInstruction;

public interface IResumable {

    void resume(AsyncScriptInstruction task);

}
