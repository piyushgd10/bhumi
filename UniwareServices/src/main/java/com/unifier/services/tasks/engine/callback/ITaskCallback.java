/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Sep-2014
 *  @author harsh
 */
package com.unifier.services.tasks.engine.callback;

import com.unifier.core.entity.AsyncScriptInstruction;

public interface ITaskCallback {
    public void handle(AsyncScriptInstruction task);

}
