/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2014
 *  @author harsh
 */
package com.unifier.services.tasks.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.core.entity.AsyncScriptInstruction.StatusCode;
import com.unifier.core.entity.AsyncScriptInstructionParameter;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.tasks.IAsyncScriptInstructionDao;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.tasks.IAsyncScriptService;
import com.unifier.services.tasks.engine.callback.impl.AsyncScriptProcessor;
import com.uniware.core.entity.Channel;

@Service("asyncScriptService")
public class AsyncScriptServiceImpl implements IAsyncScriptService {

    private static final Logger        LOG = LoggerFactory.getLogger(AsyncScriptServiceImpl.class);

    @Autowired
    private IAsyncScriptInstructionDao asyncScriptInstructionDao;

    @Override
    public void addAsyncScriptInstruction(AsyncScriptInstruction task) {
        persistTask(task);
    }

    @Transactional
    public AsyncScriptInstruction persistTask(AsyncScriptInstruction task) {
        return asyncScriptInstructionDao.addTask(task);
    }

    @SuppressWarnings({ "rawtypes" })
    @Override
    public boolean scheduleScriptTask(String scriptName, Channel channel, ScriptExecutionContext ctx, Class callbackClass, String taskName) {
        AsyncScriptInstruction task = new AsyncScriptInstruction();
        LOG.info("test task 0 - {}", task.toString());
        Map<String, Object> contextVars = ctx.getScheduleScriptContext();
        task.setName(taskName);
        task.setCreated(DateUtils.getCurrentTime());
        task.setScheduledTime(DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.SECOND, Integer.parseInt((String) contextVars.get("delayInSeconds"))));
        task.setStatusCode(StatusCode.SCHEDULED.name());
        task.setScriptName(scriptName);
        task.setTaskClass(AsyncScriptProcessor.class.getName());
        for (Map.Entry<String, Object> entry : contextVars.entrySet()) {
            task.addContextVariable(entry.getKey(), entry.getValue());
        }
        task.setInstructionName((String) contextVars.get("instructionName"));
        task.setCallback(callbackClass.getName());
        addAsyncScriptInstruction(task);
        return true;
    }

    @Override
    public List<AsyncScriptInstruction> getAllScheduledAsyncScriptInstructions() {
        return asyncScriptInstructionDao.getAllScheduledAsyncScriptTasks();
    }

    @Override
    @Transactional
    public List<AsyncScriptInstruction> getAllScheduledAsyncScriptInstructions(String callBack) {
        return asyncScriptInstructionDao.getAllScheduledAsyncScriptTasks(callBack);
    }

    @Override
    @Transactional
    public AsyncScriptInstruction getPendingAsyncScriptInstruction(String key, String value, String callback) {
        for (final AsyncScriptInstruction task : getAllScheduledAsyncScriptInstructions(callback)) {
            for (AsyncScriptInstructionParameter param : task.getInstructionParameters()) {
                if (param.getName().equals(key) && param.getValue().equals(value)) {
                    return task;
                }
            }
        }
        return null;
    }

    @Override
    @Transactional
    public AsyncScriptInstruction updateInstruction(AsyncScriptInstruction task) {
        return asyncScriptInstructionDao.updateTask(task);
    }

}
