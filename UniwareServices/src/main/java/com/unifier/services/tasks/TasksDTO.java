/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 21, 2012
 *  @author praveeng
 */
package com.unifier.services.tasks;

import java.util.HashMap;
import java.util.Map;

public class TasksDTO {

    private Map<String, TaskDTO> recurrentTasks = new HashMap<>();
    private Map<String, TaskDTO> runtimeTasks   = new HashMap<>();

    /**
     * @return the recurrentTasks
     */
    public Map<String, TaskDTO> getRecurrentTasks() {
        return recurrentTasks;
    }

    /**
     * @param recurrentTasks the recurrentTasks to set
     */
    public void setRecurrentTasks(Map<String, TaskDTO> recurrentTasks) {
        this.recurrentTasks = recurrentTasks;
    }

    /**
     * @return the runtimeTasks
     */
    public Map<String, TaskDTO> getRuntimeTasks() {
        return runtimeTasks;
    }

    /**
     * @param runtimeTasks the runtimeTasks to set
     */
    public void setRuntimeTasks(Map<String, TaskDTO> runtimeTasks) {
        this.runtimeTasks = runtimeTasks;
    }

}
