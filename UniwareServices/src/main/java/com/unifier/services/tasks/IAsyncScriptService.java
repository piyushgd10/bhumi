/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Aug-2014
 *  @author harsh
 */
package com.unifier.services.tasks;

import java.util.List;

import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.entity.Channel;

public interface IAsyncScriptService {

    void addAsyncScriptInstruction(AsyncScriptInstruction instruction);

    @SuppressWarnings({ "rawtypes" })
    boolean scheduleScriptTask(String scriptName, Channel channel, ScriptExecutionContext ctx, Class callbackClass, String taskName);

    List<AsyncScriptInstruction> getAllScheduledAsyncScriptInstructions();

    List<AsyncScriptInstruction> getAllScheduledAsyncScriptInstructions(String callBack);

    AsyncScriptInstruction getPendingAsyncScriptInstruction(String key, String value, String callback);

    AsyncScriptInstruction updateInstruction(AsyncScriptInstruction instruction);

}
