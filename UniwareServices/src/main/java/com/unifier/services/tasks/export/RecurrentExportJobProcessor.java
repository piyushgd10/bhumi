/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Jun-2012
 *  @author praveeng
 */
package com.unifier.services.tasks.export;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.JobResult;
import com.unifier.services.export.IExportService;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.job.JobConstants;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tasks.JobUtils;

public class RecurrentExportJobProcessor implements IJobWorker {

    private static final Logger LOG             = LoggerFactory.getLogger(RecurrentExportJobProcessor.class);

    public static final String  PARAM_FREQUENCY = "frequency";

    @Override public JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult) {
        IExportService exportService = applicationContext.getBean(IExportService.class);
        List<ExportJob> exportJobs = exportService.getExportJobByFrequency(jobDataMap.getString(PARAM_FREQUENCY));

        List<ExportJob> exportJobsToRun = new ArrayList<>();
        for (ExportJob exportJob : exportJobs) {
            if (JobUtils.isItAGoodTimeToRun(exportJob.getCronExpression(), jobDataMap.getString(JobConstants.TRIGGER_CRON_EXPRESSION))) {
                exportJobsToRun.add(exportJob);
            }
        }
        String message = exportJobsToRun.isEmpty() ? "No export jobs to schedule" : "Export jobs:" + exportJobsToRun.toString() + " scheduled successfully";
        for (ExportJob exportJob : exportJobsToRun) {
            if (exportJob.getFacilityCode() != null) {
                UserContext.current().setFacility(CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(exportJob.getFacilityCode()));
            } else {
                UserContext.current().setFacility(null);
            }
            exportService.submitExportJob(exportJob.getId());
        }
        LOG.info(message);
        jobResult.setMessage(message);
        return jobResult;
    }
}
