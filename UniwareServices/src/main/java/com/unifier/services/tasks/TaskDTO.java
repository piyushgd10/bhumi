/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 28, 2012
 *  @author praveeng
 */
package com.unifier.services.tasks;

import java.util.Date;

import com.unifier.core.entity.JobResult;

/**
 * @author praveeng
 */
public class TaskDTO {

    public enum Type {
        RUNTIME,
        RECURRENT
    }

    private String    id;
    private String    name;
    private String    type;
    private String    delay;
    private boolean   enabled;
    private String    lastExecResult;
    private Date      lastExecTime;
    private String    currentStatus;
    private JobResult taskResult;
    private String    detailedMessageTemplate;
    private float     percentageCompleted;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the delay
     */
    public String getDelay() {
        return delay;
    }

    /**
     * @param delay the delay to set
     */
    public void setDelay(String delay) {
        this.delay = delay;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the lastExecResult
     */
    public String getLastExecResult() {
        return lastExecResult;
    }

    /**
     * @param lastExecResult the lastExecResult to set
     */
    public void setLastExecResult(String lastExecResult) {
        this.lastExecResult = lastExecResult;
    }

    /**
     * @return the lastExecTime
     */
    public Date getLastExecTime() {
        return lastExecTime;
    }

    /**
     * @param lastExecTime the lastExecTime to set
     */
    public void setLastExecTime(Date lastExecTime) {
        this.lastExecTime = lastExecTime;
    }

    /**
     * @return the currentStatus
     */
    public String getCurrentStatus() {
        return currentStatus;
    }

    /**
     * @param currentStatus the currentStatus to set
     */
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     * @return the taskResult
     */
    public JobResult getTaskResult() {
        return taskResult;
    }

    /**
     * @param taskResult the taskResult to set
     */
    public void setTaskResult(JobResult taskResult) {
        this.taskResult = taskResult;
    }

    /**
     * @return the detailedMessageTemplate
     */
    public String getDetailedMessageTemplate() {
        return detailedMessageTemplate;
    }

    /**
     * @param detailedMessageTemplate the detailedMessageTemplate to set
     */
    public void setDetailedMessageTemplate(String detailedMessageTemplate) {
        this.detailedMessageTemplate = detailedMessageTemplate;
    }

    /**
     * @return the percentageCompleted
     */
    public float getPercentageCompleted() {
        return percentageCompleted;
    }

    /**
     * @param percentageCompleted the percentageCompleted to set
     */
    public void setPercentageCompleted(float percentageCompleted) {
        this.percentageCompleted = percentageCompleted;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

}
