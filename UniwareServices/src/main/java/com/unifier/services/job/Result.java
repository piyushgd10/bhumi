/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 24/08/15
*  @author sunny
*/

package com.unifier.services.job;

import java.util.HashMap;
import java.util.Map;

public class Result {

    public enum Status {
        SUCCESS,
        FAILED,
        INTERRUPTED
    }

    private Status              status;
    private String              errorMessage;
    private Map<String, Object> resultItems = new HashMap<>();

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Map<String, Object> getResultItems() {
        return resultItems;
    }

    public void setResultItems(Map<String, Object> resultItems) {
        this.resultItems = resultItems;
    }

    public void addResultItem(String name, Object resultItem) {
        this.resultItems.put(name, resultItem);
    }
}
