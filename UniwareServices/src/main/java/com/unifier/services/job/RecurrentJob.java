/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 9/25/15 1:12 PM
 * @author amdalal
 */

package com.unifier.services.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.Job;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.email.IEmailService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RecurrentJob extends AbstractJob {

    private static final Logger LOG = LoggerFactory.getLogger(RecurrentJob.class);

    @Override public String getName() {
        return this.getClass().getName();
    }

    @Override protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        JobDetail jobDetail = context.getJobDetail();
        LOG.info("Starting job: {}", jobDetail.getKey().getName());
        long startTime = System.currentTimeMillis();
        ApplicationContext applicationContext;
        try {
            applicationContext = (ApplicationContext) context.getScheduler().getContext().get(JobConstants.APPLICATION_CONTEXT_KEY);
        } catch (SchedulerException e) {
            LOG.error("Failed to get application context from scheduler context", e);
            throw new JobExecutionException(e);
        }
        IJobService jobService = applicationContext.getBean(IJobService.class);

        List<Tenant> tenants;
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        if (jobDataMap.containsKey("tenantCode")) {
            Tenant tenant = CacheManager.getInstance().getCache(TenantCache.class).getTenantByCode(jobDataMap.getString("tenantCode"));
            UserContext.current().setTenant(tenant);
            tenants = Arrays.asList(tenant);
        } else {
            tenants = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenants();
        }
        Job job = jobService.getJobByCode(jobDetail.getKey().getName());
        List<Future<?>> jobFutures = new ArrayList<>(tenants.size());
        for (Tenant tenant : tenants) {
            UserContext.current().setTenant(tenant);
            ITenantProfileService tenantProfileService = applicationContext.getBean(ITenantProfileService.class);
            TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
            if ((tenantProfile != null) && !StringUtils.equalsAny(tenantProfile.getPaymentStatus(), TenantProfileVO.PaymentStatus.LIMITED_SERVICES,
                    TenantProfileVO.PaymentStatus.BLOCKED) && (CollectionUtils.isEmpty(job.getRestrictedProductTypes()) || !job.getRestrictedProductTypes().contains(
                    tenantProfile.getTenantType().name()))) {
                try {
                    switch (job.getExecutionLevel()) {
                        case TENANT:
                            jobFutures.add(submitJob(context, applicationContext));
                            break;
                        case FACILITY:
                            for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
                                if (CollectionUtils.isEmpty(job.getRestrictedFacilityTypes()) || !job.getRestrictedFacilityTypes().contains(facility.getType())) {
                                    UserContext.current().setFacility(facility);
                                    jobFutures.add(submitJob(context, applicationContext));
                                } else {
                                    LOG.warn("Skipping for facility: {} as job is not applicable for facility type : {}", facility.getName(), facility.getType());
                                }
                            }
                    }
                } catch (Throwable e) {
                    job.setSeverity(job.getSeverity() + 1);
                    LOG.error("Exception while executing job:" + jobDetail.getKey().getName(), e);
                    throw new JobExecutionException(e);
                } finally {
                    if (job.getSeverity() != IJob.Severity.NONE.level()) {
                        job.setSeverity(IJob.Severity.NONE.level());
                        sendTaskResultEmail(job, applicationContext);
                    }
                    UserContext.destroy();
                }
            } else {
                LOG.warn("Skipping tenant {} as account is blocked or job not applicable to tenant type", tenant.getCode());
            }
        }

        for (Future<?> jobFuture : jobFutures) {
            try {
                jobFuture.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        job.setLastExecTime(DateUtils.getCurrentTime());
        LOG.info("Completed job:{} in {} ms", jobDetail.getKey().getName(), (System.currentTimeMillis() - startTime));
    }

    private void sendTaskResultEmail(final Job job, ApplicationContext applicationContext) {
        if (StringUtils.isNotEmpty(job.getNotificationEmailTemplate()) && StringUtils.isNotEmpty(job.getNotificationEmail())) {
            List<String> recipients = new ArrayList<>();
            recipients.addAll(StringUtils.split(job.getNotificationEmail()));
            EmailMessage message = new EmailMessage(recipients, job.getNotificationEmailTemplate());
            message.addTemplateParam("task", job);
            IEmailService emailService = applicationContext.getBean(IEmailService.class);
            emailService.send(message);
        }
    }

    private Future<?> submitJob(JobExecutionContext context, ApplicationContext applicationContext) throws JobExecutionException {
        JobEvent jobEvent = new JobEvent();
        jobEvent.setJobCode(context.getJobDetail().getKey().getName());
        jobEvent.setRequestTimestamp(DateUtils.getCurrentTime());
        jobEvent.setJobDataMap(context.getJobDetail().getJobDataMap());
        return applicationContext.getBean(IJobService.class).enqueueJob(jobEvent);
    }
}
