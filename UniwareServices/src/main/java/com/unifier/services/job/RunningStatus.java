/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 24/08/15
*  @author sunny
*/

package com.unifier.services.job;

public class RunningStatus {

    private long   mileStoneCount = 100;
    private long   currentMileStone;
    private String currentStatus;

    public long getMileStoneCount() {
        return mileStoneCount;
    }

    public void setMileStoneCount(long mileStoneCount) {
        this.mileStoneCount = mileStoneCount;
    }

    public long getCurrentMileStone() {
        return currentMileStone;
    }

    public void setCurrentMileStone(long currentMileStone) {
        this.currentMileStone = currentMileStone;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public void setMileStone(String currentStatus, int milestones) {
        this.currentStatus = currentStatus;
        currentMileStone += milestones;
    }

    public void setMileStone(String currentStatus) {
        this.currentStatus = currentStatus;
        currentMileStone++;
    }

}