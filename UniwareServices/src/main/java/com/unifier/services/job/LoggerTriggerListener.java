/*
*  @version     1.0, 02/09/16
*  @author sunny
*/

package com.unifier.services.job;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.utils.DateUtils;

public class LoggerTriggerListener implements TriggerListener {

    private static final Logger LOG                      = LoggerFactory.getLogger(LoggerTriggerListener.class);
    private static final String TRIGGER_FIRED_MESSAGE    = "Trigger {}.{} fired at: {}";
    private static final String TRIGGER_MISFIRED_MESSAGE = "Trigger {}.{} misfired at: {}.  Should have fired at: {}";

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public void triggerFired(Trigger trigger, JobExecutionContext context) {
        LOG.info(TRIGGER_FIRED_MESSAGE, trigger.getJobKey().getName(), trigger.getJobKey().getGroup(), DateUtils.getCurrentTime());
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
        return false;
    }

    @Override
    public void triggerMisfired(Trigger trigger) {
        LOG.info(TRIGGER_MISFIRED_MESSAGE, trigger.getJobKey().getName(), trigger.getJobKey().getGroup(), DateUtils.getCurrentTime(), trigger.getNextFireTime());
    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext context, Trigger.CompletedExecutionInstruction triggerInstructionCode) {

    }
}
