/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 25, 2015
 *  @author amit
 */
package com.unifier.services.job;

import org.quartz.JobDataMap;
import org.springframework.context.ApplicationContext;

import com.unifier.core.entity.JobResult;

/**
 * @author singla
 */
public interface IJobWorker {

    JobResult execute(ApplicationContext applicationContext, JobDataMap jobDataMap, JobResult jobResult);
}
