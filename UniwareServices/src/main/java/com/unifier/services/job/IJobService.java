/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Sep 25, 2015
 *  @author amit
 */
package com.unifier.services.job;

import com.unifier.core.api.tasks.GetTaskRequest;
import com.unifier.core.api.tasks.GetTaskResponse;
import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.core.entity.Job;
import java.util.List;
import java.util.concurrent.Future;
import org.quartz.SchedulerException;

public interface IJobService {

    void stopScheduler();

    void loadJobs();

    void processMessage(JobEvent event);

    Future<?> enqueueJob(JobEvent jobEvent);

    void scheduleJobs() throws SchedulerException;

    void initScheduler() throws SchedulerException;

    void runJob(String jobCode) throws SchedulerException;

    Job getJobByCode(String jobCode);

    void saveJob(Job Job);

    //List<JobExecutionContext> getRuntimeJobs();

    List<Job> getGlobalRecurrentJobs();

//    JobDetail getJobDetailFromScheduler(String jobCode) throws SchedulerException;

    List<Job> getRecurrentJobsForTenant();

    GetTaskResponse getJob(GetTaskRequest request);

    List<UserSearchDTO> getSubscribedUsers(String exportJobId);

//    List<Job> getRecurrentJobsForTenant(String tenantCode);
//
//    void removeAllJobsForTenant();

//    void deleteJobs() throws SchedulerException;

}