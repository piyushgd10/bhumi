/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Sep 25, 2015
 *  @author amit
 */
package com.unifier.services.job;

public class JobConstants {

    public static final String APPLICATION_CONTEXT_KEY = "applicationContext";
    public static final String TENANT_CODE_KEY         = "tenantCode";
    public static final String JOB_CODE_KEY            = "jobCode";
    public static final String TRIGGER_CRON_EXPRESSION = "triggerCronExpression";
    public static final String MULTI_TRIGGER_SEPARATOR = "\\|";
    public static final String FACILITY_ID_KEY         = "facilityId";
    public static final String JOB_EXECUTION_LEVEL_KEY = "executionLevel";
}
