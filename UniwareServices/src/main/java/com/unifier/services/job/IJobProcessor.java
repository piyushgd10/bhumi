/*
*  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
*  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*  
*  @version     1.0, 24/08/15
*  @author sunny
*/

package com.unifier.services.job;

public interface IJobProcessor {

    JobStatus execute() throws Exception;
}