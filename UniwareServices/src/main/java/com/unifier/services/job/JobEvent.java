/*
*  @version     1.0, 06/09/16
*  @author sunny
*/

package com.unifier.services.job;

import java.util.Date;

import org.quartz.JobDataMap;

public class JobEvent {

    private String     jobCode;
    private JobDataMap jobDataMap;
    private Date       requestTimestamp;

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public JobDataMap getJobDataMap() {
        return jobDataMap;
    }

    public void setJobDataMap(JobDataMap jobDataMap) {
        this.jobDataMap = jobDataMap;
    }

    public Date getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Date requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public String toString() {
        return "JobEvent{" + "jobCode='" + jobCode + '\'' + ", jobDataMap=" + jobDataMap + ", requestTimestamp=" + requestTimestamp + '}';
    }
}
