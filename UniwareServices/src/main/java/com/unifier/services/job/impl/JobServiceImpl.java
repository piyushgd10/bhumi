package com.unifier.services.job.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.JobDetailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.annotation.Level;
import com.unifier.core.api.tasks.GetTaskRequest;
import com.unifier.core.api.tasks.GetTaskResponse;
import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ExportSubscriber;
import com.unifier.core.entity.Job;
import com.unifier.core.entity.JobResult;
import com.unifier.core.entity.User;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.mao.job.IJobMao;
import com.unifier.services.export.IExportService;
import com.unifier.services.job.IJobResultService;
import com.unifier.services.job.IJobService;
import com.unifier.services.job.IJobWorker;
import com.unifier.services.job.JobConstants;
import com.unifier.services.job.JobEvent;
import com.unifier.services.job.LoggerTriggerListener;
import com.unifier.services.job.RecurrentJob;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.concurrent.ContextAwareExecutor;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Tenant;
import com.uniware.core.locking.ILockingService;
import com.uniware.core.locking.Namespace;
import com.uniware.core.utils.UserContext;
import com.uniware.services.messaging.jms.ActiveMQConnector;

@Service(value = "jobService")
public class JobServiceImpl implements IJobService, ApplicationContextAware {

    private static final Logger         LOG          = LoggerFactory.getLogger(JobServiceImpl.class);
    private static final String         SERVICE_NAME = "jobService";

    private ApplicationContext          applicationContext;

    @Autowired
    private ILockingService             lockingService;

    @Autowired
    private IJobResultService           jobResultService;

    @Autowired
    private SchedulerFactoryBean        schedulerFactoryBean;

    @Autowired
    private IJobMao                     jobMao;

    @Autowired
    private IUsersService               usersService;

    @Autowired
    private IExportService              exportService;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Autowired
    @Qualifier("jobsConnector")
    private ActiveMQConnector           jobsActiveMQConnector;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void processMessage(JobEvent event) {
        long start = System.currentTimeMillis();
        LOG.info("Processing event {}", event);
        Job job = getJobByCode(event.getJobCode());
        JobResult jobResult = jobResultService.get(job.getCode(), job.getExecutionLevel());
        if (jobResult == null) {
            jobResult = new JobResult();
            jobResult.setJobCode(job.getCode());
        }
        Lock lock = lockingService.getLock(Namespace.JOB, job.getCode(), UserContext.current().getFacility() != null ? Level.FACILITY : Level.TENANT);
        boolean lockAcquired = false;
        try {
            lockAcquired = lock.tryLock();
            if (lockAcquired) {
                IJobWorker worker = (IJobWorker) Class.forName(job.getTaskClass()).newInstance();
                Map<String, String> data = new HashMap<>();
                for (String key : event.getJobDataMap().getKeys()) {
                    data.put(key, event.getJobDataMap().getString(key));
                }
                LOG.info("Running job: {} with data: {}", job.getCode(), data);
                jobResult = worker.execute(applicationContext, event.getJobDataMap(), jobResult);
                jobResult.setStatusCode(JobResult.StatusCode.SUCCESSFUL);
            } else {
                LOG.warn("Job already running: {}", job.getCode());
            }
        } catch (Exception e) {
            LOG.error("Error processing job event", e);
            jobResult.setStatusCode(JobResult.StatusCode.FAILED);
        } finally {
            if (lockAcquired) {
                lock.unlock();
            }
            jobResult.setLastExecTime(DateUtils.getCurrentTime());
            jobResultService.saveJob(jobResult);
        }
        LOG.info("Done processing event {} in {} ms", event, System.currentTimeMillis() - start);
    }

    @Override
    public void stopScheduler(){
        try {
            schedulerFactoryBean.destroy();
        } catch (SchedulerException e) {
        }
    }

    @Override
    public void loadJobs() {
        startScheduler();
        try {
            LOG.info("Loading recurrent jobs");
            scheduleJobs();
            LOG.info("Done loading recurrent jobs");
        } catch (SchedulerException e) {
            LOG.error("Failed to load jobs", e);
            throw new RuntimeException(e);
        }
        try {
            initScheduler();
        } catch (SchedulerException e) {
            LOG.error("Failed to initialize scheduler", e);
            throw new RuntimeException(e);
        }
    }

    @Transactional
    private void startScheduler() {
        LOG.info("Starting Quartz Scheduler..");
        try {
            schedulerFactoryBean.getScheduler().start();
            schedulerFactoryBean.getScheduler().getListenerManager().addTriggerListener(new LoggerTriggerListener());
            LOG.info("Done Starting Quartz Scheduler");
        } catch (SchedulerException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Future<?> enqueueJob(final JobEvent jobEvent) {
        ContextAwareExecutor jobExecutor = executorFactory.getExecutor(SERVICE_NAME);
        LOG.info("Submitting job {} to executor. Queue size: {}, active count: {}", jobEvent.getJobCode(), jobExecutor.getQueue().size(), jobExecutor.getActiveCount());
        return jobExecutor.submit(new Runnable() {
            @Override
            public void run() {
                long queuingStartTime = System.currentTimeMillis();
                String tenantCode = UserContext.current().getTenant().getCode();
                String facilityCode = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode();
                LOG.info("Queuing job: {} for tenant: {}, facility: {}", jobEvent.getJobCode(), tenantCode, facilityCode);
                jobsActiveMQConnector.produceContextAwareMessage(MessagingConstants.Queue.JOB_QUEUE, jobEvent);
                LOG.info("Queued job:{} in {} ms", jobEvent.getJobCode(), (System.currentTimeMillis() - queuingStartTime));
            }
        });
    }

    @Override
    @Transactional
    public void scheduleJobs() throws SchedulerException {
        schedulerFactoryBean.getScheduler().getContext().put(JobConstants.APPLICATION_CONTEXT_KEY, applicationContext);
        for (Job job : jobMao.getGlobalRecurrentJobs()) {
            doScheduleJob(job);
        }

        // If this is an enterprise client, schedule any tenant specific jobs
        // XXX We may have to do this for professional clients as well
        List<Tenant> tenants = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenants();
        for (Tenant tenant : tenants) {
            if (Product.Type.ENTERPRISE.name().equals(tenant.getProduct().getCode())) {
                LOG.info("Loading specific jobs for {}", tenant.getCode());
                UserContext.current().setTenant(tenant);
                List<Job> jobs = jobMao.getRecurrentJobsForTenant();
                if (!jobs.isEmpty()) {
                    for (Job job : jobs) {
                        doScheduleJob(job);
                    }
                } else {
                    LOG.info("No tenant specific jobs present");
                }
                UserContext.destroy();
            }
        }
    }

    private void doScheduleJob(Job job) throws SchedulerException {
        if (job.getEndTime() != null && job.getEndTime().before(DateUtils.getCurrentTime())) {
            LOG.info("Skipped scheduling expired job: {}", job.getName());
            return;
        }
        LOG.info("Scheduling job: {}", job.getName());
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(RecurrentJob.class);
        prepareJobDetailFactoryBean(job, jobDetailFactoryBean);
        ((JobDetailImpl) jobDetailFactoryBean.getObject()).setKey(JobKey.jobKey(job.getCode(), job.getCode()));
        // trigger
        Set<Trigger> triggers = new HashSet<>(1);
        int counter = 0;
        for (String cronExpression : StringUtils.split(job.getCronExpression(), JobConstants.MULTI_TRIGGER_SEPARATOR)) {
            CronTriggerFactoryBean triggerFactoryBean = prepareTriggerFactoryBean(job, jobDetailFactoryBean, cronExpression);
            triggerFactoryBean.setName(job.getName() + "_trigger" + ++counter);
            try {
                triggerFactoryBean.afterPropertiesSet();
            } catch (ParseException e) {
                e.printStackTrace();
                throw new SchedulerException(e);
            }
            triggers.add(triggerFactoryBean.getObject());
        }
        schedulerFactoryBean.getScheduler().scheduleJob(jobDetailFactoryBean.getObject(), triggers, true);
        LOG.info("Done scheduling job: {}", job.getName());
    }

    @Override
    @Transactional
    public void initScheduler() throws SchedulerException {
        try {
            schedulerFactoryBean.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SchedulerException(e);
        }
    }

    private CronTriggerFactoryBean prepareTriggerFactoryBean(Job job, JobDetailFactoryBean jobDetailFactoryBean, String cronExpression) {
        CronTriggerFactoryBean triggerFactoryBean = new CronTriggerFactoryBean();
        triggerFactoryBean.setJobDetail(jobDetailFactoryBean.getObject());
        triggerFactoryBean.setGroup(job.getCode());
        triggerFactoryBean.setCronExpression(cronExpression);
        triggerFactoryBean.setMisfireInstruction(1);
        return triggerFactoryBean;
    }

    private void prepareJobDetailFactoryBean(Job job, JobDetailFactoryBean jobDetailFactoryBean) {
        jobDetailFactoryBean.setName(job.getCode());
        jobDetailFactoryBean.setDurability(true);
        jobDetailFactoryBean.setGroup(job.getCode());
        jobDetailFactoryBean.getJobDataMap().put(JobConstants.JOB_EXECUTION_LEVEL_KEY, job.getExecutionLevel().name());
        jobDetailFactoryBean.getJobDataMap().put(JobConstants.JOB_CODE_KEY, job.getCode());
        jobDetailFactoryBean.getJobDataMap().put(JobConstants.TRIGGER_CRON_EXPRESSION, job.getCronExpression());
        if (StringUtils.isNotBlank(job.getTenantCode())) {
            jobDetailFactoryBean.getJobDataMap().put(JobConstants.TENANT_CODE_KEY, job.getTenantCode());
        }
        if (job.getTaskParameters() != null) {
            for (Map.Entry<String, String> e : job.getTaskParameters().entrySet()) {
                jobDetailFactoryBean.getJobDataMap().put(e.getKey(), e.getValue());
            }
        }
        jobDetailFactoryBean.afterPropertiesSet();
    }

    @Override
    public void runJob(String jobCode) throws SchedulerException {
        Job job = getJobByCode(jobCode);
        JobEvent jobEvent = new JobEvent();
        jobEvent.setJobCode(job.getCode());
        jobEvent.setRequestTimestamp(DateUtils.getCurrentTime());
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(JobConstants.JOB_EXECUTION_LEVEL_KEY, job.getExecutionLevel().name());
        jobDataMap.put(JobConstants.JOB_CODE_KEY, job.getCode());
        jobDataMap.put(JobConstants.TRIGGER_CRON_EXPRESSION, job.getCronExpression());
        if (StringUtils.isNotBlank(job.getTenantCode())) {
            jobDataMap.put(JobConstants.TENANT_CODE_KEY, job.getTenantCode());
        }
        if (job.getTaskParameters() != null) {
            for (Map.Entry<String, String> e : job.getTaskParameters().entrySet()) {
                jobDataMap.put(e.getKey(), e.getValue());
            }
        }
        jobEvent.setJobDataMap(jobDataMap);
        processMessage(jobEvent);
    }

    @Override
    public Job getJobByCode(String jobCode) {
        return jobMao.getJobByCode(jobCode);
    }

    @Override
    public void saveJob(Job Job) {
        jobMao.saveJob(Job);
    }

    @Override
    public List<Job> getGlobalRecurrentJobs() {
        return jobMao.getGlobalRecurrentJobs();
    }

    @Override
    public List<Job> getRecurrentJobsForTenant() {
        return jobMao.getRecurrentJobsForTenant();
    }

    @Override
    public GetTaskResponse getJob(GetTaskRequest request) {
        GetTaskResponse response = new GetTaskResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Job job = jobMao.getJobByName(request.getTaskName());
            if (job == null) {
                context.addError(WsResponseCode.INVALID_REQUEST);
            } else {
                GetTaskResponse.GetTaskDTO dto = new GetTaskResponse.GetTaskDTO();
                dto.setName(job.getName());
                dto.setEnabled(job.isEnabled());
                dto.setCronExpression(job.getCronExpression());
                dto.setTaskClass(job.getTaskClass());
                for (Map.Entry<String, String> e : job.getTaskParameters().entrySet()) {
                    GetTaskResponse.TaskParameterDTO taskParameterDTO = new GetTaskResponse.TaskParameterDTO();
                    taskParameterDTO.setName(e.getKey());
                    taskParameterDTO.setValue(e.getValue());
                    dto.getTaskParameters().add(taskParameterDTO);
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public List<UserSearchDTO> getSubscribedUsers(String exportJobId) {
        List<UserSearchDTO> userDetails = new ArrayList<>();
        ExportSubscriber exportSubscriber = exportService.getExportSubscribers(exportJobId);
        for (String username : exportSubscriber.getUsernames()) {
            UserSearchDTO userData = new UserSearchDTO();
            User user = usersService.getUserByUsername(username);
            userData.setId(user.getId());
            userData.setName(user.getName());
            userData.setUserName(user.getUsername());
            userData.setLastLoginTime(user.getLastLoginTime());
            userDetails.add(userData);
        }
        return userDetails;
    }
}
