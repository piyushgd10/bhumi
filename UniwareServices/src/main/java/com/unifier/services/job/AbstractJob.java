/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 25, 2015
 *  @author amit
 */
package com.unifier.services.job;

import com.unifier.core.entity.JobResult;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author singla
 */
public abstract class AbstractJob extends QuartzJobBean implements IJob {

    private int id;
    private long mileStoneCount = 100;
    private long   currentMileStone;
    private String currentStatus;
    private JobResult jobResult = new JobResult();
    private boolean taskCompleted;
    private String  notificationEmail;
    private String  notificationEmailTemplate;

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#getTaskResult()
     */
    @Override
    public JobResult getJobResult() {
        return jobResult;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#setTaskResult(com.uniware.services.tasks.engine.TaskResult)
     */
    @Override
    public void setJobResult(JobResult jobResult) {
        this.taskCompleted = true;
        this.currentMileStone = mileStoneCount;
        this.jobResult = jobResult;
    }

    /**
     * @return
     */
    public float getPercentageComplete() {
        return ((float) (currentMileStone * 10000 / mileStoneCount)) / 100;
    }

    /**
     * @param currentStatus
     */
    public void setMileStone(String currentStatus) {
        this.currentStatus = currentStatus;
        currentMileStone++;
    }

    /**
     * @param currentStatus
     */
    public void setMileStone(String currentStatus, int milestones) {
        this.currentStatus = currentStatus;
        currentMileStone += milestones;
    }

    /**
     * @return
     */
    public String getCurrentStatus() {
        return currentStatus;
    }

    /**
     * @param mileStones
     */
    public void setMileStoneCount(long mileStones) {
        if (mileStones > 0) {
            mileStoneCount = mileStones;
        }
        currentMileStone = 0;
        taskCompleted = false;
        jobResult = new JobResult();
        currentStatus = "initializing...";
    }

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#isTaskCompleted()
     */
    @Override
    public boolean isTaskCompleted() {
        return taskCompleted;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#setCurrentStatus(java.lang.String)
     */
    @Override
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#getId()
     */
    @Override
    public int getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see com.uniware.services.tasks.engine.ITask#setId(int)
     */
    @Override
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getNotificationEmailTemplate() {
        return notificationEmailTemplate;
    }

    /**
     * @param notificationEmailTemplate
     */
    public void setNotificationEmailTemplate(String notificationEmailTemplate) {
        this.notificationEmailTemplate = notificationEmailTemplate;
    }

    /**
     * @param notificationEmail
     */
    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    /**
     * @return
     */
    public String getNotificationEmail() {
        return notificationEmail;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractJob other = (AbstractJob) obj;
        if (id != other.id)
            return false;
        return true;
    }

}