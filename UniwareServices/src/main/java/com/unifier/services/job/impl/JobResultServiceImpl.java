package com.unifier.services.job.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.annotation.Level;
import com.unifier.core.entity.JobResult;
import com.unifier.mao.jobresult.job.IJobResultMao;
import com.unifier.services.job.IJobResultService;

@Service
public class JobResultServiceImpl implements IJobResultService {

    @Autowired
    private IJobResultMao jobResultMao;

    @Override
    public void saveJob(JobResult jobResult) {
        jobResultMao.saveJob(jobResult);
    }

    @Override
    public JobResult get(String jobCode, Level level) {
        return jobResultMao.get(jobCode, level);
    }
}
