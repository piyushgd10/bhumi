package com.unifier.services.job;

import com.unifier.core.annotation.Level;
import com.unifier.core.entity.JobResult;

public interface IJobResultService {

    void saveJob(JobResult jobResult);

    JobResult get(String jobCode, Level level);
}
