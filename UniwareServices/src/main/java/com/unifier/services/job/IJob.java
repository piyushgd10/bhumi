/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 25, 2015
 *  @author amit
 */
package com.unifier.services.job;

import com.unifier.core.entity.JobResult;

/**
 * @author singla
 */
public interface IJob {

    enum Severity {
        NONE(0, "All"),
        MINOR(1, "Minor"),
        MAJOR(2, "Major"),
        FATAL(3, "Fatal"),
        CRITICAL(4, "Critical");
        private String displayName;
        private int    level;

        Severity(int level, String displayName) {
            this.displayName = displayName;
            this.level = level;
        }

        public static Severity getSeverity(int level) {
            switch (level) {
                case 0:
                    return NONE;
                case 1:
                    return MINOR;
                case 2:
                    return MAJOR;
                case 3:
                    return FATAL;
                default:
                    return CRITICAL;
            }
        }

        public int level() {
            return level;
        }

        public String displayName() {
            return displayName;
        }
    };

    String getName();

    boolean isTaskCompleted();

    JobResult getJobResult();

    void setJobResult(JobResult jobResult);

    void setCurrentStatus(String currentStatus);

    void setId(int id);

    int getId();

}