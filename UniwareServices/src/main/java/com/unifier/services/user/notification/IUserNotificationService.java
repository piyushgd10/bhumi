/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 11, 2012
 *  @author singla
 */
package com.unifier.services.user.notification;

import java.util.List;

import com.uniware.core.api.usernotification.AddUserNotificationRequest;
import com.uniware.core.api.usernotification.AddUserNotificationRespone;
import com.uniware.core.api.usernotification.DeleteAllNotificationsRequest;
import com.uniware.core.api.usernotification.DeleteAllNotificationsResponse;
import com.uniware.core.api.usernotification.DeleteNotificationRequest;
import com.uniware.core.api.usernotification.DeleteNotificationResponse;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountRequest;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountResponse;
import com.uniware.core.api.usernotification.GetUserNotificationsRequest;
import com.uniware.core.api.usernotification.GetUserNotificationsRespone;
import com.uniware.core.vo.UserNotificationTypeVO;

/**
 * @author Sunny
 */
public interface IUserNotificationService {

    AddUserNotificationRespone addNotification(AddUserNotificationRequest request);

    List<UserNotificationTypeVO> getAllUserNotificationTypes();

    GetUserNotificationsRespone getUserNotifications(GetUserNotificationsRequest request);

    DeleteNotificationResponse removeNotification(DeleteNotificationRequest request);

    GetUnreadNotificationCountResponse getUnreadMessageCount(GetUnreadNotificationCountRequest request);

    DeleteAllNotificationsResponse removeAllNotifications(DeleteAllNotificationsRequest request);
}