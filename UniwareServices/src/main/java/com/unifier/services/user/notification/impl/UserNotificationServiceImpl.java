/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 11, 2012
 *  @author singla
 */
package com.unifier.services.user.notification.impl;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.user.notification.IUserNotificationService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.usernotification.AddUserNotificationRequest;
import com.uniware.core.api.usernotification.AddUserNotificationRespone;
import com.uniware.core.api.usernotification.DeleteAllNotificationsRequest;
import com.uniware.core.api.usernotification.DeleteAllNotificationsResponse;
import com.uniware.core.api.usernotification.DeleteNotificationRequest;
import com.uniware.core.api.usernotification.DeleteNotificationResponse;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountRequest;
import com.uniware.core.api.usernotification.GetUnreadNotificationCountResponse;
import com.uniware.core.api.usernotification.GetUserNotificationsRequest;
import com.uniware.core.api.usernotification.GetUserNotificationsRespone;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.UserNotificationTypeVO;
import com.uniware.core.vo.UserNotificationVO;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserNotificationMao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Sunny
 */
@Service
public class UserNotificationServiceImpl implements IUserNotificationService {

    @Autowired
    private IUserNotificationMao userNotificationMao;

    @Autowired
    private IUsersService        usersService;


    @Override
    public GetUserNotificationsRespone getUserNotifications(GetUserNotificationsRequest request) {
        GetUserNotificationsRespone response = new GetUserNotificationsRespone();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            response.setUserNotifications(userNotificationMao.getUserNotifications(request.getNotificationTypes(), request.getStart(), request.getPageSize()));
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public AddUserNotificationRespone addNotification(AddUserNotificationRequest request) {
        AddUserNotificationRespone response = new AddUserNotificationRespone();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserNotificationVO userNotification = new UserNotificationVO();
            userNotification.setIdentifier(request.getIdentifier());
            userNotification.setMessage(request.getMessage());
            userNotification.setActionUrl(request.getActionUrl());
            userNotification.setNotificationType(request.getNotificationType());
            userNotification.setTitle(request.getTitle());
            userNotification.setCreated(DateUtils.getCurrentTime());
            userNotificationMao.save(userNotification);
            response.setSuccessful(true);
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public List<UserNotificationTypeVO> getAllUserNotificationTypes() {
        return userNotificationMao.getAllUserNotificationTypes();
    }

    @Override
    public DeleteNotificationResponse removeNotification(DeleteNotificationRequest request) {
        DeleteNotificationResponse response = new DeleteNotificationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            userNotificationMao.remove(request.getNotificationIdentifier());
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public GetUnreadNotificationCountResponse getUnreadMessageCount(GetUnreadNotificationCountRequest request) {
        GetUnreadNotificationCountResponse response = new GetUnreadNotificationCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = usersService.getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid User");
            } else {
                Date unreadCountDate = userProfile.getNotificationReadTime() != null ? userProfile.getNotificationReadTime() : new Date();
                response.setCount(userNotificationMao.getUnreadCount(unreadCountDate));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public DeleteAllNotificationsResponse removeAllNotifications(DeleteAllNotificationsRequest request) {
        DeleteAllNotificationsResponse response = new DeleteAllNotificationsResponse();
        userNotificationMao.removeAll();
        response.setSuccessful(true);
        return response;
    }

}
