/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 23, 2012
 *  @author singla
 */
package com.unifier.services.startup.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.jms.JMSException;

import org.apache.xmlbeans.SystemProperties;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.codahale.metrics.Slf4jReporter;
import com.izettle.metrics.influxdb.InfluxDbHttpSender;
import com.izettle.metrics.influxdb.InfluxDbReporter;
import com.unifier.core.cache.CacheDirtyEvent;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.startup.IBaseStartupDao;
import com.unifier.services.job.IJobService;
import com.unifier.services.job.JobEvent;
import com.unifier.services.startup.IStartupService;
import com.uniware.core.api.prices.PushAllPricesOnChannelRequest;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.Constants;
import com.uniware.core.utils.UserContext;
import com.uniware.services.channel.IChannelService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.CacheDirtyMessageListener;
import com.uniware.services.messaging.jms.CreateTenantEvent;
import com.uniware.services.messaging.jms.CreateTenantListener;
import com.uniware.services.messaging.jms.ExportJobEvent;
import com.uniware.services.messaging.jms.ExportMessageListener;
import com.uniware.services.messaging.jms.ImportJobEvent;
import com.uniware.services.messaging.jms.ImportMessageListener;
import com.uniware.services.messaging.jms.JobEventMessageListener;
import com.uniware.services.messaging.jms.PushChannelPriceMessageListener;
import com.uniware.services.messaging.jms.StompMessageListener;
import com.uniware.services.messaging.jms.SyncChannelCatalogEvent;
import com.uniware.services.messaging.jms.SyncChannelCatalogMessageListener;
import com.uniware.services.messaging.jms.SyncChannelInventoryEvent;
import com.uniware.services.messaging.jms.SyncChannelInventoryMessageListener;
import com.uniware.services.messaging.jms.SyncChannelOrdersEvent;
import com.uniware.services.messaging.jms.SyncChannelOrdersMessageListener;
import com.uniware.services.messaging.jms.SyncChannelWarehouseInventoryEvent;
import com.uniware.services.messaging.jms.SyncChannelWarehouseInventoryMessageListener;
import com.uniware.services.messaging.jms.SyncReconciliationInvoiceEvent;
import com.uniware.services.messaging.jms.SyncReconciliationInvoiceMessageListener;
import com.uniware.services.messaging.jms.UniwareMessageListener;
import com.uniware.services.publisher.Message;

@Service("baseStartupService")
public class StartupServiceImpl implements IStartupService {

    private static final Logger         LOG                    = LoggerFactory.getLogger(StartupServiceImpl.class);
    private static final String         SERVICE_NAME           = "startupService";

    private static final String         JVM_USAGE              = "jvm.fd.usage";
    private static final String         JVM_GC                 = "jvm.gc";
    private static final String         JVM_MEMORY             = "jvm.memory";
    private static final String         JVM_THREAD_STATES      = "jvm.thread-states";
    private static final String         SYSTEM_METRICS         = "system-metrics";
    private static final String         SERVICE_METRICS        = "service-metrics";
    private static final String         SERVICE_METRICS_LOGGER = "service-metrics-logger";
    private static final String         SYSTEM_METRICS_LOGGER  = "system-metrics-logger";

    @Autowired
    private IJobService                 jobService;

    @Autowired
    private IBaseStartupDao             baseStartupDao;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           connector;

    @Autowired
    @Qualifier("jobsConnector")
    private ActiveMQConnector           jobsConnector;

    @Autowired
    private ApplicationContext          applicationContext;

    @Autowired
    private IChannelService             channelService;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Override
    public void loadStartupResources() throws JMSException, SchedulerException, ExecutionException, InterruptedException {
        boolean executeJobs = false;
        String propExecuteJobs = System.getProperty(Constants.EXECUTE_JOBS_ON_HOST);
        if (StringUtils.isBlank(propExecuteJobs) || Boolean.parseBoolean(propExecuteJobs)) {
            executeJobs = true;
        }

        registerListeners(executeJobs);

        // To log metrics to an SLF4J logger
        initializeMetricsRegistry();

        CacheManager.getInstance().loadEagerGlobalCaches();
        ConfigurationManager.getInstance().loadEagerGlobalConfigurations();

        // Set up the tenant resource path if not present.
        List<Tenant> tenants = CacheManager.getInstance().getCache(TenantCache.class).getActiveTenants();
        List<Future<?>> tenantCacheFutures = new ArrayList<>(tenants.size());
        final boolean loadCaches = !executeJobs;
        for (Tenant tenant : tenants) {
            UserContext.current().setTenant(tenant);
            Future<?> f = executorFactory.getExecutor(SERVICE_NAME).submit(new Runnable() {
                @Override
                public void run() {
                    channelService.resetAllSyncStatuses();
                    if (loadCaches) {
                        // load eager caches & configurations
                        for (Facility facility : CacheManager.getInstance().getCache(FacilityCache.class).getFacilities()) {
                            UserContext.current().setFacility(facility);
                            CacheManager.getInstance().loadEagerFacilityLevelCaches();
                            ConfigurationManager.getInstance().loadEagerFacilityLevelConfigurations();
                        }
                        CacheManager.getInstance().loadEagerCaches();
                        ConfigurationManager.getInstance().loadEagerConfigurations();
                    }
                }
            });
            tenantCacheFutures.add(f);
        }
        for (Future<?> f : tenantCacheFutures) {
            f.get();
        }
        executorFactory.getExecutor(SERVICE_NAME).shutdownNow();

        boolean triggerJobs = false;
        String propTriggerJobs = System.getProperty(Constants.TRIGGER_JOBS_ON_HOST);
        if (StringUtils.isBlank(propTriggerJobs) || Boolean.parseBoolean(propTriggerJobs)) {
            triggerJobs = true;
        }
        if (executeJobs && triggerJobs) {
            jobService.loadJobs();
        }else{
            jobService.stopScheduler();
        }
    }

    @Transactional
    private void registerListeners(boolean executeJobs) {
        LOG.info("Registering Event Listeners...");
        baseStartupDao.registerHibernateEventListeners();
        registerActiveMQConsumers(executeJobs);
        LOG.info("Done Registering Event Listeners...");
    }

    private void registerActiveMQConsumers(boolean executeJobs) {
        // Cache dirty event listener
        UniwareMessageListener<CacheDirtyEvent> cacheDirtyMessageListener = new CacheDirtyMessageListener();
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(cacheDirtyMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        connector.createListener(MessagingConstants.Queue.CACHE_DIRTY, cacheDirtyMessageListener);

        // stomp message listener
        LOG.info("Registering Stomp message listener");
        UniwareMessageListener<Message> stompMessageListener = new StompMessageListener("StompMessageListener-0");
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(stompMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        connector.createListener(MessagingConstants.Queue.STOMP_MESSAGES_TOPIC, stompMessageListener);

        if (executeJobs) {
            // Job event listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getRecurrentJobListenerListenerCount(); i++) {
                LOG.info("Registering job event message listener: {}", i);
                UniwareMessageListener<JobEvent> jobEventListener = new JobEventMessageListener("RecurrentJobEventListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(jobEventListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                jobsConnector.createListener(MessagingConstants.Queue.JOB_QUEUE, jobEventListener);
            }

            // Import listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportJobListenerCount(); i++) {
                LOG.info("Registering import message listener: {}", i);
                UniwareMessageListener<ImportJobEvent> importMessageListener = new ImportMessageListener("ImportMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(importMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.IMPORT_QUEUE, importMessageListener);
            }

            // Export listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getExportJobListenerCount(); i++) {
                LOG.info("Registering order sync message listener: {}", i);
                UniwareMessageListener<ExportJobEvent> exportMessageListener = new ExportMessageListener("ExportMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(exportMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.EXPORT_QUEUE, exportMessageListener);
            }

            // Create Tenant listeners
            LOG.info("Registering create tenant message listener");
            UniwareMessageListener<CreateTenantEvent> createTenantListener = new CreateTenantListener("CreateTenantListener");
            applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(createTenantListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
            connector.createListener(MessagingConstants.Queue.TENANT_QUEUE, createTenantListener);

            // Order Sync listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelOrdersListenerCount(); i++) {
                LOG.info("Registering OrderSync message listener: {}", i);
                UniwareMessageListener<SyncChannelOrdersEvent> syncChannelOrdersMessageListener = new SyncChannelOrdersMessageListener("SyncChannelOrdersMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(syncChannelOrdersMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.ORDER_SYNC_QUEUE, syncChannelOrdersMessageListener);
            }

            // Inventory Sync listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelInventoryListenerCount(); i++) {
                LOG.info("Registering InventorySync message listener: {}", i);
                UniwareMessageListener<SyncChannelInventoryEvent> syncChannelInventoryMessageListener = new SyncChannelInventoryMessageListener(
                        "SyncChannelInventoryMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(syncChannelInventoryMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.INVENTORY_SYNC_QUEUE, syncChannelInventoryMessageListener);
            }

            // Catalog Sync listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelCatalogListenerCount(); i++) {
                LOG.info("Registering CatalogSync message listener: {}", i);
                UniwareMessageListener<SyncChannelCatalogEvent> syncChannelCatalogMessageListener = new SyncChannelCatalogMessageListener("SyncChannelCatalogMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(syncChannelCatalogMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.CATALOG_SYNC_QUEUE, syncChannelCatalogMessageListener);
            }

            // Channel Warehouse Inventory Sync listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelWarehouseInventoryListenerCount(); i++) {
                LOG.info("Registering Channel Warehouse Inventory Sync message listener: {}", i);
                UniwareMessageListener<SyncChannelWarehouseInventoryEvent> syncChannelWarehouseInventoryMessageListener = new SyncChannelWarehouseInventoryMessageListener(
                        "SyncChannelWarehouseInventoryMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(syncChannelWarehouseInventoryMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,
                        false);
                connector.createListener(MessagingConstants.Queue.CHANNEL_WAREHOUSE_INVENTORY_SYNC_QUEUE, syncChannelWarehouseInventoryMessageListener);
            }

            // Reconciliation Invoice Sync listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelReconciliationInvoiceListenerCount(); i++) {
                LOG.info("Registering Reconciliation Invoice message listener: {}", i);
                UniwareMessageListener<SyncReconciliationInvoiceEvent> syncReconciliationInvoiceMessageListener = new SyncReconciliationInvoiceMessageListener(
                        "SyncReconciliationInvoiceMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(syncReconciliationInvoiceMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,
                        false);
                connector.createListener(MessagingConstants.Queue.RECONCILIATION_INVOICE_QUEUE, syncReconciliationInvoiceMessageListener);
            }

            // Price Sync Listeners
            for (int i = 0; i < CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getSyncChannelCatalogListenerCount(); i++) {
                LOG.info("Registering PriceSync message listener: {}", i);
                UniwareMessageListener<PushAllPricesOnChannelRequest> pushChannelPriceMessageListener = new PushChannelPriceMessageListener("PushChannelPriceMessageListener-" + i);
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(pushChannelPriceMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
                connector.createListener(MessagingConstants.Queue.PRICE_SYNC_QUEUE, pushChannelPriceMessageListener);
            }
        } else {
            LOG.info("Property executeJobs set to false, will not execute jobs");
        }
    }

    private void initializeMetricsRegistry() {

        EnvironmentPropertiesCache cache = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class);

        //
        // System Metrics
        // Create Metric Registry for system metrics
        // Register the jvm metrics in registry
        //
        final MetricRegistry systemMetrics = SharedMetricRegistries.getOrCreate(SYSTEM_METRICS);
        systemMetrics.register(JVM_USAGE, new com.codahale.metrics.jvm.FileDescriptorRatioGauge());
        systemMetrics.register(JVM_GC, new com.codahale.metrics.jvm.GarbageCollectorMetricSet());
        systemMetrics.register(JVM_MEMORY, new com.codahale.metrics.jvm.MemoryUsageGaugeSet());
        systemMetrics.register(JVM_THREAD_STATES, new com.codahale.metrics.jvm.ThreadStatesGaugeSet());

        //
        // Service Metrics
        // Get the SharedMetricRegistry and the metrics(like Timed, Gauge, Metered etc) are registered using annotations in Classes
        //
        final MetricRegistry serviceMetrics = SharedMetricRegistries.getOrCreate(SERVICE_METRICS);

        if (StringUtils.isNotBlank(cache.getMetricsServerHost())) {
            // Log service metrics to influxdb
            LOG.info("Recording metrics in influxdb");
            Map<String, String> tags = new HashMap<>();
            tags.put("serverName", SystemProperties.getProperty("clusterName"));
            tags.put("appIdentifier", StringUtils.isNotBlank(SystemProperties.getProperty("appIdentifier")) ? SystemProperties.getProperty("appIdentifier") : "app1");
            try {
                InfluxDbHttpSender systemMetricsInfluxDbHttpSender = new InfluxDbHttpSender("http", cache.getMetricsServerHost(), cache.getMetricsServerPort(), "uniware", "",
                        TimeUnit.MINUTES, 30000, 30000);
                InfluxDbReporter.forRegistry(systemMetrics).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).filter(MetricFilter.ALL).withTags(
                        tags).build(systemMetricsInfluxDbHttpSender).start(2, TimeUnit.MINUTES);
                InfluxDbHttpSender serviceMetricsInfluxDbHttpSender = new InfluxDbHttpSender("http", cache.getMetricsServerHost(), cache.getMetricsServerPort(), "uniware", "",
                        TimeUnit.MINUTES, 30000, 30000);
                InfluxDbReporter.forRegistry(serviceMetrics).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).filter(MetricFilter.ALL).withTags(
                        tags).build(serviceMetricsInfluxDbHttpSender).start(2, TimeUnit.MINUTES);
            } catch (Exception e) {
                LOG.error("Error instantiating influxdb", e);
            }
        } else {
            // Log service metrics to an SLF4J logger
            LOG.info("Recording metrics in log files");
            final Slf4jReporter systemMetricsReporter = Slf4jReporter.forRegistry(systemMetrics).outputTo(LoggerFactory.getLogger(SYSTEM_METRICS_LOGGER)).convertRatesTo(
                    TimeUnit.SECONDS) // rate_unit=events/second
                    .convertDurationsTo(TimeUnit.MILLISECONDS) // duration_unit=milliseconds
                    .build();
            systemMetricsReporter.start(5, TimeUnit.MINUTES);

            final Slf4jReporter serviceMetricsReporter = Slf4jReporter.forRegistry(serviceMetrics).outputTo(LoggerFactory.getLogger(SERVICE_METRICS_LOGGER)).convertRatesTo(
                    TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).build();
            serviceMetricsReporter.start(5, TimeUnit.MINUTES);
        }
    }
}
