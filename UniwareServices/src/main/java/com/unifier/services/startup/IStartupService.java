/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 23, 2012
 *  @author singla
 */
package com.unifier.services.startup;

import javax.jms.JMSException;

import org.quartz.SchedulerException;

import java.util.concurrent.ExecutionException;

public interface IStartupService {

    void loadStartupResources() throws JMSException, SchedulerException, ExecutionException, InterruptedException;

}
