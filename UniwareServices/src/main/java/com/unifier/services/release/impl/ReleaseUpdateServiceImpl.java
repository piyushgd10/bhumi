/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.unifier.services.release.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.release.GetAllReleaseUpdatesRequest;
import com.unifier.core.api.release.GetAllReleaseUpdatesResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesResponse.ReleaseUpdateDTO;
import com.unifier.core.api.release.MarkReleaseUpdateReadRequest;
import com.unifier.core.api.release.MarkReleaseUpdateReadResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.ReleaseVersion;
import com.unifier.core.entity.User;
import com.unifier.dao.release.IReleaseDao;
import com.unifier.mao.release.IReleaseMao;
import com.unifier.services.release.IReleaseUpdateService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.ReleaseUpdateVO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserProfileMao;

@Service(value = "releaseService")
@Transactional
public class ReleaseUpdateServiceImpl implements IReleaseUpdateService {

    @Autowired
    private IReleaseMao     releaseMao;

    @Autowired
    private IUsersService   usersService;

    @Autowired
    private IReleaseDao     releaseDao;

    @Autowired
    private IUserProfileMao userProfileMao;

    @Override
    public GetUnreadReleaseUpdatesResponse getReleaseUpdates(GetUnreadReleaseUpdatesRequest request) {
        GetUnreadReleaseUpdatesResponse response = new GetUnreadReleaseUpdatesResponse();
        ValidationContext context = request.validate();
        ReleaseUpdateVO releaseUpdateVO = null;
        if (!context.hasErrors()) {
            UserProfileVO userProfile = usersService.getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                int lastReleaseUpdateVersionRead = userProfile.getLastReleaseUpdateVersionRead();
                ReleaseVersion latestReleaseVersion = releaseDao.getLatestReleaseVersion();
                if (latestReleaseVersion != null) {
                    Integer latestReleaseUpdateVersionToRead = latestReleaseVersion.getMajorVersion();
                    if (latestReleaseUpdateVersionToRead.compareTo(lastReleaseUpdateVersionRead) > 0) {
                        releaseUpdateVO = releaseMao.getReleaseUpdate(latestReleaseUpdateVersionToRead);
                    }
                    if (releaseUpdateVO != null) {
                        ReleaseUpdateDTO releaseUpdateDTO = new ReleaseUpdateDTO();
                        releaseUpdateDTO.setVersionId(releaseUpdateVO.getVersionId());
                        releaseUpdateDTO.setTitleKey(releaseUpdateVO.getTitleKey());
                        releaseUpdateDTO.setContentKey(releaseUpdateVO.getContentKey());
                        releaseUpdateDTO.setUrl(releaseUpdateVO.getUrl());
                        List<GetUnreadReleaseUpdatesResponse.UpdateDTO> updates = new ArrayList<>();
                        if (releaseUpdateVO.getUpdates() != null) {
                            for (ReleaseUpdateVO.Update updateVO : releaseUpdateVO.getUpdates()) {
                                GetUnreadReleaseUpdatesResponse.UpdateDTO update = new GetUnreadReleaseUpdatesResponse.UpdateDTO();
                                update.setTitleKey(updateVO.getTitleKey());
                                update.setContentKey(updateVO.getContentKey());
                                update.setImageUrl(updateVO.getImageUrl());
                                update.setUrl(updateVO.getUrl());
                                updates.add(update);
                            }
                        }
                        releaseUpdateDTO.setUpdates(updates);
                        releaseUpdateDTO.setReleaseDate(releaseUpdateVO.getReleaseDate());
                        response.setReleaseUpdate(releaseUpdateDTO);
                    }
                } else {
                    context.addError(WsResponseCode.LATEST_RELEASE_UPDATE_VERSION_NOT_FOUND, "latest release update version not found in release history");
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public MarkReleaseUpdateReadResponse markUpdateRead(MarkReleaseUpdateReadRequest request) {
        MarkReleaseUpdateReadResponse response = new MarkReleaseUpdateReadResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = usersService.getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                ReleaseVersion latestReleaseVersion = releaseDao.getLatestReleaseVersion();
                userProfile.setLastReleaseUpdateVersionRead(latestReleaseVersion.getMajorVersion());
                userProfileMao.save(userProfile);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetAllReleaseUpdatesResponse getAllReleaseUpdates(GetAllReleaseUpdatesRequest request) {
        GetAllReleaseUpdatesResponse response = new GetAllReleaseUpdatesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersService.getUserById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                List<ReleaseUpdateVO> releaseUpdateVOs = releaseMao.getAllReleaseUpdates();
                List<GetAllReleaseUpdatesResponse.ReleaseUpdateDTO> releaseUpdates = new ArrayList<>(releaseUpdateVOs.size());
                for (ReleaseUpdateVO releaseUpdateVO : releaseUpdateVOs) {
                    GetAllReleaseUpdatesResponse.ReleaseUpdateDTO releaseUpdateDTO = new GetAllReleaseUpdatesResponse.ReleaseUpdateDTO();
                    releaseUpdateDTO.setVersionId(releaseUpdateVO.getVersionId());
                    releaseUpdateDTO.setTitleKey(releaseUpdateVO.getTitleKey());
                    releaseUpdateDTO.setContentKey(releaseUpdateVO.getContentKey());
                    releaseUpdateDTO.setUrl(releaseUpdateVO.getUrl());
                    releaseUpdateDTO.setReleaseDate(releaseUpdateVO.getReleaseDate());
                    releaseUpdates.add(releaseUpdateDTO);
                }
                response.setReleaseUpdates(releaseUpdates);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public GetUnreadReleaseUpdateCountResponse getUnreadReleaseUpdatesCount(GetUnreadReleaseUpdateCountRequest request) {
        GetUnreadReleaseUpdateCountResponse response = new GetUnreadReleaseUpdateCountResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = usersService.getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                int lastReleaseUpdateVersionRead = userProfile.getLastReleaseUpdateVersionRead();
                ReleaseVersion globalReleaseVersion = releaseDao.getLatestReleaseVersion();
                if (globalReleaseVersion != null) {
                    int globalMajorVersion = globalReleaseVersion.getMajorVersion();
                    if (lastReleaseUpdateVersionRead < globalMajorVersion) {
                        response.setUnreadUpdatedCount(globalMajorVersion - lastReleaseUpdateVersionRead);
                    } else {
                        response.setUnreadUpdatedCount(0);
                    }
                } else {
                    context.addError(WsResponseCode.LATEST_RELEASE_UPDATE_VERSION_NOT_FOUND, "latest release update version not found in release history");
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            response.setSuccessful(true);
        }
        return response;

    }

    @Override
    //@Transactional(readOnly = true)
    public ReleaseVersion getLatestReleaseUpdateVersion() {
        return releaseDao.getLatestReleaseVersion();
    }
}
