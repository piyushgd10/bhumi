/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16/04/14
 *  @author amit
 */

package com.unifier.services.release;

import com.unifier.core.api.release.GetAllReleaseUpdatesRequest;
import com.unifier.core.api.release.GetAllReleaseUpdatesResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdateCountResponse;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesRequest;
import com.unifier.core.api.release.GetUnreadReleaseUpdatesResponse;
import com.unifier.core.api.release.MarkReleaseUpdateReadRequest;
import com.unifier.core.api.release.MarkReleaseUpdateReadResponse;
import com.unifier.core.entity.ReleaseVersion;

public interface IReleaseUpdateService {

    GetUnreadReleaseUpdatesResponse getReleaseUpdates(GetUnreadReleaseUpdatesRequest request);

    GetAllReleaseUpdatesResponse getAllReleaseUpdates(GetAllReleaseUpdatesRequest request);

    MarkReleaseUpdateReadResponse markUpdateRead(MarkReleaseUpdateReadRequest request);

    GetUnreadReleaseUpdateCountResponse getUnreadReleaseUpdatesCount(GetUnreadReleaseUpdateCountRequest request);

    /**
     * @return
     */
    ReleaseVersion getLatestReleaseUpdateVersion();
}
