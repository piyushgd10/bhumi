/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 21, 2011
 *  @author singla
 */
package com.unifier.services.email.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.unifier.core.utils.ApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.email.EditEmailTemplateRequest;
import com.unifier.core.api.email.EditEmailTemplateResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.email.SimpleMailMessage;
import com.unifier.core.email.SimpleMailMessage.Attachment;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.entity.User;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.vo.BouncedEmailVO;
import com.unifier.dao.email.IEmailDao;
import com.unifier.dao.email.IEmailMao;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.email.IEmailService;
import com.unifier.services.user.notification.IUserNotificationService;
import com.uniware.core.api.email.SendEmailResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.EmailConfiguration;
import com.uniware.services.configuration.EmailConfiguration.EmailTemplateVO;
import com.uniware.services.messaging.jms.ActiveMQConnector;

/**
 * @author singla
 */
@Service
public class EmailServiceImpl implements IEmailService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private IEmailDao emailDao;

    @Autowired
    private IEmailMao emailMao;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    @Override
    public SendEmailResponse send(EmailMessage message, String source) {
        SendEmailResponse response = new SendEmailResponse();
        List<String> invalidEmails = new ArrayList<String>();
        sanitizeEmailList(message.getTo(), invalidEmails);
        sanitizeEmailList(message.getCc(), invalidEmails);
        sanitizeEmailList(message.getBcc(), invalidEmails);
        if (!invalidEmails.isEmpty()) {
            LOG.warn("Message type: {}, Invalid recepients {}", message.getTemplateName(), invalidEmails);
            response.setInvalidEmails(invalidEmails);
        } else {
            response.setSuccessful(true);
        }
        enqueueEmailMessage(message, source);
        return response;
    }

    @Override
    public SendEmailResponse send(EmailMessage message) {
        SendEmailResponse response = new SendEmailResponse();
        List<String> invalidEmails = new ArrayList<String>();
        sanitizeEmailList(message.getTo(), invalidEmails);
        sanitizeEmailList(message.getCc(), invalidEmails);
        sanitizeEmailList(message.getBcc(), invalidEmails);
        if (!invalidEmails.isEmpty()) {
            LOG.warn("Message type: {}, Invalid recepients {}", message.getTemplateName(), invalidEmails);
            response.setInvalidEmails(invalidEmails);
        } else {
            response.setSuccessful(true);
        }
        enqueueEmailMessage(message, null);
        return response;
    }

    private void enqueueEmailMessage(EmailMessage message, String source) {
        EmailTemplateVO template = ConfigurationManager.getInstance().getConfiguration(EmailConfiguration.class).getTemplateByType(message.getTemplateName());
        if (template != null && template.isEnabled()) {
            SimpleMailMessage mailMessage = null;
            try {
                mailMessage = prepareMailMessage(template, message, source);
            } catch (Exception e) {
                LOG.error("Exception while preparing email message type: {}, Reason: {}", message.getTemplateName(), e.getMessage());
            }
            if (mailMessage != null) {
                LOG.info("Adding message type: {}, message: {}  to email queue.", message.getTemplateName(), mailMessage);
                activeMQConnector.produceMessage(MessagingConstants.Queue.EMAIL_MESSAGE_QUEUE, mailMessage);
            }
        } else {
            LOG.error("Ignoring message type: {} as template does not exist or is disabled", message.getTemplateName());
        }
    }

    private SimpleMailMessage prepareMailMessage(EmailTemplateVO template, EmailMessage emailMessage, String source) {
        SimpleMailMessage message = new SimpleMailMessage();
        if (emailMessage.getTo() == null || emailMessage.getTo().size() == 0) {
            if (template.getTo() != null) {
                emailMessage.addRecepients(StringUtils.split(String.valueOf(template.getTo().evaluate(emailMessage.getTemplateParams()))));
            } else {
                throw new IllegalArgumentException("No recipient specified.");
            }
        }
        String from = emailMessage.getFrom();
        if (from == null) {
            from = String.valueOf(template.getFrom().evaluate(emailMessage.getTemplateParams()));
        }

        if (template.getCc() != null) {
            List<String> additionalCCs = StringUtils.split(String.valueOf(template.getCc().evaluate(emailMessage.getTemplateParams())));
            List<String> invalidAdditionalCCs = new ArrayList<>();
            sanitizeEmailList(additionalCCs, invalidAdditionalCCs);
            if (invalidAdditionalCCs.size() > 0) {
                LOG.warn("Skipped invalid emails: {} in additional CCs", invalidAdditionalCCs);
            }
            emailMessage.addCCs(additionalCCs);
        }

        if (template.getBcc() != null) {
            List<String> additionalBCCs = StringUtils.split(String.valueOf(template.getBcc().evaluate(emailMessage.getTemplateParams())));
            List<String> invalidAdditionalBCCs = new ArrayList<>();
            sanitizeEmailList(additionalBCCs, invalidAdditionalBCCs);
            if (invalidAdditionalBCCs.size() > 0) {
                LOG.warn("Skipped invalid emails: {} in additional BCCs", invalidAdditionalBCCs);
            }
            emailMessage.addBCCs(additionalBCCs);
        }

        String replyTo = emailMessage.getReplyTo();
        if (replyTo == null) {
            replyTo = String.valueOf(template.getReplyTo().evaluate(emailMessage.getTemplateParams()));
        }

        message.setTo(emailMessage.getTo());
        message.setFrom(from);
        message.setCc(emailMessage.getCc());
        message.setBcc(emailMessage.getBcc());

        if (replyTo != null) {
            message.setReplyTo(replyTo);
        }

        if (StringUtils.isNotBlank(emailMessage.getSubject())) {
            message.setSubject(emailMessage.getSubject());
        } else {
            message.setSubject(template.getSubjectTemplate().evaluate(emailMessage.getTemplateParams()));
        }
        String body = emailMessage.getBody();
        if (StringUtils.isBlank(body)) {
            body = template.getBodyTemplate().evaluate(emailMessage.getTemplateParams());
        }
        message.setBody(body + (StringUtils.isNotBlank(System.getProperty("env")) ? "<div class=\"" + StringUtils.capitalizeString(System.getProperty("env")) + "\" />" : ""));
        String downloadDirectoryPath = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getExportDirectoryPath() + "/shippingLabel/";
        String downloadLinkPrefix = "https://" + UserContext.current().getTenant().getAccessUrl() + "/files/shippingLabel";
        for (File file : emailMessage.getAttachments()) {
            String fileName = EncryptionUtils.md5Encode(UserContext.current().getTenant().getCode() + file.getName() + System.currentTimeMillis());
            try {
                org.apache.commons.io.FileUtils.copyFile(file, new File(downloadDirectoryPath + "/" + fileName));
            } catch (IOException e) {
                LOG.error("Failed to add attachment {}", file.getName());
            }
            message.addAttachment(new Attachment(file.getName(), ApplicationUtils.appendAppIdentifier(downloadLinkPrefix + "/" + fileName)));
        }
        for (String attachmentUrl : emailMessage.getAttachmentUrls()) {
            String name = attachmentUrl.substring(attachmentUrl.lastIndexOf("/") + 1);
            message.addAttachment(new Attachment(name, attachmentUrl));
        }
        message.setCreated(DateUtils.getCurrentTime());
        message.addProperty("source", "uniware");
        message.addProperty("tenantCode", UserContext.current().getTenant().getCode());
        message.addProperty("productType", UserContext.current().getTenant().getProduct().getCode());
        message.addProperty("messageType", template.getType());
        message.addProperty("channelSource", source);
        return message;
    }

    private void sanitizeEmailList(Collection<String> emails, List<String> invalidEmails) {
        if (emails != null && emails.size() > 0) {
            Iterator<String> iter = emails.iterator();
            while (iter.hasNext()) {
                String email = iter.next();
                if (isEmailInvalid(email)) {
                    iter.remove();
                    invalidEmails.add(email);
                }
            }
        }
    }

    @Override
    public void sendSignUpApprovalMailToAdmin(String toAddress, User user) {
        EmailMessage emailMessage = new EmailMessage(toAddress, "signUpApprovalEmail");
        emailMessage.addTemplateParam("user", user);
        emailMessage.addTemplateParam("approvalLink", "abc");
        send(emailMessage);
    }

    @Override
    public boolean isEmailInvalid(String email) {
        return emailMao.getBouncedEmailVO(email) != null;
    }

    @Override
    public void addEmailBounce(String email, String error) {
        BouncedEmailVO bouncedEmailVO = new BouncedEmailVO();
        bouncedEmailVO.setEmail(email);
        bouncedEmailVO.setErrorJson("{'message' : '" + error + "'}");
        emailMao.save(bouncedEmailVO);
    }

    @Override
    @Transactional
    @MarkDirty(values = { EmailConfiguration.class })
    public EditEmailTemplateResponse editEmailTemplate(EditEmailTemplateRequest request) {
        EditEmailTemplateResponse response = new EditEmailTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            EmailTemplate template = getEmailTemplateByType(request.getType());
            if (template == null) {
                context.addError(WsResponseCode.INVALID_EMAIL_TEMPLATE_TYPE, "Invalid Email Template Type");
            } else {
                template.setEnabled(request.isEnabled());
                template.setCcEmail(request.getCc());
                template.setBccEmail(request.getBcc());
                template.setSubjectTemplate(request.getSubjectTemplate());
                template.setBodyTemplate(request.getBodyTemplate());
                template = emailDao.updateEmailTemplate(template);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public EmailTemplate getEmailTemplateByType(String type) {
        return emailDao.getEmailTemplateByType(type);
    }

    @Override
    @Transactional
    public List<EmailTemplate> getEmailTemplates() {
        return emailDao.getEmailTemplates();
    }
}
