/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Jun-2014
 *  @author sunny
 */
package com.unifier.services.email;

import java.util.List;

import com.unifier.core.api.email.EditEmailTemplateRequest;
import com.unifier.core.api.email.EditEmailTemplateResponse;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.entity.User;
import com.uniware.core.api.email.SendEmailResponse;

public interface IEmailService {

    SendEmailResponse send(EmailMessage message);

    void sendSignUpApprovalMailToAdmin(String toAddress, User user);

    boolean isEmailInvalid(String email);

    void addEmailBounce(String email, String error);

    EditEmailTemplateResponse editEmailTemplate(EditEmailTemplateRequest request);

    EmailTemplate getEmailTemplateByType(String type);

    List<EmailTemplate> getEmailTemplates();

    SendEmailResponse send(EmailMessage message, String source);

}
