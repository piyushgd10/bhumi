/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.unifier.services.export;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.unifier.core.api.datatable.GetDatatableResultCountRequest;
import com.unifier.core.api.datatable.GetDatatableResultCountResponse;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.datatable.GetDatatableResultResponse;
import com.unifier.core.api.export.AddExportSubscriberRequest;
import com.unifier.core.api.export.AddExportSubscriptionResponse;
import com.unifier.core.api.export.CloneExportJobTypeRequest;
import com.unifier.core.api.export.CloneExportJobTypeResponse;
import com.unifier.core.api.export.CreateExportJobRequest;
import com.unifier.core.api.export.CreateExportJobResponse;
import com.unifier.core.api.export.DeleteExportSubscriptionRequest;
import com.unifier.core.api.export.DeleteExportSubscriptionResponse;
import com.unifier.core.api.export.EditExportJobRequest;
import com.unifier.core.api.export.EditExportJobResponse;
import com.unifier.core.api.export.EditExportJobTypeRequest;
import com.unifier.core.api.export.EditExportJobTypeResponse;
import com.unifier.core.api.export.EditExportsSubscriptionRequest;
import com.unifier.core.api.export.EditExportsSubscriptionResponse;
import com.unifier.core.api.export.GetExportJobStatusRequest;
import com.unifier.core.api.export.GetExportJobStatusResponse;
import com.unifier.core.api.export.GetExportSubscriptionResponse;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.entity.ExportSubscriber;
import com.unifier.core.export.config.ExportConfig;
import com.uniware.core.entity.Vendor;

/**
 * @author singla
 */
public interface IExportService {

    /**
     * @param request
     * @return
     */
    CreateExportJobResponse createExportJob(CreateExportJobRequest request);

    /**
     * @param exportJobId
     * @return
     */
    ExportJob getExportJobById(String exportJobId);

    /**
     * @param exportJobId
     * @return
     * @throws IOException
     */
    ExportJob executeExportJob(String exportJobId) throws Exception;

    void updateExportJob(ExportJob exportJob);

    Map<String, Object> getFilterParameters(Vendor vendor, ExportConfig exportConfig, List<WsExportFilter> filters);

    /**
     * @param request
     * @return
     */
    EditExportJobTypeResponse editExportJobType(EditExportJobTypeRequest request);

    /**
     * @param name
     * @return
     */
    ExportJobType getExportJobTypeByName(String name);

    /**
     * @param frequency
     * @return
     */
    List<ExportJob> getExportJobByFrequency(String frequency);

    /**
     * @param exportJobId
     * @return
     */
    ExportSubscriber getExportSubscribers(String exportJobId);

    /**
     * @return
     */
    List<ExportJobType> getExportJobTypes();

    /**
     * @param username
     * @return
     */
    GetExportSubscriptionResponse getExportJobsBySubscriber(String username);

    /**
     * @param request
     * @return
     */
    EditExportsSubscriptionResponse updateExportsSubscription(EditExportsSubscriptionRequest request);

    /**
     * @param request
     * @return
     */
    EditExportJobResponse editExportJob(EditExportJobRequest request);

    /**
     * @param request
     * @return
     */
    GetExportJobStatusResponse getExportJobStatus(GetExportJobStatusRequest request);

    AddExportSubscriptionResponse addExportSubscriber(AddExportSubscriberRequest request);

    DeleteExportSubscriptionResponse deleteExportSubxcription(DeleteExportSubscriptionRequest request);

    GetDatatableResultResponse getDatatableResult(GetDatatableResultRequest request);

    void submitExportJob(String exportJobId);

    List<Object[]> executeAnonymousQuery(String query, Map<String, Object> parameters);

    CloneExportJobTypeResponse cloneExportJobType(CloneExportJobTypeRequest request);

    List<ExportJob> getUserExports(String username);

    GetDatatableResultCountResponse getDatatableResultCount(GetDatatableResultCountRequest request);
}
