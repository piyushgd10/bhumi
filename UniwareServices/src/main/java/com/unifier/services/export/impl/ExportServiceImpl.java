/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.unifier.services.export.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.unifier.core.utils.CollectionUtils;
import com.uniware.services.configuration.TenantSystemConfiguration;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unifier.core.api.datatable.GetDatatableResultCountRequest;
import com.unifier.core.api.datatable.GetDatatableResultCountResponse;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.datatable.GetDatatableResultResponse;
import com.unifier.core.api.datatable.WsDatatableRow;
import com.unifier.core.api.datatable.WsSortColumn;
import com.unifier.core.api.export.AddExportSubscriberRequest;
import com.unifier.core.api.export.AddExportSubscriptionResponse;
import com.unifier.core.api.export.CloneExportJobTypeRequest;
import com.unifier.core.api.export.CloneExportJobTypeResponse;
import com.unifier.core.api.export.CreateExportJobRequest;
import com.unifier.core.api.export.CreateExportJobResponse;
import com.unifier.core.api.export.DeleteExportSubscriptionRequest;
import com.unifier.core.api.export.DeleteExportSubscriptionResponse;
import com.unifier.core.api.export.EditExportJobRequest;
import com.unifier.core.api.export.EditExportJobResponse;
import com.unifier.core.api.export.EditExportJobTypeRequest;
import com.unifier.core.api.export.EditExportJobTypeResponse;
import com.unifier.core.api.export.EditExportsSubscriptionRequest;
import com.unifier.core.api.export.EditExportsSubscriptionRequest.ExportSubscription;
import com.unifier.core.api.export.EditExportsSubscriptionResponse;
import com.unifier.core.api.export.GetExportJobStatusRequest;
import com.unifier.core.api.export.GetExportJobStatusResponse;
import com.unifier.core.api.export.GetExportSubscriptionResponse;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.EmailMessage;
import com.unifier.core.entity.EmailTemplate;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.entity.ExportSubscriber;
import com.unifier.core.entity.User;
import com.unifier.core.export.config.ExportColumn;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.export.config.ExportFilter;
import com.unifier.core.export.config.ExportResultComparator;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.queryParser.GenerateNoSqlQueryFromJsonRequest;
import com.unifier.core.queryParser.GenerateNoSqlQueryRequest;
import com.unifier.core.queryParser.GenerateQueryRequest;
import com.unifier.core.queryParser.IQueryParser;
import com.unifier.core.queryParser.nosql.NoSqlCriteria;
import com.unifier.core.queryParser.nosql.NoSqlQuery;
import com.unifier.core.queryParser.nosql.NoSqlSorter;
import com.unifier.core.queryParser.nosql.mongo.GenerateMongoQueryResponse;
import com.unifier.core.table.config.DatatableConfig;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.Resolution;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.export.IExportDao;
import com.unifier.dao.export.IExportMao;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.email.IEmailService;
import com.unifier.services.export.IExportService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.unifier.services.users.IUsersService;
import com.unifier.services.vo.TenantProfileVO;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;
import com.uniware.services.configuration.ExportJobConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.ExportJobEvent;

/**
 * @author singla
 */
@Service
public class ExportServiceImpl implements IExportService {

    private static final Logger         LOG                = LoggerFactory.getLogger(ExportServiceImpl.class);

    private static final String         SERVICE_NAME       = "exportservice";
    private static final int            EXPORT_UPPER_LIMIT = 2000000;

    @Autowired
    private IExportDao                  exportDao;

    @Autowired
    private IExportMao                  exportMao;

    @Autowired
    private IUsersService               usersService;

    @Autowired
    private IEmailService               emailService;

    @Autowired
    private IDocumentService            documentService;

    @Autowired
    private ITenantProfileService       tenantProfileService;

    @Autowired
    @Qualifier(value = "mongoQueryParser")
    private IQueryParser                mongoQueryParser;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector           activeMQConnector;

    @Autowired
    private ContextAwareExecutorFactory executorFactory;

    @Override
    public CreateExportJobResponse createExportJob(CreateExportJobRequest request) {
        LOG.info("Creating export job {}", request);
        CreateExportJobResponse response = createExportJobInternal(request);
        if (response.isSuccessful()) {
            ExportJob exportJob = getExportJobById(response.getExportJobId());
            ExportJobEvent exportJobEvent = new ExportJobEvent();
            exportJobEvent.setId(exportJob.getId());
            exportJobEvent.setName(exportJob.getDescription());
            long start = System.currentTimeMillis();
            LOG.info("Queuing export event: {}", exportJobEvent);
            activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.EXPORT_QUEUE, exportJobEvent);
            LOG.info("Queued export event: {} in {} ms", exportJobEvent, System.currentTimeMillis() - start);
        }
        return response;
    }

    @Transactional
    public CreateExportJobResponse createExportJobInternal(CreateExportJobRequest request) {
        CreateExportJobResponse response = new CreateExportJobResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
            ExportConfig exportConfig = configuration.getExportConfigByName(request.getExportJobTypeName());
            ExportJobType exportJobType = configuration.getExportJobTypeByName(request.getExportJobTypeName());
            Integer exportsDataRangeInMonths = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getExportsDataRangeInMonths();
            Integer exportsDataSpanInDays = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getExportsDataSpanInDays();
            if (exportConfig == null || exportJobType == null) {
                context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME);
            } else if (ExportJob.Frequency.RECURRENT.name().equals(request.getFrequency()) && StringUtils.isBlank(request.getCronExpression())) {
                context.addError(WsResponseCode.INVALID_EXPORT_SCHEDULE_CONFIG);
            } else {
                for (String column : request.getExportColums()) {
                    ExportColumn exportColumn = exportConfig.getExportColumnById(column);
                    if (exportColumn == null) {
                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_COLUMN, "invalid column type :" + column);
                    }
                }
                Set<String> requestFilterIds = new HashSet<String>();

                if (request.getExportFilters() != null) {
                    for (WsExportFilter requestFilter : request.getExportFilters()) {
                        requestFilterIds.add(requestFilter.getId());
                    }
                }
                List<ExportFilter> filters = exportConfig.getExportFilters();
                for (ExportFilter filter : filters) {
                    if (filter.isRequired() && !requestFilterIds.contains(filter.getId())) {
                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER, "Missing required filter :" + filter.getName());
                    }
                }
                if (!context.hasErrors() && request.getExportFilters() != null && request.getExportFilters().size() > 0) {
                    for (WsExportFilter wsExportFilter : request.getExportFilters()) {
                        ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                        if (exportFilter == null) {
                            context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER, "invalid filter type :" + wsExportFilter.getId());
                        } else {
                            switch (exportFilter.getType()) {
                                case BOOLEAN:
                                    if (wsExportFilter.getChecked() == null) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "checked is mandatory for boolean value type");
                                    }
                                    break;
                                case TEXT:
                                    if (StringUtils.isEmpty(wsExportFilter.getText())) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "text is mandatory for text value type");
                                    }
                                    break;
                                case DATERANGE:
                                    if (wsExportFilter.getDateRange() == null) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateRange is mandatory for daterange value type");
                                    } else if (null != exportsDataSpanInDays && DateUtils.diff(wsExportFilter.getDateRange().getEnd(), wsExportFilter.getDateRange().getStart(), Resolution.DAY) > exportsDataSpanInDays) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateRange cannot span more than " + exportsDataSpanInDays + " days");
                                    } else if (null != exportsDataRangeInMonths
                                        && DateUtils.diff(DateUtils.getCurrentDate(), wsExportFilter.getDateRange().getStart(), Resolution.MONTH) > exportsDataRangeInMonths) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateRange start cannot be older than "
                                                + exportsDataRangeInMonths + " months");
                                    }
                                    break;
                                case DATETIME:
                                    if (wsExportFilter.getDateTime() == null) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateTime is mandatory for datetime value type");
                                    }
                                    break;
                                case SELECT:
                                    if (StringUtils.isEmpty(wsExportFilter.getSelectedValue())) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "selectedValue is mandatory for select value type");
                                    } else if (!exportFilter.getSelectValues().contains(wsExportFilter.getSelectedValue())) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_SELECT_VALUE, "selectedValue is not applicable for this filter");
                                    }
                                    break;
                                case MULTISELECT:
                                    if (wsExportFilter.getSelectedValues() == null || wsExportFilter.getSelectedValues().size() == 0) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "selectedValues is mandatory for multiselect value type");
                                    } else {
                                        for (String selectedValue : wsExportFilter.getSelectedValues()) {
                                            if (!exportFilter.getSelectValues().contains(selectedValue)) {
                                                context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_SELECT_VALUE, "selectedValue is not applicable for this filter");
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }

                if (!context.hasErrors() && null != exportsDataRangeInMonths) {
                    DateUtils.DateRange defaultDateRange = new DateUtils.DateRange();
                    defaultDateRange.setEnd(DateUtils.getCurrentTime());
                    defaultDateRange.setStart(DateUtils.addToDate(DateUtils.getCurrentDate(), Calendar.MONTH, exportsDataRangeInMonths * -1));
                    if (null == request.getExportFilters()) {
                        request.setExportFilters(new ArrayList<WsExportFilter>());
                    }
                    addDefaultFilters(request.getExportFilters(), exportConfig.getExportFilters(), defaultDateRange);
                }

                if (!context.hasErrors()) {
                    ExportJob exportJob = new ExportJob();
                    User user = usersService.getUserWithDetailsById(request.getUserId());
                    exportJob.setUsername(user.getUsername());
                    exportJob.setCronExpression(request.getCronExpression());
                    exportJob.setFrequency(request.getFrequency());
                    exportJob.setRequest(request);
                    exportJob.setExportJobTypeName(exportJobType.getName());
                    exportJob.setCreated(DateUtils.getCurrentTime());
                    exportJob.setStatusCode(ExportJob.Status.SCHEDULED);
                    exportJob.setUpdated(DateUtils.getCurrentTime());
                    exportJob.setNotificationEmail(request.getNotificationEmail());
                    if (StringUtils.isEmpty(request.getReportName())) {
                        exportJob.setDescription("Export Job " + exportConfig.getName() + " created by " + user.getUsername() + " at " + exportJob.getCreated());
                    } else {
                        exportJob.setDescription(request.getReportName());
                    }
                    exportJob = exportDao.addExportJob(exportJob);
                    response.setJobCode(exportJob.getId() + "-" + EncryptionUtils.md5Encode(exportJob.getId(), "unicom"));
                    response.setExportJobId(exportJob.getId());
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private void addDefaultFilters(List<WsExportFilter> requestFilters, List<ExportFilter> configFilters,
        DateUtils.DateRange defaultDateRange) {
        Set<String> requestFilterIds = new HashSet<String>();
        if (requestFilters != null) {
            for (WsExportFilter requestFilter : requestFilters) {
                requestFilterIds.add(requestFilter.getId());
            }
            for (ExportFilter filter : configFilters) {
                if (filter.getType().equals(ExportFilter.ValueType.DATERANGE) && !filter.isDefaultDateRangeRestrictionExempted()
                    && !requestFilterIds.contains(filter.getId())) {
                    WsExportFilter dateRangeFilter = new WsExportFilter();
                    dateRangeFilter.setId(filter.getId());
                    dateRangeFilter.setDateRange(defaultDateRange);
                    requestFilters.add(dateRangeFilter);
                }
            }
        }
    }

    @Override
    @Transactional
    public ExportJob getExportJobById(String exportJobId) {
        return exportDao.getExportJobById(exportJobId);
    }

    @Override
    public void submitExportJob(String exportJobId) {
        ExportJob exportJob = getExportJobById(exportJobId);
        executorFactory.getExecutor(SERVICE_NAME).submit(new ExportJobProcessorTask(exportJob));
    }

    private class ExportJobProcessorTask implements Runnable {
        private final ExportJob exportJob;

        public ExportJobProcessorTask(ExportJob exportJob) {
            this.exportJob = exportJob;
        }

        @Override
        public void run() {
            try {
                executeExportJob(exportJob.getId());
                if (!ExportJob.Status.FAILED.equals(exportJob.getStatusCode())) {
                    LOG.info("Sending email for export job {}", exportJob);
                    sendExportJobMail(exportJob);
                } else {
                    LOG.warn("Ignoring export job email as export failed.");
                }
            } catch (Exception e) {
                LOG.error("Error executing export job: {}", exportJob.getId());
                LOG.error("Stack trace: ", e);
                e.printStackTrace();
            }
        }

        private void sendExportJobMail(ExportJob exportJob) {
            List<String> recipients = new ArrayList<String>();
            ExportSubscriber exportSubscriber = getExportSubscribers(exportJob.getId());
            for (String username : exportSubscriber.getUsernames()) {
                recipients.add(usersService.getUserByUsername(username).getEmail());
            }

            LOG.info("Email recipients for export job {} : {}", exportJob, recipients);
            if (recipients.size() > 0) {
                EmailMessage message = new EmailMessage(recipients, EmailTemplate.Type.EXPORT.name());
                message.addTemplateParam("exportJob", exportJob);
                emailService.send(message);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ExportJob executeExportJob(String exportJobId) throws Exception {
        return executeExportJobInternal(exportJobId);
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private ExportJob executeExportJobInternal(String exportJobId) throws Exception {
        ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
        ExportJob exportJob = getExportJobById(exportJobId);
        LOG.info("Executing export job: {}", exportJob);
        Long startTime = System.currentTimeMillis();
        ExportJobType exportJobType = configuration.getExportJobTypeByName(exportJob.getExportJobTypeName());
        ExportConfig exportConfig = configuration.getExportConfigByName(exportJob.getExportJobTypeName());
        String filePath = getFilePath(exportJob);
        String exportDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getExportDirectoryPath();
        File file = new File(FileUtils.normalizeFilePath(exportDirectory, filePath));
        long totalRows = -1;

        if (exportConfig.isNoSql()) {
            totalRows = executeNoSqlExportJob(exportJob, exportConfig, file);
        } else {
            totalRows = executeSqlExportJob(exportJob, exportConfig, file);
        }
        exportJob.setTaskDurationInSec((int) ((System.currentTimeMillis() - startTime) / 1000));
        if (exportJob.isSuccessful()) {
            exportJob.setMessage("Export successful");
            String exportFileUrl = null;
            if (StringUtils.isNotBlank(exportJobType.getPostProcessorScriptName())) {
                ScraperScript script = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(exportJobType.getPostProcessorScriptName());
                LOG.info("Running post processor for export job {}", exportJob);
                try {
                    ScriptExecutionContext context = ScriptExecutionContext.current();
                    context.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
                    context.addVariable("filePath", file.getAbsolutePath());
                    TenantProfileVO tenantProfile = tenantProfileService.getTenantProfileByCode(UserContext.current().getTenant().getCode());
                    context.addVariable("tenantProfile", tenantProfile);
                    script.execute();
                    exportFileUrl = ScriptExecutionContext.current().getScriptOutput();
                } catch (ScriptExecutionException see) {
                    exportJob.setStatusCode(ExportJob.Status.FAILED);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            } else {
                exportFileUrl = documentService.uploadFile(file, "unicommerce-export");
            }
            exportJob.setExportFilePath(exportFileUrl);
            exportJob.setExportCount(totalRows);
        }
        exportDao.updateExportJob(exportJob);
        sendExportJobMail(exportJob);
        LOG.info("Completed export job: {} in {} ms", exportJob, System.currentTimeMillis() - startTime);
        return exportJob;
    }

    private long executeNoSqlExportJob(ExportJob exportJob, ExportConfig exportConfig, File file) throws Exception {
        long totalRows = -1;
        User user = usersService.getUserByUsername(exportJob.getUsername());
        CreateExportJobRequest request = exportJob.getRequest();
        int start = 0;
        int maxResults = 5000;
        GenerateMongoQueryResponse generateMongoQueryResponse = getGenerateMongoQueryResponse(exportConfig, user, request.getExportFilters(), null, null, start, maxResults, false);

        if (!exportConfig.isResultCountNotSupported()) {
            totalRows = exportMao.executeCountQuery(generateMongoQueryResponse.getQuery(), generateMongoQueryResponse.getClazz());
            if (totalRows > EXPORT_UPPER_LIMIT) {
                exportJob.setMileStoneCount((int) totalRows);
                exportJob.setStatusCode(ExportJob.Status.COMPLETE);
                String message = "Export Rejected as Export Limit Exceeded-" + EXPORT_UPPER_LIMIT;
                exportJob.setMessage(message);
                LOG.warn(message);
                return -1;
            }
        }

        // Fetch the result
        file.createNewFile();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        try {
            bufferedWriter.append(getHeaderRow(exportConfig, request));
            while (true) {
                generateMongoQueryResponse = getGenerateMongoQueryResponse(exportConfig, user, request.getExportFilters(), null, null, start, maxResults, false);
                List<?> resultObjects = exportMao.executeQuery(generateMongoQueryResponse.getQuery(), generateMongoQueryResponse.getPagination(),
                        generateMongoQueryResponse.getClazz());
                for (int i = 0; i < resultObjects.size(); i++) {
                    Object resultObject = resultObjects.get(i);
                    StringBuilder builder = new StringBuilder();
                    for (String columnId : request.getExportColums()) {
                        ExportColumn exportColumn = exportConfig.getExportColumnById(columnId);
                        if (exportColumn.isExportable()) {
                            Object value = null;
                            Map<String, Object> contextParams = new HashMap<>();
                            contextParams.put("entity", resultObject);
                            try {
                                value = exportColumn.getCalculationExpression().evaluate(contextParams);
                            } catch (Exception e) {
                                LOG.error("error evaluating expression while executing export job", e);
                            }
                            if (value == null) {
                                builder.append(StringUtils.EMPTY_STRING);
                            } else if (value instanceof Timestamp) {
                                builder.append(StringEscapeUtils.escapeCsv(DateUtils.dateToString(new Date(((Timestamp) value).getTime()), "yyyy-MM-dd HH:mm:ss")));
                            } else {
                                builder.append(StringEscapeUtils.escapeCsv(String.valueOf(value)));
                            }
                            builder.append(exportConfig.getDelimiter());
                        }
                    }
                    builder.deleteCharAt(builder.length() - 1).append(System.getProperty("line.separator"));
                    bufferedWriter.append(builder);
                }
                bufferedWriter.flush();
                if (resultObjects.size() < maxResults) {
                    exportJob.setMileStone("Processing complete", resultObjects.size());
                    exportJob.setSuccessful(true);
                    exportJob.setStatusCode(ExportJob.Status.COMPLETE);
                    break;
                } else {
                    start += maxResults;
                    exportJob.setMileStone(start + " rows processed", maxResults);
                }
                exportDao.updateExportJob(exportJob);
            }
        } finally {
            bufferedWriter.close();
        }
        return totalRows;
    }

    private long executeSqlExportJob(ExportJob exportJob, ExportConfig exportConfig, File file) throws Exception {
        long totalRows = -1;
        CreateExportJobRequest request = exportJob.getRequest();
        User user = usersService.getUserByUsername(exportJob.getUsername());
        Map<String, Object> filterValues = getFilterParameters(user.getVendor(), exportConfig, request.getExportFilters());
        if (!exportConfig.isResultCountNotSupported()) {
            // Fetch the result count
            Query countQuery = exportDao.prepareResultCountQuery(exportConfig, request.getExportFilters(), filterValues, false);
            countQuery.setMaxResults(1);
            totalRows = ((BigInteger) countQuery.uniqueResult()).longValue();
            if (totalRows > EXPORT_UPPER_LIMIT) {
                exportJob.setMileStoneCount((int) totalRows);
                exportJob.setStatusCode(ExportJob.Status.COMPLETE);
                String message = "Export Rejected as Export Limit Exceeded-" + EXPORT_UPPER_LIMIT;
                exportJob.setMessage(message);
                LOG.warn(message);
                return -1;
            }
            exportJob.setMileStoneCount((int) totalRows);
            LOG.info("Total rows for export job {} : {}", exportJob, totalRows);
        }
        cleanColumnsList(exportConfig, request.getExportColums());
        for (ExportColumn hiddenColumn : exportConfig.getHiddenColumns()) {
            if (!request.getExportColums().contains(hiddenColumn.getId())) {
                request.getExportColums().add(hiddenColumn.getId());
            }
        }
        Query query = exportDao.prepareQuery(exportConfig, request.getExportColums(), request.getExportFilters(), filterValues, null, false);
        file.createNewFile();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        try {
            bufferedWriter.append(getHeaderRow(exportConfig, request));
            int firstResult = 0;
            int maxResults = 5000;
            while (true) {
                query.setFirstResult(firstResult);
                query.setMaxResults(maxResults);
                List<Map<String, Object>> results = exportDao.executeAnonymousQuery(query);
                for (Map<String, Object> row : results) {
                    StringBuilder builder = new StringBuilder();
                    for (String columnId : request.getExportColums()) {
                        ExportColumn exportColumn = exportConfig.getExportColumnById(columnId);
                        if (exportColumn.isExportable()) {
                            Object value = null;
                            if (exportColumn.isCalculated()) {
                                Map<String, Object> contextParams = new HashMap<String, Object>();
                                contextParams.putAll(row);
                                contextParams.putAll(filterValues);
                                try {
                                    value = exportColumn.getCalculationExpression().evaluate(contextParams);
                                } catch (Exception e) {
                                    LOG.error("error evaluating expression while executing export job", e);
                                }
                            } else {
                                value = row.get(columnId);
                            }
                            if (value == null) {
                                builder.append(StringUtils.EMPTY_STRING);
                            } else if (value instanceof Timestamp) {
                                builder.append(StringEscapeUtils.escapeCsv(DateUtils.dateToString(new Date(((Timestamp) value).getTime()), "yyyy-MM-dd HH:mm:ss")));
                            } else {
                                builder.append(StringEscapeUtils.escapeCsv(String.valueOf(value)));
                            }
                            builder.append(exportConfig.getDelimiter());
                        }
                    }
                    builder.deleteCharAt(builder.length() - 1).append(System.getProperty("line.separator"));
                    bufferedWriter.append(builder);
                }
                bufferedWriter.flush();
                if (results.size() < maxResults) {
                    exportJob.setMileStone("Processing complete", results.size());
                    exportJob.setSuccessful(true);
                    exportJob.setStatusCode(ExportJob.Status.COMPLETE);
                    break;
                } else {
                    firstResult += maxResults;
                    exportJob.setMileStone(firstResult + " rows processed", maxResults);
                }
                exportDao.updateExportJob(exportJob);
            }
        } finally {
            bufferedWriter.close();
        }
        return totalRows;
    }

    @Override
    public void updateExportJob(ExportJob exportJob) {
        exportDao.updateExportJob(exportJob);
    }

    private void sendExportJobMail(ExportJob exportJob) {
        if (exportJob.getNotificationEmail() != null) {
            List<String> recipients = Arrays.asList(exportJob.getNotificationEmail());
            LOG.info("Email recipients for export job {} : {}", exportJob, recipients);
            if (recipients.size() > 0) {
                EmailMessage message = new EmailMessage(recipients, EmailTemplate.Type.EXPORT.name());
                message.addTemplateParam("exportJob", exportJob);
                emailService.send(message);
            }
        }
    }

    private void cleanColumnsList(ExportConfig exportConfig, List<String> columns) {
        Iterator<String> columnIterator = columns.iterator();
        while (columnIterator.hasNext()) {
            if (exportConfig.getExportColumnById(columnIterator.next()) == null) {
                columnIterator.remove();
            }
        }
    }

    @Override
    public Map<String, Object> getFilterParameters(Vendor vendor, ExportConfig exportConfig, List<WsExportFilter> filters) {
        Map<String, Object> filterValues = exportDao.getFilterParameters(exportConfig, filters);
        if (filterValues == null) {
            filterValues = new HashMap<>();
        }
        if (vendor != null) {
            filterValues.put("vendorId", vendor.getId());
        }
        filterValues.put("facilities", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds());
        filterValues.put("facilityId", UserContext.current().getFacilityId());
        filterValues.put("tenantId", UserContext.current().getTenantId());
        return filterValues;
    }

    private String getHeaderRow(ExportConfig exportConfig, CreateExportJobRequest request) {
        StringBuilder builder = new StringBuilder();
        for (String exportColumnId : request.getExportColums()) {
            ExportColumn exportColumn = exportConfig.getExportColumnById(exportColumnId);
            if (exportColumn.isExportable()) {
                builder.append(StringEscapeUtils.escapeCsv(exportColumn.getName())).append(exportConfig.getDelimiter());
            }
        }
        builder.deleteCharAt(builder.length() - 1).append(System.getProperty("line.separator"));
        return builder.toString();
    }

    /**
     * @param exportJob
     */
    private String getFilePath(ExportJob exportJob) {
        return new StringBuilder().append(exportJob.getId()).append('_').append(EncryptionUtils.md5Encode(String.valueOf(System.nanoTime()))).append(".csv").toString();
    }

    @Override
    @Transactional
    @MarkDirty(values = { ExportJobConfiguration.class })
    public EditExportJobTypeResponse editExportJobType(EditExportJobTypeRequest request) {
        EditExportJobTypeResponse response = new EditExportJobTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ExportJobType exportJobType = getExportJobTypeByName(request.getName());
            if (exportJobType == null) {
                context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "Invalid Export Job Type");
            } else {
                exportJobType.setExportJobConfig(request.getExportJobConfig());
                exportJobType.setEnabled(request.isEnabled());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public ExportJobType getExportJobTypeByName(String name) {
        return exportDao.getExportJobTypeByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExportJob> getExportJobByFrequency(String frequency) {
        return exportDao.getExportJobByFrequency(frequency);
    }

    @Override
    @Transactional(readOnly = true)
    public ExportSubscriber getExportSubscribers(String exportJobId) {
        return exportDao.getExportSubscribers(exportJobId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExportJobType> getExportJobTypes() {
        return exportDao.getExportJobTypes();
    }

    @Override
    @Transactional(readOnly = true)
    public GetExportSubscriptionResponse getExportJobsBySubscriber(String username) {
        GetExportSubscriptionResponse response = new GetExportSubscriptionResponse();
        List<ExportSubscriber> exportSubscriptions = exportDao.getExportJobsBySubscriber(username);
        Map<String, Boolean> subscriptions = new HashMap<String, Boolean>();
        for (ExportSubscriber exportSubscriber : exportSubscriptions) {
            subscriptions.put(exportSubscriber.getExportJobId(), exportSubscriber.isSubscribed());
        }

        for (ExportJob exportJob : exportDao.getRecurrentExportJobs()) {
            try {
                ExportJobType exportJobType = getExportJobTypeByName(exportJob.getExportJobTypeName());
                ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
                ExportConfig exportConfig = configuration.getExportConfigByJobTypeId(exportJobType.getId());
                if (exportConfig != null) {
                    GetExportSubscriptionResponse.RecurrentExportJobDTO exportJobDTO = new GetExportSubscriptionResponse.RecurrentExportJobDTO();
                    exportJobDTO.setExportJobId(exportJob.getId());
                    exportJobDTO.setEnabled(exportJob.isEnabled());
                    exportJobDTO.setAccessResourceName(exportJobType.getAccessResourceName());
                    exportJobDTO.setExportJobTypeName(exportJobType.getName());
                    exportJobDTO.setFrequency(exportJob.getCronExpression());
                    exportJobDTO.setScheduledTime(DateUtils.getNextRun(exportJob.getCronExpression()));
                    exportJobDTO.setReportName(exportJob.getDescription());
                    exportJobDTO.setUserName(exportJob.getUsername());
                    CreateExportJobRequest request = exportJob.getRequest();

                    for (WsExportFilter wsExportFilter : request.getExportFilters()) {
                        exportJobDTO.getExportFilters().add(exportConfig.getExportFilterById(wsExportFilter.getId()));
                    }
                    exportJobDTO.setExportColumns(request.getExportColums());
                    exportJobDTO.setSubscribed(subscriptions.get(exportJob.getId()) == null ? false : subscriptions.get(exportJob.getId()));
                    response.getExportJobs().add(exportJobDTO);
                }
            } catch (Exception e) {
                LOG.error("Error adding export job: {}", exportJob.getExportJobTypeName());
            }
        }
        response.setSuccessful(true);
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public AddExportSubscriptionResponse addExportSubscriber(AddExportSubscriberRequest request) {
        AddExportSubscriptionResponse response = new AddExportSubscriptionResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            exportDao.deleteExportSubscribers(request.getExportJobId());
            List<String> usernames = Arrays.asList(request.getSubscribingUsers());
            ExportSubscriber subscriber = new ExportSubscriber(usernames, request.getExportJobId(), DateUtils.getCurrentDate(), DateUtils.getCurrentTime(), true);
            exportDao.addExportSubscriber(subscriber);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public EditExportsSubscriptionResponse updateExportsSubscription(EditExportsSubscriptionRequest request) {
        EditExportsSubscriptionResponse response = new EditExportsSubscriptionResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            User user = usersService.getUserById(request.getUserId());
            for (ExportSubscription exportSubscription : request.getExportSubscriptions()) {
                LOG.info("Updating subscriber {} for export job {}", user.getUsername(), exportSubscription.getExportJobId());
                ExportSubscriber subscriber = exportDao.getExportSubscriber(exportSubscription.getExportJobId(), user.getUsername());
                if (subscriber != null) {
                    subscriber.setSubscribed(exportSubscription.isSubscribed());
                } else if (exportSubscription.isSubscribed()) {
                    subscriber = new ExportSubscriber(new ArrayList<>(Arrays.asList(user.getUsername())), exportSubscription.getExportJobId(), DateUtils.getCurrentDate(),
                            DateUtils.getCurrentTime(), true);
                    exportDao.addExportSubscriber(subscriber);
                }
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditExportJobResponse editExportJob(EditExportJobRequest request) {
        EditExportJobResponse response = new EditExportJobResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            ExportJob exportJob = exportDao.getExportJobById(request.getExportJobId());
            exportJob.setEnabled(request.getEnabled());
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    @MarkDirty(values = { ExportJobConfiguration.class })
    public CloneExportJobTypeResponse cloneExportJobType(CloneExportJobTypeRequest request) {
        CloneExportJobTypeResponse response = new CloneExportJobTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ExportJobType exportJobTypeToClone = exportDao.getExportJobTypeByName(request.getCloneExportJobTypeName());
            if (exportJobTypeToClone != null) {
                context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "Duplicate export job type name");
            }
            if (!context.hasErrors()) {
                ExportJobType exportJobType = exportDao.getExportJobTypeByName(request.getExportJobTypeName());
                if (exportJobType == null) {
                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "Invalid export job type name");
                }
                if (exportJobType != null && ExportJobType.Type.TABLE.equals(exportJobType.getType())) {
                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "Clone can only be created for type EXPORT");
                }
                if (!context.hasErrors()) {
                    ExportJobType clonedExportJobType = new ExportJobType();
                    clonedExportJobType.setName(request.getCloneExportJobTypeName());
                    clonedExportJobType.setDisplayName(request.getCloneExportJobTypeName());
                    clonedExportJobType.setAccessResourceName(exportJobType.getAccessResourceName());
                    clonedExportJobType.setExportJobConfig(exportJobType.getExportJobConfig());
                    clonedExportJobType.setEnabled(exportJobType.isEnabled());
                    clonedExportJobType.setNoSql(exportJobType.isNoSql());
                    clonedExportJobType.setTenant(exportJobType.getTenant());
                    clonedExportJobType.setCreated(DateUtils.getCurrentTime());
                    clonedExportJobType.setType(exportJobType.getType());
                    clonedExportJobType.setPostProcessorScriptName(exportJobType.getPostProcessorScriptName());
                    exportDao.createExportJobType(clonedExportJobType);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetExportJobStatusResponse getExportJobStatus(GetExportJobStatusRequest request) {
        GetExportJobStatusResponse response = new GetExportJobStatusResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<String> exportJobCodeTokens = StringUtils.split(request.getJobCode(), "-");
            String exportJobId = exportJobCodeTokens.get(0);
            String md5Code = exportJobCodeTokens.get(1);
            if (!md5Code.equals(EncryptionUtils.md5Encode(exportJobId, "unicom"))) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid Job Code");
            } else {
                ExportJob exportJob = getExportJobById(exportJobId);
                ExportJob.Status statusCode = exportJob.getStatusCode();
                response.setStatus(statusCode.name());
                if (exportJob.isSuccessful()) {
                    response.setFilePath(exportJob.getExportFilePath());
                    response.setSuccessful(true);
                }
            }

        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DeleteExportSubscriptionResponse deleteExportSubxcription(DeleteExportSubscriptionRequest request) {
        DeleteExportSubscriptionResponse response = new DeleteExportSubscriptionResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            if (exportDao.getExportSubscription(request.getExportJobId()).size() > 0) {
                exportDao.deleteExportSubscription(request.getExportJobId());
                response.setSuccessful(true);
            } else {
                context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "Invalid expord job id");
                response.setSuccessful(false);
            }
        }
        return response;
    }

    @Override
    public GetDatatableResultResponse getDatatableResult(GetDatatableResultRequest request) {
        RequiredDataFromPrimaryDS dataFromPrimary = prefetchDataFromPrimaryDS(request);
        // Fetch data from appropriate DS depending upon the property.
        return getDatatableResultResponse(request, dataFromPrimary, false);
    }

    private GetDatatableResultResponse getDatatableResultResponse(GetDatatableResultRequest request, RequiredDataFromPrimaryDS dataFromPrimary, boolean countOnly) {
        if (CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDatatableViaReplicationEnabled()) {
            return getDatatableResultInternalFromSecondaryDS(request, dataFromPrimary, countOnly);
        } else {
            return getDatatableResultInternalFromPrimaryDS(request, dataFromPrimary, countOnly);
        }
    }

    @Transactional(readOnly = true)
    private GetDatatableResultResponse getDatatableResultInternalFromPrimaryDS(GetDatatableResultRequest request, RequiredDataFromPrimaryDS dataFromPrimary, boolean countOnly) {
        return getDatatableResultInternal(request, dataFromPrimary, countOnly);
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private GetDatatableResultResponse getDatatableResultInternalFromSecondaryDS(GetDatatableResultRequest request, RequiredDataFromPrimaryDS dataFromPrimary, boolean countOnly) {
        return getDatatableResultInternal(request, dataFromPrimary, countOnly);
    }

    @Transactional(readOnly = true)
    private RequiredDataFromPrimaryDS prefetchDataFromPrimaryDS(GetDatatableResultRequest request) {
        RequiredDataFromPrimaryDS dataFromPrimary = new RequiredDataFromPrimaryDS();
        ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
        dataFromPrimary.setDatatableConfig((DatatableConfig) configuration.getExportConfigByName(request.getName()));
        dataFromPrimary.setVendor(usersService.getUserWithDetailsById(request.getUserId()).getVendor());
        return dataFromPrimary;
    }

    /**
     * When rendering datatable from secondary DS (aka replication), we will prefetch required data from primary and
     * give it to {@Code ExportServiceImpl#getDatatableResultInternal}
     */
    private class RequiredDataFromPrimaryDS {

        private DatatableConfig datatableConfig;

        private Vendor          vendor;

        public DatatableConfig getDatatableConfig() {
            return datatableConfig;
        }

        public void setDatatableConfig(DatatableConfig datatableConfig) {
            this.datatableConfig = datatableConfig;
        }

        public Vendor getVendor() {
            return vendor;
        }

        public void setVendor(Vendor vendor) {
            this.vendor = vendor;
        }
    }

    private GetDatatableResultResponse getDatatableResultInternal(GetDatatableResultRequest request, RequiredDataFromPrimaryDS dataFromPrimary, boolean countOnly) {
        GetDatatableResultResponse response = new GetDatatableResultResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            DatatableConfig exportConfig = dataFromPrimary.getDatatableConfig();
            Integer datatablesDataRangeInMonths =  ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getDatatablesDataRangeInMonths();

            if (request.getFilters() != null && request.getFilters().size() > 0) {
                for (WsExportFilter wsExportFilter : request.getFilters())
                {
                    ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                    if (exportFilter == null) {
                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER,
                            "invalid filter type :" + wsExportFilter.getId());
                        break;
                    } else {
                        if (null != datatablesDataRangeInMonths && exportFilter.getType().equals(ExportFilter.ValueType.DATERANGE)
                            && DateUtils.diff(DateUtils.getCurrentDate(), wsExportFilter.getDateRange().getStart(), Resolution.MONTH)
                            > datatablesDataRangeInMonths) {
                            context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE,
                                "dateRange start cannot be older than "
                                    + datatablesDataRangeInMonths + " months");
                            break;
                        }
                    }
                }
            }

            if (!context.hasErrors()) {
                cleanColumnsList(exportConfig, request.getColumns());
                if (exportConfig.isNoSql()) {
                    response = getNoSqlDatatableResult(request, exportConfig, countOnly);
                } else {
                    if (null != datatablesDataRangeInMonths) {
                        DateUtils.DateRange defaultDateRange = new DateUtils.DateRange();
                        defaultDateRange.setEnd(DateUtils.getCurrentTime());
                        defaultDateRange.setStart(DateUtils.addToDate(DateUtils.getCurrentDate(), Calendar.MONTH,
                            datatablesDataRangeInMonths * -1));
                        if (null == request.getFilters()) {
                            request.setFilters(new ArrayList<WsExportFilter>());
                        }
                        addDefaultFilters(request.getFilters(), exportConfig.getExportFilters(), defaultDateRange);
                    }
                    response = getSqlDatatableResult(request, exportConfig, dataFromPrimary, countOnly);
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private GetDatatableResultResponse getSqlDatatableResult(GetDatatableResultRequest request, DatatableConfig exportConfig, RequiredDataFromPrimaryDS dataFromPrimary,
            boolean countOnly) {
        GetDatatableResultResponse response = new GetDatatableResultResponse();
        Vendor vendor = dataFromPrimary.getVendor();
        Map<String, Object> filterValues = getFilterParameters(vendor, exportConfig, request.getFilters());
        if (!countOnly) {
            Query query = exportDao.prepareQuery(exportConfig, request.getColumns(), request.getFilters(), filterValues, request.getSortColumns(), true);
            query.setFirstResult(request.getStart());
            query.setMaxResults(request.getNoOfResults());
            List<Map<String, Object>> results;
            List<Map<String, Object>> dirtyRecords = exportDao.executeAnonymousQuery(query);
            if (exportConfig.isServeFromView()) {
                // fetch and merge non-dirty records from view
                results = mergeRecords(exportConfig, dirtyRecords, getNonDirtyRecords(exportConfig, request, dirtyRecords.size(), filterValues), request);
            } else {
                results = dirtyRecords;
            }
            int rowNo = request.getStart() + 1;
            for (Map<String, Object> row : results) {
                WsDatatableRow datatableRow = new WsDatatableRow();
                datatableRow.setRowNo(rowNo++);
                datatableRow.setValues(getResultRowValues(exportConfig, filterValues, request.getColumns(), row));
                response.getRows().add(datatableRow);
            }
        }
        if (countOnly || (request.isFetchResultCount() && !exportConfig.isResultCountNotSupported())) {
            Query query = exportDao.prepareResultCountQuery(exportConfig, request.getFilters(), filterValues, true);
            query.setMaxResults(1);
            long dirtyRecordsCount = ((BigInteger) query.uniqueResult()).longValue();
            long nonDirtyRecordsCount = 0;
            if (exportConfig.isServeFromView()) {
                nonDirtyRecordsCount = exportMao.getNonDirtyRecordsCount(exportConfig, request, filterValues);
            }
            response.setResultCount(dirtyRecordsCount + nonDirtyRecordsCount);
        }
        return response;
    }

    /**
     * Get appropriate number of non-dirty records from view.
     *
     * @param exportConfig
     * @param request
     * @param dirtyRecordsCount
     * @return
     */
    private List<Map<String, Object>> getNonDirtyRecords(ExportConfig exportConfig, GetDatatableResultRequest request, int dirtyRecordsCount, Map<String, Object> filterValues) {
        GenerateNoSqlQueryRequest mongoQueryRequest = new GenerateNoSqlQueryRequest(true);
        filterValues.put("viewDirty", false);
        mongoQueryRequest.getQueryParams().putAll(filterValues);
        NoSqlQuery mongoQuery = new NoSqlQuery();
        mongoQuery.setEntity(exportConfig.getViewCollectionName());
        // set filters
        if (request.getFilters() != null) {
            for (WsExportFilter wsExportFilter : request.getFilters()) {
                ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                NoSqlCriteria noSqlCriteria = NoSqlCriteria.compile(exportFilter.getConditionForView());
                mongoQuery.getCriteria().add(noSqlCriteria);
            }
        }
        for (ExportFilter hiddenFilter : exportConfig.getHiddenFilters()) {
            NoSqlCriteria noSqlCriteria = NoSqlCriteria.compile(hiddenFilter.getConditionForView());
            mongoQuery.getCriteria().add(noSqlCriteria);
        }
        mongoQuery.getCriteria().add(NoSqlCriteria.compile("{'name' : 'viewDirty', 'operator' : 'IS', 'value' : '#{#viewDirty}'}"));
        // set sorters
        if (request.getSortColumns() != null) {
            for (WsSortColumn wsSortColumn : request.getSortColumns()) {
                GenerateQueryRequest.Sort sort = new GenerateQueryRequest.Sort();
                sort.setColumn(wsSortColumn.getColumn());
                sort.setDescending(wsSortColumn.isDescending());
                mongoQueryRequest.getSortList().add(sort);
                NoSqlSorter noSqlSorter = new NoSqlSorter(wsSortColumn.getColumn(), wsSortColumn.getSortDirection().trim());
                mongoQuery.getSorters().add(noSqlSorter);
            }
        }
        mongoQueryRequest.setNoSqlQuery(mongoQuery);
        mongoQueryRequest.setStart((request.getStart() - dirtyRecordsCount) > 0 ? (request.getStart() - dirtyRecordsCount) : 0);
        mongoQueryRequest.setNumberOfResults(dirtyRecordsCount + request.getNoOfResults());
        GenerateMongoQueryResponse mongoQueryRes = (GenerateMongoQueryResponse) mongoQueryParser.generateQuery(mongoQueryRequest);
        return exportMao.executeQueryAndReturnMap(mongoQueryRes.getQuery(), mongoQueryRes.getPagination(), mongoQuery.getEntity());
    }

    /**
     * Merge dirty and non-dirty records and return final list of records to be displayed in datatable.
     *
     * @param dirtyRecords
     * @param nonDirtyRecords
     * @param request
     * @return
     */
    private List<Map<String, Object>> mergeRecords(ExportConfig exportConfig, List<Map<String, Object>> dirtyRecords, List<Map<String, Object>> nonDirtyRecords,
            GetDatatableResultRequest request) {
        dirtyRecords.addAll(nonDirtyRecords);
        Collections.sort(dirtyRecords, new ExportResultComparator(exportConfig, request.getSortColumns()));
        return dirtyRecords.subList(0, dirtyRecords.size() > request.getNoOfResults() ? request.getNoOfResults() : dirtyRecords.size());
    }

    private GetDatatableResultResponse getNoSqlDatatableResult(GetDatatableResultRequest request, DatatableConfig exportConfig, boolean countOnly) {
        GetDatatableResultResponse response = new GetDatatableResultResponse();
        User user = usersService.getUserWithDetailsById(request.getUserId());

        GenerateMongoQueryResponse generateMongoQueryResponse = getGenerateMongoQueryResponse(exportConfig, user, request.getFilters(), request.getSortColumns(),
                exportConfig.getDefaultSortBy(), request.getStart(), request.getNoOfResults(), countOnly);

        // Fetch the result
        if(!countOnly){
            List<?> resultObjects = exportMao.executeQuery(generateMongoQueryResponse.getQuery(), generateMongoQueryResponse.getPagination(), generateMongoQueryResponse.getClazz());
            // Now filter all the columns that we need.
            for (int i = 0; i < resultObjects.size(); i++) {
                Object[] rowValues = new Object[request.getColumns().size()];
                Object resultObject = resultObjects.get(i);
                WsDatatableRow row = new WsDatatableRow();
                row.setRowNo(i + 1);
                int index = 0;
                for (String columnId : request.getColumns()) {
                    ExportColumn exportColumn = exportConfig.getExportColumnById(columnId);
                    Object value = null;
                    Map<String, Object> contextParams = new HashMap<String, Object>();
                    contextParams.put("entity", resultObject);
                    try {
                        value = exportColumn.getCalculationExpression().evaluate(contextParams);
                    } catch (Exception e) {
                        LOG.error("error evaluating expression while executing export job", e);
                    }
                    rowValues[index++] = value;
                }
                row.setValues(rowValues);
                response.getRows().add(row);
            }
        }

        if (countOnly || (request.isFetchResultCount() && !exportConfig.isResultCountNotSupported())) {
            response.setResultCount(exportMao.executeCountQuery(generateMongoQueryResponse.getQuery(), generateMongoQueryResponse.getClazz()));
        }
        return response;
    }

    private GenerateMongoQueryResponse getGenerateMongoQueryResponse(ExportConfig exportConfig, User user, List<WsExportFilter> exportFilters, List<WsSortColumn> sortColumns,
            String defaultSortByColumn, int start, int numOfResults, boolean countOnly) {
        GenerateNoSqlQueryFromJsonRequest mongoQueryReq = new GenerateNoSqlQueryFromJsonRequest();
        mongoQueryReq.setQueryString(exportConfig.getQueryString());
        mongoQueryReq.setStart(start);
        mongoQueryReq.setNumberOfResults(numOfResults);
        mongoQueryReq.getQueryParams().put("context", UserContext.current());
        // filters
        mongoQueryReq.setFilterIdToValueMap(getFilterParameters(user.getVendor(), exportConfig, exportFilters));

        if (exportFilters != null) {
            List<GenerateQueryRequest.Filter> filterList = new ArrayList<>();
            for (WsExportFilter wsExportFilter : exportFilters) {
                ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                GenerateQueryRequest.Filter filter = new GenerateQueryRequest.Filter();
                filter.setId(wsExportFilter.getId());
                filter.setCondition(exportFilter.getCondition());
                filterList.add(filter);
            }
            mongoQueryReq.setFilterList(filterList);
        }
        // sorting
        if (countOnly){
            List<GenerateQueryRequest.Sort> sortList = new ArrayList<>();
            if (sortColumns != null) {
                for (WsSortColumn wsSortColumn : sortColumns) {
                    GenerateQueryRequest.Sort sort = new GenerateQueryRequest.Sort();
                    sort.setColumn(wsSortColumn.getColumn());
                    sort.setDescending(wsSortColumn.isDescending());
                    sortList.add(sort);
                }
            } else if (defaultSortByColumn != null) {
                WsSortColumn wsSortColumn = new Gson().fromJson(defaultSortByColumn, WsSortColumn.class);
                GenerateQueryRequest.Sort sort = new GenerateQueryRequest.Sort();
                sort.setColumn(wsSortColumn.getColumn());
                sort.setDescending(wsSortColumn.isDescending());
                sortList.add(sort);
            }
            mongoQueryReq.setSortList(sortList);
        }
        return (GenerateMongoQueryResponse) mongoQueryParser.generateQuery(mongoQueryReq);
    }

    private Object[] getResultRowValues(ExportConfig exportConfig, Map<String, Object> filterValues, List<String> columns, Map<String, Object> row) {
        Object[] rowValues = new Object[columns.size()];
        int index = 0;
        for (String columnId : columns) {
            ExportColumn exportColumn = exportConfig.getExportColumnById(columnId);
            Object value = null;
            if (exportColumn.isCalculated()) {
                Map<String, Object> contextParams = new HashMap<String, Object>();
                contextParams.putAll(row);
                contextParams.putAll(filterValues);
                if (exportColumn.getValue().toLowerCase().startsWith("query:")) {
                    contextParams.put("tenantId", UserContext.current().getTenantId());
                    contextParams.put("facilityId", UserContext.current().getFacilityId());
                    contextParams.put("facilities", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds());
                    value = executeAnonymousQuery(exportColumn.getValue().substring("query:".length()), contextParams).get(0);
                } else {
                    try {
                        value = exportColumn.getCalculationExpression().evaluate(contextParams);
                    } catch (Exception e) {
                        LOG.error("error evaluating expression while executing export job", e);
                    }
                }
            } else if (exportColumn.isNonSerializableCustomField() && row.get(columnId) != null && StringUtils.isNotBlank(row.get(columnId).toString())) {
                CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
                CustomFieldMetadataVO customFieldMetadata = configuration.getCustomFieldByEntityByFieldName(exportColumn.getEntity(), exportColumn.getFieldName());
                if (customFieldMetadata != null && customFieldMetadata.getValueType().equalsIgnoreCase("date")) {
                    value = new Date(Long.parseLong(row.get(columnId).toString()));
                } else {
                    value = row.get(columnId);
                }
            } else {
                value = row.get(columnId);
            }
            rowValues[index++] = value;
        }
        return rowValues;
    }

    @Override
    @Transactional
    public List<Object[]> executeAnonymousQuery(String query, Map<String, Object> parameters) {
        return exportDao.executeAnonymousQuery(query, parameters);
    }

    @Override
    public List<ExportJob> getUserExports(String username) {
        return exportDao.getUserExports(username);
    }

    @Override
    public GetDatatableResultCountResponse getDatatableResultCount(GetDatatableResultCountRequest request) {
        RequiredDataFromPrimaryDS dataFromPrimary = prefetchDataFromPrimaryDS(request);
        // Fetch data from appropriate DS depending upon the property.
        return getDatatableResultResponse(request, dataFromPrimary, true);
    }
}
