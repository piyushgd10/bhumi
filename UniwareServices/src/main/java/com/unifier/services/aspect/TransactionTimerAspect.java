/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 8/7/15 8:16 AM
 * @author sunny
 */
package com.unifier.services.aspect;

import com.unifier.core.cache.CacheManager;
import com.unifier.dao.startup.IBaseStartupDao;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.Constants;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author singla
 */
// The order of transactional aspect is 200. So this aspect runs after transaction is committed.
@Aspect
public class TransactionTimerAspect {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionTimerAspect.class);

    @Autowired
    private IBaseStartupDao baseStartupDao;

    @Around("execution(* *(..)) && @annotation(transaction)")
    public Object startTimer(final ProceedingJoinPoint jp, Transactional transaction) throws Throwable {
        boolean monitoringEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isTransactionMonitoringEnabled();
        if (monitoringEnabled) {
            TransactionContext transactionContext = TransactionContext.current();
            MethodSignature ms = (MethodSignature) jp.getSignature();
            try {
                if (transactionContext == null) {
                    transactionContext = new TransactionContext();
                    Number connectionId = baseStartupDao.getCurrentConnectionId();
                    ThreadContext.put(Constants.DB_CONNECTION_ID, connectionId.toString());
                    transactionContext.setConnectionId(connectionId);
                    transactionContext.setStartTime(System.currentTimeMillis());
                    TransactionContext.setTransactionContext(transactionContext);
                    LOG.info("Transaction with connectionId {} started for method {}", transactionContext.getConnectionId(), ms.getName());
                }
                transactionContext.incrementCounter();
                return jp.proceed();
            } finally {
                transactionContext.decrementCounter();
                if (transactionContext.getCounter() == 0) {
                    LOG.info("Transaction with connectionId {} for method {} took {} ms", new Object[]{transactionContext.getConnectionId(), ms.getName(), System.currentTimeMillis() - transactionContext.getStartTime()});
                    ThreadContext.remove(Constants.DB_CONNECTION_ID);
                    TransactionContext.destroy();
                }
            }
        } else {
            return jp.proceed();
        }
    }

    private static class TransactionContext {
        private static ThreadLocal<TransactionContext> ctx = new ThreadLocal<>();
        private Number connectionId;
        private long   startTime;
        private int    counter;

        public static TransactionContext current() {
            return ctx.get();
        }

        public static void destroy() {
            ctx.remove();
        }

        public static void setTransactionContext(TransactionContext context) {
            ctx.set(context);
        }

        public Number getConnectionId() {
            return connectionId;
        }

        public void setConnectionId(Number connectionId) {
            this.connectionId = connectionId;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public int getCounter() {
            return counter;
        }

        public void incrementCounter() {
            counter++;
        }

        public void decrementCounter() {
            counter--;
        }
    }
}
