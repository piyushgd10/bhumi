/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.unifier.services.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclarePrecedence;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Sunny
 */
@DeclarePrecedence(value = "com.unifier.services.aspect.RootAspect, com.unifier.services.aspect.LockingAspect, com.unifier.services.aspect.MarkDirtyAspect, org.springframework.transaction.aspectj.AbstractTransactionAspect, *")
@Aspect
public class RootAspect {

}
