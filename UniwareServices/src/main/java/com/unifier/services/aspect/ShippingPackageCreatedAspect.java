/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.unifier.services.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.uniware.services.shipping.impl.CreateShippingPackagePostCommitHandler;

/**
 * @author Sunny
 */
@Aspect
public class ShippingPackageCreatedAspect {

    private static final Logger                    LOG = LoggerFactory.getLogger(ShippingPackageCreatedAspect.class);

    @Autowired
    private CreateShippingPackagePostCommitHandler createShippingPackagePostCommitHandler;

    @Before("execution(* com.uniware.services.shipping.impl.ShippingServiceImpl.createShippingPackage(..))")
    public void trigger(final JoinPoint jp) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("ShippingPackageCreatedAspect @Before");
        }
        TransactionSynchronizationManager.registerSynchronization(createShippingPackagePostCommitHandler);
        if (LOG.isDebugEnabled()) {
            LOG.debug("ShippingPackageCreatedAspect @Before complete");
        }
    }
}
