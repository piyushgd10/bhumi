/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.unifier.services.aspect;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.unifier.core.api.base.ServiceResponse;

/**
 * @author singla
 */
@Order(1000)
//The order of transactional aspect is 200. So this aspect runs before transaction is committed.
@Aspect
@Component
public class TransactionRollbackAspect {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionRollbackAspect.class);

    @AfterReturning(pointcut = "execution(@com.unifier.services.aspect.RollbackOnFailure * *(..))", returning = "response")
    public void trigger(ServiceResponse response) {
        if (response != null && !response.isSuccessful()) {
            LOG.debug("Executing Rollback");
            LOG.error(response.getClass().toString() + ":" + response.getErrors());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            LOG.debug("DONE Executing Rollback");
        }
    }
}
