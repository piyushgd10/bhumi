/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.unifier.services.aspect;

import com.uniware.core.locking.annotation.Locks;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.unifier.core.annotation.audit.LogActivity;
import com.unifier.core.api.audit.ActivityDto;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.ActivityContext;
import com.uniware.services.audit.IActivityLogService;

/**
 * Will intercept service method calls with {@link LogActivity}. Enable activity logging in before pointcut. After
 * returning pointcut submits activities logged by service API to a executor
 * 
 * @author parijat
 */
@Aspect
public class LogActivityAspect {

    private static final Logger  LOG             = LoggerFactory.getLogger(LogActivityAspect.class);

    @Autowired
    private IActivityLogService  activityLogService;

    private ThreadLocal<Integer> logServiceCount = new ThreadLocal<>();

    @Around("execution(* *(..)) && @annotation(logActivityAnnotation)")
    public Object doActivityLogging(ProceedingJoinPoint pjp, LogActivity logActivityAnnotation) throws Throwable {
        boolean activityLoggingEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isActivityLoggingEnabled();
        if (activityLoggingEnabled) {
            ActivityContext context = ActivityContext.current();
            context.setEnable(true);
            MethodSignature ms = (MethodSignature) pjp.getSignature();
            if (logServiceCount.get() == null) {
                logServiceCount.set(0);
            }
            logServiceCount.set(logServiceCount.get().intValue() + 1);
            LOG.info("Start activity logging for {}, serviceCount {}", ms.getName(), logServiceCount.get().intValue());
        }
        Object returnValue = null;
        try {
            returnValue = pjp.proceed();
            if (activityLoggingEnabled) {
                try {
                    if (returnValue == null || !(returnValue instanceof ServiceResponse) || ((ServiceResponse) returnValue).isSuccessful()) {
                        MethodSignature ms = (MethodSignature) pjp.getSignature();
                        LOG.info("LogActivity persist for {}, serviceCount {}", ms.getName(), logServiceCount.get().intValue());
                        if (logServiceCount.get().intValue() == 1) {
                            LOG.info("Persisting {} logs", ActivityContext.current().getLoggingDtos().size());
                            for (ActivityDto activityDto : ActivityContext.current().getLoggingDtos().values()) {
                                activityLogService.persistActivityLogs(activityDto);
                            }
                        }
                    }
                } catch (Exception e) {
                    LOG.error("Error in persisting audit logs " + e.getMessage());
                }
            }
        }  finally {
            if (activityLoggingEnabled) {
                logServiceCount.set(logServiceCount.get().intValue() - 1);
                if (logServiceCount.get().intValue() == 0) {
                    ActivityContext.destory();
                    logServiceCount.remove();
                }
            }
        }
        return returnValue;
    }
}
