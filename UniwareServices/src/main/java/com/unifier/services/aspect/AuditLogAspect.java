/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.unifier.services.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.unifier.core.annotation.audit.AuditMethod;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.audit.impl.AuditPostCommitHandler;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.utils.AuditContext;
import com.uniware.core.utils.UserContext;

/**
 * @author parijat The aspect will intercept {@link AuditMethod}.
 */
@Aspect
public class AuditLogAspect {

    @Autowired
    private AuditPostCommitHandler auditPostCommitHandler;

    private static final Logger    LOG = LoggerFactory.getLogger(AuditLogAspect.class);

    /**
     * This will be called before all method calls having {@link AuditMethod} annotation. Will initialize audit context
     * for the current executing thread Based on {@link AuditMethod} logChangeAudit flag, hibernate field level auditing
     * will be enabled/disabled.
     * 
     * @param jp
     */
    @Before("@annotation(auditMethod)")
    public void enableAuditContext(final JoinPoint jp, AuditMethod auditMethod) {
        if (CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isAuditLoggingEnabled()) {
            AuditContext auditContext = AuditContext.current();
            UserContext.current();
            MethodSignature ms = (MethodSignature) jp.getSignature();
            String methodName = StringUtils.isNotBlank(auditMethod.methodName()) ? auditMethod.methodName() : ms.getName();
            if (auditContext.isAuditEnabled()) {
                if (checkMethodForNewTransactionPropagation(ms.getMethod())) {
                    AuditContext context = new AuditContext(auditContext);
                    LOG.info("Start auditing for {} with parent method {}", methodName, auditContext.getBelongsToMethod());
                    context.setBelongsToMethod(methodName);
                    context.setContextIdentifier(Thread.currentThread().getName() + methodName);
                    AuditContext.setAuditContext(context);
                }
            } else {
                auditContext.setAuditEnabled(true);
                if (StringUtils.isNotBlank(auditContext.getBelongsToMethod())) {
                    auditContext.setBelongsToMethod(methodName + "\\" + auditContext.getBelongsToMethod());
                } else {
                    auditContext.setBelongsToMethod(methodName);
                }
                LOG.info("Start auditing for  method {}", auditContext.getBelongsToMethod());
                auditContext.setContextIdentifier(Thread.currentThread().getName());
            }
            if (checkForTransactionalMethod(ms.getMethod()) && TransactionSynchronizationManager.isSynchronizationActive()) {
                TransactionSynchronizationManager.registerSynchronization(auditPostCommitHandler);
            }
        }
    }

    /**
     * Check if current method's transaction requires new propagation
     * 
     * @param method
     * @return
     */
    private boolean checkMethodForNewTransactionPropagation(Method method) {
        boolean isNewTransaction = false;
        if (method.isAnnotationPresent(Transactional.class)) {
            isNewTransaction = Propagation.REQUIRES_NEW.equals(method.getAnnotation(Transactional.class).propagation());
        }
        return isNewTransaction;
    }

    /**
     * Check if current executing method is transactional
     * 
     * @param method
     * @return
     */
    private boolean checkForTransactionalMethod(Method method) {
        boolean isTransactional = false;
        if (method.isAnnotationPresent(Transactional.class)) {
            isTransactional = true;
        }
        return isTransactional;
    }

}
