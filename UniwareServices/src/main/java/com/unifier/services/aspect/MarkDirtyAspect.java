/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.unifier.services.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unifier.core.annotation.Cache;
import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.cache.ICache;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.configuration.IConfiguration;

/**
 * @author singla
 */
@Aspect
public class MarkDirtyAspect {

    private static final Logger LOG = LoggerFactory.getLogger(MarkDirtyAspect.class);

    @AfterReturning(pointcut = "@annotation(com.unifier.services.aspect.MarkDirty)", returning = "response")
    public void trigger(final JoinPoint jp, Object response) {
        if (response == null || !(response instanceof ServiceResponse) || ((ServiceResponse) response).isSuccessful()) {
            MethodSignature ms = (MethodSignature) jp.getSignature();
            Method m = ms.getMethod();
            Class<? extends Object>[] clazzes = m.getAnnotation(MarkDirty.class).values();
            for (Class<? extends Object> clazz : clazzes) {
                if (ICache.class.isAssignableFrom(clazz)) {
                    if (clazz.isAnnotationPresent(Cache.class)) {
                        CacheManager.getInstance().markCacheDirty(clazz);
                    }
                } else if (IConfiguration.class.isAssignableFrom(clazz)) {
                    ConfigurationManager.getInstance().markConfigurationDirty(clazz);
                }
            }
            LOG.info("MarkDirty @AfterReturning complete " + jp.toLongString());
        }
    }
}
