/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.services.admin.access.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unifier.core.cache.CacheManager;

import com.unifier.core.entity.FacilityAccessResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.advanced.AccessResourceDTO;
import com.unifier.core.api.advanced.CreateRoleRequest;
import com.unifier.core.api.advanced.CreateRoleResponse;
import com.unifier.core.api.advanced.EditRoleResourceMappingRequest;
import com.unifier.core.api.advanced.EditRoleResourceMappingResponse;
import com.unifier.core.api.advanced.RoleDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.HibernateUtils;
import com.unifier.dao.admin.access.IAccessControlDao;
import com.unifier.services.admin.access.IAccessControlService;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.RolesCache;

@Service
@Transactional
public class AccessControlServiceImpl implements IAccessControlService {

    @Autowired
    IAccessControlDao accessControlDao;

    @Override
    @MarkDirty(values = { RolesCache.class })
    public CreateRoleResponse createRole(CreateRoleRequest request) {
        CreateRoleResponse response = new CreateRoleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (accessControlDao.getRoleByCode(request.getCode()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "Role exists with code \"" + request.getCode() + "\"");
            } else {
                Role role = new Role();
                role.setCode(request.getCode().toUpperCase());
                role.setName(request.getRoleName().toUpperCase());
                role.setCreated(DateUtils.getCurrentTime());
                role.setLevel(request.getLevel().toUpperCase());
                role = accessControlDao.createRole(role);
                response.setRoleCode(role.getCode());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<RoleDTO> getRoleDTOs() {
        List<Role> roles = accessControlDao.getRoles();
        List<RoleDTO> roleDTOs = new ArrayList<RoleDTO>();
        for (Role role : roles) {
            RoleDTO roleDTO = new RoleDTO();
            roleDTO.setId(role.getId());
            roleDTO.setCode(role.getCode());
            roleDTO.setName(role.getName());
            roleDTO.setLevel(role.getLevel());
            roleDTO.setHidden(role.isHidden());
            for (RoleAccessResource roleAccessResource : role.getRoleAccessResources()) {
                AccessResource accessResource = CacheManager.getInstance().getCache(RolesCache.class).getAccessResourceByName(roleAccessResource.getAccessResourceName());
                if (accessResource != null) {
                    roleDTO.addAccessResource(accessResource.getName(), roleAccessResource.isEnabled());
                }
            }
            roleDTOs.add(roleDTO);
        }
        return roleDTOs;
    }

    @Override
    @Transactional
    public List<AccessResourceDTO> getAccessibleAccessResourceDTOs(Integer userId) {
        User user = accessControlDao.getAllAccessResourcesForUser(userId);
        List<AccessResourceDTO> accessResourceDTOs = new ArrayList<AccessResourceDTO>();
        AccessResource accessResource;
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.isEnabled()) {
                HibernateUtils.initializeAndUnproxy(userRole.getRole().getRoleAccessResources());
                for (RoleAccessResource roleAccessResource : userRole.getRole().getRoleAccessResources()) {
                    if (roleAccessResource.isEnabled()) {
                        accessResource = CacheManager.getInstance().getCache(RolesCache.class).getAccessResourceByName(roleAccessResource.getAccessResourceName());
                        if (accessResource != null) {
                            AccessResourceDTO accessResourceDTO = new AccessResourceDTO();
                            accessResourceDTO.setId(accessResource.getId());
                            accessResourceDTO.setName(accessResource.getName());
                            accessResourceDTO.setLevel(accessResource.getLevel());
                            if (accessResource.getAccessResourceGroup() != null) {
                                accessResourceDTO.setAccessGroupName(accessResource.getAccessResourceGroup().getName());
                            }
                            accessResourceDTOs.add(accessResourceDTO);
                        }
                    }
                }
            }
        }
        return accessResourceDTOs;
    }

    @Override
    public EditRoleResourceMappingResponse editRoleResourceMapping(EditRoleResourceMappingRequest request) {
        EditRoleResourceMappingResponse response = new EditRoleResourceMappingResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            AccessResource accessResource = null;
            for (String accessResourceName : request.getNewAccessResourceNames()) {
                RoleAccessResource roleAccessResource = new RoleAccessResource();
                roleAccessResource.setRole(new Role(request.getRoleId()));
                accessResource = CacheManager.getInstance().getCache(RolesCache.class).getAccessResourceByName(accessResourceName);
                if (accessResource != null) {
                    roleAccessResource.setAccessResourceName(accessResource.getName());
                } else {
                    context.addError(WsResponseCode.INVALID_REQUEST, "Access Resource : " + accessResourceName + "not found");
                }
                roleAccessResource.setCreated(DateUtils.getCurrentDayTime());
                roleAccessResource.setEnabled(true);
                roleAccessResource = accessControlDao.createRoleAccessResource(roleAccessResource);
            }
            for (Map.Entry<String, Boolean> entry : request.getUpdateAccessResourceNames().entrySet()) {
                RoleAccessResource roleAccessResource = accessControlDao.getRoleAccessResource(request.getRoleId(), entry.getKey());
                roleAccessResource.setEnabled(entry.getValue());
                roleAccessResource = accessControlDao.editRoleAccessResource(roleAccessResource);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public List<Role> getRoles() {
        return accessControlDao.getRoles();
    }

    @Override
    public List<AccessResourceGroup> getAccessResourceGroups() {
        return accessControlDao.getAccessResourceGroups();
    }

    @Override
    public List<AccessResource> getAccessResources() {
        return accessControlDao.getAccessResources();
    }

    @Override
    public List<FacilityAccessResource> getFacilityAccessResources() {
        return accessControlDao.getFacilityAccessResources();
    }

    @Override
    public List<RoleAccessResource> getRoleAccessResources(String accessResourceName) {
        return accessControlDao.getRoleAccessResources(accessResourceName);
    }
}
