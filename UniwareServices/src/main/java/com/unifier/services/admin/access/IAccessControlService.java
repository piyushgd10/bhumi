/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-May-2012
 *  @author vibhu
 */
package com.unifier.services.admin.access;

import java.util.List;

import com.unifier.core.api.advanced.AccessResourceDTO;
import com.unifier.core.api.advanced.CreateRoleRequest;
import com.unifier.core.api.advanced.CreateRoleResponse;
import com.unifier.core.api.advanced.EditRoleResourceMappingRequest;
import com.unifier.core.api.advanced.EditRoleResourceMappingResponse;
import com.unifier.core.api.advanced.RoleDTO;
import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.FacilityAccessResource;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;

public interface IAccessControlService {

    CreateRoleResponse createRole(CreateRoleRequest request);

    List<RoleDTO> getRoleDTOs();

    List<AccessResourceDTO> getAccessibleAccessResourceDTOs(Integer userId);

    EditRoleResourceMappingResponse editRoleResourceMapping(EditRoleResourceMappingRequest request);

    List<Role> getRoles();

    List<AccessResourceGroup> getAccessResourceGroups();

    List<AccessResource> getAccessResources();

    List<FacilityAccessResource> getFacilityAccessResources();

    List<RoleAccessResource> getRoleAccessResources(String accessResourceName);
}