/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 21, 2012
 *  @author singla
 */
package com.unifier.services.binary.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.entity.BinaryObject;
import com.unifier.dao.binary.IBinaryObjectDao;
import com.unifier.services.binary.IBinaryObjectService;

/**
 * @author singla
 */
@Service
@Transactional
public class BinaryObjectServiceImpl implements IBinaryObjectService {

    @Autowired
    private IBinaryObjectDao binaryObjectDao;

    /* (non-Javadoc)
     * @see com.uniware.services.binary.IBinaryObjectService#getBinaryObjectById(int)
     */
    @Override
    public BinaryObject getBinaryObjectById(int binaryObjectId) {
        return binaryObjectDao.getBinaryObjectById(binaryObjectId);
    }

    /* (non-Javadoc)
     * @see com.uniware.services.binary.IBinaryObjectService#addBinaryObject(com.uniware.core.entity.BinaryObject)
     */
    @Override
    public BinaryObject addBinaryObject(BinaryObject binaryObject) {
        return binaryObjectDao.addBinaryObject(binaryObject);
    }
}
