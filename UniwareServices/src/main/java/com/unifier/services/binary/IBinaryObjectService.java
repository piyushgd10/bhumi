/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 21, 2012
 *  @author singla
 */
package com.unifier.services.binary;

import com.unifier.core.entity.BinaryObject;

/**
 * @author singla
 */
public interface IBinaryObjectService {

    /**
     * @param binaryObjectId
     * @return
     */
    BinaryObject getBinaryObjectById(int binaryObjectId);

    /**
     * @param binaryObject
     * @return
     */
    BinaryObject addBinaryObject(BinaryObject binaryObject);

}
