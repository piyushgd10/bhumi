/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.services.customfields;

import java.util.List;

import com.unifier.core.api.customfields.CreateCustomFieldRequest;
import com.unifier.core.api.customfields.CreateCustomFieldResponse;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesRequest;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesResponse;
import com.unifier.core.api.customfields.ListCustomFieldsResponse;
import com.unifier.core.entity.CustomFieldMetadata;

public interface ICustomFieldService {

    CreateCustomFieldResponse createCustomField(CreateCustomFieldRequest request);

    ListCustomFieldsResponse listCustomFields();

    List<CustomFieldMetadata> getCustomFieldsMetadata();

    EditCustomFieldPossibleValuesResponse editCustomField(EditCustomFieldPossibleValuesRequest request);

}
