/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.services.customfields.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.customfields.CreateCustomFieldRequest;
import com.unifier.core.api.customfields.CreateCustomFieldResponse;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesRequest;
import com.unifier.core.api.customfields.EditCustomFieldPossibleValuesResponse;
import com.unifier.core.api.customfields.ListCustomFieldsResponse;
import com.unifier.core.api.customfields.ListCustomFieldsResponse.CustomFieldMetaDataDTO;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.CustomFieldMetadata;
import com.unifier.core.entity.CustomFieldMetadata.ValueType;
import com.unifier.core.entity.CustomFieldValue;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.dao.customfields.ICustomFieldDao;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.customfields.ICustomFieldService;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;

@Service
@Transactional
public class CustomFieldServiceImpl implements ICustomFieldService {

    private static final Logger LOG = LoggerFactory.getLogger(CustomFieldServiceImpl.class);

    private List<String>        mappingFields;

    @Autowired
    private ICustomFieldDao     customFieldDao;

    @Override
    public ListCustomFieldsResponse listCustomFields() {
        ListCustomFieldsResponse response = new ListCustomFieldsResponse();
        List<CustomFieldMetadata> customFields = new ArrayList<CustomFieldMetadata>();
        customFields = customFieldDao.listAllCustomFields();
        for (CustomFieldMetadata customField : customFields) {
            CustomFieldMetaDataDTO customFieldDto = new CustomFieldMetaDataDTO();
            customFieldDto.setEntity(customField.getEntity());
            customFieldDto.setDisplayName(customField.getDisplayName());
            customFieldDto.setName(customField.getName());
            customFieldDto.setDefaultValue(customField.getDefaultValue());
            customFieldDto.setType(customField.getValueType());
            customFieldDto.setRequired(customField.isRequired());
            customFieldDto.setSerializable(customField.isSerializable());
            customFieldDto.setMappingFieldName(customField.getMappingFieldName());
            customFieldDto.setEnabled(customField.isEnabled());
            customFieldDto.setPossibleValues(customField.getPossibleValues());
            response.getCustomFields().add(customFieldDto);
            response.setSuccessful(true);
        }
        return response;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    @RollbackOnFailure
    public CreateCustomFieldResponse createCustomField(CreateCustomFieldRequest request) {
        CreateCustomFieldResponse response = new CreateCustomFieldResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Class entity = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getEntityByDisplayName(request.getEntity());
            if (entity == null) {
                context.addError(ResponseCode.INVALID_FORMAT, "Invalid entity name");
            } else {
                ValueType valueType = null;
                try {
                    valueType = ValueType.valueOf(request.getValueType());
                } catch (IllegalArgumentException e) {
                    context.addError(ResponseCode.INVALID_FORMAT, "Invalid value for value type");
                }
                if (!context.hasErrors()) {
                    if (!entity.isAnnotationPresent(Table.class)) {
                        context.addError(ResponseCode.INVALID_FORMAT, "There doesn't exist any entity with given name");
                    } else if (!ValidatorUtils.isSqlColumnNameValid(request.getName())) {
                        context.addError(ResponseCode.INVALID_FORMAT, "Invalid column name");
                    }
                }
                if (!context.hasErrors()) {
                    FieldType fieldType = new FieldType(valueType);
                    if (Boolean.parseBoolean(EnvironmentProperties.getAllowSerializableCustomFields())) {
                        createSerializableCustomField(request, context, entity, fieldType);
                    } else {
                        createNonSerializableCustomField(request, context, entity, fieldType);
                    }
                }
                if (!context.hasErrors()) {
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    //
    // For serializable fields, add the field in the corresponding entity and 
    // then and add an entry in custom_field_metadata for each tenant.
    //
    @SuppressWarnings("rawtypes")
    private void createSerializableCustomField(CreateCustomFieldRequest request, ValidationContext context, Class entity, FieldType fieldType) {
        addColumnToEntity(request, entity, fieldType, context);
        if (!context.hasErrors()) {
            for (Tenant t : customFieldDao.getAllTenants()) {
                addCustomFieldMetadata(request, t, entity, fieldType, null, context, true);
            }
        }
    }

    @MarkDirty(values = { CustomFieldsMetadataConfiguration.class })
    @SuppressWarnings("rawtypes")
    private void createNonSerializableCustomField(CreateCustomFieldRequest request, ValidationContext context, Class entity, FieldType fieldType) {
        String mappingFieldName = getNextAvailableNonSerializableMappingField(entity.getName());
        if (mappingFieldName == null) {
            context.addError(WsResponseCode.INVALID_REQUEST, "Mapping fields exhausted");
        } else {
            addCustomFieldMetadata(request, CacheManager.getInstance().getCache(TenantCache.class).getCurrentTenant(), entity, fieldType, mappingFieldName, context, false);
        }
    }

    @Override
    @MarkDirty(values = { CustomFieldsMetadataConfiguration.class })
    public EditCustomFieldPossibleValuesResponse editCustomField(EditCustomFieldPossibleValuesRequest request) {
        EditCustomFieldPossibleValuesResponse response = new EditCustomFieldPossibleValuesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            CustomFieldMetadata customFieldMetadata = customFieldDao.getCustomFieldsMetadataByEntityAndName(request.getEntity(), request.getName());
            if (customFieldMetadata == null) {
                context.addError(WsResponseCode.INVALID_CUSTOM_FIELD, "Invalid entity or name");
            } else {
                customFieldMetadata.setPossibleValues(request.getPossibleValues());
                customFieldDao.updateCustomFieldMetadata(customFieldMetadata);
            }
        }
        if (!context.hasErrors()) {
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void addColumnToEntity(CreateCustomFieldRequest request, Class entity, FieldType fieldType, ValidationContext context) {
        String sqlType = fieldType.getSqlType();
        Table table = (Table) entity.getAnnotation(Table.class);
        StringBuilder builder = new StringBuilder("alter table ").append(table.name()).append(" add column ");
        builder.append(request.getName()).append(" ").append(sqlType);
        if (sqlType.equalsIgnoreCase("decimal")) {
            builder.append("(12,2)");
            sqlType = sqlType + "(12,2)";
        }
        if (sqlType.equalsIgnoreCase("int")) {
            builder.append("(10) unsigned");
            sqlType = sqlType + "(10)";
        }
        if (sqlType.equalsIgnoreCase("varchar")) {
            if (request.getLength() == null) {
                context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, "Length must be specified");
            } else {
                builder.append("(").append(request.getLength()).append(") ");
                sqlType = sqlType + "(" + request.getLength() + ")";
            }
        }
        if (ValueType.SELECT.equals(fieldType.getValueType()) && StringUtils.isBlank(request.getPossibleValues())) {
            context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, "Possible values must be specified for 'select' value type");
        }

        if (!context.hasErrors()) {
            String defaultValue = request.getDefaultValue();
            if (StringUtils.isNotBlank(defaultValue)) {
                if (ValueType.SELECT.equals(fieldType.getValueType())) {
                    String possibleValues = request.getPossibleValues().replaceAll("[\\s]*,[\\s]*", ",");
                    List<String> possibleValuez = StringUtils.split(possibleValues.toLowerCase(), ",");
                    if (!possibleValuez.contains(defaultValue.toLowerCase())) {
                        context.addError(ResponseCode.INVALID_FORMAT, "Invalid default value");
                    } else {
                        builder.append(" default '").append(possibleValues).append("' ");
                    }
                } else if (ValueType.CHECKBOX.equals(fieldType.getValueType())) {
                    if (Boolean.parseBoolean(defaultValue)) {
                        builder.append(" default '").append(1).append("' ");
                    } else {
                        builder.append(" default '").append(0).append("' ");
                    }
                } else {
                    builder.append(" default '").append(defaultValue).append("' ");
                }
            }
            if (request.isRequired()) {
                builder.append(" not null ");
            }
        }
        if (!context.hasErrors()) {
            String query = builder.toString();
            fieldType.setSqlType(sqlType);
            try {
                customFieldDao.addCustomField(query);
            } catch (Exception e) {
                LOG.error("Exception occured while creating  custom field", e);
                context.addError(WsResponseCode.INVALID_REQUEST, e.getMessage());
            }
        }
    }

    public void addCustomFieldMetadata(CreateCustomFieldRequest request, Tenant t, @SuppressWarnings("rawtypes") Class entity, FieldType fieldType, String mappingFieldName,
            ValidationContext context, boolean serializable) {
        try {
            if (ValueType.DECIMAL.name().equals(request.getValueType()) || ValueType.INTEGER.name().equals(request.getValueType())) {
                request.setValueType(ValueType.TEXT.name());
            }
            CustomFieldMetadata customFieldMetadata = new CustomFieldMetadata(entity.getName(), request.getName(), request.getDisplayName(), request.getValueType().toLowerCase(),
                    fieldType.getJavaType(), fieldType.getSqlType(), request.getPossibleValues(), request.getValidatorPattern(), DateUtils.getCurrentTime(),
                    DateUtils.getCurrentTime());
            customFieldMetadata.setDefaultValue(request.getDefaultValue());
            customFieldMetadata.setRequired(request.isRequired());
            customFieldMetadata.setEnabled(true);
            customFieldMetadata.setSerializable(serializable);
            customFieldMetadata.setMappingFieldName(mappingFieldName);
            customFieldMetadata.setTenant(t);
            customFieldMetadata = customFieldDao.createCustomFieldMetadata(customFieldMetadata);
        } catch (Exception e) {
            LOG.error("Exception occured while creating  custom field", e);
            context.addError(WsResponseCode.INVALID_REQUEST, e.getMessage());
        }
    }

    public String getNextAvailableNonSerializableMappingField(String entityName) {
        List<CustomFieldMetadataVO> customFieldMetadatas = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class).getCustomFieldsByEntity(
                entityName);
        Set<String> currentMappingFields = new HashSet<String>();
        if (customFieldMetadatas != null) {
            for (CustomFieldMetadataVO cfmVO : customFieldMetadatas) {
                if (!cfmVO.isSerializable()) {
                    currentMappingFields.add(cfmVO.getMappingFieldName());
                }
            }
        }
        for (String mappingField : getAllMappingFields()) {
            if (!currentMappingFields.contains(mappingField)) {
                return mappingField;
            }
        }
        return null;
    }

    private List<String> getAllMappingFields() {
        if (mappingFields == null) {
            mappingFields = new ArrayList<String>();
            for (Field f : CustomFieldValue.class.getDeclaredFields()) {
                if (f.getName().contains("value")) {
                    mappingFields.add(f.getName());
                }
            }
            Collections.sort(mappingFields);
        }
        return mappingFields;
    }

    private static class FieldType {
        private final ValueType valueType;
        private String          sqlType;
        private final String    javaType;

        public FieldType(ValueType valueType) {
            this.valueType = valueType;
            switch (valueType) {
                case TEXT:
                    sqlType = "varchar";
                    javaType = "java.lang.String";
                    break;
                case SELECT:
                    sqlType = "varchar";
                    javaType = "java.lang.String";
                    break;
                case DATE:
                    sqlType = "datetime";
                    javaType = "java.util.Date";
                    break;
                case CHECKBOX:
                    sqlType = "tinyint";
                    javaType = "java.lang.Boolean";
                    break;
                case INTEGER:
                    sqlType = "int";
                    javaType = "java.lang.Integer";
                    break;
                case DECIMAL:
                    sqlType = "decimal";
                    javaType = "java.math.BigDecimal";
                    break;
                default:
                    sqlType = null;
                    javaType = null;
                    break;
            }
        }

        public String getSqlType() {
            return sqlType;
        }

        /**
         * @param sqlType the sqlType to set
         */
        public void setSqlType(String sqlType) {
            this.sqlType = sqlType;
        }

        public String getJavaType() {
            return javaType;
        }

        public ValueType getValueType() {
            return valueType;
        }

    }

    @Override
    @Transactional
    public List<CustomFieldMetadata> getCustomFieldsMetadata() {
        return customFieldDao.getCustomFieldsMetadata();
    }
}
