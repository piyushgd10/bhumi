/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2012
 *  @author singla
 */
package com.unifier.services.sms;

import java.util.HashMap;
import java.util.Map;

public class SmsMessage {

    private String              to;
    private String              templateName;
    private Map<String, Object> templateParams = new HashMap<String, Object>();

    public SmsMessage(String to, String templateName) {
        this.to = to;
        this.templateName = templateName;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the templateName
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * @param templateName the templateName to set
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    /**
     * @return the templateParams
     */
    public Map<String, Object> getTemplateParams() {
        return templateParams;
    }

    /**
     * @param templateParams the templateParams to set
     */
    public void setTemplateParams(Map<String, Object> templateParams) {
        this.templateParams = templateParams;
    }

    public void addTemplateParam(String name, Object value) {
        templateParams.put(name, value);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SmsMessage [to=" + to + ", templateName=" + templateName + "]";
    }

}
