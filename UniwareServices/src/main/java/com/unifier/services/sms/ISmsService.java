/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2012
 *  @author singla
 */
package com.unifier.services.sms;

import java.util.List;

import com.unifier.core.sms.SmsMessage;
import com.uniware.core.entity.SmsTemplate;

public interface ISmsService {

    void send(SmsMessage message);

    List<SmsTemplate> getSmsTemplates();

    void send(SmsMessage message, String channelSource);
}
