/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2012
 *  @author singla
 */
package com.unifier.services.sms.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.sms.SimpleSmsMessage;
import com.unifier.core.sms.SmsMessage;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.sms.ISmsDao;
import com.unifier.services.sms.ISmsService;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.SmsConfiguration;
import com.uniware.services.configuration.SmsConfiguration.SmsTemplateVO;
import com.uniware.services.messaging.jms.ActiveMQConnector;

@Service
public class SmsServiceImpl implements ISmsService {
    private static final Logger LOG = LoggerFactory.getLogger(SmsServiceImpl.class);

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    @Autowired
    private ISmsDao smsDao;

    @Override
    public void send(SmsMessage message) {
        LOG.info("Adding message type - {} to sms queue.", message.getTemplateName());
        enqueueSms(message, null);

    }

    @Override
    public void send(SmsMessage message, String channelSource) {
        LOG.info("Adding message type - {} to sms queue.", message.getTemplateName());
        enqueueSms(message, channelSource);
    }

    private void enqueueSms(SmsMessage message, String channelSource) {
        SmsTemplateVO template = ConfigurationManager.getInstance().getConfiguration(SmsConfiguration.class).getTemplateByName(message.getTemplateName());
        if (template != null && template.isEnabled()) {
            SimpleSmsMessage smsMessage = null;
            try {
                smsMessage = prepareSms(message, template, channelSource);
            } catch (Exception e) {
                LOG.error("Exception while preparing sms message type: {}, Reason: {}", message.getTemplateName(), e.getMessage());
            }
            if (smsMessage != null) {
                LOG.info("Adding message type: {}, message: {}  to sms queue.", message.getTemplateName(), smsMessage);
                activeMQConnector.produceMessage(MessagingConstants.Queue.SMS_MESSAGE_QUEUE, smsMessage);
            }
        } else {
            LOG.error("Ignoring message type: {} as template does not exist or is disabled", message.getTemplateName());
        }
    }

    private SimpleSmsMessage prepareSms(SmsMessage message, SmsTemplateVO template, String channelSource) {
        SimpleSmsMessage smsMessage = new SimpleSmsMessage();
        if (message.getTo() == null) {
            throw new IllegalArgumentException("No recipient specified.");
        }
        smsMessage.setTo(message.getTo());
        String messageText = template.getMessageTemplate().evaluate(message.getTemplateParams());
        if (StringUtils.isNotBlank(messageText)) {
            smsMessage.setMessageText(messageText);
        } else {
            throw new IllegalArgumentException("Empty message.");
        }
        smsMessage.addProperty("source", "uniware");
        smsMessage.addProperty("channelSource", channelSource);
        smsMessage.addProperty("tenantCode", UserContext.current().getTenant().getCode());
        smsMessage.addProperty("productType", UserContext.current().getTenant().getProduct().getCode());
        smsMessage.addProperty("messageType", template.getName());
        return smsMessage;
    }

    @Override
    @Transactional
    public List<SmsTemplate> getSmsTemplates() {
        return smsDao.getSmsTemplates();
    }
}
