/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 22, 2012
 *  @author singla
 */
package com.unifier.services.pdf.impl;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.resource.XMLResource;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.HtmlTags;
import com.itextpdf.text.html.simpleparser.ChainedProperties;
import com.itextpdf.text.html.simpleparser.HTMLTagProcessor;
import com.itextpdf.text.html.simpleparser.HTMLTagProcessors;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.ImageProvider;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.BinaryObject;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.services.binary.IBinaryObjectService;
import com.unifier.services.pdf.IPdfDocumentService;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions.PrintDialog;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.vo.SamplePrintTemplateVO;

/**
 * @author singla
 */
@SuppressWarnings("deprecation")
@Service
public class PdfDocumentServiceImpl implements IPdfDocumentService {

    private static final Logger  LOG        = LoggerFactory.getLogger(PdfDocumentServiceImpl.class);

    public static final String   PAGE_BREAK = "<pageBreak/>";

    private final HttpSender     httpSender = new HttpSender();

    @Autowired
    private IBinaryObjectService binaryObjectService;

    /* (non-Javadoc)
     * @see com.uniware.services.pdf.IPdfDocumentService#writeHtmlToPdf(java.io.OutputStream, java.lang.String)
     */
    @Override
    public void writeHtmlToPdf(OutputStream outputStream, String html, PrintOptions options) {
        if (options.isXml()) {
            writeXMLToPDF(outputStream, html, options);
        } else {
            Rectangle pageSize = PageSize.getRectangle(options.getPageSize());
            if (options.isLandscape()) {
                pageSize = pageSize.rotate();
            }
            Document document = new Document(pageSize, options.margins[3], options.margins[1], options.margins[0], options.margins[2]);
            PdfWriter writer = null;
            try {
                writer = PdfWriter.getInstance(document, outputStream);
                document.open();
                UniwareHTMLWorker worker = new UniwareHTMLWorker();
                List<Element> p = worker.parse(writer, html);
                for (int k = 0; k < p.size(); ++k) {
                    document.add(p.get(k));
                }
                if (PrintDialog.SILENT.name().equals(options.getPrintDialog())) {
                    writer.addJavaScript("if (typeof(silentPrint) == 'function') { silentPrint(); } else { this.print(); }");
                    writer.addViewerPreference(PdfName.NUMCOPIES, new PdfNumber(options.getNoOfCopies()));
                } else if (PrintDialog.DIALOG.name().equals(options.getPrintDialog())) {
                    writer.addJavaScript("this.print();");
                    writer.addViewerPreference(PdfName.NUMCOPIES, new PdfNumber(options.getNoOfCopies()));
                }
            } catch (Exception e) {
                LOG.error("unable to get serialize html to pdf", e);
            } finally {
                document.close();
                if (writer != null) {
                    writer.close();
                }

            }
        }
    }

    private void writeXMLToPDF(OutputStream outputStream, String html, PrintOptions options) {
        org.w3c.dom.Document document = XMLResource.load(new ByteArrayInputStream(html.getBytes())).getDocument();
        ITextRenderer renderer = new ITextRenderer();
        renderer.getSharedContext().setReplacedElementFactory(new ImageReplacedElementFactory(renderer.getSharedContext().getReplacedElementFactory()));
        renderer.setDocument(document, null);
        renderer.layout();
        try {
            renderer.createPDF(outputStream);
        } catch (Exception e) {
            LOG.error("unable to get serialize html to pdf", e);
        }
    }

    private class ImageReplacedElementFactory implements ReplacedElementFactory {

        private final ReplacedElementFactory superFactory;

        public ImageReplacedElementFactory(ReplacedElementFactory superFactory) {
            this.superFactory = superFactory;
        }

        @Override
        public ReplacedElement createReplacedElement(LayoutContext layoutContext, BlockBox blockBox, UserAgentCallback userAgentCallback, int cssWidth, int cssHeight) {
            org.w3c.dom.Element element = blockBox.getElement();
            if (element == null) {
                return null;
            }
            boolean match = false;
            String nodeName = element.getNodeName();
            if ("img".equals(nodeName)) {
                String src = element.getAttribute("src");
                com.lowagie.text.Image image = null;
                int code39Index = src.indexOf("/barcode/code39/");
                int code128cIndex = src.indexOf("/barcode/code128");
                int storeIndex = src.indexOf("/object-store/view/");
                if (code39Index != -1) {
                    Barcode39 barcode = new Barcode39();
                    String[] split = element.getAttribute("src").split("/");
                    barcode.setCode(split[split.length - 1]);
                    try {
                        image = com.lowagie.text.Image.getInstance(barcode.createAwtImage(Color.BLACK, Color.WHITE), Color.WHITE);
                        //image.scaleAbsolute(1800f, 500f);
                        image.scalePercent(1100);
                    } catch (Exception e) {
                        LOG.error("unable to get barcode image {}", e);
                    }
                    match = true;
                } else if (code128cIndex != -1) {
                    Barcode128 code = new Barcode128();
                    String[] split = element.getAttribute("src").split("/");
                    code.setCode(split[split.length - 1]);
                    try {
                        image = com.lowagie.text.Image.getInstance(code.createAwtImage(Color.BLACK, Color.WHITE), Color.WHITE);
                        //image.scaleAbsolute(1800f, 500f);
                        image.scalePercent(1100);
                    } catch (Exception e) {
                        LOG.error("unable to get barcode image {}", e);
                    }
                    match = true;
                } else if (storeIndex != -1) {
                    LOG.info("Resolving object-store image");
                    int objectStoreId = Integer.parseInt(src.substring(storeIndex + "/object-store/view/".length()));
                    BinaryObject binaryObject = binaryObjectService.getBinaryObjectById(objectStoreId);
                    if (binaryObject != null) {
                        try {
                            image = com.lowagie.text.Image.getInstance(binaryObject.getContent());
                        } catch (Exception e) {
                            LOG.error("unable to get object image at id:" + objectStoreId, e);
                        }
                    }
                    match = true;
                } else if (src.startsWith("/static/img/")) {
                    try {
                        image = com.lowagie.text.Image.getInstance(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAppRootPath() + src);
                    } catch (Exception e) {
                        LOG.error("unable to get static file at:" + src, e);
                    }
                    match = true;
                }
                if (image != null) {
                    FSImage fsImage = new ITextFSImage(image);
                    if (fsImage != null) {
                        if ((cssWidth != -1) || (cssHeight != -1)) {
                            fsImage.scale(cssWidth, cssHeight);
                        }
                        return new ITextImageElement(fsImage);
                    }
                }
            }
            if (!match) {
                return superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
            }
            return null;
        }

        @Override
        public void reset() {
            superFactory.reset();
        }

        @Override
        public void setFormSubmissionListener(FormSubmissionListener listener) {
            superFactory.setFormSubmissionListener(listener);
        }

        @Override
        public void remove(org.w3c.dom.Element e) {
            superFactory.remove(e);
        }

    }

    /* (non-Javadoc)
     * @see com.uniware.services.pdf.IPdfDocumentService#writeHtmlToPdf(java.io.OutputStream, java.lang.String)
     */
    @Override
    public void writeHtmlToPdf(OutputStream outputStream, String html) {
        writeHtmlToPdf(outputStream, html, new PrintOptions());
    }

    public static class PrintOptions {
        private String  printDialog = PrintDialog.DEFAULT.name();
        private int     noOfCopies  = 1;
        private boolean landscape;
        private String  pageSize    = "A4";
        private int[]   margins     = new int[] { 10, 10, 10, 10 };
        private boolean xml;

        public enum PrintDialog {
            DEFAULT,
            DIALOG,
            SILENT
        }

        /**
         */
        public PrintOptions() {
        }

        /**
         * @param printDialog
         * @param noOfCopies
         */
        public PrintOptions(String printDialog, int noOfCopies) {
            this(printDialog, noOfCopies, false);
        }

        /**
         * @param printDialog
         * @param noOfCopies
         */
        public PrintOptions(String printDialog, int noOfCopies, boolean landscape) {
            this.printDialog = printDialog;
            this.noOfCopies = noOfCopies;
            this.landscape = landscape;
        }

        public PrintOptions(String printDialog, int noOfCopies, boolean landscape, String pageSize, String pageMargins) {
            this.printDialog = printDialog;
            this.noOfCopies = noOfCopies;
            this.landscape = landscape;
            this.pageSize = pageSize;
            String[] strMargins = pageMargins.split(",");
            for (int i = 0; i < strMargins.length && i < 4; i++) {
                try {
                    margins[i] = Integer.parseInt(strMargins[i]);
                } catch (NumberFormatException e) {
                    LOG.error("unable to parse margin:{}", strMargins[i]);
                }
            }
        }

        public PrintOptions(SamplePrintTemplateVO samplePrintTemplate) {
            this.printDialog = samplePrintTemplate.getPrintDialog();
            this.noOfCopies = samplePrintTemplate.getNumberOfCopies();
            this.landscape = samplePrintTemplate.isLandscape();
            String[] strMargins = samplePrintTemplate.getPageMargins().split(",");
            for (int i = 0; i < strMargins.length && i < 4; i++) {
                try {
                    margins[i] = Integer.parseInt(strMargins[i]);
                } catch (NumberFormatException e) {
                    LOG.error("unable to parse margin:{}", strMargins[i]);
                }
            }
            this.landscape = samplePrintTemplate.isLandscape();
            this.pageSize = samplePrintTemplate.getPageSize();
            this.xml = samplePrintTemplate.isXml();
        }

        public PrintOptions(PrintTemplateDTO printTemplateDTO) {
            this.printDialog = printTemplateDTO.getPrintDialog();
            this.noOfCopies = printTemplateDTO.getNumberOfCopies();
            this.landscape = printTemplateDTO.isLandscape();
            String[] strMargins = printTemplateDTO.getPageMargins().split(",");
            for (int i = 0; i < strMargins.length && i < 4; i++) {
                try {
                    margins[i] = Integer.parseInt(strMargins[i]);
                } catch (NumberFormatException e) {
                    LOG.error("unable to parse margin:{}", strMargins[i]);
                }
            }
            this.landscape = printTemplateDTO.isLandscape();
            this.pageSize = printTemplateDTO.getPageSize();
        }

        /**
         * @return the printDialog
         */
        public String getPrintDialog() {
            return printDialog;
        }

        /**
         * @param printDialog the printDialog to set
         */
        public void setPrintDialog(String printDialog) {
            this.printDialog = printDialog;
        }

        /**
         * @return the noOfCopies
         */
        public int getNoOfCopies() {
            return noOfCopies;
        }

        /**
         * @param noOfCopies the noOfCopies to set
         */
        public void setNoOfCopies(int noOfCopies) {
            this.noOfCopies = noOfCopies;
        }

        /**
         * @return the landscape
         */
        public boolean isLandscape() {
            return landscape;
        }

        /**
         * @param landscape the landscape to set
         */
        public void setLandscape(boolean landscape) {
            this.landscape = landscape;
        }

        /**
         * @return the margins
         */
        public int[] getMargins() {
            return margins;
        }

        /**
         * @param margins the margins to set
         */
        public void setMargins(int[] margins) {
            this.margins = margins;
        }

        /**
         * @return the pageSize
         */
        public String getPageSize() {
            return pageSize;
        }

        /**
         * @param pageSize the pageSize to set
         */
        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public boolean isXml() {
            return xml;
        }

        public void setXml(boolean xml) {
            this.xml = xml;
        }
    }

    private class UniwareHTMLWorker {

        public List<Element> parse(final PdfWriter writer, String html) throws IOException {
            final HashMap<String, Object> providerMap = new HashMap<String, Object>();
            providerMap.put(HTMLWorker.IMG_PROVIDER, new ImageProvider() {
                @Override
                public Image getImage(String src, Map<String, String> attrs, ChainedProperties chain, DocListener doc) {
                    Image image = null;
                    int code39Index = src.indexOf("/barcode/code39/");
                    int code128cIndex = src.indexOf("/barcode/code128");
                    int storeIndex = src.indexOf("/object-store/view/");
                    if (code39Index != -1) {
                        Barcode39 barcode = new Barcode39();
                        String[] params = src.substring(code39Index + "/barcode/code39/".length()).split("/", 3);
                        if (params.length == 3) {
                            barcode.setBarHeight(Float.parseFloat(params[0]));
                            barcode.setX(Float.parseFloat(params[1]));
                            barcode.setCode(params[2]);
                            barcode.setChecksumText(false);
                            barcode.setExtended(true);
                            image = barcode.createImageWithBarcode(writer.getDirectContent(), null, null);
                        }

                    } else if (code128cIndex != -1) {
                        Barcode128 barcode = new Barcode128();
                        String[] params = src.substring(code128cIndex + "/barcode/code128".length()).split("/", 4);
                        if (params.length == 4) {
                            if ("c".equalsIgnoreCase(params[1])) {
                                barcode.setCodeType(Barcode128.CODE_C);
                            } else if ("a".equalsIgnoreCase(params[1])) {
                                barcode.setCodeType(Barcode128.CODE_A);
                            }
                            barcode.setBarHeight(Float.parseFloat(params[1]));
                            barcode.setX(Float.parseFloat(params[2]));
                            barcode.setCode(params[3]);
                            barcode.setChecksumText(false);
                            barcode.setExtended(true);
                            image = barcode.createImageWithBarcode(writer.getDirectContent(), null, null);
                        }
                    } else if (storeIndex != -1) {
                        int objectStoreId = Integer.parseInt(src.substring(storeIndex + "/object-store/view/".length()));
                        BinaryObject binaryObject = binaryObjectService.getBinaryObjectById(objectStoreId);
                        if (binaryObject != null) {
                            try {
                                image = Image.getInstance(binaryObject.getContent());
                            } catch (Exception e) {
                                LOG.error("unable to get object image at id:" + objectStoreId, e);
                            }
                        }
                    } else if (src.startsWith("/static/img/")) {
                        try {
                            image = Image.getInstance(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAppRootPath() + src);
                        } catch (Exception e) {
                            LOG.error("unable to static file at:" + src, e);
                        }
                    } else {
                        String tmpFilePath = "/tmp/" + src.substring(src.lastIndexOf("/") + 1);
                        try {
                            httpSender.downloadToFile(src, tmpFilePath);
                            image = Image.getInstance(tmpFilePath);
                        } catch (Exception e) {
                            LOG.error("Unable to download image", e);
                        }
                    }
                    if (image != null) {
                        if (attrs.containsKey("width") && attrs.containsKey("height")) {
                            image.scaleAbsolute(Float.parseFloat(attrs.get("width")), Float.parseFloat(attrs.get("height")));
                        }
                        if (attrs.containsKey("rotate")) {
                            float rotate = Float.parseFloat(attrs.get("rotate"));
                            image.setRotationDegrees(rotate);
                        }
                    }
                    return image;
                }
            });
            final StyleSheet st = new StyleSheet();
            final Map<String, HTMLTagProcessor> tagProcessors = new HTMLTagProcessors();
            tagProcessors.put("style", new HTMLTagProcessor() {

                @Override
                public void startElement(HTMLWorker worker, String tag, Map<String, String> attrs) throws DocumentException, IOException {
                    String className = attrs.get("class");
                    if (StringUtils.isNotEmpty(className)) {
                        HashMap<String, String> temp = new HashMap<String, String>();
                        temp.putAll(attrs);
                        temp.remove("class");
                        st.loadStyle(className, temp);
                    }
                }

                @Override
                public void endElement(HTMLWorker worker, String tag) throws DocumentException {
                    worker.setSkipText(true);
                    worker.updateChain(tag);
                }
            });
            tagProcessors.put("pagebreak", new HTMLTagProcessor() {
                @Override
                public void startElement(HTMLWorker worker, String tag, Map<String, String> attrs) throws DocumentException, IOException {
                }

                @Override
                public void endElement(HTMLWorker worker, String tag) throws DocumentException {
                    worker.add(Chunk.NEXTPAGE);
                }
            });
            tagProcessors.put(HtmlTags.IMG, new HTMLTagProcessor() {
                @Override
                public void startElement(HTMLWorker worker, String tag, Map<String, String> attrs) throws DocumentException, IOException {
                    worker.updateChain(tag, attrs);
                    try {
                        worker.processImage(worker.createImage(attrs), attrs);
                    } catch (Exception e) {
                        LOG.error("unable to fetch image:" + attrs);
                    }
                    worker.updateChain(tag);
                }

                @Override
                public void endElement(HTMLWorker worker, String tag) {
                }

            });
            return HTMLWorker.parseToList(new StringReader(html), st, tagProcessors, providerMap);
        }
    }

    /*
     * Merges pdf files keeping their original orientation.
     */
    @Override
    public void merge(List<File> list, OutputStream outputStream) {
        Document document = new Document();
        PdfCopy copy = null;
        try {
            copy = new PdfCopy(document, outputStream);
            document.open();
            for (File file : list) {
                PdfReader reader = new PdfReader(file.getAbsolutePath());
                try {
                    for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                        copy.addPage(copy.getImportedPage(reader, i));
                    }
                } finally {
                    reader.close();
                }
                outputStream.flush();
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            document.close();
            if (copy != null) {
                copy.close();
            }
            FileUtils.safelyCloseOutputStream(outputStream);
        }
    }

    @Override
    public void doMerge(List<File> list, OutputStream outputStream) throws DocumentException, IOException {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            PdfContentByte cb = writer.getDirectContent();

            for (File file : list) {
                PdfReader reader = new PdfReader(file.getAbsolutePath());
                try {
                    for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                        document.newPage();
                        PdfImportedPage page = writer.getImportedPage(reader, i);
                        cb.addTemplate(page, 0, 0);
                    }
                } finally {
                    reader.close();
                }
                outputStream.flush();
            }
        } finally {
            document.close();
            FileUtils.safelyCloseOutputStream(outputStream);
        }
    }

    public static void main(String[] args) throws IOException {
        PdfReader reader = new PdfReader("/home/unicom/Desktop/Uniware/MSD Fabrics _3.6.pdf");
        PdfTextExtractor.getTextFromPage(reader, 1); //Extracting the content from a particular page.
        PdfReaderContentParser parser = new PdfReaderContentParser(reader);
        TextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
        strategy = parser.processContent(1, strategy);
        System.out.println(strategy.getResultantText());

        /*TaggedPdfReaderTool readertool = new TaggedPdfReaderTool();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        readertool.convertToXml(reader, stream);

        reader.close();
        System.out.println(new String(stream.toByteArray()));*/
    }
}
