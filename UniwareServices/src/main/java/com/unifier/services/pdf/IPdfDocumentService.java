/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 22, 2012
 *  @author singla
 */
package com.unifier.services.pdf;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import com.itextpdf.text.DocumentException;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;

/**
 * @author singla
 */
public interface IPdfDocumentService {

    /**
     * @param outputStream
     * @param html
     */
    void writeHtmlToPdf(OutputStream outputStream, String html);

    /**
     * @param outputStream
     * @param html
     * @param options
     */
    void writeHtmlToPdf(OutputStream outputStream, String html, PrintOptions options);

    /**
     * @param list
     * @param outputStream
     * @throws DocumentException
     * @throws IOException
     */
    void doMerge(List<File> list, OutputStream outputStream) throws DocumentException, IOException;

    void merge(List<File> list, OutputStream outputStream);

}
