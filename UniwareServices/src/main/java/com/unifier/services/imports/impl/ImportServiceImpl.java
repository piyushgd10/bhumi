/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.services.imports.impl;

import com.unifier.core.api.imports.CreateImportJobRequest;
import com.unifier.core.api.imports.CreateImportJobResponse;
import com.unifier.core.api.imports.EditImportJobTypeRequest;
import com.unifier.core.api.imports.EditImportJobTypeResponse;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.api.validation.WsError;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.entity.User;
import com.unifier.core.expressions.Expression;
import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.DelimitedFileParser.RowIterator;
import com.unifier.core.fileparser.LineParser;
import com.unifier.core.fileparser.Row;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.CollectionUtils;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.imports.IImportDao;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.imports.IImportService;
import com.unifier.services.imports.ImportJobHandler;
import com.unifier.services.imports.ImportJobHandlerRequest;
import com.unifier.services.imports.ImportJobHandlerResponse;
import com.unifier.services.imports.ImportJobResponse;
import com.unifier.services.imports.ImportJobTypeColumnDTO;
import com.unifier.services.imports.ImportJobTypeDTO;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.services.configuration.ImportJobConfiguration;
import com.uniware.services.configuration.TenantSystemConfiguration;
import com.uniware.services.document.IDocumentService;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.ImportJobEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@SuppressWarnings("unchecked")
public class ImportServiceImpl implements IImportService {
    private static final Pattern EXCEL_TRUNCATION_PATTERN = Pattern.compile("[1-9]\\.[0-9]+[Ee]\\+[0-9]+");
    public static final  String  COLUMN_NAME_RESULT       = "Import Result";
    public static final  String  COLUMN_NAME_STATUS       = "Import Status";
    public static final  String  COLUMN_NAME_ROW_NO       = "Import Row No";

    private static final Logger LOG = LoggerFactory.getLogger(ImportServiceImpl.class);

    @Autowired
    private IImportDao importDao;

    @Autowired
    private IUsersService usersService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    @Autowired
    private IDocumentService documentService;

    @Override
    public CreateImportJobResponse createImportJob(CreateImportJobRequest request) {
        CreateImportJobResponse response = new CreateImportJobResponse();
        ValidationContext context = new ValidationContext();
        if (StringUtils.isNotBlank(request.getFileUrl())) {
            File file = null;
            try {
                file = downloadImportFile(request.getFileUrl());
                request.setInputStream(new FileInputStream(file));
            } catch (Exception e) {
                context.addError(WsResponseCode.UNKNOWN_ERROR, "Error downloading file", e.getMessage());
            }
            if (!context.hasErrors()) {
                request.setFileName(file.getName());
                request.setImportOption(ImportJobType.ImportOptions.CREATE_NEW_AND_UPDATE_EXISTING.name());
            }
        } else if (request.getInputStream() == null) {
            context.addError(WsResponseCode.INVALID_URI, "File Url is required");
        }
        if (!context.hasErrors()) {
            context = request.validate();
        }
        if (!context.hasErrors()) {
            try {
                LOG.info("Creating import job: {}", request);
                response = createImportJobInternal(request);
            } catch (IOException ioe) {
                context.addError(WsResponseCode.UNKNOWN_ERROR, ioe.getMessage());
                LOG.error("Exception processing import : {}", ioe);
            }
        }
        response.addErrors(context.getErrors());
        if (response.isSuccessful()) {
            ImportJob importJob = getImportJobById(response.getImportJobId());
            ImportJobEvent importJobEvent = new ImportJobEvent();
            importJobEvent.setId(importJob.getId());
            importJobEvent.setName(importJob.getDescription());
            LOG.info("Queuing import event: {}", importJobEvent);
            activeMQConnector.produceContextAwareMessage(MessagingConstants.Queue.IMPORT_QUEUE, importJobEvent);
        }
        return response;
    }

    @Override
    public ImportJob getImportJobById(String importJobId) {
        ImportJob importJob = importDao.getImportJobById(importJobId);
        return importJob;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CreateImportJobResponse createImportJobInternal(CreateImportJobRequest request) throws IOException {
        CreateImportJobResponse response = new CreateImportJobResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            ImportJobConfiguration importJobConfiguration = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class);
            ImportJobType importJobType = importJobConfiguration.getImportJobTypeByName(request.getImportJobTypeName());
            ImportJobTypeDTO importJobTypeConfig = importJobConfiguration.getImportJobTypeConfigByName(request.getImportJobTypeName());
            if (importJobTypeConfig == null) {
                context.addError(WsResponseCode.INVALID_IMPORT_JOB_TYPE_NAME, "Invalid Import Job Type Name");
            } else {
                InputStreamReader isr = new InputStreamReader(request.getInputStream(), "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String header = reader.readLine();
                String[] tokens = LineParser.parseTokens(header, importJobTypeConfig.getDelimiter());
                Set<String> columns = CollectionUtils.asSet(tokens);

                Set<String> missingColumns = new HashSet<String>();
                if (ImportJobType.ImportOptions.UPDATE_EXISTING.name().equals(request.getImportOption())) {
                    for (ImportJobTypeColumnDTO column : importJobTypeConfig.getSourceColumns()) {
                        if (column.isRequiredInUpdate() && !columns.contains(StringUtils.removeNonWordChars(column.getSource()).toLowerCase())) {
                            missingColumns.add(column.getSource());
                        }
                    }
                } else {
                    for (ImportJobTypeColumnDTO column : importJobTypeConfig.getSourceColumns()) {
                        if (column.isRequired() && !columns.contains(StringUtils.removeNonWordChars(column.getSource()).toLowerCase())) {
                            missingColumns.add(column.getSource());
                        }
                    }
                }

                if (missingColumns.size() > 0) {
                    context.addError(WsResponseCode.INVALID_COLUMN_ENTRIES, "missing column(s) in import file : " + StringUtils.join(',', missingColumns));
                } else if (!importJobTypeConfig.getImportOptions().contains(ImportOptions.valueOf(request.getImportOption()))) {
                    context.addError(WsResponseCode.INVALID_REQUEST, "invalid import option");
                } else {
                    String importFilePath = getImportFileRelativePath();
                    String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
                    File importFile = new File(FileUtils.normalizeFilePath(importDirectory, importFilePath));
                    importFile.createNewFile();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(importFile), "UTF-8"));
                    writer.append(header);
                    String line;
                    int totalRows = 0;
                    long upperImportLimit = 0;
                    long globalImportLimit = 100000;
                    if (importJobType.getUpperLimit() != null) {
                        long localImportLimit = new Long(importJobType.getUpperLimit());
                        upperImportLimit = localImportLimit;
                    } else if(ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getUpperLimitForImport() != 0){
                        upperImportLimit = ConfigurationManager.getInstance().getConfiguration(TenantSystemConfiguration.class).getUpperLimitForImport();
                    } else {
                        upperImportLimit = globalImportLimit;
                    }
                    while ((line = reader.readLine()) != null) {
                        writer.newLine();
                        writer.append(line);
                        totalRows++;
                        if (totalRows > upperImportLimit) {
                            context.addError(WsResponseCode.IMPORT_UPPER_LIMIT_EXCEEDED, "Import Rejected as Import Limit Exceeded the Maximum Limit of" + upperImportLimit);
                            break;
                        }
                    }
                    writer.close();
                    if (!context.hasErrors()) {
                        if (importJobType.isGroupBySort()) {
                            ImportJobTypeDTO importJobTypeDTO = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigById(
                                    importJobType.getId());
                            RowIterator iter = new DelimitedFileParser(importFile.getAbsolutePath()).parse();
                            Map<String, List<Row>> groupIdentifierToRows = new LinkedHashMap<>();
                            while (iter.hasNext()) {
                                Row row = iter.next();
                                StringBuilder groupIdentifier = new StringBuilder();
                                for (String groupByColumn : importJobTypeDTO.getGroupByColumns()) {
                                    groupIdentifier.append(row.getColumnValue(groupByColumn));
                                }
                                List<Row> rows = groupIdentifierToRows.get(groupIdentifier.toString());
                                if (rows == null) {
                                    rows = new ArrayList<>();
                                    groupIdentifierToRows.put(groupIdentifier.toString(), rows);
                                }
                                rows.add(row);
                            }
                            String sortedImportFileRelativePath = getImportFileRelativePath();
                            File sortedImportFile = new File(FileUtils.normalizeFilePath(importDirectory, sortedImportFileRelativePath));
                            sortedImportFile.createNewFile();
                            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(sortedImportFile), "UTF-8"));
                            writer.append(header);
                            for (Entry<String, List<Row>> e : groupIdentifierToRows.entrySet()) {
                                for (Row row : e.getValue()) {
                                    writer.newLine();
                                    writer.append(row.toCSV());
                                }
                            }
                            writer.close();
                            importFile = sortedImportFile;
                        }

                        User user = usersService.getUserWithDetailsById(request.getUserId());
                        ImportJob importJob = new ImportJob();
                        importJob.setUserName(user.getUsername());
                        importJob.setImportOption(request.getImportOption());
                        importJob.setImportJobTypeName(importJobType.getName());
                        importJob.setScheduledTime(request.getScheduleTime() != null ? request.getScheduleTime() : DateUtils.getCurrentTime());
                        importJob.setCreated(DateUtils.getCurrentTime());
                        importJob.setStatusCode(ImportJob.Status.SCHEDULED);
                        importJob.setFileName(importFile.getName());
                        importJob.setFilePath(documentService.uploadFile(importFile, "unicommerce-import"));
                        importJob.setUpdated(DateUtils.getCurrentTime());
                        importJob.setTotalRows(totalRows);

                        importJob.setNotificationEmail(request.getNotificationEmail());
                        importJob.setDescription("Import Job " + importJobType.getName() + " created by " + user.getUsername() + " at " + importJob.getCreated());
                        importJob = importDao.addImportJob(importJob);
                        response.setImportJobId(importJob.getId());
                        response.setSuccessful(true);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private String getImportFileRelativePath() {
        return new StringBuilder().append("import-").append(String.valueOf(System.nanoTime())).append(".csv").toString();
    }

    @Override
    public ImportJobResponse executeImportJob(String importJobId) throws Exception {
        ImportJobResponse response = new ImportJobResponse();
        ImportJob importJob = getImportJobById(importJobId);
        importJob.setStatusCode(ImportJob.Status.RUNNING);
        importDao.updateImportJob(importJob);

        String importLogDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportLogDirectoryPath();
        String importFileName = importJob.getFilePath().substring(importJob.getFilePath().lastIndexOf('/') + 1);
        String importLogFilePath = importFileName.substring(0, importFileName.length() - 4) + "_log.csv";
        File logFile = new File(FileUtils.normalizeFilePath(importLogDirectory, importLogFilePath));

        // create log file if import file contains errors
        if (!logFile.exists()) {
            LOG.info("Creating log file: {}", logFile.getName());
            logFile.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile), "UTF-8"));

        String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
        String importFilePath = FileUtils.normalizeFilePath(importDirectory, importJob.getFileName());
        FileUtils.downloadFile(importJob.getFilePath(), importFilePath);

        ImportJobTypeDTO importJobTypeDTO = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName());
        if (importJob.getRowsProcessed() == 0) {
            importJobTypeDTO.getImportJobHandler().preProcessor(importJob);
        }
        DelimitedFileParser parser = new DelimitedFileParser(importFilePath, importJobTypeDTO.getDelimiter());
        RowIterator it = parser.parse();

        writer.append(it.getColumnNames());
        if (!it.hasColumn(COLUMN_NAME_RESULT)) {
            writer.append(importJobTypeDTO.getDelimiter()).append(COLUMN_NAME_RESULT).append(importJobTypeDTO.getDelimiter()).append(COLUMN_NAME_STATUS).append(
                    importJobTypeDTO.getDelimiter()).append(COLUMN_NAME_ROW_NO);
        }

        //skip already processed rows
        it.skip(importJob.getRowsProcessed());

        // Initialize task and task result
        importJob.setMileStoneCount(importJob.getTotalRows());
        importJob.setMileStone(importJob.getRowsProcessed() + " rows processed", importJob.getRowsProcessed());

        User user = usersService.getUserByUsername(importJob.getUserName());
        List<String> groupByColumns = importJobTypeDTO.getGroupByColumns();
        if (groupByColumns != null && groupByColumns.size() > 0) {
            importJob = handleGroupedRowImport(importJob, writer, it, groupByColumns, user);
        } else {
            importJob = handleSingleRowImport(importJob, writer, it, user);
        }
        importJobTypeDTO.getImportJobHandler().postProcessor(importJob);
        writer.close();
        response.setJobName(importJobTypeDTO.getName());
        String logFilePath = documentService.uploadFile(logFile, "unicommerce-import-log");
        response.setLogFilePath(logFilePath);
        response.setSuccessfulImportCount(importJob.getSuccessfulImportCount());
        response.setFailedImportCount(importJob.getFailedImportCount());
        importJob.setLogFilePath(logFilePath);
        if (importJob.getSuccessfulImportCount() > 0) {
            importJob.setSuccessful(true);
        }
        importJob.setStatusCode(ImportJob.Status.COMPLETE);
        importJob.setSuccessfulImportCount(response.getSuccessfulImportCount());
        importJob.setFailedImportCount(response.getFailedImportCount());
        importDao.updateImportJob(importJob);
        return response;
    }

    /**
     * @param importJob
     * @param writer
     * @param it
     * @param groupByColumns
     */
    public ImportJob handleGroupedRowImport(ImportJob importJob, BufferedWriter writer, RowIterator it, List<String> groupByColumns, User user) throws IOException {
        List<Row> rows = new ArrayList<>();
        List<Row> transformedRows = new ArrayList<>();
        String lastRowIdentity = null;
        ImportJobTypeDTO importJobConfig = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName());
        do {
            Row row = it.next();
            Row transformedRow;
            transformedRow = transformRow(importJob, importJobConfig, row);
            if (transformedRow != null) {
                StringBuilder builder = new StringBuilder();
                for (String groupByColumn : groupByColumns) {
                    builder.append(transformedRow.getColumnValue(groupByColumn).trim()).append('|');
                }
                String currentRowIdentity = builder.toString();
                if (!currentRowIdentity.equalsIgnoreCase(lastRowIdentity) && rows.size() > 0 && transformedRows.size() > 0) {
                    try {
                        for (Row tRow : transformedRows) {
                            validateTransformedRow(importJob, importJobConfig, tRow);
                        }
                        ImportJobHandlerResponse response = executeGroupedRowImport(importJob, transformedRows, user);
                        if (!response.isSuccessful()) {
                            for (Row r : rows) {
                                writeResult(writer, r, response);
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        ImportJobHandlerResponse response = new ImportJobHandlerResponse();
                        response.addError(new WsError(e.getMessage()));
                        for (Row r : rows) {
                            writeResult(writer, r, response);
                        }
                        importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                        importJob.setRowsProcessed(row.getLineNo());
                    }
                    rows.clear();
                    transformedRows.clear();
                }
                if (!isFilteredRow(importJobConfig, row)) {
                    rows.add(row);
                    transformedRows.add(transformedRow);
                } else {
                    ImportJobHandlerResponse response = new ImportJobHandlerResponse();
                    response.addError(new WsError("Row Filtered"));
                    writeResult(writer, row, response);
                    importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                }
                lastRowIdentity = currentRowIdentity;
            }
            importJob = updateImportJob(importJob);
        } while (it.hasNext());

        if (rows.size() > 0 && transformedRows.size() > 0) {
            try {
                for (Row tRow : transformedRows) {
                    validateTransformedRow(importJob, importJobConfig, tRow);
                }
                ImportJobHandlerResponse response = executeGroupedRowImport(importJob, transformedRows, user);
                if (!response.isSuccessful()) {
                    for (Row r : rows) {
                        writeResult(writer, r, response);
                    }
                }
            } catch (IllegalArgumentException e) {
                ImportJobHandlerResponse response = new ImportJobHandlerResponse();
                response.addError(new WsError(e.getMessage()));
                for (Row r : rows) {
                    writeResult(writer, r, response);
                }
                importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                importJob.setRowsProcessed(importJob.getTotalRows());
            }
        }
        return importJob;
    }

    public ImportJobHandlerResponse executeGroupedRowImport(ImportJob importJob, List<Row> rows, User user) throws IOException {
        ImportJobHandlerResponse response;
        try {
            response = executeGroupedRows(importJob, rows, user);
            if (response != null) {
                if (!response.isSuccessful()) {
                    importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                } else {
                    importJob.setSuccessfulImportCount(importJob.getSuccessfulImportCount() + 1);
                }
            }
        } catch (Throwable e) {
            response = new ImportJobHandlerResponse();
            response.addError(new WsError(e.getMessage()));
            importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
        } finally {
            importJob.setRowsProcessed(rows.get(rows.size() - 1).getLineNo());
            importJob.setMileStone(rows.get(rows.size() - 1).getLineNo() + " rows processed", rows.size());
        }
        return response;
    }

    /**
     * @param importJob
     * @param rows
     * @return
     * @throws Exception
     */
    public ImportJobHandlerResponse executeGroupedRows(ImportJob importJob, List<Row> rows, User user) throws Exception {
        doSanityCheck(rows);
        ImportJobHandlerRequest request = new ImportJobHandlerRequest();
        request.setUserId(user.getId());
        request.setRows(rows);
        ImportJobHandler importHandler = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName()).getImportJobHandler();
        return importHandler.handleRow(request, ImportOptions.valueOf(importJob.getImportOption()));
    }

    private void validateTransformedRow(ImportJob importJob, ImportJobTypeDTO importJobConfig, Row row) {
        for (ImportJobTypeColumnDTO importJobTypeColumn : importJobConfig.getImportJobTypeColumns()) {
            if (importJobTypeColumn.getSource() != null
                    && ((importJobTypeColumn.isRequired() && !ImportJobType.ImportOptions.UPDATE_EXISTING.name().equals(importJob.getImportOption())) || (importJobTypeColumn.isRequiredInUpdate() && ImportJobType.ImportOptions.UPDATE_EXISTING.name().equals(
                            importJob.getImportOption()))) && StringUtils.isBlank(row.getColumnValue(importJobTypeColumn.getName()))) {
                throw new IllegalArgumentException("Missing required column value "
                        + (StringUtils.isNotBlank(importJobTypeColumn.getSource()) ? importJobTypeColumn.getSource() : importJobTypeColumn.getName()));
            }
        }
    }

    public static Row transformRow(ImportJob importJob, ImportJobTypeDTO importJobConfig, Row row) {
        String[] columnValues = new String[importJobConfig.getImportJobTypeColumns().size()];
        int i = 0;
        for (ImportJobTypeColumnDTO importJobTypeColumn : importJobConfig.getImportJobTypeColumns()) {
            String transformedValue = null;
            if (importJobTypeColumn.getSource() != null) {
                transformedValue = row.getColumnValue(importJobTypeColumn.getSource());
            }
            if (importJobTypeColumn.getValueExpression() != null) {
                Map<String, Object> contextParams = new HashMap<String, Object>();
                contextParams.put("value", transformedValue);
                contextParams.put("row", row);
                transformedValue = importJobTypeColumn.getValueExpression().evaluate(contextParams, String.class);
            }
            columnValues[i++] = transformedValue;
        }
        return new Row(row.getLineNo(), columnValues, importJobConfig.getTransformedRowHeader());
    }

    private void writeResult(BufferedWriter writer, Row row, ImportJobHandlerResponse response) throws IOException {
        StringBuilder builder = new StringBuilder();
        String result;
        String status;
        if (!response.isSuccessful()) {
            result = getResult(response.getErrors());
            status = "FAILED";
        } else {
            result = "";
            status = "SUCCESSFUL";
        }
        if (row.hasColumn(COLUMN_NAME_RESULT)) {
            row.setColumnValue(COLUMN_NAME_RESULT, result);
            row.setColumnValue(COLUMN_NAME_STATUS, status);
            row.setColumnValue(COLUMN_NAME_ROW_NO, String.valueOf(row.getLineNo()));
            builder.append(row.toCSV());
        } else {
            builder.append(row.toCSV()).append(row.getDelimiter()).append(StringUtils.escapeCsv(result)).append(row.getDelimiter()).append(status).append(row.getDelimiter()).append(
                    row.getLineNo());
        }
        writer.newLine();
        writer.append(builder);
    }

    private String getResult(List<WsError> errors) {
        StringBuilder builder = new StringBuilder();
        for (WsError error : errors) {
            builder.append(error.getDescription()).append('|');
        }
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    /**
     * @param importJob
     * @param writer
     * @param it
     * @throws IOException
     */
    public ImportJob handleSingleRowImport(ImportJob importJob, BufferedWriter writer, RowIterator it, User user) throws IOException {
        ImportJobTypeDTO importJobConfig = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName());
        while (it.hasNext()) {
            Row row = it.next();
            Row transformedRow = null;
            try {
                transformedRow = transformRow(importJob, importJobConfig, row);
                validateTransformedRow(importJob, importJobConfig, transformedRow);
            } catch (IllegalArgumentException e) {
                ImportJobHandlerResponse response = new ImportJobHandlerResponse();
                response.addError(new WsError(e.getMessage()));
                transformedRow = null;
                writeResult(writer, row, response);
                importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                importJob.setRowsProcessed(row.getLineNo());
            }
            if (transformedRow != null) {
                if (!isFilteredRow(importJobConfig, row)) {
                    ImportJobHandlerResponse response = executeRowImport(importJob, transformedRow, user);
                    if (!response.isSuccessful()) {
                        writeResult(writer, row, response);
                    }
                } else {
                    ImportJobHandlerResponse response = new ImportJobHandlerResponse();
                    response.addError(new WsError("Row Filtered"));
                    writeResult(writer, row, response);
                    importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                }
            }
            importJob = updateImportJob(importJob);
        }
        return importJob;
    }

    @Override
    @Transactional
    public ImportJob updateImportJob(ImportJob importJob) {
        return importDao.updateImportJob(importJob);
    }

    public ImportJobHandlerResponse executeRowImport(ImportJob importJob, Row row, User user) throws IOException {
        ImportJobHandlerResponse response;
        try {
            response = executeRow(importJob, row, user);
            if (response != null) {
                if (!response.isSuccessful()) {
                    importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
                } else {
                    importJob.setSuccessfulImportCount(importJob.getSuccessfulImportCount() + 1);
                }
                importJob.setMileStone((importJob.getSuccessfulImportCount() + 1) + " rows processed.");
            }
        } catch (Exception e) {
            response = new ImportJobHandlerResponse();
            response.addError(new WsError(e.getMessage()));
            importJob.setFailedImportCount(importJob.getFailedImportCount() + 1);
        } finally {
            importJob.setRowsProcessed(row.getLineNo());
        }
        return response;
    }

    public ImportJobHandlerResponse executeRow(ImportJob importJob, Row row, User user) throws Exception {
        doSanityCheck(row);
        ImportJobHandlerRequest request = new ImportJobHandlerRequest();
        request.setUserId(user.getId());
        request.setRow(row);
        ImportJobHandler importHandler = ConfigurationManager.getInstance().getConfiguration(ImportJobConfiguration.class).getImportJobTypeConfigByName(
                importJob.getImportJobTypeName()).getImportJobHandler();
        return importHandler.handleRow(request, ImportOptions.valueOf(importJob.getImportOption()));
    }

    private void doSanityCheck(Row row) {
        List<Row> rows = new ArrayList<Row>(1);
        rows.add(row);
        doSanityCheck(rows);
    }

    private void doSanityCheck(List<Row> rows) {
        for (Row row : rows) {
            for (String value : row.getColumnValues()) {
                if (StringUtils.isNotBlank(value)) {
                    Matcher m = EXCEL_TRUNCATION_PATTERN.matcher(value);
                    if (m.matches()) {
                        throw new IllegalArgumentException("Excel truncation error in value: " + value);
                    }
                }
            }
        }
    }

    @Override
    public ImportJobHandler constructImportJobHandler(ImportJobType jobType) throws Exception {
        Class<? extends ImportJobHandler> handlerClass = (Class<? extends ImportJobHandler>) Class.forName(jobType.getHandlerClass());
        ImportJobHandler importHandler = handlerClass.newInstance();
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(importHandler, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        return importHandler;
    }

    private boolean isFilteredRow(ImportJobTypeDTO importJobConfig, Row row) {
        if (importJobConfig.getFilterExpressions() != null && importJobConfig.getFilterExpressions().size() > 0) {
            for (Expression filterExpression : importJobConfig.getFilterExpressions()) {
                Map<String, Object> contextParams = new HashMap<String, Object>();
                contextParams.put("row", row);
                if (filterExpression.evaluate(contextParams, Boolean.class)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    @Transactional
    public List<ImportJobType> getAllImportJobTypes() {
        return importDao.getAllImportJobTypes();
    }

    @Override
    @Transactional
    public ImportJobType getImportJobTypeByName(String name) {
        return importDao.getImportJobTypeByName(name);
    }

    @Override
    @Transactional
    @MarkDirty(values = { ImportJobConfiguration.class })
    public EditImportJobTypeResponse editImportJobType(EditImportJobTypeRequest request) {
        EditImportJobTypeResponse response = new EditImportJobTypeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ImportJobType importJobType = getImportJobTypeByName(request.getName());
            if (importJobType == null) {
                context.addError(WsResponseCode.INVALID_IMPORT_JOB_TYPE_NAME, "Invalid Import Job Type");
            } else {
                importJobType.setImportJobConfig(request.getImportJobConfig());
                importJobType.setImportOptions(StringUtils.join(',', request.getImportOptions()));
                importJobType.setEnabled(request.isEnabled());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<ImportJobType> getImportJobTypes() {
        return importDao.getImportJobTypes();
    }

    @Override
    public List<ImportJob> getUserImports(String username) {
        return importDao.getUserImports(username);
    }

    private File downloadImportFile(String sourcePath) {
        String importFilePath = getImportFileRelativePath();
        String importDirectory = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getImportDirectoryPath();
        return FileUtils.downloadFile(sourcePath, FileUtils.normalizeFilePath(importDirectory , importFilePath));
    }
}
