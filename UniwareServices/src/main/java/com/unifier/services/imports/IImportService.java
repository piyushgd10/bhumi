/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import com.unifier.core.api.imports.CreateImportJobRequest;
import com.unifier.core.api.imports.CreateImportJobResponse;
import com.unifier.core.api.imports.EditImportJobTypeRequest;
import com.unifier.core.api.imports.EditImportJobTypeResponse;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface IImportService {

    CreateImportJobResponse createImportJob(CreateImportJobRequest request) ;

    ImportJobResponse executeImportJob(String importJobId) throws Exception;

    ImportJob getImportJobById(String importJobId);

    ImportJobHandler constructImportJobHandler(ImportJobType jobType) throws Exception;

    List<ImportJobType> getAllImportJobTypes();

    ImportJobType getImportJobTypeByName(String name);

    EditImportJobTypeResponse editImportJobType(EditImportJobTypeRequest request);

    ImportJob updateImportJob(ImportJob importJob);

    List<ImportJobType> getImportJobTypes();

    List<ImportJob> getUserImports(String username);
}
