/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Apr-2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import com.unifier.core.api.base.ServiceResponse;

public class ImportJobResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long serialVersionUID = 7575937690592606561L;
    private String            jobName;
    private String            logFilePath;
    private int               successfulImportCount;
    private int               failedImportCount;

    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * @return the logFilePath
     */
    public String getLogFilePath() {
        return logFilePath;
    }

    /**
     * @param logFilePath the logFilePath to set
     */
    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    /**
     * @return the successfulImportCount
     */
    public int getSuccessfulImportCount() {
        return successfulImportCount;
    }

    /**
     * @param successfulImportCount the successfulImportCount to set
     */
    public void setSuccessfulImportCount(int successfulImportCount) {
        this.successfulImportCount = successfulImportCount;
    }

    /**
     * @return the failedImportCount
     */
    public int getFailedImportCount() {
        return failedImportCount;
    }

    /**
     * @param failedImportCount the failedImportCount to set
     */
    public void setFailedImportCount(int failedImportCount) {
        this.failedImportCount = failedImportCount;
    }

}
