/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Apr 2, 2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.entity.ImportJobType;
import com.unifier.core.entity.ImportJobType.ImportOptions;
import com.unifier.core.expressions.Expression;
import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.utils.StringUtils;

public class ImportJobTypeDTO implements Serializable {
    private String                             name;
    private char                               delimiter            = DelimitedFileParser.DEFAULT_DELIMITER;
    private List<ImportOptions>                importOptions        = new ArrayList<ImportJobType.ImportOptions>();
    private ImportJobHandler                   importJobHandler;
    private final List<ImportJobTypeColumnDTO> importJobTypeColumns = new ArrayList<ImportJobTypeColumnDTO>();
    private final List<ImportJobTypeColumnDTO> sourceColumns        = new ArrayList<ImportJobTypeColumnDTO>();
    private List<String>                       groupByColumns;
    private List<Expression>                   filterExpressions;
    private final Map<String, Integer>         transformedRowHeader = new HashMap<String, Integer>();

    private Expression                         logExpression;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the importJobTypeColumns
     */
    @JsonIgnore
    public List<ImportJobTypeColumnDTO> getImportJobTypeColumns() {
        return importJobTypeColumns;
    }

    /**
     * @return the importJobHandler
     */
    @JsonIgnore
    public ImportJobHandler getImportJobHandler() {
        return importJobHandler;
    }

    /**
     * @param importJobHandler the importJobHandler to set
     */
    public void setImportJobHandler(ImportJobHandler importJobHandler) {
        this.importJobHandler = importJobHandler;
    }

    public void addColumn(ImportJobTypeColumnDTO importJobTypeColumnDTO) {
        this.importJobTypeColumns.add(importJobTypeColumnDTO);
        this.transformedRowHeader.put(StringUtils.removeNonWordChars(importJobTypeColumnDTO.getName()).toLowerCase(), this.importJobTypeColumns.size() - 1);
        if (StringUtils.isNotEmpty(importJobTypeColumnDTO.getSource())) {
            sourceColumns.add(importJobTypeColumnDTO);
        }
        if (importJobTypeColumnDTO.isGroupBy()) {
            if (this.groupByColumns == null) {
                this.groupByColumns = new ArrayList<String>();
            }
            this.groupByColumns.add(importJobTypeColumnDTO.getName());
        }
    }

    public Map<String, Integer> getTransformedRowHeader() {
        return transformedRowHeader;
    }

    /**
     * @return the groupByColumns
     */
    @JsonIgnore
    public List<String> getGroupByColumns() {
        return groupByColumns;
    }

    /**
     * @return the sourceColumns
     */
    public List<ImportJobTypeColumnDTO> getSourceColumns() {
        return sourceColumns;
    }

    /**
     * @return the filterExpressions
     */
    @JsonIgnore
    public List<Expression> getFilterExpressions() {
        return filterExpressions;
    }

    public void addFilterExpression(Expression filterExpression) {
        if (this.filterExpressions == null) {
            this.filterExpressions = new ArrayList<Expression>();
        }
        this.filterExpressions.add(filterExpression);
    }

    /**
     * @return the logExpression
     */
    public Expression getLogExpression() {
        return logExpression;
    }

    /**
     * @param logExpression the logExpression to set
     */
    public void setLogExpression(Expression logExpression) {
        this.logExpression = logExpression;
    }

    /**
     * @return the importOptions
     */
    public List<ImportOptions> getImportOptions() {
        return importOptions;
    }

    /**
     * @param importOptions the importOptions to set
     */
    public void setImportOptions(List<ImportOptions> importOptions) {
        this.importOptions = importOptions;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }

}
