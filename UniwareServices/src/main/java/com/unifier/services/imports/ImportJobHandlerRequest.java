/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 22, 2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import com.unifier.core.api.base.ServiceRequest;
import com.unifier.core.fileparser.Row;

import java.util.List;


public class ImportJobHandlerRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 9112543563339592557L;
    private Integer           userId;
    private Row               row;
    private List<Row>         rows;

    /**
     * @return the row
     */
    public Row getRow() {
        return row;
    }

    /**
     * @param row the row to setupdateExisting
     */
    public void setRow(Row row) {
        this.row = row;
    }

    /**
     * @return the rows
     */
    public List<Row> getRows() {
        return rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
