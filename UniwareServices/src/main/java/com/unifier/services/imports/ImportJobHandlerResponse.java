/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 22, 2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.validation.WsError;

public class ImportJobHandlerResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long   serialVersionUID = 1384622641809075534L;
    private Map<String, Object> resultItems      = new HashMap<String, Object>();

    public ImportJobHandlerResponse() {
    }

    public ImportJobHandlerResponse(ServiceResponse response) {
        if (response.hasErrors()) {
            for (WsError error : response.getErrors()) {
                this.addError(error);
            }
        }
        this.setSuccessful(response.isSuccessful());
    }

    public void addResponse(ServiceResponse response) {
        if (response.hasErrors()) {
            for (WsError error : response.getErrors()) {
                this.addError(error);
            }
        }
        this.setSuccessful(response.isSuccessful());
    }

    @Override
    public String getMessage() {
        StringBuilder builder = new StringBuilder();
        for (WsError error : this.getErrors()) {
            builder.append(error.getDescription()).append('|');
        }

        return builder.deleteCharAt(builder.length() - 1).toString();
    }

    /**
     * @return the resultItems
     */
    public Map<String, Object> getResultItems() {
        return resultItems;
    }

    /**
     * @param resultItems the resultItems to set
     */
    public void setResultItems(Map<String, Object> resultItems) {
        this.resultItems = resultItems;
    }

    public void addResultItem(String key, Object value) {
        this.resultItems.put(key, value);
    }
}
