/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 26, 2012
 *  @author praveeng
 */
package com.unifier.services.imports;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.unifier.core.expressions.Expression;

import java.io.Serializable;

public class ImportJobTypeColumnDTO implements Serializable {
    private String     name;
    private String     source;
    private boolean    required;
    private boolean    requiredInUpdate;
    private String     description;
    private Expression valueExpression;
    private boolean    groupBy;

    public ImportJobTypeColumnDTO(String name, String source, boolean required, boolean requiredInUpdate, String description, Expression valueExpression, boolean groupBy) {
        this.name = name;
        this.source = source;
        this.required = required;
        this.requiredInUpdate = requiredInUpdate;
        this.description = description;
        this.valueExpression = valueExpression;
        this.groupBy = groupBy;
    }

    /**
     * 
     */
    public ImportJobTypeColumnDTO() {
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isRequiredInUpdate() {
        return requiredInUpdate;
    }

    public void setRequiredInUpdate(boolean requiredInUpdate) {
        this.requiredInUpdate = requiredInUpdate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the groupBy
     */
    @JsonIgnore
    public boolean isGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(boolean groupBy) {
        this.groupBy = groupBy;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the valueExpression
     */
    @JsonIgnore
    public Expression getValueExpression() {
        return valueExpression;
    }

    /**
     * @param valueExpression the valueExpression to set
     */
    public void setValueExpression(Expression valueExpression) {
        this.valueExpression = valueExpression;
    }

}
