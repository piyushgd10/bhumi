package com.unifier.services.printing;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.AddPrintTemplateResponse;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesRequest;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesResponse;
import com.unifier.core.api.print.GetPrintTemplateByTypeRequest;
import com.unifier.core.api.print.GetPrintTemplateByTypeResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixRequest;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixResponse;
import com.uniware.core.api.print.GetPrintableTemplateRequest;
import com.uniware.core.api.print.GetPrintableTemplateResponse;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;

import java.util.List;

/**
 * Created by admin on 9/4/15.
 */
public interface IGlobalPrintConfigService {

    GetAllSamplePrintTemplatesResponse getAllGlobalSamplePrintTemplates(GetAllSamplePrintTemplatesRequest request);

    GetSamplePrintTemplatesByTypeResponse getGlobalSamplePrintTemplatesByType(GetSamplePrintTemplatesByTypeRequest request);

    GetPrintTemplateByTypeResponse getPrintTemplateByType(GetPrintTemplateByTypeRequest request);

    SamplePrintTemplateVO getGlobalSamplePrintTemplateByTypeAndName(String type, String name);

    SamplePrintTemplateVO getGlobalSamplePrintTemplate(String code);

    void updateSampleTemplate(SamplePrintTemplateVO template);

    void removeSampleTemplate(SamplePrintTemplateVO template);

    List<SamplePrintTemplateVO> getGlobalSamplePrintTemplates();

    PrintTemplateVO getPrintTemplateByType(PrintTemplateVO.Type type);

    GetPrintableTemplateResponse getPrintableTemplate(GetPrintableTemplateRequest request);

    GetPrintableTemplateByFilenamePrefixResponse getPrintableTemplateByFileNamePrefix(GetPrintableTemplateByFilenamePrefixRequest request);
}
