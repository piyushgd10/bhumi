/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 14, 2012
 *  @author praveeng
 */
package com.unifier.services.printing;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.AddPrintTemplateResponse;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesRequest;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesResponse;
import com.unifier.core.api.print.GetPrintTemplateByTypeRequest;
import com.unifier.core.api.print.GetPrintTemplateByTypeResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixRequest;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixResponse;
import com.uniware.core.api.print.GetPrintableTemplateRequest;
import com.uniware.core.api.print.GetPrintableTemplateResponse;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.PrintTemplateVO.Type;
import com.uniware.core.vo.SamplePrintTemplateVO;

import java.util.List;

/**
 * @author praveeng
 */
public interface IPrintConfigService {

    /**
     * @param type
     * @return
     */
    PrintTemplateVO getPrintTemplateByType(Type type);

    EditPrintTemplateResponse editPrintTemplate(EditPrintTemplateRequest request);

    List<PrintTemplateDTO> getPrintTemplates();

    GetSamplePrintTemplatesByTypeResponse getSamplePrintTemplatesByType(GetSamplePrintTemplatesByTypeRequest request);

    SamplePrintTemplateVO getSamplePrintTemplateByTypeAndName(String type, String name);

    AddPrintTemplateResponse addPrintTemplate(AddPrintTemplateRequest request);

    GetPrintTemplateByTypeResponse getPrintTemplateByType(GetPrintTemplateByTypeRequest request);

    GetAllSamplePrintTemplatesResponse getAllSamplePrintTemplates(GetAllSamplePrintTemplatesRequest request);

    List<SamplePrintTemplateVO> getSamplePrintTemplates();

    List<PrintTemplateVO> getAllPrintTemplates();

    SamplePrintTemplateVO getSamplePrintTemplate(String code);

    @Deprecated
    GetPrintableTemplateResponse getPrintableTemplate(GetPrintableTemplateRequest request);

    GetPrintableTemplateByFilenamePrefixResponse getPrintableTemplateByFileNamePrefix(GetPrintableTemplateByFilenamePrefixRequest request);
}
