package com.unifier.services.printing.impl;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.AddPrintTemplateResponse;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesRequest;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesResponse;
import com.unifier.core.api.print.GetPrintTemplateByTypeRequest;
import com.unifier.core.api.print.GetPrintTemplateByTypeResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.api.print.PrintTemplateMinDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.printing.IGlobalPrintConfigMao;
import com.unifier.dao.printing.IPrintConfigMao;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.printing.IGlobalPrintConfigService;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixRequest;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixResponse;
import com.uniware.core.api.print.GetPrintableTemplateRequest;
import com.uniware.core.api.print.GetPrintableTemplateResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.GlobalSamplePrintTemplateCache;
import com.uniware.services.cache.PrintTemplateCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 9/4/15.
 */
@Service
public class GlobalPrintConfigServiceImpl implements IGlobalPrintConfigService {

    private static final Logger LOG = LoggerFactory.getLogger(PrintConfigServiceImpl.class);

    @Autowired
    private IGlobalPrintConfigMao globalPrintConfigMao;

    @Autowired
    private IPrintConfigMao printConfigMao;

    @Autowired
    private ITenantProfileService tenantProfileService;

    @Override
    public GetAllSamplePrintTemplatesResponse getAllGlobalSamplePrintTemplates(GetAllSamplePrintTemplatesRequest request) {
        GetAllSamplePrintTemplatesResponse response = new GetAllSamplePrintTemplatesResponse();
        Map<String, List<PrintTemplateMinDTO>> typeToMinTemplates = new HashMap<String, List<PrintTemplateMinDTO>>();
        GlobalSamplePrintTemplateCache globalSamplePrintTemplateCache = CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class);
        PrintTemplateCache printTemplateCache = CacheManager.getInstance().getCache(PrintTemplateCache.class);
        for (String reqType : request.getTypeList()) {
            List<PrintTemplateMinDTO> minDTOList = new ArrayList<PrintTemplateMinDTO>();
            PrintTemplateVO selectedPrintTemplate = printTemplateCache.getPrintTemplateByType(PrintTemplateVO.Type.valueOf(reqType));
            SamplePrintTemplateVO selectedSamplePrintTemplate = null;
            if (selectedPrintTemplate != null) {
                selectedSamplePrintTemplate = globalSamplePrintTemplateCache.getGlobalSamplePrintTemplate(selectedPrintTemplate.getSamplePrintTemplateCode());
            } else {
                LOG.info("No selected print template found for type: {}", reqType);
            }
            for (PrintTemplateDTO templateDTO : globalSamplePrintTemplateCache.getGlobalSamplePrintTemplatesByType(reqType)) {
                PrintTemplateMinDTO minDTO = new PrintTemplateMinDTO(templateDTO);
                if (selectedSamplePrintTemplate != null) {
                    minDTO.setSelected(selectedSamplePrintTemplate.getName().equals(templateDTO.getName()));
                }
                minDTOList.add(minDTO);
            }
            typeToMinTemplates.put(reqType, minDTOList);
        }
        response.setTypeToPrintTemplates(typeToMinTemplates);
        response.setSuccessful(true);
        return response;
    }

    @Override
    public GetSamplePrintTemplatesByTypeResponse getGlobalSamplePrintTemplatesByType(GetSamplePrintTemplatesByTypeRequest request) {
        GetSamplePrintTemplatesByTypeResponse response = new GetSamplePrintTemplatesByTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.getSamplePrintTemplates().addAll(CacheManager.getInstance().getCache(GlobalSamplePrintTemplateCache.class).getGlobalSamplePrintTemplatesByType(request.getType()));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetPrintTemplateByTypeResponse getPrintTemplateByType(GetPrintTemplateByTypeRequest request) {
        GetPrintTemplateByTypeResponse response = new GetPrintTemplateByTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            PrintTemplateVO printTemplate = getPrintTemplateByType(PrintTemplateVO.Type.valueOf(request.getType()));
            if (printTemplate != null) {
                SamplePrintTemplateVO globalSamplePrintTemplate = getGlobalSamplePrintTemplate(printTemplate.getSamplePrintTemplateCode());
                PrintTemplateDTO dto = null;
                if (globalSamplePrintTemplate != null) {
                    dto = new PrintTemplateDTO();
                    dto.setSelected(true);
                    response.setPrintTemplate(dto);
                    response.setSuccessful(true);
                }
            }
        }
        return response;
    }

    @Override
    public PrintTemplateVO getPrintTemplateByType(PrintTemplateVO.Type type) {
        return printConfigMao.getPrintTemplateByType(type.name());
    }

    @Override
    public SamplePrintTemplateVO getGlobalSamplePrintTemplateByTypeAndName(String type, String name) {
        return globalPrintConfigMao.getGlobalSamplePrintTemplateByTypeAndName(type, name);
    }

    @Override
    public SamplePrintTemplateVO getGlobalSamplePrintTemplate(String code) {
        return globalPrintConfigMao.getGlobalSamplePrintTemplate(code);
    }

    @Override
    public List<SamplePrintTemplateVO> getGlobalSamplePrintTemplates() {
        return globalPrintConfigMao.getGlobalSamplePrintTemplates();
    }

    @Override
    public void updateSampleTemplate(SamplePrintTemplateVO template) {
        globalPrintConfigMao.updateGlobalSampleTemplate(template);
    }

    @Override
    public void removeSampleTemplate(SamplePrintTemplateVO template) {
        globalPrintConfigMao.removeGlobalSamplePrintTemplate(template.getCode());
    }

    @Override
    public GetPrintableTemplateResponse getPrintableTemplate(GetPrintableTemplateRequest request) {
        GetPrintableTemplateResponse response = new GetPrintableTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (tenantProfileService.getTenantProfileByLicenseKey(request.getLicenseKey()) != null) {
                for (String type : request.getTypeList()) {
                    PrintTemplateVO printTemplate = getPrintTemplateByType(PrintTemplateVO.Type.valueOf(type.trim()));
                    if (printTemplate != null) {
                        SamplePrintTemplateVO samplePrintTemplateVO = getGlobalSamplePrintTemplate(printTemplate.getSamplePrintTemplateCode());
                        // serve only openly accessible templates
                        if (samplePrintTemplateVO.isOpenlyAccessible()) {
                            PrintTemplateDTO dto = new PrintTemplateDTO(samplePrintTemplateVO);
                            response.getTypeToTemplateMap().put(type, dto);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetPrintableTemplateByFilenamePrefixResponse getPrintableTemplateByFileNamePrefix(GetPrintableTemplateByFilenamePrefixRequest request) {
        GetPrintableTemplateByFilenamePrefixResponse response = new GetPrintableTemplateByFilenamePrefixResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (tenantProfileService.getTenantProfileByLicenseKey(request.getLicenseKey()) != null) {
                for (String prefix : request.getFileNamePrefixList()) {
                    SamplePrintTemplateVO spt = CacheManager.getInstance().getCache(PrintTemplateCache.class).getSelectedSamplePrintTemplateByFileNamePrefix(prefix);
                    if (spt != null && spt.isOpenlyAccessible()) {
                        response.getFileNamePrefixToTemplateMap().put(prefix, new PrintTemplateDTO(spt));
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

}
