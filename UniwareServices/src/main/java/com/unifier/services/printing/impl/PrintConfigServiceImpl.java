/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, Mar 14, 2012
 *  @author praveeng
 */

package com.unifier.services.printing.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.AddPrintTemplateResponse;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesRequest;
import com.unifier.core.api.print.GetAllSamplePrintTemplatesResponse;
import com.unifier.core.api.print.GetPrintTemplateByTypeRequest;
import com.unifier.core.api.print.GetPrintTemplateByTypeResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.api.print.PrintTemplateMinDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.template.Template;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.printing.IPrintConfigMao;
import com.unifier.services.aspect.MarkDirty;
import com.unifier.services.printing.IGlobalPrintConfigService;
import com.unifier.services.printing.IPrintConfigService;
import com.unifier.services.tenantprofile.service.ITenantProfileService;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixRequest;
import com.uniware.core.api.print.GetPrintableTemplateByFilenamePrefixResponse;
import com.uniware.core.api.print.GetPrintableTemplateRequest;
import com.uniware.core.api.print.GetPrintableTemplateResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.PrintTemplateVO.Type;
import com.uniware.core.vo.SamplePrintTemplateVO;
import com.uniware.services.cache.PrintTemplateCache;
import com.uniware.services.cache.SamplePrintTemplateCache;

/**
 * @author Sunny Agarwal
 */
@Service
public class PrintConfigServiceImpl implements IPrintConfigService {

    private static final Logger       LOG = LoggerFactory.getLogger(PrintConfigServiceImpl.class);

    @Autowired
    private IPrintConfigMao           printConfigMao;

    @Autowired
    private IGlobalPrintConfigService globalPrintConfigService;

    @Autowired
    private ITenantProfileService     tenantProfileService;

    @Override
    public GetAllSamplePrintTemplatesResponse getAllSamplePrintTemplates(GetAllSamplePrintTemplatesRequest request) {
        GetAllSamplePrintTemplatesResponse response = new GetAllSamplePrintTemplatesResponse();
        Map<String, List<PrintTemplateMinDTO>> typeToMinTemplates = new HashMap<String, List<PrintTemplateMinDTO>>();
        SamplePrintTemplateCache samplePrintTemplateCache = CacheManager.getInstance().getCache(SamplePrintTemplateCache.class);
        PrintTemplateCache printTempalteCache = CacheManager.getInstance().getCache(PrintTemplateCache.class);
        for (String reqType : request.getTypeList()) {
            List<PrintTemplateMinDTO> minDTOList = new ArrayList<PrintTemplateMinDTO>();
            PrintTemplateVO selectedPrintTemplate = printTempalteCache.getPrintTemplateByType(Type.valueOf(reqType));
            SamplePrintTemplateVO selectedSamplePrintTemplate = null;
            if (selectedPrintTemplate != null) {
                selectedSamplePrintTemplate = samplePrintTemplateCache.getSamplePrintTemplate(selectedPrintTemplate.getSamplePrintTemplateCode());
            } else {
                LOG.info("No selected print template found for type: {}", reqType);
            }
            for (PrintTemplateDTO templateDTO : samplePrintTemplateCache.getSamplePrintTemplatesByType(reqType)) {
                PrintTemplateMinDTO minDTO = new PrintTemplateMinDTO(templateDTO);
                if (selectedSamplePrintTemplate != null) {
                    minDTO.setSelected(selectedSamplePrintTemplate.getName().equals(templateDTO.getName()));
                }
                minDTOList.add(minDTO);
            }
            typeToMinTemplates.put(reqType, minDTOList);
        }
        response.setTypeToPrintTemplates(typeToMinTemplates);
        response.setSuccessful(true);
        return response;
    }

    @Override
    public GetSamplePrintTemplatesByTypeResponse getSamplePrintTemplatesByType(GetSamplePrintTemplatesByTypeRequest request) {
        GetSamplePrintTemplatesByTypeResponse response = new GetSamplePrintTemplatesByTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            response.getSamplePrintTemplates().addAll(CacheManager.getInstance().getCache(SamplePrintTemplateCache.class).getSamplePrintTemplatesByType(request.getType()));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    public GetPrintTemplateByTypeResponse getPrintTemplateByType(GetPrintTemplateByTypeRequest request) {
        GetPrintTemplateByTypeResponse response = new GetPrintTemplateByTypeResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            PrintTemplateVO printTemplate = getPrintTemplateByType(PrintTemplateVO.Type.valueOf(request.getType()));
            if (printTemplate != null) {
                PrintTemplateDTO dto = new PrintTemplateDTO(getSamplePrintTemplate(printTemplate.getSamplePrintTemplateCode()));
                dto.setSelected(true);
                response.setPrintTemplate(dto);
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @MarkDirty(values = { SamplePrintTemplateCache.class, PrintTemplateCache.class })
    public AddPrintTemplateResponse addPrintTemplate(AddPrintTemplateRequest request) {
        AddPrintTemplateResponse response = new AddPrintTemplateResponse();
        ValidationContext context = request.validate();
        SamplePrintTemplateVO samplePrintTemplate = null;
        if (request.isGlobal())
            samplePrintTemplate = globalPrintConfigService.getGlobalSamplePrintTemplateByTypeAndName(request.getType(), request.getName());
        else
            samplePrintTemplate = getSamplePrintTemplateByTypeAndName(request.getType(), request.getName());

        if (!context.hasErrors() && samplePrintTemplate == null) {
            context.addError(WsResponseCode.INVALID_SAMPLE_TEMPLATE);
        }

        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            PrintTemplateVO printTemplate = getPrintTemplateByType(PrintTemplateVO.Type.valueOf(request.getType()));
            if (printTemplate == null) {
                printTemplate = new PrintTemplateVO();
                printTemplate.setEnabled(true);
                printTemplate.setType(request.getType());
                Date now = DateUtils.getCurrentDate();
                printTemplate.setCreated(now);
                printTemplate.setUpdated(now);
            }
            printTemplate.setSamplePrintTemplateCode(samplePrintTemplate.getCode());
            printConfigMao.updatePrintTemplate(printTemplate);
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @MarkDirty(values = { SamplePrintTemplateCache.class, PrintTemplateCache.class })
    public EditPrintTemplateResponse editPrintTemplate(EditPrintTemplateRequest request) {
        EditPrintTemplateResponse response = new EditPrintTemplateResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        } else {
            try {
                Template.compile(request.getWsPrintTemplate().getName(), request.getWsPrintTemplate().getTemplate());
            } catch (Exception e) {
                context.addError(WsResponseCode.INVALID_SAMPLE_TEMPLATE, "Unable to compile template. " + e);
            }
        }
        if (!context.hasErrors()) {
            SamplePrintTemplateVO template;
            if (!request.getWsPrintTemplate().isGlobal()) {
                template = getSamplePrintTemplateByTypeAndName(request.getWsPrintTemplate().getType(), request.getWsPrintTemplate().getName());
            } else {
                template = globalPrintConfigService.getGlobalSamplePrintTemplateByTypeAndName(request.getWsPrintTemplate().getType(), request.getWsPrintTemplate().getName());
            }
            if (template == null) {
                template = new SamplePrintTemplateVO(request.getWsPrintTemplate().getType(), request.getWsPrintTemplate().getName());
            }
            template.setDescription(request.getWsPrintTemplate().getDescription());
            template.setTemplate(request.getWsPrintTemplate().getTemplate());
            template.setPageMargins(request.getWsPrintTemplate().getPageMargins());
            template.setPageSize(request.getWsPrintTemplate().getPageSize());
            template.setNumberOfCopies(request.getWsPrintTemplate().getNumberOfCopies());
            template.setPrintDialog(request.getWsPrintTemplate().getPrintDialog());
            template.setLandscape(request.getWsPrintTemplate().isLandscape());
            template.setUpdated(DateUtils.getCurrentTime());
            if (request.getWsPrintTemplate().isGlobal()) {
                globalPrintConfigService.updateSampleTemplate(template);
            } else {
                printConfigMao.updateSampleTemplate(template);
            }
            response.setResponse(addPrintTemplate(new AddPrintTemplateRequest(request.getWsPrintTemplate().getType(), request.getWsPrintTemplate().getName(),
                    request.getWsPrintTemplate().isGlobal())));
        }
        response.setSuccessful(!context.hasErrors());
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public List<PrintTemplateDTO> getPrintTemplates() {
        List<PrintTemplateDTO> printTemplate = new ArrayList<>();
        for (PrintTemplateVO template : printConfigMao.getPrintTemplates()) {
            SamplePrintTemplateVO samplePrintTemplate = getSamplePrintTemplate(template.getSamplePrintTemplateCode());
            PrintTemplateDTO dto = new PrintTemplateDTO(samplePrintTemplate);
            dto.setSelected(true);
            printTemplate.add(dto);
        }
        return printTemplate;
    }

    @Override
    public PrintTemplateVO getPrintTemplateByType(Type type) {
        return printConfigMao.getPrintTemplateByType(type.name());
    }

    @Override
    public SamplePrintTemplateVO getSamplePrintTemplateByTypeAndName(String type, String name) {
        SamplePrintTemplateVO samplePrintTemplateVO = printConfigMao.getSamplePrintTemplateByTypeAndName(type, name);
        return samplePrintTemplateVO;
    }

    @Override
    public SamplePrintTemplateVO getSamplePrintTemplate(String code) {
        SamplePrintTemplateVO samplePrintTemplateVO = printConfigMao.getSamplePrintTemplate(code);
        return samplePrintTemplateVO;
    }

    @Override
    public List<SamplePrintTemplateVO> getSamplePrintTemplates() {
        return printConfigMao.getSamplePrintTemplates();
    }

    @Override
    public List<PrintTemplateVO> getAllPrintTemplates() {
        return printConfigMao.getPrintTemplates();
    }

    @Override
    public GetPrintableTemplateResponse getPrintableTemplate(GetPrintableTemplateRequest request) {
        GetPrintableTemplateResponse response = new GetPrintableTemplateResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (tenantProfileService.getTenantProfileByLicenseKey(request.getLicenseKey()) != null) {
                for (String type : request.getTypeList()) {
                    PrintTemplateVO printTemplate = getPrintTemplateByType(PrintTemplateVO.Type.valueOf(type.trim()));
                    if (printTemplate != null) {
                        SamplePrintTemplateVO samplePrintTemplateVO = getSamplePrintTemplate(printTemplate.getSamplePrintTemplateCode());
                        // serve only openly accessible templates
                        if (samplePrintTemplateVO.isOpenlyAccessible()) {
                            PrintTemplateDTO dto = new PrintTemplateDTO(samplePrintTemplateVO);
                            response.getTypeToTemplateMap().put(type, dto);
                        }
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetPrintableTemplateByFilenamePrefixResponse getPrintableTemplateByFileNamePrefix(GetPrintableTemplateByFilenamePrefixRequest request) {
        GetPrintableTemplateByFilenamePrefixResponse response = new GetPrintableTemplateByFilenamePrefixResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (tenantProfileService.getTenantProfileByLicenseKey(request.getLicenseKey()) != null) {
                for (String prefix : request.getFileNamePrefixList()) {
                    SamplePrintTemplateVO spt = CacheManager.getInstance().getCache(PrintTemplateCache.class).getSelectedSamplePrintTemplateByFileNamePrefix(prefix);
                    if (spt != null && spt.isOpenlyAccessible()) {
                        response.getFileNamePrefixToTemplateMap().put(prefix, new PrintTemplateDTO(spt));
                    }
                }
            }
        }
        response.addErrors(context.getErrors());
        response.setSuccessful(!context.hasErrors());
        return response;
    }
}
