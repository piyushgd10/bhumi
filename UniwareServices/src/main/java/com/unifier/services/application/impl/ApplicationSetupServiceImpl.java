/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.services.application.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.application.EditSystemConfigurationRequest;
import com.unifier.core.api.application.EditSystemConfigurationResponse;
import com.unifier.core.api.application.FacilityAllocationRuleDTO;
import com.unifier.core.api.application.SystemConfigurationDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.ProductFeature;
import com.unifier.core.entity.ProductRole;
import com.unifier.core.entity.ProductSource;
import com.unifier.core.entity.SystemConfig;
import com.unifier.dao.application.IApplicationSetupDao;
import com.unifier.dao.application.IApplicationSetupMao;
import com.unifier.services.application.IApplicationSetupService;
import com.unifier.services.aspect.MarkDirty;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.State;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.vo.ProductDefaultVO;
import com.uniware.core.vo.UICustomListVO;
import com.uniware.services.configuration.BaseSystemConfiguration;
import com.uniware.services.configuration.SystemConfiguration;

@Service
@Transactional
public class ApplicationSetupServiceImpl implements IApplicationSetupService {

    @Autowired
    private IApplicationSetupDao applicationSetupDao;

    @Autowired
    private IApplicationSetupMao applicationSetupMao;

    @Override
    @MarkDirty(values = { SystemConfiguration.class, BaseSystemConfiguration.class })
    public EditSystemConfigurationResponse editSystemConfiguration(EditSystemConfigurationRequest request) {
        EditSystemConfigurationResponse response = new EditSystemConfigurationResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            SystemConfig systemConfig = applicationSetupDao.getSystemConfigByName(request.getName());
            if (systemConfig == null) {
                context.addError(WsResponseCode.INVALID_SYSTEM_CONFIG_NAME, "Invalid System Configuration Name.");
            } else {
                systemConfig.setValue(request.getValue());
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public SystemConfigurationDTO getSystemConfiguration(String name) {
        SystemConfig systemConfig = applicationSetupDao.getSystemConfigByName(name);
        SystemConfigurationDTO configurationDTO = new SystemConfigurationDTO();
        configurationDTO.setName(systemConfig.getName());
        configurationDTO.setDisplayName(systemConfig.getDisplayName());
        configurationDTO.setValue(systemConfig.getValue());
        return configurationDTO;
    }

    @Override
    public List<SystemConfigurationDTO> getSystemConfiguration() {
        List<SystemConfigurationDTO> systemConfigurations = new ArrayList<SystemConfigurationDTO>();
        for (SystemConfig systemConfig : applicationSetupDao.getSystemConfiguration()) {
            systemConfigurations.add(new SystemConfigurationDTO(systemConfig));
        }
        return systemConfigurations;
    }

    @Override
    public List<FacilityAllocationRuleDTO> getFacilityAllocationRules() {
        List<FacilityAllocationRuleDTO> facilityAllocationRules = new ArrayList<FacilityAllocationRuleDTO>();
        for (FacilityAllocationRule facilityAllocationRule : applicationSetupDao.getFacilityAllocationRules()) {
            facilityAllocationRules.add(new FacilityAllocationRuleDTO(facilityAllocationRule));
        }
        return facilityAllocationRules;
    }

    @Override
    public List<SystemConfig> getSystemConfigs() {
        return applicationSetupDao.getSystemConfigs();
    }

    @Override
    public List<ProductDefaultVO> getProductDefaults() {
        return applicationSetupMao.getProductDefaults();
    }

    @Override
    public List<ProductFeature> getProductFeatures() {
        return applicationSetupDao.getProductFeatures();

    }

    @Override
    public List<ProductRole> getProductRoles() {
        return applicationSetupDao.getProductRoles();

    }

    @Override
    public List<ProductSource> getProductSources(String productCode) {
        return applicationSetupDao.getProductSources(productCode);
    }

    @Override
    public List<SystemConfig> getTenantSystemConfigs() {
        return applicationSetupDao.getTenantSystemConfigs();
    }

    @Override
    public List<UserDatatableView> getDatatableViews() {
        return applicationSetupDao.getDatatableViews();
    }

    @Override
    public List<State> getStates() {
        return applicationSetupDao.getStates();
    }

    @Override
    public List<Country> getCountries() {
        return applicationSetupDao.getCountries();
    }

    @Override
    public List<EnvironmentProperty> getEnvironmentProperties() {
        return applicationSetupDao.getEnvironmentProperties();
    }

    @Override
    public List<PaymentMethod> getPaymentMethods() {
        return applicationSetupDao.getPaymentMethods();
    }

    @Override
    public List<UICustomListVO> getUICustomLists() {
        return applicationSetupMao.getUICustomLists();
    }

    @Override
    public List<Product> getAllProducts() {
        return applicationSetupDao.getAllProducts();
    }

    @Override
    public Product getProductByCode(String code) {
        return applicationSetupDao.getProductByCode(code);
    }

    @Override
    public List<ShippingPackageStatus> getAllShippingPackageStatus() {
        return applicationSetupDao.getAllShippingPackageStatus();
    }

    @Override
    public List<SaleOrderStatus> getAllSaleOrderStatus() {
        return applicationSetupDao.getAllSaleOrderStatus();
    }

    @Override
    public List<SaleOrderItemStatus> getAllSaleOrderItemStatus() {
        return applicationSetupDao.getAllSaleOrderItemStatus();
    }

    @Override
    public SystemConfig addSystemConfig(SystemConfig systemConfig){
        return applicationSetupDao.addSystemConfig(systemConfig);
    }

    @Override
    public SystemConfig updateSystemConfig(SystemConfig systemConfig){
        return applicationSetupDao.updateSystemConfig(systemConfig);
    }
}
