/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.services.application;

import java.util.List;

import com.unifier.core.api.application.EditSystemConfigurationRequest;
import com.unifier.core.api.application.EditSystemConfigurationResponse;
import com.unifier.core.api.application.FacilityAllocationRuleDTO;
import com.unifier.core.api.application.SystemConfigurationDTO;
import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.ProductFeature;
import com.unifier.core.entity.ProductRole;
import com.unifier.core.entity.ProductSource;
import com.unifier.core.entity.SystemConfig;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.State;
import com.uniware.core.vo.UICustomListVO;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.vo.ProductDefaultVO;

public interface IApplicationSetupService {

    EditSystemConfigurationResponse editSystemConfiguration(EditSystemConfigurationRequest request);

    SystemConfigurationDTO getSystemConfiguration(String name);

    List<SystemConfigurationDTO> getSystemConfiguration();

    List<FacilityAllocationRuleDTO> getFacilityAllocationRules();

    List<SystemConfig> getSystemConfigs();

    List<ProductSource> getProductSources(String productCode);

    List<SystemConfig> getTenantSystemConfigs();

    List<UserDatatableView> getDatatableViews();

    List<State> getStates();

    List<Country> getCountries();

    List<EnvironmentProperty> getEnvironmentProperties();

    List<PaymentMethod> getPaymentMethods();

    List<UICustomListVO> getUICustomLists();

    List<Product> getAllProducts();

    List<ProductDefaultVO> getProductDefaults();

    List<ProductFeature> getProductFeatures();

    List<ShippingPackageStatus> getAllShippingPackageStatus();

    List<SaleOrderStatus> getAllSaleOrderStatus();

    List<SaleOrderItemStatus> getAllSaleOrderItemStatus();

    Product getProductByCode(String code);

    List<ProductRole> getProductRoles();

    SystemConfig addSystemConfig(SystemConfig systemConfig);

    SystemConfig updateSystemConfig(SystemConfig systemConfig);
}
