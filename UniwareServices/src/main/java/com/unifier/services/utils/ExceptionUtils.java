/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Jun-2013
 *  @author karunsingla
 */
package com.unifier.services.utils;

import org.springframework.dao.DataIntegrityViolationException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ExceptionUtils {

    public static boolean isDuplicateKeyException(DataIntegrityViolationException e) {
        if (e.getRootCause() instanceof MySQLIntegrityConstraintViolationException) {
            MySQLIntegrityConstraintViolationException me = (MySQLIntegrityConstraintViolationException) e.getRootCause();
            if (me.getErrorCode() == 1062) {
                return true;
            }
        }
        return false;
    }
}
