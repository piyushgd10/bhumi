/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 24, 2012
 *  @author singla
 */
package com.unifier.services.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.api.customfields.CustomFieldMetadataDTO;
import com.unifier.core.api.customfields.WsCustomFieldValue;
import com.unifier.core.api.validation.ResponseCode;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.CustomFieldValue;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;

/**
 * @author singla
 */
public class CustomFieldUtils {

    private static final Logger             LOG               = LoggerFactory.getLogger(CustomFieldUtils.class);

    private static DefaultConversionService conversionService = new DefaultConversionService();
    static {
        conversionService.addConverter(new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                return new Date(Long.parseLong(source));
            }
        });

        conversionService.addConverter(new Converter<Date, String>() {
            @Override
            public String convert(Date source) {
                return Long.toString(source.getTime());
            }
        });
    }

    public static Map<String, Object> getCustomFieldValues(Object entity) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        String entityName = sanitizeClassName(entity.getClass().getName());
        List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
        Map<String, Object> customFieldValues = new HashMap<>();
        if (customFields != null) {
            for (CustomFieldMetadataVO customField : customFields) {
                try {
                    Object customFieldValue = null;
                    if (customField.isSerializable()) {
                        customFieldValue = customField.getGetter().invoke(entity);
                    } else {
                        CustomFieldValue cfv = (CustomFieldValue) configuration.getCustomFieldValueGetterByEntity(entityName).invoke(entity);
                        if (cfv != null) {
                            customFieldValue = conversionService.convert(configuration.getCustomFieldReadMethod(customField.getMappingFieldName()).invoke(cfv),
                                    customField.getJavaType());
                        }
                    }
                    customFieldValues.put(customField.getFieldName(), customFieldValue);
                } catch (Exception e) {
                    LOG.error("unable to get value for custom field, invocation of getter - failed", e);
                }
            }
        }
        return customFieldValues;
    }

    public static List<CustomFieldMetadataDTO> getCustomFieldValuesDTO(List<WsCustomFieldValue> wsCustomFieldValues, Class<?> clazz) {
        List<CustomFieldMetadataDTO> customFieldValues = new ArrayList<>();
        if (wsCustomFieldValues != null) {
            CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
            String entityName = sanitizeClassName(clazz.getName());
            Map<String, String> customFieldNameToValue = new HashMap<>(wsCustomFieldValues.size());
            for (WsCustomFieldValue wsCustomFieldValue : wsCustomFieldValues) {
                customFieldNameToValue.put(wsCustomFieldValue.getName(), wsCustomFieldValue.getValue());
            }
            List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
            if (customFields != null) {
                for (CustomFieldMetadataVO customField : customFields) {
                    if (customFieldNameToValue.containsKey(customField.getFieldName())) {
                        String rawFieldValue = customFieldNameToValue.get(customField.getFieldName());
                        Object customFieldValue = null;
                        if (!customField.getJavaType().equals(String.class)) {
                            try {
                                customFieldValue = StringUtils.isNotBlank(rawFieldValue) ? conversionService.convert(rawFieldValue, customField.getJavaType()) : null;
                            } catch (Exception e) {
                                LOG.error("Unable to convert custom field value: {}, javaType: {}, error: {}", new Object[]{rawFieldValue, customField.getJavaType(), e.getMessage()});
                            }
                        } else {
                            customFieldValue = rawFieldValue;
                        }
                        customFieldValues.add(new CustomFieldMetadataDTO(customField.getFieldName(), customFieldValue, customField.getValueType(), customField.getDisplayName(),
                                customField.isRequired(), customField.getPossibleValues()));
                    }
                }
            }
        }
        return customFieldValues;
    }

    public static List<CustomFieldMetadataDTO> getCustomFieldValuesDTO(Object entity) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        String entityName = sanitizeClassName(entity.getClass().getName());
        List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
        List<CustomFieldMetadataDTO> customFieldValues = new ArrayList<>();
        if (customFields != null) {
            for (CustomFieldMetadataVO customField : customFields) {
                Object customFieldValue = null;
                try {
                    if (customField.isSerializable()) {
                        customFieldValue = customField.getGetter().invoke(entity);
                    } else {
                        CustomFieldValue cfv = (CustomFieldValue) configuration.getCustomFieldValueGetterByEntity(entityName).invoke(entity);
                        if (cfv != null) {
                            String rawFieldValue = (String) configuration.getCustomFieldReadMethod(customField.getMappingFieldName()).invoke(cfv);
                            if (!customField.getJavaType().equals(String.class)) {
                                customFieldValue = StringUtils.isNotBlank(rawFieldValue) ? conversionService.convert(rawFieldValue, customField.getJavaType()) : null;
                            } else {
                                customFieldValue = rawFieldValue;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOG.error("unable to get value for custom field, invocation of getter -failed", e);
                }
                customFieldValues.add(new CustomFieldMetadataDTO(customField.getFieldName(), customFieldValue, customField.getValueType(), customField.getDisplayName(),
                        customField.isRequired(), customField.getPossibleValues()));
            }
        }
        return customFieldValues;
    }

    public static <T> T setCustomFieldValues(T entity, Map<String, Object> customFieldValues, boolean updateBlankValues) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        String entityName = sanitizeClassName(entity.getClass().getName());
        List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
        if (customFields != null) {
            for (CustomFieldMetadataVO customField : customFields) {
                if (customFieldValues.containsKey(customField.getFieldName())) {
                    Object customFieldValue = customFieldValues.get(customField.getFieldName());
                    if (customFieldValue != null || updateBlankValues) {
                        try {
                            if (customField.isSerializable()) {
                                customField.getSetter().invoke(entity, customFieldValue);
                            } else {
                                customFieldValue = conversionService.convert(customFieldValue, String.class);
                                CustomFieldValue cfv = (CustomFieldValue) configuration.getCustomFieldValueGetterByEntity(entityName).invoke(entity);
                                if (cfv == null) {
                                    cfv = new CustomFieldValue((Integer) configuration.getIdGetterByEntity(entityName).invoke(entity), entityName, DateUtils.getCurrentTime());
                                }
                                configuration.getCustomFieldWriteMethod(customField.getMappingFieldName()).invoke(cfv, customFieldValue);
                                configuration.getCustomFieldValueSetterByEntity(entityName).invoke(entity, cfv);
                            }
                        } catch (Exception e) {
                            LOG.error("unable to get value for custom field, invocation of setter failed", e);
                        }
                    }
                }
            }
        }
        return entity;
    }

    public static <T> T setCustomFieldValues(T entity, Map<String, Object> customFieldValues) {
        return setCustomFieldValues(entity, customFieldValues, true);
    }

    public static <T> T setCustomFieldValue(T entity, String fieldName, Object customFieldValue) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        String entityName = sanitizeClassName(entity.getClass().getName());
        CustomFieldMetadataVO customField = configuration.getCustomFieldByEntityByFieldName(entityName, fieldName);
        try {
            if (customField.isSerializable()) {
                customField.getSetter().invoke(entity, customFieldValue);
            } else {
                customFieldValue = conversionService.convert(customFieldValue, String.class);
                CustomFieldValue cfv = (CustomFieldValue) configuration.getCustomFieldValueGetterByEntity(entityName).invoke(entity);
                if (cfv == null) {
                    cfv = new CustomFieldValue((Integer) configuration.getIdGetterByEntity(entityName).invoke(entity), entityName, DateUtils.getCurrentTime());
                }
                configuration.getCustomFieldWriteMethod(customField.getMappingFieldName()).invoke(cfv, customFieldValue);
                configuration.getCustomFieldValueSetterByEntity(entityName).invoke(entity, cfv);
            }
        } catch (Exception e) {
            LOG.error("unable to get value for custom field, invocation of setter - failed", e);
        }
        return entity;
    }

    public static <T> Object getCustomFieldValue(T entity, String fieldName) {
        String entityName = sanitizeClassName(entity.getClass().getName());
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        CustomFieldMetadataVO customField = configuration.getCustomFieldByEntityByFieldName(entityName, fieldName);
        Object value = null;
        if (customField != null) {
            try {
                if (customField.isSerializable()) {
                    value = customField.getGetter().invoke(entity);
                } else {
                    CustomFieldValue cfv = (CustomFieldValue) configuration.getCustomFieldValueGetterByEntity(entityName).invoke(entity);
                    if (cfv != null) {
                        value = conversionService.convert(configuration.getCustomFieldReadMethod(customField.getMappingFieldName()).invoke(cfv), customField.getJavaType());
                    }
                    if (value == null && customField.getDefaultValue() != null) {
                        value = conversionService.convert(customField.getDefaultValue(), customField.getJavaType());
                    }
                }
            } catch (Exception e) {
                LOG.error("unable to get value for custom field, invocation of getter - failed", e);
            }
        } else {
            LOG.error("unable to find custom field {} on class {}", fieldName, entityName);
        }
        return value;
    }

    /**
     * @param response response to which errors should be added
     * @param entityName entity name for which custom fields is to be fetched
     * @param row row object of Fileparser
     */
    public static List<WsCustomFieldValue> getCustomFieldValues(ServiceResponse response, String entityName, Row row) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
        List<WsCustomFieldValue> customFieldValues = new ArrayList<>();
        if (customFields != null) {
            for (CustomFieldMetadataVO customField : customFields) {
                String fieldValue = row.getColumnValue(customField.getDisplayName());
                if (fieldValue != null) {
                    customFieldValues.add(new WsCustomFieldValue(customField.getFieldName(), fieldValue));
                }
            }
        }
        return customFieldValues;
    }

    public static Map<String, Object> getCustomFieldValues(ValidationContext context, String entityName, List<WsCustomFieldValue> customFieldRawValues) {
        return getCustomFieldValues(context, entityName, customFieldRawValues, true);
    }

    public static Map<String, Object> getCustomFieldValues(ValidationContext context, String entityName, List<WsCustomFieldValue> customFieldRawValues, boolean updateOnly) {
        CustomFieldsMetadataConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        List<CustomFieldMetadataVO> customFields = configuration.getCustomFieldsByEntity(entityName);
        if (customFields != null) {
            Map<String, Object> customFieldValues = new HashMap<>();
            Map<String, String> customFieldRawValuesMap = getCustomFieldValuesMap(customFieldRawValues);
            for (CustomFieldMetadataVO customField : customFields) {
                try {
                    String rawFieldValue = null;
                    boolean setCustomField = false;
                    if (customFieldRawValuesMap.containsKey(customField.getFieldName())) {
                        rawFieldValue = customFieldRawValuesMap.get(customField.getFieldName());
                        if (StringUtils.isNotBlank(customField.getDefaultValue()) && StringUtils.isBlank(rawFieldValue)) {
                            rawFieldValue = customField.getDefaultValue();
                        }
                        if (customField.isRequired() && StringUtils.isBlank(rawFieldValue)) {
                            context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, customField.getDisplayName() + " may not be empty");
                        } else {
                            setCustomField = true;
                        }
                    } else {
                        if (!updateOnly) {
                            if (StringUtils.isNotBlank(customField.getDefaultValue())) {
                                rawFieldValue = customField.getDefaultValue();
                            }
                            if (customField.isRequired() && StringUtils.isBlank(rawFieldValue)) {
                                context.addError(ResponseCode.MISSING_REQUIRED_PARAMETERS, customField.getDisplayName() + " may not be empty");
                            } else {
                                setCustomField = true;
                            }
                        }
                    }
                    if (setCustomField) {
                        if (StringUtils.isNotBlank(rawFieldValue)) {
                            Object fieldValue = conversionService.convert(rawFieldValue, customField.getJavaType());
                            customFieldValues.put(customField.getFieldName(), fieldValue);
                        } else {
                            if (customField.getJavaType().equals(String.class)) {
                                customFieldValues.put(customField.getFieldName(), rawFieldValue);
                            } else {
                                customFieldValues.put(customField.getFieldName(), null);
                            }
                        }
                    }
                } catch (Exception e) {
                    context.addError(ResponseCode.INVALID_FORMAT, "invalid format for custom field: " + customField.getDisplayName());
                }
            }
            return customFieldValues;
        } else {
            return Collections.emptyMap();
        }
    }

    private static Map<String, String> getCustomFieldValuesMap(List<WsCustomFieldValue> customFieldRawValues) {
        if (customFieldRawValues != null) {
            Map<String, String> customFieldValuesMap = new HashMap<>(customFieldRawValues.size());
            for (WsCustomFieldValue wsCustomFieldValue : customFieldRawValues) {
                customFieldValuesMap.put(wsCustomFieldValue.getName(), wsCustomFieldValue.getValue());
            }
            return customFieldValuesMap;
        } else {
            return Collections.emptyMap();
        }
    }

    private static String sanitizeClassName(String className) {
        if (className.contains("javassist")) {
            className = className.substring(0, className.indexOf("_"));
        }
        return className;
    }

}
