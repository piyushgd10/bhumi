/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.services.search;

import java.util.List;

import com.unifier.core.entity.SearchTypes;
import com.uniware.core.api.search.GetAllSearchTypesResponse;
import com.uniware.core.api.search.GlobalSearchRequest;
import com.uniware.core.api.search.GlobalSearchResponse;

public interface IGlobalSearchService {

    GlobalSearchResponse globalSearch(GlobalSearchRequest request);

    GetAllSearchTypesResponse getAllSearchTypes(String productCode);

    String searchHelpResourceTopics(String term);
    
    List<SearchTypes> getAllSearchTypes();

    List<SearchTypes> getAllSearchTypesByProduct(String productCode);

}