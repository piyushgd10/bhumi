/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.services.search.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.SearchTypes;
import com.unifier.core.expressions.Expression;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.searchtype.ISearchTypeDao;
import com.unifier.services.search.IGlobalSearchService;
import com.uniware.core.api.catalog.dto.ItemTypeDTO;
import com.uniware.core.api.party.SearchVendorRequest;
import com.uniware.core.api.party.SearchVendorResponse.VendorSearchDTO;
import com.uniware.core.api.search.GetAllSearchTypesResponse;
import com.uniware.core.api.search.GlobalSearchRequest;
import com.uniware.core.api.search.GlobalSearchResponse;
import com.uniware.core.api.search.SearchDTO;
import com.uniware.core.api.search.SearchDTO.Entry;
import com.uniware.core.api.search.SearchType;
import com.uniware.core.api.search.SearchTypeDTO;
import com.uniware.core.api.shipping.SearchManifestRequest;
import com.uniware.core.api.shipping.SearchManifestResponse.ShippingManifestDTO;
import com.uniware.core.api.warehouse.SearchItemTypesRequest;
import com.uniware.core.entity.Invoice;
import com.uniware.core.entity.PurchaseOrder;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.utils.EnvironmentProperties;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.services.cache.SearchTypeCache;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.purchase.IPurchaseService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.vendor.IVendorService;

@Service("globalSearchService")
public class GlobalSearchServiceImpl implements IGlobalSearchService {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalSearchServiceImpl.class);

    @Autowired
    private ISearchTypeDao searchTypeDao;

    @Autowired
    private ISaleOrderService saleOrderService;

    @Autowired
    private IShippingService shippingService;

    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private IVendorService vendorService;

    @Autowired
    private ICatalogService catalogService;

    @Autowired
    private IInvoiceService invoiceService;

    @Autowired
    private IDispatchService dispatchService;

    @Override
    @Transactional(readOnly = true)
    public GetAllSearchTypesResponse getAllSearchTypes(String productCode) {
        GetAllSearchTypesResponse response = new GetAllSearchTypesResponse();
        List<SearchTypeDTO> searchTypeDTOs = new ArrayList<SearchTypeDTO>();
        boolean isAllFacility = UserContext.current().getFacility() == null;
        for (SearchTypeDTO searchType : CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOsByProduct(productCode)) {
            if (isAllFacility) {
                if (searchType.isAllFacilityType()) {
                    searchTypeDTOs.add(searchType);
                }
            } else {
                if (!searchType.isHidden()) {
                    searchTypeDTOs.add(searchType);
                }
            }
        }
        response.setSuccessful(true);
        response.setSearchTypes(searchTypeDTOs);
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GlobalSearchResponse globalSearch(GlobalSearchRequest request) {
        GlobalSearchResponse response = new GlobalSearchResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            String keyword = request.getKeyword().trim();
            List<SearchDTO> searchDTOs = new ArrayList<SearchDTO>();
            if (StringUtils.isNotBlank(request.getType())) {
                switch (request.getType()) {
                    case "SALE_ORDER":
                        searchDTOs.add(searchSaleOrders(keyword));
                        break;
                    case "SHIPPING_PACKAGE":
                        searchDTOs.add(searchShippingPackages(keyword));
                        break;
                    case "PURCHASE_ORDER":
                        searchDTOs.add(searchPurchaseOrders(keyword));
                        break;
                    case "INVOICE":
                        searchDTOs.add(searchInvoices(keyword));
                        break;
                    case "ITEM_TYPE":
                        searchDTOs.add(searchItemTypes(keyword));
                        break;
                    case "VENDOR":
                        searchDTOs.add(searchVendors(keyword));
                        break;
                    case "MANIFEST":
                        searchDTOs.add(searchShippingManifest(keyword));
                        break;
                }
            } else {
                searchDTOs.add(searchSaleOrders(keyword));
                if (UserContext.current().getFacilityId() != null) {
                    searchDTOs.add(searchShippingPackages(keyword));
                    searchDTOs.add(searchPurchaseOrders(keyword));
                }
                searchDTOs.add(searchItemTypes(keyword));
                searchDTOs.add(searchVendors(keyword));
            }
            response.setSearchDTOs(searchDTOs);
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private SearchDTO searchShippingManifest(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchTypes.Type.MANIFEST.name());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.MANIFEST.name());
        SearchManifestRequest request = new SearchManifestRequest();
        request.setManifestCode(keyword);
        List<ShippingManifestDTO> shippigManifestsDtos = dispatchService.searchManifest(request).getElements();
        for (ShippingManifestDTO shippingManifest : shippigManifestsDtos) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("shippingManifest", shippingManifest);
            searchDTO.getSearchResults().add(new Entry(shippingManifest.getManifestCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchVendors(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.VENDOR.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.VENDOR.name());
        List<VendorSearchDTO> vendors = vendorService.searchVendor(new SearchVendorRequest(keyword)).getVendors();
        for (VendorSearchDTO vendor : vendors) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("vendor", vendor);
            searchDTO.getSearchResults().add(new Entry(vendor.getCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchItemTypes(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.ITEM_TYPE.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.ITEM_TYPE.name());
        SearchItemTypesRequest request = new SearchItemTypesRequest();
        request.setProductCode(keyword);
        List<ItemTypeDTO> itemTypes = catalogService.searchItemTypes(request).getElements();
        for (ItemTypeDTO itemType : itemTypes) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("itemType", itemType);
            searchDTO.getSearchResults().add(new Entry(itemType.getSkuCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchInvoices(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.INVOICE.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.INVOICE.name());
        List<Invoice> invoices = invoiceService.searchInvoice(keyword);
        for (Invoice invoice : invoices) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("invoice", invoice);
            params.put("shippingPackage", shippingService.getShippingPackageByInvoiceId(invoice.getId()));
            searchDTO.getSearchResults().add(new Entry(invoice.getCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchSaleOrders(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.SALE_ORDER.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.SALE_ORDER.name());
        List<SaleOrder> saleOrdersByDisplayOrderCode = saleOrderService.searchSaleOrderByDisplayOrderCode(keyword);
        List<SaleOrder> saleOrdersByOrderCode = saleOrderService.searchSaleOrderByOrderCode(keyword);
        List<SaleOrderVO> failedOrderVOs = saleOrderService.searchFailedOrPartialOrder(keyword);
        HashSet<SaleOrder> saleOrderSet = new HashSet<SaleOrder>();
        for (SaleOrder saleOrder : saleOrdersByDisplayOrderCode) {
            saleOrderSet.add(saleOrder);
        }
        for (SaleOrder saleOrder : saleOrdersByOrderCode) {
            saleOrderSet.add(saleOrder);
        }
        for (SaleOrder saleOrder : saleOrderSet) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("saleOrder", saleOrder);
            searchDTO.getSearchResults().add(new Entry(saleOrder.getCode(), expression.evaluate(params, String.class)));
        }
        searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.FAILED_SALE_ORDER.name());
        for (SaleOrderVO saleOrderVO : failedOrderVOs) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("saleOrder", saleOrderVO);
            searchDTO.getSearchResults().add(new Entry(saleOrderVO.getCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchShippingPackages(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.SHIPPING_PACKAGE.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.SHIPPING_PACKAGE.name());
        List<ShippingPackage> shippingPackages = shippingService.getShippingPackagesByTrackingNumber(keyword);
        ShippingPackage shippingPackage = shippingService.getShippingPackageByCode(keyword);
        if (shippingPackage != null) {
            shippingPackages.add(shippingPackage);
        }
        for (ShippingPackage sp : shippingPackages) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("shippingPackage", sp);
            searchDTO.getSearchResults().add(new Entry(sp.getCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    private SearchDTO searchPurchaseOrders(String keyword) {
        SearchDTO searchDTO = new SearchDTO(SearchType.PURCHASE_ORDER.getName());
        SearchTypeDTO searchType = CacheManager.getInstance().getCache(SearchTypeCache.class).getSearchTypeDTOByType(SearchTypes.Type.PURCHASE_ORDER.name());
        List<PurchaseOrder> purchaseOrders = purchaseService.searchPurchaseOrder(keyword);
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            Expression expression = Expression.compile(searchType.getLandingPageUrl());
            Map<String, Object> params = new HashMap<>();
            params.put("purchaseOrder", purchaseOrder);
            searchDTO.getSearchResults().add(new Entry(purchaseOrder.getCode(), expression.evaluate(params, String.class)));
        }
        return searchDTO;
    }

    @Override
    public String searchHelpResourceTopics(String term) {
        String response = "";
        try {
            HttpSender request = new HttpSender();
            Map<String, String> requestParams = new HashMap<String, String>();
            requestParams.put("term", term);
            request.setTimeout(10, TimeUnit.SECONDS);
            response = request.executeGet(EnvironmentProperties.getDeskHelpSearchPath(), requestParams);
        } catch (Exception e) {
            LOG.info("Error on desk search" + e.getMessage());
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SearchTypes> getAllSearchTypes() {
        return searchTypeDao.getAllSearchTypes();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SearchTypes> getAllSearchTypesByProduct(String productCode) {
        return searchTypeDao.getAllSearchTypesByProduct(productCode);
    }
}
