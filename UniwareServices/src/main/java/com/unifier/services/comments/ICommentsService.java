/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.services.comments;

import com.unifier.core.api.comments.AddCommentRequest;
import com.unifier.core.api.comments.AddCommentResponse;
import com.unifier.core.api.comments.DeleteCommentRequest;
import com.unifier.core.api.comments.DeleteCommentResponse;
import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;

public interface ICommentsService {

    AddCommentResponse addComment(AddCommentRequest request);

    DeleteCommentResponse deleteComment(DeleteCommentRequest request);

    GetCommentsResponse getComments(GetCommentsRequest request);

    int getCommentsCount(String referenceIdentifier);

}
