/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.services.comments.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.comments.AddCommentRequest;
import com.unifier.core.api.comments.AddCommentResponse;
import com.unifier.core.api.comments.DeleteCommentRequest;
import com.unifier.core.api.comments.DeleteCommentResponse;
import com.unifier.core.api.comments.GetCommentsRequest;
import com.unifier.core.api.comments.GetCommentsResponse;
import com.unifier.core.api.comments.UserCommentDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.entity.UserComment;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.comments.ICommentsDao;
import com.unifier.services.comments.ICommentsService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.validation.WsResponseCode;

@Service("commentsService")
public class CommentsServiceImpl implements ICommentsService {

    public enum Identifier {
        SaleOrder("SO-"),
        PurchaseOrder("PO-"),
        Item("ITEM-");
        private final String prefix;

        private Identifier(String prefix) {
            this.prefix = prefix;
        }

        public String prefix() {
            return this.prefix;
        }
    }

    @Autowired
    private ICommentsDao  commentsDao;

    @Autowired
    private IUsersService usersService;

    @Override
    @Transactional
    public AddCommentResponse addComment(AddCommentRequest request) {
        AddCommentResponse response = new AddCommentResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserComment userComment = new UserComment();
            userComment.setComment(request.getComment());
            userComment.setReferenceIdentifier(request.getReferenceIdentifier());
            userComment.setUser(usersService.getUserWithDetailsById(request.getUserId()));
            userComment.setCreated(DateUtils.getCurrentTime());
            userComment.setSystemGenerated(request.isSystemGenerated());
            userComment = commentsDao.addUserComment(userComment);
            response.setSuccessful(true);

            UserCommentDTO userCommentDTO = prepareUserCommentDTO(userComment);
            response.setUserCommentDTO(userCommentDTO);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DeleteCommentResponse deleteComment(DeleteCommentRequest request) {
        DeleteCommentResponse response = new DeleteCommentResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserComment userComment = commentsDao.getUserCommentById(request.getCommentId());
            if (!userComment.getUser().getId().equals(request.getUserId())) {
                context.addError(WsResponseCode.ONLY_OWNER_CAN_DELETE_COMMENT, "You can delete your own comments");
            } else if (userComment.isSystemGenerated()) {
                context.addError(WsResponseCode.CANNOT_DELETE_SYSTEM_GENERATED_COMMENT, "You cannot delete system generated comments");
            } else {
                response.setSuccessful(true);
                userComment.setDeleted(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public GetCommentsResponse getComments(GetCommentsRequest request) {
        GetCommentsResponse response = new GetCommentsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            List<UserComment> userComments = commentsDao.getUserComments(request.getReferenceIdentifier());
            for (UserComment userComment : userComments) {
                UserCommentDTO commentDTO = prepareUserCommentDTO(userComment);
                response.getUserCommentDTOs().add(commentDTO);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private UserCommentDTO prepareUserCommentDTO(UserComment userComment) {
        UserCommentDTO commentDTO = new UserCommentDTO();
        commentDTO.setCommentId(userComment.getId());
        commentDTO.setComment(userComment.getComment());
        commentDTO.setCreated(userComment.getCreated());
        commentDTO.setUsername(userComment.getUser().getUsername());
        commentDTO.setSystemGenerated(userComment.isSystemGenerated());
        return commentDTO;
    }

    public int getCommentsCount(String referenceIdentifier){
        return commentsDao.getUserCommentsCount(referenceIdentifier);
    }
}
