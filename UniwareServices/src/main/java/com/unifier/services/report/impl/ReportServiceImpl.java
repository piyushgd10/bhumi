/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.services.report.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uniware.core.cache.EnvironmentPropertiesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.api.reports.AddWidgetRequest;
import com.unifier.core.api.reports.AddWidgetResponse;
import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.api.reports.GetWidgetDataResponse;
import com.unifier.core.api.reports.RemoveWidgetRequest;
import com.unifier.core.api.reports.RemoveWidgetResponse;
import com.unifier.core.api.reports.ReorderWidgetsRequest;
import com.unifier.core.api.reports.ReorderWidgetsRequest.WsWidget;
import com.unifier.core.api.reports.ReorderWidgetsResponse;
import com.unifier.core.api.reports.WidgetDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.entity.UserWidget;
import com.unifier.core.queryParser.GenerateNoSqlQueryFromJsonRequest;
import com.unifier.core.queryParser.IQueryParser;
import com.unifier.core.queryParser.nosql.mongo.GenerateMongoQueryResponse;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.core.utils.DateUtils.TextRange;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.dao.report.IReportDao;
import com.unifier.mao.report.IReportMao;
import com.unifier.scraper.core.QueryType;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.runtime.IQueryProvider;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.report.IReportService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.DashboardWidgetConfiguration;

@Service("reportService")
public class ReportServiceImpl implements IReportService {

    private static final Logger LOG = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Autowired
    private IReportDao          reportDao;

    @Autowired
    private IUsersService       usersService;

    @Autowired
    private IReportMao          reportMao;

    @Autowired
    @Qualifier(value = "mongoQueryParser")
    private IQueryParser        mongoQueryParser;

    @Override
    @Transactional
    public ReorderWidgetsResponse reorderWidgets(ReorderWidgetsRequest request) {
        ReorderWidgetsResponse response = new ReorderWidgetsResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            int index = 0;
            for (WsWidget widget : request.getWidgets()) {
                UserWidget userWidget = reportDao.getUserWidget(request.getUserId(), widget.getCode());
                if (userWidget == null) {
                    context.addError(WsResponseCode.INVALID_USER_WIDGET, "Invalid User Widget");
                } else {
                    userWidget.setSequence(index);
                }
                index++;
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddWidgetResponse addWidget(AddWidgetRequest request) {
        AddWidgetResponse response = new AddWidgetResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            UserWidget userWidget = reportDao.getUserWidget(request.getUserId(), request.getCode());
            if (userWidget == null) {
                userWidget = new UserWidget();

                DashboardWidget dashboardWidget = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class).getDashboardWidgetByCode(
                        request.getCode());
                userWidget.setDashboardWidget(dashboardWidget);
                userWidget.setSubscribed(true);
                userWidget.setSequence(0);
                userWidget.setUser(usersService.getUserWithDetailsById(request.getUserId()));
                userWidget.setCreated(DateUtils.getCurrentTime());
                userWidget.setUpdated(DateUtils.getCurrentTime());
                reportDao.addUserWidget(userWidget);
            } else {
                userWidget.setSubscribed(true);
            }
            response.setSuccessful(true);
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public RemoveWidgetResponse removeWidget(RemoveWidgetRequest request) {
        RemoveWidgetResponse response = new RemoveWidgetResponse();
        ValidationContext context = request.validate();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            UserWidget userWidget = reportDao.getUserWidget(request.getUserId(), request.getCode());
            if (userWidget == null) {
                context.addError(WsResponseCode.INVALID_USER_WIDGET, "Invalid User Widget");
            } else {
                userWidget.setSubscribed(false);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    public List<WidgetDTO> getUserWidgets(Integer userId) {
        List<UserWidget> userWidgets = null;
        if(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled()){
            userWidgets = getUserWidgetsFromSecondaryDB(userId);
        } else {
            userWidgets = getUserWidgetsFromPrimaryDB(userId);
        }
        List<WidgetDTO> widgets = new ArrayList<WidgetDTO>();
        for (UserWidget userWidget : userWidgets) {
            DashboardWidget dashboardWidget = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class).getDashboardWidgetByCode(
                    userWidget.getDashboardWidget().getCode());
            if (dashboardWidget != null) {
                WidgetDTO widgetDTO = new WidgetDTO();
                widgetDTO.setCode(dashboardWidget.getCode());
                widgetDTO.setName(dashboardWidget.getName());
                widgetDTO.setWidgetGroup(dashboardWidget.getWidgetGroup());
                widgetDTO.setSequence(userWidget.getSequence());
                widgets.add(widgetDTO);
            }
        }
        Collections.sort(widgets, new Comparator<WidgetDTO>() {
            @Override
            public int compare(WidgetDTO o1, WidgetDTO o2) {
                return o1.getSequence().compareTo(o2.getSequence());
            }
        });
        return widgets;
    }

    @Transactional(readOnly = true)
    private List<UserWidget> getUserWidgetsFromPrimaryDB(Integer userId){
        return reportDao.getUserWidgets(userId);
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private List<UserWidget> getUserWidgetsFromSecondaryDB(Integer userId){
        return reportDao.getUserWidgets(userId);
    }

    @Override
    public GetWidgetDataResponse getWidgetData(GetWidgetDataRequest request) {
        long starTime = System.currentTimeMillis();
        GetWidgetDataResponse response = new GetWidgetDataResponse();
        DashboardWidgetConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(DashboardWidgetConfiguration.class);
        final DashboardWidget dashboardWidget = configuration.getDashboardWidgetByCode(request.getWidgetCode());
        ScraperScript widgetScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName("WIDGET_" + request.getWidgetCode(), true);
        ScriptExecutionContext executionContext = ScriptExecutionContext.current();
        try {
            String responseXml = null;
            if(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled()){
                responseXml = executeWidgetScriptOnSecondaryDB(request, dashboardWidget.isHql(), widgetScript, executionContext);
            } else {
                responseXml = executeWidgetScriptOnPrimaryDB(request, dashboardWidget.isHql(), widgetScript, executionContext);
            }
            String json = XMLParser.toJson(responseXml).toString();
            response.setData(json);
            response.setSuccessful(true);
        } catch (ScriptExecutionException e) {
            // e.printStackTrace();
            LOG.error("Error executing dashboard widget script " + widgetScript.getScriptName(), e);
        } finally {
            ScriptExecutionContext.destroy();
        }
        LOG.info("Time taken to fetch data for widget [{}] is [{}]", request.getWidgetCode(), (System.currentTimeMillis() - starTime));
        return response;
    }

    @Transactional(readOnly = true)
    private String executeWidgetScriptOnPrimaryDB(GetWidgetDataRequest request, boolean isHql, ScraperScript widgetScript, ScriptExecutionContext executionContext){
        return executeWidgetScriptInternal(request, isHql, widgetScript, executionContext);
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private String executeWidgetScriptOnSecondaryDB(GetWidgetDataRequest request, boolean isHql, ScraperScript widgetScript, ScriptExecutionContext executionContext){
        return executeWidgetScriptInternal(request, isHql, widgetScript, executionContext);
    }

    private String executeWidgetScriptInternal(GetWidgetDataRequest request, boolean isHql, ScraperScript widgetScript, ScriptExecutionContext executionContext){
        for (Entry<String, Object> entry : request.getRequestMap().entrySet()) {
            executionContext.addVariable(entry.getKey(), entry.getValue());
        }
        DateRange dateRange = null;
        if (StringUtils.isNotBlank(request.getTimeRange())) {
            dateRange = new DateRange(TextRange.valueOf(request.getTimeRange()));
        } else {
            if (StringUtils.isNotBlank(request.getStartDate()) && StringUtils.isNotBlank(request.getEndDate())) {
                dateRange = new DateRange(new Date(Long.parseLong(request.getStartDate())), new Date(Long.parseLong(request.getEndDate())));
            }
        }
        if (dateRange != null) {
            executionContext.addVariable("daterange", dateRange);
        }
        executionContext.getQueryProviderMap().put(QueryType.MONGO, new IQueryProvider() {
            @SuppressWarnings("unchecked")
            @Override
            public List<Map<String, Object>> executeQuery(String query, Map<String, Object> queryParams) {
                GenerateNoSqlQueryFromJsonRequest request = new GenerateNoSqlQueryFromJsonRequest();
                request.setQueryString(query);
                request.getQueryParams().put("tenantCode", UserContext.current().getTenant().getCode());
                if (UserContext.current().getFacilityId() != null) {
                    request.getQueryParams().put("facilityCode",
                            CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(UserContext.current().getFacilityId()).getCode());
                }
                request.getQueryParams().putAll(queryParams);
                GenerateMongoQueryResponse mongoQuery = (GenerateMongoQueryResponse) mongoQueryParser.generateQuery(request);
                return reportMao.executeAnonymousQuery(mongoQuery.getQuery(), mongoQuery.getPagination(), mongoQuery.getClazz());
            }
        });

        executionContext.getQueryProviderMap().put(QueryType.SQL, new IQueryProvider() {
            @Override
            public List<Map<String, Object>> executeQuery(String query, Map<String, Object> queryParams) {
                queryParams.put("facilities", CacheManager.getInstance().getCache(FacilityCache.class).getFacilityIds());
                queryParams.put("facilityId", UserContext.current().getFacilityId());
                queryParams.put("tenantId", UserContext.current().getTenantId());
                return reportDao.executeAnonymousQuery(query, queryParams, isHql);
            }
        });
        executionContext.setTraceLoggingEnabled(UserContext.current().isTraceLoggingEnabled());
        widgetScript.execute();
        return executionContext.getScriptOutput();
    }

    @Override
    @Transactional(readOnly = true)
    public List<DashboardWidget> getPreloadedWidgets() {
        return reportDao.getPreloadedWidgets();
    }

    @Override
    public List<DashboardWidget> getDashboardWidgets() {
        return CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled() ? getDashboardWidgetsFromSecondaryDB() : getDashboardWidgetsFromPrimaryDB();
    }

    @Transactional(readOnly = true)
    private List<DashboardWidget> getDashboardWidgetsFromPrimaryDB(){
        return reportDao.getDashboardWidgets();
    }

    @Transactional(readOnly = true, value = "txManagerReplication")
    private List<DashboardWidget> getDashboardWidgetsFromSecondaryDB(){
        return reportDao.getDashboardWidgets();
    }

}
