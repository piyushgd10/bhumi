/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.services.report;

import java.util.List;

import com.unifier.core.api.reports.AddWidgetRequest;
import com.unifier.core.api.reports.AddWidgetResponse;
import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.api.reports.GetWidgetDataResponse;
import com.unifier.core.api.reports.RemoveWidgetRequest;
import com.unifier.core.api.reports.RemoveWidgetResponse;
import com.unifier.core.api.reports.ReorderWidgetsRequest;
import com.unifier.core.api.reports.ReorderWidgetsResponse;
import com.unifier.core.api.reports.WidgetDTO;
import com.unifier.core.entity.DashboardWidget;

public interface IReportService {

    ReorderWidgetsResponse reorderWidgets(ReorderWidgetsRequest request);

    AddWidgetResponse addWidget(AddWidgetRequest request);

    RemoveWidgetResponse removeWidget(RemoveWidgetRequest request);

    List<WidgetDTO> getUserWidgets(Integer userId);

    List<DashboardWidget> getPreloadedWidgets();

    GetWidgetDataResponse getWidgetData(GetWidgetDataRequest request);

    List<DashboardWidget> getDashboardWidgets();

}
