/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Nov 27, 2011
 *  @author singla
 */
package com.unifier.services.users;

import com.unifier.core.api.access.GrantApiAccessRequest;
import com.unifier.core.api.access.GrantApiAccessResponse;
import com.unifier.core.api.access.GrantApiAccessToVendorRequest;
import com.unifier.core.api.access.GrantApiAccessToVendorResponse;
import com.unifier.core.api.myaccount.AddUrlVisitedByUserRequest;
import com.unifier.core.api.myaccount.AddUrlVisitedByUserResponse;
import com.unifier.core.api.myaccount.EditDigestDetailRequest;
import com.unifier.core.api.myaccount.EditDigestDetailResponse;
import com.unifier.core.api.myaccount.EditUserDetailRequest;
import com.unifier.core.api.myaccount.EditUserDetailResponse;
import com.unifier.core.api.myaccount.GetUserDigestRequest;
import com.unifier.core.api.myaccount.GetUserDigestResponse;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadRequest;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadResponse;
import com.unifier.core.api.myaccount.RegisterDeviceRequest;
import com.unifier.core.api.myaccount.RegisterDeviceResponse;
import com.unifier.core.api.token.GetLoginTokenRequest;
import com.unifier.core.api.token.GetLoginTokenResponse;
import com.unifier.core.api.user.AddOrEditUserDetailRequest;
import com.unifier.core.api.user.AddOrEditUserDetailResponse;
import com.unifier.core.api.user.AddUserDatatableViewsRequest;
import com.unifier.core.api.user.AddUserDatatableViewsResponse;
import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.AddUserRolesResponse;
import com.unifier.core.api.user.AdvanceSearchUsersRequest;
import com.unifier.core.api.user.AdvanceSearchUsersResponse;
import com.unifier.core.api.user.CreateApiUserRequest;
import com.unifier.core.api.user.CreateApiUserResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.user.DeleteApiUserRequest;
import com.unifier.core.api.user.DeleteApiUserResponse;
import com.unifier.core.api.user.EditApiUserRequest;
import com.unifier.core.api.user.EditApiUserResponse;
import com.unifier.core.api.user.GetAccessResourceRequest;
import com.unifier.core.api.user.GetAccessResourceResponse;
import com.unifier.core.api.user.GetAllUsersRequest;
import com.unifier.core.api.user.GetAllUsersResponse;
import com.unifier.core.api.user.GetApiUserRequest;
import com.unifier.core.api.user.GetApiUserResponse;
import com.unifier.core.api.user.GetUserDetailsRequest;
import com.unifier.core.api.user.GetUserDetailsResponse;
import com.unifier.core.api.user.RemoveUserRoleRequest;
import com.unifier.core.api.user.RemoveUserRoleResponse;
import com.unifier.core.api.user.SwitchFacilityRequest;
import com.unifier.core.api.user.SwitchFacilityResponse;
import com.unifier.core.api.user.UpdateUserLastReadTimeRequest;
import com.unifier.core.api.user.UpdateUserLastReadTimeResponse;
import com.unifier.core.api.user.UserDTO;
import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.uniware.core.api.validation.SendOTPRequest;
import com.uniware.core.api.validation.SendOTPResponse;
import com.uniware.core.api.validation.VerifyOTPRequest;
import com.uniware.core.api.validation.VerifyOTPResponse;
import com.uniware.core.vo.UserProfileVO;

import java.util.List;

/**
 * @author singla
 */
public interface IUsersService {

    /**
     * @param username
     * @return
     */
    User getUserByUsername(String username);

    GetUserDigestResponse getUserDigestFlags(GetUserDigestRequest request);

    User getUserWithDetailByMobile(String mobile);

    UserDTO getUserDTOByUsername(String username);

    CreateUserResponse createUser(CreateUserRequest request);

    AddOrEditUserDetailResponse editUserDetail(AddOrEditUserDetailRequest request);

    /**
     * @param userId
     * @return
     */
    User getUserWithDetailsById(Integer userId);

    public Integer boolToInt(boolean foo);

    ApiUser getApiUserByUsername(String username);

    SwitchFacilityResponse switchFacility(SwitchFacilityRequest request);

    List<UserSearchDTO> lookupUsers(String keyword);

    RemoveUserRoleResponse removeUserRole(RemoveUserRoleRequest request);

    AddUserRolesResponse addUserRoles(AddUserRolesRequest request);

    GrantApiAccessToVendorResponse grantApiAccess(GrantApiAccessToVendorRequest request);

    List<String> getUsersByRole(String role);

    ApiUser addOrUpdateApiUser(ApiUser apiUser);

    AddUserDatatableViewsResponse addUserDatatableViews(AddUserDatatableViewsRequest request);

    String[] decodeToken(String token);

    GetLoginTokenResponse getLoginToken(GetLoginTokenRequest request);

    GrantApiAccessResponse grantApiAccess(GrantApiAccessRequest request);

    EditUserDetailResponse editUserDetail(EditUserDetailRequest request);

    EditDigestDetailResponse editDigestDetail(EditDigestDetailRequest request);

    CreateApiUserResponse createApiUser(CreateApiUserRequest request);

    GetApiUserResponse getApiUser(GetApiUserRequest request);

    UpdateUserLastReadTimeResponse updateUserLastReadTime(UpdateUserLastReadTimeRequest request);

    UpdateUserLastReadTimeResponse updateUserNotificationReadTime(UpdateUserLastReadTimeRequest request);

    DeleteApiUserResponse deleteApiUser(DeleteApiUserRequest request);

    EditApiUserResponse editApiUser(EditApiUserRequest request);

    String makeTokenSignature(long tokenExpiryTime, String username);

    void updateUser(User user);

    User getUserById(Integer userId);

    User getUserByMobile(String mobile);

    User getUserWithDetailByEmail(String email);

    User getUserByEmail(String email);

    String generateLoginToken(String username, int expiryInMinutes, String[] params);

    GetAllUsersResponse getAllUsers(GetAllUsersRequest request);

    GetUserDetailsResponse getUserWithDetailByUsername(GetUserDetailsRequest request);

    List<UserSearchDTO> lookupUsersByUsername(String keyword);

    List<UserSearchDTO> lookupUsersByName(String keyword);

    AddOrEditUserDetailResponse addUser(AddOrEditUserDetailRequest request);

    AdvanceSearchUsersResponse searchUsers(AdvanceSearchUsersRequest request);

    MarkReferrerNotificationReadResponse markReferrerNotificationRead(MarkReferrerNotificationReadRequest request);

    AddUrlVisitedByUserResponse addUrlVisitedByUser(AddUrlVisitedByUserRequest request);

    GetAccessResourceResponse getAccessResourcesByUsername(GetAccessResourceRequest request);

    RegisterDeviceResponse registerDevice(RegisterDeviceRequest request);

    UserProfileVO getUserProfileByUsername(String username);

    User identifyUser(String identifier);

    SendOTPResponse sendOTP(SendOTPRequest request) ;

    VerifyOTPResponse verifyOTP(VerifyOTPRequest request) ;

}
