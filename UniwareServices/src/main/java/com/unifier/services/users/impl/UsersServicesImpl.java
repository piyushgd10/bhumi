/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Nov 27, 2011
 *  @author singla
 */
package com.unifier.services.users.impl;

import com.unifier.core.entity.ExportJobType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unifier.core.api.access.GrantApiAccessRequest;
import com.unifier.core.api.access.GrantApiAccessResponse;
import com.unifier.core.api.access.GrantApiAccessToVendorRequest;
import com.unifier.core.api.access.GrantApiAccessToVendorResponse;
import com.unifier.core.api.export.WsExportColumn;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.api.myaccount.AddUrlVisitedByUserRequest;
import com.unifier.core.api.myaccount.AddUrlVisitedByUserResponse;
import com.unifier.core.api.myaccount.EditDigestDetailRequest;
import com.unifier.core.api.myaccount.EditDigestDetailResponse;
import com.unifier.core.api.myaccount.EditMobileResponse;
import com.unifier.core.api.myaccount.EditUserDetailRequest;
import com.unifier.core.api.myaccount.EditUserDetailResponse;
import com.unifier.core.api.myaccount.GetLastLoginDetailsResponse;
import com.unifier.core.api.myaccount.GetUserDigestRequest;
import com.unifier.core.api.myaccount.GetUserDigestResponse;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadRequest;
import com.unifier.core.api.myaccount.MarkReferrerNotificationReadResponse;
import com.unifier.core.api.myaccount.RegisterDeviceRequest;
import com.unifier.core.api.myaccount.RegisterDeviceResponse;
import com.unifier.core.api.myaccount.UpdateUserDigestRequest;
import com.unifier.core.api.myaccount.UpdateUserDigestResponse;
import com.unifier.core.api.token.GetLoginTokenRequest;
import com.unifier.core.api.token.GetLoginTokenResponse;
import com.unifier.core.api.user.AddOrEditUserDetailRequest;
import com.unifier.core.api.user.AddOrEditUserDetailResponse;
import com.unifier.core.api.user.AddUserDatatableViewsRequest;
import com.unifier.core.api.user.AddUserDatatableViewsResponse;
import com.unifier.core.api.user.AddUserRolesRequest;
import com.unifier.core.api.user.AddUserRolesResponse;
import com.unifier.core.api.user.AdvanceSearchUsersRequest;
import com.unifier.core.api.user.AdvanceSearchUsersResponse;
import com.unifier.core.api.user.ApiUserDTO;
import com.unifier.core.api.user.CreateApiUserRequest;
import com.unifier.core.api.user.CreateApiUserResponse;
import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.api.user.CreateUserResponse;
import com.unifier.core.api.user.DeleteApiUserRequest;
import com.unifier.core.api.user.DeleteApiUserResponse;
import com.unifier.core.api.user.EditApiUserRequest;
import com.unifier.core.api.user.EditApiUserResponse;
import com.unifier.core.api.user.GetAccessResourceRequest;
import com.unifier.core.api.user.GetAccessResourceResponse;
import com.unifier.core.api.user.GetAllUsersRequest;
import com.unifier.core.api.user.GetAllUsersResponse;
import com.unifier.core.api.user.GetApiUserRequest;
import com.unifier.core.api.user.GetApiUserResponse;
import com.unifier.core.api.user.GetUserDetailsRequest;
import com.unifier.core.api.user.GetUserDetailsResponse;
import com.unifier.core.api.user.RemoveUserRoleRequest;
import com.unifier.core.api.user.RemoveUserRoleResponse;
import com.unifier.core.api.user.SwitchFacilityRequest;
import com.unifier.core.api.user.SwitchFacilityResponse;
import com.unifier.core.api.user.UpdateUserLastReadTimeRequest;
import com.unifier.core.api.user.UpdateUserLastReadTimeResponse;
import com.unifier.core.api.user.UserDTO;
import com.unifier.core.api.user.UserDTO.FacilityRole;
import com.unifier.core.api.user.UserSearchDTO;
import com.unifier.core.api.validation.ValidationContext;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.Role.Level;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.core.export.config.ExportColumn;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.export.config.ExportFilter;
import com.unifier.core.transport.http.HttpSender;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.EncryptionUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.ValidatorUtils;
import com.unifier.dao.export.IExportDao;
import com.unifier.dao.users.IUsersDao;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.unifier.services.aspect.RollbackOnFailure;
import com.unifier.services.release.IReleaseUpdateService;
import com.unifier.services.report.IReportService;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.validation.SendOTPRequest;
import com.uniware.core.api.validation.SendOTPResponse;
import com.uniware.core.api.validation.VerifyOTPRequest;
import com.uniware.core.api.validation.VerifyOTPResponse;
import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.cache.RolesCache;
import com.uniware.core.entity.Facility;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserProfileMao;
import com.uniware.dao.warehouse.IFacilityDao;
import com.uniware.services.cache.ScriptVersionedCache;
import com.uniware.services.configuration.DatatableViewConfiguration;
import com.uniware.services.configuration.ExportJobConfiguration;

/**
 * @author singla
 */
@Service("usersService")
public class UsersServicesImpl implements IUsersService {

    private static final Logger   LOG                                    = LoggerFactory.getLogger(UsersServicesImpl.class);
    private static final String   DELIMITER                              = ":";
    private static final String   TOKEN_SALT                             = "superSecret";
    private static final Integer  MAX_API_USER                           = 5;
    private static final String   USER_DIGEST_SUBSCRIPTION_SCRIPT        = "userDigestSubscriptionScript";
    private static final String   EDIT_MOBILE_IN_UNIAUTH_SCRIPT          = "editMobileInUniauthScript";
    private static final String   UPDATE_USER_DIGEST_SUBSCRIPTION_SCRIPT = "updateUserDigestSubscriptionScript";
    private static final String   CREATE_USER_SCRIPT                     = "createUserScript";
    private static final String   REGISTER_DEVICE_SCRIPT                 = "registerDeviceScript";
    private static final String   SEND_OTP_SCRIPT                        = "sendOTPScript";
    private static final String   VERIFY_OTP_SCRIPT                      = "verifyOTPScript";
    private static final String   GET_LAST_LOGIN_DETAILS_SCRIPT          = "getLastLoginDetailsScript";

    @Autowired
    private IUsersDao             usersDao;

    @Autowired
    private IReportService        reportService;

    @Autowired
    private IExportDao            exportDao;

    @Autowired
    private IFacilityDao          facilityDao;

    @Autowired
    private IReleaseUpdateService releaseUpdateService;

    @Autowired
    private IUserProfileMao       userProfileMao;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.uniware.services.users.UsersService#getUserByEmail(java.lang.String)
     */
    @Override
    @Transactional
    public User getUserByUsername(String username) {
        return usersDao.getUserByUsername(username);
    }

    @Override
    @Transactional
    public User getUserByEmail(String email) {
        return usersDao.getUserByEmail(email);
    }

    @Override
    @Transactional
    public User getUserByMobile(String mobile) {
        return usersDao.getUserByMobile(mobile);
    }

    @Override
    @Transactional
    public User getUserWithDetailByEmail(String email) {
        return usersDao.getUserWithDetailByEmail(email);
    }

    @Override
    @Transactional
    public GetUserDigestResponse getUserDigestFlags(GetUserDigestRequest request) {
        GetUserDigestResponse response = new GetUserDigestResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = userProfileMao.getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No User exists with username \"" + request.getUsername() + "\"");
            } else {
                try {
                    ScriptExecutionContext seContext = ScriptExecutionContext.current();
                    ScraperScript userDigestScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(USER_DIGEST_SUBSCRIPTION_SCRIPT, true);
                    Map<String, Object> resultItems = new HashMap<>();
                    String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getCustomerCentralUrl();
                    seContext.addVariable("request", request);
                    seContext.addVariable("accessUrl", url);
                    seContext.addVariable("resultItems", resultItems);
                    userDigestScript.execute();
                    response = new Gson().fromJson(resultItems.get("response").toString(), GetUserDigestResponse.class);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public User getUserWithDetailByMobile(String mobile) {
        return usersDao.getUserWithDetailByMobile(mobile);
    }

    @Override
    @Transactional
    public UserDTO getUserDTOByUsername(String username) {
        return new UserDTO(usersDao.getUserByUsername(username));
    }

    @Override
    @Transactional
    public User getUserWithDetailsById(Integer userId) {
        return usersDao.getUserWithDetailsById(userId);
    }

    @Override
    @Transactional
    public User getUserById(Integer userId) {
        return usersDao.getUserById(userId);
    }

    @Override
    @Transactional
    public CreateUserResponse createUser(CreateUserRequest request) {
        CreateUserResponse response = new CreateUserResponse();
        ValidationContext context = request.validate();
        User user = new User();
        if (context.hasErrors()) {
            response.setSuccessful(false);
            response.setErrors(context.getErrors());
        } else {
            String normalizedEmail = StringUtils.normalizeEmail(request.getEmail().toLowerCase());
            if (usersDao.getUserByUsername(request.getUsername()) != null || usersDao.getUserByNormalizedEmail(request.getUsername()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "User with username \"" + request.getUsername() + "\"  already exists");
            } else if (usersDao.getUserByNormalizedEmail(normalizedEmail) != null || usersDao.getUserByUsername(normalizedEmail) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "User with email \"" + request.getEmail() + "\" already exists");
            } else {
                user.setUsername(request.getEmail().toLowerCase());
                user.setName(request.getName());
                user.setEmail(request.getEmail().toLowerCase());
                user.setMobile(request.getMobile());
                if (request.isVerifiedMobile()) {
                    user.setMobileVerified(true);
                }
                user.setUserEmail(request.getEmail().toLowerCase());
                user.setNormalizedEmail(normalizedEmail);
                user.setHidden(request.isHidden());
                user.setEnabled(true);
                user.setCreated(DateUtils.getCurrentTime());
                user.setUpdated(DateUtils.getCurrentTime());

                for (String roleCode : request.getRoles()) {
                    UserRole userRole = new UserRole();
                    userRole.setCreated(DateUtils.getCurrentTime());
                    userRole.setUpdated(DateUtils.getCurrentTime());
                    userRole.setEnabled(true);
                    userRole.setUser(user);
                    Role role = CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(roleCode);
                    if (role == null) {
                        context.addError(WsResponseCode.INVALID_ROLE_CODE, "Role: " + roleCode + " not found in system.");
                        break;
                    }
                    userRole.setRole(role);
                    if (Level.FACILITY.name().equals(role.getLevel())) {
                        userRole.setFacility(UserContext.current().getFacility());
                    }
                    user.getUserRoles().add(userRole);
                }
            }
            if (!context.hasErrors() && !request.isHidden()) {
                createUserOnAuth(user, context, request.getPassword());
            }

            // set response to unsuccessful if any errors
            if (context.hasErrors()) {
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                usersDao.addUser(user);
                //addPreloadedWidgets(user);
                createUserProfile(user);
                response.setSuccessful(true);
                response.setUserDTO(new UserDTO(user));
            }
        }
        return response;
    }

    private boolean createUserOnAuth(User user, ValidationContext context, String password) {
        try {
            ScriptExecutionContext seContext = ScriptExecutionContext.current();
            seContext.setTraceLoggingEnabled(true);
            ScraperScript createUserScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(CREATE_USER_SCRIPT, true);
            String serviceUrl = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
            seContext.addVariable("serviceUrl", serviceUrl);
            seContext.addVariable("user", user);
            seContext.addVariable("password", password);
            createUserScript.execute();
            return true;
        } catch (Exception e) {
            context.addError(WsResponseCode.DUPLICATE_CODE, e.getMessage());
            LOG.error("Error creating user on auth server", e);
        } finally {
            ScriptExecutionContext.destroy();
        }
        return false;
    }

//    private void addPreloadedWidgets(User user) {
//        List<DashboardWidget> dashboardWidgets = reportService.getPreloadedWidgets();
//        for (DashboardWidget widget : dashboardWidgets) {
//            AddWidgetRequest request = new AddWidgetRequest();
//            request.setCode(widget.getCode());
//            request.setUserId(user.getId());
//            reportService.addWidget(request);
//        }
//    }

    private void createUserProfile(User user) {
        com.uniware.core.vo.UserProfileVO userProfile = new com.uniware.core.vo.UserProfileVO();
        userProfile.setUsername(user.getUsername());
        userProfile.setLastReleaseUpdateVersionRead(releaseUpdateService.getLatestReleaseUpdateVersion().getMajorVersion());
        userProfile.setVerified(false);
        userProfileMao.save(userProfile);
    }

    @Override
    @Transactional
    public AddOrEditUserDetailResponse editUserDetail(AddOrEditUserDetailRequest request) {
        AddOrEditUserDetailResponse response = new AddOrEditUserDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserWithDetailByUsername(request.getUsername());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID);
            } else {
                user.setEnabled(request.isEnabled());
                user.setName(request.getName());
                user = usersDao.editUser(user);
                List<String> tenantRolesToRemove = new ArrayList<>();
                Map<String, List<String>> faciltyRolesToRemoveMap = new HashMap<String, List<String>>();
                Map<String, List<String>> faciltyRolesMap = facilityRolesListToMap(request.getFacilityRoles());
                for (UserRole userRole : user.getUserRoles()) {
                    if (userRole.isEnabled() && !userRole.getRole().isHidden()) {
                        if (userRole.getRole().getLevel().equals(Role.Level.TENANT.name())) {
                            if (request.getTenantRoles().contains(userRole.getRole().getCode())) {
                                request.getTenantRoles().remove(userRole.getRole().getCode());
                            } else {
                                tenantRolesToRemove.add(userRole.getRole().getCode());
                            }
                        } else {
                            if (faciltyRolesMap.get(userRole.getFacility().getCode()) != null) {
                                List<String> roleCodes = faciltyRolesMap.get(userRole.getFacility().getCode());
                                if (roleCodes.contains(userRole.getRole().getCode())) {
                                    faciltyRolesMap.get(userRole.getFacility().getCode()).remove(userRole.getRole().getCode());
                                } else {
                                    if (faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()) != null) {
                                        faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()).add(userRole.getRole().getCode());
                                    } else {
                                        faciltyRolesToRemoveMap.put(userRole.getFacility().getCode(), new ArrayList<String>());
                                        faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()).add(userRole.getRole().getCode());
                                    }
                                }
                            } else {
                                if (faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()) != null) {
                                    faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()).add(userRole.getRole().getCode());
                                } else {
                                    faciltyRolesToRemoveMap.put(userRole.getFacility().getCode(), new ArrayList<String>());
                                    faciltyRolesToRemoveMap.get(userRole.getFacility().getCode()).add(userRole.getRole().getCode());
                                }
                            }
                        }
                    }
                }
                List<FacilityRole> facilityRolesToRemove = facilityRolesMapToList(faciltyRolesToRemoveMap);
                List<FacilityRole> facilityRolesToAdd = facilityRolesMapToList(faciltyRolesMap);
                try {
                    addUserRolesInternal(request.getTenantRoles(), facilityRolesToAdd, user);
                    removeUserRolesInternal(tenantRolesToRemove, facilityRolesToRemove, user);
                    response.setSuccessful(true);
                    response.setUserDTO(new UserDTO(user));
                } catch (Exception e) {
                    LOG.error("Failed to edit user detail", e);
                    context.addError(WsResponseCode.UNABLE_TO_ADD_USER_ROLE, e.getMessage());
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public MarkReferrerNotificationReadResponse markReferrerNotificationRead(MarkReferrerNotificationReadRequest request) {
        MarkReferrerNotificationReadResponse response = new MarkReferrerNotificationReadResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "No User exists with username \"" + request.getUsername() + "\"");
            } else if (userProfile.isReferrerNotificationRead()) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Notifications already read by user");
            } else {
                if (!request.isRemindLater()) {
                    userProfile.setReferrerNotificationRead(true);
                }
                userProfile.setReferrerNotificationReadTime(DateUtils.getCurrentTime());
                userProfileMao.save(userProfile);
                response.setSuccessful(true);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public EditUserDetailResponse editUserDetail(EditUserDetailRequest request) {
        EditUserDetailResponse response = new EditUserDetailResponse();
        request.setTenantCode(UserContext.current().getTenant().getCode());
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserByUsername(request.getUsername());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No User exists with username \"" + request.getUsername() + "\"");
            } else {
                String validMobile = ValidatorUtils.getValidMobileOrNull(request.getMobile());
                if (!StringUtils.getNotNullValue(user.getMobile()).equals(StringUtils.getNotNullValue(validMobile))) {
                    if (validMobile == null && !(request.getMobile().equals("")))
                        context.addError(WsResponseCode.UNKNOWN_ERROR, "The mobile number is not well-formed");
                    if (!context.hasErrors()) {
                        User mobileUser = getUserByMobile(request.getMobile());
                        if (mobileUser != null)
                            context.addError(WsResponseCode.DUPLICATE_MOBILE_NUMBER, "Another user already has this mobile number verified");
                        else {
                            try {
                                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                                ScraperScript editMobileInUniauthScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                                        EDIT_MOBILE_IN_UNIAUTH_SCRIPT, true);
                                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
                                Map<String, Object> resultItems = new HashMap<>();
                                seContext.addVariable("authUrl", url);
                                seContext.addVariable("email", user.getEmail());
                                seContext.addVariable("mobile", validMobile);
                                seContext.addVariable("resultItems", resultItems);
                                editMobileInUniauthScript.execute();
                                EditMobileResponse editMobileResponse = new Gson().fromJson(resultItems.get("response").toString(), EditMobileResponse.class);
                                if (editMobileResponse.isSuccessful()) {
                                    user.setMobileVerified(false);
                                    user.setMobile(validMobile);
                                    usersDao.editUser(user);
                                    response.setSuccessful(true);
                                } else {
                                    context.addErrors(editMobileResponse.getErrors());
                                }
                            } catch (Exception e) {
                                LOG.error("Failed to edit user detail", e);
                                context.addError(WsResponseCode.INVALID_USER_DETAIL, e.getMessage());
                            } finally {
                                ScriptExecutionContext.destroy();
                            }
                        }
                    }
                }
                else {
                    response.setSuccessful(true);
                    response.setMessage("No change detected");
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public EditDigestDetailResponse editDigestDetail(EditDigestDetailRequest request) {
        EditDigestDetailResponse response = new EditDigestDetailResponse();
        request.setTenantCode(UserContext.current().getTenant().getCode());
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserByUsername(request.getUsername());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No User exists with username \"" + request.getUsername() + "\"");
            } else {
                try {
                    ScriptExecutionContext seContext = ScriptExecutionContext.current();
                    ScraperScript updateUserSubscriptionScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(
                            UPDATE_USER_DIGEST_SUBSCRIPTION_SCRIPT, true);
                    UpdateUserDigestRequest digestRequest = new UpdateUserDigestRequest(request.getTenantCode(), request.getUsername(), request.isEmailDailyDigest(),
                            request.isEmailWeeklyDigest(), request.isEmailChannelStatus(), request.isEmailHourlyOrderSummary(), request.isSmsDailyDigest(),
                            request.isSmsWeeklyDigest(), request.isSmsChannelStatus(), request.isSmsHourlyOrderSummary());
                    Map<String, Object> resultItems = new HashMap<>();
                    String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getCustomerCentralUrl();
                    seContext.addVariable("accessUrl", url);
                    seContext.addVariable("request", digestRequest);
                    seContext.addVariable("resultItems", resultItems);
                    updateUserSubscriptionScript.execute();
                    UpdateUserDigestResponse updateDigestResponse = new Gson().fromJson(resultItems.get("response").toString(), UpdateUserDigestResponse.class);
                    if (updateDigestResponse.isSuccessful()) {
                        response.setSuccessful(true);
                        response.setUserAccountDetailsDTO(updateDigestResponse.getUserAccountDetailsDTO());
                    } else {
                        context.addErrors(updateDigestResponse.getErrors());
                    }
                } catch (Exception e) {
                    LOG.error("Failed to edit digest detail", e);
                    context.addError(WsResponseCode.INVALID_USER_DETAIL, e.getMessage());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    public Integer boolToInt(boolean foo) {
        return (foo) ? 1 : 0;
    }

    @Override
    @Transactional
    public ApiUser getApiUserByUsername(String username) {
        return usersDao.getApiUserByUsername(username);
    }

    @Override
    @Transactional
    public SwitchFacilityResponse switchFacility(SwitchFacilityRequest request) {
        SwitchFacilityResponse response = new SwitchFacilityResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            Facility facility = null;
            if (StringUtils.isNotBlank(request.getFacilityCode())) {
                facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(request.getFacilityCode());
                if (facility == null) {
                    context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Invalid facility code");
                }
            }
            if (!context.hasErrors()) {
                User user = getUserWithDetailsById(request.getUserId());
                user.setCurrentFacility(facility);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<UserSearchDTO> lookupUsers(String keyword) {
        List<UserSearchDTO> usersDTOList = new ArrayList<UserSearchDTO>();
        List<String> usernames = new ArrayList<String>();
        for (UserSearchDTO user : lookupUsersByName(keyword)) {
            usersDTOList.add(user);
            usernames.add(user.getUserName());
        }
        for (UserSearchDTO user : lookupUsersByUsername(keyword)) {
            if (!usernames.contains(user.getUserName())) {
                usersDTOList.add(user);
                usernames.add(user.getUserName());
            }
        }
        return usersDTOList;
    }

    @Override
    @Transactional
    public RemoveUserRoleResponse removeUserRole(RemoveUserRoleRequest request) {
        RemoveUserRoleResponse response = new RemoveUserRoleResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserRole userRole = usersDao.getUserRole(request.getFacilityCode(), request.getRoleCode(), request.getUsername());
            if (userRole == null) {
                context.addError(WsResponseCode.INVALID_FACILITY_CODE, "User doesn't have this role.");
            }
            if (!context.hasErrors()) {
                usersDao.deleteUserRole(userRole);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public AddUserDatatableViewsResponse addUserDatatableViews(AddUserDatatableViewsRequest request) {
        AddUserDatatableViewsResponse response = new AddUserDatatableViewsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_CODE, "Invalid user. No user found.");
                response.setSuccessful(false);
            } else {
                if (StringUtils.isBlank(request.getName())) {
                    context.addError(WsResponseCode.INVALID_DATATABLE_VIEW_NAME, "Datatable view name cannot be empty.");
                } else {
                    ExportJobConfiguration configuration = ConfigurationManager.getInstance().getConfiguration(ExportJobConfiguration.class);
                    ExportConfig exportConfig = configuration.getExportConfigByName(request.getDatatableName());
                    if (exportConfig == null) {
                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME);
                    } else {
                        if (request.getDataTableView() != null) {
                            for (WsExportColumn column : request.getDataTableView().getColumns()) {
                                ExportColumn exportColumn = exportConfig.getExportColumnById(column.getName());
                                if (exportColumn == null) {
                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_COLUMN, "invalid column type :" + column);
                                }
                            }
                            if (request.getDataTableView().getFilters() != null && request.getDataTableView().getFilters().size() > 0) {
                                for (WsExportFilter wsExportFilter : request.getDataTableView().getFilters()) {
                                    ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                                    if (exportFilter == null) {
                                        context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER, "invalid filter type :" + wsExportFilter.getId());
                                    } else {
                                        switch (exportFilter.getType()) {
                                            case BOOLEAN:
                                                if (wsExportFilter.getChecked() == null) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "checked is mandatory for boolean value type");
                                                }
                                                break;
                                            case TEXT:
                                                if (StringUtils.isEmpty(wsExportFilter.getText())) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "text is mandatory for text value type");
                                                }
                                                break;
                                            case DATERANGE:
                                                if (wsExportFilter.getDateRange() == null) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateRange is mandatory for daterange value type");
                                                }
                                                break;
                                            case DATETIME:
                                                if (wsExportFilter.getDateTime() == null) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "dateTime is mandatory for datetime value type");
                                                }
                                                break;
                                            case SELECT:
                                                if (StringUtils.isEmpty(wsExportFilter.getSelectedValue())) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "selectedValue is mandatory for select value type");
                                                } else if (!exportFilter.getSelectValues().contains(wsExportFilter.getSelectedValue())) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_SELECT_VALUE, "selectedValue is not applicable for this filter");
                                                }
                                                break;
                                            case MULTISELECT:
                                                if (wsExportFilter.getSelectedValues() == null || wsExportFilter.getSelectedValues().size() == 0) {
                                                    context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_VALUE, "selectedValues is mandatory for multiselect value type");
                                                } else {
                                                    for (String selectedValue : wsExportFilter.getSelectedValues()) {
                                                        if (!exportFilter.getSelectValues().contains(selectedValue)) {
                                                            context.addError(WsResponseCode.INVALID_EXPORT_JOB_FILTER_SELECT_VALUE,
                                                                    "selectedValue is not applicable for this filter");
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        } else {
                            context.addError(WsResponseCode.INVALID_DATATABLE_VIEW, "Datatable view cannot be empty.");
                        }
                    }
                    if (!context.hasErrors()) {
                        UserDatatableView view = usersDao.getDatatableViewByName(request.getName(), request.getDatatableName(), request.getUserId());
                        if (view != null) {
                            view.setConfigJson(new Gson().toJson(request.getDataTableView()));
                            view.setUpdated(DateUtils.getCurrentTime());
                            if (request.getIsDefaultView() && (view.getDisplayOrder() != 1)) {
                                usersDao.incrementDisplayOrder(request.getDatatableName());
                                view.setDisplayOrder(1);
                            }
                            view = usersDao.updateUserDatatableView(view);
                        } else {
                            view = new UserDatatableView();
                            view.setConfigJson(new Gson().toJson(request.getDataTableView()));
                            view.setDatatableType(request.getDatatableName());
                            ExportJobType exportJobType = exportDao.getExportJobTypeByName(request.getDatatableName());
                            if (exportJobType != null) {
                                view.setAccessResourceName(exportJobType.getAccessResourceName());
                                view.setName(request.getName());
                                view.setTenant(UserContext.current().getTenant());
                                view.setCode(constructUserDatatableViewCodeByName(request.getName()));
                                if (request.getIsDefaultView()) {
                                    usersDao.incrementDisplayOrder(request.getDatatableName());
                                    view.setDisplayOrder(1);
                                }
                                view.setCreated(DateUtils.getCurrentTime());
                                usersDao.addUserDatatableView(view);
                            } else {
                                context.addError(WsResponseCode.INVALID_EXPORT_JOB_TYPE_NAME, "ExportJobType Not Found");
                            }
                        }
                        DatatableViewConfiguration datatableViewConfiguration = ConfigurationManager.getInstance().getConfiguration(DatatableViewConfiguration.class);
                        datatableViewConfiguration.addDatatableView(view);
                        response.setSuccessful(true);
                    } else {
                        response.setSuccessful(false);
                    }
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    private String constructUserDatatableViewCodeByName(String viewName) {
        return viewName.replaceAll("\\W", "_").toUpperCase();
    }

    @Override
    @Transactional
    public AddUserRolesResponse addUserRoles(AddUserRolesRequest request) {
        AddUserRolesResponse response = new AddUserRolesResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserByUsername(request.getUsername());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_CODE, "No User exists with username \"" + request.getUsername() + "\"");
                response.setSuccessful(false);
                response.setErrors(context.getErrors());
            } else {
                if (request.getFacilityCodes() != null) {
                    for (String facilityCode : request.getFacilityCodes()) {
                        for (String roleCode : request.getFacilityRoleCodes()) {
                            UserRole userRole = usersDao.getUserRole(facilityCode, roleCode, user.getUsername());
                            if (userRole != null) {
                                userRole.setEnabled(true);
                                usersDao.updateUserRole(userRole);
                            } else {
                                userRole = new UserRole();
                                userRole.setUser(user);
                                userRole.setFacility(facilityDao.getFacilityByCode(facilityCode));
                                userRole.setRole(CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(roleCode));
                                userRole.setEnabled(true);
                                userRole.setCreated(DateUtils.getCurrentTime());
                                usersDao.addUserRole(userRole);
                            }
                        }
                    }
                }
                if (request.getTenantRoleCodes() != null) {
                    for (String tenantRoleCode : request.getTenantRoleCodes()) {
                        UserRole userRole = usersDao.getUserRole(null, tenantRoleCode, user.getUsername());
                        if (userRole != null) {
                            userRole.setEnabled(true);
                            usersDao.updateUserRole(userRole);
                        } else {
                            userRole = new UserRole();
                            userRole.setUser(user);
                            userRole.setFacility(null);
                            userRole.setRole(CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(tenantRoleCode));
                            userRole.setEnabled(true);
                            userRole.setCreated(DateUtils.getCurrentTime());
                            usersDao.addUserRole(userRole);
                        }
                    }
                }
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public List<String> getUsersByRole(String role) {
        return usersDao.getUsersByRole(role);
    }

    @Override
    @Transactional
    public ApiUser addOrUpdateApiUser(ApiUser apiUser) {
        return usersDao.updateApiUser(apiUser);
    }

    @Override
    @Transactional
    public GrantApiAccessToVendorResponse grantApiAccess(GrantApiAccessToVendorRequest request) {
        GrantApiAccessToVendorResponse response = new GrantApiAccessToVendorResponse();
        ValidationContext context = request.validate();
        User user = null;
        if (!context.hasErrors()) {
            user = getUserByUsername(request.getUserName());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID);
            } else if (user.getVendor() == null) {
                context.addError(WsResponseCode.INVALID_VENDOR_CODE);
            }
        }
        if (context.hasErrors()) {
            response.addErrors(context.getErrors());
        } else {
            ApiUser apiUser = getApiUserByUsername(user.getUsername());
            if (apiUser == null) {
                apiUser = new ApiUser();
                apiUser.setUser(user);
                apiUser.setUsername(user.getUsername());
                apiUser.setApiName(user.getUsername());
                apiUser.setEnabled(true);
                apiUser.setCreated(DateUtils.getCurrentTime());
            }
            apiUser.setPassword(UUID.randomUUID().toString());
            apiUser = addOrUpdateApiUser(apiUser);
            Map<String, String> params = new HashMap<String, String>();
            params.put("token", EncryptionUtils.encrypt(apiUser.getPassword()));
            params.put("username", apiUser.getUsername());
            if (StringUtils.isNotBlank(request.getRuParams())) {
                try {
                    for (String s : request.getRuParams().split("&")) {
                        String[] keyValue = s.split("=");
                        params.put(keyValue[0], keyValue[1]);
                    }
                } catch (Exception e) {
                    LOG.error("Error evaluating ruParams: {}, Error: {}", request.getRuParams(), e.getMessage());
                }
            }
            response.setRedirectUrl(HttpSender.createURL(request.getRedirectUrl(), params));
            response.setSuccessful(true);
        }
        return response;
    }

    @Override
    @Transactional
    public GrantApiAccessResponse grantApiAccess(GrantApiAccessRequest request) {
        GrantApiAccessResponse response = new GrantApiAccessResponse();
        ValidationContext context = request.validate();
        User user;
        if (!context.hasErrors()) {
            user = getUserByUsername(request.getUserName());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            }
            if (user.getUserFacilities().size() > 1) {
                context.addError(WsResponseCode.INVALID_API_REQUEST, "Invalid user with multiple facility");
            }

            if (!context.hasErrors()) {
                ApiUser apiUser = getApiUserByUsername(user.getUsername());
                if (apiUser == null) {
                    apiUser = new ApiUser();
                    apiUser.setUser(user);
                    apiUser.setUsername(user.getUsername());
                    apiUser.setApiName(user.getUsername());
                    apiUser.setEnabled(true);
                    apiUser.setCreated(DateUtils.getCurrentTime());
                }
                apiUser.setPassword(UUID.randomUUID().toString());
                apiUser = addOrUpdateApiUser(apiUser);
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", EncryptionUtils.encrypt(apiUser.getPassword()));
                params.put("username", apiUser.getUsername());
                if (StringUtils.isNotBlank(request.getRuParams())) {
                    try {
                        for (String s : request.getRuParams().split("&")) {
                            String[] keyValue = s.split("=");
                            params.put(keyValue[0], keyValue[1]);
                        }
                    } catch (Exception e) {
                        LOG.error("Error evaluating ruParams: {}, Error: {}", request.getRuParams(), e.getMessage());
                    }
                }
                response.setRedirectUrl(HttpSender.createURL(request.getRedirectUrl(), params));
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional
    public GetLoginTokenResponse getLoginToken(GetLoginTokenRequest request) {
        GetLoginTokenResponse response = new GetLoginTokenResponse();
        ValidationContext context = request.validate();
        String username = StringUtils.isNotBlank(request.getUsername()) ? request.getUsername() : StringUtils.isNotBlank(request.getSupportUsername()) ? request.getSupportUsername() : null;
        if(StringUtils.isBlank(username)) {
            context.addError(WsResponseCode.INVALID_USER_ID, "Invalid username");
        }
        if (!context.hasErrors()) {
            User user = getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID);
            } else {
                String[] tokenParams = { username };
                String token = generateLoginToken(user.getUsername(), 15, tokenParams);
                response.setToken(token);
                LOG.info("Token: {}", token);
                response.setSuccessful(true);
            }
        }
        return response;
    }

    @Override
    @Transactional
    public CreateApiUserResponse createApiUser(CreateApiUserRequest request) {
        CreateApiUserResponse response = new CreateApiUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                List<ApiUser> apiUsers = getApiUserByUserId(request.getUserId());
                if (MAX_API_USER.equals(apiUsers.size())) {
                    context.addError(WsResponseCode.INVALID_API_REQUEST, "Max 5 api key can be generated per user");
                } else {
                    ApiUser apiUser = getApiUserByUsername(request.getName());
                    if (apiUser != null) {
                        context.addError(WsResponseCode.INVALID_USER_DETAIL, "Api already exists with username");
                    }
                }
                if (!context.hasErrors()) {
                    ApiUser apiUser = new ApiUser();
                    apiUser.setUser(user);
                    apiUser.setUsername(constructUsername(request.getName()));
                    apiUser.setApiName(request.getName());
                    apiUser.setEnabled(true);
                    apiUser.setPassword(UUID.randomUUID().toString());
                    apiUser.setCreated(DateUtils.getCurrentTime());
                    apiUser = addOrUpdateApiUser(apiUser);
                    ApiUserDTO apiUserDTO = new ApiUserDTO();
                    apiUserDTO.setApiKey(apiUser.getPassword());
                    apiUserDTO.setApiName(apiUser.getApiName());
                    apiUserDTO.setUsername(apiUser.getUsername());
                    apiUserDTO.setEnabled(apiUser.isEnabled());
                    response.setApiUser(apiUserDTO);
                    response.setSuccessful(true);
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Transactional
    private List<ApiUser> getApiUserByUserId(Integer userId) {
        return usersDao.getApiUserByUserId(userId);
    }

    private String constructUsername(String username) {
        return username.replaceAll("\\W", "_").toLowerCase();
    }

    @Override
    @Transactional
    public GetApiUserResponse getApiUser(GetApiUserRequest request) {
        GetApiUserResponse response = new GetApiUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user id");
            } else {
                List<ApiUserDTO> apiUserDTOs = new ArrayList<ApiUserDTO>();
                List<ApiUser> apiUsers = getApiUserByUserId(request.getUserId());
                for (ApiUser apiUser : apiUsers) {
                    ApiUserDTO apiUserDTO = new ApiUserDTO();
                    apiUserDTO.setApiKey(apiUser.getPassword());
                    apiUserDTO.setUsername(apiUser.getUsername());
                    apiUserDTO.setApiName(apiUser.getApiName());
                    apiUserDTO.setEnabled(apiUser.isEnabled());
                    apiUserDTOs.add(apiUserDTO);
                }
                response.setApiUsers(apiUserDTOs);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public UpdateUserLastReadTimeResponse updateUserLastReadTime(UpdateUserLastReadTimeRequest request) {
        UpdateUserLastReadTimeResponse response = new UpdateUserLastReadTimeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid user");
            } else {
                UserProfileVO userProfile = getUserProfileByUsername(user.getUsername());
                userProfile.setLastReadTime(new Date(request.getLastMessageReadTimeStamp()));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public UpdateUserLastReadTimeResponse updateUserNotificationReadTime(UpdateUserLastReadTimeRequest request) {
        UpdateUserLastReadTimeResponse response = new UpdateUserLastReadTimeResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserWithDetailsById(request.getUserId());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_ID, "Invalid User");
            } else {
                UserProfileVO userProfile = getUserProfileByUsername(user.getUsername());
                userProfile.setNotificationReadTime(new Date(request.getLastMessageReadTimeStamp()));
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public DeleteApiUserResponse deleteApiUser(DeleteApiUserRequest request) {
        DeleteApiUserResponse response = new DeleteApiUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ApiUser apiUser = getApiUserByUsername(request.getUsername());
            if (apiUser == null) {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid api username");
            } else {
                usersDao.deleteApiUser(apiUser);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public EditApiUserResponse editApiUser(EditApiUserRequest request) {
        EditApiUserResponse response = new EditApiUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            ApiUser apiUser = getApiUserByUsername(request.getUsername());
            if (apiUser == null) {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid api username");
            } else {
                apiUser.setEnabled(request.getEnabled());
                apiUser = addOrUpdateApiUser(apiUser);
                ApiUserDTO apiUserDTO = new ApiUserDTO();
                apiUserDTO.setApiKey(apiUser.getPassword());
                apiUserDTO.setEnabled(apiUser.isEnabled());
                apiUserDTO.setApiName(apiUser.getApiName());
                apiUserDTO.setUsername(apiUser.getUsername());
                response.setApiUserDTO(apiUserDTO);
                response.setSuccessful(true);
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public String[] decodeToken(String token) {
        String tokenAsPlainText = EncryptionUtils.hexDecodeAsString(token);
        String[] tokens = tokenAsPlainText.split(DELIMITER);
        return tokens;
    }

    @Override
    @Transactional
    public String makeTokenSignature(long tokenExpiryTime, String username) {
        String data = username + DELIMITER + tokenExpiryTime;
        return EncryptionUtils.md5Encode(data, TOKEN_SALT);
    }

    /**
     * @param tokenValues the tokens to be encoded.
     * @return base64 encoding of the tokens concatenated with the ":" delimiter.
     */
    private String getEncodedToken(List<String> tokenValues) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tokenValues.size(); i++) {
            sb.append(tokenValues.get(i));
            if (i < tokenValues.size() - 1) {
                sb.append(DELIMITER);
            }
        }

        String value = sb.toString();
        return EncryptionUtils.hexEncode(value);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        usersDao.updateUser(user);
    }

    @Override
    public String generateLoginToken(String username, int expiryInMinutes, String[] params) {
        long tokenExpiryTime = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.MINUTE, expiryInMinutes).getTime();
        String signature = makeTokenSignature(tokenExpiryTime, username);
        List<String> tokenValues = new ArrayList<String>();
        tokenValues.add(username);
        tokenValues.add(String.valueOf(tokenExpiryTime));
        tokenValues.add(signature);
        if (params != null) {
            for (String param : params) {
                tokenValues.add(param);
            }
        }
        LOG.info("TokenValues: ", tokenValues);
        String token = getEncodedToken(tokenValues);
        return token;
    }

    @Override
    @Transactional
    public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {
        GetAllUsersResponse response = new GetAllUsersResponse();
        ValidationContext context = request.validate();
        List<UserSearchDTO> userSearchDTOs = new ArrayList<>();
        if (!context.hasErrors()) {
            response.setTotalCount(usersDao.getAllUsersCount());
            if (StringUtils.isBlank(request.getSortBy()) || !StringUtils.equalsAny(request.getSortBy(), new String[] { "created", "lastLoginTime", "name" })) {
                request.setSortBy("name");
            }
            for (User user : usersDao.getAllUsers(request)) {
                userSearchDTOs.add(new UserSearchDTO(user));
            }
            response.setSuccessful(true);
        }
        response.setUserDTOs(userSearchDTOs);
        response.setErrors(context.getErrors());
        return response;
    }

    @Override
    @Transactional
    public GetUserDetailsResponse getUserWithDetailByUsername(GetUserDetailsRequest request) {
        GetUserDetailsResponse response = new GetUserDetailsResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = usersDao.getUserWithDetailByUsername(request.getUsername());
            if (user == null) {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid Username");
            } else {
                try {
                    ScriptExecutionContext seContext = ScriptExecutionContext.current();
                    ScraperScript getLastLoginDetailsScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(GET_LAST_LOGIN_DETAILS_SCRIPT, true);
                    String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
                    Map<String, Object> resultItems = new HashMap<>();
                    seContext.addVariable("authUrl", url);
                    seContext.addVariable("email", user.getEmail());
                    seContext.addVariable("resultItems", resultItems);
                    getLastLoginDetailsScript.execute();
                    GetLastLoginDetailsResponse getLastLoginDetailsResponse = new Gson().fromJson(resultItems.get("response").toString(), GetLastLoginDetailsResponse.class);
                    if (getLastLoginDetailsResponse.isSuccessful()) {
                        user.setLastLoginTime(getLastLoginDetailsResponse.getLastLoginTime());
                        user.setLastAccessedFrom(getLastLoginDetailsResponse.getLastAccessedFrom());
                        updateUser(user);
                        response.setUser(new UserDTO(user));
                        response.setSuccessful(true);
                    } else
                        context.addErrors(getLastLoginDetailsResponse.getErrors());
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
        response.setErrors(context.getErrors());
        return response;
    }

    public void addUserRolesInternal(List<String> tenantRoles, List<FacilityRole> facilityRoles, User user) {
        if (facilityRoles != null && facilityRoles.size() > 0) {
            for (FacilityRole facilityRole : facilityRoles) {
                for (String roleCode : facilityRole.getRoles()) {
                    UserRole userRole = usersDao.getUserRole(facilityRole.getFacilityCode(), roleCode, user.getUsername());
                    if (userRole != null) {
                        userRole.setEnabled(true);
                        usersDao.updateUserRole(userRole);
                    } else {
                        Facility facility = facilityDao.getFacilityByCode(facilityRole.getFacilityCode());
                        Role role = CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(roleCode);
                        if (facility != null && role != null) {
                            userRole = new UserRole();
                            userRole.setUser(user);
                            userRole.setFacility(facility);
                            userRole.setRole(role);
                            userRole.setEnabled(true);
                            userRole.setCreated(DateUtils.getCurrentTime());
                            usersDao.addUserRole(userRole);
                        }
                    }
                }
            }
        }
        if (tenantRoles != null && tenantRoles.size() > 0) {
            for (String tenantRoleCode : tenantRoles) {
                UserRole userRole = usersDao.getUserRole(null, tenantRoleCode, user.getUsername());
                if (userRole != null) {
                    userRole.setEnabled(true);
                    usersDao.updateUserRole(userRole);
                } else {
                    Role role = CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(tenantRoleCode);
                    if (role != null) {
                        userRole = new UserRole();
                        userRole.setUser(user);
                        userRole.setFacility(null);
                        userRole.setRole(CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(tenantRoleCode));
                        userRole.setEnabled(true);
                        userRole.setCreated(DateUtils.getCurrentTime());
                        usersDao.addUserRole(userRole);
                    }
                }
            }
        }
    }

    public void removeUserRolesInternal(List<String> tenantRoles, List<FacilityRole> facilityRoles, User user) {
        if (facilityRoles != null && facilityRoles.size() > 0) {
            for (FacilityRole facilityRole : facilityRoles) {
                for (String roleCode : facilityRole.getRoles()) {
                    UserRole userRole = usersDao.getUserRole(facilityRole.getFacilityCode(), roleCode, user.getUsername());
                    if (userRole != null) {
                        usersDao.deleteUserRole(userRole);
                    }
                }
            }
        }
        if (tenantRoles != null && tenantRoles.size() > 0) {
            for (String tenantRoleCode : tenantRoles) {
                UserRole userRole = usersDao.getUserRole(null, tenantRoleCode, user.getUsername());
                if (userRole != null) {
                    usersDao.deleteUserRole(userRole);
                }
            }
        }
    }

    @Override
    @Transactional
    public List<UserSearchDTO> lookupUsersByUsername(String keyword) {
        List<UserSearchDTO> usersDTOList = new ArrayList<UserSearchDTO>();
        for (User user : usersDao.lookupUsersByUsername(keyword)) {
            UserSearchDTO usersDTO = new UserSearchDTO(user);
            usersDTOList.add(usersDTO);
        }
        return usersDTOList;
    }

    @Override
    @Transactional
    public List<UserSearchDTO> lookupUsersByName(String keyword) {
        List<UserSearchDTO> usersDTOList = new ArrayList<UserSearchDTO>();
        for (User user : usersDao.lookupUsers(keyword)) {
            UserSearchDTO usersDTO = new UserSearchDTO(user);
            usersDTOList.add(usersDTO);
        }
        return usersDTOList;
    }

    public Map<String, List<String>> facilityRolesListToMap(List<FacilityRole> facilityRoles) {
        Map<String, List<String>> facilityRolesMap = new HashMap<String, List<String>>();
        Iterator<FacilityRole> iterator = facilityRoles.iterator();
        while (iterator.hasNext()) {
            FacilityRole facilityRole = iterator.next();
            facilityRolesMap.put(facilityRole.getFacilityCode(), facilityRole.getRoles());
        }
        return facilityRolesMap;
    }

    public List<FacilityRole> facilityRolesMapToList(Map<String, List<String>> facilityRolesMap) {
        List<FacilityRole> facilityRoles = new ArrayList<FacilityRole>();
        Iterator<Entry<String, List<String>>> iterator = facilityRolesMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, List<String>> row = iterator.next();
            facilityRoles.add(new FacilityRole(row.getKey(), row.getValue()));
        }
        return facilityRoles;
    }

    @Override
    @Transactional
    @RollbackOnFailure
    public AddOrEditUserDetailResponse addUser(AddOrEditUserDetailRequest request) {
        AddOrEditUserDetailResponse response = new AddOrEditUserDetailResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            if (request.getUsername().length() < 3 || request.getUsername().contains(" ")) {
                context.addError(WsResponseCode.INVALID_REQUEST, "Please enter a valid username" + request.getUsername() + "is not a valid username");
                LOG.error(request.getUsername() + "is not a valid username");
            }
            if (usersDao.getUserByUsername(request.getUsername()) != null) {
                context.addError(WsResponseCode.DUPLICATE_CODE, "User already exist with username :" + request.getUsername());
                LOG.error("Error creating user as user already exist with username : " + request.getUsername());
            }
        }
        if (!context.hasErrors()) {
            User user = new User();
            String normalizedEmail = StringUtils.normalizeEmail(request.getUsername().toLowerCase());
            user.setUsername(request.getUsername().toLowerCase());
            user.setName(request.getName());
            user.setEmail(request.getUsername().toLowerCase());
            user.setMobile(request.getMobile());
            user.setUserEmail(request.getUsername().toLowerCase());
            user.setNormalizedEmail(normalizedEmail);
            user.setHidden(false);
            user.setEnabled(true);
            user.setCreated(DateUtils.getCurrentTime());
            user.setUpdated(DateUtils.getCurrentTime());
            if (request.getTenantRoles().size() > 0) {
                for (String roleCode : request.getTenantRoles()) {
                    UserRole userRole = new UserRole();
                    userRole.setCreated(DateUtils.getCurrentTime());
                    userRole.setUpdated(DateUtils.getCurrentTime());
                    userRole.setEnabled(true);
                    userRole.setUser(user);
                    Role role = CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(roleCode);
                    if (role == null) {
                        context.addError(WsResponseCode.INVALID_ROLE_CODE, "Role: " + roleCode + " not found in system.");
                        break;
                    }
                    userRole.setRole(role);
                    user.getUserRoles().add(userRole);
                }
            }
            if (request.getFacilityRoles().size() > 0) {
                for (FacilityRole facilityRole : request.getFacilityRoles()) {
                    for (String roleCode : facilityRole.getRoles()) {
                        UserRole userRole = new UserRole();
                        userRole.setCreated(DateUtils.getCurrentTime());
                        userRole.setUpdated(DateUtils.getCurrentTime());
                        userRole.setEnabled(true);
                        userRole.setUser(user);
                        Role role = CacheManager.getInstance().getCache(RolesCache.class).getRoleByCode(roleCode);
                        if (role == null) {
                            context.addError(WsResponseCode.INVALID_ROLE_CODE, "Role: " + roleCode + " not found in system.");
                            break;
                        }
                        userRole.setRole(role);
                        Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityByCode(facilityRole.getFacilityCode());
                        if (facility == null) {
                            context.addError(WsResponseCode.INVALID_FACILITY_CODE, "Facility: " + facilityRole.getFacilityCode() + " not found in system.");
                            break;
                        }
                        userRole.setFacility(facility);
                        user.getUserRoles().add(userRole);
                    }
                }
            }
            if (!context.hasErrors()) {
                createUserOnAuth(user, context, null);
            }

            if (!context.hasErrors()) {
                usersDao.addUser(user);
                //addPreloadedWidgets(user);
                createUserProfile(user);
                response.setUserDTO(new UserDTO(user));
                response.setSuccessful(true);
            } else {
                response.setErrors(context.getErrors());
            }
        }
        if(context.hasErrors()){
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public AdvanceSearchUsersResponse searchUsers(AdvanceSearchUsersRequest request) {
        AdvanceSearchUsersResponse response = new AdvanceSearchUsersResponse();
        List<UserSearchDTO> userDTOs = new ArrayList<UserSearchDTO>();
        List<User> users = usersDao.searchUsers(request);
        for (User user : users) {
            List<UserRole> userRoles = new ArrayList<UserRole>();
            for (UserRole userRole : user.getUserRoles()) {
                if (!userRole.getRole().isHidden()) {
                    userRoles.add(userRole);
                }
            }
            user.getUserRoles().clear();
            user.getUserRoles().addAll(userRoles);
            userDTOs.add(new UserSearchDTO(user));
        }
        response.setTotalCount(new Long(userDTOs.size()));
        Integer startIndex = (request.getPageNumber() - 1) * request.getPageSize();
        Integer endIndex = startIndex + request.getPageSize();
        if (endIndex > userDTOs.size()) {
            endIndex = userDTOs.size();
        }
        List<UserSearchDTO> searchDTOs = userDTOs.subList(startIndex, endIndex);
        response.setSuccessful(true);
        response.setUserDTOs(searchDTOs);
        return response;
    }

    @Override
    public AddUrlVisitedByUserResponse addUrlVisitedByUser(AddUrlVisitedByUserRequest request) {
        AddUrlVisitedByUserResponse response = new AddUrlVisitedByUserResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            UserProfileVO userProfile = getUserProfileByUsername(request.getUsername());
            if (userProfile == null) {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid username");
            } else {
                userProfile.getVisitedUrls().add(request.getVisitedUrl());
                userProfileMao.save(userProfile);
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public GetAccessResourceResponse getAccessResourcesByUsername(GetAccessResourceRequest request) {
        GetAccessResourceResponse response = new GetAccessResourceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserByUsername(request.getUsername());
            List<String> accessResources = new ArrayList<>();
            if (user != null) {
                Set<UserRole> userRoles = user.getUserRoles();
                for (UserRole userRole : userRoles) {
                    Set<String> resources = CacheManager.getInstance().getCache(RolesCache.class).getAccessResourcesByRole(userRole.getRole().getCode());
                    accessResources.addAll(resources);
                }
                response.setAccessResources(StringUtils.join(',', accessResources));
            } else {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "Invalid username" + request.getUsername());
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public RegisterDeviceResponse registerDevice(RegisterDeviceRequest request) {
        RegisterDeviceResponse response = new RegisterDeviceResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                ScriptExecutionContext seContext = ScriptExecutionContext.current();
                ScraperScript registerDeviceScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(REGISTER_DEVICE_SCRIPT);
                Map<String, Object> resultItems = new HashMap<>();
                String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getCustomerCentralUrl();
                seContext.addVariable("accessUrl", url);
                seContext.addVariable("request", request);
                seContext.addVariable("resultItems", resultItems);
                registerDeviceScript.execute();
                response = new Gson().fromJson(resultItems.get("response").toString(), RegisterDeviceResponse.class);
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        response.setSuccessful(!context.hasErrors());
        return response;
    }

    @Override
    public UserProfileVO getUserProfileByUsername(String username) {
        return userProfileMao.getUserProfileByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public User identifyUser(String identifier) {
        User user = getUserByUsername(identifier);
        if (user == null) {
            user = getUserWithDetailByEmail(identifier);
        }
        if (user == null) {
            user = getUserWithDetailByMobile(identifier);
        }
        return user;
    }

    @Override
    public SendOTPResponse sendOTP(SendOTPRequest request) {
        SendOTPResponse response = new SendOTPResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            User user = getUserByEmail(request.getEmail());
            if (user != null) {
                User mobileUser = getUserByMobile(request.getMobile());
                if (mobileUser != null && mobileUser.isMobileVerified() && !user.equals(mobileUser))
                    context.addError(WsResponseCode.DUPLICATE_CODE, "This mobile number is already verified by another user");
                else {
                    try {
                        ScraperScript sendOTPScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(SEND_OTP_SCRIPT);
                        ScriptExecutionContext seContext = ScriptExecutionContext.current();
                        Map<String, Object> resultItems = new HashMap<>();
                        String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
                        seContext.addVariable("authUrl", url);
                        seContext.addVariable("email", user.getEmail());
                        seContext.addVariable("mobile", user.getMobile());
                        seContext.addVariable("resultItems", resultItems);
                        sendOTPScript.execute();
                        response = new Gson().fromJson(resultItems.get("response").toString().replace("\n", ""), SendOTPResponse.class);
                    } finally {
                        ScriptExecutionContext.destroy();
                    }
                }
            } else {
                context.addError(WsResponseCode.INVALID_USER_DETAIL, "No user with this email id");
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }

    @Override
    @Transactional
    public VerifyOTPResponse verifyOTP(VerifyOTPRequest request) {
        VerifyOTPResponse response = new VerifyOTPResponse();
        ValidationContext context = request.validate();
        if (!context.hasErrors()) {
            try {
                User user = getUserByEmail(request.getEmail());
                if (user != null) {
                    ScraperScript verifyOTPScript = CacheManager.getInstance().getCache(ScriptVersionedCache.class).getScriptByName(VERIFY_OTP_SCRIPT);
                    Map<String, Object> resultItems = new HashMap<>();
                    ScriptExecutionContext seContext = ScriptExecutionContext.current();
                    String url = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).getAuthServerUrl();
                    seContext.addVariable("authUrl", url);
                    seContext.addVariable("email", user.getEmail());
                    seContext.addVariable("otp", request.getOtp());
                    seContext.addVariable("resultItems", resultItems);
                    verifyOTPScript.execute();
                    response = new Gson().fromJson(resultItems.get("response").toString(), VerifyOTPResponse.class);
                    if (response.isVerified()) {
                        user.setMobileVerified(true);
                        usersDao.editUser(user);
                    }
                    response.setSuccessful(true);
                } else {
                    context.addError(WsResponseCode.INVALID_USER_DETAIL, "No user with this email id");
                }
            } finally {
                ScriptExecutionContext.destroy();
            }
        }
        if (context.hasErrors()) {
            response.setErrors(context.getErrors());
        }
        return response;
    }
}
