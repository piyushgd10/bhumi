/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, June 05, 2013
 *  @author Pankaj
 */
package com.unifier.dao.sms;

import java.util.List;

import com.uniware.core.entity.SmsChannel;
import com.uniware.core.entity.SmsTemplate;

/**
 * @author Pankaj
 */
public interface ISmsDao {

    List<SmsChannel> getSmsChannels();

    List<SmsTemplate> getSmsTemplates();

}
