/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, June 05, 2013
 *  @author pankaj
 */
package com.unifier.dao.sms.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.dao.sms.ISmsDao;
import com.uniware.core.entity.SmsChannel;
import com.uniware.core.entity.SmsTemplate;
import com.uniware.core.utils.UserContext;

/**
 * @author pankaj
 */
@SuppressWarnings("unchecked")
@Repository
public class SmsDaoImpl implements ISmsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<SmsChannel> getSmsChannels() {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct sc from SmsChannel sc join fetch sc.smsChannelParameters scp where scp.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<SmsTemplate> getSmsTemplates() {
        Query query = sessionFactory.getCurrentSession().createQuery("from SmsTemplate where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }
}
