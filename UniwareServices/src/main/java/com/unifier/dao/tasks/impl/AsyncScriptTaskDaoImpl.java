/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2014
 *  @author harsh
 */
package com.unifier.dao.tasks.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.AsyncScriptInstruction;
import com.unifier.dao.tasks.IAsyncScriptInstructionDao;
import com.uniware.core.utils.UserContext;

@Repository
public class AsyncScriptTaskDaoImpl implements IAsyncScriptInstructionDao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public AsyncScriptInstruction addTask(AsyncScriptInstruction onetimeTask) {
        onetimeTask.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(onetimeTask);
        return onetimeTask;
    }

    @Override
    public AsyncScriptInstruction updateTask(AsyncScriptInstruction task) {
        mongoOperations.save(task);
        return task;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AsyncScriptInstruction> getAllScheduledAsyncScriptTasks() {
        String scheduled = AsyncScriptInstruction.StatusCode.SCHEDULED.name();
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("statusCode").is(scheduled);
        return mongoOperations.find(new Query(criteria), AsyncScriptInstruction.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AsyncScriptInstruction> getAllScheduledAsyncScriptTasks(String callBack) {
        List<String> statuses = Arrays.asList(AsyncScriptInstruction.StatusCode.SCHEDULED.name(), AsyncScriptInstruction.StatusCode.RUNNING.name());
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("statusCode").in(statuses).and("callback").is(callBack);
        return mongoOperations.find(new Query(criteria), AsyncScriptInstruction.class);
    }
}
