/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Aug-2014
 *  @author harsh
 */
package com.unifier.dao.tasks;

import java.util.List;

import com.unifier.core.entity.AsyncScriptInstruction;

public interface IAsyncScriptInstructionDao {

    AsyncScriptInstruction addTask(AsyncScriptInstruction onetimeTask);

    AsyncScriptInstruction updateTask(AsyncScriptInstruction task);

    List<AsyncScriptInstruction> getAllScheduledAsyncScriptTasks();

    List<AsyncScriptInstruction> getAllScheduledAsyncScriptTasks(String callBack);

}
