/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 8, 2011
 *  @author singla
 */
package com.unifier.dao.users;

import java.util.List;

import com.unifier.core.api.user.AdvanceSearchUsersRequest;
import com.unifier.core.api.user.GetAllUsersRequest;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.uniware.core.entity.UserDatatableView;

/**
 * @author singla
 */
public interface IUsersDao {

    /**
     * @param username
     * @return
     */
    User getUserByUsername(String username);

    User getUserWithDetailByMobile(String mobile);

    User addUser(User user);

    User editUser(User user);

    List<User> searchUsers(String username, List<String> roles, String state);

    /**
     * @param userId
     * @return
     */
    User getUserWithDetailsById(Integer userId);

    void deleteUserRole(UserRole userRole);

    ApiUser getApiUserByUsername(String username);

    List<User> lookupUsers(String keyword);

    UserRole getUserRole(String facilityCode, String roleCode, String username);

    UserRole addUserRole(UserRole userRole);

    UserRole updateUserRole(UserRole userRole);

    User checkUserNameAvailability(Integer userId, String uname);

    ApiUser updateApiUser(ApiUser apiUser);

    List<String> getUsersByRole(String role);

    List<UserDatatableView> getViewsforTable(String tableName, int userId);

    UserDatatableView addUserDatatableView(UserDatatableView datatableView);

    UserDatatableView updateUserDatatableView(UserDatatableView datatableView);

    UserDatatableView getDatatableViewByName(String viewName, String datatableName, int userId);

    void incrementDisplayOrder(String datatableName);

    List<ApiUser> getApiUserByUserId(Integer userId);

    void deleteApiUser(ApiUser apiUser);

    void updateUser(User user);

    User getUserById(Integer userId);

    User getUserByNormalizedEmail(String email);

    User getUserByMobile(String mobile);

    User getUserWithDetailByEmail(String email);

    User getUserByEmail(String email);

    User getUserWithDetailByUsername(String username);

    List<User> getAllUsers(GetAllUsersRequest request);
    
    List<User> lookupUsersByUsername(String keyword);

    Long getAllUsersCount();

    List<User> searchUsers(AdvanceSearchUsersRequest request);

}
