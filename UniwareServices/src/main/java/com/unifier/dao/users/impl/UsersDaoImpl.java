/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Dec 10, 2011
 *  @author singla
 */
package com.unifier.dao.users.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.api.user.AdvanceSearchUsersRequest;
import com.unifier.core.api.user.GetAllUsersRequest;
import com.unifier.core.entity.ApiUser;
import com.unifier.core.entity.Role.Level;
import com.unifier.core.entity.User;
import com.unifier.core.entity.UserRole;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.users.IUsersDao;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserProfileVO;
import com.uniware.dao.user.notification.IUserProfileMao;

/**
 * @author singla
 */
@Repository
public class UsersDaoImpl implements IUsersDao {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private IUserProfileMao userProfileMao;

    /*
     * (non-Javadoc)
     * 
     * @see com.uniware.dao.users.UsersDao#getUserByEmail(java.lang.String)
     */
    @Override
    public User getUserByUsername(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User user left join fetch user.tenant left join fetch user.userRoles userRole left join fetch userRole.role left join fetch user.vendors left join fetch user.userDatatableViews where user.tenant.id = :tenantId and username = :username");
        query.setParameter("username", username);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @Override
    public User getUserByNormalizedEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User user where user.tenant.id = :tenantId and user.normalizedEmail = :email");
        query.setParameter("email", email);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @Override
    public User getUserByEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User user where user.tenant.id = :tenantId and user.userEmail = :email");
        query.setParameter("email", email);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }


    @Override
    public User getUserByMobile(String mobile) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User user where user.tenant.id = :tenantId and user.mobile = :mobile and user.mobileVerified = :verified");
        query.setParameter("mobile", mobile);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("verified", true);
        return (User) query.uniqueResult();
    }


    @Override
    public User getUserWithDetailByEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User user left join fetch user.tenant left join fetch user.userRoles userRole left join fetch userRole.role left join fetch user.vendors left join fetch user.userDatatableViews where user.tenant.id = :tenantId and email = :email");
        query.setParameter("email", email);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @Override
    public User getUserWithDetailByMobile(String mobile) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User user left join fetch user.tenant left join fetch user.userRoles userRole left join fetch userRole.role left join fetch user.vendors left join fetch user.userDatatableViews where user.tenant.id = :tenantId and user.mobile = :mobile and user.mobileVerified = :verified");
        query.setParameter("mobile", mobile);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("verified", true);
        return (User) query.uniqueResult();
    }

    @Override
    public User addUser(User user) {
        user.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(user);
        return user;
    }

    @Override
    public User editUser(User user) {
        user.setTenant(UserContext.current().getTenant());
        return (User) sessionFactory.getCurrentSession().merge(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> searchUsers(String username, List<String> roles, String state) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserRole.class);

        Criteria criteriaUser = criteria.createCriteria("user").setFetchMode("userRoles", FetchMode.JOIN);
        criteriaUser.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
        criteriaUser.add(Restrictions.eq("hidden", false));
        if (StringUtils.isNotEmpty(username)) {
            criteriaUser.add(Restrictions.like("username", "%" + username + "%"));
        }
        if (StringUtils.isNotEmpty(state)) {
            criteriaUser.add(Restrictions.eq("enabled", "active".equalsIgnoreCase(state)));
        }
        criteria.createCriteria("role").add(Restrictions.in("code", roles));

        List<UserRole> userRoles = criteria.list();
        List<User> users = new ArrayList<User>();
        if (userRoles != null) {
            for (UserRole userRole : userRoles) {
                if (!users.contains(userRole.getUser())) {
                    users.add(userRole.getUser());
                }
            }
        }

        if (StringUtils.isNotEmpty(username)) {
            Query query = sessionFactory.getCurrentSession().createQuery(
                    "from User user left outer join fetch user.userRoles userRole where user.tenant.id = :tenantId and user.hidden = 0 and userRole is null and user.username != 'system' and user.username like :username");
            query.setParameter("tenantId", UserContext.current().getTenantId());
            query.setParameter("username", "%" + username + "%");
            users.addAll(query.list());
        } else {
            Query query = sessionFactory.getCurrentSession().createQuery(
                    "from User user left outer join fetch user.userRoles userRole where user.tenant.id = :tenantId and user.hidden = 0 and userRole is null and user.username != 'system'");
            query.setParameter("tenantId", UserContext.current().getTenantId());
            users.addAll(query.list());
        }
        return users;
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.users.IUsersDao#getUserById(java.lang.Integer)
     */
    @Override
    public User getUserWithDetailsById(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User user left join fetch user.userRoles userRole left join fetch userRole.role left join fetch user.vendors left join fetch user.userDatatableViews where user.tenant.id = :tenantId and user.id = :userId");
        query.setParameter("userId", userId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @Override
    public User getUserById(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User user where user.tenant.id = :tenantId and user.id = :userId");
        query.setParameter("userId", userId);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @Override
    public void deleteUserRole(UserRole userRole) {
        sessionFactory.getCurrentSession().delete(userRole);
    }

    @Override
    public ApiUser getApiUserByUsername(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from ApiUser apiUser join fetch apiUser.user u left join fetch u.userRoles ur left join fetch ur.role left join fetch u.vendors left join fetch ur.facility where apiUser.tenant.id = :tenantId and apiUser.username = :username");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("username", username);
        return (ApiUser) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> lookupUsers(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User where tenant.id = :tenantId and hidden = 0 and (name like :name or name like :surname)");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name + "%");
        query.setParameter("surname", "% " + name + "%");
        query.setMaxResults(10);
        return query.list();
    }

    @Override
    public UserRole getUserRole(String facilityCode, String roleCode, String username) {
        if (facilityCode == null) {
            Query query = sessionFactory.getCurrentSession().createQuery(
                    "from UserRole userrole where userrole.role.tenant.id = :tenantId and userrole.user.username = :username and userrole.role.code = :roleCode and userrole.facility.id is null");
            query.setParameter("tenantId", UserContext.current().getTenantId());
            query.setParameter("username", username);
            query.setParameter("roleCode", roleCode);
            return (UserRole) query.uniqueResult();
        } else {
            Query query = sessionFactory.getCurrentSession().createQuery(
                    "from UserRole userrole where userrole.role.tenant.id = :tenantId and userrole.user.username = :username and userrole.role.code = :roleCode and userrole.facility.code = :facilityCode");
            query.setParameter("tenantId", UserContext.current().getTenantId());
            query.setParameter("username", username);
            query.setParameter("facilityCode", facilityCode);
            query.setParameter("roleCode", roleCode);
            return (UserRole) query.uniqueResult();
        }

    }

    @Override
    public UserRole addUserRole(UserRole userRole) {
        sessionFactory.getCurrentSession().persist(userRole);
        return userRole;
    }

    @Override
    public UserRole updateUserRole(UserRole userRole) {
        userRole = (UserRole) sessionFactory.getCurrentSession().merge(userRole);
        return userRole;
    }

    @Override
    public User checkUserNameAvailability(Integer userId, String uname) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User user where user.tenant.id = :tenantId and user.id != :userId and user.username = :uname");
        query.setParameter("userId", userId);
        query.setParameter("uname", uname);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setMaxResults(1);
        return (User) query.uniqueResult();
    }

    @Override
    public ApiUser updateApiUser(ApiUser apiUser) {
        apiUser.setTenant(UserContext.current().getTenant());
        return (ApiUser) sessionFactory.getCurrentSession().merge(apiUser);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getUsersByRole(String role) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct u.username from UserRole ur join ur.user u join ur.role r where u.tenant.id = :tenantId and r.tenant.id = :tenantId and r.code = :roleCode and r.level = :level");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("roleCode", role);
        query.setParameter("level", Level.FACILITY.name());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UserDatatableView> getViewsforTable(String tableName, int userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from UserDatatableView uv where uv.datatableType = :datatableType " + "and uv.user.id = :userId and uv.tenant.id = :tenantId");
        query.setParameter("userId", userId);
        query.setParameter("datatableType", tableName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public UserDatatableView getDatatableViewByName(String viewName, String datatableName, int userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from UserDatatableView uv where uv.datatableType = :datatableName and uv.name = :viewName and uv.tenant.id = :tenantId");
        query.setParameter("viewName", viewName);
        query.setParameter("datatableName", datatableName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (UserDatatableView) query.uniqueResult();
    }

    @Override
    public UserDatatableView addUserDatatableView(UserDatatableView datatableView) {
        sessionFactory.getCurrentSession().persist(datatableView);
        return datatableView;
    }

    @Override
    public UserDatatableView updateUserDatatableView(UserDatatableView datatableView) {
        datatableView = (UserDatatableView) sessionFactory.getCurrentSession().merge(datatableView);
        return datatableView;
    }

    @Override
    public void incrementDisplayOrder(String datatableName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "update  UserDatatableView uv set uv.displayOrder = uv.displayOrder+1 where uv.datatableType = :datatableName and uv.tenant.id = :tenantId");
        query.setParameter("datatableName", datatableName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ApiUser> getApiUserByUserId(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ApiUser apiUser join fetch apiUser.user u where apiUser.tenant.id = :tenantId and u.id = :userId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("userId", userId);
        return query.list();
    }

    @Override
    public void deleteApiUser(ApiUser apiUser) {
        sessionFactory.getCurrentSession().delete(apiUser);
    }

    @Override
    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers(GetAllUsersRequest request) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class, "u");
        criteria.add(Restrictions.eq("hidden", false));
        criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
        if (request.isAscending()) {
            criteria.addOrder(Order.asc(request.getSortBy()));
        } else {
            criteria.addOrder(Order.desc(request.getSortBy()));
        }
        if (request.getPageNumber() > 1) {
            criteria.setFirstResult((request.getPageNumber() - 1) * request.getPageSize());
        }
        if (request.getPageSize() > 0) {
            criteria.setMaxResults(request.getPageSize());
        }
        return criteria.list();
        /*Query query = sessionFactory.getCurrentSession().createQuery("from User where tenant.id = :tenantId order by :sortBy :order");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("sortBy", request.getSortBy());
        query.setParameter("order", request.getOrder());
        if(request.getPageNumber() > 1){
            query.setFirstResult((request.getPageNumber()-1)*request.getPageSize());
        }
        if (request.getPageSize() > 0){
            query.setMaxResults(request.getPageSize());
        }
        return query.list();*/
    }

    @Override
    public Long getAllUsersCount() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class, "u");
        criteria.add(Restrictions.eq("hidden", false));
        criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
        criteria.setProjection(Projections.countDistinct("u.id"));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public User getUserWithDetailByUsername(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User user left join fetch user.userRoles userRole left join fetch userRole.role where user.tenant.id = :tenantId and username = :username");
        query.setParameter("username", username);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (User) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> lookupUsersByUsername(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from User where tenant.id = :tenantId and hidden = 0 and (username like :username or username like :partialUsername) and username not like :notlike");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("username", username + "%");
        query.setParameter("notlike", "%@%" + username + "%");
        query.setParameter("partialUsername", "%." + username + "%");
        query.setMaxResults(10);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> searchUsers(AdvanceSearchUsersRequest request) {
        List<User> users = new ArrayList<User>();
        Criteria criteria;
        if (StringUtils.isNotEmpty(request.getNameOrEmail())) {
            criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
            criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
            criteria.add(Restrictions.eq("hidden", false));
            criteria.add(Restrictions.disjunction().add(Restrictions.like("username", request.getNameOrEmail() + "%")).add(
                    Restrictions.like("username", "%." + request.getNameOrEmail() + "%")).add(Restrictions.like("name", request.getNameOrEmail() + "%")).add(
                    Restrictions.like("name", "% " + request.getNameOrEmail() + "%")));
            users = criteria.list();
        } else {
            boolean search = false;
            Criteria criteriaRoles = sessionFactory.getCurrentSession().createCriteria(UserRole.class);
            criteria = criteriaRoles.createCriteria("user").setFetchMode("userRoles", FetchMode.JOIN);
            criteria.createCriteria("tenant").add(Restrictions.idEq(UserContext.current().getTenantId()));
            criteria.add(Restrictions.eq("hidden", false));
            if (StringUtils.isNotEmpty(request.getRole())) {
                search = true;
                criteriaRoles.createCriteria("role").add(Restrictions.eq("code", request.getRole()));
            }
            if (StringUtils.isNotEmpty(request.getFacility())) {
                search = true;
                criteriaRoles.createCriteria("facility").add(Restrictions.eq("displayName", request.getFacility()));
            }

            if (StringUtils.isNotEmpty(request.getEmail())) {
                search = true;
                criteria.add(Restrictions.disjunction().add(Restrictions.like("username", request.getEmail() + "%")).add(
                        Restrictions.like("username", "%." + request.getEmail() + "%")));
            }
            if (StringUtils.isNotEmpty(request.getName())) {
                search = true;
                criteria.add(Restrictions.disjunction().add(Restrictions.like("name", request.getName() + "%")).add(Restrictions.like("name", "% " + request.getName() + "%")));
            }
            if (request.getEnabled() != null) {
                search = true;
                criteria.add(Restrictions.eq("enabled", request.getEnabled()));
            }
            if (request.getVerified() != null) {
                search = true;
                criteria.add(Restrictions.eq("verified", request.getVerified()));
            }
            if (StringUtils.isNotEmpty(request.getPhone())) {
                search = true;
                criteria.add(Restrictions.like("mobile", "%" + request.getPhone() + "%"));
            }
            if (StringUtils.isNotEmpty(request.getStartDate()) && StringUtils.isNotEmpty(request.getEndDate())) {
                search = true;
                Date startDate = DateUtils.stringToDate(request.getStartDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS");
                Date endDate = DateUtils.stringToDate(request.getEndDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS");
                criteria.add(Restrictions.conjunction().add(Restrictions.ge("created", startDate)).add(Restrictions.le("created", endDate)));
            }
            if (search) {
                List<UserRole> userRoles = criteriaRoles.list();
                if (userRoles != null) {
                    for (UserRole userRole : userRoles) {
                        if (!users.contains(userRole.getUser())) {
                            users.add(userRole.getUser());
                        }
                    }
                }
            }

        }
        return users;
    }
}
