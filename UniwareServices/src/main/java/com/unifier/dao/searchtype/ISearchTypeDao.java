package com.unifier.dao.searchtype;

import java.util.List;

import com.unifier.core.entity.SearchTypes;

public interface ISearchTypeDao {
 
    public List<SearchTypes> getAllSearchTypes();

    List<SearchTypes> getAllSearchTypesByProduct(String productCode);

}
