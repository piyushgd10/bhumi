package com.unifier.dao.searchtype.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.SearchTypes;
import com.unifier.dao.searchtype.ISearchTypeDao;
import com.uniware.core.utils.UserContext;

@Repository
public class SearchTypeDaoImpl implements ISearchTypeDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    @SuppressWarnings("unchecked")
    @Override
    public List<SearchTypes> getAllSearchTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("select st from SearchTypes st join fetch st.accessResource");
        return query.list();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<SearchTypes> getAllSearchTypesByProduct(String productCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("select st from SearchTypes st, ProductAccessResource par join fetch st.accessResource ar where st.accessResource.name = par.accessResource.name and par.product.code = :productCode order by st.displayOrder");
        query.setParameter("productCode", productCode);
        return query.list();
    }
}
