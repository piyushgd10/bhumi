/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.dao.imports.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;
import com.unifier.core.utils.DateUtils;
import com.unifier.dao.imports.IImportDao;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;

@Repository
public class ImportDaoImpl implements IImportDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public ImportJob addImportJob(ImportJob importJob) {
        importJob.setTenantCode(UserContext.current().getTenant().getCode());
        importJob.setFacilityCode(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode());
        mongoOperations.save(importJob);
        return importJob;
    }

    @Override
    public ImportJob getImportJobById(String importJobId) {
        return mongoOperations.findById(importJobId, ImportJob.class);
    }

    @Override
    public ImportJob updateImportJob(ImportJob importJob) {
        mongoOperations.save(importJob);
        return importJob;
    }

    @Override
    public List<ImportJob> getUserImports(String username) {
        Date date = DateUtils.getCurrentTime();
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000L);
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(
                Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("userName").is(username).and("created").gte(
                        date)).with(new Sort(Sort.Direction.DESC, "created")),
                ImportJob.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ImportJobType> getAllImportJobTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ImportJobType where tenant.id=:tenantId order by name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public ImportJobType getImportJobTypeByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ImportJobType where name = :name and tenant.id=:tenantId");
        query.setParameter("name", name);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (ImportJobType) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ImportJobType> getImportJobTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ImportJobType where enabled = 1 and tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

}
