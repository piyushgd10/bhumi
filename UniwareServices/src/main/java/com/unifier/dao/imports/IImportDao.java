/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.dao.imports;

import java.util.List;

import com.unifier.core.entity.ImportJob;
import com.unifier.core.entity.ImportJobType;

public interface IImportDao {
    ImportJob addImportJob(ImportJob importJob);

    ImportJob getImportJobById(String importJobId);

    ImportJob updateImportJob(ImportJob importJob);

    List<ImportJob> getUserImports(String username);

    List<ImportJobType> getAllImportJobTypes();

    ImportJobType getImportJobTypeByName(String name);

    List<ImportJobType> getImportJobTypes();
}
