/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2013
 *  @author unicom
 */
package com.unifier.dao.config;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.event.spi.MergeEvent;
import org.hibernate.event.spi.MergeEventListener;
import org.hibernate.event.spi.PersistEvent;
import org.hibernate.event.spi.PersistEventListener;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostInsertEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;

import com.unifier.core.annotation.Customizable;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.CustomFieldValue;
import com.unifier.core.utils.DateUtils;
import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration;
import com.uniware.services.configuration.CustomFieldsMetadataConfiguration.CustomFieldMetadataVO;

public class CustomFieldsUpdateEventListener implements PostInsertEventListener, MergeEventListener, PersistEventListener {

    private static final Logger             LOG               = LoggerFactory.getLogger(CustomFieldsUpdateEventListener.class);

    private static final long               serialVersionUID  = -850450804469059432L;

    private static DefaultConversionService conversionService = new DefaultConversionService();
    static {
        conversionService.addConverter(new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                return new Date(Long.parseLong(source));
            }
        });
        conversionService.addConverter(new Converter<Date, String>() {
            @Override
            public String convert(Date source) {
                return new Long(source.getTime()).toString();
            }
        });
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        doMergeNonSerializableCustomFields(event.getEntity(), event.getSession());
    }

    @Override
    public void onMerge(MergeEvent event) throws HibernateException {
        doMergeNonSerializableCustomFields(event.getOriginal(), event.getSession());
    }

    @Override
    public void onMerge(MergeEvent event, @SuppressWarnings("rawtypes") Map copiedAlready) throws HibernateException {
        doMergeNonSerializableCustomFields(event.getOriginal(), event.getSession());
    }

    private void doMergeNonSerializableCustomFields(Object entity, Session session) {
        Class<?> clazz = entity.getClass();
        if (!clazz.isAnnotationPresent(Customizable.class)) {
            return;
        }
        CustomFieldsMetadataConfiguration config = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        List<CustomFieldMetadataVO> customFields = config.getCustomFieldsByEntity(entity.getClass().getName());
        boolean nonSerializableCustomFieldPresent = false;
        if (customFields != null) {
            for (CustomFieldMetadataVO cfv : customFields) {
                if (!cfv.isSerializable()) {
                    nonSerializableCustomFieldPresent = true;
                    break;
                }
            }
        }
        if (!nonSerializableCustomFieldPresent) {
            return;
        }

        try {
            Method method = config.getCustomFieldValueGetterByEntity(entity.getClass().getName());
            if (method != null) {
                CustomFieldValue cfv = (CustomFieldValue) method.invoke(entity);
                if (cfv == null) {
                    cfv = new CustomFieldValue((Integer) config.getIdGetterByEntity(entity.getClass().getName()).invoke(entity), entity.getClass().getName(),
                            DateUtils.getCurrentTime());
                } else {
                    cfv.setIdentifier((Integer) config.getIdGetterByEntity(entity.getClass().getName()).invoke(entity));
                }
                if (cfv.getId() == null) {
                    session.persist(cfv);
                    config.getCustomFieldValueSetterByEntity(entity.getClass().getName()).invoke(entity, cfv);
                } else {
                    session.merge(cfv);
                }
            }
        } catch (Exception e) {
            LOG.error("[FATAL] Unable to set custom fields, entity: " + entity.getClass().getName(), e);
        }
    }

    @Override
    public void onPersist(PersistEvent event) throws HibernateException {
        Class<?> clazz = event.getObject().getClass();
        if (!clazz.isAnnotationPresent(Customizable.class)) {
            return;
        }
        CustomFieldsMetadataConfiguration config = ConfigurationManager.getInstance().getConfiguration(CustomFieldsMetadataConfiguration.class);
        List<CustomFieldMetadataVO> customFields = config.getCustomFieldsByEntity(event.getObject().getClass().getName());
        if (customFields != null) {
            CustomFieldValue cfv = null;

            // Set default values for all custom fields.
            for (CustomFieldMetadataVO customFieldVO : customFields) {
                try {
                    if (!customFieldVO.isSerializable()) {
                        if (cfv == null) {
                            Method method = config.getCustomFieldValueGetterByEntity(event.getObject().getClass().getName());
                            cfv = (CustomFieldValue) method.invoke(event.getObject());
                            if (cfv == null) {
                                cfv = new CustomFieldValue(event.getObject().getClass().getName(), DateUtils.getCurrentTime());
                                config.getCustomFieldValueSetterByEntity(event.getObject().getClass().getName()).invoke(event.getObject(), cfv);
                            }
                        }
                        Object value = config.getCustomFieldReadMethod(customFieldVO.getMappingFieldName()).invoke(cfv);
                        if (value == null && customFieldVO.getDefaultValue() != null) {
                            config.getCustomFieldWriteMethod(customFieldVO.getMappingFieldName()).invoke(cfv, customFieldVO.getDefaultValue());
                        }
                    } else {
                        Object value = CustomFieldUtils.getCustomFieldValue(event.getObject(), customFieldVO.getFieldName());
                        if (value == null && customFieldVO.getDefaultValue() != null) {
                            CustomFieldUtils.setCustomFieldValue(event.getObject(), customFieldVO.getFieldName(),
                                    conversionService.convert(customFieldVO.getDefaultValue(), customFieldVO.getJavaType()));
                        }
                    }
                } catch (Exception e) {
                    LOG.error("Unable to set default value for field: {}, entity: {}", customFieldVO.getFieldName(), event.getObject().getClass().getName());
                }
            }
        }

    }

    @Override
    public void onPersist(PersistEvent event, @SuppressWarnings("rawtypes") Map createdAlready) throws HibernateException {
        // TODO Auto-generated method stub

    }
}
