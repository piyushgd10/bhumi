/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.unifier.dao.audit;

import java.util.List;

import com.uniware.core.vo.ActivityMetaVO;

/**
 * @author parijat
 */
public interface IActivityLogMao {

    void persistActivityMetaVO(ActivityMetaVO vo);

    List<ActivityMetaVO> getActivitybyIdentifierOrGroupIdentifier(String identifier, String entityName, boolean sort);

    List<ActivityMetaVO> getActivitybyIdentifier(String identifier, String entityName, boolean sort);

    List<ActivityMetaVO> getActivitybyIdentifierGrpIdentifier(String identifier, String entityName);

    int getActivityCountByIdentifier(String identifier, String entityName);

}
