/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.unifier.dao.audit;

import java.util.List;

import com.uniware.core.vo.EntityAuditLogVO;

public interface IAuditMao {

    List<EntityAuditLogVO> getEntityAuditById(String entityName, String identifier);
    
    List<EntityAuditLogVO> getEntitiesAuditByTenant(boolean filterByFacility);

    void save(EntityAuditLogVO logVO);

}
