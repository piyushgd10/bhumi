/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Jun-2014
 *  @author parijat
 */
package com.unifier.dao.audit.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.audit.IActivityLogMao;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.ActivityMetaVO;

@Repository
public class ActivityLogMaoImpl implements IActivityLogMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public void persistActivityMetaVO(ActivityMetaVO metaVO) {
        metaVO.setTenantCode(UserContext.current().getTenant().getCode());
        if (UserContext.current().getFacility() != null) {
            metaVO.setFacilityCode(UserContext.current().getFacility().getCode());
        }
        metaVO.setCreated(DateUtils.getCurrentTime());

        if (StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
            metaVO.setUsername(UserContext.current().getUniwareUserName());
        } else {
            if (StringUtils.isNotBlank(UserContext.current().getApiUsername())) {
                metaVO.setApiUsername(UserContext.current().getApiUsername());
            }
        }
        mongoOperations.save(metaVO);
    }

    @Override
    public List<ActivityMetaVO> getActivitybyIdentifierOrGroupIdentifier(String identifier, String entityName, boolean sort) {
        String tenantCode = UserContext.current().getTenant().getCode();
        Query query = new Query(new Criteria().orOperator(Criteria.where("identifier").is(identifier).and("entityName").is(entityName).and("tenantCode").is(tenantCode),
                Criteria.where("groupIdentifier").is(identifier).and("tenantCode").is(tenantCode)));
        if (sort) {
            query.with(new Sort(Sort.Direction.ASC, "created"));
        }
        return mongoOperations.find(query, ActivityMetaVO.class);
    }

    @Override
    public List<ActivityMetaVO> getActivitybyIdentifier(String identifier, String entityName, boolean sort) {
        String tenantCode = UserContext.current().getTenant().getCode();
        Query query = new Query(Criteria.where("identifier").is(identifier).and("entityName").is(entityName).and("tenantCode").is(tenantCode));
        if (sort) {
            query.with(new Sort(Sort.Direction.ASC, "created"));
        }
        return mongoOperations.find(query, ActivityMetaVO.class);
    }

    @Override
    public List<ActivityMetaVO> getActivitybyIdentifierGrpIdentifier(String groupIdentifier, String entityName) {
        Query query = new Query(Criteria.where("groupIdentifier").is(entityName + "-" + groupIdentifier).and("tenantCode").is(UserContext.current().getTenant().getCode()));
        if (UserContext.current().getFacility() != null) {
            query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
        }
        query.with(new Sort(Sort.Direction.ASC, "created"));
        return mongoOperations.find(query, ActivityMetaVO.class);
    }

    public int getActivityCountByIdentifier(String identifier, String entityName){
        String tenantCode = UserContext.current().getTenant().getCode();
        Query query = new Query(new Criteria().orOperator(Criteria.where("identifier").is(identifier).and("entityName").is(entityName).and("tenantCode").is(tenantCode),
                Criteria.where("groupIdentifier").is(identifier).and("tenantCode").is(tenantCode)));
        return ((Long) mongoOperations.count(query, ActivityMetaVO.class)).intValue();
    }

}
