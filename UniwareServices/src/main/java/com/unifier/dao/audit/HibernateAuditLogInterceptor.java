/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author parijat
 */
package com.unifier.dao.audit;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifier.core.annotation.audit.AuditClass;
import com.unifier.core.annotation.audit.AuditField;
import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.AuditItemField;
import com.unifier.core.expressions.Expression;
import com.uniware.core.utils.AuditContext;
import com.uniware.services.audit.IAuditService;

/**
 * @author parijat
 */
@Component
public class HibernateAuditLogInterceptor extends EmptyInterceptor {

    /**
     * 
     */
    private static final long   serialVersionUID = -4535109854182326150L;
    private static final Logger LOG              = LoggerFactory.getLogger(HibernateAuditLogInterceptor.class);

    @Autowired
    private IAuditService       entityAuditService;

    /* (non-Javadoc)
     * @see org.hibernate.EmptyInterceptor#onDelete(java.lang.Object, java.io.Serializable, java.lang.Object[], java.lang.String[], org.hibernate.type.Type[])
     */
    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (AuditContext.current().isAuditEnabled()) {
            LOG.info("Auditing onDelete {}", id);
            evaluateandPutAuditLog(entity, id, state, null, propertyNames);
        }
        super.onDelete(entity, id, state, propertyNames, types);
    }

    /* (non-Javadoc)
     * @see org.hibernate.EmptyInterceptor#onFlushDirty(java.lang.Object, java.io.Serializable, java.lang.Object[], java.lang.Object[], java.lang.String[], org.hibernate.type.Type[])
     * intercepted while updating entity 
     */
    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        if (AuditContext.current().isAuditEnabled()) {
            LOG.info("Auditing onFlushDirty {}", id);
            evaluateandPutAuditLog(entity, id, currentState, previousState, propertyNames);
        }
        return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
    }

    /* (non-Javadoc)
     * @see org.hibernate.EmptyInterceptor#onSave(java.lang.Object, java.io.Serializable, java.lang.Object[], java.lang.String[], org.hibernate.type.Type[])
     */
    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (AuditContext.current().isAuditEnabled()) {
            LOG.info("Auditing onSave {}", id);
            evaluateandPutAuditLog(entity, id, state, null, propertyNames);
        }
        return super.onSave(entity, id, state, propertyNames, types);
    }

    /**
     * Evaluate change log for all entities in the current hibernate session. Will evaluate the identifier and group
     * identifier of current entity and persist the values as {@link AuditItem} in current {@link AuditContext}
     * 
     * @param entity
     * @param id
     * @param currentState
     * @param previousState
     * @param propertyNames
     * @param action
     */
    private void evaluateandPutAuditLog(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames) {
        if (AuditContext.current().isAuditEnabled() && entity.getClass().isAnnotationPresent(AuditClass.class)) {
            Map<String, Object> contextParams = new HashMap<String, Object>();
            contextParams.put("entity", entity);
            AuditItem auditItem = null;
            String identifer = evaluateExpression(entity.getClass().getAnnotation(AuditClass.class).identifier(), contextParams);
            String entityName = getSimpleClassName(entity);
            String contextKeyName = identifer + ":" + entityName;
            if (AuditContext.current().getEntityIdentifierToAuditItems().containsKey(contextKeyName)) {
                auditItem = AuditContext.current().getEntityIdentifierToAuditItems().get(contextKeyName);
            } else {
                auditItem = new AuditItem();
                auditItem.setGroupIdentifier(evaluateExpression(entity.getClass().getAnnotation(AuditClass.class).groupIdentifier(), contextParams));
                auditItem.setIdentifier(identifer);
                auditItem.setEntityName(entityName);
            }
            if (id != null) {
                auditItem.setEntityId(id.toString());
            }
            LOG.info("Starting entity audit identifier: {} name:{} groupIdentifier:{}", new Object[]{auditItem.getIdentifier(), auditItem.getEntityName(), auditItem.getGroupIdentifier()});
            int index = 0;
            Map<String, AuditItemField> auditFields = auditItem.getAuditFields();
            for (Field field : entity.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(AuditField.class)) {
                    index = 0;
                    for (int i = 0; i < propertyNames.length; i++) {
                        if (propertyNames[i].equalsIgnoreCase(field.getName())) {
                            index = i;
                            break;
                        }
                    }
                    if (currentState[index] != null) {
                        AuditItemField auditField = null;
                        contextParams.put("currentValue", currentState[index]);
                        if (previousState != null) {
                            contextParams.put("previousValue", previousState[index]);
                        }
                        contextParams.put("propertyName", propertyNames[index]);
                        if (auditFields.containsKey(propertyNames[index])) {
                            auditField = auditFields.get(propertyNames[index]);
                        } else {
                            auditField = new AuditItemField();
                            auditField.setFieldName(evaluateExpression(field.getAnnotation(AuditField.class).fieldName(), contextParams));
                        }
                        String newValue = (currentState == null || currentState[index] == null) ? "" : evaluateExpression(
                                field.getAnnotation(AuditField.class).newValueExpression(), contextParams);
                        String oldValue = (previousState == null || previousState[index] == null) ? "" : evaluateExpression(
                                field.getAnnotation(AuditField.class).oldValueExpression(), contextParams);
                        if (!newValue.equalsIgnoreCase(oldValue)) {
                            auditField.setNewValue(newValue);
                            auditField.setOldValue(oldValue);
                            auditFields.put(propertyNames[index], auditField);
                        }
                        if (!AuditContext.current().getMethodVarsMap().containsKey(entityName)) {
                            AuditContext.current().getMethodVarsMap().put(entityName, new ArrayList<AuditItemField>());
                        }
                        AuditContext.current().getMethodVarsMap().get(entityName).add(auditField);
                    }
                }
            }
            AuditContext.current().getEntityIdentifierToAuditItems().put(contextKeyName, auditItem);
        }
    }

    /**
     * Evaluates spring expression
     * 
     * @param identifierExpression
     * @param contextParams
     * @return
     */
    private String evaluateExpression(String identifierExpression, Map<String, Object> contextParams) {
        Expression expression = Expression.compile(identifierExpression);
        return expression.evaluate(contextParams, String.class);
    }

    public String getSimpleClassName(Object object) {
        if (object != null) {
            String className = object.getClass().getName();
            return className.substring(className.lastIndexOf('.') + 1);
        }
        return null;
    }

}
