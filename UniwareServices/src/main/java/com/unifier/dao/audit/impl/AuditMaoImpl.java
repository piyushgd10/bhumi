/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Apr-2014
 *  @author parijat
 */
package com.unifier.dao.audit.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.utils.StringUtils;
import com.unifier.dao.audit.IAuditMao;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.EntityAuditLogVO;

@Repository
public class AuditMaoImpl implements IAuditMao {

	@Autowired
	@Qualifier(value = "tenantSpecificMongo")
	private MongoOperations mongoOperations;

	@Override
	public List<EntityAuditLogVO> getEntityAuditById(String entityName, String identifier) {
		Query query = new Query(Criteria.where("entityName").is(entityName).and("identifier").is(identifier).orOperator(Criteria.where("entityId").is(identifier)).orOperator(
				Criteria.where("groupIdentifier").is(identifier)));
		query.addCriteria(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
		query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
		return mongoOperations.find(query, EntityAuditLogVO.class);
	}

	@Override
	public List<EntityAuditLogVO> getEntitiesAuditByTenant(boolean filterByFacility) {
		Query query = new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()));
        if (filterByFacility && UserContext.current().getFacility() != null) {
            query.addCriteria(Criteria.where("facilityCode").is(UserContext.current().getFacility().getCode()).orOperator(Criteria.where("facilityCode").is(null)));
        }
        return mongoOperations.find(query, EntityAuditLogVO.class);
    }

    @Override
    public void save(EntityAuditLogVO entityVO) {
        entityVO.setTenantCode(UserContext.current().getTenant().getCode());
        entityVO.setFacilityCode(UserContext.current().getFacility().getCode());
        if (StringUtils.isNotBlank(UserContext.current().getUniwareUserName())) {
            entityVO.setUsername(UserContext.current().getUniwareUserName());
        }
        if (StringUtils.isNotBlank(UserContext.current().getApiUsername())) {
            entityVO.setApiUsername(UserContext.current().getApiUsername());
        }
        mongoOperations.save(entityVO);
    }

}
