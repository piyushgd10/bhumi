/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Aug-2014
 *  @author parijat
 */
package com.unifier.dao.audit.impl;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;

import com.unifier.core.api.audit.AuditItem;
import com.unifier.core.api.audit.AuditItemField;
import com.unifier.core.utils.StringUtils;
import com.uniware.core.utils.AuditContext;
import com.uniware.services.audit.IAuditService;

/**
 * @author parijat
 */
@Component(value = "auditPostCommitHandler")
public class AuditPostCommitHandler extends TransactionSynchronizationAdapter {

    private static final Logger LOGGER         = LoggerFactory.getLogger(AuditPostCommitHandler.class);
    private static String       LINE_SEPARATOR = System.getProperty("line.separator");

    @Autowired
    private IAuditService       auditService;

    /* (non-Javadoc)
     * @see org.springframework.transaction.support.TransactionSynchronizationAdapter#afterCommit()
     */
    @Override
    public void afterCommit() {
        if (AuditContext.current().isAuditEnabled() && AuditContext.current().getEntityIdentifierToAuditItems() != null) {
            String methodAuditLog = StringUtils.EMPTY_STRING;
            for (String key : AuditContext.current().getEntityIdentifierToAuditItems().keySet()) {
                AuditItem auditItem = AuditContext.current().getEntityIdentifierToAuditItems().get(key);
                //                methodAuditLog += (key + ":" + LINE_SEPARATOR);
                int index = 0;
                for (String fieldKey : auditItem.getAuditFields().keySet()) {
                    index++;
                    String auditLog = getDefaultChangeLog(auditItem.getAuditFields().get(fieldKey));
                    if (StringUtils.isNotBlank(auditLog)) {
                        methodAuditLog += (auditLog + (index != auditItem.getAuditFields().keySet().size() ? LINE_SEPARATOR : StringUtils.EMPTY_STRING));
                    }
                }
            }
            LOGGER.info("Persisiting audit info for method {}", AuditContext.current().getBelongsToMethod());
            auditService.saveAuditLog(AuditContext.current().getBelongsToMethod(), methodAuditLog, AuditContext.current().getEntityIdentifierToAuditItems());
            // After committing transaction and persisting audit changes merge context into parent method context
            if (AuditContext.current().getParentContext() != null) {
                LOGGER.info("Merging context of method {} to parent method", AuditContext.current().getBelongsToMethod(),
                        AuditContext.current().getParentContext().getBelongsToMethod());
                for (Iterator<String> iterator = AuditContext.current().getMethodVarsMap().keySet().iterator(); iterator.hasNext();) {
                    String key = iterator.next();
                    /**
                     * Field values from a method called inside a method will be available as
                     * methodName.entityName.fieldName.oldValue and like wise newValue
                     */
                    AuditContext.current().getParentContext().getMethodVarsMap().put(AuditContext.current().getBelongsToMethod() + "." + key,
                            AuditContext.current().getMethodVarsMap().get(key));
                }
                AuditContext.current().getParentContext().getMethodVarsMap().putAll(AuditContext.current().getMethodVarsMap());
                AuditContext.setAuditContext(AuditContext.current().getParentContext());
                LOGGER.info("Current audit context set for method {}" + AuditContext.current().getBelongsToMethod());
            }
        }
    }

    private String getDefaultChangeLog(AuditItemField field) {
        if (StringUtils.isNotBlank(field.getOldValue())) {
            return field.getFieldName() + ": Changed from (" + field.getOldValue() + ") to (" + field.getNewValue() + ") ";
        } else {
            if (StringUtils.isNotBlank(field.getNewValue())) {
                return field.getFieldName() + ": Set to (" + field.getNewValue() + ") ";
            } else {
                return StringUtils.EMPTY_STRING;
            }
        }
    }

    /* (non-Javadoc)
     * @see org.springframework.transaction.support.TransactionSynchronizationAdapter#afterCompletion(int)
     */
    @Override
    public void afterCompletion(int status) {
        LOGGER.info("Transaction committed with status {}", status == STATUS_COMMITTED ? "COMMITTED" : "ROLLED_BACK");
        if (AuditContext.current().getParentContext() == null) {
            AuditContext.destory();
        }
    }
}
