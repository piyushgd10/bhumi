/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Aug-2013
 *  @author sunny
 */
package com.unifier.dao.export;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.export.config.ExportConfig;
import com.uniware.core.view.View;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;

public interface IExportMao {

    <T> List<T> executeQuery(Query query, Class<T> clazz);

    List<?> executeQuery(Query query, Pageable pagination, Class<?> clazz);

    List<Map<String, Object>> executeQueryAndReturnMap(Query query, Pageable pagination, String collectionName);

    long executeCountQuery(Query query, Class<?> clazz);

    long executeCountQuery(Query query, String collectionName);

    void markViewDirty(Set<String> saleOrderCodeSet, String viewCollectionName);

    void updateViews(Map<View, List<Map<String, Object>>> collectionNameToDataMap);

    long getNonDirtyRecordsCount(ExportConfig exportConfig, GetDatatableResultRequest request, Map<String, Object> filterValues);
}
