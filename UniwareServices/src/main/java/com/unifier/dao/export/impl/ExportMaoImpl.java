/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.unifier.dao.export.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.DocumentCallbackHandler;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.unifier.core.annotation.Level;
import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.export.config.ExportFilter;
import com.unifier.core.queryParser.GenerateNoSqlQueryRequest;
import com.unifier.core.queryParser.IQueryParser;
import com.unifier.core.queryParser.nosql.NoSqlCriteria;
import com.unifier.core.queryParser.nosql.NoSqlQuery;
import com.unifier.core.queryParser.nosql.mongo.GenerateMongoQueryResponse;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.export.IExportMao;
import com.uniware.core.utils.UserContext;
import com.uniware.core.view.View;

/**
 * @author Sunny
 */
@Repository
public class ExportMaoImpl implements IExportMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Autowired
    private IQueryParser    mongoQueryParser;

    public <T> List<T> executeQuery(Query query, Class<T> clazz) {
        return mongoOperations.find(query, clazz);
    }

    @Override
    public List<?> executeQuery(Query query, Pageable pagination, Class<?> clazz) {
        return mongoOperations.find(query.with(pagination), clazz);
    }

    @Override
    public List<Map<String, Object>> executeQueryAndReturnMap(Query query, Pageable pagination, String collectionName) {
        final List<Map<String, Object>> resultList = new ArrayList<>();
        mongoOperations.executeQuery(query.with(pagination), collectionName, new DocumentCallbackHandler() {
            @Override
            public void processDocument(DBObject dbObject) throws MongoException, DataAccessException {
                resultList.add(dbObject.toMap());
            }
        });
        return resultList;
    }

    @Override
    public long executeCountQuery(Query query, Class<?> clazz) {
        return mongoOperations.count(query, clazz);
    }

    @Override
    public long executeCountQuery(Query query, String collectionName) {
        return mongoOperations.count(query, collectionName);
    }

    /**
     * Mark the set of sale orders dirty (viewDirty = true) in mongodb in specified collection. This method assumes name
     * of field storing tenantCode is "tenant" and one storing sale order code is "saleOrderNum". Specific to
     * {@link com.uniware.core.view.View#SHIPPING_PACKAGE_VIEW} as of now.
     * 
     * @param saleOrderCodeSet
     * @param viewCollectionName
     */
    @Override
    public void markViewDirty(Set<String> saleOrderCodeSet, String viewCollectionName) {
        Query query = new Query(Criteria.where("tenant").is(UserContext.current().getTenant().getCode()).and("saleOrderNum").in(saleOrderCodeSet));
        Update update = new Update().set("viewDirty", true);
        mongoOperations.updateMulti(query, update, viewCollectionName);
    }

    /**
     * Upsert given data for given dirty sale order into view. This method assumes name of field storing sale order code
     * is "saleOrderNum". Specific to {@link com.uniware.core.view.View#SHIPPING_PACKAGE_VIEW} as of now.
     * 
     * @param collectionNameToDataMap
     */
    @Override
    public void updateViews(Map<View, List<Map<String, Object>>> collectionNameToDataMap) {
        for (Map.Entry<View, List<Map<String, Object>>> collectionLevelEntry : collectionNameToDataMap.entrySet()) {
            View view = collectionLevelEntry.getKey();
            DBCollection collection = mongoOperations.getCollection(view.getCollectionName());
            for (Map<String, Object> saleOrderDataMap : collectionLevelEntry.getValue()) {
                DBObject document = new BasicDBObject();
                Object currentDocumentId = null;
                DBObject currentDocument = getDirtyViewDocument(view, saleOrderDataMap.get(view.getUniqueField()).toString(), view.getCollectionName());
                if (currentDocument != null) {
                    currentDocumentId = currentDocument.get("_id");
                }
                for (Map.Entry<String, Object> dataEntry : saleOrderDataMap.entrySet()) {
                    // TODO: fix this. SQL gives BigInteger 1 or 0. We need boolean.
                    if (dataEntry.getValue() instanceof BigInteger) {
                        dataEntry.setValue(StringUtils.parseBoolean(String.valueOf(dataEntry.getValue())));
                    }
                    document.put(dataEntry.getKey(), dataEntry.getValue());
                }
                document.put("_id", currentDocumentId);
                document.put("viewDirty", false);
                collection.save(document); // this will upsert
            }
        }
    }

    private DBObject getDirtyViewDocument(View view, String uniqueFieldValue, String collectionName) {
        Criteria c = Criteria.where("tenant").is(UserContext.current().getTenant().getCode()).and(view.getUniqueField()).is(uniqueFieldValue);
        if (view.getLevel().equals(Level.FACILITY)) {
            c.and("facilityId").is(UserContext.current().getFacilityId());
        }
        Query query = new Query(c);
        final DBObject[] result = new DBObject[1];
        mongoOperations.executeQuery(query, collectionName, new DocumentCallbackHandler() {
            @Override
            public void processDocument(DBObject dbObject) throws MongoException, DataAccessException {
                result[0] = dbObject;
            }
        });
        return result[0];
    }

    @Override
    public long getNonDirtyRecordsCount(ExportConfig exportConfig, GetDatatableResultRequest request, Map<String, Object> filterValues) {
        GenerateNoSqlQueryRequest mongoQueryRequest = new GenerateNoSqlQueryRequest(true);
        filterValues.put("viewDirty", false);
        mongoQueryRequest.getQueryParams().putAll(filterValues);
        NoSqlQuery mongoQuery = new NoSqlQuery();
        mongoQuery.setEntity(exportConfig.getViewCollectionName());
        // set filters
        if (request.getFilters() != null) {
            for (WsExportFilter wsExportFilter : request.getFilters()) {
                ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                NoSqlCriteria noSqlCriteria = NoSqlCriteria.compile(exportFilter.getConditionForView());
                mongoQuery.getCriteria().add(noSqlCriteria);
            }
        }
        for (ExportFilter hiddenFilter : exportConfig.getHiddenFilters()) {
            NoSqlCriteria noSqlCriteria = NoSqlCriteria.compile(hiddenFilter.getConditionForView());
            mongoQuery.getCriteria().add(noSqlCriteria);
        }
        mongoQuery.getCriteria().add(NoSqlCriteria.compile("{'name' : 'viewDirty', 'operator' : 'IS', 'value' : '#{#viewDirty}'}"));
        /**
         * // set sorters if (request.getSortColumns() != null) { for (WsSortColumn wsSortColumn :
         * request.getSortColumns()) { GenerateQueryRequest.Sort sort = new GenerateQueryRequest.Sort();
         * sort.setColumn(wsSortColumn.getColumn()); sort.setDescending(wsSortColumn.isDescending());
         * mongoQueryRequest.getSortList().add(sort); // TODO: check below NoSqlSorter noSqlSorter = new
         * NoSqlSorter(wsSortColumn.getColumn(), wsSortColumn.getSortDirection().trim());
         * mongoQuery.getSorters().add(noSqlSorter); } }
         **/
        mongoQueryRequest.setNoSqlQuery(mongoQuery);
        /**
         * mongoQueryRequest.setStart((request.getStart() - dirtyRecordsCount) > 0 ? (request.getStart() -
         * dirtyRecordsCount) : 0); mongoQueryRequest.setNumberOfResults(dirtyRecordsCount + request.getNoOfResults());
         **/
        GenerateMongoQueryResponse mongoQueryRes = (GenerateMongoQueryResponse) mongoQueryParser.generateQuery(mongoQueryRequest);
        return executeCountQuery(mongoQueryRes.getQuery(), mongoQuery.getEntity());
    }
}
