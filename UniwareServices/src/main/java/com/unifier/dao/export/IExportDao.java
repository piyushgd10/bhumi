/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.unifier.dao.export;

import java.util.List;
import java.util.Map;

import com.unifier.core.entity.ExportSubscriber;
import org.hibernate.Query;

import com.unifier.core.api.datatable.WsSortColumn;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.export.config.ExportConfig;

/**
 * @author singla
 */
public interface IExportDao {

    /**
     * @param query
     * @return
     */
    List<Object[]> executeAnonymousQuery(String query);

    List<Map<String, Object>> executeAnonymousQuery(Query query);

    /**
     * @param exportJob
     */
    ExportJob addExportJob(ExportJob exportJob);

    List<ExportJob> getUserExports(String username);

    /**
     * @param exportJobId
     * @return
     */
    ExportJob getExportJobById(String exportJobId);

    /**
     * @param name
     * @return
     */
    ExportJobType getExportJobTypeByName(String name);

    /**
     * @param frequency
     * @return
     */
    List<ExportJob> getExportJobByFrequency(String frequency);

    /**
     * @return
     */
    ExportSubscriber getExportSubscribers(String exportJobId);

    /**
     * @return
     */
    List<ExportJobType> getExportJobTypes();

    /**
     * @param username
     * @return
     */
    List<ExportSubscriber> getExportJobsBySubscriber(String username);

    /**
     * @param exportJobId
     * @return
     */
    ExportSubscriber getExportSubscriber(String exportJobId, String username);

    /**
     * @param subscriber
     * @return
     */
    ExportSubscriber addExportSubscriber(ExportSubscriber subscriber);

    /**
     * @return
     */
    List<ExportJob> getRecurrentExportJobs();

    /**
     * @param exportJobId
     * @return
     */
    void editExportJob(String exportJobId);

    public List<ExportJob> getExportSubscription(String exportJobId);

    Map<String, Object> getFilterParameters(ExportConfig exportConfig, List<WsExportFilter> filters);

    ExportJob updateExportJob(ExportJob exportJob);

    Query prepareQuery(ExportConfig exportConfig, List<String> columns, List<WsExportFilter> filters, Map<String, Object> filterParameters, List<WsSortColumn> sortColumns,
            boolean isDatatableRequest);

    void deleteExportSubscription(String exportJobId);

    Query prepareResultCountQuery(ExportConfig exportConfig, List<WsExportFilter> filters, Map<String, Object> filterParameters, boolean isDatatableRequest);

    List<Object[]> executeAnonymousQuery(String query, Map<String, Object> parameters);

    void deleteExportSubscribers(String exportJobId);

    ExportJobType createExportJobType(ExportJobType exportJobType);

}
