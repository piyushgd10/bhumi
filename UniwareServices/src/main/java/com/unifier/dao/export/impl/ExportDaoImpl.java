/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.unifier.dao.export.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.unifier.core.api.datatable.WsSortColumn;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.ExportJob;
import com.unifier.core.entity.ExportJobType;
import com.unifier.core.entity.ExportSubscriber;
import com.unifier.core.export.config.ExportColumn;
import com.unifier.core.export.config.ExportConfig;
import com.unifier.core.export.config.ExportFilter;
import com.unifier.core.table.config.DatatableConfig;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.dao.export.IExportDao;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;

/**
 * @author singla
 */
@Repository
@SuppressWarnings("unchecked")
@DynamicUpdate
public class ExportDaoImpl implements IExportDao {

    @Autowired
    @Qualifier(value = "sessionFactory")
    private SessionFactory  sessionFactory;

    @Autowired
    @Qualifier(value = "sessionFactoryReplication")
    private SessionFactory  sessionFactoryReplication;

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    /* (non-Javadoc)
     * @see com.uniware.dao.export.IExportDao#executeAnonymousQuery(java.lang.String)
     */
    @Override
    public List<Object[]> executeAnonymousQuery(String query) {
        return sessionFactory.getCurrentSession().createSQLQuery(query).list();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<Object[]> executeAnonymousQuery(String querySql, Map<String, Object> parameters) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(querySql);
        for (String namedParameter : query.getNamedParameters()) {
            Object value = parameters.get(namedParameter);
            if (value != null) {
                if (value instanceof List) {
                    query.setParameterList(namedParameter, (Collection) value);
                } else {
                    query.setParameter(namedParameter, value);
                }
            }
        }
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.export.IExportDao#addExportJob(com.uniware.core.entity.ExportJob)
     */
    @Override
    public ExportJob addExportJob(ExportJob exportJob) {
        exportJob.setTenantCode(UserContext.current().getTenant().getCode());
        if (UserContext.current().getFacilityId() != null) {
            exportJob.setFacilityCode(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode());
        }
        mongoOperations.save(exportJob);
        return exportJob;
    }

    @Override
    public List<ExportJob> getUserExports(String username) {
        Date date = DateUtils.getCurrentTime();
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000L);
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("username").is(username).and("created").gte(date);
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(criteria).with(new Sort(Sort.Direction.DESC, "created")), ExportJob.class);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.export.IExportDao#getExportJobById(java.lang.Integer)
     */
    @Override
    public ExportJob getExportJobById(String exportJobId) {
        return mongoOperations.findById(exportJobId, ExportJob.class);
    }

    @Override
    public ExportJob updateExportJob(ExportJob exportJob) {
        mongoOperations.save(exportJob);
        return exportJob;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Query prepareQuery(ExportConfig exportConfig, List<String> columns, List<WsExportFilter> filters, Map<String, Object> filterParameters, List<WsSortColumn> sortColumns,
            boolean isDatatableRequest) {
        StringBuilder builder = new StringBuilder("select ");
        Map<String, String> customFieldQueryReplacements = new HashMap<String, String>();
        for (String exportColumnId : columns) {
            ExportColumn exportColumn = exportConfig.getExportColumnById(exportColumnId);
            if (!exportColumn.isCalculated()) {
                builder.append(exportColumn.getValue()).append(" as ").append(exportColumn.getId()).append(',');
                if (exportColumn.isNonSerializableCustomField()) {
                    if (!customFieldQueryReplacements.containsKey(exportColumn.getEntity())) {
                        String customFieldTableAlias = exportColumn.getTableAlias() + "cf";
                        customFieldQueryReplacements.put(exportColumn.getEntity(), " left join custom_field_value " + customFieldTableAlias + " on (" + customFieldTableAlias
                                + ".identifier = " + exportColumn.getTableAlias() + ".id and " + customFieldTableAlias + ".entity = '" + exportColumn.getEntity() + "') ");
                    }
                }
            }
        }

        builder.deleteCharAt(builder.length() - 1);
        if (columns.size() == 1) {
            // as hibernate will return the element directly instead of array in case of single return alias
            builder.append(",1");
        }

        //
        // String manipulation to accommodate customFieldValue joins.
        // order o, item_type it where blahblah ==> order o, item_type it left join custom_field_value itcf on it.id = itcf.identifier and itcf.entity = 'item_type' where blahblah
        // order o, item_type it ==> order o, item_type it left join custom_field_value itcf on it.id = itcf.identifier and itcf.entity = 'item_type'
        //
        String querySuffix = exportConfig.getQueryString();
        if (!customFieldQueryReplacements.isEmpty()) {
            String regexToBeReplaced = "(?i)\\swhere";
            Pattern p = Pattern.compile(regexToBeReplaced);
            Matcher m = p.matcher(querySuffix);
            if (m.find()) {
                querySuffix = m.replaceFirst(StringUtils.join(' ', customFieldQueryReplacements.values()) + " where ");
            } else {
                querySuffix += StringUtils.join(' ', customFieldQueryReplacements.values());
            }
        }
        builder.append(' ').append(querySuffix);
        applyFiltersAndGroupBy(exportConfig, filters, builder, false);
        boolean orderBy = false;
        if (sortColumns != null && sortColumns.size() > 0) {
            for (WsSortColumn wsSortColumn : sortColumns) {
                ExportColumn sortColumn = exportConfig.getExportColumnById(wsSortColumn.getColumn());
                if (sortColumn != null) {
                    if (!orderBy) {
                        builder.append(" order by ");
                        orderBy = true;
                    }
                    builder.append(sortColumn.getId()).append(wsSortColumn.getSortDirection()).append(',');
                }
            }
        }
        if (orderBy) {
            builder.setLength(builder.length() - 1);
        } else if (ExportJobType.Type.TABLE.name().equalsIgnoreCase(exportConfig.getType()) && StringUtils.isNotBlank(((DatatableConfig) exportConfig).getDefaultSortBy())) {
            builder.append(" order by ").append(((DatatableConfig) exportConfig).getDefaultSortBy());
        }
        String queryString = builder.toString();
        Query query;
        SessionFactory sf = sessionFactory;
        boolean isDatatableViaReplicationEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDatatableViaReplicationEnabled();
        boolean isExportViaReplicationEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isExportViaReplicationEnabled();
        if ((isDatatableRequest && isDatatableViaReplicationEnabled) || (!isDatatableRequest && isExportViaReplicationEnabled)) {
            sf = sessionFactoryReplication;
        }
        query = sf.getCurrentSession().createSQLQuery(queryString);
        for (String namedParameter : query.getNamedParameters()) {
            if (filterParameters != null && filterParameters.containsKey(namedParameter)) {
                Object value = filterParameters.get(namedParameter);
                if (value instanceof List) {
                    query.setParameterList(namedParameter, (Collection) value);
                } else {
                    query.setParameter(namedParameter, value);
                }
            }
        }
        query.setReadOnly(true);
        query.setCacheable(false);
        return query;
    }

    private List<ExportFilter> applyFiltersAndGroupBy(ExportConfig exportConfig, List<WsExportFilter> filters, StringBuilder builder, boolean forCount) {
        boolean filtersApplicable = filters != null;
        boolean hiddenFiltersApplicable = exportConfig.getHiddenFilters().size() > 0;
        List<ExportFilter> applicableGroupFilters = new ArrayList<>();
        if (filtersApplicable || hiddenFiltersApplicable || exportConfig.isServeFromView()) {
            StringJoiner filterJoiner = new StringJoiner(" and ");
            if (filtersApplicable) {
                filtersApplicable = false;
                for (WsExportFilter wsExportFilter : filters) {
                    ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                    if (!exportFilter.isGroupFilter()) {
                        filtersApplicable = true;
                        filterJoiner.add(exportFilter.getCondition());
                    } else {
                        applicableGroupFilters.add(exportFilter);
                    }
                }
            }
            if (hiddenFiltersApplicable) {
                hiddenFiltersApplicable = false;
                for (ExportFilter hiddenFilter : exportConfig.getHiddenFilters()) {
                    if (!hiddenFilter.isGroupFilter()) {
                        hiddenFiltersApplicable = true;
                        filterJoiner.add(hiddenFilter.getCondition());
                    } else {
                        applicableGroupFilters.add(hiddenFilter);
                    }
                }
            }
            if (exportConfig.isServeFromView()) {
                filterJoiner.add("view_dirty = 1");
            }
            if (filtersApplicable || hiddenFiltersApplicable || exportConfig.isServeFromView()) {
                builder.append(exportConfig.getQueryString().contains(" where ") || exportConfig.getQueryString().contains(" WHERE ") ? " and " : " where ");
                builder.append(filterJoiner.toString());
            }
        }
        if (StringUtils.isNotBlank(exportConfig.getGroupBy()) && (!forCount || exportConfig.getGroupBy().contains(" having ") || applicableGroupFilters.size() > 0)) {
            builder.append(' ').append(exportConfig.getGroupBy());
            StringJoiner filterJoiner = new StringJoiner(" and ");
            if (applicableGroupFilters.size() > 0) {
                if (!exportConfig.getGroupBy().contains(" having ")) {
                    builder.append(" having ");
                }
                applicableGroupFilters.forEach(filter -> filterJoiner.add(filter.getCondition()));
                builder.append(filterJoiner.toString());
            }
        }
        return applicableGroupFilters;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Query prepareResultCountQuery(ExportConfig exportConfig, List<WsExportFilter> filters, Map<String, Object> filterParameters, boolean isDatatableRequest) {
        StringBuilder builder = new StringBuilder();
        Map<String, String> customFieldQueryReplacements = new HashMap<String, String>();
        exportConfig.getExportColumns().stream().filter(exportColumn -> !exportColumn.isCalculated()).forEach((ExportColumn exportColumn) -> {
            if (exportColumn.isNonSerializableCustomField()) {
                if (!customFieldQueryReplacements.containsKey(exportColumn.getEntity())) {
                    String customFieldTableAlias = exportColumn.getTableAlias() + "cf";
                    customFieldQueryReplacements.put(exportColumn.getEntity(), " left join custom_field_value " + customFieldTableAlias + " on (" + customFieldTableAlias
                            + ".identifier = " + exportColumn.getTableAlias() + ".id and " + customFieldTableAlias + ".entity = '" + exportColumn.getEntity() + "') ");
                }
            }
        });

        //
        // String manipulation to accommodate customFieldValue joins.
        // order o, item_type it where blahblah ==> order o, item_type it left join custom_field_value itcf on it.id = itcf.identifier and itcf.entity = 'item_type' where blahblah
        // order o, item_type it ==> order o, item_type it left join custom_field_value itcf on it.id = itcf.identifier and itcf.entity = 'item_type'
        //
        String querySuffix = exportConfig.getResultCountQuery() != null ? exportConfig.getResultCountQuery() : exportConfig.getQueryString();
        if (!customFieldQueryReplacements.isEmpty()) {
            String regexToBeReplaced = "(?i)\\swhere";
            Pattern p = Pattern.compile(regexToBeReplaced);
            Matcher m = p.matcher(querySuffix);
            if (m.find()) {
                querySuffix = m.replaceFirst(StringUtils.join(' ', customFieldQueryReplacements.values()) + " where ");
            } else {
                querySuffix += StringUtils.join(' ', customFieldQueryReplacements.values());
            }
        }
        builder.append(' ').append(querySuffix);
        applyFiltersAndGroupBy(exportConfig, filters, builder, true);
        String queryString = builder.toString();
        if (queryString.contains(" having ")) {
            queryString = "select count(*) as count from (" + queryString + ") t";
        }
        Query query;
        SessionFactory sf = sessionFactory;
        boolean isDatatableViaReplicationEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDatatableViaReplicationEnabled();
        boolean isExportViaReplicationEnabled = CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isExportViaReplicationEnabled();
        if ((isDatatableRequest && isDatatableViaReplicationEnabled) || (!isDatatableRequest && isExportViaReplicationEnabled)) {
            sf = sessionFactoryReplication;
        }
        query = sf.getCurrentSession().createSQLQuery(queryString);
        for (String namedParameter : query.getNamedParameters()) {
            if (filterParameters != null && filterParameters.containsKey(namedParameter)) {
                Object value = filterParameters.get(namedParameter);
                if (value instanceof List) {
                    query.setParameterList(namedParameter, (Collection) value);
                } else {
                    query.setParameter(namedParameter, value);
                }
            }
        }
        query.setReadOnly(true);
        query.setCacheable(false);
        return query;
    }

    @Override
    public Map<String, Object> getFilterParameters(ExportConfig exportConfig, List<WsExportFilter> filters) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        if (filters != null) {
            for (WsExportFilter wsExportFilter : filters) {
                ExportFilter exportFilter = exportConfig.getExportFilterById(wsExportFilter.getId());
                if (exportFilter != null) {
                    switch (exportFilter.getType()) {
                        case BOOLEAN:
                            if (exportFilter.isNonSerializableCustomField()) {
                                parameters.put(exportFilter.getId(), wsExportFilter.getChecked().toString());
                            } else {
                                parameters.put(exportFilter.getId(), wsExportFilter.getChecked());
                            }
                            break;
                        case TEXT:
                            Object value = wsExportFilter.getText();
                            processTextFilter(parameters, exportFilter, value);
                            break;
                        case DATERANGE:
                            if (exportFilter.isNonSerializableCustomField()) {
                                parameters.put(exportFilter.getId(), wsExportFilter.getDateRange());
                                parameters.put(exportFilter.getId() + "Start", String.valueOf(wsExportFilter.getDateRange().getStart().getTime()));
                                parameters.put(exportFilter.getId() + "End", String.valueOf(wsExportFilter.getDateRange().getEnd().getTime()));
                            } else {
                                parameters.put(exportFilter.getId(), wsExportFilter.getDateRange());
                                parameters.put(exportFilter.getId() + "Start", wsExportFilter.getDateRange().getStart());
                                parameters.put(exportFilter.getId() + "End", wsExportFilter.getDateRange().getEnd());
                            }
                            break;
                        case DATETIME:
                            parameters.put(exportFilter.getId(), wsExportFilter.getDateTime());
                            break;
                        case SELECT:
                            parameters.put(exportFilter.getId(), wsExportFilter.getSelectedValue());
                            break;
                        case MULTISELECT:
                            parameters.put(exportFilter.getId(), wsExportFilter.getSelectedValues());
                    }
                }
            }
        }

        for (ExportFilter hiddenFilter : exportConfig.getHiddenFilters()) {
            switch (hiddenFilter.getType()) {
                case TEXT:
                    processTextFilter(parameters, hiddenFilter, hiddenFilter.getHiddenValue());
                    break;
            }
        }
        return parameters;
    }

    private void processTextFilter(Map<String, Object> parameters, ExportFilter exportFilter, Object value) {
        if (exportFilter.getValueExpression() != null) {
            Map<String, Object> contextParams = new HashMap<>();
            contextParams.put(exportFilter.getId(), value);
            value = exportFilter.getValueExpression().evaluate(contextParams);
        }
        parameters.put(exportFilter.getId(), value);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.export.IExportDao#executeAnonymousQuery(org.hibernate.Query)
     */
    @Override
    public List<Map<String, Object>> executeAnonymousQuery(Query query) {
        return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
    }

    @Override
    public ExportJobType getExportJobTypeByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ExportJobType ejt where ejt.tenant.id=:tenantId and ejt.name=:name");
        query.setParameter("name", name);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (ExportJobType) query.uniqueResult();
    }

    @Override
    public List<ExportJob> getExportJobByFrequency(String frequency) {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("frequency").is(frequency).and("enabled").is(true);
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(criteria), ExportJob.class);
    }

    //Check for user enabled
    @Override
    public ExportSubscriber getExportSubscribers(String exportJobId) {
        Criteria criteria = Criteria.where("subscribed").is(true).and("exportJobId").is(exportJobId);
        return mongoOperations.findOne(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);
    }

    @Override
    public List<ExportJobType> getExportJobTypes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ExportJobType ejt where ejt.tenant.id=:tenantId order by name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<ExportSubscriber> getExportJobsBySubscriber(String username) {
        List<ExportSubscriber> exportSubscribers = Collections.synchronizedList(new ArrayList<ExportSubscriber>());
        Criteria criteria = Criteria.where("usernames").is(username);
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);
//        exportSubscribers.addAll();
//        ExportJob exportJob = null;
//        Iterator<ExportSubscriber> subscriberIterator = exportSubscribers.iterator();
//        while (subscriberIterator.hasNext()) {
//            ExportSubscriber exportSubscriber = subscriberIterator.next();
//            Criteria criteria1 = Criteria.where("frequency").is(ExportJob.Frequency.ONETIME.name()).and("enabled").is(true).and("id").is(exportSubscriber.getExportJobId());
//            exportJob = mongoOperations.findOne(new org.springframework.data.mongodb.core.query.Query(criteria1), ExportJob.class);
//            if (exportJob != null) {
//                subscriberIterator.remove();
//            }
//            exportJob = null;
//        }
//
//        return exportSubscribers;
    }

    @Override
    public ExportSubscriber getExportSubscriber(String exportJobId, String username) {
        Criteria criteria = Criteria.where("exportJobId").is(exportJobId).and("usernames").is(username);
        return mongoOperations.findOne(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);

    }

    @Override
    public ExportSubscriber addExportSubscriber(ExportSubscriber subscriber) {
        Criteria criteria = Criteria.where("exportJobId").is(subscriber.getExportJobId());
        ExportSubscriber exportSubscriber = mongoOperations.findOne(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);
        if (exportSubscriber == null) {
            mongoOperations.save(subscriber);
        } else {
            subscriber.getUsernames().addAll(exportSubscriber.getUsernames());
            Update update = new Update().set("usernames", subscriber.getUsernames());
            mongoOperations.updateFirst(new org.springframework.data.mongodb.core.query.Query(criteria), update, ExportJob.class);
        }
        return subscriber;
    }

    @Override
    public List<ExportJob> getRecurrentExportJobs() {
        Criteria criteria = Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("enabled").is(true).and("frequency").ne(
                ExportJob.Frequency.ONETIME.name()).orOperator(
                Criteria.where("facilityCode").is(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode()), Criteria.where("facilityCode").is(null));
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(criteria), ExportJob.class);
    }

    @Override
    public void editExportJob(String exportJobId) {
        org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query(Criteria.where("id").is(exportJobId));
        Update update = new Update().set("enabled", false);
        mongoOperations.updateFirst(query, update, ExportJob.class);
    }

    @Override
    public List<ExportJob> getExportSubscription(String exportJobId) {
        Criteria criteria = Criteria.where("id").is(exportJobId);
        return mongoOperations.find(new org.springframework.data.mongodb.core.query.Query(criteria), ExportJob.class);
    }

    @Override
    public void deleteExportSubscription(String exportJobId) {
        Criteria criteria = Criteria.where("id").is(exportJobId);
        mongoOperations.remove(new org.springframework.data.mongodb.core.query.Query(criteria), ExportJob.class);
        criteria = Criteria.where("exportJobId").is(exportJobId);
        mongoOperations.remove(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);
    }

    @Override
    public void deleteExportSubscribers(String exportJobId) {
        Criteria criteria = Criteria.where("exportJobId").is(exportJobId);
        mongoOperations.remove(new org.springframework.data.mongodb.core.query.Query(criteria), ExportSubscriber.class);
    }

    @Override
    public ExportJobType createExportJobType(ExportJobType exportJobType) {
        sessionFactory.getCurrentSession().persist(exportJobType);
        return exportJobType;
    }

}
