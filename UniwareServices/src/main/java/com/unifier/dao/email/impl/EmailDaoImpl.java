/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.unifier.dao.email.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.EmailTemplate;
import com.unifier.dao.email.IEmailDao;
import com.uniware.core.utils.UserContext;

@SuppressWarnings("unchecked")
@Repository
public class EmailDaoImpl implements IEmailDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public EmailTemplate getEmailTemplateByType(String type) {
        Query query = sessionFactory.getCurrentSession().createQuery("from EmailTemplate where type=:type and tenant.id=:tenantId");
        query.setParameter("type", type);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (EmailTemplate) query.uniqueResult();
    }

    @Override
    public EmailTemplate updateEmailTemplate(EmailTemplate template) {
        return (EmailTemplate) sessionFactory.getCurrentSession().merge(template);
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.startup.IStartupDao#getEmailTemplates()
     */
    @Override
    public List<EmailTemplate> getEmailTemplates() {
        Query query = sessionFactory.getCurrentSession().createQuery("from EmailTemplate where tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }
}
