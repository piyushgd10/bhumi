/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Apr-2013
 *  @author unicom
 */
package com.unifier.dao.email.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.vo.BouncedEmailVO;
import com.unifier.dao.email.IEmailMao;

@Repository
public class EmailMaoImpl implements IEmailMao {

	@Autowired
	@Qualifier(value = "commonMongo")
	private MongoOperations mongoOperations;

	@Override
	public void save(BouncedEmailVO emailVO) {
		mongoOperations.save(emailVO);
	}

	@Override
	public BouncedEmailVO getBouncedEmailVO(String email) {
		return mongoOperations.findOne(new Query(Criteria.where("email").is(email)), BouncedEmailVO.class);
	}
}
