/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 3, 2013
 *  @author sunny
 */
package com.unifier.dao.email;

import com.unifier.core.vo.BouncedEmailVO;

public interface IEmailMao {

    void save(BouncedEmailVO emailVO);

    BouncedEmailVO getBouncedEmailVO(String email);

}
