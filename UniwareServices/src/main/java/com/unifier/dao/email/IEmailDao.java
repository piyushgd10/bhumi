/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 18, 2012
 *  @author praveeng
 */
package com.unifier.dao.email;

import java.util.List;

import com.unifier.core.entity.EmailTemplate;

public interface IEmailDao {

    EmailTemplate getEmailTemplateByType(String type);

    EmailTemplate updateEmailTemplate(EmailTemplate template);

    List<EmailTemplate> getEmailTemplates();
}
