/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.dao.comments;

import java.util.List;

import com.unifier.core.entity.UserComment;

public interface ICommentsDao {

    UserComment addUserComment(UserComment userComment);

    List<UserComment> getUserComments(String referenceIdentifier);

    UserComment getUserCommentById(Integer commentId);

    int getUserCommentsCount(String referenceIdentifier);

}
