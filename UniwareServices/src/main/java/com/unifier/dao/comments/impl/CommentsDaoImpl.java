/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 29, 2012
 *  @author singla
 */
package com.unifier.dao.comments.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.UserComment;
import com.unifier.dao.comments.ICommentsDao;
import com.uniware.core.utils.UserContext;

@Repository
public class CommentsDaoImpl implements ICommentsDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public UserComment addUserComment(UserComment userComment) {
        userComment.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(userComment);
        return userComment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UserComment> getUserComments(String referenceIdentifier) {
        Query query;
        query = sessionFactory.getCurrentSession().createQuery(
                "select uc from UserComment uc where uc.tenant.id = :tenantId and uc.referenceIdentifier = :referenceIdentifier and deleted=0 order by uc.updated desc");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("referenceIdentifier", referenceIdentifier);
        return query.list();
    }

    @Override
    public UserComment getUserCommentById(Integer commentId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select uc from UserComment uc where uc.tenant.id=:tenantId and uc.id = :commentId and deleted = 0");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("commentId", commentId);
        return (UserComment) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public int getUserCommentsCount(String referenceIdentifier){
        Query query;
        query = sessionFactory.getCurrentSession().createQuery(
                "select count(uc) from UserComment uc where uc.tenant.id = :tenantId and uc.referenceIdentifier = :referenceIdentifier and deleted=0 order by uc.updated desc");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("referenceIdentifier", referenceIdentifier);
        return ((Long) query.uniqueResult()).intValue();
    }
}
