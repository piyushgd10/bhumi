/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/08/14
 *  @author amit
 */

package com.unifier.dao.application.impl;

import java.util.List;

import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UICustomListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.dao.application.IApplicationSetupMao;
import com.uniware.core.vo.ProductDefaultVO;

@Repository(value = "applicationSetupMao")
public class ApplicationSetupMaoImpl implements IApplicationSetupMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commonMongoOperations;

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations tenantSpecificMongoOperations;

    @Override
    public List<ProductDefaultVO> getProductDefaults() {
        return commonMongoOperations.findAll(ProductDefaultVO.class);
    }

    @Override
    public List<UICustomListVO> getUICustomLists() {
        return tenantSpecificMongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode())), UICustomListVO.class);
    }

}