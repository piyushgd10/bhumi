/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.dao.application;

import java.util.List;

import com.uniware.core.vo.ProductDefaultVO;
import com.uniware.core.vo.UICustomListVO;

public interface IApplicationSetupMao {

    List<ProductDefaultVO> getProductDefaults();

    List<UICustomListVO> getUICustomLists();
}
