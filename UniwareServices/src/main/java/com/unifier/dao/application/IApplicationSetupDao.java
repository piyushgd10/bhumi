/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.dao.application;

import java.util.List;

import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.ProductFeature;
import com.unifier.core.entity.ProductRole;
import com.unifier.core.entity.ProductSource;
import com.unifier.core.entity.SystemConfig;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.State;
import com.uniware.core.entity.UserDatatableView;

public interface IApplicationSetupDao {

    /**
     * @param name
     * @return
     */
    SystemConfig getSystemConfigByName(String name);

    /**
     * @param systemConfiguration
     * @return
     */
    SystemConfig updateSystemConfig(SystemConfig systemConfiguration);

    SystemConfig addSystemConfig(SystemConfig systemConfiguration);

    /**
     * @return
     */
    List<SystemConfig> getSystemConfiguration();

    List<FacilityAllocationRule> getFacilityAllocationRules();

    List<SystemConfig> getSystemConfigs();

    List<SystemConfig> getTenantSystemConfigs();

    List<UserDatatableView> getDatatableViews();

    List<EnvironmentProperty> getEnvironmentProperties();

    List<State> getStates();

    List<Country> getCountries();

    List<Section> getSections();

    List<PaymentMethod> getPaymentMethods();

    List<Product> getAllProducts();

    List<ProductFeature> getProductFeatures();

    List<ShippingPackageStatus> getAllShippingPackageStatus();

    List<SaleOrderStatus> getAllSaleOrderStatus();

    List<SaleOrderItemStatus> getAllSaleOrderItemStatus();

    Product getProductByCode(String code);

    List<ProductSource> getProductSources(String productCode);

    List<ProductRole> getProductRoles();

}
