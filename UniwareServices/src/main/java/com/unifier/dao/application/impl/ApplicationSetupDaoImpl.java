/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, 23-Jul-2012
 *  @author praveeng
 */
package com.unifier.dao.application.impl;

import com.unifier.core.entity.EnvironmentProperty;
import com.unifier.core.entity.ProductFeature;
import com.unifier.core.entity.ProductRole;
import com.unifier.core.entity.ProductSource;
import com.unifier.core.entity.SystemConfig;
import com.unifier.dao.application.IApplicationSetupDao;
import com.uniware.core.entity.Country;
import com.uniware.core.entity.FacilityAllocationRule;
import com.uniware.core.entity.PaymentMethod;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.SaleOrderItemStatus;
import com.uniware.core.entity.SaleOrderStatus;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.ShippingPackageStatus;
import com.uniware.core.entity.State;
import com.uniware.core.entity.UserDatatableView;
import com.uniware.core.utils.UserContext;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unchecked")
@Repository
public class ApplicationSetupDaoImpl implements IApplicationSetupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SystemConfig getSystemConfigByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from SystemConfig where tenant.id = :tenantId and (facility.id = :facilityId or facility.id is null) and name = :name");
        query.setParameter("name", name);
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (SystemConfig) query.uniqueResult();
    }

    @Override
    public SystemConfig updateSystemConfig(SystemConfig systemConfiguration) {
        systemConfiguration.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().merge(systemConfiguration);
        return systemConfiguration;
    }

    @Override
    public SystemConfig addSystemConfig(SystemConfig systemConfiguration) {
        systemConfiguration.setFacility(UserContext.current().getFacility());
        sessionFactory.getCurrentSession().persist(systemConfiguration);
        return systemConfiguration;
    }

    @Override
    public List<SystemConfig> getSystemConfiguration() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from SystemConfig where tenant.id = :tenantId and (facility.id = :facilityId or facility.id is null) order by groupName, sequence");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<FacilityAllocationRule> getFacilityAllocationRules() {
        Query query = sessionFactory.getCurrentSession().createQuery("from FacilityAllocationRule where tenant.id = :tenantId order by preference");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.startup.IStartupDao#getSystemConfigs()
     */
    @Override
    public List<SystemConfig> getSystemConfigs() {
        Query query = sessionFactory.getCurrentSession().createQuery("from SystemConfig where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<SystemConfig> getTenantSystemConfigs() {
        Query query = sessionFactory.getCurrentSession().createQuery("from SystemConfig where tenant.id = :tenantId and facility.id is null");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public List<UserDatatableView> getDatatableViews() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select udv from UserDatatableView udv ,ProductAccessResource par where udv.accessResourceName = par.accessResource.name and par.product.code = :productCode and udv.tenant.id = :tenantId order by udv.displayOrder");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("productCode", UserContext.current().getTenant().getProduct().getCode());
        return query.list();
    }

    @Override
    public List<EnvironmentProperty> getEnvironmentProperties() {
        return sessionFactory.getCurrentSession().createQuery("from EnvironmentProperty").list();
    }

    @Override
    public List<Section> getSections() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Section where facility.id = :facilityId");
        query.setParameter("facilityId", UserContext.current().getFacilityId());
        return query.list();
    }

    @Override
    public List<State> getStates() {
        return sessionFactory.getCurrentSession().createQuery("from State order by name").list();
    }

    @Override
    public List<Country> getCountries() {
        return sessionFactory.getCurrentSession().createQuery("from Country order by name").list();
    }

    @Override
    public List<PaymentMethod> getPaymentMethods() {
        return sessionFactory.getCurrentSession().createQuery("from PaymentMethod where enabled = 1").list();
    }

    @Override
    public List<Product> getAllProducts() {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct p from Product p");
        List<Product> productList = query.list();
        for (Product p : productList) {
            Hibernate.initialize(p.getProductFeatures());
        }
        return query.list();
    }

    @Override
    public Product getProductByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Product where code = :code");
        query.setParameter("code", code);
        return (Product) query.uniqueResult();
    }

    @Override
    public List<ProductFeature> getProductFeatures() {
        return sessionFactory.getCurrentSession().createQuery("from ProductFeature").list();
    }

    @Override
    public List<ProductSource> getProductSources(String productCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductSource where product.code = :productCode");
        query.setParameter("productCode", productCode);
        return query.list();
    }

    @Override
    public List<ProductRole> getProductRoles() {
        return sessionFactory.getCurrentSession().createQuery("from ProductRole").list();
    }

    @Override
    public List<ShippingPackageStatus> getAllShippingPackageStatus() {
        return sessionFactory.getCurrentSession().createQuery("select sps from ShippingPackageStatus sps left join fetch sps.applicableFor af").list();
    }

    @Override
    public List<SaleOrderStatus> getAllSaleOrderStatus() {
        return sessionFactory.getCurrentSession().createQuery("select sos from SaleOrderStatus sos left join fetch sos.applicableFor af").list();
    }

    @Override
    public List<SaleOrderItemStatus> getAllSaleOrderItemStatus() {
        return sessionFactory.getCurrentSession().createQuery("select sois from SaleOrderItemStatus sois left join fetch sois.applicableFor af").list();
    }
}