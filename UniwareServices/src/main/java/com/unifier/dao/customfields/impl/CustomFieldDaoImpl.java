/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.dao.customfields.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.CustomFieldMetadata;
import com.unifier.dao.customfields.ICustomFieldDao;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

@Repository
public class CustomFieldDaoImpl implements ICustomFieldDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CustomFieldMetadata createCustomFieldMetadata(CustomFieldMetadata customFieldMetadata) {
        sessionFactory.getCurrentSession().persist(customFieldMetadata);
        return customFieldMetadata;
    }

    @Override
    public boolean addCustomField(String queryString) {
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(queryString);
        return query.executeUpdate() > 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CustomFieldMetadata> listAllCustomFields() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomFieldMetadata where tenant.id = :tenantId order by entity, displayName");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Tenant> getAllTenants() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Tenant");
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.startup.IStartupDao#getCustomFieldsMetadata()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<CustomFieldMetadata> getCustomFieldsMetadata() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cfd from CustomFieldMetadata cfd where cfd.enabled = 1 and cfd.tenant.id = :tenantId");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public CustomFieldMetadata getCustomFieldsMetadataByEntityAndName(String entity, String name) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select cfd from CustomFieldMetadata cfd where cfd.entity = :entity and cfd.tenant.id = :tenantId and cfd.name = :name");
        query.setParameter("tenantId", UserContext.current().getTenantId());
        query.setParameter("name", name);
        query.setParameter("entity", entity);
        return (CustomFieldMetadata) query.uniqueResult();
    }

    @Override
    public CustomFieldMetadata updateCustomFieldMetadata(CustomFieldMetadata customFieldMetadata) {
        return (CustomFieldMetadata) sessionFactory.getCurrentSession().merge(customFieldMetadata);
    }

}
