/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Aug-2012
 *  @author praveeng
 */
package com.unifier.dao.customfields;

import java.util.List;

import com.unifier.core.entity.CustomFieldMetadata;
import com.uniware.core.entity.Tenant;

public interface ICustomFieldDao {

    /**
     * @param customFieldMetadata
     * @return
     */
    CustomFieldMetadata createCustomFieldMetadata(CustomFieldMetadata customFieldMetadata);

    /**
     * @param queryString
     * @return
     */
    boolean addCustomField(String queryString);

    List<CustomFieldMetadata> listAllCustomFields();

    List<Tenant> getAllTenants();

    List<CustomFieldMetadata> getCustomFieldsMetadata();

    CustomFieldMetadata getCustomFieldsMetadataByEntityAndName(String entity, String name);

    CustomFieldMetadata updateCustomFieldMetadata(CustomFieldMetadata customFieldMetadata);
}
