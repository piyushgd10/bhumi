/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 23, 2012
 *  @author singla
 */
package com.unifier.dao.startup;


public interface IBaseStartupDao {

    void registerHibernateEventListeners();

    Number getCurrentConnectionId();
}
