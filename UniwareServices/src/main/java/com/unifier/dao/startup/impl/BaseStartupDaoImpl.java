/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 23, 2012
 *  @author singla
 */
package com.unifier.dao.startup.impl;

import org.hibernate.SessionFactory;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.dao.config.CustomFieldsUpdateEventListener;
import com.unifier.dao.startup.IBaseStartupDao;

@Repository
public class BaseStartupDaoImpl implements IBaseStartupDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void registerHibernateEventListeners() {
        CustomFieldsUpdateEventListener listener = new CustomFieldsUpdateEventListener();
        EventListenerRegistry registry = ((SessionFactoryImpl) sessionFactory).getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.PERSIST).prependListener(listener);
        registry.getEventListenerGroup(EventType.POST_INSERT).prependListener(listener);
        registry.getEventListenerGroup(EventType.MERGE).prependListener(listener);
    }

    @Override
    public Number getCurrentConnectionId() {
        return (Number) sessionFactory.getCurrentSession().createSQLQuery("SELECT CONNECTION_ID()").uniqueResult();
    }
}
