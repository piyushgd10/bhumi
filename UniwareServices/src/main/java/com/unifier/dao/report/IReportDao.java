/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.dao.report;

import java.util.List;
import java.util.Map;

import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.entity.UserWidget;

public interface IReportDao {

    UserWidget getUserWidget(Integer userId, String code);

    UserWidget addUserWidget(UserWidget userWidget);

    List<UserWidget> getUserWidgets(Integer userId);

    List<Map<String, Object>> executeAnonymousQuery(String query, Map<String, Object> queryParams);

    List<DashboardWidget> getPreloadedWidgets();

    UserWidget updateUserWidget(UserWidget userWidget);

    List<Map<String, Object>> executeAnonymousQuery(String queryStr, Map<String, Object> queryParams, boolean hql);

    List<DashboardWidget> getDashboardWidgets();

}
