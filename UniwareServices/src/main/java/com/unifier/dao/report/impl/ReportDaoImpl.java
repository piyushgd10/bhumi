/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */
package com.unifier.dao.report.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.unifier.core.cache.CacheManager;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.DashboardWidget;
import com.unifier.core.entity.UserWidget;
import com.unifier.dao.report.IReportDao;

@SuppressWarnings("unchecked")
@Repository
public class ReportDaoImpl implements IReportDao {

    @Autowired
    @Qualifier(value = "sessionFactory")
    private SessionFactory sessionFactory;

    @Autowired
    @Qualifier(value = "sessionFactoryReplication")
    private SessionFactory sessionFactoryReplication;

    @Override
    public UserWidget getUserWidget(Integer userId, String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserWidget where user.id = :userId and dashboardWidget.code = :code");
        query.setParameter("userId", userId);
        query.setParameter("code", code);
        return (UserWidget) query.uniqueResult();
    }

    @Override
    public UserWidget addUserWidget(UserWidget userWidget) {
        sessionFactory.getCurrentSession().persist(userWidget);
        return userWidget;
    }

    @Override
    public List<UserWidget> getUserWidgets(Integer userId) {
        SessionFactory sf = sessionFactory;
        if(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled()){
            sf = sessionFactoryReplication;
        }
        Query query = sf.getCurrentSession().createQuery("from UserWidget where user.id = :userId and subscribed = 1 order by sequence");
        query.setParameter("userId", userId);
        return query.list();
    }

    @Override
    public List<Map<String, Object>> executeAnonymousQuery(String queryStr, Map<String, Object> queryParams) {
        return executeAnonymousQuery(queryStr, queryParams, true, false);
    }

    @Override
    public List<Map<String, Object>> executeAnonymousQuery(String queryStr, Map<String, Object> queryParams, boolean hql) {
        return executeAnonymousQuery(queryStr, queryParams, hql, CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled());
    }

    public List<Map<String, Object>> executeAnonymousQuery(String queryStr, Map<String, Object> queryParams, boolean hql, boolean executeOnReplica){
        SessionFactory sf = sessionFactory;
        if(executeOnReplica){
            sf = sessionFactoryReplication;
        }
        StringBuilder builder = new StringBuilder(queryStr);
        Query query = null;
        if (hql) {
            query = sf.getCurrentSession().createQuery(builder.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        } else {
            query = sf.getCurrentSession().createSQLQuery(builder.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        }
        if (queryParams != null) {
            for (String namedParameter : query.getNamedParameters()) {
                Object value = queryParams.get(namedParameter);
                if (!(value instanceof List)) {
                    query.setParameter(namedParameter, value);
                } else {
                    if (value != null) {
                        query.setParameterList(namedParameter, (Collection<Object>) value);
                    }
                }
            }
        }
        return query.list();
    }

    @Override
    public List<DashboardWidget> getPreloadedWidgets() {
        return sessionFactory.getCurrentSession().createQuery("from DashboardWidget where preloaded = 1 and enabled = 1").list();
    }

    @Override
    public UserWidget updateUserWidget(UserWidget userWidget) {
        sessionFactory.getCurrentSession().merge(userWidget);
        return userWidget;
    }

    @Override
    public List<DashboardWidget> getDashboardWidgets() {
        SessionFactory sf = sessionFactory;
        if(CacheManager.getInstance().getCache(EnvironmentPropertiesCache.class).isDashboardViaReplicationEnabled()){
            sf = sessionFactoryReplication;
        }
        return sf.getCurrentSession().createQuery("from DashboardWidget where enabled=1").list();
    }
}
