/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.dao.admin.access;

import java.util.List;

import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.FacilityAccessResource;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;
import com.unifier.core.entity.User;

public interface IAccessControlDao {

    Role createRole(Role role);

    Role getRoleByCode(String code);

    List<Role> getRoles();

    User getAllAccessResourcesForUser(Integer userId);

    RoleAccessResource createRoleAccessResource(RoleAccessResource roleAccessResource);

    RoleAccessResource editRoleAccessResource(RoleAccessResource roleAccessResource);

    List<RoleAccessResource> getRoleAccessResources(String accessResourceName);

    List<AccessResourceGroup> getAccessResourceGroups();

    List<AccessResource> getAccessResources();

    List<FacilityAccessResource> getFacilityAccessResources();

    RoleAccessResource getRoleAccessResource(Integer roleId, String accessResourceName);
}
