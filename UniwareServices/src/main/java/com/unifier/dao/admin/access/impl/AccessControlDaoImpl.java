/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2012
 *  @author vibhu
 */
package com.unifier.dao.admin.access.impl;

import java.util.List;

import com.unifier.core.entity.FacilityAccessResource;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.AccessResource;
import com.unifier.core.entity.AccessResourceGroup;
import com.unifier.core.entity.Role;
import com.unifier.core.entity.RoleAccessResource;
import com.unifier.core.entity.User;
import com.unifier.dao.admin.access.IAccessControlDao;
import com.unifier.services.vo.TenantProfileVO.Type;
import com.uniware.core.utils.UserContext;

@Repository
public class AccessControlDaoImpl implements IAccessControlDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Role createRole(Role role) {
        role.setTenant(UserContext.current().getTenant());
        sessionFactory.getCurrentSession().persist(role);
        return role;
    }

    @Override
    public Role getRoleByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role role where role.tenant.id=:tenantId and role.code = :code");
        query.setParameter("code", code);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (Role) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Role> getRoles() {
        Query query = null;
        if (UserContext.current().getTenant().getProduct().getCode().equals(Type.ENTERPRISE.name())) {
            query = sessionFactory.getCurrentSession().createQuery("select distinct r from Role r left join fetch r.roleAccessResources where r.tenant.id = :tenantId");
        } else {
            query = sessionFactory.getCurrentSession().createQuery(
                    "select distinct r from Role r left join fetch r.roleAccessResources rar where r.tenant.id=:tenantId and r.code in (select role from ProductRole where product.code = :productCode)");
            query.setParameter("productCode", UserContext.current().getTenant().getProduct().getCode());
        }
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    @Override
    public User getAllAccessResourcesForUser(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct user from User user join user.userRoles userRole join userRole.role role join role.roleAccessResources rar where rar.enabled=1 and userRole.enabled=1 and user.id=:userId");
        query.setParameter("userId", userId);
        return (User) query.uniqueResult();
    }

    @Override
    public RoleAccessResource createRoleAccessResource(RoleAccessResource roleAccessResource) {
        sessionFactory.getCurrentSession().persist(roleAccessResource);
        return roleAccessResource;
    }

    @Override
    public RoleAccessResource editRoleAccessResource(RoleAccessResource roleAccessResource) {
        roleAccessResource = (RoleAccessResource) sessionFactory.getCurrentSession().merge(roleAccessResource);
        return roleAccessResource;
    }

    @Override
    public List<RoleAccessResource> getRoleAccessResources(String accessResourceName) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct rar from RoleAccessResource rar join fetch rar.role r where rar.role.tenant.id = :tenantId and rar.accessResourceName = :accessResourceName");
        query.setParameter("accessResourceName", accessResourceName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return query.list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.startup.IStartupDao#getAccessResourceGroups()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AccessResourceGroup> getAccessResourceGroups() {
        return sessionFactory.getCurrentSession().createQuery("select arg from AccessResourceGroup arg ").list();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.startup.IStartupDao#getAccessResources()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AccessResource> getAccessResources() {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct ar from ProductAccessResource par join par.accessResource ar left join fetch ar.accessPatterns left join fetch ar.accessResourceGroup where par.enabled = 1 and par.product.code = :productCode");
        query.setParameter("productCode", UserContext.current().getTenant().getProduct().getCode());
        return query.list();
    }

    @Override
    public List<FacilityAccessResource> getFacilityAccessResources() {
        Query query = sessionFactory.getCurrentSession().createQuery("from FacilityAccessResource where enabled = :enabled");
        query.setParameter("enabled", true);
        return query.list();
    }

    @Override
    public RoleAccessResource getRoleAccessResource(Integer roleId, String accessResourceName){
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from RoleAccessResource rar join fetch rar.role r where rar.role.tenant.id = :tenantId and rar.accessResourceName = :accessResourceName and rar.role.id = :roleId");
        query.setParameter("roleId", roleId);
        query.setParameter("accessResourceName", accessResourceName);
        query.setParameter("tenantId", UserContext.current().getTenantId());
        return (RoleAccessResource) query.uniqueResult();
    }
}