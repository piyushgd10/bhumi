package com.unifier.dao.release.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.ReleaseVersion;
import com.unifier.dao.release.IReleaseDao;

@Repository
public class ReleaseDaoImpl implements IReleaseDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ReleaseVersion getLatestReleaseVersion() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ReleaseVersion rv order by rv.majorVersion desc");
        query.setMaxResults(1);
        return (ReleaseVersion) query.uniqueResult();
    }

}
