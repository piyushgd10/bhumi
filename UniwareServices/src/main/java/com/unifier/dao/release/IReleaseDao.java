/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 21, 2012
 *  @author praveeng
 */
package com.unifier.dao.release;

import com.unifier.core.entity.ReleaseVersion;

public interface IReleaseDao {
    ReleaseVersion getLatestReleaseVersion();
}
