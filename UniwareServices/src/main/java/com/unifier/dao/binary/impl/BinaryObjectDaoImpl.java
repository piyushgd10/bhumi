/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 21, 2012
 *  @author singla
 */
package com.unifier.dao.binary.impl;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.BinaryObject;
import com.unifier.dao.binary.IBinaryObjectDao;

/**
 * @author singla
 */
@Repository
public class BinaryObjectDaoImpl implements IBinaryObjectDao {

    @Autowired
    private SessionFactory sessionFactory;

    /* (non-Javadoc)
     * @see com.uniware.dao.binary.IBinaryObjectDao#getBinaryObjectById(int)
     */
    @Override
    public BinaryObject getBinaryObjectById(int binaryObjectId) {
        return (BinaryObject) sessionFactory.getCurrentSession().createCriteria(BinaryObject.class).add(Restrictions.idEq(binaryObjectId)).uniqueResult();
    }

    /* (non-Javadoc)
     * @see com.uniware.dao.binary.IBinaryObjectDao#addBinaryObject(com.uniware.core.entity.BinaryObject)
     */
    @Override
    public BinaryObject addBinaryObject(BinaryObject binaryObject) {
        sessionFactory.getCurrentSession().persist(binaryObject);
        return binaryObject;
    }

}
