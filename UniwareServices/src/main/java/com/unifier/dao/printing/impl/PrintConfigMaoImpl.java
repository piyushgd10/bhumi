/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 16, 2012
 *  @author praveeng
 */
package com.unifier.dao.printing.impl;

import com.unifier.core.utils.DateUtils;
import com.unifier.dao.printing.IPrintConfigMao;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrintConfigMaoImpl implements IPrintConfigMao {

	@Autowired
	@Qualifier(value = "tenantSpecificMongo")
	private MongoOperations mongoOperations;



	@Override
	public PrintTemplateVO getPrintTemplateByType(String type) {
		return mongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("type").is(type)), PrintTemplateVO.class);
	}

	@Override
	public void updatePrintTemplate(PrintTemplateVO template) {
		template.setTenantCode(UserContext.current().getTenant().getCode());
		template.setUpdated(DateUtils.getCurrentTime());
        mongoOperations.save(template);
	}

	@Override
	public List<PrintTemplateVO> getPrintTemplates() {
		return mongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("enabled").is(Boolean.TRUE)), PrintTemplateVO.class);
	}

	@Override
	public SamplePrintTemplateVO getSamplePrintTemplateByTypeAndName(String type, String name) {
		return mongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("type").is(type).and("name").is(name)), SamplePrintTemplateVO.class);
    }

    @Override
    public void updateSampleTemplate(SamplePrintTemplateVO template) {
        template.setUpdated(DateUtils.getCurrentTime());
        template.setTenantCode(UserContext.current().getTenant().getCode());
        mongoOperations.save(template);
    }

    @Override
    public void removeSampleTemplate(SamplePrintTemplateVO template) {
        mongoOperations.remove(new Query(Criteria.where("code").is(template.getCode()).and("tenantCode").is(UserContext.current().getTenant().getCode())),SamplePrintTemplateVO.class);
    }

    @Override
    public List<SamplePrintTemplateVO> getSamplePrintTemplates() {
        return mongoOperations.find(new Query(Criteria.where("enabled").is(Boolean.TRUE).and("tenantCode").is(UserContext.current().getTenant().getCode())),SamplePrintTemplateVO.class);
    }

    @Override
    public SamplePrintTemplateVO getSamplePrintTemplate(String code) {
        return mongoOperations.findOne(new Query(Criteria.where("code").is(code).and("tenantCode").is(UserContext.current().getTenant().getCode())),SamplePrintTemplateVO.class);
    }

}
