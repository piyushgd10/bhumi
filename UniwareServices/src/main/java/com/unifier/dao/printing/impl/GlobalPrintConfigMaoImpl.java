package com.unifier.dao.printing.impl;

import com.unifier.core.utils.DateUtils;
import com.unifier.dao.printing.IGlobalPrintConfigMao;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
/**
 * Created by admin on 9/4/15.
 */

@Repository
public class GlobalPrintConfigMaoImpl implements IGlobalPrintConfigMao{

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations commonMongoOperations;

    @Override
    public void updatePrintTemplate(PrintTemplateVO template) {
        template.setUpdated(DateUtils.getCurrentTime());
        commonMongoOperations.save(template);
    }

    @Override
    public SamplePrintTemplateVO getGlobalSamplePrintTemplateByTypeAndName(String type, String name) {
        SamplePrintTemplateVO sampleVO = commonMongoOperations.findOne(new Query(Criteria.where("global").is(Boolean.TRUE).and("type").is(type).and("name").is(name)), SamplePrintTemplateVO.class);
        return sampleVO;
    }

    @Override
    public void updateGlobalSampleTemplate(SamplePrintTemplateVO template) {
        template.setUpdated(DateUtils.getCurrentTime());
        template.setTenantCode(null);
        commonMongoOperations.save(template);
    }

    @Override
    public List<SamplePrintTemplateVO> getGlobalSamplePrintTemplates() {
        return commonMongoOperations.find(new Query(Criteria.where("enabled").is(Boolean.TRUE).and("global").is(Boolean.TRUE)),SamplePrintTemplateVO.class);
    }

    @Override
    public SamplePrintTemplateVO getGlobalSamplePrintTemplate(String code) {
        return commonMongoOperations.findOne(new Query(Criteria.where("code").is(code).and("global").is(Boolean.TRUE)),SamplePrintTemplateVO.class);
    }

    @Override
    public void removeGlobalSamplePrintTemplate(String code) {
        commonMongoOperations.remove(new Query(Criteria.where("code").is(code).and("global").is(Boolean.TRUE)),SamplePrintTemplateVO.class);
    }

}
