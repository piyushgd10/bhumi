/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 16, 2012
 *  @author praveeng
 */
package com.unifier.dao.printing;

import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;

import java.util.List;

public interface IPrintConfigMao {

    void updatePrintTemplate(PrintTemplateVO template);

    PrintTemplateVO getPrintTemplateByType(String name);

    List<PrintTemplateVO> getPrintTemplates();

    SamplePrintTemplateVO getSamplePrintTemplateByTypeAndName(String type, String name);

    void updateSampleTemplate(SamplePrintTemplateVO template);

    void removeSampleTemplate(SamplePrintTemplateVO template);

    List<SamplePrintTemplateVO> getSamplePrintTemplates();

    SamplePrintTemplateVO getSamplePrintTemplate(String samplePrintTemplateCode);

}
