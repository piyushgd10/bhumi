package com.unifier.dao.printing;

import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.core.vo.SamplePrintTemplateVO;

import java.util.List;

/**
 * Created by admin on 9/4/15.
 */
public interface IGlobalPrintConfigMao {

    void updatePrintTemplate(PrintTemplateVO template);

    SamplePrintTemplateVO getGlobalSamplePrintTemplateByTypeAndName(String type, String name);

    void updateGlobalSampleTemplate(SamplePrintTemplateVO template);

    List<SamplePrintTemplateVO> getGlobalSamplePrintTemplates();

    SamplePrintTemplateVO getGlobalSamplePrintTemplate(String code);

    void removeGlobalSamplePrintTemplate(String code);
}
