package com.unifier.mao.job.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.entity.Job;
import com.unifier.mao.job.IJobMao;
import com.uniware.core.utils.UserContext;

@Repository(value = "jobMao")
public class JobMaoImpl implements IJobMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations tenantMongoOperations;

    @Override
    public void saveJob(Job Job) {
        mongoOperations.save(Job);
    }

    @Override
    public List<Job> getGlobalRecurrentJobs() {
        return mongoOperations.find(Query.query(Criteria.where("tenantCode").is(null).and("enabled").is(true)), Job.class);
    }

    @Override
    public List<Job> getRecurrentJobsForTenant() {
        return tenantMongoOperations.find(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("enabled").is(true)), Job.class);
    }

    @Override
    public Job getJobByCode(String jobCode) {
        Job job = mongoOperations.findOne(new Query(Criteria.where("code").is(jobCode)), Job.class);
        if (job == null && UserContext.current().getTenant() != null) {
            job = tenantMongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("code").is(jobCode)), Job.class);
        }
        return job;
    }

    @Override
    public Job getJobByName(String taskName) {
        Job job = mongoOperations.findOne(new Query(Criteria.where("name").is(taskName)), Job.class);
        if (job == null && UserContext.current().getTenant() != null) {
            job = tenantMongoOperations.findOne(new Query(Criteria.where("tenantCode").is(UserContext.current().getTenant().getCode()).and("name").is(taskName)), Job.class);
        }
        return job;
    }

}
