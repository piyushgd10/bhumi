package com.unifier.mao.job;

import java.util.List;

import com.unifier.core.entity.Job;

public interface IJobMao {

    void saveJob(Job Job);

    List<Job> getGlobalRecurrentJobs();

    Job getJobByCode(String jobCode);

    Job getJobByName(String taskName);

    List<Job> getRecurrentJobsForTenant();

}
