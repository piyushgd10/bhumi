package com.unifier.mao.release.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.mao.release.IReleaseMao;
import com.unifier.services.vo.ReleaseUpdateVO;

@Repository(value = "releaseMao")
public class ReleaseMaoImpl implements IReleaseMao {

    @Autowired
    @Qualifier(value = "commonMongo")
    private MongoOperations mongoOperations;

    @Override
    public List<ReleaseUpdateVO> getReleaseUpdates(Integer lastReleaseUpdateVersionRead) {
        Query query = new Query(Criteria.where("versionId").gt(lastReleaseUpdateVersionRead.intValue()));
        query.with(new Sort(Sort.Direction.DESC, "versionId"));
        return mongoOperations.find(query, ReleaseUpdateVO.class);
    }

    @Override
    public List<ReleaseUpdateVO> getAllReleaseUpdates() {
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "versionId"));
        return mongoOperations.find(query, ReleaseUpdateVO.class);
    }

    @Override
    public ReleaseUpdateVO getReleaseUpdate(Integer latestReleaseUpdateVersionRead) {
        Query query = new Query(Criteria.where("versionId").is(latestReleaseUpdateVersionRead.intValue()));
        return mongoOperations.findOne(query, ReleaseUpdateVO.class);
    }
}
