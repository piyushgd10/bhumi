/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/08/14
 *  @author amit
 */

package com.unifier.mao.release;

import java.util.List;

import com.unifier.services.vo.ReleaseUpdateVO;


public interface IReleaseMao {

    List<ReleaseUpdateVO> getReleaseUpdates(Integer lastReleaseUpdateVersionRead);

    List<ReleaseUpdateVO> getAllReleaseUpdates();

    ReleaseUpdateVO getReleaseUpdate(Integer latestReleaseUpdateVersionRead);
}