package com.unifier.mao.jobresult.job.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.entity.JobResult;
import com.unifier.mao.jobresult.job.IJobResultMao;
import com.uniware.core.cache.FacilityCache;
import com.uniware.core.utils.UserContext;

@Repository(value = "jobResultMao")
public class JobResultMaoImpl implements IJobResultMao {

    @Autowired
    @Qualifier(value = "tenantSpecificMongo")
    private MongoOperations mongoOperations;

    @Override
    public void saveJob(JobResult jobResult) {
        jobResult.setTenantCode(UserContext.current().getTenant().getCode());
        jobResult.setFacilityCode(CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode());
        mongoOperations.save(jobResult);
    }

    @Override
    public JobResult get(String jobCode, Level level) {
        String currentFacilityCode = CacheManager.getInstance().getCache(FacilityCache.class).getCurrentFacilityCode();
        Criteria criteria = Criteria.where("jobCode").is(jobCode).and("tenantCode").is(UserContext.current().getTenant().getCode());
        if (Level.FACILITY.equals(level)) {
            criteria.and("facilityCode").is(currentFacilityCode);
        }
        return mongoOperations.findOne(new Query(criteria), JobResult.class);
    }
}
