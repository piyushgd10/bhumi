package com.unifier.mao.jobresult.job;

import com.unifier.core.annotation.Level;
import com.unifier.core.entity.JobResult;

public interface IJobResultMao {

    void saveJob(JobResult jobResult);

    JobResult get(String jobCode, Level level);
}
