/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/08/14
 *  @author amit
 */

package com.unifier.mao.report.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.unifier.dao.export.IExportMao;
import com.unifier.mao.report.IReportMao;

@Repository(value = "reportMao")
public class ReportMaoImpl implements IReportMao {

	@Autowired
	private IExportMao exportMao;

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<Map<String, T>> executeAnonymousQuery(Query query, Pageable pagination, Class<T> clazz) {
		List<?> list = exportMao.executeQuery(query, pagination, clazz);
		Iterator<?> i = list.iterator();
		List<Map<String, T>> response = new ArrayList<>(list.size());
		while (i.hasNext()) {
			Map<String, T> dataMap = new HashMap<>(1);
			dataMap.put("", (T) i.next());
			response.add(dataMap);
		}
		return response;
	}
}
