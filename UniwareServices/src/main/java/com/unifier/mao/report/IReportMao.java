/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11/08/14
 *  @author amit
 */

package com.unifier.mao.report;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Map;

public interface IReportMao {

    <T> List<Map<String, T>> executeAnonymousQuery(Query query, Pageable pagination, Class<T> clazz);
}