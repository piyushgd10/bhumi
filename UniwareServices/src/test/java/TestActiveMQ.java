/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.jms.JMSException;

import com.unifier.core.jms.MessagingConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.email.SimpleMailMessage;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestActiveMQ {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector activeMQConnector;

    //ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 100, , unit, workQueue);
    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("sunny");
        UserContext.current().setTenant(t);
    }

    @Test
    public void testExecutor() throws JMSException, InterruptedException, ExecutionException {
        SimpleMailMessage message = new SimpleMailMessage();
        Set<String> emails = new HashSet<>();
        emails.add("test");
        message.setTo(emails);
        message.setFrom("test");
        message.setCc(emails);
        message.setBcc(emails);
        message.setSubject("sadasdd");
        message.setBody("adsddasd");
        message.setCreated(DateUtils.getCurrentTime());
        message.addProperty("source", "uniware");
        message.addProperty("tenantCode", UserContext.current().getTenant().getCode());
        ThreadPoolExecutor exec = new ThreadPoolExecutor(10, 100, 1000, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        long startTime = System.currentTimeMillis();
        List<Future<?>> futures = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            futures.add(exec.submit(new TestQueue(message)));
        }
        for (int i = 0; i < 100; i++) {
            futures.get(i).get();
        }
        System.out.println("Time taken: " + (System.currentTimeMillis() - startTime));
    }

    public class TestQueue implements Runnable {

        private SimpleMailMessage message;

        public TestQueue(SimpleMailMessage message) {
            super();
            this.message = message;
        }

        @Override
        public void run() {
            activeMQConnector.produceMessage(MessagingConstants.Queue.TRANSFER_PRICE_RECONCILIATION_QUEUE, message);
        }

    }
}
