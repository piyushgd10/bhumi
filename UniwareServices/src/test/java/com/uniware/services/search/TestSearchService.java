package com.uniware.services.search;

import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.entity.SearchTypes;
import com.unifier.services.search.IGlobalSearchService;
import com.uniware.core.api.search.GlobalSearchRequest;
import com.uniware.core.api.search.GlobalSearchResponse;
import com.uniware.core.api.search.SearchType;
import com.uniware.core.utils.UserContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestSearchService {

    @Autowired
    private IGlobalSearchService searchService;

    @Test
    public void globalSearch() {
        GlobalSearchRequest request = new GlobalSearchRequest();
        request.setKeyword("100004743"); // sale order display code
        request.setType(SearchType.SALE_ORDER.name());
        //request.setText("Combo15");  sale order code
        //request.setKeyword("01P257");
        UserContext.current().setFacility(new Facility(1));
        UserContext.current().setTenantId(1);
        //request.setKeyword("NAES/12-13/10");
        GlobalSearchResponse response = searchService.globalSearch(request);
        System.out.println(response.getSearchDTOs());
    }
    
    @Test
    public void getAllsearchTypes() {
        List<SearchTypes> searchType = searchService.getAllSearchTypes();
        System.out.println(searchType);
    }
}
