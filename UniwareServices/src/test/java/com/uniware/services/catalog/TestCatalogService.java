/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Dec-2011
 *  @author vibhu
 */
package com.uniware.services.catalog;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.catalog.CreateCategoryRequest;
import com.uniware.core.api.catalog.CreateCategoryResponse;
import com.uniware.core.api.catalog.CreateItemTypeRequest;
import com.uniware.core.api.catalog.CreateItemTypeResponse;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestCatalogService {

    @Autowired
    private ApplicationContext            applicationContext;
    
    @Autowired
    private ITenantService                tenantService;
    
    @Autowired
    private ICatalogService catalogService;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("Triveni");
        UserContext.current().setTenant(t);
    }
    
  
    @Test
    public void testCreateCategory() {
        CreateCategoryRequest req = new CreateCategoryRequest();
        req.getCategory().setCode("R");
        req.getCategory().setName("TOdsfY");
        // req.getCategory().setSectionId(1);
        CreateCategoryResponse res = catalogService.createCategory(req);
        assert (res.getErrors().size() == 0);

        //removeObject(res.getCategory());
    }

    @Test
    public void testCreateItemType() {
        CreateItemTypeRequest req = new CreateItemTypeRequest();
        req.getItemType().setCategoryCode("sad");
        req.getItemType().setHeight(500);
        req.getItemType().setLength(300);
        req.getItemType().setWidth(5000);
        req.getItemType().setSkuCode("NOKIA ABC");

        CreateItemTypeResponse res = catalogService.createItemType(req);
        assert (res.getErrors().size() == 0);

        //removeObject(res.getItemType());
    }

}
