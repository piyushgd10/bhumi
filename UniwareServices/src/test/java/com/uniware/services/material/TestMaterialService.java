/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 11, 2012
 *  @author singla
 */
package com.uniware.services.material;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.material.RemoveItemFromGatePassRequest;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestMaterialService {

    @Autowired
    private IMaterialService   materialService;

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("singla");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void removeItemToGatePass() {
        RemoveItemFromGatePassRequest request = new RemoveItemFromGatePassRequest();
        request.setGatePassCode("GP12");
        request.setItemCode("01568");
        System.out.println(materialService.removeItemFromGatePass(request));
    }

}
