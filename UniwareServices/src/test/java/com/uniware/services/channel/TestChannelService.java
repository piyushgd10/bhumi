/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2013
 *  @author Sunny Agarwal
 */
package com.uniware.services.channel;

import java.beans.IntrospectionException;

import com.uniware.core.entity.Source;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.channel.GetChannelProductSummaryRequest;
import com.uniware.core.api.channel.GetChannelProductSummaryResponse;
import com.uniware.core.entity.Channel;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.SourceSaleOrderImportInstructionsVO;
import com.uniware.dao.channel.IChannelDao;
import com.uniware.dao.channel.IChannelMao;
import com.uniware.services.configuration.SourceConfiguration;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestChannelService {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private IChannelService channelService;

    @Autowired
    private IChannelMao channelMao;

    @Autowired
    private IChannelDao channelDao;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private IChannelCatalogService channelCatalogService;

    @Before
    public void loadCache() throws SecurityException, ClassNotFoundException, NoSuchMethodException, IntrospectionException {
        UserContext.current().setTenant(tenantService.getTenantByCode("parijat"));
        UserContext.current().setTenant(tenantService.getTenantByCode("staging"));
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @Test
    @Transactional
    public void testChannelRevenue() {
        Channel channel = channelDao.getChannelByCode("FLIPKART");
        channelService.updateChannelConnector(channel.getChannelConnectors().iterator().next());
    }

    @Test
    @Transactional
    public void testChannelProductSummary() {
        UserContext.current().setTenantId(1);
        GetChannelProductSummaryRequest request = new GetChannelProductSummaryRequest();
        request.setChannelCode("YEBHI_B2B");
        GetChannelProductSummaryResponse response = channelService.getChannelProductSummaryByChannelCode(request);
        System.out.println(response.isSuccessful());
        System.out.println(response.getUnmappedProductCount());
    }

    //    @Test
    //    public void createChannel() {
    //        AddChannelRequest request = new AddChannelRequest();
    //        WsChannel channel = new WsChannel();
    //        channel.setSourceCode("SHOPCLUES");
    //        channel.setEnabled(true);
    //        channel.setChannelName("test");
    //        channel.setAllowCombinedManifest(false);
    //        channel.setAutoVerifyOrders(true);
    //        channel.setInventoryAllocationPriority(1);
    //        channel.setTat(3);
    //        request.setWsChannel(channel);
    //        System.out.println(new Gson().toJson(request));
    //        //channelService.addChannel(request);
    //    }
    //
    //    @Test
    //    public void testEditChannel() {
    //        EditChannelRequest request = new EditChannelRequest();
    //        Channel channel = channelService.getChannelByCode("SHOPIFY");
    //        WsChannel wsChannel = new WsChannel();
    //        wsChannel.setAllowCombinedManifest(channel.isAllowCombinedManifest());
    //        wsChannel.setAutoVerifyOrders(channel.isAutoVerifyOrders());
    //        if (channel.getBillingParty() != null)
    //            wsChannel.setBillingPartyCode(channel.getBillingParty().getCode());
    //        wsChannel.setChannelName(channel.getName());
    //        if (channel.getCustomer() != null)
    //            wsChannel.setCustomerCode(channel.getCustomer().getCode());
    //        wsChannel.setEnabled(channel.isEnabled());
    //        wsChannel.setInventoryAllocationPriority(channel.getInventoryAllocationPriority());
    //        wsChannel.setInventoryUpdateFormula(channel.getInventoryUpdateFormula());
    //        wsChannel.setLedgerName(channel.getLedgerName());
    //        wsChannel.setNotificationsEnabled(channel.isNotificationsEnabled());
    //        //        wsChannel.setReconciliationSyncStatus(channel.getReconciliationSyncStatus().name());
    //        wsChannel.setOrderSyncEnabled(channel.getOrderSyncStatus().equals(Channel.SyncStatus.ON));
    //        wsChannel.setPackageType(channel.getPackageType());
    //        wsChannel.setPaymentReconciliationEnabled(channel.isPaymentReconciliationEnabled());
    //        if (channel.getPriceMaster() != null)
    //            wsChannel.setPriceMasterCode(channel.getPriceMaster().getCode());
    //        wsChannel.setProductDelistingEnabled(channel.isProductDelistingEnabled());
    //        wsChannel.setProductRelistingEnabled(channel.isProductRelistingEnabled());
    //        wsChannel.setShipmentLabelFormat(channel.getShipmentLabelFormat());
    //        wsChannel.setShippingLabelAggregationFormat(channel.getShippingLabelAggregationFormat());
    //        wsChannel.setSourceCode(channel.getSource().getCode());
    //        wsChannel.setTat(channel.getTat());
    //        wsChannel.setThirdPartyShipping(channel.isThirdPartyShipping());
    //        request.setWsChannel(wsChannel);
    //        channelService.editChannel(request);
    //    }

    @Test
    public void addSourceInstruction() {
        for (Source s : ConfigurationManager.getInstance().getConfiguration(SourceConfiguration.class).getApplicableSources()) {
            if (com.uniware.core.entity.Source.Type.B2B.equals(s.getType())) {
                SourceSaleOrderImportInstructionsVO i = new SourceSaleOrderImportInstructionsVO();
                i.setSourceCode(s.getCode());
                i.setInstructions("The file provided by the e-commerce can be directly uploaded.");
                channelMao.addSourceSaleOrderImportInstructionsVO(i);
            } else if ("CUSTOM".equals(s.getCode())) {
                SourceSaleOrderImportInstructionsVO i = new SourceSaleOrderImportInstructionsVO();
                i.setSourceCode(s.getCode());
                i.setInstructions("The file extension should be .csv and format should match the template provided.");
                channelMao.addSourceSaleOrderImportInstructionsVO(i);
            }
        }
    }
}
