/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Jul-2013
 *  @author sunny
 */
package com.uniware.services.bundle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.bundle.CreateBundleRequest;
import com.uniware.core.api.bundle.CreateBundleResponse;
import com.uniware.core.api.bundle.WsBundle;
import com.uniware.core.api.bundle.WsComponentItemType;
import com.uniware.core.entity.Bundle;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestBundleService {

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private IBundleService     bundleService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void loadStartupCache() {
        UserContext.current().setTenant(new Tenant(2));
        //UserContext.current().setFacilityId(18);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @Test
    public void testCreateBundle() {
        CreateBundleRequest request = new CreateBundleRequest();
        WsBundle bundle = new WsBundle("TSVJ2814");
        List<WsComponentItemType> bundleItemTypes = new ArrayList<WsComponentItemType>();
        bundleItemTypes.add(new WsComponentItemType("TSVJ2816", 2, new BigDecimal(100)));
        bundleItemTypes.add(new WsComponentItemType("TSVJ3468", 1, new BigDecimal(90)));
        bundle.setWsComponentItemTypes(bundleItemTypes);
        request.setWsBundle(bundle);
        CreateBundleResponse response = bundleService.createBundle(request);
        System.out.println(response.isSuccessful());
        System.out.println(response.getErrors());
        System.out.println(response);
    }

    @Test
    public void testBundle() {
        List<Bundle> bundles = bundleService.getBundlesByComponentItemType("TSVJ2816");
        System.out.println(bundles.size());
    }

}
