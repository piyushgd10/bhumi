/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */
package com.uniware.services.saleorder;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;
import com.uniware.core.api.model.WsSaleOrder;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.concurrent.ContextAwareExecutorFactory;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.dao.shipping.IPaymentReconciliationService;
import com.uniware.services.common.ISequenceGenerator;
import com.uniware.services.inventory.IInventoryService;
import com.uniware.services.script.service.IScriptService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tenant.ITenantService;
import com.uniware.services.warehouse.IFacilityService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestSaleOrderService {
    @Autowired
    private ISaleOrderService             saleOrderService;
    @Autowired
    private SessionFactory                sessionFactory;
    @Autowired
    private IShippingService              shippingService;
    @Autowired
    private ITenantService                tenantService;
    @Autowired
    private IFacilityService              facilityService;
    @Autowired
    private IInventoryService             inventoryService;

    @Autowired
    private IScriptService                scriptService;
    @Autowired
    private IPaymentReconciliationService paymentReconciliationService;

    @Autowired
    ISequenceGenerator                    sequenceGenerator;

    @Autowired
    private ApplicationContext            applicationContext;

    @Autowired
    private ContextAwareExecutorFactory   executorFactory;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("sunny");
        UserContext.current().setTenant(t);
    }

    @Test
    public void testExecutor() {
//        MDC.put("username", "sunny");
//        MDC.put("tenant", UserContext.current().getTenant().getName());
//        executorFactory.getExecutor("test").submit(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("I am running in new thread");
//            }
//        });
        System.out.println(saleOrderService.getSaleOrdersForProcessing());
    }

    @Test
    public void testScriptAutoComplete() {
        System.out.println(scriptService.lookupScripts("saleOrder"));
    }

    @Test
    public void test() {
        Tenant t = tenantService.getTenantByCode("Triveni");
        UserContext.current().setTenant(t);
        //FacilityDTO f = facilityService.getFacilityByCode("01");
        //UserContext.current().setFacilityId(1);
        //startupService.loadChannels();
        //System.out.println(sequenceGenerator.generateNext(Sequence.Name.BILL_OF_MATERIALS));
        //System.out.println(sequenceGenerator.generateNext(Sequence.Name.BILL_OF_MATERIALS));
        for (int code = 1234567; code < 1234667; code++) {
            SaleOrderVO data1 = new SaleOrderVO();
            data1.setCode(String.valueOf(code));
            data1.setRequest(new CreateSaleOrderRequest());
            data1.setResponse(new CreateSaleOrderResponse());
            saleOrderService.createOrUpdateFailedSaleOrder(data1);
        }

        System.out.println(saleOrderService.getAllSaleOrderVOs());
    }

    @Test
    public void fetchSaleOrder() {
        UserContext.current().setTenant(new Tenant(1));
        paymentReconciliationService.reconcileShipmentPayments("COD");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void createSaleOrder() {
        List<ItemType> itemTypes = new ArrayList<ItemType>();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from ItemType");
        itemTypes = query.list();
        List<WsAddressDetail> allAddresses = getAddresses();

        for (int j = 0; j < 1000; j++) {

            CreateSaleOrderRequest request = new CreateSaleOrderRequest();
            WsSaleOrder saleOrder = new WsSaleOrder();
            saleOrder.setCode("TEST1000000001" + j);
            saleOrder.setCashOnDelivery(Math.round(Math.random()) == 0 ? true : false);
            List<WsAddressDetail> addresses = new ArrayList<WsAddressDetail>();
            Map<String, WsAddressDetail> addressMap = new HashMap<String, WsAddressDetail>();
            saleOrder.setSaleOrderItems(new ArrayList<WsSaleOrderItem>());
            WsAddressRef addressRef = new WsAddressRef();
            int numberOfOrderItems = (int) Math.round(Math.random() * 2 + 1);
            boolean createPackageBeforeHand = (Math.round(Math.random()) == 0 ? true : false);
            ItemType it;
            for (int i = 0; i < numberOfOrderItems; i++) {
                WsAddressDetail Address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));

                if (!addressMap.containsKey(Address1.getName())) {
                    Address1.setId("Address" + (i + 1));
                    addressMap.put(Address1.getName(), Address1);
                    addresses.add(Address1);
                    addressRef.setReferenceId("Address1");
                } else {
                    addressRef.setReferenceId(addressMap.get(Address1.getName()).getId());
                }
                it = itemTypes.get((int) Math.round(Math.random() * (itemTypes.size() - 1)));
                if (createPackageBeforeHand) {
                    int packageId = (int) Math.round(Math.random() * (numberOfOrderItems - 1));
                    saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (j * 10 + i), 1 + packageId, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                } else {
                    saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (j * 10 + i), 0, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                }
            }

            saleOrder.setAddresses(addresses);
            WsAddressRef shippingAddressRef = new WsAddressRef();
            shippingAddressRef.setReferenceId("Address1");
            saleOrder.setShippingAddress(shippingAddressRef);
            saleOrder.setBillingAddress(shippingAddressRef);

            request.setSaleOrder(saleOrder);
            CreateSaleOrderResponse response = saleOrderService.createSaleOrder(request);
            assertEquals(true, response.isSuccessful());
        }
    }

    private WsSaleOrderItem getSaleOrderItem(String code, int packet, String itemSKU, String methodCode, boolean giftwrap, WsAddressRef wsAddressRef, boolean isCOD) {
        WsSaleOrderItem saleOrderItem = new WsSaleOrderItem();
        saleOrderItem.setCode(code);
        saleOrderItem.setPacketNumber(packet);
        saleOrderItem.setItemSku(itemSKU);
        saleOrderItem.setShippingMethodCode(methodCode);
        saleOrderItem.setGiftWrap(giftwrap);

        saleOrderItem.setSellingPrice(new BigDecimal(Math.round(Math.random() * 10000 + 100)));
        if (giftwrap) {
            saleOrderItem.setGiftMessage("Happy");
            saleOrderItem.setGiftWrapCharges(new BigDecimal(20));
        }
        if (isCOD) {
            saleOrderItem.setCashOnDeliveryCharges(new BigDecimal(30));
        }
        saleOrderItem.setTotalPrice(saleOrderItem.getSellingPrice().add(
                saleOrderItem.getShippingCharges().add(saleOrderItem.getCashOnDeliveryCharges().add(saleOrderItem.getGiftWrapCharges()))));
        return saleOrderItem;
    }

    private List<WsAddressDetail> getAddresses() {
        List<WsAddressDetail> allAddresses = new ArrayList<WsAddressDetail>();

        WsAddressDetail address = new WsAddressDetail();
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Karun Singla");
        address.setPincode("122002");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Ankit Pruthi");
        address.setPincode("122002");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Vibhu Garg");
        address.setPincode("122002");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Kuvalaya");
        address.setPincode("122002");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Kamal");
        address.setPincode("122002");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);
        return allAddresses;
    }

}
