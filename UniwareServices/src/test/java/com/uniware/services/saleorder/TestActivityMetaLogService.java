/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 20-Aug-2014
 *  @author parijat
 */
package com.uniware.services.saleorder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.audit.ActivityDto;
import com.unifier.core.api.audit.FetchEntityActivityRequest;
import com.unifier.core.api.audit.FetchEntityActivityResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.audit.IActivityLogService;
import com.uniware.services.audit.impl.ActivityEntityEnum;
import com.uniware.services.audit.impl.ActivityTypeEnum;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestActivityMetaLogService {

    @Autowired
    private IActivityLogService activityLogService;

    @Autowired
    private ApplicationContext  applicationContext;

    @Autowired
    private ITenantService      tenantService;

    private static ExecutorService executorService = new ThreadPoolExecutor(20, 100, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("parijat");
        UserContext.current().setTenant(t);
        UserContext.current().setUniwareUserName("parijat@unicommerce.com");
    }

    private ActivityDto createDTOPayLoadObject() {
        ActivityDto activityDto = new ActivityDto(ActivityEntityEnum.SALE_ORDER.getName(), "SO-TestLoad_ID_DTO", "SO_TestLoad_Grp_DTO", ActivityTypeEnum.CREATION.name());
        List<String> logs = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            logs.add(i + " This is test log for sale order InnoDB load");
        }
        activityDto.setActivityLogs(logs);
        return activityDto;
    }

    private void testSaveActivityMetaLogObj() {
        activityLogService.persistActivityLogs(createDTOPayLoadObject());
    }

    private void testGetActivityMetaLogs() {
        FetchEntityActivityRequest request = new FetchEntityActivityRequest();
        request.setEntityIdentifier("SO-TestLoad_ID_DTO");
        request.setEntityName(ActivityEntityEnum.SALE_ORDER.getName());
        FetchEntityActivityResponse response = activityLogService.getActivityLogsbyEntityId(request);
        System.out.println(response.getEntityActivityLogs().size() + " Logs found");
    }
    
    class TestDBReadLatency implements Runnable {

        @Override
        public void run() {
            System.out.println("Starting read thread");
            while (true) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                long startTime = System.currentTimeMillis();
                testGetActivityMetaLogs();
                System.out.println("Read time [" + (System.currentTimeMillis() - startTime) + "]");
            }
        }

    }

    class TestDBConcurrentPayLoad implements Runnable {

        private String myName;

        public TestDBConcurrentPayLoad(String myName, Tenant tenant) {
            this.myName = myName;
            UserContext.current().setTenant(tenant);
            Thread.currentThread().setName(myName);
            System.out.println("Starting " + myName);
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            long startTime1 = startTime;
            for (int i = 0; i < 100000; i++) { // inserts in a single thread
                testSaveActivityMetaLogObj();
                if (i > 0 && (i % 100 == 0)) {
                    System.out.println("Inserted 100 logs in [" + (System.currentTimeMillis() - startTime1) + "]");
                    startTime1 = System.currentTimeMillis();
                }
            }
            System.out.println(myName + " took [" + (System.currentTimeMillis() - startTime) / (1000) + "] secs");
        }

    }

    @Test
    public void testPayLoad() throws InterruptedException {

        for (int i = 0; i < 10000; i++) { // parallel threads
            executorService.execute(new TestDBConcurrentPayLoad("Thread_ID_" + i, UserContext.current().getTenant()));
        }
        Thread readThread = new Thread(new TestDBReadLatency());
        readThread.start();
        executorService.shutdown();
        if (executorService.awaitTermination(1000000, TimeUnit.SECONDS)) {
            System.out.println("All logs inserted");
        }
        readThread.interrupt();
    }
}
