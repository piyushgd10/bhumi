/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 30, 2012
 *  @author singla
 */
package com.uniware.services.reports;

import java.util.HashMap;
import java.util.Map;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.reports.GetWidgetDataRequest;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.services.report.IReportService;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestDashboardWidgetScript {

    @Autowired
    private IReportService     reportService;

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void loadDashboardConfiguration() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("singla");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));

    }

    @Test
    public void testScript() throws ScriptExecutionException, ScriptCompilationException {
        GetWidgetDataRequest request = new GetWidgetDataRequest();
        Map<String, String> map = new HashMap<String, String>();
        map.keySet().toArray(new String[0]);
        //        request.getRequestMap().put("daterange", new DateRange(TextRange.LAST_60_DAYS));
        //        request.getRequestMap().put("dateutils", new DateUtils());
        request.setWidgetCode("REVENUE_AND_PRODUCTS_BY_PERIOD");
        String jsonData = reportService.getWidgetData(request).getData();
        System.out.println(jsonData);
    }
}
