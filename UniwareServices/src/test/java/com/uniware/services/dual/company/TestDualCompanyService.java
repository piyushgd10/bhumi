/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.uniware.services.dual.company;

import java.io.IOException;

import org.jibx.runtime.JiBXException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.validation.WsError;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JibxUtils;
import com.uniware.core.api.dual.company.AutoCompletePutawayRequest;
import com.uniware.core.api.dual.company.AutoCompletePutawayResponse;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestDualCompanyService {

    @Autowired
    private IDualCompanyService dualCompanyService;

    @Before
    public void loadConfigurations() {

    }

    @Test
    public void testAutoCompletePutaway() throws JiBXException, IOException {
        AutoCompletePutawayRequest request = JibxUtils.unmarshall(AutoCompletePutawayRequest.class, FileUtils.getFileAsString("../1.xml"));
        AutoCompletePutawayResponse response = dualCompanyService.autoCompletePutaway(request);
        System.out.println(response.isSuccessful());
        if (!response.isSuccessful()) {
            for (WsError error : response.getErrors()) {
                System.out.println("Code:" + error.getCode() + ", Description:" + error.getDescription());
            }
        }
    }
}
