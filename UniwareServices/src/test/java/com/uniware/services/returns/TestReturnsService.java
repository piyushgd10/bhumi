/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 11, 2012
 *  @author singla
 */
package com.uniware.services.returns;

import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.returns.CreateReversePickupRequest;
import com.uniware.core.api.returns.ReshipShippingPackageRequest;
import com.uniware.core.api.returns.ReshipmentDTO;
import com.uniware.core.api.warehouse.GetReversePickupRequest;
import com.uniware.core.api.warehouse.GetReversePickupResponse;
import com.uniware.core.entity.ReversePickup;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestReturnsService {

    @Autowired
    IReturnsService            returnsService;

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("singla");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    public void testReversePickup() {
        CreateReversePickupRequest request = new CreateReversePickupRequest();
        request.setActionCode(ReversePickup.Action.REPLACE_IMMEDIATELY_EXPECT_RETURN.code());
    }

    @Test
    public void testPendingReshipmentsAction() {
        List<ReshipmentDTO> reshipments = returnsService.getPendingReshipments();
        System.out.println(reshipments);
    }

    @Test
    public void testSearchReversePickup() {
        GetReversePickupRequest request = new GetReversePickupRequest();
        request.setReversePickupCode("1");
        //request.setTrackingNumber("58576754736");
        GetReversePickupResponse response = returnsService.getReversePickup(request);
        System.out.println(response);
    }

    @Test
    public void reshipShippingPackage() {
        ReshipShippingPackageRequest request = new ReshipShippingPackageRequest();
        request.setSaleOrderCode("600035587");
        request.setShippingPackageCode("01P1083");
        System.out.println(returnsService.reshipShippingPackage(request));
    }

}
