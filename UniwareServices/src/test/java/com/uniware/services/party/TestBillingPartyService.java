/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.services.party;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.party.CreateBillingPartyRequest;
import com.uniware.core.api.party.CreateBillingPartyResponse;
import com.uniware.core.api.party.WsBillingParty;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.utils.UserContext;
import com.uniware.services.billing.party.IBillingPartyService;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestBillingPartyService {

    @Autowired
    private ITenantService       tenantService;

    @Autowired
    private ApplicationContext   applicationContext;

    @Autowired
    private IBillingPartyService billingPartyService;

    @Before
    public void loadCache() {
        UserContext.current().setTenant(tenantService.getTenantByCode("sunny"));
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @Test
    public void testCreateBillingParty() {
        CreateBillingPartyRequest request = new CreateBillingPartyRequest();
        WsBillingParty billingParty = new WsBillingParty();
        billingParty.setName("sunny");
        billingParty.setCode("sunny");
        billingParty.setPan("AOEPA212R");
        request.setBillingParty(billingParty);
        WsPartyAddress address = new WsPartyAddress();
        address.setAddressLine1("dasd");
        address.setCity("delhi");
        address.setStateCode("delhi");
        address.setPincode("110020");
        address.setPhone("9999999999");
        address.setAddressType("BILLING");
        address.setPartyCode("sunny");
        billingParty.setBillingAddress(address);
        WsPartyAddress address1 = new WsPartyAddress();
        address1.setAddressLine1("dasd");
        address1.setCity("delhi");
        address1.setStateCode("delhi");
        address1.setPincode("110020");
        address1.setPhone("9999999999");
        address1.setAddressType("SHIPPING");
        address1.setPartyCode("sunny");
        billingParty.setShippingAddress(address1);
        CreateBillingPartyResponse response = billingPartyService.createBillingParty(request);
        System.out.println(response.isSuccessful());
    }
}
