/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.usernotification;

import java.io.IOException;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.services.email.IEmailService;
import com.unifier.services.user.notification.IUserNotificationService;
import com.uniware.core.api.usernotification.AddUserNotificationRequest;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.UserNotificationTypeVO.NotificationType;
import com.uniware.services.tenant.ITenantService;

/**
 * @author Sunny
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestUserNotificationService {

    @Autowired
    private IUserNotificationService userNotificationService;

    @Autowired
    private ITenantService           tenantService;

    @Autowired
    private IEmailService            emailService;

    @Before
    public void testConfig() {
        Tenant t = tenantService.getTenantByCode("Pankaj");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void testNotification() throws IOException {
        userNotificationService.addNotification(new AddUserNotificationRequest("identifier", NotificationType.INVALID_VENDOR_EMAIL, "Invalid email sunny@sunn.com", null,
                "This is title."));
    }


    @Test
    public void testEmailBounce() {
        emailService.addEmailBounce("dsad@das.com", "hard bounce");
        System.out.println(emailService.isEmailInvalid("dsad@das.com"));
    }
}
