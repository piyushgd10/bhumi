/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-May-2012
 *  @author praveeng
 */

package com.uniware.services.lookup;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.uniware.core.api.inventory.ItemTypeInventoryDTO;
import com.uniware.core.api.inventory.SearchInventoryRequest;
import com.uniware.core.api.inventory.SearchInventoryResponse;
import com.uniware.core.api.item.GetItemTypeDetailRequest;
import com.uniware.core.api.item.GetItemTypeDetailResponse;

/**
 * @author praveeng
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestLookupService {

    @Autowired
    private ILookupService lookupService;

    public void searchInventory() {
        SearchInventoryRequest request = new SearchInventoryRequest();
        request.setItemType("stone");
        SearchInventoryResponse response = lookupService.searchInventory(request);
        List<ItemTypeInventoryDTO> list = response.getElements();
        for (ItemTypeInventoryDTO dto : list) {
            System.out.println("Name: " + dto.getItemTypeName() + "   Shelf: " + dto.getShelf() + " Quantity: " + dto.getQuantity());
        }
    }

    @Test
    public void getItemTypeDetails() {
        GetItemTypeDetailRequest request = new GetItemTypeDetailRequest();
        request.setItemSKU("156261");
        GetItemTypeDetailResponse response = lookupService.getItemTypeDetails(request);

        System.out.println("OpenPurchase " + response.getItemTypeDTO().getOpenPurchase());
        System.out.println("OpenSale " + response.getItemTypeDTO().getOpenSale());
        System.out.println("InPicking " + response.getItemTypeDTO().getInPicking());
        System.out.println("Inventory " + response.getItemTypeDTO().getInventory());
        System.out.println("PendingGRN " + response.getItemTypeDTO().getPendingGRN());
        System.out.println("PutawayPending " + response.getItemTypeDTO().getPutawayPending());
    }
}
