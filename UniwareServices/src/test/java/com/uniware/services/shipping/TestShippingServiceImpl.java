/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Apr-2013
 *  @author unicom
 */
package com.uniware.services.shipping;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import com.uniware.core.entity.Facility;
import org.apache.activemq.AsyncCallback;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.jms.MessagingConstants;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.JsonUtils;
import com.uniware.core.api.reconciliation.ReconciliationShippingPackageVO;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusRequest;
import com.uniware.core.api.shipping.UpdateShipmentTrackingStatusResponse;
import com.uniware.core.cache.TenantCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.messaging.jms.ActiveMQConnector;
import com.uniware.services.messaging.jms.ActiveMQConnector.UnifierMessage;
import com.uniware.services.messaging.jms.UniwareMessageListener;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestShippingServiceImpl {

    @Autowired
    private IShippingService shippingService;

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Autowired
    @Qualifier("activeMQConnector1")
    private ActiveMQConnector connector;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ITenantService tenantService;

    @Before
    public void loadStartupCache() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        UserContext.current().setTenant(tenantService.getTenantByCode("parijat"));
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void TestUpdateTrackingStatus() {
        UserContext.current().setFacility(new Facility(1));
        UpdateShipmentTrackingStatusRequest request = new UpdateShipmentTrackingStatusRequest();
        request.setShippingPackageCode("01P312");
        request.setStatusDate(DateUtils.getCurrentDate());
        request.setShipmentTrackingStatus("Akshay");
        UpdateShipmentTrackingStatusResponse response = shippingService.updateShipmentTrackingStatus(request);
        System.out.println(response);

    }

    @Test
    public void testMessaging() {
        try {
            registerReconciliationMessageListener();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 100; i++) {
            thread(new MessageProducer(("Producer Thread " + i), UserContext.current().getTenant(), UserContext.current().getFacilityId()), false);
        }
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    class MessageProducer implements Runnable {
        private String  myName;
        private Tenant  tenant;
        private Integer facilityId;

        public MessageProducer(String myName, Tenant tenant, Integer facilityId) {
            this.myName = myName;
            this.tenant = tenant;
            this.facilityId = facilityId;
        }

        @Override
        public void run() {
            UserContext.current().setTenant(tenant);
            UserContext.current().setFacility(new Facility(1));

            for (int i = 0; i < 10; i++) {
                ReconciliationShippingPackageVO reconciliationShippingPackageVO = new ReconciliationShippingPackageVO();
                reconciliationShippingPackageVO.setShippingPackageCode(myName + " SHIPPINGPACKAGECODE " + i);
                long starttime = System.currentTimeMillis();
                connector.produceMessage(MessagingConstants.Queue.TRANSFER_PRICE_RECONCILIATION_QUEUE, reconciliationShippingPackageVO);
                System.out.println(myName + "stat:" + "Time to produce {" + i + "}th message is " + (System.currentTimeMillis() - starttime));
            }
        }

    }

    abstract class UniwareAsyncCallBack<T> implements AsyncCallback {

        private final Logger logger = LoggerFactory.getLogger(UniwareAsyncCallBack.class);

        private Class<T>     clazz;
        private String      tenantCode;
        private Message      message;
        protected int        retryCount;

        public UniwareAsyncCallBack(Class<T> clazz, String tenantCode) {
            this.clazz = clazz;
            this.tenantCode = tenantCode;
            retryCount++;
        }

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }

        public Message getMessage() {
            return message;
        }

        public void setMessage(Message message) {
            this.message = message;
        }

        @Override
        public void onException(JMSException exception) {
            logger.error("Error handling message [{}]", exception.getMessage());
            UserContext.current().setTenant(CacheManager.getInstance().getCache(TenantCache.class).getActiveTenantByCode(tenantCode));
            if (message instanceof TextMessage) {
                try {
                    //                logger.info("Processing JMS for identifier [{}]", message.getStringProperty(MessagingConstants.MESSAGE_IDENTIFIER_KEY));
                    UnifierMessage unifierMessage = JsonUtils.stringToJson(((TextMessage) message).getText(), UnifierMessage.class);
                    T messageObject = JsonUtils.stringToJson(unifierMessage.getObjectJson(), clazz);
                    //Facility facility = CacheManager.getInstance().getCache(FacilityCache.class).getFacilityById(unifierMessage.getFacilityId());
                   // UserContext.current().setFacilityId(facility.getId());
                    handleException(messageObject);
                } catch (JMSException e) {
                    logger.error("Error in processing JMS message.", e);
                    throw new RuntimeException(e);
                }
            }
        }

        @Override
        public void onSuccess() {
            System.out.println("Successfully processed  message " + message);
        }

        public abstract void handleException(T messageObject);

    }

    class ReconciliationJMSAsyncCall extends UniwareAsyncCallBack<ReconciliationShippingPackageVO> {

        public ReconciliationJMSAsyncCall(String tenantCode) {
            super(ReconciliationShippingPackageVO.class, tenantCode);
        }

        @Override
        public void handleException(ReconciliationShippingPackageVO messageObject) {
            this.retryCount++;
            if (retryCount <= 5) {
                System.out.println("Retrying message post for [{" + retryCount + "}] time");
                connector.produceMessage(MessagingConstants.Queue.TRANSFER_PRICE_RECONCILIATION_QUEUE, messageObject);
            }
        }

    }

    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }

    private void registerReconciliationMessageListener() throws JMSException {
        UniwareMessageListener<ReconciliationShippingPackageVO> reconciliationMessageListener = new MyMessageListener();
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(reconciliationMessageListener, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        connector.createListener(MessagingConstants.Queue.TRANSFER_PRICE_RECONCILIATION_QUEUE, reconciliationMessageListener);
    }

    public class MyMessageListener extends UniwareMessageListener<ReconciliationShippingPackageVO> {

        public MyMessageListener() {
            super(ReconciliationShippingPackageVO.class);
        }

        @Override
        public String getListenerName() {
            return "MyMessageListener";
        }

        @Override
        public void processMessage(ReconciliationShippingPackageVO messageObject) {
            System.out.println(getListenerName() + ":" + messageObject.getShippingPackageCode());
        }

    }

}
