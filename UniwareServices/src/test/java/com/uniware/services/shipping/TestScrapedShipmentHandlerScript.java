/*
 *  Copyright 2012 Unicommerce Technologies (P) Limited . All Rights Reserved.
 *  UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jan 30, 2012
 *  @author singla
 */
package com.uniware.services.shipping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.exception.ScriptParseException;
import com.unifier.scraper.sl.exception.ScriptValidationException;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.services.shipping.scraper.ScrapedShipmentStatusHandler;

/**
 * @author singla
 */
public class TestScrapedShipmentHandlerScript {

    //@Test
    public void testOverniteScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-overnite.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("7057547523");
        System.out.print(trackingNumbers.get(0) + ":");
        testScript(filePath, trackingNumbers, null);
        trackingNumbers.clear();
    }

    //@Test
    public void testOndotScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-ondot.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("206655398");
        System.out.print(trackingNumbers.get(0) + ":");
        testScript(filePath, trackingNumbers, null);
        trackingNumbers.clear();
    }

    //@Test
    public void testSpeedPostScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-speedpost.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("");
        testScript(filePath, trackingNumbers, null);
        trackingNumbers.clear();
    }

    @Test
    public void testBlueDartScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-bluedart.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        shippingProviderParameters.put("LOGIN_ID", "GG374091");
        shippingProviderParameters.put("LICENCE_KEY", "d6fcf6da477f80f51a71663ac14e33a8");
        trackingNumbers.addAll(StringUtils.split("69001264360,69001264371,69001264382,69001264393,59108952055,59108953315,59108953422,59108953400,59108953326,59108953385,59108953470,59108953455,59108953481,69001265513,69001265535,59108953842,59108953772,59108953956,59108953890,59108953886,59108953761,59108953934,69001265410,69001265465,59108954435,59108954262,59108954214,59108953654,59108953610,59108954542,59108954531,59108954181,59108954166,59108954030,59108954041,59108954052,59108954424,59108954332,59108954461,59108954446,59108954634,59108954763,59108954752,59108954866,59108954844,59108954601,59108955054,59108955010,59108954575,69001265992"));
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    //    @Test
    public void testAramexScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-aramex.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        trackingNumbers.add("6285041991");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    //@Test
    public void testJavasScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-javas.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("DELDBN200131");
        testScript(filePath, trackingNumbers, null);
    }

    @Test
    public void testDtdcScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-dtdc.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("V12125456");
        testScript(filePath, trackingNumbers, null);
    }

    @Test
    public void testChhotuScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "/home/unicom/git/Uniware/UniwareResources/scripts/scraper-awb/scraper-fedex-tracking.xml";
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        shippingProviderParameters.put("KEY", "5QW9DFKl3K3GDfV1");
        shippingProviderParameters.put("PASSWORD", "4vl2GgCrMYd6QZCGWpC6vebxw");
        shippingProviderParameters.put("ACCOUNT_NUMBER", "391171416");
        shippingProviderParameters.put("METER_NUMBER", "105879456");
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("794827894224");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    @Test
    public void testAflScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-afl.xml";
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("861040706492540");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    @Test
    public void testDelhiveryScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-delhivery.xml";
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        shippingProviderParameters.put("LICENCE_KEY", "c602d4560ef1e4e27ecd509f9c3b050aa8ab4224");
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("12710022466");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    @Test
    public void testQuantium() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-quantium.xml";
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        List<String> trackingNumbers = new ArrayList<String>();
        trackingNumbers.add("DBNC01729");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    @Test
    public void testEcomExpressScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-ecom.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        shippingProviderParameters.put("USERNAME", "ecom");
        shippingProviderParameters.put("PASSWORD", "qw3nc76d0u");
        trackingNumbers.addAll(StringUtils.split("700399335,100070531,100070379,700407222,700406802,700448139"));
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    // @Test
    public void testCanadaPostScript() throws ScriptExecutionException, ScriptParseException, IOException, ScriptCompilationException, ScriptValidationException {
        String filePath = "../UniwareResources/scripts/scraper-awb/scraper-canadaPost.xml";
        List<String> trackingNumbers = new ArrayList<String>();
        Map<String, String> shippingProviderParameters = new HashMap<String, String>();
        shippingProviderParameters.put("AUTH_USERNAME", "76f35488f2bfa09d");
        shippingProviderParameters.put("AUTH_PASSWORD", "c1b580b0a4007b86b4067f");
        trackingNumbers.add("4001887204660875");
        testScript(filePath, trackingNumbers, shippingProviderParameters);
    }

    public void testScript(String filePath, List<String> trackingNumbers, Map<String, String> shippingProviderParameters) throws ScriptParseException, IOException,
            ScriptCompilationException, ScriptExecutionException, ScriptValidationException {

        ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString(filePath));
        pScript.validate();
        ScriptExecutionContext context = ScriptExecutionContext.current();
        context.addVariable("trackingNumbers", trackingNumbers);
        context.addVariable("shippingProviderParameters", shippingProviderParameters);

        try {
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            String output = ScriptExecutionContext.current().getScriptOutput();
            if (StringUtils.isNotEmpty(output)) {
                ScrapedShipmentStatusHandler handler = new ScrapedShipmentStatusHandler();
                Map<String, ProviderShipmentStatus> map = handler.parse(output);
                System.out.println(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println(ScriptExecutionContext.current().getScriptOutput());
            ScriptExecutionContext.destroy();
        }
    }

}
