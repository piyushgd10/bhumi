/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 15, 2012
 *  @author singla
 */
package com.uniware.services.shipping;

import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.catalog.SearchShippingProviderLocationRequest;
import com.uniware.core.api.catalog.SearchShippingProviderLocationResponse;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelRequest;
import com.uniware.core.api.shipping.CreateInvoiceAndGenerateLabelResponse;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.configuration.ShippingConfiguration;
import com.uniware.services.exception.NoAvailableShippingProviderException;
import com.uniware.services.exception.NoAvailableTrackingNumberException;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestShippingProviderService {

    @Autowired
    private IShippingProviderService shippingProviderService;

    @Autowired
    private IShippingService         shippingService;

    @Autowired
    private IShipmentTrackingService shipmentTrackingService;

    @Autowired
    private ITenantService           tenantService;

    @Autowired
    private ApplicationContext       applicationContext;

    @Before
    public void loadStartupCache() {
        Tenant t = tenantService.getTenantByCode("staging");
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);

        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    public void testHandlerConstruction() throws NoAvailableTrackingNumberException {
        ShippingProvider shippingProvider = ConfigurationManager.getInstance().getConfiguration(ShippingConfiguration.class).getShippingProviderByCode("FF");
        ShippingPackage shippingPackage = shippingService.getShippingPackageById(1);
        shippingPackage.setShippingProviderCode(shippingProvider.getCode());
        shippingPackage.setShippingProviderName(shippingProvider.getName());
        System.out.println(shippingProviderService.generateTrackingNumber(shippingPackage));
    }

    @Test
    public void testShippingProviderLocation() throws NoAvailableShippingProviderException {
        List<ShippingProvider> locations = shippingProviderService.getAvailableShippingProviders(1, "", null);
        System.out.println(locations);
    }

    @Test
    public void testShippingProviderLocationSearch() {
        SearchShippingProviderLocationRequest request = new SearchShippingProviderLocationRequest();
        SearchShippingProviderLocationResponse response = shippingProviderService.searchShippingProviderLocation(request);
        System.out.println(response.getTotalRecords());
    }

    @Test
    public void testCreateInvoiceAndGenerateLabel() {
        CreateInvoiceAndGenerateLabelRequest request = new CreateInvoiceAndGenerateLabelRequest();
        request.setShippingPackageCode("01P1015");
        request.setUserId(72);
        CreateInvoiceAndGenerateLabelResponse response = shippingProviderService.createInvoiceAndGenerateLabel(request);
        System.out.println(response.getLabel());
    }
}
