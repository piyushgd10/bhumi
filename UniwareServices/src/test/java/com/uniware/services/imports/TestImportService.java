package com.uniware.services.imports;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.services.imports.IImportService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestImportService {

    @Autowired
    private IImportService importService;

    @Test
    public void testExport() throws IOException {
        importService.getImportJobById("1");
    }

}
