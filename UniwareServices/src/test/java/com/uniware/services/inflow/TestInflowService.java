/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.services.inflow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.pagination.SearchOptions;
import com.uniware.core.api.inflow.CreateInflowReceiptRequest;
import com.uniware.core.api.inflow.CreateInflowReceiptResponse;
import com.uniware.core.api.inflow.GetInflowReceiptRequest;
import com.uniware.core.api.inflow.GetInflowReceiptResponse;
import com.uniware.core.api.inflow.SearchReceiptRequest;
import com.uniware.core.api.inflow.SearchReceiptResponse;
import com.uniware.core.api.inflow.SearchReceiptResponse.WsInflowReceipt;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestInflowService {
    @Autowired
    private IInflowService     inflowService;

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("sunny");
        UserContext.current().setTenant(t);
    }

    //@Test
    public void testSearchInflow() {
        SearchReceiptRequest request = new SearchReceiptRequest();
        request.setInflowReceiptCode("1");
        SearchOptions options = new SearchOptions();
        options.setDisplayLength(50);
        options.setDisplayStart(0);
        options.setGetCount(true);

        request.setSearchOptions(options);
        SearchReceiptResponse response = inflowService.searchReceipt(request);
        for (WsInflowReceipt dto : response.getElements()) {
            System.out.println(dto.getCode());
        }

    }

    @Test
    public void getInflowReceipt() {
        GetInflowReceiptRequest request = new GetInflowReceiptRequest();
        request.setInflowReceiptCode("GM1");

        GetInflowReceiptResponse response = inflowService.getInflowReceipt(request);
        System.out.println(response);
    }

    //@Test
    public void receivePurchaseOrder() {
        CreateInflowReceiptRequest request = new CreateInflowReceiptRequest();
        request.setPurchaseOrderCode("1");

        CreateInflowReceiptResponse response = inflowService.createInflowReceipt(request);
        System.out.println(response.getInflowReceiptCode());
    }

}
