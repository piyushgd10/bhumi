/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.services.inflow;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.inflow.AddInflowReceiptToVendorInvoiceRequest;
import com.uniware.core.api.inflow.CreateVendorCreditInvoiceRequest;
import com.uniware.core.api.inflow.WsVendorCreditInvoice;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestVendorInvoiceService {
    @Autowired
    private IInflowService        inflowService;

    @Autowired
    private ITenantService        tenantService;

    @Autowired
    private ApplicationContext    applicationContext;

    @Autowired
    private IVendorInvoiceService vendorInvoiceService;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("singla");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void createVendorInvoice() {
        CreateVendorCreditInvoiceRequest request = new CreateVendorCreditInvoiceRequest();
        WsVendorCreditInvoice vendorInvoice = new WsVendorCreditInvoice();
        vendorInvoice.setPurchaseOrderCode("66217");
        vendorInvoice.setVendorInvoiceDate(DateUtils.getCurrentDate());
        vendorInvoice.setVendorInvoiceNumber("121213");
        request.setUserId(1);
        request.setVendorInvoice(vendorInvoice);
        System.out.println(vendorInvoiceService.createCreditVendorInvoice(request));
    }

    @Test
    public void addInflowReceiptToVendorInvoice() {
        AddInflowReceiptToVendorInvoiceRequest request = new AddInflowReceiptToVendorInvoiceRequest();
        request.setInflowReceiptCode("G1677");
        request.setPurchaseOrderCode("66217");
        request.setVendorInvoiceCode("VI-01-00003");
        System.out.println(vendorInvoiceService.addInflowReceiptToVendorInvoice(request));

    }
}
