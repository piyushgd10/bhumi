/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 29, 2011
 *  @author singla
 */
package com.uniware.services.picker;

import java.util.ArrayList;
import java.util.List;

import com.uniware.core.api.picker.CreateManualPicklistRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.picker.CreatePicklistResponse;
import com.uniware.core.api.picker.PicklistDTO;
import com.uniware.core.api.picker.PicklistItemDTO;
import com.uniware.core.api.picker.SearchPicklistRequest;
import com.uniware.core.api.picker.SearchPicklistResponse;
import com.uniware.core.entity.Picklist;

/**
 * @author singla
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestPickerService {
    @Autowired
    private IPickerService pickerService;

    @Before
    public void loadStartupCache() {

    }

    @Test
    public void testGetPicklistForShippingPackage() {
        Picklist picklist = pickerService.getPicklistForShippingPackage("100009");
        System.out.println(picklist.getId());
    }

    @Test
    public void testCreatePicklist() {
        //        bucketCodes.add("STDTEST2");
//        CreateManualPicklistRequest picklistRequest = new CreateManualPicklistRequest(10, 1, 1);
//        CreatePicklistResponse response = pickerService.createPicklist(picklistRequest);
//        System.out.println(response);
//        for (PicklistItemDTO picklistItemDTO : response.getPicklist().getPicklistItems()) {
//            System.out.println(picklistItemDTO.getItemName());
//        }
    }

    @Test
    public void testSearchPicklist() {
        SearchPicklistRequest request = new SearchPicklistRequest();
        request.setFromDate(DateUtils.createDate(2011, 11, 12));
        request.setToDate(DateUtils.createDate(2012, 1, 23));
        List<String> statuses = new ArrayList<String>();
        statuses.add(Picklist.StatusCode.CREATED.name());
        request.setStatusCodes(statuses);
        SearchPicklistResponse response = pickerService.searchPicklist(request);
        for (PicklistDTO picklist : response.getElements()) {
            System.out.println(picklist.getId() + "," + picklist.getCreated());
        }

    }
}
