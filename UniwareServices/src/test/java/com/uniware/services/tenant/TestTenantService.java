/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 26, 2011
 *  @author singla
 */
package com.uniware.services.tenant;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.utils.JsonUtils;
import com.uniware.core.api.facility.CreateFacilityRequest;
import com.uniware.core.api.facility.CreateFacilityResponse;
import com.uniware.core.api.facility.GetSellerDetailsFromTinRequest;
import com.uniware.core.api.facility.GetSellerDetailsFromTinResponse;
import com.uniware.core.api.party.WsFacility;
import com.uniware.core.api.party.WsPartyAddress;
import com.uniware.core.api.party.WsPartyContact;
import com.uniware.core.api.tenant.CreateTenantRequest;
import com.uniware.core.api.tenant.CreateTenantResponse;
import com.uniware.core.entity.Product;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.warehouse.IFacilityService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestTenantService {

    @Autowired
    private ITenantSetupService tenantSetupService;

    @Autowired
    private IFacilityService    facilityService;

    @Autowired
    private ApplicationContext  applicationContext;

    @Before
    public void loadConfigurations() {
        Product p = new Product();
        p.setId(1);
        p.setCode("PROFESSIONAL");
        p.setName("Professional");
        Tenant t = new Tenant(2);
        t.setProduct(p);
        UserContext.current().setTenant(t);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @Test
    public void testCreateNewTenant() {
        CreateTenantRequest request = new CreateTenantRequest();
        request.setAccessUrl("harsh5.unicommerce.com");
        request.setCode("harsh5");
        request.setUsername("harsh15@unicommerce.com");
        request.setApiUsername("system");
        request.setApiPassword("unicom");
        request.setMode("TESTING");
        request.setName("HarshKapila1");
        request.setPassword("unicom");
        request.setConfirmPassword("unicom");
        request.setEmail("harsh@unicommerce.com");
        CreateTenantResponse response = tenantSetupService.createTenant(request);
        System.out.println(response.getErrors());
        System.out.println(response);
    }

    @Test
    public void testCreateNewFacility() {
        Product p = new Product();
        p.setId(1);
        p.setCode("STANDARD");
        p.setName("Standard");
        Tenant t = new Tenant(109);
        t.setProduct(p);
        UserContext.current().setTenant(t);
        CreateFacilityRequest request = new CreateFacilityRequest();
        UserContext.current().getTenant().setCode("harsh5");
        WsFacility facility = new WsFacility();
        facility.setType("WAREHOUSE");
        facility.setName("harsh4");
        facility.setDisplayName("Gurgaon Sector 14");
        facility.setCode("harsh5");
        facility.setEnabled(true);

        WsPartyContact contact = new WsPartyContact();
        contact.setContactType("PRIMARY");
        contact.setName("Harsh");
        contact.setEmail("harsh@unicommerce.com");
        contact.setPhone("9818074790");
        facility.getPartyContacts().add(contact);

        WsPartyAddress addr = new WsPartyAddress();
        WsPartyAddress addr1 = new WsPartyAddress();
        addr.setAddressLine1("Sector-29");
        addr.setCity("Gurgaon");
        addr.setStateCode("HARYANA");
        addr.setAddressType("SHIPPING");
        addr.setPincode("122001");
        addr.setPhone("9818074790");
        addr1.setAddressLine1("Sector-14");
        addr1.setCity("Gurgaon");
        addr1.setStateCode("HARYANA");
        addr1.setAddressType("BILLING");
        addr1.setPincode("122002");
        addr1.setPhone("9818074790");
        facility.setShippingAddress(addr);
        facility.setBillingAddress(addr1);
        request.setFacility(facility);
        request.setUsername("harsh15@unicommerce.com");
        CreateFacilityResponse response = facilityService.createFacility(request);
        System.out.println(response.isSuccessful());
        System.out.println(response.getErrors());
    }

    public static void main(String[] args) {
        CreateTenantRequest request = new CreateTenantRequest();
        request.setAccessUrl("harsh5.unicommerce.com");
        request.setCode("harsh5");
        request.setUsername("harsh15@unicommerce.com");
        request.setApiUsername("system");
        request.setApiPassword("unicom");
        request.setMode("TESTING");
        request.setName("HarshKapila1");
        request.setPassword("unicom");
        request.setConfirmPassword("unicom");
        request.setEmail("harsh@unicommerce.com");
        Product p = new Product();
        p.setId(1);
        p.setCode("STANDARD");
        p.setName("Standard");
        Tenant t = new Tenant(109);
        t.setProduct(p);
        UserContext.current().setTenant(t);
        UserContext.current().getTenant().setCode("harsh5");
        WsFacility facility = new WsFacility();
        facility.setType("WAREHOUSE");
        facility.setName("harsh4");
        facility.setDisplayName("Gurgaon Sector 14");
        facility.setCode("harsh5");
        facility.setEnabled(true);

        WsPartyContact contact = new WsPartyContact();
        contact.setContactType("PRIMARY");
        contact.setName("Harsh");
        contact.setEmail("harsh@unicommerce.com");
        contact.setPhone("9818074790");
        facility.getPartyContacts().add(contact);

        WsPartyAddress addr = new WsPartyAddress();
        WsPartyAddress addr1 = new WsPartyAddress();
        addr.setAddressLine1("Sector-29");
        addr.setCity("Gurgaon");
        addr.setStateCode("HARYANA");
        addr.setAddressType("SHIPPING");
        addr.setPincode("122001");
        addr.setPhone("9818074790");
        addr1.setAddressLine1("Sector-14");
        addr1.setCity("Gurgaon");
        addr1.setStateCode("HARYANA");
        addr1.setAddressType("BILLING");
        addr1.setPincode("122002");
        addr1.setPhone("9818074790");
        facility.setBillingAddress(addr1);
        facility.setShippingAddress(addr);
        request.setFacility(facility);
        System.out.println(JsonUtils.objectToString(request));

    }
    
    @Test
    public void getTinInformation() {
        GetSellerDetailsFromTinRequest request = new GetSellerDetailsFromTinRequest();
        request.setTin("3456789056");
        GetSellerDetailsFromTinResponse response = facilityService.getSellerDetailsUsingTin(request);
    }
}
