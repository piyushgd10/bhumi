/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIALUse is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */
package com.uniware.services.user;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.unifier.core.api.user.AddOrEditUserDetailRequest;
import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.access.GrantApiAccessToVendorRequest;
import com.unifier.core.api.access.GrantApiAccessToVendorResponse;
import com.unifier.core.api.token.GetLoginTokenRequest;
import com.unifier.core.api.token.GetLoginTokenResponse;
import com.unifier.core.api.user.CreateApiUserRequest;
import com.unifier.core.api.user.CreateApiUserResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.unifier.core.entity.User;
import com.unifier.services.users.IUsersService;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestUserService {

    @Autowired
    private IUsersService      usersService;

    @Autowired
    private ITenantService     tenantService;

    @Autowired
    private ApplicationContext context;

    @Before
    public void loadCache() {
        //baseStartupService.loadRoles();
        UserContext.current().setFacility(new Facility(1));
        context.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        context.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        UserContext.current().setTenant(tenantService.getTenantByCode("sunny"));
    }

    @Test
    public void testToken() {
        GetLoginTokenRequest request = new GetLoginTokenRequest();
        request.setSupportUsername("rahul");
        request.setUserId(1);
        GetLoginTokenResponse response = usersService.getLoginToken(request);
        System.out.println(response.getToken());
        String[] values = usersService.decodeToken(response.getToken());
        User user = usersService.getUserWithDetailsById(1);
        String sig = usersService.makeTokenSignature(new Long(values[2]).longValue(), user.getUsername());
        System.out.println(sig);
        System.out.println(sig.equals(values[3]));
    }

    @Test
    public void addApiUser() {
        GrantApiAccessToVendorRequest request = new GrantApiAccessToVendorRequest();
        request.setChannel("uniware");
        request.setUserName("vendor1");
        request.setRedirectUrl("www.uniware.com");
        GrantApiAccessToVendorResponse response = usersService.grantApiAccess(request);
        System.out.println(response.isSuccessful());
        System.out.println(response.getRedirectUrl());
    }

    @Test
    public void createApiUser() {
        CreateApiUserRequest request = new CreateApiUserRequest();
        User user = usersService.getUserByUsername("triveni");
        request.setUserId(user.getId());
        CreateApiUserResponse response = usersService.createApiUser(request);
        System.out.println(response.isSuccessful());
    }

    @Test
    public void getUserByEmail() {
        User user = usersService.getUserByUsername("karun@unicommerce.com");
        assertEquals(true, user != null);
    }

    /*@Test
    public void searchUsers() {
        List<UserDTO> users = usersService.searchUsers("", null, null);
        //UserDTO user = users.get(0);
                for (UserRole userRole : user.getUserRoles()) {
                    System.out.println(userRole.getRole().getId());
                }
        
        assertEquals(users.size(), 1);
    }*/

    @Test
    public void editUser() {
        String username = "dispatcher1@unicommerce.com";
        AddOrEditUserDetailRequest request = new AddOrEditUserDetailRequest();
        List<String> roles = new ArrayList<String>();
        roles.add("SUPER");
        roles.add("PICKER");
        usersService.editUserDetail(request);
        System.out.println(usersService.getUserByUsername(username));
    }
}
