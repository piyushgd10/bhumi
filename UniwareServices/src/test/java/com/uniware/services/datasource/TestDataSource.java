package com.uniware.services.datasource;

import com.uniware.core.vo.DataSourceConfigurationVO;
import com.uniware.dao.datasource.IDataSourceMao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestDataSource {

    @Autowired
    private IDataSourceMao dataSourceMao;

    @Test
    public void createDataSource() {
        DataSourceConfigurationVO ds = new DataSourceConfigurationVO();
        // <Resource name="jdbc/UniwareDS"
        // auth="Container"
        // type="javax.sql.DataSource"
        // maxActive="100"
        // maxIdle="20"
        // maxWait="15000"
        // username="root"
        // password="uniware"
        // driverClassName="com.mysql.jdbc.Driver"
        // removeAbandoned="true"
        // removeAbandonedTimeout="30"
        // url="jdbc:mysql://localhost:3306/uniware?autoReconnect=true&amp;characterEncoding=UTF-8"/>
        ds.setServerName("sunny");
        ds.setUrl("jdbc:mysql://localhost:3306/uniware?autoReconnect=true&amp;characterEncoding=UTF-8");
        //ds.setMaxActive(100);
        ds.setMaxIdle(20);
        ds.setMaxWaitMillis(15000);
        ds.setUsername("root");
        ds.setPassword("uniware");
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        //ds.setRemoveAbandoned(true);
        ds.setRemoveAbandonedTimeout(30);
        dataSourceMao.save(ds);
    }
    

}
