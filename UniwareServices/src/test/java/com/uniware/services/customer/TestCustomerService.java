/**
 * 
 */
package com.uniware.services.customer;

import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.uniware.core.api.party.dto.CustomerDTO;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

/**
 * @author parijat
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestCustomerService {

    @Autowired
    private ICustomerService customerService;

    @Before
    public void loadStartupCache() {
        UserContext.current().setTenant(new Tenant(1));
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void testGetCustomersByCodeEmailOrMobile() {
        List<CustomerDTO> customers = customerService.getCustomersByCodeEmailOrMobile("test1");
        Assert.assertNotNull(customers);
    }
}
