/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARYARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 26, 2011
 *  @author singla
 */
package com.uniware.services.tasks.saleorder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestSaleOrderProcessor {

    @Autowired
    private SaleOrderProcessor saleOrderProcessor;

    @Before
    public void loadConfigurations() {
    }

    @Test
    public void testExecute() {
       // saleOrderProcessor.execute();
    }
}
