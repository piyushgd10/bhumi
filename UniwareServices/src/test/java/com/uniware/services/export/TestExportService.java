/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 18, 2012
 *  @author singla
 */
package com.uniware.services.export;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.datatable.GetDatatableResultRequest;
import com.unifier.core.api.datatable.GetDatatableResultResponse;
import com.unifier.core.api.datatable.WsSortColumn;
import com.unifier.core.api.export.CloneExportJobTypeRequest;
import com.unifier.core.api.export.WsExportFilter;
import com.unifier.core.cache.CacheManager;
import com.unifier.dao.export.IExportMao;
import com.unifier.services.export.IExportService;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.SaleOrderVO;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestExportService {

    @Autowired
    private IExportService exportService;

    @Autowired
    private IExportMao     exportMao;

    @Autowired
    private ITenantService tenantService;

    @Before
    public void testConfig() {
        Tenant t = tenantService.getTenantByCode("staging");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
        EnvironmentPropertiesCache propertiesCache = new EnvironmentPropertiesCache();
        //propertiesCache.addProperty(EnvironmentPropertiesCache.APP_ROOT_PATH, null);
        CacheManager.getInstance().setCache(propertiesCache);
    }

    //@Test
    public void testInQuery() {
        List<String> channels = new ArrayList<String>();
        //channels.add("SNAPDEAL");
        //channels.add("FLIPKART");
        channels.add("SHOPCLUES_FC");
        Query query = new Query(Criteria.where("request.saleOrder.channel").in(channels));
        System.out.println(exportMao.executeCountQuery(query, SaleOrderVO.class));
    }

    //@Test
    public void testNoSqlDatatable() throws IOException {
        GetDatatableResultRequest request = new GetDatatableResultRequest();
        String name = "DATATABLE UNSUCCESSFUL ORDERS";
        request.setName(name);
        List<String> columns = new ArrayList<String>();
        columns.add("orderCode");
        columns.add("created");
        columns.add("channel");
        request.setColumns(columns);
        int start = 0;
        request.setStart(start);
        int noOfResults = 10;
        request.setNoOfResults(noOfResults);
        request.setUserId(1);
        List<WsExportFilter> filters = new ArrayList<WsExportFilter>();
        WsExportFilter codeFilter = new WsExportFilter();
        codeFilter.setId("orderCodeFilter");
        codeFilter.setText("12345");
        filters.add(codeFilter);
        request.setFilters(filters);
        List<WsSortColumn> sortColumns = new ArrayList<WsSortColumn>();
        sortColumns.add(new WsSortColumn("code", false));
        request.setSortColumns(sortColumns);
        request.setFetchResultCount(true);
        GetDatatableResultResponse response = exportService.getDatatableResult(request);
        System.out.println(response.getRows());
        System.out.println(response.getResultCount());
    }

    @Test
    public void testCloneExportJobType() {
        CloneExportJobTypeRequest request = new CloneExportJobTypeRequest();
        request.setExportJobTypeName("Copy of Back Orders Vendor");
        request.setCloneExportJobTypeName("Copy of Back Orders Vendor");
        exportService.cloneExportJobType(request);
    }

}
