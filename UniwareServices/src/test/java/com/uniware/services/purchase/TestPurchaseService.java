/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Mar 2, 2012
 *  @author singla
 */
package com.uniware.services.purchase;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.entity.User;
import com.unifier.core.utils.DateUtils;
import com.unifier.core.utils.DateUtils.DateRange;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest;
import com.uniware.core.api.purchase.AddItemsToPurchaseCartRequest.WsPurchaseCartItem;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsRequest;
import com.uniware.core.api.purchase.AddOrEditPurchaseOrderItemsResponse;
import com.uniware.core.api.purchase.AmendPurchaseOrderRequest;
import com.uniware.core.api.purchase.AmendPurchaseOrderResponse;
import com.uniware.core.api.purchase.ApprovePurchaseOrderRequest;
import com.uniware.core.api.purchase.ApprovePurchaseOrderResponse;
import com.uniware.core.api.purchase.CreatePurchaseOrderRequest;
import com.uniware.core.api.purchase.CreatePurchaseOrderResponse;
import com.uniware.core.api.purchase.GetPurchaseOrderRequest;
import com.uniware.core.api.purchase.GetPurchaseOrderResponse;
import com.uniware.core.api.purchase.GetReorderItemsRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrderDetailResponse;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersRequest;
import com.uniware.core.api.purchase.GetVendorPurchaseOrdersResponse;
import com.uniware.core.api.purchase.PurchaseOrderDTO;
import com.uniware.core.api.purchase.SearchItemTypeForCartRequest;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse;
import com.uniware.core.api.purchase.SearchItemTypeForCartResponse.ItemTypeCartDTO;
import com.uniware.core.api.purchase.SearchPurchaseOrderRequest;
import com.uniware.core.api.purchase.SearchPurchaseOrderResponse;
import com.uniware.core.api.purchase.WsPurchaseOrderItem;
import com.uniware.core.entity.Vendor;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestPurchaseService {
    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private ITenantService   tenantService;

    @Autowired
    private IUsersService    usersService;

    @Before
    public void loadCache() {
        UserContext.current().setTenant(tenantService.getTenantByCode("Pankaj"));
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void getVendorPurchaseOrders() {
        UserContext.current().setTenant(tenantService.getTenantByCode("staging"));
        GetVendorPurchaseOrdersRequest request = new GetVendorPurchaseOrdersRequest();
        User user = usersService.getUserByUsername("karun@unicommerce.com");
        List<Integer> vendorIds = new ArrayList<Integer>();
        for (Vendor vendor : user.getVendors()) {
            vendorIds.add(vendor.getId());
        }
        DateRange range = new DateRange(DateUtils.addToDate(DateUtils.getCurrentDate(), Calendar.DATE, -300), DateUtils.getCurrentTime());
        request.setApprovedBetween(range);
        GetVendorPurchaseOrdersResponse response = purchaseService.getVendorPurchaseOrders(request);
        System.out.println(response.getPurchaseOrderCodes());
    }

    @Test
    public void getPurchaseOrderByCode() {
        GetVendorPurchaseOrderDetailRequest request = new GetVendorPurchaseOrderDetailRequest();
        request.setPurchaseOrderCode("NAES/12-13/1");
        GetVendorPurchaseOrderDetailResponse response = purchaseService.getVendorPurchaseOrderDetail(request);
        System.out.println(response.getPurchaseOrderDetailDTO().getPartyAddressDTO());
    }

    @Test
    public void testSearchPurchaseOrders() {
        SearchPurchaseOrderRequest request = new SearchPurchaseOrderRequest();
        request.setPurchaseOrderCode("10000");
        SearchPurchaseOrderResponse response = purchaseService.searchPurchaseOrders(request);
        for (PurchaseOrderDTO purchaseOrder : response.getElements()) {
            System.out.println(purchaseOrder.getCode());
        }
    }

    @Test
    public void testSearchItemType() {
        SearchItemTypeForCartRequest request = new SearchItemTypeForCartRequest();
        request.setVendorId(10);
        SearchItemTypeForCartResponse response = purchaseService.searchItemType(request);
        for (ItemTypeCartDTO dto : response.getItemTypes()) {
            System.out.println(dto.getName());
        }
    }

    public void createPurchaseOrder() {
        CreatePurchaseOrderRequest request = new CreatePurchaseOrderRequest();
        request.setVendorCode("V002");
        request.setVendorAgreementName("1");
        CreatePurchaseOrderResponse response = purchaseService.createPurchaseOrder(request);
        System.out.println(response.getErrors());
    }

    public void addPurchaseOrderItem() {
        AddOrEditPurchaseOrderItemsRequest request = new AddOrEditPurchaseOrderItemsRequest();
        request.setPurchaseOrderCode("1");
        WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
        wsPurchaseOrderItem.setItemSKU("21st-mmx-Q50-Wht213");
        wsPurchaseOrderItem.setQuantity(100);
        List<WsPurchaseOrderItem> wsPurchaseOrderItems = new ArrayList<WsPurchaseOrderItem>();
        wsPurchaseOrderItems.add(wsPurchaseOrderItem);
        request.setPurchaseOrderItems(wsPurchaseOrderItems);
        AddOrEditPurchaseOrderItemsResponse response = purchaseService.addOrEditPurchaseOrderItems(request);
        System.out.println(response.getErrors());
    }

    public void editPurchaseOrderItem() {
        AddOrEditPurchaseOrderItemsRequest request = new AddOrEditPurchaseOrderItemsRequest();
        request.setPurchaseOrderCode("1");
        WsPurchaseOrderItem wsPurchaseOrderItem = new WsPurchaseOrderItem();
        wsPurchaseOrderItem.setItemSKU("21st-mmx-Q50-Wht213");
        wsPurchaseOrderItem.setQuantity(20);
        request.getPurchaseOrderItems().add(wsPurchaseOrderItem);
        AddOrEditPurchaseOrderItemsResponse response = purchaseService.addOrEditPurchaseOrderItems(request);
        System.out.println(response.getErrors());
    }

    public void approvePurchaseOrder() throws FileNotFoundException {
        ApprovePurchaseOrderRequest request = new ApprovePurchaseOrderRequest();
        request.setPurchaseOrderCode("1");
        ApprovePurchaseOrderResponse response = purchaseService.approvePurchaseOrder(request);
        System.out.println(response.getErrors());
    }

    public void addItemsToPurchaseCart() {
        AddItemsToPurchaseCartRequest request = new AddItemsToPurchaseCartRequest();
        request.setUserId(1);
        List<WsPurchaseCartItem> purchaseCartItems = new ArrayList<AddItemsToPurchaseCartRequest.WsPurchaseCartItem>();
        WsPurchaseCartItem wsPurchaseCartItem = new WsPurchaseCartItem();
        wsPurchaseCartItem.setItemSku("003/AAA00003063w");
        wsPurchaseCartItem.setVendorCode("XYZ");
        wsPurchaseCartItem.setQuantity(4);
        wsPurchaseCartItem.setUnitPrice(new BigDecimal(800));
        purchaseCartItems.add(wsPurchaseCartItem);
        request.setPurchaseCartItems(purchaseCartItems);
        purchaseService.addItemsToPurchaseCart(request);
    }

    @SuppressWarnings("unused")
    @Test
    public void testGetPurchaseOrder() {
        GetPurchaseOrderRequest request = new GetPurchaseOrderRequest();
        request.setPurchaseOrderCode("10001");
        GetPurchaseOrderResponse response = purchaseService.getPurchaseOrderDetail(request);
    }

    @Test
    public void testAmendPurchaseOrder() {
        AmendPurchaseOrderRequest request = new AmendPurchaseOrderRequest();
        request.setPurchaseOrderCode("12070");
        List<WsPurchaseOrderItem> purchaseOrderItems = new ArrayList<WsPurchaseOrderItem>();
        WsPurchaseOrderItem poi = new WsPurchaseOrderItem();
        poi.setItemSKU("1128133");
        poi.setQuantity(4);
        purchaseOrderItems.add(poi);
        request.setPurchaseOrderItems(purchaseOrderItems);
        request.setUserId(1);
        AmendPurchaseOrderResponse response = purchaseService.amendPurchaseOrder(request);
        System.out.println(response.isSuccessful());
    }

    @Test
    public void testGetReordersNew() {
        GetReorderItemsRequest request = new GetReorderItemsRequest();
        //        request.setItemTypeName("GLWCLS016");
        request.setVendorId(123);
        purchaseService.getReorderItemsNew(request);
    }
}
