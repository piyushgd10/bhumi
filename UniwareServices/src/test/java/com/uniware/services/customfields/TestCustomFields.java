/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Jun 21, 2012
 *  @author singla
 */
package com.uniware.services.customfields;

import com.uniware.core.entity.Facility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.services.utils.CustomFieldUtils;
import com.uniware.core.api.saleorder.GetSaleOrderRequest;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestCustomFields {

    @Autowired
    private ISaleOrderService saleOrderService;

    @Before
    public void load() {
        UserContext.current().setTenant(new Tenant(1));
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void addCustomField() {
        GetSaleOrderRequest request = new GetSaleOrderRequest();
        request.setCode("561154654");
        SaleOrder so = saleOrderService.getSaleOrderById(2);
        CustomFieldUtils.setCustomFieldValue(so, "custom", "test1");
        saleOrderService.updateSaleOrder(so);
        SaleOrder so1 = saleOrderService.getSaleOrderById(2);
        System.out.println(CustomFieldUtils.getCustomFieldValues(so1));
    }
}
