/*
 *  Copyright 2015 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-May-2015
 *  @author parijat
 */
package com.uniware.services.location;

import java.beans.IntrospectionException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.location.SyncPincodeLatLongRequest;
import com.uniware.core.api.location.SyncPincodeLatLongResponse;
import com.uniware.core.entity.Location;
import com.uniware.core.utils.UserContext;
import com.uniware.services.tenant.ITenantService;

/**
 * @author parijat
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestPincodeSyncLatLongService {

    @Autowired
    private ITenantService             tenantService;

    @Autowired
    private ApplicationContext         applicationContext;

    @Autowired
    private IPincodeLatLongSyncService pincodeLatLongSyncService;

    @Autowired
    private ILocationService           locationService;

    @Before
    public void loadCache() throws SecurityException, ClassNotFoundException, NoSuchMethodException, IntrospectionException {
        UserContext.current().setTenant(tenantService.getTenantByCode("parijat"));
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
    }

    @Test
    public void testLatLongSync() {
        List<Location> locations = locationService.getLocations(0, 10);
        SyncPincodeLatLongRequest request = new SyncPincodeLatLongRequest();
        request.setLocations(locations);
        SyncPincodeLatLongResponse response = pincodeLatLongSyncService.syncPincodesLatLong(request);
        System.out.println("Sucess " + response.getSuccessCount() + " Failed " + response.getFailCount() + " Errors : " + response.getErrorMessages());
    }
}
