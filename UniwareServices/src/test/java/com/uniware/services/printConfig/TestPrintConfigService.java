/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */
package com.uniware.services.printConfig;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.print.AddPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateRequest;
import com.unifier.core.api.print.EditPrintTemplateResponse;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeRequest;
import com.unifier.core.api.print.GetSamplePrintTemplatesByTypeResponse;
import com.unifier.core.api.print.PrintTemplateDTO;
import com.unifier.core.api.print.WsPrintTemplate;
import com.unifier.services.printing.IPrintConfigService;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.core.vo.PrintTemplateVO;
import com.uniware.services.tenant.ITenantService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestPrintConfigService {
    @Autowired
    private IPrintConfigService printConfigService;

    @Autowired
    private ITenantService      tenantService;

    @Before
    public void loadCache() {
        Tenant t = tenantService.getTenantByCode("Staging");
        UserContext.current().setTenant(t);
    }

    @Test
    public void testGetSamplePrintTemplates() {
        GetSamplePrintTemplatesByTypeResponse response = printConfigService.getSamplePrintTemplatesByType(new GetSamplePrintTemplatesByTypeRequest(
                PrintTemplateVO.Type.INVOICE.name()));
        Assert.assertTrue(response.isSuccessful());
        System.out.println(response.getSamplePrintTemplates());
    }

    @Test
    public void testAddSamplePrintTemplate() {
        PrintTemplateDTO template = printConfigService.getSamplePrintTemplatesByType(new GetSamplePrintTemplatesByTypeRequest(PrintTemplateVO.Type.INVOICE.name())).getSamplePrintTemplates().get(
                0);
        AddPrintTemplateRequest addRequest = new AddPrintTemplateRequest();
        addRequest.setType(template.getType());
        addRequest.setName(template.getName());
        System.out.println(printConfigService.addPrintTemplate(addRequest));
    }

    @Test
    public void testEditSamplePrintTemplate() {
        PrintTemplateDTO template = printConfigService.getSamplePrintTemplatesByType(new GetSamplePrintTemplatesByTypeRequest(PrintTemplateVO.Type.INVOICE.name())).getSamplePrintTemplates().get(
                0);
        EditPrintTemplateRequest request = new EditPrintTemplateRequest();
        WsPrintTemplate pt = new WsPrintTemplate();
        pt.setType(template.getType());
        pt.setName("new template");
        pt.setDescription("dasfdfsdfsd ffsd");
        pt.setTemplate(template.getTemplate());
        pt.setNumberOfCopies(template.getNumberOfCopies());
        pt.setPrintDialog(template.getPrintDialog());
        pt.setPageSize(template.getPageSize());
        pt.setPageMargins(template.getPageMargins());
        pt.setLandscape(template.isLandscape());
        request.setWsPrintTemplate(pt);
        EditPrintTemplateResponse response = printConfigService.editPrintTemplate(request);
        System.out.println(response.isSuccessful());
        System.out.println(response.getErrors());
    }
}
