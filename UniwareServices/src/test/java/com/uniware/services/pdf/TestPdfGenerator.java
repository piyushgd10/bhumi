/*
 *  Copyright 2012 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Feb 13, 2012
 *  @author singla
 */
package com.uniware.services.pdf;

import com.unifier.core.utils.FileUtils;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl;
import com.unifier.services.pdf.impl.PdfDocumentServiceImpl.PrintOptions;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;


/**
 * @author singla
 */
public class TestPdfGenerator {

    @Test
    public void testPrintDocument() throws IOException {
        PrintOptions options = new PrintOptions();
        options.setPageSize("A4");
        //        options.setPrintDialog(PrintDialog.SILENT.name());
        options.setMargins(new int[] { 10, 10, 10, 10 });
        new PdfDocumentServiceImpl().writeHtmlToPdf(new FileOutputStream("/Users/singla/Desktop/output.pdf"), FileUtils.getFileAsString("/Users/singla/Desktop/invoice.xml"),
                options);
    }
}
