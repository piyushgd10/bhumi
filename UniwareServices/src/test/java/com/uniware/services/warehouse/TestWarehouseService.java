package com.uniware.services.warehouse;

import com.uniware.core.api.validation.WsResponseCode;
import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.SearchShelfRequest;
import com.uniware.core.api.warehouse.SearchShelfResponse;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestWarehouseService {

    @Autowired
    private IShelfService shelfService;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testSearchShelfs() {
        SearchShelfRequest req = new SearchShelfRequest();
        req.setSectionCode("APL");
        req.setShelfTypeCode("67");
        SearchShelfResponse response = shelfService.searchShelfs(req);
        System.out.println(response.getShelfCodes());
    }

    @Test
    public void testCreateShelf() {

        // new shelf type
        CreateShelfTypeRequest cbr = new CreateShelfTypeRequest();
        cbr.setCode("STDSHELFT");
        cbr.setLength(1000);
        cbr.setWidth(100);
        cbr.setHeight(500);
        CreateShelfTypeResponse cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes != null);

        cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes.getErrors().size() == 1);

        // shelf type with same code, should return null
        cbr.setLength(100);
        cbr.setWidth(100);
        cbr.setHeight(500);
        cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes.getErrors().get(0).getCode() == WsResponseCode.DUPLICATE_CODE.code());

    }

}
