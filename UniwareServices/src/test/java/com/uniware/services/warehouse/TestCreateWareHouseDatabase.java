package com.uniware.services.warehouse;

import com.uniware.core.api.warehouse.CreateShelfTypeRequest;
import com.uniware.core.api.warehouse.CreateShelfTypeResponse;
import com.uniware.core.api.warehouse.CreateShippingPackageTypeRequest;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Section;
import com.uniware.core.entity.Shelf;
import com.uniware.core.entity.VendorItemType;
import com.uniware.services.shipping.IShippingAdminService;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestCreateWareHouseDatabase {

    @Autowired
    private IShippingAdminService shippingAdminService;

    @Autowired
    private IShelfService shelfService;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void loadConfigurations() {
    }

    //@Test
    @SuppressWarnings("unchecked")
    public void testCreateShelfDatabase() {

        List<Section> sections = new ArrayList<Section>();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Section");
        sections = query.list();
        session.close();

        CreateShelfTypeRequest cbr = new CreateShelfTypeRequest();
        cbr.setCode("S");
        cbr.setLength(500);
        cbr.setWidth(200);
        cbr.setHeight(500);
        CreateShelfTypeResponse cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes != null);
        cbRes = shelfService.createShelfType(cbr);

        cbr = new CreateShelfTypeRequest();
        cbr.setCode("M");
        cbr.setLength(1000);
        cbr.setWidth(400);
        cbr.setHeight(500);
        cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes != null);
        cbRes = shelfService.createShelfType(cbr);

        cbr = new CreateShelfTypeRequest();
        cbr.setCode("L");
        cbr.setLength(2000);
        cbr.setWidth(1000);
        cbr.setHeight(1000);
        cbRes = shelfService.createShelfType(cbr);
        assertEquals(true, cbRes != null);
        cbRes = shelfService.createShelfType(cbr);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCreateItemDatabase() {

        List<Section> sections = new ArrayList<Section>();

        List<ItemType> itemTypes = new ArrayList<ItemType>();
        Map<Integer, List<Shelf>> sectionShelfMap = new HashMap<Integer, List<Shelf>>();
        Map<ItemType, List<VendorItemType>> itemTypeVendorMap = new HashMap<ItemType, List<VendorItemType>>();

        Session session = sessionFactory.openSession();

        Query query = session.createQuery("from Section");
        sections = query.list();
        query = session.createQuery("from ItemType");
        itemTypes = query.list();
        for (Section section : sections) {
            query = session.createQuery("from Shelf where section.id = :sectionId");
            query.setParameter("sectionId", section.getId());
            sectionShelfMap.put(section.getId(), query.list());
        }

        for (ItemType itemType : itemTypes) {
            query = session.createQuery("from VendorItemType where itemType.id = :itemTypeId");
            query.setParameter("itemTypeId", itemType.getId());
            itemTypeVendorMap.put(itemType, query.list());
        }

        //System.out.println(items.get(0).getCategories().size());
        session.close();

    }

    //@Test
    public void testCreatePackageDatabase() {
        CreateShippingPackageTypeRequest spt = new CreateShippingPackageTypeRequest();
        spt.setBoxHeight(100);
        spt.setBoxWidth(150);
        spt.setBoxLength(250);
        spt.setCode("Small");
        shippingAdminService.createShippingPackageType(spt);
    }
}
