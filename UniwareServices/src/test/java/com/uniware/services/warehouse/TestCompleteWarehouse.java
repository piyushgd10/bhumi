/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */
package com.uniware.services.warehouse;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.api.user.CreateUserRequest;
import com.unifier.core.entity.User;
import com.unifier.core.transport.http.HttpTransportException;
import com.unifier.services.users.IUsersService;
import com.uniware.core.api.invoice.CreateInvoiceRequest;
import com.uniware.core.api.invoice.CreateInvoiceResponse;
import com.uniware.core.api.invoice.CreateShippingPackageInvoiceRequest;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;
import com.uniware.core.api.model.WsSaleOrder;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.picker.CreateManualPicklistRequest;
import com.uniware.core.api.picker.CreatePicklistResponse;
import com.uniware.core.api.returns.CreateReshipmentRequest;
import com.uniware.core.api.returns.CreateReshipmentResponse;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.api.saleorder.HoldSaleOrderRequest;
import com.uniware.core.api.saleorder.HoldSaleOrderResponse;
import com.uniware.core.api.shipping.AddShippingPackageToManifestRequest;
import com.uniware.core.api.shipping.AddShippingPackageToManifestResponse;
import com.uniware.core.api.shipping.AllocateShippingProviderRequest;
import com.uniware.core.api.shipping.AllocateShippingProviderResponse;
import com.uniware.core.api.shipping.CloseShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestRequest;
import com.uniware.core.api.shipping.CreateShippingManifestResponse;
import com.uniware.core.entity.AddressDetail;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Location;
import com.uniware.core.entity.PickSet;
import com.uniware.core.entity.Reshipment;
import com.uniware.core.entity.SaleOrder;
import com.uniware.core.entity.SaleOrderItem;
import com.uniware.core.entity.ShippingManifest;
import com.uniware.core.entity.ShippingPackage;
import com.uniware.core.entity.ShippingProvider;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.invoice.IInvoiceService;
import com.uniware.services.packer.IPackerService;
import com.uniware.services.picker.IPickerService;
import com.uniware.services.returns.IReturnsService;
import com.uniware.services.saleorder.ISaleOrderService;
import com.uniware.services.shipping.IDispatchService;
import com.uniware.services.shipping.IShipmentTrackingService;
import com.uniware.services.shipping.IShippingInvoiceService;
import com.uniware.services.shipping.IShippingProviderService;
import com.uniware.services.shipping.IShippingService;
import com.uniware.services.tasks.saleorder.SaleOrderProcessor;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
@SuppressWarnings({ "unchecked", "unused" })
public class TestCompleteWarehouse {

    private static final Logger                                                    LOG                 = LoggerFactory.getLogger(TestCompleteWarehouse.class);
    @Autowired
    private ISaleOrderService                                                      saleOrderService;
    @Autowired
    private SessionFactory                                                         sessionFactory;
    private List<Location>                                                         allLocationPincodes;
    private List<ItemType>                                                         itemTypes;

    private List<PickSet>                                                          picksets;
    private List<User>                                                             users;
    private List<ShippingProvider>                                                 shippingProviders;
    private static AtomicLong                                                      totalOrderItems     = new AtomicLong(0);
    private static ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Boolean>> pickSetEmptyBuckets = new ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Boolean>>();

    private static ExecutorService                                                 executorService     = new ThreadPoolExecutor(20, 100, 60, TimeUnit.SECONDS,
                                                                                                               new LinkedBlockingQueue<Runnable>());

    private static Session                                                         session;

    @Autowired
    private IUsersService                                                          userService;

    @Autowired
    private IPackerService                                                         packerService;

    @Autowired
    private IShippingService                                                       shippingService;

    @Autowired
    private IShippingProviderService                                               shippingProviderService;

    @Autowired
    private IPickerService                                                         pickerService;

    @Autowired
    private IInvoiceService                                                        invoiceService;

    @Autowired
    private SaleOrderProcessor                                                     saleOrderProcessor;

    @Autowired
    private ICatalogService                                                        catalogService;

    @Autowired
    private IReturnsService                                                        returnService;

    @Autowired
    private IShipmentTrackingService                                               shipmentTrackingService;

    @Autowired
    private IDispatchService                                                       dispatchService;

    @Autowired
    private IShippingInvoiceService                                                shippingInvoiceService;

    @Before
    public void loadStartupConfig() {

        loadData();

    }

    public void clearOrderData() {
        Session session = sessionFactory.openSession();
        Query query = session.createSQLQuery("delete from sale_order");
        query.executeUpdate();
        query = session.createSQLQuery("delete from sale_order_item");
        query.executeUpdate();
        query = session.createSQLQuery("delete from shipping_package");
        query.executeUpdate();
        query = session.createSQLQuery("delete from picklist");
        query.executeUpdate();
        query = session.createSQLQuery("delete from shipment_tracking");
        query.executeUpdate();
        session.close();
    }

    public void loadData() {
        itemTypes = new ArrayList<ItemType>();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from ItemType");

        itemTypes = query.list();
        itemTypes = itemTypes.subList(0, itemTypes.size() > 10 ? 10 : itemTypes.size());
        query = session.createQuery("from Location");
        allLocationPincodes = query.list();
        query = session.createQuery("from Bucket");
        query = session.createQuery("from BucketType");

        query = session.createQuery("from PickSet");
        picksets = query.list();
        query = session.createQuery("from User");
        users = query.list();
        query = session.createQuery("from ShippingProvider sp where sp.integrated = 1 and sp.configured = 1 and sp.enabled = 1");
        shippingProviders = query.list();
        session.close();
    }

    @Test
    public void testWarehouse() throws IOException, HttpTransportException, InterruptedException {
        int runLoopTimes = 0;
        Session session = sessionFactory.openSession();
        clearOrderData();
        while (totalOrderItems.intValue() == 0 && runLoopTimes < 1) {
            runLoopTimes++;

            // create orders
            createSaleOrder(100, "DEMOORDER");
            //create shipping packages and allocate inventory
            //saleOrderProcessor.execute();
            //generate picklists for all orders
            //generatePicklist(runLoopTimes, session);
        }
        //testTwistedOrders(session);
    }

    private void generatePicklist(int runLoopTimes, Session session) throws InterruptedException {
        executorService = new ThreadPoolExecutor(20, 100, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        Query query = session.createQuery("select count(soi) from SaleOrderItem soi where soi.shippingPackage.statusCode = 'CREATED'");
        totalOrderItems = new AtomicLong((Long) query.uniqueResult());

        for (PickSet pickSet : picksets) {
            pickSetEmptyBuckets.put(pickSet.getId(), new ConcurrentHashMap<Integer, Boolean>());
        }

        // create picklists
        for (int i = 0; i < 4; i++) {
            executorService.execute(new PicklistGenerator(i));
        }

        executorService.shutdown();
        if (executorService.awaitTermination(1000000, TimeUnit.SECONDS)) {
            LOG.debug("All picklists created");
        }

        assertEquals(totalOrderItems.intValue() == 0, true);
    }

    public void testTwistedOrders(Session session) {
        int k = 0;
        while (k < 20) {

            k++;
            Query query = session.createQuery("from ShippingPackage so where so.statusCode = 'PICKING'");
            ShippingPackage shippingPackage = (ShippingPackage) query.list().get(0);
            LOG.debug("TestTwistedOrders Shipping Package id:" + shippingPackage.getId());
            int saleOrderItemId = shippingPackage.getSaleOrderItems().iterator().next().getId();

            //receivePicklist(session, saleOrderItemId);

            shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);
            assertEquals(true, shippingPackage.getStatusCode().equals(ShippingPackage.StatusCode.PICKED.name()));

            createInvoiceAndAllocateCourier(shippingPackage);

            //SaleOrder os = saleOrderService.getSaleOrderForUpdate(1);
            //Now the order is ready_to_ship
            //assertEquals(true, shippingPackage.getStatusCode().equals(ShippingPackage.StatusCode.READY_TO_SHIP.name()));

            shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId(), true);

            CreateShippingManifestResponse csmRes = createManifestAndAddPackage(shippingPackage);
            //now sp status should be dispatched
            //assertEquals(true, shippingPackage.getStatusCode().equals(ShippingPackage.StatusCode.DISPATCHED.name()));

            closeManifest(csmRes);
            boolean testOrder = false;
            double rand = Math.random();
            if (rand > 0.6) {
                testOrder = testReshipment(session, shippingPackage);
            } else if (rand > 0.2) {
            } else {
                testOrder = testNormalOrder(shippingPackage);
            }
            assert (testOrder == true);
        }
    }

    private void closeManifest(CreateShippingManifestResponse csmRes) {
        CloseShippingManifestRequest closeSMReq = new CloseShippingManifestRequest();
        closeSMReq.setShippingManifestCode(csmRes.getShippingManifestCode());
        dispatchService.closeShippingManifest(closeSMReq);
    }

    private CreateShippingManifestResponse createManifestAndAddPackage(ShippingPackage shippingPackage) {
        CreateShippingManifestRequest csmReq = new CreateShippingManifestRequest();
        csmReq.setShippingProviderCode(shippingPackage.getShippingProviderCode());
        csmReq.setUserId(users.get((int) Math.round(Math.random() * (users.size() - 1))).getId());
        CreateShippingManifestResponse csmRes = dispatchService.createShippingManifest(csmReq);
        AddShippingPackageToManifestRequest asptmReq = new AddShippingPackageToManifestRequest();
        asptmReq.setAwbOrShipmentCode(shippingPackage.getCode().toString());
        asptmReq.setShippingManifestCode(csmRes.getShippingManifestCode());
        AddShippingPackageToManifestResponse asptmRes = dispatchService.addShippingPackageToManifest(asptmReq);

        assert (asptmRes.getErrors().size() > 0 == false);
        return csmRes;
    }

    private void createInvoiceAndAllocateCourier(ShippingPackage shippingPackage) {
        CreateInvoiceRequest gocIReq = new CreateInvoiceRequest();
        gocIReq.setShippingPackageCode(shippingPackage.getCode());
        gocIReq.setUserId(users.get((int) Math.round(Math.random() * (users.size() - 1))).getId());
        CreateShippingPackageInvoiceRequest createShippingPackageInvoiceRequest = new CreateShippingPackageInvoiceRequest(gocIReq);
        CreateInvoiceResponse invoiceResponse = shippingInvoiceService.createShippingPackageInvoice(createShippingPackageInvoiceRequest);
        AllocateShippingProviderRequest aspReq = new AllocateShippingProviderRequest();
        aspReq.setShippingPackageCode(shippingPackage.getCode());
        AllocateShippingProviderResponse aspRes = shippingProviderService.allocateShippingProvider(aspReq);
    }

    private boolean testNormalOrder(ShippingPackage shippingPackage) {
        if (shippingPackage.getStatusCode().equals(ShippingPackage.StatusCode.DISPATCHED.name())) {
            return true;
        }
        return true;
    }

    private boolean testReshipment(Session session, ShippingPackage shippingPackage) {
        shippingPackage = shippingService.getShippingPackageById(shippingPackage.getId());
        Query query = session.createQuery("select soi from SaleOrderItem soi join fetch soi.saleOrder join fetch soi.shippingAddress where soi.shippingPackage.id = :id");
        query.setInteger("id", shippingPackage.getId());
        List<SaleOrderItem> saleOrderItems = query.list();

        //package is dispatched
        //create a reshipment request
        CreateReshipmentRequest crsReq = new CreateReshipmentRequest();
        crsReq.setShippingPackageCode(shippingPackage.getCode());

        WsAddressDetail wsAddressDetail = new WsAddressDetail();

        wsAddressDetail = getAddress(saleOrderItems.get(0).getShippingAddress());
        //        crsReq.setShippingAddress(wsAddressDetail);

        String actionCode = Reshipment.Action.RESHIP_IMMEDIATELY_DONT_EXPECT_RETURN.code();
        int actionRandNum = (int) Math.round(Math.random() * 3.99);
        switch (actionRandNum) {
            case 0:
                actionCode = Reshipment.Action.RESHIP_IMMEDIATELY_EXPECT_RETURN.code();
                break;
            case 1:
                actionCode = Reshipment.Action.RESHIP_IMMEDIATELY_DONT_EXPECT_RETURN.code();
                break;
            case 2:
                actionCode = Reshipment.Action.WAIT_FOR_RETURN_AND_CANCEL.code();
                break;
            case 3:
                actionCode = Reshipment.Action.WAIT_FOR_RETURN_AND_RESHIP.code();
                break;
        }

        crsReq.setActionCode(actionCode);
        //        crsReq.setShippingAddress(wsAddressDetail);
        CreateReshipmentResponse crpRes = returnService.createReshipment(crsReq);
        if (!crpRes.isSuccessful()) {
            LOG.error("CreateReshipmentRequest failed shipping package id " + shippingPackage.getId() + "Error :" + crpRes.getErrors().get(0).getCode());
            return false;
        }

        return true;
    }

    private WsAddressDetail getAddress(AddressDetail shippingAddress) {
        WsAddressDetail wsAddressDetail = new WsAddressDetail();
        wsAddressDetail.setAddressLine1(shippingAddress.getAddressLine1());
        wsAddressDetail.setAddressLine2(shippingAddress.getAddressLine2());
        wsAddressDetail.setCity(shippingAddress.getCity());
        wsAddressDetail.setName(shippingAddress.getName());
        wsAddressDetail.setId("1");
        wsAddressDetail.setPhone(shippingAddress.getPhone());
        wsAddressDetail.setPincode(shippingAddress.getPincode());
        wsAddressDetail.setCountry(shippingAddress.getCountryCode());
        wsAddressDetail.setState(shippingAddress.getStateCode());
        return wsAddressDetail;
    }

    //@Test

    public void testHoldOrder() {

        session = sessionFactory.openSession();

        ShippingManifest shippingManifest = new ShippingManifest();
        ShippingProvider sp = shippingProviders.get((int) Math.round(Math.random() * (shippingProviders.size() - 1)));
        shippingManifest.setComments("TESTHOLDORDER");
        shippingManifest.setShippingProviderCode(sp.getCode());
        shippingManifest.setStatusCode(ShippingManifest.StatusCode.CREATED.name());
        shippingManifest.setCreated(new Date());
        shippingManifest.setUpdated(new Date());
        shippingManifest.setUser(users.get((int) Math.round(Math.random() * (users.size() - 1))));

        dispatchService.addShippingManifest(shippingManifest);

        Query query = session.createQuery("from SaleOrder so where so.statusCode = 'CREATED'");
        SaleOrder createdOrder = (SaleOrder) query.uniqueResult();
        HoldSaleOrderRequest hsor = new HoldSaleOrderRequest();
        HoldSaleOrderResponse hsoRes = new HoldSaleOrderResponse();
        if (createdOrder != null) {
            hsor.setSaleOrderCode(createdOrder.getCode());
            hsoRes = saleOrderService.holdSaleOrder(hsor);
            assertEquals(hsoRes.getErrors().size(), 0);
        }

        query = session.createQuery("select so from SaleOrder so where so.statusCode = 'CANCELLED'");
        SaleOrder cancelledOrder = (SaleOrder) query.uniqueResult();
        if (cancelledOrder != null) {
            hsor = new HoldSaleOrderRequest();
            hsor.setSaleOrderCode(cancelledOrder.getCode());

            hsoRes = saleOrderService.holdSaleOrder(hsor);
            assertEquals(hsoRes.getErrors().size(), 0);
        }

        query = session.createQuery("select so from SaleOrder so where so.statusCode = 'COMPLETE'");
        SaleOrder completeOrder = (SaleOrder) query.uniqueResult();
        if (completeOrder != null) {
            hsor = new HoldSaleOrderRequest();
            hsor.setSaleOrderCode(completeOrder.getCode());

            hsoRes = saleOrderService.holdSaleOrder(hsor);
            assertEquals(hsoRes.getErrors().size(), 0);
        }
    }

    private final class PicklistGenerator implements Runnable {

        private final int worker;

        public PicklistGenerator(int worker) {
            this.worker = worker;
        }

        @Override
        public void run() {

            while (totalOrderItems.intValue() > 0) {
                int pickSet = picksets.get((int) Math.round(Math.random() * (picksets.size() - 1))).getId();
                //while (pickSetEmptyBuckets.get(pickSet).size() < bucketTypes.size()) {
                List<String> bucketCodes = new ArrayList<String>();
                try {
                    int i = (int) Math.round(Math.random() * 3);

                    //                    for (Bucket bucket : oBucketCodes) {
                    //                        if (pickSetEmptyBuckets.get(pickSet).containsKey(bucket.getBucketType().getId())) {
                    //                            bucketCodes.remove(bucket.getCode());
                    //                        }
                    //                    }
                    if (bucketCodes.size() == 0) {
                        continue;
                    } else {
                        LOG.debug("Worker:" + worker + ", Buckets:" + bucketCodes);
                    }
                    CreateManualPicklistRequest cpr = new CreateManualPicklistRequest();
                    cpr.setPickSetId(pickSet);
                    cpr.setUserId(users.get((int) Math.round(Math.random() * (users.size() - 1))).getId());
                    CreatePicklistResponse cpRes = pickerService.createManualPicklist(cpr);
                    if (!cpRes.isSuccessful()) {
                        LOG.error("CreateManualPicklistRequest failed Errors :" + cpRes.getErrors().get(0).getCode());
                    }
                    if (cpRes.getPicklist() == null || cpRes.getPicklist().getPicklistItems().size() == 0) {
                    } else {
                        totalOrderItems.addAndGet(-cpRes.getPicklist().getPicklistItems().size());
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                //}
            }
        }
    }

    public void createSaleOrder(int numberOfOrders, String startNumber) throws IOException, HttpTransportException {

        List<WsAddressDetail> allAddresses;

        for (int j = 0; j < numberOfOrders; j++) {
            allAddresses = getAddresses();
            CreateSaleOrderRequest request = new CreateSaleOrderRequest();
            WsSaleOrder saleOrder = new WsSaleOrder();

            saleOrder.setCode(startNumber + j);
            saleOrder.setCashOnDelivery(Math.round(Math.random()) == 0 ? true : false);
            List<WsAddressDetail> addresses = new ArrayList<WsAddressDetail>();
            saleOrder.setSaleOrderItems(new ArrayList<WsSaleOrderItem>());
            saleOrder.setNotificationEmail("ankit.pruthi@gmail.com");
            saleOrder.setNotificationMobile("8826680745");
            int numberOfOrderItems = (int) Math.round(Math.random() * 10 + 1);
            switch ((int) Math.round(Math.random() * 10 + 1)) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    numberOfOrderItems = 1;
                    break;
                case 6:
                case 7:
                    numberOfOrderItems = 2;
                    break;
                case 8:
                    numberOfOrderItems = 2;
                    break;
                case 9:
                    numberOfOrderItems = 3;
                    break;
                case 10:
                    numberOfOrderItems = 3;
                    break;
                case 11:
                    numberOfOrderItems = 4;
                    break;
                default:
                    numberOfOrderItems = 1;
            }
            boolean createPackageBeforeHand = (Math.round(Math.random()) == 0 ? true : false);
            ItemType it;

            WsAddressDetail Address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));
            addresses.add(Address1);
            //            for (int i = 0; i < numberOfOrderItems; i++) {
            //                Address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));
            //                if (!addresses.contains(Address1)) {
            //                    addresses.add(Address1);
            //                }
            //            }
            int packetNumber = 1;
            for (int i = 0; i < numberOfOrderItems; i++) {
                int qty = (int) Math.round(Math.random() * 10);
                if (qty < 7) {
                    qty = 1;
                } else if (qty < 9) {
                    qty = 2;
                } else {
                    qty = 3;
                }
                it = itemTypes.get((int) Math.round(Math.random() * (itemTypes.size() - 1)));
                for (int g = 0; g < qty; g++) {
                    WsAddressRef addressRef = new WsAddressRef();
                    if (createPackageBeforeHand) {
                        int packageId = (int) Math.round(Math.random() * (addresses.size() - 1));
                        addressRef.setReferenceId(addresses.get(packageId).getId());
                        saleOrder.getSaleOrderItems().add(
                                getSaleOrderItem("" + (g * 100 + j * 10 + i), packetNumber, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                    } else {
                        addressRef.setReferenceId(addresses.get((int) Math.round(Math.random() * (addresses.size() - 1))).getId());
                        saleOrder.getSaleOrderItems().add(
                                getSaleOrderItem("" + (g * 100 + j * 10 + i), packetNumber, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                    }
                }
                packetNumber++;
            }

            saleOrder.setAddresses(addresses);
            WsAddressRef shippingAddressRef = new WsAddressRef();
            shippingAddressRef.setReferenceId(saleOrder.getSaleOrderItems().get(0).getShippingAddress().getReferenceId());
            saleOrder.setShippingAddress(shippingAddressRef);
            saleOrder.setBillingAddress(shippingAddressRef);

            //            List<WsShippingProvider> shippingProviders = new ArrayList<WsShippingProvider>();
            //
            //            for (WsSaleOrderItem soi : saleOrder.getSaleOrderItems()) {
            //                WsShippingProvider shippingProvider = new WsShippingProvider();
            //                shippingProvider.setCode("BLUEDART");
            //                shippingProvider.setPacketNumber(soi.getPacketNumber());
            //                shippingProviders.add(shippingProvider);
            //            }

            //saleOrder.setShippingProviders(shippingProviders);

            request.setSaleOrder(saleOrder);
            CreateSaleOrderResponse response = saleOrderService.createSaleOrder(request);
            if (!response.isSuccessful()) {
                LOG.error("CreateSaleOrderResponse failed Errors :" + response.getErrors().get(0).getCode());
            }
            assertEquals(true, response.isSuccessful());
        }
    }

    private WsSaleOrderItem getSaleOrderItem(String code, int packet, String itemSKU, String methodCode, boolean giftwrap, WsAddressRef wsAddressRef, boolean isCOD) {
        WsSaleOrderItem saleOrderItem = new WsSaleOrderItem();
        saleOrderItem.setCode(code);
        saleOrderItem.setPacketNumber(packet);
        saleOrderItem.setItemSku(itemSKU);
        saleOrderItem.setShippingMethodCode(methodCode);
        saleOrderItem.setGiftWrap(giftwrap);
        saleOrderItem.setShippingAddress(wsAddressRef);

        saleOrderItem.setSellingPrice(new BigDecimal(Math.round(Math.random() * 10000 + 100)));
        saleOrderItem.setSellingPrice(new BigDecimal(25000));
        if (giftwrap) {
            saleOrderItem.setGiftMessage("Happy");
            saleOrderItem.setGiftWrapCharges(new BigDecimal(20));
        }
        if (isCOD) {
            saleOrderItem.setCashOnDeliveryCharges(new BigDecimal(30));
        }
        saleOrderItem.setTotalPrice(saleOrderItem.getSellingPrice().add(
                saleOrderItem.getShippingCharges().add(saleOrderItem.getCashOnDeliveryCharges().add(saleOrderItem.getGiftWrapCharges()))));
        return saleOrderItem;
    }

    private List<WsAddressDetail> getAddresses() {
        List<WsAddressDetail> allAddresses = new ArrayList<WsAddressDetail>();

        WsAddressDetail address = new WsAddressDetail();
        address.setId("Address1");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Karun Singla");
        address.setState("AP");
        address.setPhone("+91 1743 222177");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address2");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("AP");
        address.setCountry("IN");
        address.setName("Ankit Pruthi");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("AP");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address3");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("AP");
        address.setCountry("IN");
        address.setName("Vibhu Garg");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("AP");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address4");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("AP");
        address.setCountry("IN");
        address.setName("Kuvalaya");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("AP");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address5");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("AP");
        address.setCountry("IN");
        address.setName("Kamal");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("AP");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);
        return allAddresses;
    }

    public void addUser() {
        CreateUserRequest cuReq = new CreateUserRequest();
        cuReq.setName("ankit@unicommerce.com");
        cuReq.setPassword("unicom");
        cuReq.setConfirmPassword("unicom");
        cuReq.setUsername("ankit@unicommerce.com");
        List<String> roles = new ArrayList<String>();
        roles.add("admin");
        cuReq.setRoles(roles);
        userService.createUser(cuReq);
    }
}
