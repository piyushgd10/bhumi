/*
 *  Copyright 2011 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 30, 2011
 *  @author singla
 */
package com.uniware.services.warehouse;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.transport.http.HttpTransportException;
import com.uniware.core.api.model.WsAddressDetail;
import com.uniware.core.api.model.WsAddressRef;
import com.uniware.core.api.model.WsSaleOrder;
import com.uniware.core.api.model.WsSaleOrderItem;
import com.uniware.core.api.saleorder.CreateSaleOrderRequest;
import com.uniware.core.api.saleorder.CreateSaleOrderResponse;
import com.uniware.core.entity.ItemType;
import com.uniware.core.entity.Location;
import com.uniware.services.catalog.ICatalogService;
import com.uniware.services.saleorder.ISaleOrderService;

/**
 * @author singla
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class CreateDemoDatabase {
    @Autowired
    private ISaleOrderService   saleOrderService;
    @Autowired
    private SessionFactory      sessionFactory;
    private List<Location>      allLocationPincodes;
    private List<ItemType>      itemTypes;

    private static Session      session;

    @Autowired
    private ICatalogService     catalogService;

    private static final Logger LOG = LoggerFactory.getLogger(CreateDemoDatabase.class);

    @Before
    public void loadStartupConfig() {
        loadData();
    }

    public void clearOrderData() {
        session = sessionFactory.openSession();
        Query query = session.createSQLQuery("truncate sale_order");
        query.executeUpdate();
        query = session.createSQLQuery("truncate sale_order_item");
        query.executeUpdate();
        query = session.createSQLQuery("truncate shipping_package");
        query.executeUpdate();
        query = session.createSQLQuery("truncate picklist");
        query.executeUpdate();
        query = session.createSQLQuery("truncate shipment_tracking");
        query.executeUpdate();
        session.close();
    }

    @SuppressWarnings("unchecked")
    public void loadData() {
        itemTypes = new ArrayList<ItemType>();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from ItemType");
        itemTypes = query.list();

        query = session.createQuery("from Location");
        allLocationPincodes = query.list();

        session.close();
    }

    @Test
    public void createDemoData() throws IOException, HttpTransportException, InterruptedException {
        clearOrderData();

        //create demo orders
        createDemoOrder("DEMOORDER");
        // create random orders
        createSaleOrder(10, "TESTORDER");
    }

    private void createDemoOrder(String startNumber) {

        List<WsAddressDetail> allAddresses = getAddresses();
        CreateSaleOrderRequest request = new CreateSaleOrderRequest();
        WsSaleOrder saleOrder = new WsSaleOrder();
        saleOrder.setCode(startNumber + 1);
        saleOrder.setCashOnDelivery(Math.round(Math.random()) == 0 ? true : false);
        saleOrder.setSaleOrderItems(new ArrayList<WsSaleOrderItem>());
        WsAddressRef addressRef = new WsAddressRef();
        WsAddressDetail address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));
        addressRef.setReferenceId(address1.getId());

        //added two items of Reebok sunglasses
        ItemType it = catalogService.getNonBundledItemTypeById(2397);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (100), 1, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (101), 1, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));

        it = new ItemType();
        it = catalogService.getNonBundledItemTypeById(2439);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (102), 1, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
        it = new ItemType();
        it = catalogService.getNonBundledItemTypeById(2475);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (103), 1, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));

        List<WsAddressDetail> addresses = new ArrayList<WsAddressDetail>();
        addresses.add(address1);
        saleOrder.setAddresses(addresses);
        WsAddressRef shippingAddressRef = new WsAddressRef();
        shippingAddressRef.setReferenceId(addressRef.getReferenceId());
        saleOrder.setShippingAddress(shippingAddressRef);
        saleOrder.setBillingAddress(shippingAddressRef);

        //second package with return and reverse pickup items
        it = new ItemType();
        it = catalogService.getNonBundledItemTypeById(3032);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (104), 2, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
        it = new ItemType();
        it = catalogService.getNonBundledItemTypeById(3036);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (105), 2, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
        it = new ItemType();
        it = catalogService.getNonBundledItemTypeById(2475);
        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (106), 2, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));

        address1 = new WsAddressDetail();
        address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));
        addressRef = new WsAddressRef();

        request.setSaleOrder(saleOrder);
        CreateSaleOrderResponse response = saleOrderService.createSaleOrder(request);

        // System.out.println(orderToXml(request));
        if (!response.isSuccessful()) {
            LOG.error("CREATE DEMO SALEORDER failed :" + response.getErrors().get(0).toString());
        }
        assertEquals(true, response.isSuccessful());

    }

    public void createSaleOrder(int numberOfOrders, String startNumber) throws IOException, HttpTransportException {

        List<WsAddressDetail> allAddresses;

        for (int j = 0; j < numberOfOrders; j++) {
            allAddresses = getAddresses();
            CreateSaleOrderRequest request = new CreateSaleOrderRequest();
            WsSaleOrder saleOrder = new WsSaleOrder();
            saleOrder.setCode(startNumber + j);
            saleOrder.setCashOnDelivery(Math.round(Math.random()) == 0 ? true : false);
            List<WsAddressDetail> addresses = new ArrayList<WsAddressDetail>();
            saleOrder.setSaleOrderItems(new ArrayList<WsSaleOrderItem>());

            int numberOfOrderItems = (int) Math.round(Math.random() * 10 + 1);
            switch ((int) Math.round(Math.random() * 10 + 1)) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    numberOfOrderItems = 1;
                    break;
                case 6:
                case 7:
                    numberOfOrderItems = 2;
                    break;
                case 8:
                    numberOfOrderItems = 6;
                    break;
                case 9:
                    numberOfOrderItems = 3;
                    break;
                case 10:
                    numberOfOrderItems = 4;
                    break;
                case 11:
                    numberOfOrderItems = 5;
                    break;
                default:
                    numberOfOrderItems = 1;
            }
            boolean createPackageBeforeHand = (Math.round(Math.random()) == 0 ? true : false);
            ItemType it;

            WsAddressDetail Address1;

            for (int i = 0; i < numberOfOrderItems; i++) {
                Address1 = allAddresses.get((int) Math.round(Math.random() * (allAddresses.size() - 1)));
                if (!addresses.contains(Address1)) {
                    addresses.add(Address1);
                }
            }

            for (int i = 0; i < numberOfOrderItems; i++) {
                int qty = (int) Math.round(Math.random() * 10);
                if (qty < 7) {
                    qty = 1;
                } else if (qty < 9) {
                    qty = 2;
                } else {
                    qty = 3;
                }
                it = itemTypes.get((int) Math.round(Math.random() * (itemTypes.size() - 1)));
                for (int g = 0; g < qty; g++) {
                    WsAddressRef addressRef = new WsAddressRef();
                    if (createPackageBeforeHand) {
                        int packageId = (int) Math.round(Math.random() * (addresses.size() - 1));
                        addressRef.setReferenceId(addresses.get(packageId).getId());
                        saleOrder.getSaleOrderItems().add(
                                getSaleOrderItem("" + (g * 100 + j * 10 + i), 1 + packageId, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                    } else {
                        addressRef.setReferenceId(addresses.get((int) Math.round(Math.random() * (addresses.size() - 1))).getId());
                        saleOrder.getSaleOrderItems().add(getSaleOrderItem("" + (g * 100 + j * 10 + i), 0, it.getSkuCode(), "STD", false, addressRef, saleOrder.getCashOnDelivery()));
                    }
                }
            }

            saleOrder.setAddresses(addresses);
            WsAddressRef shippingAddressRef = new WsAddressRef();
            shippingAddressRef.setReferenceId(saleOrder.getSaleOrderItems().get(0).getShippingAddress().getReferenceId());
            saleOrder.setShippingAddress(shippingAddressRef);
            saleOrder.setBillingAddress(shippingAddressRef);
            if (addresses.size() > 2) {
                System.out.println("testing");
            }
            request.setSaleOrder(saleOrder);
            CreateSaleOrderResponse response = saleOrderService.createSaleOrder(request);
            // System.out.println(orderToXml(request));
            if (!response.isSuccessful()) {
                System.out.println("kya ho gaya");
            }
            assertEquals(true, response.isSuccessful());
        }
    }

    private WsSaleOrderItem getSaleOrderItem(String code, int packet, String itemSKU, String methodCode, boolean giftwrap, WsAddressRef wsAddressRef, boolean isCOD) {
        WsSaleOrderItem saleOrderItem = new WsSaleOrderItem();
        saleOrderItem.setCode(code);
        saleOrderItem.setPacketNumber(packet);
        saleOrderItem.setItemSku(itemSKU);
        saleOrderItem.setShippingMethodCode(methodCode);
        saleOrderItem.setGiftWrap(giftwrap);
        saleOrderItem.setShippingAddress(wsAddressRef);

        saleOrderItem.setSellingPrice(new BigDecimal(Math.round(Math.random() * 10000 + 100)));
        if (giftwrap) {
            saleOrderItem.setGiftMessage("Happy");
            saleOrderItem.setGiftWrapCharges(new BigDecimal(20));
        }
        if (isCOD) {
            saleOrderItem.setCashOnDeliveryCharges(new BigDecimal(30));
        }
        saleOrderItem.setTotalPrice(saleOrderItem.getSellingPrice().add(
                saleOrderItem.getShippingCharges().add(saleOrderItem.getCashOnDeliveryCharges().add(saleOrderItem.getGiftWrapCharges()))));
        return saleOrderItem;
    }

    private List<WsAddressDetail> getAddresses() {
        List<WsAddressDetail> allAddresses = new ArrayList<WsAddressDetail>();

        WsAddressDetail address = new WsAddressDetail();
        address.setId("Address1");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Karun Singla");
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address2");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Ankit Pruthi");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address3");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Vibhu Garg");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address4");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Kuvalaya");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);

        address = new WsAddressDetail();
        address.setId("Address5");
        address.setAddressLine1("324, Galleria Towers");
        address.setAddressLine2("DLF Phase 4");
        address.setCity("Gurgaon");
        address.setCountry("IN");
        address.setName("Kamal");
        if (Math.round(Math.random() * 10) == 0) {
            address.setPincode(allLocationPincodes.get((int) Math.round(Math.random() * (allLocationPincodes.size() - 1))).getPincode());
        } else {
            address.setPincode("110034");
        }
        address.setState("Haryana");
        address.setPhone("+91 1743 222177");
        allAddresses.add(address);
        return allAddresses;
    }

}
