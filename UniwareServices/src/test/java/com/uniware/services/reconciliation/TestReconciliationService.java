/*
 *  Copyright 2014 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2014
 *  @author parijat
 */
package com.uniware.services.reconciliation;

import com.uniware.core.entity.Facility;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.unifier.core.cache.CacheManager;
import com.unifier.core.configuration.ConfigurationManager;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceRequest;
import com.uniware.core.api.channel.SyncChannelReconciliationInvoiceResponse;
import com.uniware.core.api.reconciliation.CreateChannelPaymentReconciliationRequest;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;
import com.uniware.services.channel.IChannelReconciliationInvoiceSyncService;
import com.uniware.services.tenant.ITenantService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class TestReconciliationService {

    @Autowired
    private IReconciliationService                   reconciliationService;

    @Autowired
    private ITenantService                           tenantService;

    @Autowired
    private IChannelReconciliationInvoiceSyncService channelReconciliationInvoiceSyncService;

    @Autowired
    private ApplicationContext                       applicationContext;

    @Before
    public void setup() {
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(CacheManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(ConfigurationManager.getInstance(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
        Tenant t = tenantService.getTenantByCode("parijat");
        UserContext.current().setTenant(t);
        UserContext.current().setFacility(new Facility(1));
    }

    @Test
    public void testMongoHistoriesFetch() {
        //        ChannelPaymentReconciliationHistoryVO historyVO = reconciliationService.getChannelReconHistoryForTransaction("OD30908022034", "NFT-131004064GN00006XXXXXXX");
        //        Assert.assertNotNull(historyVO);
    }

    @Test
    public void testChannelReconciliationInvoiceTask() {
        SyncChannelReconciliationInvoiceRequest request = new SyncChannelReconciliationInvoiceRequest("FLIPKART");
        SyncChannelReconciliationInvoiceResponse response = channelReconciliationInvoiceSyncService.syncChannelReconciliationInvoices(request);
        Assert.assertTrue(response.isSuccessful());
    }

    @Test
    public void testCreateReconciliationRow() {

        //        List<TransactionRow> transactionRows = new ArrayList<TransactionRow>();
        //        transactionRows.add(new TransactionRow("DEBIT_INITIATE", "Return Completed", new BigDecimal(-315)));
        //        transactionRows.add(new TransactionRow("CREDIT_INITIATE", "Delivered", new BigDecimal(398.83)));
        //        transactionRows.add(new TransactionRow("CREDIT_INITIATE", "Delivered", new BigDecimal(414.56)));
        //        transactionRows.add(new TransactionRow("CREDIT_INITIATE", "Delivered", new BigDecimal(465.17)));
        CreateChannelPaymentReconciliationRequest request = null;/* new CreateChannelPaymentReconciliationRequest("OD30908022034", "NFT-131004064GN00006XXXXXXX", DateUtils.stringToDate(
                                                                 "04-Oct-13",
                                                                 "dd-MMM-yy"), "CUSTOM");*/
        //reconciliationService.reconcileOrderItem(request);
    }

}
