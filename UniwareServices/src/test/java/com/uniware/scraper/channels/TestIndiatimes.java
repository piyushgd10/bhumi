package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestIndiatimes {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"ranjit.pathways@gmail.com\",\"password\":\"Market@1\"},\"channel\":{\"code\":\"INDIATIMES__RANJIT_PATHWAYS\",\"name\":\"Indiatimes (ranjit.pathways\",\"source\":{\"code\":\"INDIATIMES\",\"name\":\"Indiatimes\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":45,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"indiatimesLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"NOT_AVAILABLE\",\"thirdPartyShipping\":true,\"useChannelSKU\":true,\"tat\":48,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":1},\"source\":{\"code\":\"INDIATIMES\",\"name\":\"Indiatimes\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":45,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"indiatimesLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"saleOrderCode\":\"3003018257-00001\"}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testIndiatimesUserVerifiaction() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/indiatimes-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "contact@storji.com");
            context.addVariable("password", "shopping@123");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } finally {

            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testIndiatimesGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/indiatimes-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            if (StringUtils.isNotBlank(saleOrderListXml)) {
                Element rootElement = XMLParser.parse(saleOrderListXml);
                for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                    saleOrderElements.add(saleOrderElement);
                }
            } else {
                System.out.println("No sale order codes in list output");
            }
        } finally {
            ScriptExecutionContext.destroy();
        }

        ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/indiatimes-getsaleorder.xml")).compile();

        if (sandbox.getSaleOrderCode() != null && !saleOrderElements.isEmpty()) {
            Element saleOrderElement = null;
            for (Element eSaleOrder : saleOrderElements) {
                String saleOrderCode = eSaleOrder.text();
                if (saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    saleOrderElement = eSaleOrder;
                }
            }

            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("saleOrderCode", sandbox.getSaleOrderCode());
            context.addVariable("saleOrderElement", saleOrderElement);
            context.addVariable("saleOrderDetailElement", resultItems.get(sandbox.getSaleOrderCode()));
            context.addVariable("resultItems", resultItems);
            try {
                saleOrderDetailsScript.execute();
                String saleOrderRequestXml = context.getScriptOutput();
                System.out.println(saleOrderRequestXml);
            } finally {
                ScriptExecutionContext.destroy();
            }
        } else {
            for (Element saleOrderElement : saleOrderElements) {
                String saleOrderCode = saleOrderElement.text();
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Test
    public void testIndiatimesShippingProviderAllocation() throws Exception {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/indiatimes-shipping-provider-allocation.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String out = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(out);
        }
    }

}
