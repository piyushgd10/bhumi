package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestLadyblush {
    
    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"Trivenisarees\",\"password\":\"Trivenisarees123\"},\"channel\":{\"code\":\"LADYBLUSH\",\"name\":\"LadyBlush\",\"source\":{\"code\":\"LADYBLUSH\",\"name\":\"LadyBlush\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":150,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"ladyblushLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"NOT_AVAILABLE\",\"thirdPartyShipping\":true,\"useChannelSKU\":false,\"tat\":5,\"notificationsEnabled\":false,\"inventoryUpdateFormula\":\"\",\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":0},\"source\":{\"code\":\"LADYBLUSH\",\"name\":\"LadyBlush\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":150,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"ladyblushLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false}}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testLadyblushUserVerifiaction() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode
                    .parse(FileUtils
                            .getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ladyblush-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "Trivenisarees");
            context.addVariable("password", "Trivenisarees123");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } finally {
            
            ScriptExecutionContext.destroy();
        }
    }
    
    @Test
    public void testLadyblushGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ladyblush-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ladyblush-getsaleorder.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }
}
