package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestCraftsvilla {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"support@theelephantcompany.com\",\"password\":\"TEC@12345\"},\"channel\":{\"code\":\"CRAFTSVILLA\",\"name\":\"Craftsvilla\",\"source\":{\"code\":\"CRAFTSVILLA\",\"name\":\"Craftsvilla\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":140,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"craftsVillaLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"NOT_AVAILABLE\",\"thirdPartyShipping\":false,\"tat\":5,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":0},\"source\":{\"code\":\"CRAFTSVILLA\",\"name\":\"Craftsvilla\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":140,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"craftsVillaLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"saleOrderCode\":\"100093703\"}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testCraftsvillaUserVerification() throws Exception {
        Map<String, String> responseParams = new HashMap<String, String>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/craftsVilla-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            Map<String, String> requestParams = new HashMap<String, String>();
            context.addVariable("username", "vastradivastra@gmail.com");
            context.addVariable("password", "vastradi4");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully LoggedIn !!!");
        } finally {
            //            System.out.println(ScriptExecutionContext.current()
            //                    .getScriptOutput());
            ScriptExecutionContext.destroy();
        }
    }
    
    @Test
    public void testCraftsvillaGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/craftsVilla-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/craftsVilla-getsaleorder.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }
}
