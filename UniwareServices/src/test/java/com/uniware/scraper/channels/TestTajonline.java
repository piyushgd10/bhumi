package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestTajonline {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"DELHIMALL\",\"password\":\"tolshop1906\"},\"channel\":{\"code\":\"TAJONLINE\",\"name\":\"tajonline\",\"source\":{\"code\":\"TAJONLINE\",\"name\":\"Taj Online\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":155,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"tajonlineLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":false,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"NOT_AVAILABLE\",\"thirdPartyShipping\":false,\"useChannelSKU\":true,\"tat\":3,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":2},\"source\":{\"code\":\"TAJONLINE\",\"name\":\"Taj Online\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":false,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":155,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"tajonlineLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":false,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"saleOrderCode\":\"932175\"}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testTajOnlineUserVerification() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/tajonline-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testTajOnlineGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/tajonline-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            if (StringUtils.isNotBlank(saleOrderListXml)) {
                Element rootElement = XMLParser.parse(saleOrderListXml);
                for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                    saleOrderElements.add(saleOrderElement);
                }
            } else {
                System.out.println("No sale order codes in list output");
            }
        } finally {
            ScriptExecutionContext.destroy();
        }

        ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/tajonline-getsaleorder.xml")).compile();

        if (sandbox.getSaleOrderCode() != null) {
            Element saleOrderElement = null;
            for (Element eSaleOrder : saleOrderElements) {
                String saleOrderCode = eSaleOrder.text();
                if (saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    saleOrderElement = eSaleOrder;
                }
            }

            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("saleOrderCode", sandbox.getSaleOrderCode());
            context.addVariable("saleOrderElement", saleOrderElement);
            context.addVariable("saleOrderDetailElement", resultItems.get(sandbox.getSaleOrderCode()));
            try {
                saleOrderDetailsScript.execute();
                String saleOrderRequestXml = context.getScriptOutput();
                System.out.println(saleOrderRequestXml);
            } finally {
                ScriptExecutionContext.destroy();
            }
        } else {
            for (Element saleOrderElement : saleOrderElements) {
                String saleOrderCode = saleOrderElement.text();
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }

    }
}
