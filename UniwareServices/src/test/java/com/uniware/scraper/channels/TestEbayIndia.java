package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestEbayIndia {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"elites-accessories\",\"shipTogether\":\"Yes\",\"printOrderStatement\":\"yes\",\"password\":\"icicibank10\",\"eBayAuthToken\":\"AgAAAA**AQAAAA**aAAAAA**gQx/Uw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AEmIujDJmDowidj6x9nY+seQ**cx4CAA**AAMAAA**AhTji7ulay+8NpmtcNJUXOK7sCMoaOvJrkzoRBvL+nji5lzIeYrlD4ubDD1984l+mgCObL7ldiyx+RPqHGvjd9JpzBhGOc2xdzqwb1P3psVk77nMdd935WNAdAeCi6cOHuMqrUaHzRtlQEaNT8T6jBw+2tDKKAbkvmRQLmzHDiLfs3bae6DOYS2w0xsFZhuS83x9GjvCct4MFda4GXNND/L0vr9G3N0EHoWBerhmc3HXjC2I5AQRzmvwKUCtGs7ZCxCQPx6KVAtU1gKOZvI92CmNNSh8LR8E5BNk2GyEY48QX0NnYA6lOFs8Gmy1xFjGH2m6ocAevKVE7kftx716MM6bt32+sm0ny+0dyd6KhpQhT40GBK+4g1lcin6+oSAMuT1EM97R6GbXCSwa1lQ3SQxqce9u0qdRYt4aZVYM0TeLzt2qXA+4dXzPrTKgYypKeFMnKOdq4u5MWjkY4wOlNWNInopJmfFlitvj5ZPZDYqj6sMtHOoPyJlhlfDZLQJws312j14iUUKFBaoigsDg3toCMA0pv7JQ1iCJcN4q2ewAGJPLK9sxhxuR2RxZkyAm5Z1NPPg6958zG7Mj17jW9EihDVYgRLobLFk/eiFwrNEJUGwSaWOs9YY7cxXRamqzOTlp/5tWJzvCBdk/t/OrMiWEqGHjrNX3sE9w8mIMiCUgxvCW4152JuPwOVXRW5gjxaZz5g0d4i+IiQmLq5j0sSaY7MsdKs+UcAvkoLEvIDjvC7U81utDwMa148CPvjNB\"},\"channel\":{\"httpProxyHost\":\"103.227.36.42\",\"httpProxyPort\":5050,\"code\":\"EBAY\",\"name\":\"Ebay\",\"source\":{\"code\":\"EBAY_INDIA\",\"name\":\"eBay India\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":5,\"thirdPartyConfigurationRequired\":true,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"ebayIndiaLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"ON\",\"thirdPartyShipping\":true,\"tat\":48,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":0},\"source\":{\"code\":\"EBAY_INDIA\",\"name\":\"eBay India\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":5,\"thirdPartyConfigurationRequired\":true,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"ebayIndiaLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false}}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testEbayGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("/Users/karunsingla/Work/Unicommerce/Uniware/UniwareResources/scripts/scraper/ebay-india-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.setTraceLoggingEnabled(true);
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            if (StringUtils.isNotBlank(saleOrderListXml)) {
                Element rootElement = XMLParser.parse(saleOrderListXml);
                for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                    saleOrderElements.add(saleOrderElement);
                }
            } else {
                System.out.println("No sale order codes in list output");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            ScriptExecutionContext.destroy();
        }

        ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(
                FileUtils.getFileAsString("/Users/karunsingla/Work/Unicommerce/Uniware/UniwareResources/scripts/scraper/ebay-india-getsaleorder.xml")).compile();

        if (sandbox.getSaleOrderCode() != null && !saleOrderElements.isEmpty()) {
            Element saleOrderElement = null;
            for (Element eSaleOrder : saleOrderElements) {
                String saleOrderCode = eSaleOrder.text();
                if (saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    saleOrderElement = eSaleOrder;
                }
            }

            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.setTraceLoggingEnabled(true);
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("saleOrderCode", sandbox.getSaleOrderCode());
            context.addVariable("saleOrderElement", saleOrderElement);
            context.addVariable("saleOrderDetailElement", resultItems.get(sandbox.getSaleOrderCode()));
            try {
                saleOrderDetailsScript.execute();
                String saleOrderRequestXml = context.getScriptOutput();
                System.out.println(saleOrderRequestXml);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                ScriptExecutionContext.destroy();
            }
        } else {
            for (Element saleOrderElement : saleOrderElements) {
                String saleOrderCode = saleOrderElement.text();
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }

    }

    @Test
    public void testEbayShippingProviderAllocation() throws Exception {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-india-shipping-provider-allocation.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String out = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(out);
        }
    }

}
