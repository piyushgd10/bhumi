package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestHomeShop18 {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"username\":\"Chhabra555_hs18\",\"password\":\"Chhabra555@hlghl\"},\"channel\":{\"code\":\"HOMESHOP18\",\"name\":\"Homeshop18\",\"source\":{\"code\":\"HOMESHOP18\",\"name\":\"Homeshop18\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":25,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"homeshop18LandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":true,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":false,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"OFF\",\"thirdPartyShipping\":true,\"useChannelSKU\":false,\"tat\":2,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":2},\"source\":{\"code\":\"HOMESHOP18\",\"name\":\"Homeshop18\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":25,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"homeshop18LandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":true,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":false,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false}}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testHomeshop18eUserVerifiaction() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "SporteGenie@HS18");
            context.addVariable("password", "SporteGenie@zxdfd");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } finally {

            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testHomeShop18GetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-getsaleorder.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Test
    public void testHomeShop18FetchPendency() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        String saleOrderListXml;
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-update-pendency.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "SporteGenie@HS18");
            context.addVariable("password", "SporteGenie@zxdfd");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            List<Element> saleOrderCodes = new ArrayList<Element>();
            saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testHomeShop18ShippingProviderAllocation() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        String saleOrderListXml;
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-shipping-provider-allocation.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();

            context.addVariable("resultItems", resultItems);
            context.addVariable("username", "DBM_HS18");
            context.addVariable("password", "XZXCHQGO@cG8w3kNRG");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            List<Element> saleOrderCodes = new ArrayList<Element>();
            saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testHomeShop18PostManifest() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        String saleOrderListXml;
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/homeshop18-post-manifest.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();

            context.addVariable("resultItems", resultItems);
            context.addVariable("username", "SporteGenie@HS18");
            context.addVariable("password", "SporteGenie@zxdfd");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            List<Element> saleOrderCodes = new ArrayList<Element>();
            saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testHomeShop18CatalogSync() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("/home/pankaj/workspace/Uniware/UniwareResources/scripts/scraper/homeshop18-catalog-sync.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "SporteGenie@HS18");
            context.addVariable("password", "SporteGenie@zxdfd");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
        }
    }

    @Test
    public void testHomeShop18InventoryUpdate() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("/Users/karunsingla/Work/Unicommerce/Uniware/UniwareResources/scripts/scraper/homeshop18-update-inventory.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "REITIndia_HS18");
            context.addVariable("password", "REITIndia@xdzfzxdc");
            context.addVariable("channel", new Object() {
                public String name = "homeshop18";
            });
            context.addVariable("inventorySnapshot", new Object() {
                public String channelSkuCode      = "EA11";
                public String channelProductId    = "667097";
                public long   calculatedInventory = 5;
            });
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
        }
    }
}
