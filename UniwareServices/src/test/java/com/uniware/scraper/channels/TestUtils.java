/*
 *  Copyright 2013 Unicommerce eSolutions (P) Limited . All Rights Reserved.
 *  UNICOMMERCE ESOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Dec-2013
 *  @author karunsingla
 */
package com.uniware.scraper.channels;

import java.util.HashMap;
import java.util.Map;

import com.unifier.core.annotation.Cache;
import com.unifier.core.annotation.Level;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.DateUtils;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;
import com.uniware.core.cache.EnvironmentPropertiesCache;
import com.uniware.core.entity.Tenant;
import com.uniware.core.utils.UserContext;

public class TestUtils {

    public static Map<String, Object> prepareScriptVariables(SandboxParams sandboxParams) {
        Map<String, Object> scriptVariables = new HashMap<String, Object>();
        scriptVariables.putAll(sandboxParams.getChannelConnectorParameters());
        scriptVariables.put("channel", sandboxParams.getChannel());
        scriptVariables.put("source", sandboxParams.getSource());
        scriptVariables.put("saleOrder", sandboxParams.getSaleOrder());
        scriptVariables.put("shippingPackage", sandboxParams.getShippingPackage());
        scriptVariables.put("shippingManifest", sandboxParams.getShippingManifest());
        UserContext.current().setTenant(new Tenant("dev", "dev", "dev.unicommerce.com", DateUtils.getCurrentTime(), DateUtils.getCurrentTime()));
        CacheManager.getInstance().setCache(new SandboxEnvironmentCache());
        return scriptVariables;
    }

    @Cache(level = Level.GLOBAL, type = "environmentCache")
    public static class SandboxEnvironmentCache extends EnvironmentPropertiesCache {
        @Override
        public String getExportDirectoryPath() {
            return "/tmp";
        }

        @Override
        public String getImportDirectoryPath() {
            return "/tmp";
        }
    };
}
