package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;

@SuppressWarnings("unused")
public class TestFirstcry {

    @Test
    public void testFirstCryUserVerification() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/firstcry-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "sbaghla@bluegape.com");
            context.addVariable("password", "firstcry");
            context.addVariable("channel", new Object() {
                private String  name               = "firstcry";
                private boolean thirdPartyShipping = false;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public boolean isThirdPartyShipping() {
                    return thirdPartyShipping;
                }

                public void setThirdPartyShipping(boolean thirdPartyShipping) {
                    this.thirdPartyShipping = thirdPartyShipping;
                }

            });
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testFirstGetSaleOrders() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/firstcry-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.addVariable("username", "sbaghla@bluegape.com");
            context.addVariable("password", "firstcry");
            context.addVariable("channel", new Object() {
                private String  name               = "firstcry";
                private boolean thirdPartyShipping = false;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public boolean isThirdPartyShipping() {
                    return thirdPartyShipping;
                }

                public void setThirdPartyShipping(boolean thirdPartyShipping) {
                    this.thirdPartyShipping = thirdPartyShipping;
                }

            });
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/firstcry-getSaleOrder.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.addVariable("username", "sbaghla@bluegape.com");
                context.addVariable("password", "firstcry");
                context.addVariable("channel", new Object() {
                    private String  name               = "firstcry";
                    private boolean thirdPartyShipping = false;

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public boolean isThirdPartyShipping() {
                        return thirdPartyShipping;
                    }

                    public void setThirdPartyShipping(boolean thirdPartyShipping) {
                        this.thirdPartyShipping = thirdPartyShipping;
                    }

                });
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }
}
