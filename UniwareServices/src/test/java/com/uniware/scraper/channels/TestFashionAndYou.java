package com.uniware.scraper.channels;

import com.unifier.core.api.base.ServiceResponse;
import com.unifier.core.cache.CacheManager;
import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.scraper.sl.exception.ScriptCompilationException;
import com.unifier.scraper.sl.exception.ScriptParseException;
import com.unifier.scraper.sl.exception.ScriptValidationException;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.uniware.core.api.systemnotification.sandbox.SandboxParams;
import com.uniware.core.cache.EnvironmentPropertiesCache;

@SuppressWarnings("unused")
public class TestFashionAndYou {
    
    private Map<String, Object> scriptVarirables = new HashMap<String, Object>();

    @Before
    public void setup() {
        scriptVarirables.put("importFile", "/Users/karunsingla/Downloads/FASHION AND YOU.xlsx");
        scriptVarirables.put("preProcessorResponse", new ServiceResponse() {
            private static final long serialVersionUID = -7072065776570692254L;
            private String            csvFile;

            public void setCsvFile(String csvFile) {
                this.csvFile = csvFile;
            }

            public String getCsvFile() {
                return this.csvFile;
            }
        });
        EnvironmentPropertiesCache environmentProperties = new EnvironmentPropertiesCache() {
            @Override
            public String getExportDirectoryPath() {
                return ".";
            }

            @Override
            public String getImportDirectoryPath() {
                return ".";
            }
        };
        CacheManager.getInstance().setCache(environmentProperties);
    }

    @Test
    public void testFashionAndYouPreProcessorScript() throws ScriptParseException, IOException, ScriptValidationException, ScriptCompilationException {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/fashionandyou-b2b-sale-order-preprocessor.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(scriptVarirables);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            System.out.println(ScriptExecutionContext.current().getScriptOutput());
            ScriptExecutionContext.destroy();
        }

    }
}
