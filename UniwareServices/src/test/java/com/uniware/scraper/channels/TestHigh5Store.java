package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestHigh5Store {
    
    private String              sandboxParamsJson = "";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);
	
	@Test
	public void testHigh5StoreGetaleOrderList() throws Exception {
	    Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/high5store-getsaleorderlist-script.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(
                    FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/high5store-getsaleorder-script.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Test
    public void testHigh5StoreUserVerification() throws Exception {
        Map<String, String> responseParams = new HashMap<String, String>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/high5store-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            Map<String, String> requestParams = new HashMap<String, String>();
            context.addVariable("username", "manager@rajrang.com ");
            context.addVariable("password", "Pu6X7cOb");
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In !!!");
        } finally {
            //			System.out.println(ScriptExecutionContext.current()
            //					.getScriptOutput());
            ScriptExecutionContext.destroy();
        }
    }

}
