package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.exception.ScriptExecutionException;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestSnapdeal {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"inventory_password\":\"ashishsan123\",\"username\":\"K34sai@gmail.com\",\"inventory_username\":\"k34sai@gmail.com\",\"password\":\"ashishsan\"},\"channel\":{\"code\":\"SNAPDEAL\",\"name\":\"Snapdeal\",\"source\":{\"code\":\"SNAPDEAL\",\"name\":\"Snapdeal\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":15,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"snapdealLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":true,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"OFF\",\"thirdPartyShipping\":true,\"useChannelSKU\":true,\"tat\":72,\"notificationsEnabled\":false,\"allowCombinedManifest\":true,\"autoVerifyOrders\":false,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":2},\"source\":{\"code\":\"SNAPDEAL\",\"name\":\"Snapdeal\",\"type\":\"MARKETPLACE\",\"localization\":\"NATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":15,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"landingPageScriptName\":\"snapdealLandingPageScript\",\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":true,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"saleOrderCode\":\"14710419\"}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testSnapdealUserVerification() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-user-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("Successfully Logged In!!!");
        } catch (ScriptExecutionException e) {
            e.printStackTrace();
            System.out.println("REASON: " + e.getReasonCode());
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testSnapdealGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-getsaleorder.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Test
    public void testSnapdealPostManifest() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-post-manifest.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            List<Element> saleOrderCodes = new ArrayList<Element>();
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testSnapdealInventoryPanelVerification() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-inventory-panel-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            System.out.println("successfully logged in!!!!");
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
        }
    }

    @Test
    public void testSnapdealInventoryUpdate() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-update-inventory.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("sku", "LHG00203");
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
        }
    }

    @Test
    public void testSnapdealCatalogSync() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-catalog-sync.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
        }
    }

    @Test
    public void testSnapdealShippingProviderAllocation() throws Exception {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/snapdeal-shipping-provider-allocation.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String out = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(out);
        }
    }
}
