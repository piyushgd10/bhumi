package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestEbay {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"eBayAuthToken\":\"AgAAAA**AQAAAA**aAAAAA**QGI2VA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AFlYOgCJaEoQydj6x9nY+seQ**cx4CAA**AAMAAA**XN8RxqsHeBhv8tys3Ka/fJXGUDzIfAo+zzV5q42wJ9Oe8PqwlVlI8pg3M51Bf/eE7yj+vucOPmwrOM6gGkgbntIwxyGL5DNqjBrWHpcMjxomed3mFWubb5VBzJzuRl2Aeb5f3613x4CPSuKmFcnE/+TXgVmCZiyD//fQDmUS1GHINLB9ehQFzzPhyXcN4WZXdTPxOPbBk2v5bt4xdt2GET00QNpGRtMpRzX9t7UimbAAFdlyr1iHjUDb1GPpjDejucAOFgj1MdZeOE3sWu/8mGDw3PEACg1VeLTtAjGzRgM0VOL1WJR1lLJ9gDed9KzBT99JWfTcqeXPXKwiVOlY+gc204y4nhOBWe9b7YUNmYidW1v314ZT0yCpJRLk+xA6tagkUUGLVdfZC6oGY5tRsRc2GM0E+yydi/2cUuE2Ny9vINMZjWpKd8NgG4A5bw2xj4GE6M88eg2HgoXkei4NJ3t1GFx9yuOLkFgOtpoHoL6T90GAZb+TRCzLN25m2qPlr868YJXhgl5jtxDE68cvcBGZDVYg9tLvOPqDCD4FYhgsjQLbCjvMYPHradjGQBnKFghrys4bfwoJ7xWlff+njPRvYU88uGJaZuW9ynmWGGcSqM2QSFMW6kpQQmQ/nxHluxz4PfoCgU65h825DnDksr26zVQGvnEuY8rSi3yGILoyf6nJWElRfuLaDIFTPRt/k+/76QmBV7uDNKIpWZnKHhfryeWADN3JS9AL0U9LkSKzwQraqT/IoJtKTo9/mUMU\"},\"channel\":{\"code\":\"EBAY_US\",\"name\":\"ebay us\",\"source\":{\"code\":\"EBAY_US\",\"name\":\"eBay US\",\"type\":\"MARKETPLACE\",\"localization\":\"INTERNATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":70,\"thirdPartyConfigurationRequired\":true,\"useChannelSkuForInventoryUpdate\":false,\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"ON\",\"thirdPartyShipping\":false,\"tat\":120,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":0},\"source\":{\"code\":\"EBAY_US\",\"name\":\"eBay US\",\"type\":\"MARKETPLACE\",\"localization\":\"INTERNATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":70,\"thirdPartyConfigurationRequired\":true,\"useChannelSkuForInventoryUpdate\":false,\"notificationsEnabled\":false,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useSellerSkuForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"saleOrderCode\":\"176362986010\"}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testEbayChannelPreConfigure() throws Exception {
        Map<String, String> responseParams = new HashMap<String, String>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-preconfiguration.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();

            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String output = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(output);
            System.out.println(responseParams);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testEbayGetSaleOrderList() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-getsaleorderlist-script.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
            List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
            Element rootElement = XMLParser.parse(saleOrderListXml);
            ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-getsaleorder-script.xml")).compile();
            for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                String saleOrderCode = saleOrderElement.text();
                if (sandbox.getSaleOrderCode() != null && !saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    continue;
                }
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }
    }

    @Test
    public void testEbayCatalogSync() {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-getCatalog.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(saleOrderListXml);
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testEbayShippingProviderAllocation() throws Exception {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-india-shipping-provider-allocation.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String out = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(out);
        }
    }

    @Test
    public void testEbayChannelPostConfigure() throws Exception {
        Map<String, String> responseParams = new HashMap<String, String>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-postconfiguration.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            Map<String, String> requestParams = new HashMap<String, String>();

            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("requestParams", requestParams);
            context.addVariable("responseParams", responseParams);

            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            // String output =
            // ScriptExecutionContext.current().getScriptOutput();
            System.out.println(responseParams);
            System.out.println(ScriptExecutionContext.current().getScriptOutput());
            ScriptExecutionContext.destroy();
        }
    }

    @Test
    public void testEbayDispatch() throws Exception {
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/ebay-dispatch-verification.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
        } finally {
            String out = ScriptExecutionContext.current().getScriptOutput();
            System.out.println(out);
        }
    }
}
