package com.uniware.scraper.channels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.unifier.core.utils.FileUtils;
import com.unifier.core.utils.JsonUtils;
import com.unifier.core.utils.StringUtils;
import com.unifier.core.utils.XMLParser;
import com.unifier.core.utils.XMLParser.Element;
import com.unifier.scraper.sl.parser.ScraperScriptNode;
import com.unifier.scraper.sl.runtime.ScraperScript;
import com.unifier.scraper.sl.runtime.ScriptExecutionContext;
import com.uniware.core.api.systemnotification.sandbox.SandboxParams;

@SuppressWarnings("unused")
public class TestAmazon {

    private String              sandboxParamsJson = "{\"channelParameters\":{\"secretKey\":\"uNHLfJawK0D2cnKDYb2ihqSr7eOCYEx0meM7UEEZ\",\"sellerId\":\"A2A1MSPMOQYEHT\",\"awsAccessKeyId\":\"AKIAJ6HSRH27KNIU7M4A\",\"marketplaceId\":\"A2EUQ1WTGCTBG2\"},\"channel\":{\"code\":\"AMAZON_CANADA\",\"name\":\"Amazon Canada\",\"source\":{\"code\":\"AMAZON_CA\",\"name\":\"Amazon Canada\",\"type\":\"MARKETPLACE\",\"localization\":\"INTERNATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":75,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"notificationsEnabled\":true,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false},\"enabled\":true,\"orderSyncStatus\":\"ON\",\"inventorySyncStatus\":\"ON\",\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"tat\":1,\"notificationsEnabled\":false,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"packageType\":\"FIXED\",\"inventoryAllocationPriority\":0},\"source\":{\"code\":\"AMAZON_CA\",\"name\":\"Amazon Canada\",\"type\":\"MARKETPLACE\",\"localization\":\"INTERNATIONAL\",\"enabled\":true,\"orderSyncConfigured\":true,\"inventorySyncConfigured\":true,\"thirdPartyShipping\":false,\"useChannelSKU\":false,\"priority\":75,\"thirdPartyConfigurationRequired\":false,\"useChannelSkuForInventoryUpdate\":false,\"notificationsEnabled\":true,\"pendencyConfigurationEnabled\":false,\"sourceNotificationsEnabled\":false,\"allowMultipleChannel\":true,\"allowCombinedManifest\":false,\"autoVerifyOrders\":true,\"allowAnyShippingMethod\":true,\"useChannelIdentifierForInventoryUpdate\":false,\"packageTypeConfigured\":false}}";
    private SandboxParams       sandbox           = JsonUtils.stringToJson(sandboxParamsJson, SandboxParams.class);
    private Map<String, Object> sandboxParams     = TestUtils.prepareScriptVariables(sandbox);

    @Test
    public void testAmazonGetSaleOrder() throws Exception {
        Map<String, Object> resultItems = new HashMap<String, Object>();
        List<Element> saleOrderElements = new ArrayList<XMLParser.Element>();
        try {
            ScraperScriptNode pScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/amazon-getsaleorderlist.xml"));
            pScript.validate();
            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("resultItems", resultItems);
            ScraperScript scraperScript = pScript.compile();
            scraperScript.execute();
            List<Element> saleOrderCodes = new ArrayList<Element>();
            String saleOrderListXml = ScriptExecutionContext.current().getScriptOutput();
            if (StringUtils.isNotBlank(saleOrderListXml)) {
                Element rootElement = XMLParser.parse(saleOrderListXml);
                for (Element saleOrderElement : rootElement.list("SaleOrder")) {
                    saleOrderElements.add(saleOrderElement);
                }
            } else {
                System.out.println("No sale order codes in list output");
            }
        } finally {
            ScriptExecutionContext.destroy();
        }

        ScraperScript saleOrderDetailsScript = ScraperScriptNode.parse(FileUtils.getFileAsString("../../Uniware/UniwareResources/scripts/scraper/amazon-getsaleorder.xml")).compile();

        if (sandbox.getSaleOrderCode() != null) {
            Element saleOrderElement = null;
            for (Element eSaleOrder : saleOrderElements) {
                String saleOrderCode = eSaleOrder.text();
                if (saleOrderCode.equals(sandbox.getSaleOrderCode())) {
                    saleOrderElement = eSaleOrder;
                }
            }

            ScriptExecutionContext context = ScriptExecutionContext.current();
            context.getScriptVariables().putAll(sandboxParams);
            context.addVariable("saleOrderCode", sandbox.getSaleOrderCode());
            context.addVariable("saleOrderElement", saleOrderElement);
            context.addVariable("saleOrderDetailElement", resultItems.get(sandbox.getSaleOrderCode()));
            try {
                saleOrderDetailsScript.execute();
                String saleOrderRequestXml = context.getScriptOutput();
                System.out.println(saleOrderRequestXml);
            } finally {
                ScriptExecutionContext.destroy();
            }
        } else {
            for (Element saleOrderElement : saleOrderElements) {
                String saleOrderCode = saleOrderElement.text();
                System.out.println("Fetching details for order: " + saleOrderCode);
                ScriptExecutionContext context = ScriptExecutionContext.current();
                context.getScriptVariables().putAll(sandboxParams);
                context.addVariable("saleOrderCode", saleOrderCode);
                context.addVariable("saleOrderElement", saleOrderElement);
                context.addVariable("saleOrderDetailElement", resultItems.get(saleOrderCode));
                try {
                    saleOrderDetailsScript.execute();
                    String saleOrderRequestXml = context.getScriptOutput();
                    System.out.println(saleOrderRequestXml);
                } finally {
                    ScriptExecutionContext.destroy();
                }
            }
        }

    }

}
