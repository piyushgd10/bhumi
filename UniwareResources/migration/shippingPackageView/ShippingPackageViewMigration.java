package com.unifier.core.migration;

import java.io.FileWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.unifier.core.fileparser.DelimitedFileParser;
import com.unifier.core.fileparser.Row;
import com.unifier.core.utils.JsonUtils;

public class ShippingPackageViewMigration {

    private static final BlockingQueue<Job> queue    = new ArrayBlockingQueue<>(100000, true);

    private static final Job                SHUTDOWN = new Job();

    static class JobTask implements Runnable {

        private FileWriter fileWriter;

        public JobTask(String targetDataDirPath) throws Exception {
            String name = "exportData_" + System.nanoTime() + ".json";
            fileWriter = new FileWriter(targetDataDirPath + name, true);
            fileWriter.append("[");
        }

        @Override
        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Job callable = null;
                    try {
                        callable = queue.take();
                        if (callable == SHUTDOWN) {
                            queue.add(SHUTDOWN);
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        System.out.println(Thread.currentThread().getName() + " | Processing shipment: " + callable);
                        fileWriter.append(callable.call()).append(",");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } finally {
                try {
                    fileWriter.append("{}]");
                    fileWriter.flush();
                    fileWriter.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        String sourceDataFilePath = args[0];
        String targetDataDirPath = args[1];
        Integer numThreads = Integer.parseInt(args[2]);
        char delimiter = args[3].charAt(0);

        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        DelimitedFileParser parser = new DelimitedFileParser(sourceDataFilePath, delimiter);
        DelimitedFileParser.RowIterator ri = parser.parse();
        for (int i = 0; i < numThreads; i++) {
            executor.submit(new JobTask(targetDataDirPath));
        }
        while (ri.hasNext()) {
            Row row = ri.next();
            Job job = new Job(row);
            boolean submitted;
            do {
                submitted = queue.offer(job);
                if (submitted) {
                    System.out.println("Submitted job for shipment: " + job);
                }
            } while (!submitted);
        }
        boolean submitted;
        do {
            submitted = queue.offer(SHUTDOWN);
            if (submitted) {
                System.out.println("Submitted SHUTDOWN");
            }
        } while (!submitted);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("MIGRATION_COMPLETED");
    }

    static class Job implements Callable<String> {

        private Row row;

        public Job() {
        }

        public Job(Row row) throws Exception {
            this.row = row;
        }

        @Override
        public String call() {
            try {
                DTO dto = new DTO();
                dto.setSaleOrderNum(row.getColumnValue("saleOrderNum"));
                dto.setProductInfo(row.getColumnValue("productInfo"));
                dto.setChannel(row.getColumnValue("channel"));
                dto.setStatus(row.getColumnValue("status"));
                String fulfillmentTat = row.getColumnValue("fulfillmentTat");
                dto.setFulfillmentTat(fulfillmentTat);
                /**
                 * if ("REPLACE_BY_NULL".equals(fulfillmentTat)) { dto.setFulfillmentTat(null); } else {
                 * dto.setFulfillmentTat(DateUtils.stringToDate((fulfillmentTat), "YYYY-mm-DD HH:MM:SS")); }
                 **/
                dto.setApplicableRegulatoryForms(row.getColumnValue("applicableRegulatoryForms"));
                dto.setShipmentBatchCode(row.getColumnValue("shipmentBatchCode"));
                dto.setPicklist(row.getColumnValue("picklist"));
                dto.setDispatchSummary(row.getColumnValue("dispatchSummary"));
                dto.setPaymentMethod(row.getColumnValue("paymentMethod"));
                String created = row.getColumnValue("created");
                dto.setCreated(created);
                /**
                 * if ("REPLACE_BY_NULL".equals(created)) { dto.setCreated(null); } else {
                 * dto.setCreated(DateUtils.stringToDate(created, "YYYY-mm-DD HH:MM:SS")); }
                 **/
                dto.setDisplayOrderCode(row.getColumnValue("displayOrderCode"));
                dto.setShipment(row.getColumnValue("shipment"));
                dto.setChannelCode(row.getColumnValue("channelCode"));
                dto.setItemTypeSkus(row.getColumnValue("itemTypeSkus"));
                dto.setItemTypeIds(row.getColumnValue("itemTypeIds"));
                dto.setSellerSkus(row.getColumnValue("sellerSkus"));
                dto.setItemIdToDetails(row.getColumnValue("itemIdToDetails"));
                dto.setSellerSkuToDetails(row.getColumnValue("sellerSkuToDetails"));
                dto.setProvider(row.getColumnValue("provider"));
                dto.setProviderCode(row.getColumnValue("providerCode"));
                dto.setTrackingNumber(row.getColumnValue("trackingNumber"));
                String dispatchTime = row.getColumnValue("dispatchTime");
                dto.setDispatchTime(dispatchTime);
                /**
                 * if ("REPLACE_BY_NULL".equals(dispatchTime)) { dto.setDispatchTime(null); } else {
                 * dto.setDispatchTime(DateUtils.stringToDate(dispatchTime, "YYYY-mm-DD HH:MM:SS")); }
                 **/
                dto.setShippingManagedBy(row.getColumnValue("shippingManagedBy"));
                dto.setProductManagementSwitchedOff(Boolean.parseBoolean(row.getColumnValue("productManagementSwitchedOff")));
                dto.setPaymentMethodCode(row.getColumnValue("paymentMethodCode"));
                dto.setShipmentLabelFormat(row.getColumnValue("shipmentLabelFormat"));
                dto.setItemDetailingPending(Boolean.parseBoolean(row.getColumnValue("itemDetailingPending")));
                dto.setPutawayPending(Boolean.parseBoolean(row.getColumnValue("putawayPending")));
                dto.setTenant(row.getColumnValue("tenant"));
                dto.setFacilityId(Integer.parseInt(row.getColumnValue("facilityId")));
                dto.setViewDirty(Boolean.parseBoolean(row.getColumnValue("viewDirty")));
                return JsonUtils.objectToString(dto);
            } catch (Exception e) {
                System.out.println(Thread.currentThread().getName() + " | Error for " + this);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public String toString() {
            return row.getColumnValue("shipment");
        }
    }

    static class DTO {
        private String  saleOrderNum;
        private String  productInfo;
        private String  channel;
        private String  status;
        private String  fulfillmentTat;
        private String  applicableRegulatoryForms;
        private String  shipmentBatchCode;
        private String  picklist;
        private String  dispatchSummary;
        private String  paymentMethod;
        private String  created;
        private String  displayOrderCode;
        private String  shipment;
        private String  channelCode;
        private String  itemTypeSkus;
        private String  itemTypeIds;
        private String  sellerSkus;
        private String  itemIdToDetails;
        private String  sellerSkuToDetails;
        private String  provider;
        private String  providerCode;
        private String  trackingNumber;
        private String  dispatchTime;
        private String  shippingManagedBy;
        private boolean productManagementSwitchedOff;
        private String  paymentMethodCode;
        private String  shipmentLabelFormat;
        private boolean itemDetailingPending;
        private boolean putawayPending;
        private String  tenant;
        private Integer facilityId;
        private boolean viewDirty;

        public String getSaleOrderNum() {
            return saleOrderNum;
        }

        public void setSaleOrderNum(String saleOrderNum) {
            this.saleOrderNum = saleOrderNum;
        }

        public String getProductInfo() {
            return productInfo;
        }

        public void setProductInfo(String productInfo) {
            this.productInfo = productInfo;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFulfillmentTat() {
            return fulfillmentTat;
        }

        public void setFulfillmentTat(String fulfillmentTat) {
            this.fulfillmentTat = fulfillmentTat;
        }

        public String getApplicableRegulatoryForms() {
            return applicableRegulatoryForms;
        }

        public void setApplicableRegulatoryForms(String applicableRegulatoryForms) {
            this.applicableRegulatoryForms = applicableRegulatoryForms;
        }

        public String getShipmentBatchCode() {
            return shipmentBatchCode;
        }

        public void setShipmentBatchCode(String shipmentBatchCode) {
            this.shipmentBatchCode = shipmentBatchCode;
        }

        public String getPicklist() {
            return picklist;
        }

        public void setPicklist(String picklist) {
            this.picklist = picklist;
        }

        public String getDispatchSummary() {
            return dispatchSummary;
        }

        public void setDispatchSummary(String dispatchSummary) {
            this.dispatchSummary = dispatchSummary;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDisplayOrderCode() {
            return displayOrderCode;
        }

        public void setDisplayOrderCode(String displayOrderCode) {
            this.displayOrderCode = displayOrderCode;
        }

        public String getShipment() {
            return shipment;
        }

        public void setShipment(String shipment) {
            this.shipment = shipment;
        }

        public String getChannelCode() {
            return channelCode;
        }

        public void setChannelCode(String channelCode) {
            this.channelCode = channelCode;
        }

        public String getItemTypeSkus() {
            return itemTypeSkus;
        }

        public void setItemTypeSkus(String itemTypeSkus) {
            this.itemTypeSkus = itemTypeSkus;
        }

        public String getItemTypeIds() {
            return itemTypeIds;
        }

        public void setItemTypeIds(String itemTypeIds) {
            this.itemTypeIds = itemTypeIds;
        }

        public String getSellerSkus() {
            return sellerSkus;
        }

        public void setSellerSkus(String sellerSkus) {
            this.sellerSkus = sellerSkus;
        }

        public String getItemIdToDetails() {
            return itemIdToDetails;
        }

        public void setItemIdToDetails(String itemIdToDetails) {
            this.itemIdToDetails = itemIdToDetails;
        }

        public String getSellerSkuToDetails() {
            return sellerSkuToDetails;
        }

        public void setSellerSkuToDetails(String sellerSkuToDetails) {
            this.sellerSkuToDetails = sellerSkuToDetails;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getProviderCode() {
            return providerCode;
        }

        public void setProviderCode(String providerCode) {
            this.providerCode = providerCode;
        }

        public String getTrackingNumber() {
            return trackingNumber;
        }

        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getDispatchTime() {
            return dispatchTime;
        }

        public void setDispatchTime(String dispatchTime) {
            this.dispatchTime = dispatchTime;
        }

        public String getShippingManagedBy() {
            return shippingManagedBy;
        }

        public void setShippingManagedBy(String shippingManagedBy) {
            this.shippingManagedBy = shippingManagedBy;
        }

        public boolean isProductManagementSwitchedOff() {
            return productManagementSwitchedOff;
        }

        public void setProductManagementSwitchedOff(boolean productManagementSwitchedOff) {
            this.productManagementSwitchedOff = productManagementSwitchedOff;
        }

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }

        public String getShipmentLabelFormat() {
            return shipmentLabelFormat;
        }

        public void setShipmentLabelFormat(String shipmentLabelFormat) {
            this.shipmentLabelFormat = shipmentLabelFormat;
        }

        public boolean isItemDetailingPending() {
            return itemDetailingPending;
        }

        public void setItemDetailingPending(boolean itemDetailingPending) {
            this.itemDetailingPending = itemDetailingPending;
        }

        public boolean isPutawayPending() {
            return putawayPending;
        }

        public void setPutawayPending(boolean putawayPending) {
            this.putawayPending = putawayPending;
        }

        public String getTenant() {
            return tenant;
        }

        public void setTenant(String tenant) {
            this.tenant = tenant;
        }

        public Integer getFacilityId() {
            return facilityId;
        }

        public void setFacilityId(Integer facilityId) {
            this.facilityId = facilityId;
        }

        public boolean isViewDirty() {
            return viewDirty;
        }

        public void setViewDirty(boolean viewDirty) {
            this.viewDirty = viewDirty;
        }
    }
}
