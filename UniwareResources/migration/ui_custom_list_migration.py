#!/usr/bin/python

import MySQLdb
import sys
import csv
from pymongo import MongoClient
import json
import os
import subprocess

arguments = sys.argv

arg = arguments[1]

p = subprocess.Popen(["/usr/local/bin/scriptscommon/scriptscommon/getSpecificServerDetails", "-s", arg, "-d"], stdout=subprocess.PIPE)
uniware_server_name, err = p.communicate()

print 'Server is :' + uniware_server_name
uniware_server_name = uniware_server_name.strip()
mysql_ip = uniware_server_name
uniware_db = MySQLdb.connect(host=mysql_ip, user="root", passwd="uniware", db="uniware")
uniware_cursor = uniware_db.cursor()
uniware_cursor.execute(
    "select 'com.uniware.core.vo.UICustomListVO',t.code, u.name, u.options, u.created ,u.updated from ui_custom_list u, tenant t where u.tenant_id = t.id ;")
f = open("/tmp/temp_ui_custom_list_data.csv", "w+")

uniware_writer = csv.writer(f)
for wrow in uniware_cursor.fetchall():
    uniware_writer.writerow(wrow)
f.close()
print "Done writing csv file /tmp/temp_ui_custom_list_data.csv"
print "Data fetched from MySQL, inserting in mongo"

linuxCMD = 'cat /tmp/temp_ui_custom_list_data.csv >> /tmp/temp_ui_custom_list_data_mongo.csv'
os.system(linuxCMD)

p = subprocess.Popen(["/usr/local/bin/scriptscommon/scriptscommon/getSpecificServerDetails", "-s", arg, "-tm"], stdout=subprocess.PIPE)
mongo_server_name, err = p.communicate()
mongo_server_name = mongo_server_name.strip()
mysql_ip = mongo_server_name

flag = '1';
uniware_cursor.execute(
    "select  value from environment_property where name = 'tenant.specific.mongo.enabled';")
try:
    flag =  uniware_cursor.fetchone()[0]
except :
    print 'tenantspecific mongo not enabled on this cloud'


if (flag == "true"):
    print 'Tenant Specific primary mongo is :' + mongo_server_name
else:
    mongo_server_name = "common2.mongo.unicommerce.infra:27017"
    print 'Tenant Specific primary mongo is :' + mongo_server_name

mongo_connection = MongoClient(mongo_server_name)
f = open("/tmp/temp_ui_custom_list_data_mongo.csv", "r+")
summary_reader = csv.reader(f)
count = 0

for row in summary_reader:
    className = row[0]
    tenant_code = row[1]
    name = row[2]
    options = row[3]
    created = row[4]
    updated = row[5]

    if (flag == "true"):
        db = mongo_connection[tenant_code]
    else:
        db = mongo_connection["uniware"]

    uiCustomListCollection = db.uiCustomList

    ui_custom_list = {
        '_class': className,
        'tenantCode': tenant_code,
        'name': name,
        'options': options,
        'created': created,
        'updated': updated,
        }
    id = uiCustomListCollection.insert(ui_custom_list)
    db.eval("function (id) {db.uiCustomList.find({_id:id}).forEach(function(doc){db.uiCustomList.update({_id:doc._id},{$set:{created:new ISODate(doc.created), updated:new ISODate(doc.updated)}},{upsert:false , multi:true})})}",id)
    count = count + 1
f.close()

print 'total rows: ' + str(count)

linuxCMD = 'rm /tmp/temp_ui_custom_list_data.csv'
os.system(linuxCMD)
linuxCMD = 'rm /tmp/temp_ui_custom_list_data_mongo.csv'
os.system(linuxCMD)

print 'Done!'
