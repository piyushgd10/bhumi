#!/usr/bin/env bash
MYSQL_HOST=$2;
COMMON_MONGO_HOST=$3;
MONGO_HOST=$4;

for SERVER_NAME in $1
        do
                rm -rf /tmp/tenants.csv
                mongoexport --host $COMMON_MONGO_HOST -d uniware -c tenantProfile --fields tenantCode  -q '{serverName: "'$SERVER_NAME'"}' -o /tmp/tenants.csv;
                while read document; do
                    TENANT_CODE=`echo $document | awk -F ":" '{print $4}' | awk -F '"' '{print $2}'`;
                    echo "Working on "${TENANT_CODE};
                    rm -rf /tmp/${TENANT_CODE}_task_2.csv
                    rm -rf /tmp/${TENANT_CODE}_task.csv
                    mongo --host $MONGO_HOST $TENANT_CODE --eval "db.job.drop()";
                    mysql -uroot -puniware uniware -h$MYSQL_HOST -e "select 'com.unifier.core.entity.Job', t.task_class, te.code, t.name, t.execution_level, null, null, t.last_exec_time, t.severity, t.notification_level, t.enabled, t.created, t.updated,  GROUP_CONCAT(CONCAT(tp.name, '|', tp.value) SEPARATOR '^') from tenant te, task t left join task_parameter tp on tp.task_id = t.id where t.tenant_id = te.id group by 3,4 INTO OUTFILE '/tmp/${TENANT_CODE}_task.csv'  FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"
                    echo "_class,taskClass,tenantCode,name,executionLevel,cronExpression,lastExecResult,lastExecTime,severity,notificationLevel,enabled,created,updated,taskParameters" > /tmp/${TENANT_CODE}_task_2.csv
                    cat /tmp/${TENANT_CODE}_task.csv >> /tmp/${TENANT_CODE}_task_2.csv
                    mongoimport -d $TENANT_CODE -h $MONGO_HOST -c job --type csv --file /tmp/${TENANT_CODE}_task_2.csv --headerline
                    mongo --host $MONGO_HOST  $TENANT_CODE --eval "db.job.find().forEach(function(doc){if (doc.taskParameters  == '\\\N') {doc.taskParameters = null} else {var tp = {}; doc.taskParameters.split('^').map(function(v){var t = v.split('|'); tp[t[0]]=t[1];}); doc.taskParameters = tp}; doc.tenantCode = db.getName();if (doc.lastExecTime == '\\\N') {doc.lastExecTime = null} else{doc.lastExecTime = new ISODate(doc.lastExecTime)}; doc.created = new ISODate(doc.created); if(doc.lastExecResult == '\\\N'){doc.lastExecResult = null} ;if (doc.enabled == 1) {doc.enabled = true} else {doc.enabled = false} ;doc.updated = new ISODate(doc.updated);db.job.save(doc)});"
                    mongo --host $MONGO_HOST  $TENANT_CODE --eval "db.job.find().forEach(function(doc) {doc.misfireInstruction = 'MISFIRE_INSTRUCTION_DO_NOTHING'; doc.code = doc.tenantCode.concat('-').concat(doc.name.replace(' ', '_')); db.job.save(doc)});";
        done  < /tmp/tenants.csv;
done