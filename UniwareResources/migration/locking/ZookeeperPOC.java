/*
 * Copyright 2015 Unicommerce Technologies (P) Limited . All Rights Reserved.
 * UNICOMMERCE TECHONOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * @version     1.0, 3/28/15 12:31 PM
 * @author amdalal
 */

package com.unifier.core.locking;

import java.util.Random;
import java.util.concurrent.locks.Lock;

public class ZookeeperPOC {

    public static void main(String[] args) {
        LockingClient client = new LockingClient("zookeeper1.unicommerce.info:2181");
        Lock lockParent = new DistributedLock(client, "/poc/root");
        Lock lockChild = new DistributedLock(client, "/poc/root/child");
        Thread tParent = new Thread(new Locker(lockParent, 10000, "PARENT"));
        Thread tChild = new Thread(new Locker(lockChild, 1000, "CHILD"));
        tParent.start();
        tChild.start();
    }

    private static class Locker implements Runnable {

        private Lock   lock;

        private int    sleepMs;

        private String name;

        public Locker(Lock lock, int sleepMs, String name) {
            this.lock = lock;
            this.sleepMs = sleepMs;
            this.name = name;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    lock.lock();
                    System.out.println("Acquired lock on " + name);
                    Thread.sleep(sleepMs);
                } catch (Throwable t) {
                    t.printStackTrace();
                } finally {
                    lock.unlock();
                    System.out.println("Released lock on " + name);
                }
            }
        }
    }

    public static void main2(String[] args) {
        int numLocks = Integer.parseInt(args[0]);
        int numThreads = Integer.parseInt(args[1]);
        String host = args[2];
        LockingClient client = new LockingClient(host);
        String[] keyPool = new String[numLocks];
        for (int i = 1; i <= numLocks; i++) {
            keyPool[i - 1] = String.valueOf(100 + i);
        }
        for (int i = 0; i < numThreads; i++) {
            Thread t = new Thread(new Job(numLocks, keyPool, client));
            t.setName("T" + (i + 1));
            t.start();
        }
    }

    private static class Job implements Runnable {

        private int           numLocks;

        private String[]      keyPool;

        private LockingClient client;

        public Job(int numLocks, String[] keyPool, LockingClient client) {
            this.numLocks = numLocks;
            this.keyPool = keyPool;
            this.client = client;
        }

        @Override
        public void run() {
            while (true) {
                int l = 0;
                int h = numLocks;
                String lockKey = keyPool[(new Random().nextInt(h - l) + l)];
                DistributedLock lock = new DistributedLock(client, "/" + lockKey);
                boolean acquired = false;
                String threadName = Thread.currentThread().getName();
                try {
                    long start = System.currentTimeMillis();
                    System.out.println(new StringBuilder(String.valueOf(start)).append(" | ").append(threadName).append(" | Acquiring lock on key: ").append(lockKey).toString());
                    acquired = lock.tryLock();
                    if (acquired) {
                        long end = System.currentTimeMillis();
                        System.out.println(new StringBuilder(String.valueOf(end)).append(" | ").append(threadName).append(" | Lock on key: ").append(lockKey).append(
                                " obtained in ").append((end - start)).append(" ms").toString());
                    } else {
                        System.out.println(new StringBuilder(String.valueOf(System.currentTimeMillis())).append(" | ").append(threadName).append(" | Failed to get lock on key: ").append(
                                lockKey).toString());
                    }
                } catch (Exception e) {
                    System.out.println(e);
                } finally {
                    if (acquired) {
                        long start = System.currentTimeMillis();
                        System.out.println(new StringBuilder(String.valueOf(start)).append(" | ").append(threadName).append(" | Releasing lock on key: ").append(lockKey).toString());
                        lock.unlock();
                        long end = System.currentTimeMillis();
                        System.out.println(new StringBuilder(String.valueOf(end)).append(" | ").append(threadName).append(" | Lock on key: ").append(lockKey).append(
                                " released in ").append((end - start)).append(" ms").toString());
                    }
                }
            }
        }
    }
}
