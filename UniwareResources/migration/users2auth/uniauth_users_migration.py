#!/usr/bin/python

import MySQLdb
import sys
import csv
import os

arguments = sys.argv

uniware_server_name = arguments[1]
mysql_ip = uniware_server_name
uniware_db = MySQLdb.connect(host="%s" % mysql_ip, user="root", passwd="uniware", db="uniware")
uniware_cursor = uniware_db.cursor()
uniware_cursor.execute(
    "select u.user_email, u.normalized_email, u.password, 0, u.enabled, 1, u.mobile, u.verified_mobile, u.name, u.blocked, u.last_accessed_from, u.last_login_time, u.created, u.updated, t.code, u.username  from user u join tenant t where u.tenant_id = t.id and u.user_email is not null and u.blocked = 0 and u.user_email not in ('system@unicommerce.com', 'support@unicommerce.com', 'techsupport@unicommerce.com')")
filename = "/tmp/temp_user_table_data_" + uniware_server_name + ".csv"
f = open(filename, "w+")
uniware_writer = csv.writer(f)
for wrow in uniware_cursor.fetchall():
    uniware_writer.writerow(wrow)
f.close()
print "Done writing csv file " + filename
uniauth_server_name = arguments[2]
mysql_ip = uniauth_server_name
uniauth_db = MySQLdb.connect(host="%s" % mysql_ip, user="root", passwd="uniware", db="uniauth")
uniauth_cursor = uniauth_db.cursor()
f = open(filename , "r+")
uniauth_reader = csv.reader(f)
for row in uniauth_reader:
    email = row[0]
    if not email:
        continue
    normalized_email = row[1]
    password = row[2]
    reset_password = 1
    enabled = row[4]
    email_verified = row[5]
    unverified_mobile = row[6]
    verified_mobile = 'null'
    mobile = row[7]
    if not mobile:
        mobile_verified = 0
    else:
        verified_mobile = mobile
        mobile_verified = 1
    name = row[8]
    blocked = row[9]
    last_accessed_from = row[10]
    last_login_time = row[11]
    created = row[12]
    updated = row[13]
    tenant_code = row[14]
    salt = row[15]
    print "adding user " + email
    if not last_login_time:
        if not last_accessed_from:
            uniauth_cursor.execute("insert ignore into user values(null, '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', null, null, '%s', null,'%s', '%s')" % (email, normalized_email, verified_mobile, mobile_verified, email_verified, salt, password, enabled, name, blocked, reset_password, otp, created, updated))
        else:
            uniauth_cursor.execute("insert ignore into user values(null, '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', null, '%s', '%s',null, '%s', '%s')" % (email, normalized_email, verified_mobile, mobile_verified, email_verified, salt, password, enabled, name, blocked, last_accessed_from, reset_password, otp, created, updated))
    else:
        if not last_accessed_from:
            uniauth_cursor.execute("insert ignore into user values(null, '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', null,'%s', '%s')" % (email, normalized_email, verified_mobile, mobile_verified, email_verified, salt, password, enabled, name, blocked, last_login_time, reset_password, otp, created, updated))
        else:
            uniauth_cursor.execute("insert ignore into user values(null, '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', null, '%s', '%s')" % (email, normalized_email, verified_mobile, mobile_verified, email_verified, salt, password, enabled, name, blocked, last_login_time, last_accessed_from, reset_password, otp, created, updated))
    try:
        if not mobile:
            uniauth_cursor.execute("update user set mobile = '%s' where email = '%s'" % (unverified_mobile, email))
    except:
        print "Duplicate mobile for user " + email
    try:
        uniauth_cursor.execute("select id from user where email = '%s'" % email)
        user_id = uniauth_cursor.fetchone()[0]
        uniauth_db.commit()
        uniauth_cursor.execute("insert ignore into user_account values(null, '%s', '%s', '%s', '%s', '%s')" % (user_id, tenant_code, enabled, created, updated))
        uniauth_db.commit()
        print "done adding user " + email
    except:
        print "ignoring user " + email
f.close()
#os.remove("/tmp/temp_user_table_data.csv")