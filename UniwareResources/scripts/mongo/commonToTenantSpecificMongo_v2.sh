#!/bin/sh
MONGO_HOST=$2;
CACHE_INDEXES=$3;
if [ "$CACHE_INDEXES" = "true" ]; then
	echo "##### Caching channelInventoryUpdate_O2's indexes"
	mongo --host $MONGO_HOST -d uniware --eval "db.runCommand({ touch: 'channelInventoryUpdate_O2', data: false, index: true })"
	echo "##### Caching channelItemType's indexes"
	mongo --host $MONGO_HOST -d uniware --eval "db.runCommand({ touch: 'channelItemType', data: false, index: true })"
	echo "##### Caching channelInventoryUpdateSnapshot's data"
	mongo --host $MONGO_HOST -d uniware --eval "db.runCommand({ touch: 'channelInventoryUpdateSnapshot', data: true, index: true })"
fi
for SERVER_NAME in $@
do
	rm -rf /tmp/tenants.csv
	rm -rf /tmp/$SERVER_NAME
	mongoexport --host $MONGO_HOST -d uniware -c tenantProfile --fields tenantCode  -q '{serverName: "'$SERVER_NAME'"}' -o /tmp/tenants.csv;
	while read document; do
		TENANT_CODE=`echo $document | awk -F ":" '{print $4}' | awk -F '"' '{print $2}'`;
		echo "##### Working on "$TENANT_CODE;

		echo "##### Taking dump of broadcastMessage";
		mongodump --host $MONGO_HOST -d uniware -c broadcastMessage -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/broadcastMessage
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/broadcastMessage/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.broadcastMessage.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelCatalogSyncStatus";
		mongodump --host $MONGO_HOST -d uniware -c channelCatalogSyncStatus -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelCatalogSyncStatus
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelCatalogSyncStatus/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelCatalogSyncStatus.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelInventorySyncStatus";
		mongodump --host $MONGO_HOST -d uniware -c channelInventorySyncStatus -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelInventorySyncStatus
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelInventorySyncStatus/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelInventorySyncStatus.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelInventoryUpdateSnapshot";
		mongodump --host $MONGO_HOST -d uniware -c channelInventoryUpdateSnapshot -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelInventoryUpdateSnapshot
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelInventoryUpdateSnapshot/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelInventoryUpdateSnapshot.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelItemType";
		mongodump --host $MONGO_HOST -d uniware -c channelItemType -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelItemType
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelItemType/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelItemType.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelOrderSyncStatus";
		mongodump --host $MONGO_HOST -d uniware -c channelOrderSyncStatus -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelOrderSyncStatus
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelOrderSyncStatus/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelOrderSyncStatus.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelShipmentSyncStatus";
		mongodump --host $MONGO_HOST -d uniware -c channelShipmentSyncStatus -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelShipmentSyncStatus
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelShipmentSyncStatus/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelShipmentSyncStatus.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of printTemplate";
		mongodump --host $MONGO_HOST -d uniware -c printTemplate -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/printTemplate
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/printTemplate/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.printTemplate.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelInventoryUpdate";
		mongodump --host $MONGO_HOST -d uniware -c channelInventoryUpdate -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelInventoryUpdate
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelInventoryUpdate/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelInventoryUpdate.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelInventoryUpdate_O2";
		mongodump --host $MONGO_HOST -d uniware -c channelInventoryUpdate_O2 -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelInventoryUpdate_O2
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelInventoryUpdate_O2/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelInventoryUpdate_O2.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of methodActivityMeta";
		mongodump --host $MONGO_HOST -d uniware -c methodActivityMeta -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/methodActivityMeta
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/methodActivityMeta/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.methodActivityMeta.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelCatalogSyncMetadata";
		mongodump --host $MONGO_HOST -d uniware -c channelCatalogSyncMetadata -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelCatalogSyncMetadata
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelCatalogSyncMetadata/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelCatalogSyncMetadata.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of apiUserAccessStatistics";
		mongodump --host $MONGO_HOST -d uniware -c apiUserAccessStatistics -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/apiUserAccessStatistics
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/apiUserAccessStatistics/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.apiUserAccessStatistics.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of channelPaymentReconciliationHistory";
		mongodump --host $MONGO_HOST -d uniware -c channelPaymentReconciliationHistory -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/channelPaymentReconciliationHistory
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/channelPaymentReconciliationHistory/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.channelPaymentReconciliationHistory.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of entityAuditLogs";
		mongodump --host $MONGO_HOST -d uniware -c entityAuditLogs -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/entityAuditLogs
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/entityAuditLogs/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.entityAuditLogs.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of vendorInventoryLog";
		mongodump --host $MONGO_HOST -d uniware -c vendorInventoryLog -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/vendorInventoryLog
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/vendorInventoryLog/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.vendorInventoryLog.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of saleOrder";
		mongodump --host $MONGO_HOST -d uniware -c saleOrder -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/saleOrder
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/saleOrder/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.saleOrder.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of samplePrintTemplate";
		mongodump --host $MONGO_HOST -d uniware -c samplePrintTemplate -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/samplePrintTemplate
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/samplePrintTemplate/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.samplePrintTemplate.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of script";
		mongodump --host $MONGO_HOST -d uniware -c script -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/script
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/script/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.script.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of shippingManifestStatus";
		mongodump --host $MONGO_HOST -d uniware -c shippingManifestStatus -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/shippingManifestStatus
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/shippingManifestStatus/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.shippingManifestStatus.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of userNotification";
		mongodump --host $MONGO_HOST -d uniware -c userNotification -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/userNotification
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/userNotification/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.userNotification.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### Taking dump of userReadMessages";
		mongodump --host $MONGO_HOST -d uniware -c userReadMessages -q "{tenantCode: "\"$TENANT_CODE\""}" -o /tmp/$SERVER_NAME/userReadMessages
		echo "##### Restoring"
		mongorestore --host $MONGO_HOST -d $TENANT_CODE /tmp/$SERVER_NAME/userReadMessages/uniware
		mongo --host $MONGO_HOST -d uniware --eval "db.userReadMessages.remove({tenantCode: "\"$TENANT_CODE"\"})"

		echo "##### done";
		mongo --host $MONGO_HOST -d uniware --eval "db.commonToTenantSpecificMigrationStatus.insert({tenantCode: "\"$TENANT_CODE"\", created: new ISODate()})"
	done < /tmp/tenants.csv;
done
echo "##### FINISHED!"
