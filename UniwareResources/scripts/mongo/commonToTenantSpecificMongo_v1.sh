#!/bin/sh
MONGO_HOST=$2;
for SERVER_NAME in $@
do
Q="db.getSiblingDB('uniware')['tenantProfile'].find({serverName: "\"$SERVER_NAME\""}).forEach(function(t) { print ('WORKING ON: ' +t.tenantCode);
print ('Moving broadcastMessage'); var d = new Date();
db.broadcastMessage.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['broadcastMessage'].insert(doc); db.broadcastMessage.remove(doc)});
print ('Moved broadcastMessage in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelCatalogSyncStatus');
db.channelCatalogSyncStatus.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelCatalogSyncStatus'].insert(doc); db.channelCatalogSyncStatus.remove(doc)});
print ('Moved channelCatalogSyncStatus in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelInventorySyncStatus');
db.channelInventorySyncStatus.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelInventorySyncStatus'].insert(doc); db.channelInventorySyncStatus.remove(doc)});
print ('Moved channelInventorySyncStatus in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelInventoryUpdateSnapshot');
db.channelInventoryUpdateSnapshot.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelInventoryUpdateSnapshot'].insert(doc); db.channelInventoryUpdateSnapshot.remove(doc)});
print ('Moved channelInventoryUpdateSnapshot in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelItemType');
db.channelItemType.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelItemType'].insert(doc); db.channelItemType.remove(doc)});
print ('Moved channelItemType in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelOrderSyncStatus');
db.channelOrderSyncStatus.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelOrderSyncStatus'].insert(doc); db.channelOrderSyncStatus.remove(doc)});
print ('Moved channelOrderSyncStatus in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelShipmentSyncStatus');
db.channelShipmentSyncStatus.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelShipmentSyncStatus'].insert(doc); db.channelShipmentSyncStatus.remove(doc)});
print ('Moved channelShipmentSyncStatus in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving printTemplate');
db.printTemplate.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['printTemplate'].insert(doc); db.printTemplate.remove(doc)});
print ('Moved printTemplate in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelInventoryUpdate');
db.channelInventoryUpdate.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelInventoryUpdate'].insert(doc); db.channelInventoryUpdate.remove(doc)});
print ('Moved channelInventoryUpdate in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelInventoryUpdate_O2');
db.channelInventoryUpdate_O2.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelInventoryUpdate_O2'].insert(doc); db.channelInventoryUpdate_O2.remove(doc)});
print ('Moved channelInventoryUpdate_O2 in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving methodActivityMeta');
db.methodActivityMeta.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['methodActivityMeta'].insert(doc); db.methodActivityMeta.remove(doc)});
print ('Moved methodActivityMeta in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelCatalogSyncMetadata');
db.channelCatalogSyncMetadata.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelCatalogSyncMetadata'].insert(doc); db.channelCatalogSyncMetadata.remove(doc)});
print ('Moved channelCatalogSyncMetadata in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving apiUserAccessStatistics');
db.apiUserAccessStatistics.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['apiUserAccessStatistics'].insert(doc); db.apiUserAccessStatistics.remove(doc)});
print ('Moved apiUserAccessStatistics in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving channelPaymentReconciliationHistory');
db.channelPaymentReconciliationHistory.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['channelPaymentReconciliationHistory'].insert(doc); db.channelPaymentReconciliationHistory.remove(doc)});
print ('Moved channelPaymentReconciliationHistory in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving entityAuditLogs');
db.entityAuditLogs.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['entityAuditLogs'].insert(doc); db.entityAuditLogs.remove(doc)});
print ('Moved entityAuditLogs in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving vendorInventoryLog');
db.vendorInventoryLog.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['vendorInventoryLog'].insert(doc); db.vendorInventoryLog.remove(doc)});
print ('Moved vendorInventoryLog in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving saleOrder');
db.saleOrder.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['saleOrder'].insert(doc); db.saleOrder.remove(doc)});
print ('Moved saleOrder in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving samplePrintTemplate');
db.samplePrintTemplate.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['samplePrintTemplate'].insert(doc); db.samplePrintTemplate.remove(doc)});
print ('Moved samplePrintTemplate in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving script');
db.script.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['script'].insert(doc); db.script.remove(doc)});
print ('Moved script in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving shippingManifestStatus');
db.shippingManifestStatus.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['shippingManifestStatus'].insert(doc); db.shippingManifestStatus.remove(doc)});
print ('Moved shippingManifestStatus in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving systemNotification');
db.systemNotification.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['systemNotification'].insert(doc); db.systemNotification.remove(doc)});
print ('Moved systemNotification in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving userNotification');
db.userNotification.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['userNotification'].insert(doc); db.userNotification.remove(doc)});
print ('Moved userNotification in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('Moving userReadMessages');
db.userReadMessages.find({tenantCode: t.tenantCode}).forEach(function(doc){db.getSiblingDB(doc.tenantCode)['userReadMessages'].insert(doc); db.userReadMessages.remove(doc)});
print ('Moved userReadMessages in '+ (new Date() - d)/1000 + ' secs'); var d = new Date();

print ('----------------');
})"
mongo --quiet --host $MONGO_HOST uniware --eval "$Q"
done