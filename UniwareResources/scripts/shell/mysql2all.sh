#!/bin/bash
if [ ${#} -lt "1" ]; then
   echo "Usage: `basename $0` SQL_FILE SERVER_NAMES "
   echo "   SQL file will be executed on all servers provided as comma separated names"
   echo "   Examples:"
   echo "     `basename $0` x.sql Demo"
   echo "     `basename $0` x.sql Demo,Xerion,Tyrian,Bluestone"
   exit 1
fi

function executeSQL() {
    SERVER_NAME=$1
    SERVER_IP_VAR="${SERVER_NAME}_IP"
    SERVER_MYSQL_VAR="${SERVER_NAME}_MYSQL"
    if [ "${!SERVER_IP_VAR}" != "" ]; then
        echo "executing sql on server $SERVER_NAME:${!SERVER_IP_VAR}"
        scp -i ~/.ssh/Build.pem $2 build@${!SERVER_IP_VAR}:/home/build/release/tmpSql
        if [ "${!SERVER_MYSQL_VAR}" != "" ]; then
                ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "mysql -uroot -puniware -h${!SERVER_MYSQL_VAR} uniware < /home/build/release/tmpSql"
        else
                ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "mysql -uroot -puniware uniware < /home/build/release/tmpSql"
        fi
        ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "sudo rm -f /home/build/release/tmpSql"
   else
        echo "Unknown Server $SERVER_NAME" 
   fi
}

. /home/ec2-user/servers.conf
if [ "$2" = "All" ]; then
  for line in $(cat servers.conf|grep _IP)
  do
    SERVER_NAME=`echo $line|awk -F "=" '{print $1}'|awk -F "_" '{print $1}'`
    executeSQL $SERVER_NAME $1
  done
else
  export IFS=','
  for SERVER_NAME in $2
  do
    executeSQL $SERVER_NAME $1
  done
fi