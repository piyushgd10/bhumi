#!/bin/sh
if [ "$3" = "All" ]; then
  for line in $(cat servers.conf|grep _IP)
  do
    SERVER_NAME=`echo $line|awk -F "=" '{print $1}'`
    SERVER_IP=`echo $line|awk -F "=" '{print $2}'`
    echo "Copying on server:$SERVER_NAME"
    ssh -t -i ~/.ssh/Build.pem build@$SERVER_IP "sudo grep '$2' $1"
  done
else
  export IFS=','
  for SERVER in $3
  do
     echo $SERVER
     SERVER_IP=`cat servers.conf|grep ${SERVER}_IP|awk -F "=" '{print $2}'`
      ssh -t -i ~/.ssh/Build.pem build@$SERVER_IP "sudo grep '$2' $1"
  done
fi