#!/bin/sh
ARTIFACT_NAME="uniware.`date +'%Y-%m-%d-%H-%M'`.tar"
. /home/ec2-user/servers.conf
cd /home/ec2-user/Unifier
git pull
mvn clean install
cd /home/ec2-user/Uniware
git pull
rm -rf /home/ec2-user/release/uniware
mvn clean package
tar -cf /home/ec2-user/release/$ARTIFACT_NAME -C/home/ec2-user/release/uniware .
for SERVER_NAME in $@
do
   SERVER_IP_VAR="${SERVER_NAME}_IP"
   SERVER_MYSQL_VAR="${SERVER_NAME}_MYSQL"
   if [ "${!SERVER_IP_VAR}" != "" ]; then
     echo "copying distribution to server $SERVER_NAME:${!SERVER_IP_VAR}"
     scp -i ~/.ssh/Build.pem /home/ec2-user/release/$ARTIFACT_NAME build@${!SERVER_IP_VAR}:/home/build/release
     cd /home/ec2-user/Uniware/
     git diff $SERVER_NAME:build.sql build.sql |grep -e "^+[^+]"| sed -e "s/^+//g" > /home/ec2-user/release/delta_full_$SERVER_NAME.sql
     sed -e "s/^\s*(.*$SERVER_NAME[^)]*)//g" /home/ec2-user/release/delta_full_$SERVER_NAME.sql |sed -e "s/^\s*([^)]*)/-- /g"|sed -e "s/^\s*).*$SERVER_NAME[^(]*(/-- /g"|sed -e "s/^\s*)[^(]*(//g" > /home/ec2-user/release/delta_$SERVER_NAME.sql
     echo "executing SQL Changes"
     cat /home/ec2-user/release/delta_$SERVER_NAME.sql
     scp -i ~/.ssh/Build.pem /home/ec2-user/release/delta_$SERVER_NAME.sql build@${!SERVER_IP_VAR}:/home/build/release
     if [ "${!SERVER_MYSQL_VAR}" != "" ]; then
       ssh -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "mysql -uroot -puniware -h${!SERVER_MYSQL_VAR} uniware < /home/build/release/delta_$SERVER_NAME.sql"
     else
       ssh -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "mysql -uroot -puniware uniware < /home/build/release/delta_$SERVER_NAME.sql"
     fi
     git tag -d "$SERVER_NAME"
     git tag -a "$SERVER_NAME" -m "$SERVER_NAME"
     echo "Deploying and Restarting Server"
     ssh -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} rm -rf /home/build/release/uniware/*
     ssh -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} tar -xf /home/build/release/$ARTIFACT_NAME -C /home/build/release/uniware
     ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} sudo /usr/local/apache-tomcat/bin/shutdown.sh
     ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "sudo ps xu|grep tomcat|grep -v grep|awk '{ print \$2 }'|xargs sudo kill -9"
     ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} sudo rm -rf /usr/local/apache-tomcat/webapps/ROOT/*
     ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} sudo cp -r /home/build/release/uniware/* /usr/local/apache-tomcat/webapps/ROOT/
     ssh -t -i ~/.ssh/Build.pem build@${!SERVER_IP_VAR} "sudo nohup /usr/local/apache-tomcat/bin/startup.sh </dev/null > /dev/null 2>\&1 \&"
   fi
done