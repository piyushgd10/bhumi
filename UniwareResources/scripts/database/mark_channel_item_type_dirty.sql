DELIMITER $$
DROP EVENT IF EXISTS `mark_channel_item_type_dirty`$$
CREATE DEFINER=`root`@`localhost` EVENT `mark_channel_item_type_dirty` ON SCHEDULE EVERY 24 HOUR STARTS '2014-08-22 23:00:00' ON COMPLETION PRESERVE ENABLE DO
BEGIN
    update channel_item_type set possibly_dirty = 0, dirty = 1 where possibly_dirty = 1 and dirty = 0 and status_code = 'LINKED' and disabled_due_to_errors = 0 and disabled = 0;
END $$
DELIMITER ;