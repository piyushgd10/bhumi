CREATE EVENT close_picklist 
    ON SCHEDULE EVERY 30 MINUTE 
    DO 
        UPDATE picklist SET status_code = 'CLOSED' WHERE status_code != 'CLOSED' and id NOT IN(SELECT DISTINCT pbs.picklist_id FROM pick_bucket_slot pbs, shipping_package sp, sale_order_item soi WHERE pbs.shipping_package_id = sp.id AND sp.id = soi.shipping_package_id AND sp.status_code IN ('PICKING', 'PICKED')) and id not in (select pbs.picklist_id from pick_bucket_slot pbs, pick_bucket_slot_item pbsi where pbsi.pick_bucket_slot_id = pbs.id and pbsi.status_code = 'PUTBACK_PENDING');

DELIMITER $$
DROP EVENT IF EXISTS `complete_shipment_batch`$$
CREATE EVENT complete_shipment_batch
    ON SCHEDULE EVERY 30 MINUTE
DO BEGIN
    set autocommit = 0;
    create temporary table t as SELECT DISTINCT facility_id, shipment_batch_code FROM shipping_package WHERE shipment_batch_code is not null and status_code IN ('CREATED','LOCATION_NOT_SERVICEABLE','PICKING', 'PICKED', 'PENDING_CUSTOMIZATION', 'CUSTOMIZATION_COMPLETE', 'PACKED', 'READY_TO_SHIP');
    alter table t add index(facility_id, shipment_batch_code);
    UPDATE shipment_batch sb left join t on (sb.facility_id = t.facility_id and sb.code = t.shipment_batch_code) SET sb.status_code = 'COMPLETE' WHERE t.facility_id is null;
    commit;
END$$
DELIMITER ;