DELIMITER $$
USE `uniware`$$

DROP TRIGGER `reverse_pickup_status_create_trigger`$$

CREATE TRIGGER `reverse_pickup_status_create_trigger` AFTER INSERT ON `reverse_pickup`
FOR EACH ROW
BEGIN
  SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
  INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('ReversePickup', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', 'NEW', NEW.status_code, NEW.facility_id, @TENANT_ID);
END $$

DELIMITER ;

DELIMITER $$
USE `uniware`$$

DROP TRIGGER `reverse_pickup_status_chage_trigger`$$

CREATE TRIGGER `reverse_pickup_status_chage_trigger` AFTER UPDATE ON `reverse_pickup`
FOR EACH ROW
BEGIN
    IF NEW.status_code != OLD.status_code THEN
        SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
        INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('ReversePickup', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
    END IF;
END $$

DELIMITER ;
