DELIMITER $$

DROP TRIGGER IF EXISTS `gatepass_create_trigger`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `gatepass_create_trigger` AFTER INSERT ON `outbound_gate_pass`
FOR EACH ROW
BEGIN
    SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
    INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('Gatepass', NEW.id, concat('Gatepass-', NEW.id), 'StatusCode', 'NEW', NEW.status_code, NEW.facility_id, @TENANT_ID);
END $$

DROP TRIGGER IF EXISTS `gatepass_status_change_trigger`$$
CREATE TRIGGER `gatepass_status_change_trigger` AFTER UPDATE ON `outbound_gate_pass`
FOR EACH ROW
BEGIN
	IF NEW.status_code != OLD.status_code THEN
	   SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
	   INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('Gatepass', NEW.id, concat('Gatepass-', NEW.id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
	END IF;
END $$

DELIMITER ;