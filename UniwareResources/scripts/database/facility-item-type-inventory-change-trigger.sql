DELIMITER $$

DROP TRIGGER IF EXISTS `facility_item_type_inventory_change_trigger`$$

CREATE DEFINER =`root`@`localhost` TRIGGER `facility_item_type_inventory_change_trigger` AFTER UPDATE ON `facility_item_type`
FOR EACH ROW
  BEGIN
    DECLARE CHANGE_QUANTITY INT;
    IF NEW.inventory != OLD.inventory THEN
      SET CHANGE_QUANTITY = CAST(NEW.inventory AS SIGNED) - CAST(OLD.inventory AS SIGNED);
      UPDATE item_type_inventory_snapshot SET vendor_inventory = vendor_inventory + CHANGE_QUANTITY, acknowledged = 0 WHERE item_type_id = NEW.item_type_id AND facility_id = NEW.facility_id;
    END IF;
  END $$
DELIMITER ;

DELIMITER $$
DROP TRIGGER IF EXISTS `facility_item_type_inventory_add_trigger`$$
CREATE DEFINER =`root`@`localhost` TRIGGER `facility_item_type_inventory_add_trigger` AFTER INSERT ON `facility_item_type`
FOR EACH ROW
  BEGIN
    DECLARE ROWS_AFFECTED INT;
    INSERT IGNORE INTO item_type_inventory_snapshot (facility_id, item_type_id, vendor_inventory, acknowledged, created) VALUES (NEW.facility_id, NEW.item_type_id, NEW.inventory, 0, now());
    SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
    IF @ROWS_AFFECTED = 0 THEN
      UPDATE item_type_inventory_snapshot SET vendor_inventory = vendor_inventory + NEW.inventory, acknowledged = 0 WHERE item_type_id = NEW.item_type_id AND facility_id = NEW.facility_id;
    END IF;
  END $$

DELIMITER ;