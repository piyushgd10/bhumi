
DELIMITER $$

DROP TRIGGER IF EXISTS `gatepass_item_insert_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `gatepass_item_insert_trigger` AFTER INSERT ON `outbound_gate_pass_item`
FOR EACH ROW
BEGIN
    DECLARE GATEPASS_TYPE VARCHAR(45);
    DECLARE TO_FACILITY INT;
    DECLARE ROWS_AFFECTED INT;
    SELECT type,to_party_id into @GATEPASS_TYPE, @TO_FACILITY from outbound_gate_pass where id = NEW.outbound_gate_pass_id;
    IF @GATEPASS_TYPE = 'STOCK_TRANSFER' THEN
           UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
           SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
           IF @ROWS_AFFECTED = 0 THEN
               INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, pending_stock_transfer, acknowledged, created) values (@TO_FACILITY, NEW.item_type_id, NEW.quantity, 0, now());
               SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
               IF @ROWS_AFFECTED = 0 THEN
                   UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
               END IF;
           END IF;
    END IF;
END $$

DROP TRIGGER IF EXISTS `gatepass_item_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `gatepass_item_change_trigger` AFTER UPDATE ON `outbound_gate_pass_item`
FOR EACH ROW
BEGIN
    DECLARE CHANGE_QUANTITY INT;
    DECLARE GATEPASS_TYPE VARCHAR(45);
    DECLARE TO_FACILITY INT;
    DECLARE ROWS_AFFECTED INT;
    IF NEW.quantity != OLD.quantity THEN
       SET CHANGE_QUANTITY = CAST(NEW.quantity AS SIGNED) - CAST(OLD.quantity AS SIGNED);
       SELECT type,to_party_id into @GATEPASS_TYPE, @TO_FACILITY from outbound_gate_pass where id = NEW.outbound_gate_pass_id;
       IF @GATEPASS_TYPE = 'STOCK_TRANSFER' THEN
           UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
           SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
           IF @ROWS_AFFECTED = 0 THEN
               INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, pending_stock_transfer, acknowledged, created) values (@TO_FACILITY, NEW.item_type_id, NEW.quantity, 0, now());
               SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
               IF @ROWS_AFFECTED = 0 THEN
                   UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
               END IF;
           END IF;         
       END IF;
    END IF;
    IF NEW.received_quantity != OLD.received_quantity THEN
       SET CHANGE_QUANTITY = CAST(NEW.received_quantity AS SIGNED) - CAST(OLD.received_quantity AS SIGNED);
       SELECT type,to_party_id into @GATEPASS_TYPE, @TO_FACILITY from outbound_gate_pass where id = NEW.outbound_gate_pass_id;
       IF @GATEPASS_TYPE = 'STOCK_TRANSFER' THEN
           UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer - CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
           SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
           IF @ROWS_AFFECTED = 0 THEN
               INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, pending_stock_transfer, acknowledged, created) values (@TO_FACILITY, NEW.item_type_id, NEW.received_quantity, 0, now());
               SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
               IF @ROWS_AFFECTED = 0 THEN
                   UPDATE item_type_inventory_snapshot set pending_stock_transfer = pending_stock_transfer - CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @TO_FACILITY;
               END IF;
           END IF;           
       END IF;
    END IF;
END $$

DELIMITER ;

DELIMITER $$
