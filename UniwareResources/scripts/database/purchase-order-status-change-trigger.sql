DELIMITER $$

DROP TRIGGER IF EXISTS `purchase_order_status_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `purchase_order_status_change_trigger` AFTER UPDATE ON `purchase_order`
FOR EACH ROW
BEGIN
    DECLARE DONE INT DEFAULT 0;
    DECLARE ITEM_TYPE INT;
    DECLARE ITEM_QUANTITY INT;
    DECLARE ROWS_AFFECTED INT;
    DECLARE CUR1 CURSOR FOR SELECT item_type_id,quantity FROM purchase_order_item WHERE purchase_order_id = NEW.id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE = 1;
    IF NEW.status_code != OLD.status_code THEN
        IF OLD.status_code = 'CREATED' and (NEW.status_code = 'WAITING_FOR_APPROVAL' or NEW.status_code = 'APPROVED' ) THEN
            OPEN CUR1; 
            REPEAT
                FETCH CUR1 INTO ITEM_TYPE, ITEM_QUANTITY;
                IF NOT DONE THEN
                    UPDATE item_type_inventory_snapshot set open_purchase = open_purchase + ITEM_QUANTITY, acknowledged = 0 where item_type_id = ITEM_TYPE and facility_id = NEW.facility_id;
                    SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
                    IF @ROWS_AFFECTED = 0 THEN
                        INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, open_purchase, acknowledged, created) values (NEW.facility_id, ITEM_TYPE, ITEM_QUANTITY, 0, now());
                        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
                        IF @ROWS_AFFECTED = 0 THEN
                            UPDATE item_type_inventory_snapshot set open_purchase = open_purchase + ITEM_QUANTITY, acknowledged = 0 where item_type_id = ITEM_TYPE and facility_id = NEW.facility_id;
                        END IF;
                    END IF;                    
                END IF;
            UNTIL DONE END REPEAT;
            CLOSE CUR1;
        END IF;
        IF ((OLD.status_code = 'WAITING_FOR_APPROVAL' and NEW.status_code = 'REJECTED') or (OLD.status_code = 'APPROVED' and NEW.status_code = 'AMENDED')) THEN
            OPEN CUR1; 
            REPEAT
                FETCH CUR1 INTO ITEM_TYPE, ITEM_QUANTITY;
                IF NOT DONE THEN
                    UPDATE item_type_inventory_snapshot set open_purchase = open_purchase - ITEM_QUANTITY where item_type_id = ITEM_TYPE and facility_id = NEW.facility_id;
                END IF;
            UNTIL DONE END REPEAT;
            CLOSE CUR1;
        END IF;
        SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
        INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, user_id, facility_id, tenant_id) values('PurchaseOrder', NEW.id, concat('PurchaseOrder-', NEW.id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.last_updated_by, NEW.facility_id, @TENANT_ID);
    END IF;
    
END $$

DELIMITER $$
DROP TRIGGER IF EXISTS `purchase_order_item_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `purchase_order_item_change_trigger` AFTER UPDATE ON `purchase_order_item`
FOR EACH ROW
BEGIN
    DECLARE CURRENT_FACILITY_ID INT;
    IF NEW.cancelled_quantity != OLD.cancelled_quantity THEN
        SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `purchase_order` WHERE id = NEW.purchase_order_id;
        UPDATE item_type_inventory_snapshot set open_purchase = open_purchase - NEW.cancelled_quantity where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
    END IF;
END $$

DELIMITER ;
