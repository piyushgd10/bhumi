DELIMITER $$
CREATE EVENT `kill_long_queries`
ON SCHEDULE EVERY 1 MINUTE
DO BEGIN
    DECLARE IS_COMPLETE INT DEFAULT FALSE;
    DECLARE PID INT;
    DECLARE cur CURSOR FOR  select ID from information_schema.PROCESSLIST where COMMAND = 'Query' and INFO like 'select%' and TIME > 150;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET IS_COMPLETE = TRUE;
    SET autocommit = 0;

    OPEN cur;
    query_loop: LOOP
      FETCH cur INTO PID;
      IF IS_COMPLETE THEN
        LEAVE query_loop;
      END IF;
      kill PID;
    END LOOP;
    COMMIT;
  END $$
DELIMITER ;