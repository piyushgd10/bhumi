DELIMITER $$
USE `uniware`$$

DROP TRIGGER IF EXISTS `reshipment_status_chage_trigger`$$

CREATE TRIGGER `reshipment_status_chage_trigger` AFTER UPDATE ON `reshipment`
FOR EACH ROW
BEGIN
    IF NEW.status_code != OLD.status_code THEN
        SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
        INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('Reshipment', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
    END IF;
END $$

DELIMITER ;
