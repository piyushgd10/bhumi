
--sections
INSERT INTO `section`(`id`,`code`,`name`,`created`,`updated`) VALUES(2,"APP","Apparels",now(),now());
INSERT INTO `section`(`id`,`code`,`name`,`created`,`updated`) VALUES(3,"COM","Computers",now(),now());

--categories
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`, `updated`) VALUES (2, 1, 'LED', 'LED TV',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`, `updated`) VALUES (3, 1, 'LCD', 'LCD TV',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (4, 2, 'COA', 'Coats',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (5, 2, 'SHI', 'Shirts',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (6, 2, 'TRO', 'Trousers',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (7, 3, 'MON', 'Monitors',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (8, 3, 'KEY', 'Keyboards',now(),now());
INSERT INTO `uniware`.`category` (`id`, `section_id`, `code`, `name`, `created`) VALUES (9, 3, 'PRO', 'Processors',now(),now());

--items

--Email Channel
--INSERT INTO `uniware`.`email_channel` (`id`, `name`, `class_name`) VALUES (1, 'GmailSMTP', --'com.uniware.services.email.SMTPEmailSender');
--INSERT INTO `uniware`.`email_channel_parameter` (`id`, `email_channel_id`, `name`, `value`) VALUES ('1', '1', 'host', --'smtp.gmail.com');
--INSERT INTO `uniware`.`email_channel_parameter` (`id`, `email_channel_id`, `name`, `value`) VALUES ('2', '1', 'port', '587');
--INSERT INTO `uniware`.`email_channel_parameter` (`id`, `email_channel_id`, `name`, `value`) VALUES ('3', '1', 'username', --'noreply@unicommerce.com');
--INSERT INTO `uniware`.`email_channel_parameter` (`id`, `email_channel_id`, `name`, `value`) VALUES ('4', '1', 'password', --'unicom123');
--INSERT INTO `uniware`.`email_template` (`email_channel_id`, `type`, `subject_template`, `body_template`, `from_email`, `reply_to_email`, `created`) VALUES ('1', 'testTemplate', '\"This is a test email\"', '\"Integration is complete\"', 'noreply@unicommerce.com', 'noreply@unicommerce.com', '2011-12-21');

--buckettypes
INSERT INTO `uniware`.`bucket_type` (`id`, `code`, `slot_length`, `slot_width`, `slot_height`, `slot_count`) VALUES (101, 'bucketType1', 250, 200, 150, 10);
INSERT INTO `uniware`.`bucket_type` (`id`, `code`, `slot_length`, `slot_width`, `slot_height`, `slot_count`) VALUES (102, 'bucketType2', 500, 400, 300, 4);
INSERT INTO `uniware`.`bucket_type` (`id`, `code`, `slot_length`, `slot_width`, `slot_height`, `slot_count`) VALUES (103, 'bucketType3', 100000, 100000, 100000, 1);

--buckets
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1001', '101', 'B1', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1002', '101', 'B2', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1003', '101', 'B3', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1004', '101', 'B4', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1005', '102', 'B5', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1006', '102', 'B6', '2011-11-11 15:03:05', '2011-11-11 15:03:05');
INSERT INTO `uniware`.`bucket` (`id`,`bucket_type_id`,`code`,`created`,`updated`) VALUES ('1007', '103', 'B7', '2011-11-11 15:03:05', '2011-11-11 15:03:05');







