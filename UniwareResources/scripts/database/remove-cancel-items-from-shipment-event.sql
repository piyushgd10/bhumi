DELIMITER $$
DROP EVENT IF EXISTS `remove_cancel_items_from_shipment`$$
CREATE DEFINER=`root`@`localhost` EVENT `remove_cancel_items_from_shipment` ON SCHEDULE EVERY 15 MINUTE STARTS '2013-06-26 01:00:00' ON COMPLETION NOT PRESERVE ENABLE DO
BEGIN
    create temporary table t as select distinct sp.id from sale_order_item soi,shipping_package sp where soi.shipping_package_id=sp.id and soi.status_code='CANCELLED' and sp.shipping_manifest_id is null and sp.status_code in ('PACKED','READY_TO_SHIP');
    alter table t add index(id);
	update shipping_package sp,sale_order_item soi,t set soi.shipping_package_id=null,sp.status_code='CANCELLED' where t.id=sp.id and soi.shipping_package_id=sp.id;
END $$
DELIMITER ;