
DELIMITER $$

DROP TRIGGER IF EXISTS `sale_order_status_update_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `sale_order_status_update_trigger` AFTER UPDATE ON `sale_order_item`
FOR EACH ROW
BEGIN
    DECLARE ROWS_AFFECTED INT;
    DECLARE ITEM_COUNT INT;
    DECLARE NOT_CANCELLED_ITEM_COUNT INT;
    IF NEW.status_code != OLD.status_code THEN
        IF NEW.status_code in ('CANCELLED') THEN
            UPDATE sale_order so, channel c set so.reconciliation_pending = 1 where so.channel_id = c.id and c.reconciliation_sync_status = 'ON' and so.id = NEW.sale_order_id;
        END IF;
        SELECT count(*) INTO @ITEM_COUNT FROM sale_order_item WHERE sale_order_id = NEW.sale_order_id and status_code not in ('RESHIPPED', 'DELIVERED', 'DISPATCHED', 'REPLACED', 'CANCELLED', 'ALTERNATE_ACCEPTED');
        IF @ITEM_COUNT = 0 THEN
            IF NEW.status_code != 'CANCELLED' THEN
                UPDATE sale_order set status_code = 'COMPLETE' where id = NEW.sale_order_id;
            ELSE
                SELECT count(*) INTO @NOT_CANCELLED_ITEM_COUNT FROM sale_order_item WHERE sale_order_id = NEW.sale_order_id and status_code not in ( 'CANCELLED', 'ALTERNATE_ACCEPTED');
                IF @NOT_CANCELLED_ITEM_COUNT = 0 THEN
                    UPDATE sale_order set status_code = 'CANCELLED' where id = NEW.sale_order_id;
                ELSE
                    UPDATE sale_order set status_code = 'COMPLETE' where id = NEW.sale_order_id;
                END IF;
            END IF;
        END IF;
        SET @TENANT_ID = (SELECT tenant_id FROM sale_order where id = NEW.sale_order_id);
        IF NEW.facility_id IS NOT NULL THEN
            INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('SaleOrderItem', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
        ELSE
            INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, tenant_id) values('SaleOrderItem', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', OLD.status_code, NEW.status_code, @TENANT_ID);            
        END IF;
        IF NEW.status_code = 'UNFULFILLABLE' THEN
            UPDATE item_type_inventory_snapshot set open_sale = open_sale + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, open_sale, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, 1, 0, now());
                SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
                IF @ROWS_AFFECTED = 0 THEN
                    UPDATE item_type_inventory_snapshot set open_sale = open_sale + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
                END IF;
            END IF;
        ELSEIF OLD.status_code = 'UNFULFILLABLE' THEN
            UPDATE item_type_inventory_snapshot set open_sale = open_sale - 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = OLD.facility_id;
        END IF;
    END IF;
    IF NEW.status_code = 'CREATED' and NEW.facility_id IS NOT NULL and (OLD.facility_id IS NULL or OLD.facility_id != NEW.facility_id or OLD.status_code != NEW.status_code) THEN
        UPDATE item_type_inventory_snapshot set pending_inventory_assessment = pending_inventory_assessment + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, pending_inventory_assessment, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, 1, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set pending_inventory_assessment = pending_inventory_assessment + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
    END IF;
    IF OLD.status_code = 'CREATED' and OLD.facility_id IS NOT NULL and (NEW.facility_id IS NULL or OLD.facility_id != NEW.facility_id or OLD.status_code != NEW.status_code) THEN
        UPDATE item_type_inventory_snapshot set pending_inventory_assessment = pending_inventory_assessment - 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = OLD.facility_id;
    END IF;
END $$
DELIMITER ;

DELIMITER $$
DROP TRIGGER IF EXISTS `create_sale_order_item_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `create_sale_order_item_trigger` AFTER INSERT ON `sale_order_item`
FOR EACH ROW
BEGIN
   IF NEW.status_code = 'CREATED' and NEW.facility_id IS NOT NULL THEN
        UPDATE item_type_inventory_snapshot set pending_inventory_assessment = pending_inventory_assessment + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, pending_inventory_assessment, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, 1, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set pending_inventory_assessment = pending_inventory_assessment + 1, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
    END IF;
END $$
DELIMITER ;


