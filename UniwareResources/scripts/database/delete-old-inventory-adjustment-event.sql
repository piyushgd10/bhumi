CREATE EVENT delete_old_inventory_adjustment
    ON SCHEDULE EVERY 24 HOUR
    DO 
        delete from inventory_adjustment where created < date(now() + interval -90 day);
