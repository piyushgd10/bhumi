DELIMITER $$

DROP PROCEDURE IF EXISTS `get_item_codes_fulfillable`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_item_codes_fulfillable`()
BEGIN
	DECLARE ITEM_SKU_ID INT;
	DECLARE SKU_COUNT INT;
	DECLARE DONE INT DEFAULT 0;
	DECLARE CUR1 CURSOR FOR select item_type_id, count(*) from sale_order_item where status_code = 'FULFILLABLE' and item_id is null group by item_type_id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET DONE = 1;
	DROP TABLE temp_item_code;
	CREATE TABLE temp_item_code (code VARCHAR(45) not null,sku_code VARCHAR(45) not null,name VARCHAR(255) not null, item_type_id int not null);
	OPEN CUR1;
	REPEAT
		FETCH CUR1 INTO ITEM_SKU_ID, SKU_COUNT;
		IF NOT DONE THEN
			INSERT INTO temp_item_code SELECT i.code, it.sku_code, it.name, it.id FROM item i, item_type it WHERE i.item_type_id = it.id and i.status_code = 'GOOD_INVENTORY' and it.id = ITEM_SKU_ID order by i.created limit SKU_COUNT;
		END IF;
	UNTIL DONE END REPEAT;
	CLOSE CUR1;
	SELECT * from temp_item_code;
END$$

DELIMITER ;