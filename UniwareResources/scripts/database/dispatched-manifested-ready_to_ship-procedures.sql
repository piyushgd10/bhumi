DELIMITER //
DROP PROCEDURE if EXISTS dispatchedToManifested //
DROP PROCEDURE if EXISTS manifestedToReadyToShip //
DROP PROCEDURE if EXISTS dispatchedToReadyToShip //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE dispatchedToManifested(IN shipmentCode varchar(45),IN facilityId int(10) unsigned)
BEGIN
declare shipmentTrackingNumber varchar(45);
declare shippingManager varchar(15);
declare shippingProviderId int(10) unsigned;
declare shippingProviderCode varchar(128);
declare tenantId int(10) unsigned;
declare shippingPackageId int(10) unsigned;
SET shippingPackageId=(SELECT id from shipping_package where code=shipmentCode and facility_id=facilityId);
SET shipmentTrackingNumber= (SELECT tracking_number from shipping_package where id=shippingPackageId);
SET shippingManager= (SELECT shipping_manager from shipping_package where id=shippingPackageId);
IF shippingManager='UNIWARE' THEN
SET tenantId=(SELECT tenant_id from shipping_package where id=shippingPackageId);
SET shippingProviderCode=(SELECT shipping_provider_code from shipping_package where id=shippingPackageId);
SET shippingProviderId=(SELECT id from shipping_provider where code=shippingProviderCode and tenant_id=tenantId);
DELETE from shipment_tracking where tracking_number=shipmentTrackingNumber and shipping_provider_id=shippingProviderId;
END IF;
UPDATE shipping_package set status_code="MANIFESTED", dispatched_on_channel=0,shipment_tracking_status_code=null,provider_status_code=null,dispatch_time=null where id=shippingPackageId;
UPDATE sale_order_item set status_code= "MANIFESTED" where shipping_package_id=shippingPackageId;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE manifestedToReadyToShip(IN shipmentCode varchar(45),IN facilityId int(10) unsigned)
BEGIN
declare shipmentManifestId int(10) unsigned;
declare shippingPackageId int(10) unsigned;
SET shippingPackageId = (SELECT id from shipping_package where code=shipmentCode and facility_id=facilityId);
SET shipmentManifestId = (SELECT shipping_manifest_id from shipping_package where id=shippingPackageId);
UPDATE shipping_package set status_code="READY_TO_SHIP" where id=shippingPackageId;
DELETE  from shipping_manifest_item where shipping_manifest_id=shipmentManifestId and shipping_package_id=shippingPackageId;
UPDATE shipping_package set shipping_manifest_id=null where id=shippingPackageId;
UPDATE sale_order_item set status_code= "FULFILLABLE" where shipping_package_id=shippingPackageId;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE dispatchedToReadyToShip(IN shipmentCode varchar(45),IN facilityId int(10) unsigned)
BEGIN
call dispatchedToManifested(shipmentCode,facilityId);
call manifestedToReadyToShip(shipmentCode,facilityId);
END //
DELIMITER ;