DELIMITER $$

DROP TRIGGER IF EXISTS `putaway_status_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `putaway_status_change_trigger` AFTER UPDATE ON `putaway`
FOR EACH ROW
BEGIN
	DECLARE ITEM_COUNT INT;
	IF NEW.status_code != OLD.status_code THEN
	  SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
		INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('Putaway', NEW.id, concat('Putaway-', NEW.id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
	END IF;
END $$

DELIMITER ;