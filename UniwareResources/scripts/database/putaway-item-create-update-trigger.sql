
DELIMITER $$

DROP TRIGGER IF EXISTS `putaway_item_update_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `putaway_item_update_trigger` AFTER UPDATE ON `putaway_item`
FOR EACH ROW
BEGIN
    DECLARE CHANGE_QUANTITY INT;
    DECLARE CURRENT_FACILITY_ID INT;
    DECLARE ROWS_AFFECTED INT;
    IF NEW.status_code = 'CREATED' and NEW.type='GOOD_INVENTORY' THEN
        SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `putaway` WHERE id = NEW.putaway_id;
        SET CHANGE_QUANTITY = CAST(NEW.quantity AS SIGNED) - CAST(OLD.quantity AS SIGNED);
        UPDATE item_type_inventory_snapshot set putaway_pending = putaway_pending + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, putaway_pending, acknowledged, created) values (@CURRENT_FACILITY_ID, NEW.item_type_id, NEW.quantity, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set putaway_pending = putaway_pending + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
            END IF;
        END IF;
    ELSEIF NEW.status_code != OLD.status_code and NEW.type='GOOD_INVENTORY' and NEW.status_code = 'COMPLETE' THEN
        SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `putaway` WHERE id = NEW.putaway_id;
        UPDATE item_type_inventory_snapshot set putaway_pending = putaway_pending - NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
    END IF;
END $$

DROP TRIGGER IF EXISTS `putaway_item_insert_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `putaway_item_insert_trigger` AFTER INSERT ON `putaway_item`
FOR EACH ROW
BEGIN
    DECLARE ROWS_AFFECTED INT;
    DECLARE CURRENT_FACILITY_ID INT;
    IF NEW.status_code = 'CREATED'  and NEW.type='GOOD_INVENTORY' THEN
        SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `putaway` WHERE id = NEW.putaway_id;
        UPDATE item_type_inventory_snapshot set putaway_pending = putaway_pending + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, putaway_pending, acknowledged, created) values (@CURRENT_FACILITY_ID, NEW.item_type_id, NEW.quantity, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set putaway_pending = putaway_pending + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
            END IF;
        END IF;
    END IF;
END $$

DELIMITER ;
