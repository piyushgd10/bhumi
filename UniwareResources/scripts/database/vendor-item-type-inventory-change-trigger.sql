DELIMITER $$

DROP TRIGGER IF EXISTS `vendor_item_type_inventory_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `vendor_item_type_inventory_change_trigger` AFTER UPDATE ON `vendor_item_type`
FOR EACH ROW
BEGIN
    DECLARE CHANGE_QUANTITY INT;
    IF NEW.vendor_inventory != OLD.vendor_inventory THEN
        SET CHANGE_QUANTITY = CAST(NEW.vendor_inventory AS SIGNED) - CAST(OLD.vendor_inventory AS SIGNED);
        UPDATE item_type_inventory_snapshot set vendor_inventory = vendor_inventory + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
    END IF;
END $$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS `vendor_item_type_inventory_add_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `vendor_item_type_inventory_add_trigger` AFTER INSERT ON `vendor_item_type`
FOR EACH ROW
BEGIN
    DECLARE ROWS_AFFECTED INT;
    INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, vendor_inventory, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.vendor_inventory, 0, now());
    SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
    IF @ROWS_AFFECTED = 0 THEN
		UPDATE item_type_inventory_snapshot set vendor_inventory = vendor_inventory + NEW.vendor_inventory, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
    END IF;
END $$

DELIMITER ;