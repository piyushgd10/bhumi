	DELIMITER $$
	
	DROP PROCEDURE IF EXISTS `setup_tenant_resources`$$
	
	CREATE DEFINER=`root`@`localhost` PROCEDURE `setup_tenant_resources`(IN from_tenant INT, IN to_tenant INT)
	BEGIN
		-- Tax type
		create table tax_type_tmp select * from tax_type where tenant_id = from_tenant;
		alter table tax_type_tmp change column id id int(10) unsigned;
		update tax_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into tax_type select * from tax_type_tmp;
		drop table tax_type_tmp;
		
		-- Tax type configuration
		create table tax_type_configuration_tmp select ttc.* from tax_type_configuration ttc, tax_type tt where ttc.tax_type_id = tt.id and tt.tenant_id = from_tenant;
		alter table tax_type_configuration_tmp change column id id int(10) unsigned;
		update tax_type_configuration_tmp ttc, tax_type tt1, tax_type tt2 set ttc.id = null, ttc.updated = now(), ttc.created = now(), ttc.tax_type_id = tt2.id where ttc.tax_type_id = tt1.id and tt1.code = tt2.code and tt2.tenant_id = to_tenant;
		insert ignore into tax_type_configuration select * from tax_type_configuration_tmp;
		insert into tax_type_configuration_range (id, tax_type_configuration_id, vat, cst, cst_formc, additional_tax, created, updated) select null, ttc.id, 0.000, 0.000, 0.000, 0.000, now(), now() from tax_type_configuration ttc join tax_type tt on ttc.tax_type_id=tt.id where tt.tenant_id = to_tenant;
		drop table tax_type_configuration_tmp;
		
		-- Shipping Provider
		create table shipping_provider_tmp select * from shipping_provider where tenant_id = from_tenant;
		alter table shipping_provider_tmp change column id id int(10) unsigned;
		update shipping_provider_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into shipping_provider select * from shipping_provider_tmp;
		drop table shipping_provider_tmp;
		
		-- Shipping Provider Parameter
		create table shipping_provider_parameter_tmp select spp.* from shipping_provider_parameter spp, shipping_provider sp where spp.shipping_provider_id = sp.id and sp.tenant_id = from_tenant;
		alter table shipping_provider_parameter_tmp change column id id int(10) unsigned;
		update shipping_provider_parameter_tmp spp, shipping_provider sp1, shipping_provider sp2 set spp.id = null, spp.shipping_provider_id = sp2.id where spp.shipping_provider_id = sp1.id and sp1.code = sp2.code and sp2.tenant_id = to_tenant;
		insert ignore into shipping_provider_parameter select * from shipping_provider_parameter_tmp;
		drop table shipping_provider_parameter_tmp;
		
		-- Shipping Provider State Mapping
		create table shipping_provider_state_mapping_tmp select spp.* from shipping_provider_state_mapping spp, shipping_provider sp where spp.shipping_provider_id = sp.id and sp.tenant_id = from_tenant;
		alter table shipping_provider_state_mapping_tmp change column id id int(10) unsigned;
		update shipping_provider_state_mapping_tmp spp, shipping_provider sp1, shipping_provider sp2 set spp.id = null, spp.updated = now(), spp.created = now(), spp.shipping_provider_id = sp2.id where spp.shipping_provider_id = sp1.id and sp1.code = sp2.code and sp2.tenant_id = to_tenant;
		insert ignore into shipping_provider_state_mapping select * from shipping_provider_state_mapping_tmp;
		drop table shipping_provider_state_mapping_tmp;
		
		-- Shipment Tracking Status Mapping
		create table shipment_tracking_status_mapping_tmp select spp.* from shipment_tracking_status_mapping spp, shipping_provider sp where spp.shipping_provider_id = sp.id and sp.tenant_id = from_tenant;
		alter table shipment_tracking_status_mapping_tmp change column id id int(10) unsigned;
		update shipment_tracking_status_mapping_tmp spp, shipping_provider sp1, shipping_provider sp2 set spp.id = null, spp.shipping_provider_id = sp2.id where spp.shipping_provider_id = sp1.id and sp1.code = sp2.code and sp2.tenant_id = to_tenant;
		insert ignore into shipment_tracking_status_mapping select * from shipment_tracking_status_mapping_tmp;
		drop table shipment_tracking_status_mapping_tmp;
		
		-- Shipping Provide Cutoff ???
		
		-- Payment reconciliation
		create table payment_reconciliation_tmp select * from payment_reconciliation where tenant_id = from_tenant;
		alter table payment_reconciliation_tmp change column id id int(10) unsigned;
		update payment_reconciliation_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into payment_reconciliation select * from payment_reconciliation_tmp;
		drop table payment_reconciliation_tmp;
		
		-- Role
		create table role_tmp select * from role where tenant_id = from_tenant;
		alter table role_tmp change column id id int(10) unsigned;
		update role_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into role select * from role_tmp;
        drop table role_tmp;
        
		-- Role access resource
		create table role_access_resource_tmp select rar.* from role_access_resource rar, role r where rar.role_id = r.id and r.tenant_id = from_tenant;
		alter table role_access_resource_tmp change column id id int(10) unsigned;
		update role_access_resource_tmp rar, role r1, role r2 set rar.id = null, rar.updated = now(), rar.created = now(), rar.role_id = r2.id where rar.role_id = r1.id and r1.code = r2.code and r2.tenant_id = to_tenant;
		insert ignore into role_access_resource select * from role_access_resource_tmp;
		drop table role_access_resource_tmp;
		
		-- Party Contact Type
		create table party_contact_type_tmp select * from party_contact_type where tenant_id = from_tenant;
		alter table party_contact_type_tmp change column id id int(10) unsigned;
		update party_contact_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into party_contact_type select * from party_contact_type_tmp;
        drop table party_contact_type_tmp;
        
        -- Party Address Type
        create table party_address_type_tmp select * from party_address_type where tenant_id = from_tenant;
		alter table party_address_type_tmp change column id id int(10) unsigned;
		update party_address_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into party_address_type select * from party_address_type_tmp;
        drop table party_address_type_tmp;
        
        -- Print Template
        insert ignore into sample_print_template select null, to_tenant, type, spt.name, description , template , number_of_copies , print_dialog , page_size , page_margins , landscape , auto_config , enabled , use_as_default , preview_image_path , now(), now() from  sample_print_template spt where spt.tenant_id = from_tenant;
        insert ignore into print_template (tenant_id, type, sample_print_template_id, enabled, created) select to_tenant, spt.type, spt.id, 1, now() from sample_print_template spt where spt.use_as_default = 1 and spt.tenant_id = to_tenant;
        
          -- Import Job Type
        create table import_job_type_tmp select * from import_job_type where tenant_id = from_tenant;
		alter table import_job_type_tmp change column id id int(10) unsigned;
		update import_job_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into import_job_type select * from import_job_type_tmp;
        drop table import_job_type_tmp;
        
        -- Export Job Type
        create table export_job_type_tmp select * from export_job_type where tenant_id = from_tenant;
		alter table export_job_type_tmp change column id id int(10) unsigned;
		update export_job_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into export_job_type select * from export_job_type_tmp;
        drop table export_job_type_tmp;
        
        -- Task
        create table task_tmp select * from task where tenant_id = from_tenant;
		alter table task_tmp change column id id int(10) unsigned;
		update task_tmp set id = null, last_exec_result = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into task select * from task_tmp;
        drop table task_tmp;
        
        -- Task Parameter
        create table task_parameter_tmp select tp.* from task_parameter tp, task t where tp.task_id = t.id and t.tenant_id = from_tenant;
		alter table task_parameter_tmp change column id id int(10) unsigned;
		update task_parameter_tmp tp, task t1, task t2 set tp.id = null, tp.task_id = t2.id where tp.task_id = t1.id and t1.name = t2.name and t2.tenant_id = to_tenant;
		insert ignore into task_parameter select * from task_parameter_tmp;
		drop table task_parameter_tmp;
		
		-- Channel
		insert ignore into channel (tenant_id, source_id, code, name, enabled, created) select to_tenant, s.id, s.code, s.name, 1, now() from source s where code = 'CUSTOM';

		-- Email Template
		create table email_template_tmp select * from email_template where tenant_id = from_tenant;
		alter table email_template_tmp change column id id int(10) unsigned;
		update email_template_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into email_template select * from email_template_tmp;
        drop table email_template_tmp;
		
        -- System Configurration
        create table system_configuration_tmp select * from system_configuration where tenant_id = from_tenant and facility_id is null;
		alter table system_configuration_tmp change column id id int(10) unsigned;
		update system_configuration_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into system_configuration select * from system_configuration_tmp;
        drop table system_configuration_tmp;
        
        -- Script Config
        create table script_config_tmp select * from script_config where tenant_id = from_tenant;
		alter table script_config_tmp change column id id int(10) unsigned;
		update script_config_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into script_config select * from script_config_tmp;
        drop table script_config_tmp;
        
        -- Category
        create table category_tmp select * from category where tenant_id = from_tenant;
		alter table category_tmp change column id id int(10) unsigned;
		update category_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		update category_tmp c, tax_type tt1, tax_type tt2 set c.tax_type_id = tt2.id where c.tax_type_id = tt1.id and tt1.code = tt2.code and tt2.tenant_id = to_tenant;
		insert ignore into category select * from category_tmp;
        drop table category_tmp;
        
        -- User Datatable View
        create table user_datatable_view_tmp select * from user_datatable_view where tenant_id = from_tenant and user_id is null;
		alter table user_datatable_view_tmp change column id id int(10) unsigned;
		update user_datatable_view_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
		insert ignore into user_datatable_view select * from user_datatable_view_tmp;
        drop table user_datatable_view_tmp;
      
        -- User Notification Type
        create table user_notification_type_tmp select * from user_notification_type where tenant_id = from_tenant;
        alter table user_notification_type_tmp change column id id int(10) unsigned;
        update user_notification_type_tmp set id = null, updated = now(), created = now(), tenant_id = to_tenant;
        insert ignore into user_notification_type select * from user_notification_type_tmp;
        drop table user_notification_type_tmp;
        
		END $$
	
	DELIMITER ;