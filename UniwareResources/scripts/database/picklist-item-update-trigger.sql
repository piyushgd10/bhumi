DELIMITER $$

DROP TRIGGER IF EXISTS `picklist_item_update_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `picklist_item_update_trigger` AFTER UPDATE ON `picklist_item`
FOR EACH ROW
BEGIN
    DECLARE SKU_CODE_VAR varchar(45);
    DECLARE CURRENT_FACILITY_ID INT;
    IF NEW.status_code != OLD.status_code THEN
        IF NEW.status_code = 'PUTBACK_ACCEPTED' THEN
          SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `picklist` WHERE id = NEW.picklist_id;
          SELECT item_type_id INTO @ITEM_TYPE_VAR FROM `item` WHERE code = NEW.item_code and facility_id = @CURRENT_FACILITY_ID;
          UPDATE item_type_inventory_snapshot set awaiting_putaway = awaiting_putaway + 1, acknowledged = 0 where item_type_id = @ITEM_TYPE_VAR and facility_id = @CURRENT_FACILITY_ID;
        END IF;
        IF NEW.status_code != OLD.status_code and NEW.status_code = 'PUTBACK_COMPLETE' AND OLD.status_code = 'PUTBACK_ACCEPTED' THEN
          SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `picklist` WHERE id = NEW.picklist_id;
          SELECT item_type_id INTO @ITEM_TYPE_VAR FROM `item` WHERE code = NEW.item_code and facility_id = @CURRENT_FACILITY_ID;
          UPDATE item_type_inventory_snapshot itis set awaiting_putaway = awaiting_putaway - 1, acknowledged = 0 where item_type_id = @ITEM_TYPE_VAR and facility_id = @CURRENT_FACILITY_ID;
        END IF;
    END IF;
END $$

DELIMITER ;
