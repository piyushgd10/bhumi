DELIMITER $$
DROP EVENT IF EXISTS `delist_channel_item_type`$$
CREATE DEFINER=`root`@`localhost` 
EVENT `delist_channel_item_type` ON SCHEDULE EVERY 1 DAY STARTS '2014-09-23 23:00:00' ON COMPLETION NOT PRESERVE ENABLE DO
BEGIN
    set @end_time = now() - interval 1 WEEK;
    update channel_item_type cit,channel c set cit.listing_status='DELISTED' where cit.channel_id=c.id and c.inventory_sync_status='OFF' and cit.last_catalog_sync_at < @end_time and cit.verified = 1;
END $$
DELIMITER ;

