	DELIMITER $$
	
	DROP PROCEDURE IF EXISTS `create_account`$$
	
	CREATE DEFINER=`root`@`localhost` PROCEDURE `create_account`(IN TENANT_CODE VARCHAR(100), IN BALANCE DECIMAL(12,2), IN PRODUCT_CODE VARCHAR(100), IN ACCOUNT_NAME varchar(100), IN EMAIL VARCHAR(100), IN MOBILE VARCHAR(100), IN PER_UNIT_CHARGES DECIMAL(12, 2))
BEGIN
    DECLARE ACCOUNT_CODE VARCHAR(100);
    SET autocommit = 0;
    SELECT * FROM sequence WHERE name = "ACCOUNT" FOR UPDATE;
    UPDATE sequence set current_value = current_value + 1 where name = "ACCOUNT";
    SET @ACCOUNT_CODE = (select concat(prefix, lpad(current_value, pad_to_length, '0')) from sequence where name = "ACCOUNT");
    INSERT INTO party VALUES (null, @ACCOUNT_CODE, ACCOUNT_NAME, EMAIL, MOBILE,now(), now());
    INSERT INTO account VALUES (NULL ,  @ACCOUNT_CODE, TENANT_CODE, TENANT_CODE, (select id FROM party where code = @ACCOUNT_CODE), 1, NULL, PER_UNIT_CHARGES, BALANCE, 0, NULL, NULL, 'ACTIVE', now(), now());
    INSERT INTO subscription SELECT null, ac.id, p.id, p.id, null, now(), NULL , 'ACTIVE', now(), now() FROM account ac, product p where ac.code = @ACCOUNT_CODE and p.code = PRODUCT_CODE;
    INSERT INTO user VALUES (NULL , @ACCOUNT_CODE, md5(rand()), 1, NULL , ACCOUNT_NAME, now(), now());
    INSERT INTO user_role SELECT null, u.id, r.id, 1, now(), now() from user u, role r where u.username = @ACCOUNT_CODE and r.code = 'CUSTOMER';
    COMMIT;
    set autocommit = 1;
END $$

DELIMITER ;