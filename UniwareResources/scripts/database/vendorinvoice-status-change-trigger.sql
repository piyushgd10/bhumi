DELIMITER $$
USE `uniware`$$

DROP TRIGGER IF EXISTS `vendor_invoice_status_change_trigger`$$

CREATE TRIGGER `vendor_invoice_status_change_trigger` AFTER UPDATE ON `vendor_invoice`
FOR EACH ROW
BEGIN
   IF NEW.status_code != OLD.status_code THEN
       SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
       INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('VendorInvoice', NEW.id, concat('PurchaseOrder-', NEW.purchase_order_id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
   END IF;
END $$

DELIMITER ;
