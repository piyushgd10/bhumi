DELIMITER $$

DROP TRIGGER IF EXISTS `shipping_package_status_update_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `shipping_package_status_update_trigger` AFTER UPDATE ON `shipping_package`
FOR EACH ROW
BEGIN
	DECLARE ITEM_COUNT INT;
	IF NEW.status_code != OLD.status_code THEN
	  SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
		INSERT INTO notification(tenant_id, facility_id, entity, identifier, group_identifier, field, old_value, new_value) values(@TENANT_ID, NEW.facility_id, 'ShippingPackage', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', OLD.status_code, NEW.status_code);
	END IF;
END $$

DELIMITER ;

DELIMITER $$
USE `uniware`$$
DROP TRIGGER IF EXISTS `create_shipping_package_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `create_shipping_package_trigger` AFTER INSERT ON `shipping_package`
FOR EACH ROW
BEGIN
   SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
   INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('ShippingPackage', NEW.id, concat('SaleOrder-', NEW.sale_order_id), 'StatusCode', 'NEW', NEW.status_code, NEW.facility_id, @TENANT_ID);
END $$
DELIMITER ;