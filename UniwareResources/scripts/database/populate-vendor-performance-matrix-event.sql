DELIMITER $$
DROP EVENT IF EXISTS `populate_vendor_performance_matrix`$$
CREATE DEFINER=`root`@`localhost` EVENT `populate_vendor_performance_matrix` ON SCHEDULE EVERY 1 DAY STARTS '2013-01-30 01:00:00' ON COMPLETION NOT PRESERVE ENABLE DO
BEGIN
    set @end_time = now() - interval 1 month;
    create temporary table t1 as select vit.id id, round(sum(hour(timediff(iri.created, po.approve_time)) * iri.quantity) / sum(iri.quantity)) lead_time from inflow_receipt ir, inflow_receipt_item iri, purchase_order po,vendor_item_type vit where iri.inflow_receipt_id = ir.id and ir.purchase_order_id = po.id and po.vendor_id = vit.vendor_id and vit.item_type_id = iri.item_type_id and po.status_code = 'COMPLETE' and po.code not like 'TRANSIT-%' and po.approve_time > @end_time group by vit.id having sum(iri.quantity) > 0;
    alter table t1 add index(id);
    update vendor_item_type vit, t1 set vit.lead_time = t1.lead_time where vit.id = t1.id;
    create temporary table t2 as select v.id id, round(sum(hour(timediff(iri.created, po.approve_time)) * iri.quantity) / sum(iri.quantity)) lead_time from inflow_receipt ir, inflow_receipt_item iri, purchase_order po,vendor v where iri.inflow_receipt_id = ir.id and ir.purchase_order_id = po.id and po.vendor_id = v.id and po.status_code = 'COMPLETE' and po.code not like 'TRANSIT-%' and po.approve_time > @end_time group by v.id having sum(iri.quantity) > 0;
    alter table t2 add index(id);
    update vendor v, t2 set v.lead_time = t2.lead_time where v.id = t2.id;
END $$
DELIMITER ;