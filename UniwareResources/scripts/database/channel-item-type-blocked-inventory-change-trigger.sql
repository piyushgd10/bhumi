DELIMITER $$

DROP TRIGGER IF EXISTS `channel_item_type_blocked_inventory_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `channel_item_type_blocked_inventory_change_trigger` AFTER UPDATE ON `channel_item_type`
FOR EACH ROW
BEGIN
    IF NEW.blocked_inventory != OLD.blocked_inventory OR NEW.failed_order_inventory != OLD.failed_order_inventory THEN
        UPDATE item_type_inventory_snapshot itis, facility f, party p set itis.acknowledged = 0 where itis.item_type_id = NEW.item_type_id and itis.facility_id = f.id and f.id = p.id and p.tenant_id = NEW.tenant_id;
    END IF;
END $$

DELIMITER ;