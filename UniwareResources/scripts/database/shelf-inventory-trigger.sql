
DELIMITER $$

USE `uniware`$$

DROP TRIGGER /*!50032 IF EXISTS */ `shelf_inventory_update_trigger`$$

CREATE
    /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER `shelf_inventory_update_trigger` AFTER UPDATE ON `item_type_inventory` 
    FOR EACH ROW BEGIN
    DECLARE ITEM_LENGTH INT;
    DECLARE ITEM_WIDTH INT;
    DECLARE ITEM_HEIGHT INT;
    DECLARE CHANGE_QUANTITY INT;
    DECLARE CHANGE_QUANTITY_BLOCKED INT;
    DECLARE CHANGE_VOLUME INT;
    DECLARE ROWS_AFFECTED INT;
    IF (NEW.quantity != OLD.quantity or NEW.quantity_blocked != OLD.quantity_blocked) and NEW.type='GOOD_INVENTORY' THEN
        -- SELECT LENGTH, width, height INTO @ITEM_LENGTH, @ITEM_WIDTH, @ITEM_HEIGHT FROM `unicatalog`.`item_type` WHERE id = NEW.item_type_id;
        SET CHANGE_QUANTITY = CAST(NEW.quantity AS SIGNED) - CAST(OLD.quantity AS SIGNED);
        SET CHANGE_QUANTITY_BLOCKED = CAST(NEW.quantity_blocked AS SIGNED) - CAST(OLD.quantity_blocked AS SIGNED);
        -- SET CHANGE_VOLUME = CHANGE_QUANTITY * FLOOR(@ITEM_LENGTH * @ITEM_WIDTH * @ITEM_HEIGHT / 1000);
        -- UPDATE shelf SET item_count = (CASE WHEN ((item_count + CHANGE_QUANTITY) > 0) THEN (item_count + CHANGE_QUANTITY) ELSE 0 END), filled_volume = (CASE WHEN ((filled_volume + CHANGE_VOLUME) > 0) THEN (filled_volume + CHANGE_VOLUME) ELSE 0 END) WHERE id = NEW.shelf_id;
        UPDATE item_type_inventory_snapshot set inventory = inventory + CHANGE_QUANTITY, inventory_blocked = inventory_blocked + CHANGE_QUANTITY_BLOCKED, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, inventory, inventory_blocked, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity, NEW.quantity_blocked, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set inventory = inventory + CHANGE_QUANTITY, inventory_blocked = inventory_blocked + CHANGE_QUANTITY_BLOCKED, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
        IF CHANGE_QUANTITY > 0 THEN
            UPDATE item_type_inventory_snapshot set reassess_version = reassess_version + 1 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        END IF;
    ELSEIF NEW.quantity != OLD.quantity  and (NEW.type='BAD_INVENTORY' or NEW.type='QC_REJECTED') THEN
        SET CHANGE_QUANTITY = CAST(NEW.quantity AS SIGNED) - CAST(OLD.quantity AS SIGNED);
        UPDATE item_type_inventory_snapshot set bad_inventory = bad_inventory + CHANGE_QUANTITY where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, bad_inventory, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity, 1, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set bad_inventory = bad_inventory + CHANGE_QUANTITY where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
    ELSEIF NEW.quantity != OLD.quantity  and (NEW.type='VIRTUAL_INVENTORY') THEN
        SET CHANGE_QUANTITY = CAST(NEW.quantity AS SIGNED) - CAST(OLD.quantity AS SIGNED);
        UPDATE item_type_inventory_snapshot set inventory = inventory + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, inventory, inventory_blocked, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity, NEW.quantity_blocked, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set inventory = inventory + CHANGE_QUANTITY, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
    END IF;
END;
$$

DROP TRIGGER IF EXISTS `shelf_inventory_insert_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `shelf_inventory_insert_trigger` AFTER INSERT ON `item_type_inventory`
FOR EACH ROW
BEGIN
    DECLARE ITEM_LENGTH INT;
    DECLARE ITEM_WIDTH INT;
    DECLARE ITEM_HEIGHT INT;
    DECLARE ROWS_AFFECTED INT;
    -- SELECT length, width, height INTO @ITEM_LENGTH, @ITEM_WIDTH, @ITEM_HEIGHT FROM `unicatalog`.`item_type` WHERE id = NEW.item_type_id;
    -- UPDATE shelf SET item_count = item_count +NEW.quantity, filled_volume = filled_volume + NEW.quantity * FLOOR(@ITEM_LENGTH * @ITEM_WIDTH * @ITEM_HEIGHT / 1000) WHERE id = NEW.shelf_id;
    IF NEW.type='GOOD_INVENTORY' THEN
	    UPDATE item_type_inventory_snapshot set inventory = inventory + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
	    SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
	    IF @ROWS_AFFECTED = 0 THEN
	        INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, inventory, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity, 0, now());
	        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
	        IF @ROWS_AFFECTED = 0 THEN
	            UPDATE item_type_inventory_snapshot set inventory = inventory + NEW.quantity, acknowledged = 0 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
	        END IF;
	    END IF;
	    UPDATE item_type_inventory_snapshot set reassess_version = reassess_version + 1 where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
	ELSEIF (NEW.type='BAD_INVENTORY' or NEW.type='QC_REJECTED')  THEN
	   UPDATE item_type_inventory_snapshot set bad_inventory = bad_inventory + NEW.quantity where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, bad_inventory, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity,1, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set bad_inventory = bad_inventory + NEW.quantity where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
    ELSEIF (NEW.type='VIRTUAL_INVENTORY')  THEN
	   UPDATE item_type_inventory_snapshot set inventory = inventory + NEW.quantity where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
        SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
        IF @ROWS_AFFECTED = 0 THEN
            INSERT IGNORE INTO item_type_inventory_snapshot(facility_id, item_type_id, inventory, acknowledged, created) values (NEW.facility_id, NEW.item_type_id, NEW.quantity, 0, now());
            SELECT ROW_COUNT() INTO @ROWS_AFFECTED;
            IF @ROWS_AFFECTED = 0 THEN
                UPDATE item_type_inventory_snapshot set inventory = inventory + NEW.quantity where item_type_id = NEW.item_type_id and facility_id = NEW.facility_id;
            END IF;
        END IF;
	END IF;
END $$
DELIMITER ;