DELIMITER //
DROP PROCEDURE if EXISTS standardToProfessional //
CREATE PROCEDURE standardToProfessional(IN clientTenantCode varchar(45), IN baseTenantCode varchar(45))
BEGIN
declare clientTenantId int(10) unsigned;
declare baseTenantId int(10) unsigned;
SET clientTenantId =   (SELECT id from tenant where code = clientTenantCode);
SET baseTenantId =   (SELECT id from tenant where code = baseTenantCode);
update tenant set product_code = 'PROFESSIONAL' where id = clientTenantId;
delete rar from role_access_resource rar, role r where rar.role_id = r.id and r.tenant_id = clientTenantId;
insert ignore into role select null, clientTenantId, r.code, r.name, level, hidden , now(), now() from role r where r.tenant_id = baseTenantId;
insert into role_access_resource select null, r2.id, rar.access_resource_id, rar.enabled, now(), now() from role r1, role_access_resource rar, role r2 where r1.code = r2.code and r1.tenant_id = baseTenantId and r2.tenant_id = clientTenantId and rar.role_id = r1.id;
END //
DELIMITER ;