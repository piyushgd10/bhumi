DELIMITER $$
DROP EVENT IF EXISTS `notification_deletion_90days`$$
CREATE DEFINER=`root`@`localhost`
EVENT `notification_deletion_90days` ON SCHEDULE EVERY 1 DAY STARTS '2016-08-12 23:00:00' ON COMPLETION NOT PRESERVE ENABLE DO
BEGIN
    delete from notification where created <  DATE_SUB(NOW(), INTERVAL 90 DAY);
END $$
DELIMITER ;
