--
-- Table structure for table `shipping_provider`
--

DELETE from `shipping_provider`;

DROP TABLE IF EXISTS `shipping_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_provider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  `handler_class` varchar(256) NOT NULL,
  `max_tracking_number_per_request` int(11) NOT NULL DEFAULT '50',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `configured` tinyint(1) NOT NULL DEFAULT '0',
  `scraper_script` text,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `integrated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_provider`
--

/*!40000 ALTER TABLE `shipping_provider` DISABLE KEYS */;
INSERT INTO `shipping_provider` VALUES 
	(1,'BLUEDART','Blue Dart','com.uniware.services.shipping.impl.BlueDartShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-03-08 18:09:09',1),
	(2,'DTDC','DTDC','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-05-02 10:35:45',1),
	(3,'AFL','AFL','com.uniware.services.shipping.impl.AFLShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-05-02 10:35:28',1),
	(4,'ARAMEX','Aramex','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-05-02 10:35:42',1),
	(5,'FF','First Flight','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',1,1,1,'<scraper name=\"firstFlightScraper\">\n	<tagexp name=\"teShipmentStatus\"><![CDATA[<tr><td>(?{statusDate})(?:</font>)?(?:</td>)?<td>(?{location})(?:</font>)?(?:</td>)?<td>(?{status})(?:</font>)?(?:</td>)?</tr></table>(?{end})]]></tagexp>\n	<regexp name=\"reTrackingNumber\"><![CDATA[Consignment Number :(?{trackingNumber}[^<]+)(?{end})]]></regexp>\n	<http url=\"http://www.firstflight.net/n_contrac_new.asp\" method=\"get\" var=\"resultPage\" timeout=\"40\">\n		<param name=\"tracking1\" value=\"#{#trackingNumbers}\" />\n	</http>\n	<if pattern=\"#{#teShipmentStatus}\" in=\"#{#resultPage}\">\n		<if pattern=\"#{#reTrackingNumber}\" in=\"#{#resultPage}\">\n			<startTag name=\"Shipment\" />\n			<valueTag name=\"TrackingNumber\" value=\"#{#trackingNumber}\" />\n			<valueTag name=\"StatusDate\" value=\"#{#statusDate}\" />\n			<valueTag name=\"Location\" value=\"#{#location}\" />\n			<valueTag name=\"Status\" value=\"#{#status}\" />\n			<endTag name=\"Shipment\" />\n		</if>\n	</if>\n</scraper>','2012-01-24 22:01:58','2012-05-02 10:35:48',1),
	(6,'DELHIVERY','Delhivery','com.uniware.services.shipping.impl.DelhiveryShipmentHandler',50,1,1,NULL,'2012-04-27 09:11:20','2012-04-27 09:45:15',1),
	(7,'GHARPAY','Gharpay','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:03','2012-04-27 09:45:15',1),
	(8,'BLUEDART_A','Bluedart Apex','com.uniware.services.shipping.impl.BlueDartShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:30','2012-04-27 09:45:15',1),
	(9,'BLUEDART_S','Bluedart Surface','com.uniware.services.shipping.impl.BlueDartShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:53','2012-04-27 09:45:15',1),
	(10,'BLUEDART_H','Bluedart High Value','com.uniware.services.shipping.impl.BlueDartShipmentHandler',50,1,1,NULL,'2012-04-27 09:13:21','2012-04-27 09:45:15',1),
	(11,'SEQUEL','Sequel','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-05-29 01:27:11',1),
	(12,'SPEED_POST','Speed post','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-05-29 01:27:11',1),
	(13,'QUANTIUM','Quantium','com.uniware.services.shipping.impl.QuantiumShipmentHandler',1,1,1,NULL,'2012-01-24 22:01:58','2012-05-29 01:27:11',1),
	(14,'BLUEDART_D','Bluedart DP','com.uniware.services.shipping.impl.BlueDartShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:53','2012-04-27 09:45:15',1),
	(15,'CHHOTU','Chhotu','com.uniware.services.shipping.impl.ChhotuShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:53','2012-04-27 09:45:15',1),
	(16,'OVERNITE','Overnite','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-04-27 09:12:53','2012-04-27 09:45:15',1),
	(17,'EARTH_MOVERS','Earth Movers','com.uniware.services.shipping.impl.FirstFlightShipmentHandler',50,1,1,NULL,'2012-01-24 22:01:58','2012-03-08 18:09:09',1);

/*!40000 ALTER TABLE `shipping_provider` ENABLE KEYS */;

--
-- Table structure for table `shipping_provider_method`
--

DROP TABLE IF EXISTS `shipping_provider_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_provider_method` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_method_id` int(10) unsigned NOT NULL,
  `shipping_provider_id` int(10) unsigned NOT NULL,
  `tracking_number_generation` enum('RANGE','LIST','CUSTOM','MANUAL') NOT NULL,
  `tracking_number_threshold` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipping_method_id` (`shipping_method_id`,`shipping_provider_id`),
  KEY `fk_shipping_provider_method_shipping_method_id1` (`shipping_method_id`),
  KEY `fk_shipping_provider_method_shipping_provider_id1` (`shipping_provider_id`),
  CONSTRAINT `fk_shipping_provider_method_shipping_method_id1` FOREIGN KEY (`shipping_method_id`) REFERENCES `shipping_method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_shipping_provider_method_shipping_provider_id1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `shipping_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_provider_method`
--

/*!40000 ALTER TABLE `shipping_provider_method` DISABLE KEYS */;
INSERT INTO `shipping_provider_method` VALUES 
(1,1,1,'LIST',10,0,'2012-01-25 11:24:28','2012-01-25 05:54:28'),
(2,2,1,'LIST',10,0,'2012-01-25 11:24:28','2012-01-25 05:54:28'),
(3,1,5,'LIST',10,0,'2012-03-15 14:50:42','2012-03-15 09:21:22'),
(4,2,5,'LIST',10,0,'2012-03-15 14:50:47','2012-03-15 09:21:22'),
(5,1,3,'LIST',10,0,'2012-04-11 13:57:36','2012-04-11 08:27:36'),
(6,2,3,'LIST',10,0,'2012-04-11 13:57:36','2012-04-11 08:27:36'),
(7,2,2,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(8,1,2,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(9,2,4,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(10,1,4,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(11,2,6,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(12,1,6,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(13,2,7,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(14,1,7,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(15,2,8,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(16,1,8,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(17,2,9,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(18,1,9,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(19,2,10,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(20,1,10,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(21,1,11,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(22,2,11,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(23,1,12,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(24,2,12,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(25,1,13,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(26,2,13,'MANUAL',0,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(27,2,14,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(28,1,15,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(29,2,15,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(30,1,16,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(31,2,16,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(32,1,14,'LIST',10,0,'2012-04-27 09:26:57','2012-04-27 09:26:57'),
(33,1,17,'LIST',10,0,'2012-01-25 11:24:28','2012-01-25 05:54:28'),
(34,2,17,'LIST',10,0,'2012-01-25 11:24:28','2012-01-25 05:54:28');

/*!40000 ALTER TABLE `shipping_provider_method` ENABLE KEYS */

--
-- Table structure for table `shipping_provider_cutoff`
--

DROP TABLE IF EXISTS `shipping_provider_cutoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_provider_cutoff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_provider_id` int(10) unsigned NOT NULL,
  `cutoff_time` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shipping_provider_cutoff_shipping_provider_id1` (`shipping_provider_id`),
  CONSTRAINT `fk_shipping_provider_cutoff_shipping_provider_id1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `shipping_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_provider_cutoff`
--

INSERT INTO `shipping_provider_cutoff` VALUES 
(null,1,'13:00:00'),
(null,2,'15:00:00'),
(null,3,'15:00:00'),
(null,4,'18:00:00'),
(null,5,'10:00:00'),
(null,8,'13:00:00'),
(null,10,'13:00:00'),
(null,9,'13:00:00'),
(null,6,'13:00:00'),
(null,7,'13:00:00'),
(null,11,'13:00:00'),
(null,12,'13:00:00'),
(null,13,'13:00:00'),
(null,14,'13:00:00'),
(null,15,'13:00:00'),
(null,16,'13:00:00'),
(null,17,'13:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

DROP TABLE IF EXISTS `shipping_provider_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_provider_parameter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_provider_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shipping_provider_parameter_shipping_provider_id1` (`shipping_provider_id`),
  CONSTRAINT `fk_shipping_provider_parameter_shipping_provider_id1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `shipping_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_provider_parameter`
--

/*!40000 ALTER TABLE `shipping_provider_parameter` DISABLE KEYS */;
INSERT INTO `shipping_provider_parameter` VALUES 
(null,(select id from shipping_provider where code = 'BLUEDART'),'SERVICE_URL','http://www.bluedart.com/servlet/RoutingServlet'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'SERVICE_URL','http://www.bluedart.com/servlet/RoutingServlet'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'SERVICE_URL','http://www.bluedart.com/servlet/RoutingServlet'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'SERVICE_URL','http://www.bluedart.com/servlet/RoutingServlet'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'SERVICE_URL','http://www.bluedart.com/servlet/RoutingServlet'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'SERVICE_URL','http://track.delhivery.com/api/packages/xml'),
(null,(select id from shipping_provider where code = 'AFL'),'SERVICE_URL','http://trackntrace.aflwiz.com/aflwiztrack'),
(null,(select id from shipping_provider where code = 'QUANTIUM'),'SERVICE_URL','http://atquantiumaspac.com/TrackerXML/TrackXML.aspx'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'AFL'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'QUANTIUM'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'AFL'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'QUANTIUM'),'LICENCE_KEY','license Key'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'LOGIN_ID','login Id'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'SERVICE_URL','http://api.chhotu.net/v1/xml/GetShipmentDetails/'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'LICENCE_KEY','license Key');
/*!40000 ALTER TABLE `shipping_provider_parameter` ENABLE KEYS */;

DELETE FROM `shipping_provider_state_mapping`;

DROP TABLE IF EXISTS `shipping_provider_state_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_provider_state_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_provider_id` int(10) unsigned NOT NULL,
  `text` varchar(45) NOT NULL,
  `state_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_sp_shipping_provider_id` (`shipping_provider_id`),
  KEY `fk_sp_state_id` (`state_id`),
  CONSTRAINT `fk_sp_shipping_provider_id` FOREIGN KEY (`shipping_provider_id`) REFERENCES `shipping_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sp_state_id` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_provider_state_mapping`
--

/*!40000 ALTER TABLE `shipping_provider_state_mapping` DISABLE KEYS */;
INSERT INTO `shipping_provider_state_mapping` VALUES 
(null,(select id from shipping_provider where code = 'BLUEDART'),'HIMACHAL PRADES',14,'2012-06-09 13:09:53','2012-06-09 07:39:53'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'HIMACHAL PRADES',14,'2012-06-09 13:09:53','2012-06-09 07:39:53'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'HIMACHAL PRADES',14,'2012-06-09 13:09:53','2012-06-09 07:39:53'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'HIMACHAL PRADES',14,'2012-06-09 13:09:53','2012-06-09 07:39:53'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'HIMACHAL PRADES',14,'2012-06-09 13:09:53','2012-06-09 07:39:53'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'JAMMU AND KASHM',15,'2012-06-09 13:10:42','2012-06-09 07:40:42'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'JAMMU AND KASHM',15,'2012-06-09 13:10:42','2012-06-09 07:40:42'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'JAMMU AND KASHM',15,'2012-06-09 13:10:42','2012-06-09 07:40:42'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'JAMMU AND KASHM',15,'2012-06-09 13:10:42','2012-06-09 07:40:42'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'JAMMU AND KASHM',15,'2012-06-09 13:10:42','2012-06-09 07:40:42'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UTTARANCHAL',34,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UTTARANCHAL',34,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UTTARANCHAL',34,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UTTARANCHAL',34,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UTTARANCHAL',34,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'DADRA AND NAGAR',8,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'DADRA AND NAGAR',8,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'DADRA AND NAGAR',8,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'DADRA AND NAGAR',8,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'DADRA AND NAGAR',8,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'DIU AND DAMAN',9,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'DIU AND DAMAN',9,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'DIU AND DAMAN',9,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'DIU AND DAMAN',9,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'DIU AND DAMAN',9,'2012-06-09 13:11:18','2012-06-09 07:41:18');

INSERT INTO `shipping_provider_state_mapping` VALUES
(null,(select id from shipping_provider where code = 'BLUEDART'),'CHHATISGARH',7,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'CHHATISGARH',7,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'CHHATISGARH',7,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'CHHATISGARH',7,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'CHHATISGARH',7,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'ANDAMAN & NICOB',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'ANDAMAN & NICOB',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'ANDAMAN & NICOB',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'ANDAMAN & NICOB',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'ANDAMAN & NICOB',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'ARUNACHAL PRADE',3,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'ARUNACHAL PRADE',3,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'ARUNACHAL PRADE',3,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'ARUNACHAL PRADE',3,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'ARUNACHAL PRADE',3,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART'),'ANDAMAN & NICOBAR ISLANDS',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'ANDAMAN & NICOBAR ISLANDS',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'ANDAMAN & NICOBAR ISLANDS',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'ANDAMAN & NICOBAR ISLANDS',1,'2012-06-09 13:11:18','2012-06-09 07:41:18'),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'ANDAMAN & NICOBAR ISLANDS',1,'2012-06-09 13:11:18','2012-06-09 07:41:18');

--
-- Table structure for table `shipment_tracking_status`
--

DROP TABLE IF EXISTS `shipment_tracking_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_tracking_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `shipping_package_status_id` int(10) unsigned NOT NULL,
  `delivery_attempted` tinyint(1) NOT NULL DEFAULT '0',
  `stop_polling` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `fk_shipment_tracking_status_shipping_package_status_id1` (`shipping_package_status_id`),
  CONSTRAINT `fk_shipment_tracking_status_shipping_package_status_id1` FOREIGN KEY (`shipping_package_status_id`) REFERENCES `shipping_package_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_tracking_status`
--

/*!40000 ALTER TABLE `shipment_tracking_status` DISABLE KEYS */;
INSERT INTO `shipment_tracking_status` VALUES 
(1,'DELIVERED_SHIPMENT_DELIVERED','Delivered',9,1,1),
(2,'PENDING_AWAITING_DELIVERY_INFORMATION','Pending - awaiting delivery information',8,0,0),
(3,'PENDING_CONSIGNEE_ADDRESS_INCOMPLETE','Pending - incomplete address',8,1,0),
(4,'PENDING_CONSIGNEE_ADDRESS_INCORRECT','Pending - incorrect address',8,1,0),
(5,'PENDING_CONSIGNEE_NOT_AVAILABLE','Pending - buyer not available',8,1,0),
(6,'PENDING_CONSIGNEE_REFUSED_TO_ACCEPT','Pending - buyer refused to accept',8,1,0),
(7,'PENDING_CONSIGNEE_REFUSED_TO_PAY','Pending - buyer refused to pay',8,1,0),
(8,'PENDING_DELAYED','Pending - delayed',8,0,0),
(9,'PENDING_IN_TRANSIT','Pending',8,0,0),
(10,'PENDING_INCORRECT_WAYBILL_NUMBER','Pending - incorrect AWB number',7,0,0),
(11,'PENDING_MISROUTED','Pending - shipment misrouted',8,1,0),
(12,'PENDING_NO_INFORMATION','Pending - no information',8,0,0),
(13,'PENDING_OUT_OF_DELIVERY_AREA','Pending - out of delivery area',8,1,0),
(14,'PENDING_PROHIBITED_AREA','Pending - prohibited area',8,1,0),
(15,'PENDING_REDIRECTED_SHIPMENT','Pending - shipment redirected',8,0,0),
(16,'PENDING_SCHEDULED_FOR_NEXT_DAY_DELIVERY','Pending - buyer company on holiday, will deliver tomorrow',8,1,0),
(17,'PENDING_SHIPMENT_CONFISCATED','Pending - shipment confiscated by authorities',8,0,0),
(18,'PENDING_SHIPMENT_HELD_AT_DESTINATION','Pending - shipment held at shippers instructi',8,1,0),
(19,'PENDING_SHIPMENT_MANIFESTED_NOT_RECEIVED','Pending - shipment not received by courier',8,0,0),
(20,'RETURN_PENDING','Returning to origin',8,1,0),
(21,'RETURNED_TO_ORIGIN','Returned back',10,1,1),
(22,'RETURNED_CONSIGNEE_ADDRESS_INCOMPLETE','Returned - consignee address incomplete',10,1,0),
(23,'RETURNED_CONSIGNEE_ADDRESS_INCORRECT','Returned - consignee address incorrect',10,1,0),
(24,'RETURNED_CONSIGNEE_NOT_AVAILABLE','Returned - consignee not available',10,1,0),
(25,'RETURNED_CONSIGNEE_REFUSED_TO_ACCEPT','Returned - consignee refused to accept',10,1,0),
(26,'RETURNED_CONSIGNEE_REFUSED_TO_PAY','Returned - consignee refused to pay',10,1,0),
(27,'SHIPMENT_DAMAGED','Damaged',8,1,1),
(28,'SHIPMENT_DESTROYED','Destroyed',8,1,1),
(29,'RETURNED_OUT_OF_DELIVERY_AREA','Returned - out of delivery area',10,1,0),
(30,'NO_INFORMATION','New Shipment Created',7,0,0);
/*!40000 ALTER TABLE `shipment_tracking_status` ENABLE KEYS */;

--
-- Table structure for table `shipment_tracking_status_mapping`
--

DROP TABLE IF EXISTS `shipment_tracking_status_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_tracking_status_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_provider_id` int(10) unsigned NOT NULL,
  `provider_status` varchar(150) NOT NULL,
  `shipment_tracking_status_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shipment_tracking_status_mapping_shipment_tracking_status_1` (`shipment_tracking_status_id`),
  KEY `fk_shipment_tracking_status_mapping_shipping_provider_id1` (`shipping_provider_id`),
  CONSTRAINT `fk_shipment_tracking_status_mapping_shipment_tracking_status_1` FOREIGN KEY (`shipment_tracking_status_id`) REFERENCES `shipment_tracking_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_shipment_tracking_status_mapping_shipping_provider_id1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `shipping_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_tracking_status_mapping`
--

/*!40000 ALTER TABLE `shipment_tracking_status_mapping` DISABLE KEYS */;
INSERT INTO `shipment_tracking_status_mapping` VALUES 
(null,(select id from shipping_provider where code = 'BLUEDART'),'DL-Shipment delivered',1),
(null,(select id from shipping_provider where code = 'BLUEDART'),'DL-Shipment Destroyed',28),
(null,(select id from shipping_provider where code = 'BLUEDART'),'DL-SHIPMENT RETURNED BACK TO SHIPPER',21),
(null,(select id from shipping_provider where code = 'BLUEDART'),'IT-In Transit. Await delivery information',2),
(null,(select id from shipping_provider where code = 'BLUEDART'),'NF-Incorrect Waybill number or No Information',10),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RD-Shipment Redirected',15),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-DL-Shipment delivered',21),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-IT-In Transit. Await delivery information',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-NF-Incorrect Waybill number or No Information',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-RD-Shipment Redirected',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-Delivery Attempted-Consignee Premises Closed',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-Out Of Delivery Area',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-Shipment Manifested - Not Received',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-Shpt Held At Destination As Per Shipper\'s Request',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Consignee Not Available At Present',5),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONSIGNEE REFUSED TO ACCEPT SHIPMENT',6),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Consignee Refusing To Pay Charges',7),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONSIGNEE\'S ADDRESS INCORRECT:UNABLE TO DELIVER',4),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONTACT CUSTOMER SERVICE',9),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Delay Caused Beyond Our Control',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Delivery Attempted-Consignee Premises Closed',5),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Delivery Delayed',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Late Arrival At Destination',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-LINEHAUL DELAYED; ACCIDENT/TRAFFIC-JAM',8),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',11),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',11),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-NEED DEPT NAME/EXTN.NO:UNABLE TO DELIVER',3),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-NO SUCH CONSIGNEE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Out Of Delivery Area',13),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Prohibited Area-Entry Restricted',14),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-RECIPIENT COMPANY ON HOLIDAY, DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-RELEASED TO 3RD PARTY:DELIVERY INFORMATION AWAITED',2),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SCHEDULED FOR DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Security Cleared',9),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SHIPMENT HELD AT HUB FOR OCTROI/TAX CLARIFICATION',17),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Shipment Held at Hub-Problem Package',18),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SHIPMENT HELD FOR TAXES/OCTROI',17),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SHIPMENT IN TRANSIT',9),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Shipment Manifested - Not Received',19),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SHIPMENT/PACKAGE DAMAGED',27),
(null,(select id from shipping_provider where code = 'BLUEDART'),'UD-Shpt Held At Destination As Per Shipper\'s Request',18);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',16);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'UD-SHPT HELD AT DESTINATION AS PER CUSTOMER\'S REQUEST',18);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CNEE ADDRESS UNLOCATABLE; UNABLE TO DELIVER',4);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4);



INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'DL-Shipment delivered',1),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'DL-Shipment Destroyed',28),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'DL-SHIPMENT RETURNED BACK TO SHIPPER',21),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'IT-In Transit. Await delivery information',2),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'NF-Incorrect Waybill number or No Information',10),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RD-Shipment Redirected',15),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-DL-Shipment delivered',21),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-IT-In Transit. Await delivery information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-NF-Incorrect Waybill number or No Information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-RD-Shipment Redirected',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-Delivery Attempted-Consignee Premises Closed',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-Out Of Delivery Area',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-Shipment Manifested - Not Received',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-Shpt Held At Destination As Per Shipper\'s Request',20),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Consignee Not Available At Present',5),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CONSIGNEE REFUSED TO ACCEPT SHIPMENT',6),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Consignee Refusing To Pay Charges',7),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CONSIGNEE\'S ADDRESS INCORRECT:UNABLE TO DELIVER',4),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CONTACT CUSTOMER SERVICE',9),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Delay Caused Beyond Our Control',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Delivery Attempted-Consignee Premises Closed',5),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Delivery Delayed',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Late Arrival At Destination',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-LINEHAUL DELAYED; ACCIDENT/TRAFFIC-JAM',8),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',11),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',11),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-NEED DEPT NAME/EXTN.NO:UNABLE TO DELIVER',3),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-NO SUCH CONSIGNEE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Out Of Delivery Area',13),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Prohibited Area-Entry Restricted',14),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-RECIPIENT COMPANY ON HOLIDAY, DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-RELEASED TO 3RD PARTY:DELIVERY INFORMATION AWAITED',2),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SCHEDULED FOR DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Security Cleared',9),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SHIPMENT HELD AT HUB FOR OCTROI/TAX CLARIFICATION',17),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Shipment Held at Hub-Problem Package',18),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SHIPMENT HELD FOR TAXES/OCTROI',17),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SHIPMENT IN TRANSIT',9),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Shipment Manifested - Not Received',19),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SHIPMENT/PACKAGE DAMAGED',27),
(null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-Shpt Held At Destination As Per Shipper\'s Request',18);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',16);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-SHPT HELD AT DESTINATION AS PER CUSTOMER\'S REQUEST',18);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CNEE ADDRESS UNLOCATABLE; UNABLE TO DELIVER',4);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_A'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4);


INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'DL-Shipment delivered',1),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'DL-Shipment Destroyed',28),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'DL-SHIPMENT RETURNED BACK TO SHIPPER',21),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'IT-In Transit. Await delivery information',2),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'NF-Incorrect Waybill number or No Information',10),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RD-Shipment Redirected',15),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-DL-Shipment delivered',21),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-IT-In Transit. Await delivery information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-NF-Incorrect Waybill number or No Information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-RD-Shipment Redirected',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-Delivery Attempted-Consignee Premises Closed',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-Out Of Delivery Area',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-Shipment Manifested - Not Received',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-Shpt Held At Destination As Per Shipper\'s Request',20),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Consignee Not Available At Present',5),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CONSIGNEE REFUSED TO ACCEPT SHIPMENT',6),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Consignee Refusing To Pay Charges',7),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CONSIGNEE\'S ADDRESS INCORRECT:UNABLE TO DELIVER',4),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CONTACT CUSTOMER SERVICE',9),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Delay Caused Beyond Our Control',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Delivery Attempted-Consignee Premises Closed',5),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Delivery Delayed',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Late Arrival At Destination',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-LINEHAUL DELAYED; ACCIDENT/TRAFFIC-JAM',8),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',11),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',11),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-NEED DEPT NAME/EXTN.NO:UNABLE TO DELIVER',3),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-NO SUCH CONSIGNEE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Out Of Delivery Area',13),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Prohibited Area-Entry Restricted',14),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-RECIPIENT COMPANY ON HOLIDAY, DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-RELEASED TO 3RD PARTY:DELIVERY INFORMATION AWAITED',2),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SCHEDULED FOR DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Security Cleared',9),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SHIPMENT HELD AT HUB FOR OCTROI/TAX CLARIFICATION',17),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Shipment Held at Hub-Problem Package',18),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SHIPMENT HELD FOR TAXES/OCTROI',17),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SHIPMENT IN TRANSIT',9),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Shipment Manifested - Not Received',19),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SHIPMENT/PACKAGE DAMAGED',27),
(null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-Shpt Held At Destination As Per Shipper\'s Request',18);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',16);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-SHPT HELD AT DESTINATION AS PER CUSTOMER\'S REQUEST',18);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CNEE ADDRESS UNLOCATABLE; UNABLE TO DELIVER',4);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_D'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4);

INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'DL-Shipment delivered',1),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'DL-Shipment Destroyed',28),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'DL-SHIPMENT RETURNED BACK TO SHIPPER',21),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'IT-In Transit. Await delivery information',2),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'NF-Incorrect Waybill number or No Information',10),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RD-Shipment Redirected',15),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-DL-Shipment delivered',21),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-IT-In Transit. Await delivery information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-NF-Incorrect Waybill number or No Information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-RD-Shipment Redirected',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-Delivery Attempted-Consignee Premises Closed',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-Out Of Delivery Area',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-Shipment Manifested - Not Received',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-Shpt Held At Destination As Per Shipper\'s Request',20),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Consignee Not Available At Present',5),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CONSIGNEE REFUSED TO ACCEPT SHIPMENT',6),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Consignee Refusing To Pay Charges',7),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CONSIGNEE\'S ADDRESS INCORRECT:UNABLE TO DELIVER',4),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CONTACT CUSTOMER SERVICE',9),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Delay Caused Beyond Our Control',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Delivery Attempted-Consignee Premises Closed',5),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Delivery Delayed',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Late Arrival At Destination',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-LINEHAUL DELAYED; ACCIDENT/TRAFFIC-JAM',8),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',11),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',11),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-NEED DEPT NAME/EXTN.NO:UNABLE TO DELIVER',3),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-NO SUCH CONSIGNEE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Out Of Delivery Area',13),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Prohibited Area-Entry Restricted',14),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-RECIPIENT COMPANY ON HOLIDAY, DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-RELEASED TO 3RD PARTY:DELIVERY INFORMATION AWAITED',2),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SCHEDULED FOR DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Security Cleared',9),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SHIPMENT HELD AT HUB FOR OCTROI/TAX CLARIFICATION',17),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Shipment Held at Hub-Problem Package',18),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SHIPMENT HELD FOR TAXES/OCTROI',17),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SHIPMENT IN TRANSIT',9),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Shipment Manifested - Not Received',19),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SHIPMENT/PACKAGE DAMAGED',27),
(null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-Shpt Held At Destination As Per Shipper\'s Request',18);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',16);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-SHPT HELD AT DESTINATION AS PER CUSTOMER\'S REQUEST',18);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CNEE ADDRESS UNLOCATABLE; UNABLE TO DELIVER',4);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_H'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4);



INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'DL-Shipment delivered',1),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'DL-Shipment Destroyed',28),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'DL-SHIPMENT RETURNED BACK TO SHIPPER',21),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'IT-In Transit. Await delivery information',2),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'NF-Incorrect Waybill number or No Information',10),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RD-Shipment Redirected',15),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-DL-Shipment delivered',21),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-IT-In Transit. Await delivery information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-NF-Incorrect Waybill number or No Information',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-RD-Shipment Redirected',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-Delivery Attempted-Consignee Premises Closed',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-Out Of Delivery Area',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-Shipment Manifested - Not Received',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-Shpt Held At Destination As Per Shipper\'s Request',20),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-BD FLIGHT DELAYED; BAD WEATHER/TECHNICAL SNAG',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-C\'NEE SHIFTED FROM THE GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Consignee Not Available At Present',5),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CONSIGNEE REFUSED TO ACCEPT SHIPMENT',6),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Consignee Refusing To Pay Charges',7),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CONSIGNEE\'S ADDRESS INCORRECT:UNABLE TO DELIVER',4),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CONTACT CUSTOMER SERVICE',9),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Delay Caused Beyond Our Control',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Delivery Attempted-Consignee Premises Closed',5),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Delivery Delayed',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-FLIGHT/VEHICLE/TRAIN; DELAYED/CANCELLED',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Late Arrival At Destination',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-LINEHAUL DELAYED; ACCIDENT/TRAFFIC-JAM',8),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',11),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-MISROUTE;INCORRECT PINCODE/CITY ENTERED',11),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-NEED DEPT NAME/EXTN.NO:UNABLE TO DELIVER',3),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-NO SUCH CONSIGNEE AT GIVEN ADDRESS',5),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Out Of Delivery Area',13),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Prohibited Area-Entry Restricted',14),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-RECIPIENT COMPANY ON HOLIDAY, DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-RELEASED TO 3RD PARTY:DELIVERY INFORMATION AWAITED',2),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SCHEDULED FOR DELIVERY NEXT DAY',16),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Security Cleared',9),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SHIPMENT HELD AT HUB FOR OCTROI/TAX CLARIFICATION',17),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Shipment Held at Hub-Problem Package',18),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SHIPMENT HELD FOR TAXES/OCTROI',17),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SHIPMENT IN TRANSIT',9),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Shipment Manifested - Not Received',19),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SHIPMENT/PACKAGE DAMAGED',27),
(null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-Shpt Held At Destination As Per Shipper\'s Request',18);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',16);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-SHPT HELD AT DESTINATION AS PER CUSTOMER\'S REQUEST',18);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-COMPANY ON HOLIDAY, DELIVERY ON NEXT WORKING DAY',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-CONSIGNEE NOT AVAILABLE AT GIVEN ADDRESS',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'RT-UD-MISROUTE DUE TO WRONG ADD/PINCODE GIVEN BY SHIPPER',20);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CNEE ADDRESS UNLOCATABLE; UNABLE TO DELIVER',4);
INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'BLUEDART_S'),'UD-CONSIGNEE\'S ADDRESS INCOMPLETE',4);


INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - PARTY NOT AVAILABLE','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - PARTY REFUSED TO ACCEPT','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - PNR','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Party Refuse To Accept','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Despatched From Origin','9'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-FAR OFF AREA','13'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-NON SERVICE STATION','13'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-PARTY REFUSED TO ACCEPT','6'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-Return to Origin','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-SCHEDULE FOR NEXT DAY DELIVERY','16'), 
(null,(select id from shipping_provider where code = 'FF'),'Return To HUB (RTH) -PARTY REFUSED TO ACCEPT','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Return To HUB (RTH) -PAYMENT NOT READY','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-House Locked','24'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Party Out Of Station','24'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Payment Nnot Ready','26'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Wrong Address','23'), 
(null,(select id from shipping_provider where code = 'FF'),'address insufficient','3'), 
(null,(select id from shipping_provider where code = 'FF'),'hold at destination (consignee\'s request)','18'), 
(null,(select id from shipping_provider where code = 'FF'),'received at hub - scheduled for next day delivery','16'), 
(null,(select id from shipping_provider where code = 'FF'),'satellite station scheduled for next day delivery','16'), 
(null,(select id from shipping_provider where code = 'FF'),'wrong address','4'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Address Insufficient','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Party Shifted','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Payment Not Ready','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Despatched From Hub','9'), 
(null,(select id from shipping_provider where code = 'FF'),'Pending-Origin Misroute','11'), 
(null,(select id from shipping_provider where code = 'FF'),'Return To HUB (RTH) -ADDRESS INSUFFICIENT','3'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Adress Insufficient','22'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-PARTY SHIFTED','24'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Party Refuse To Accept','25'), 
(null,(select id from shipping_provider where code = 'FF'),'late arrival of c/ment, schedule for next day deli','16'), 
(null,(select id from shipping_provider where code = 'FF'),'party refused to accept','6'), 
(null,(select id from shipping_provider where code = 'FF'),'Arrived At Hub','9'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Non-service Station','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Party Not Available','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Party Out of Station','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-ADDRESS INSUFFICIENT','22'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-PAYMENT NOT READY','26'), 
(null,(select id from shipping_provider where code = 'FF'),'far off area','13'), 
(null,(select id from shipping_provider where code = 'FF'),'payment not ready','7'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Non Service Station','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-PARTY NOT AVAILABLE','24'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-PARTY REFUSED TO ACCEPT','22'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) -','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Party Shifted','24'), 
(null,(select id from shipping_provider where code = 'FF'),'Departure From Destination (RTO) - Party Refused to Accept','20'), 
(null,(select id from shipping_provider where code = 'FF'),'Consignment Delivered','1'),
(null,(select id from shipping_provider where code = 'FF'),'Delivered-Consignment Delivered','1'),
(null,(select id from shipping_provider where code = 'FF'),'Pending-RECEIVED AT HUB - SCHEDULED FOR NEXT DAY DELIVERY','16'),
(null,(select id from shipping_provider where code = 'FF'),'Pending-LATE ARRIVAL OF C/MENT, SCHEDULE FOR NEXT DAY DELI','16'),
(null,(select id from shipping_provider where code = 'FF'),'Pending-SATELLITE STATION SCHEDULED FOR NEXT DAY DELIVERY','16'),
(null,(select id from shipping_provider where code = 'FF'),'Arrived At Destination Hub','9'),
(null,(select id from shipping_provider where code = 'FF'),'Pending-AWAITING ALTERNATIVE INSTRUCTIONS FROM CONSIGNOR','18'),
(null,(select id from shipping_provider where code = 'FF'),'Dispatched From Origin','9'),
(null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Far Off Area','20'),
(null,(select id from shipping_provider where code = 'FF'),'Despatched From Destination Hub','9');

INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-ADDRESS INSUFFICIENT',4);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-CONSIGNEE OUT OF STATION',5);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-DEFERRED DELIVERY CONSIGNMENT',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-DUE TO BAD WEATHER',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-HOLD AT DE',18);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-HOLD AT DESTINATION (CONSIGNEE',18);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-HOLD AT DESTINATION (CONSIGNEE\'S REQUEST)',18);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-HOUSE LOCKED',5);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-LATE ARRIV',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-LOCAL HOLIDAY',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-OFFICE CLOSED',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-PARTY NOT AVAILABLE',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-PAYMENT NOT READY',26);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Pending-SCHEDULED FOR NEXT DAY DELIVERY',16);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'FF'),'Return to Origin-Non Service Station',20);

INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'DELHIVERY'),'DL-Delivered','1'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'RT-Delivered','21'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'RT-Returned','21'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'RTO-RTO','20'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'UD-Pending','9'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'UD-Dispatched','9'),
(null,(select id from shipping_provider where code = 'DELHIVERY'),'UD-In Transit','9');




INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'CHHOTU'),'Delivered','1'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Not Picked','19'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Cancelled','20'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Picked','9'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'In Transit','9'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Returned','21'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Returned to Partner','21'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'Retry','16'),
(null,(select id from shipping_provider where code = 'CHHOTU'),'On Hold','18');


INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'AFL'),'OH-On Hold',18),
(null,(select id from shipping_provider where code = 'AFL'),'FD-Fwd to Third Party Delivery Agent Del',9),
(null,(select id from shipping_provider where code = 'AFL'),'OK-Delivery',1),
(null,(select id from shipping_provider where code = 'AFL'),'DF-Depart Facility',9),
(null,(select id from shipping_provider where code = 'AFL'),'RT-Returned to Consignor',21),
(null,(select id from shipping_provider where code = 'AFL'),'RD-Refused Delivery',15),
(null,(select id from shipping_provider where code = 'AFL'),'CC-Awaiting Consignee Collection',9),
(null,(select id from shipping_provider where code = 'AFL'),'AR-Arrival at Delivery Facility',9),
(null,(select id from shipping_provider where code = 'AFL'),'CA-Closed on Arrival',5),
(null,(select id from shipping_provider where code = 'AFL'),'NH-Not Home',5),
(null,(select id from shipping_provider where code = 'AFL'),'AD-Alternate delivery date',16),
(null,(select id from shipping_provider where code = 'AFL'),'HP-Held for Payment',7),
(null,(select id from shipping_provider where code = 'AFL'),'AF-Arrived Facility',9),
(null,(select id from shipping_provider where code = 'AFL'),'BA-Bad Address',5),
(null,(select id from shipping_provider where code = 'AFL'),'PU-Shipment Pick Up',9),
(null,(select id from shipping_provider where code = 'AFL'),'WC-With Delivering Courier',9),
(null,(select id from shipping_provider where code = 'AFL'),'SS-Shipment Stopped',20);
INSERT INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'AFL'),'CM-Consignee Moved',20);
INSERT INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'AFL'),'SK-STRIKES',20);


INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'QUANTIUM'),'Delivery Successful',1),
(null,(select id from shipping_provider where code = 'QUANTIUM'),'Delivery In Progress',9),
(null,(select id from shipping_provider where code = 'QUANTIUM'),'Delivery UnSuccessful',21);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'QUANTIUM'),'Origin Shipped',9);

INSERT INTO `shipment_tracking_status_mapping` VALUES
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket Delivered Sucessfully',1),
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket arrived at Destination City',9),
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket Picked Up at Origin',9),
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket arrived at orgin hub',9),
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket arrived at Destination hub',9),
(null,(select id from shipping_provider where code = 'SEQUEL'),'Docket Picked Up at Origin',9);

INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'SEQUEL'),'Docket arrived at origin hub',9);


INSERT INTO `shipment_tracking_status_mapping` VALUES 
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHOR-Shipment Origin Received',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHMF-Shipment forwarded AT HUB',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHIB-Shipment Inbound Scanned',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHMF-Shipment Manifested',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHOD-Shipment OUT FOR Delivery',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHMR-Shipment Misroute',11),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHUD-Shipment NOT Delivered',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHSP-Shipment ON Hold',18),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHOH-Shipment ON-hold',18),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHDL-Shipment Delivered',1),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHRH-Shipment RELEASE Hold',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHUP-Document Returned TO Shipper',21),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'Others-Others',2),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHMC-Shipment Manifest created ',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHCR-Shipment Record Created',30),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'SHTU-Shipment OUT FOR Delivery',9),
(NULL,(SELECT id FROM shipping_provider WHERE CODE = 'ARAMEX'),'COMM-Contacted Through Bulk SMS',9);


INSERT IGNORE INTO `shipment_tracking_status_mapping` VALUES
(NULL, (SELECT id FROM shipping_provider WHERE CODE = 'JAVAS'),'Delivered','1'),
(NULL, (SELECT id FROM shipping_provider WHERE CODE = 'JAVAS'),'RTO','20'),
(NULL, (SELECT id FROM shipping_provider WHERE CODE = 'JAVAS'),'NDR','9'),
(NULL, (SELECT id FROM shipping_provider WHERE CODE = 'JAVAS'),'In Transit','9');

INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'DELIVERED',1);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'OUT FOR DELIVERY',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'RTO IN TRANSIT',20);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'FDM MANIFEST PREPARED',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Arrived at DTDC Facility-IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Processed&Despatched from DTDC APEX-IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Arrived at DTDC Facility-OUT FOR DELIVERY',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Shipment Despatched for Delivery-RTO IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Processed&Despatched from DTDC APEX-RTO IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Shipment Despatched for Delivery-IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Shipment Returned from DTDC Local Facility-FDM MANIFEST PREPARED',20);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Shipment RTO in Transite-OUT FOR DELIVERY',20);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Processed&Despatched from DTDC APEX-FDM MANIFEST PREPARED',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Arrived at DTDC APEX-IN TRANSIT',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Arrived at DTDC Facility-FDM MANIFEST PREPARED',9);
INSERT IGNORE  INTO `shipment_tracking_status_mapping` VALUES (null,(select id from shipping_provider where code = 'DTDC'),'Processed&Despatched from DTDC Facility-IN TRANSIT',9);
