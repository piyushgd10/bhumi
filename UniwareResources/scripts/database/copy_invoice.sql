DELIMITER $$
DROP PROCEDURE IF EXISTS `copy_invoice`$$
CREATE DEFINER =`root`@`localhost` PROCEDURE `copy_invoice`(IN FROM_INVOICE_CODE VARCHAR(45), IN TO_INVOICE_CODE VARCHAR(45), IN FROM_FACILITY_ID INT)
  BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE FROM_INVOICE_ITEM_ID INT;
    DECLARE TO_INVOICE_ITEM_ID INT;
    DECLARE cur1 CURSOR FOR SELECT id FROM invoice_item WHERE invoice_id = @FROM_INVOICE_ID;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    SET autocommit = 0;

    SET @FROM_INVOICE_ID = (SELECT id FROM invoice WHERE code = FROM_INVOICE_CODE AND facility_id = FROM_FACILITY_ID);
    SELECT @FROM_INVOICE_ID;
    INSERT INTO invoice SELECT NULL, facility_id, TO_INVOICE_CODE, user_id, from_party_id, to_party_id, trashed, created, updated FROM invoice WHERE id = @FROM_INVOICE_ID;
    SET @TO_INVOICE_ID = (SELECT id FROM invoice WHERE code = TO_INVOICE_CODE AND facility_id = FROM_FACILITY_ID);
    SELECT @TO_INVOICE_ID;
    OPEN cur1;
    invoice_item_loop: LOOP
      FETCH cur1 INTO FROM_INVOICE_ITEM_ID;
      IF done THEN
        LEAVE invoice_item_loop;
      END IF;
      INSERT INTO invoice_item SELECT NULL, @TO_INVOICE_ID, item_type_id, product, description, channel_product_id, seller_sku_code, additional_info, item_details, unit_price, quantity, subtotal, discount, tax_type_code, tax_percentage, vat, cst, service_tax, additional_tax_percentage, additional_tax, shipping_charges, shipping_method_charges, cash_on_delivery_charges, gift_wrap_charges, total, prepaid_amount, voucher_value, store_credit FROM invoice_item WHERE id = FROM_INVOICE_ITEM_ID;
      SELECT last_insert_id() INTO TO_INVOICE_ITEM_ID;
      SELECT TO_INVOICE_ITEM_ID;
      INSERT INTO invoice_sale_order_item SELECT NULL, TO_INVOICE_ITEM_ID, sale_order_item_id, created, updated FROM invoice_sale_order_item WHERE invoice_item_id = FROM_INVOICE_ITEM_ID;
    END LOOP;
    INSERT INTO shipping_package SELECT NULL, tenant_id, facility_id, concat(code, "T"), shipping_package_type_id, pick_set_id, pick_sections, pick_items, sale_order_id, shipping_method_id, shipping_address_id, NULL, "CANCELLED", shipment_tracking_status_code, provider_status_code, 1, splittable, @TO_INVOICE_ID, estimated_weight, actual_weight, box_length, box_width, box_height, total_price, collectable_amount, collected_amount, payment_reconciled, selling_price, discount, shipping_charges, shipping_method_charges, cash_on_delivery_charges, gift_wrap_charges, priority, NULL, NULL, no_of_items, no_of_boxes,pod_code, NULL, NULL, NULL,regulatory_form_codes, shipment_label_format, NULL, NULL, NULL, requires_customization, NULL, payment_reference_number, shipping_manager, dispatched_on_channel, shipment_batch_code, manual_awb_allocation, putaway_pending, label_synced, created, updated FROM shipping_package WHERE invoice_id = @FROM_INVOICE_ID;
    SELECT id from shipping_package where invoice_id=@TO_INVOICE_ID;
    COMMIT;
  END $$
DELIMITER ;