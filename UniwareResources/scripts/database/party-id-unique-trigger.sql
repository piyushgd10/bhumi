DELIMITER $$
DROP TRIGGER IF EXISTS `party-id-unique-trigger`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `party-id-unique-trigger` BEFORE INSERT ON `party`
FOR EACH ROW
BEGIN
    SET NEW.id_unique = concat_ws('-', NEW.tenant_id, NEW.facility_id, NEW.code);
END $$
DELIMITER ;