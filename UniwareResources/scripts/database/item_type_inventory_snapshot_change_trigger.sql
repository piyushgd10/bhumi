
DELIMITER $$

DROP TRIGGER IF EXISTS `item_type_inventory_snapshot_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `item_type_inventory_snapshot_change_trigger` AFTER UPDATE ON `item_type_inventory_snapshot`
FOR EACH ROW
BEGIN
    DECLARE ROWS_AFFECTED INT;
    DECLARE ITEM_COUNT INT;
    DECLARE NOT_CANCELLED_ITEM_COUNT INT;
    IF NEW.inventory != OLD.inventory THEN
        IF NEW.inventory = 0 THEN
            SET @TENANT_ID = (SELECT tenant_id FROM party where id = NEW.facility_id);
            SET @SKU_CODE = (SELECT sku_code FROM item_type where id = NEW.item_type_id);
            INSERT INTO stockout_inventory_ledger(tenant_id, facility_id, sku_code, transaction_type, identifier, old_value, new_value, statusCode) values(@TENANT_ID, NEW.facility_id, @SKU_CODE, 'OUT_OF_STOCK', concat('ItemType-', NEW.item_type_id), NEW.inventory, OLD.inventory, 'NEW');
        END IF;
        IF OLD.inventory = 0 THEN
            SET @TENANT_ID = (SELECT tenant_id FROM party where id = NEW.facility_id);
            SET @SKU_CODE = (SELECT sku_code FROM item_type where id = NEW.item_type_id);
            INSERT INTO stockout_inventory_ledger(tenant_id, facility_id, sku_code, transaction_type, identifier, old_value, new_value, statusCode) values(@TENANT_ID, NEW.facility_id, @SKU_CODE, 'IN_STOCK', concat('ItemType-', NEW.item_type_id), NEW.inventory, OLD.inventory, 'NEW');
        END IF;
    END IF;
END $$
DELIMITER ;

DELIMITER $$
DROP TRIGGER IF EXISTS `create_item_type_inventory_snapshot_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `create_item_type_inventory_snapshot_trigger` AFTER INSERT ON `item_type_inventory_snapshot`
FOR EACH ROW
BEGIN
   SET @TENANT_ID = (SELECT tenant_id FROM party where id = NEW.facility_id);
   SET @SKU_CODE = (SELECT sku_code FROM item_type where id = NEW.item_type_id);
   INSERT INTO stockout_inventory_ledger(tenant_id, facility_id, sku_code, transaction_type, identifier, old_value, new_value, statusCode) values(@TENANT_ID, NEW.facility_id, @SKU_CODE, 'IN_STOCK', concat('ItemType-', NEW.item_type_id), NEW.inventory, OLD.inventory, 'NEW');
END $$
DELIMITER ;


