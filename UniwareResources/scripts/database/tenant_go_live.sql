DELIMITER $$
DROP PROCEDURE IF EXISTS `tenant_go_live`$$
CREATE DEFINER =`root`@`localhost` PROCEDURE `tenant_go_live`(IN from_tenant INT, IN clear_awb BOOLEAN)
  BEGIN
    DELETE FROM address_detail where tenant_id = from_tenant;
    DELETE FROM sale_order WHERE tenant_id = from_tenant;
    DELETE ogs.* FROM outbound_gate_pass ogs, facility f, party p WHERE ogs.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE ir.* FROM inflow_receipt ir, facility f, party p WHERE ir.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE asn.* FROM advance_shipping_notice asn, facility f, party p WHERE asn.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE po.* FROM purchase_order po, facility f, party p WHERE po.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE i.* FROM item i, item_type it WHERE i.item_type_id = it.id AND it.tenant_id = from_tenant;
    DELETE t.* FROM putaway t, facility f, party p WHERE p.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE sm.* FROM shipping_manifest sm, facility f, party p WHERE sm.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE i.* FROM invoice i, facility f, party p WHERE i.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE st.* FROM shipment_tracking st, facility f, party p WHERE st.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE t.* FROM picklist t, facility f, party p WHERE t.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;

    DELETE nt.* FROM notification nt LEFT JOIN facility f ON (nt.facility_id = f.id) LEFT JOIN party p ON (f.id = p.id) WHERE nt.tenant_id = from_tenant OR p.tenant_id = from_tenant;
    DELETE t.* FROM sale_order_flow_summary t, facility f, party p WHERE t.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE ij.* FROM import_job ij JOIN user u ON (ij.user_id = u.id) WHERE u.tenant_id = from_tenant;
    DELETE ij.* FROM onetime_task ij JOIN user u ON (ij.user_id = u.id) WHERE u.tenant_id = from_tenant;
    DELETE ij.* FROM export_job ij JOIN user u ON (ij.user_id = u.id) WHERE u.tenant_id = from_tenant;
    DELETE t.* FROM item_type_inventory t, facility f, party p WHERE t.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE t.* FROM return_manifest t, facility f, party p WHERE t.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE uc.* FROM user_comment uc JOIN user u ON (uc.user_id = u.id) WHERE u.tenant_id = from_tenant;
    DELETE t.* FROM inventory_adjustment t, facility f, party p WHERE t.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    DELETE pu.* FROM putaway pu, facility f, party p WHERE pu.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;

    DELETE FROM item_type WHERE tenant_id = from_tenant and sku_code !='unknown-sellerSkuCode';
    DELETE FROM channel_item_type WHERE tenant_id = from_tenant;
    UPDATE sequence SET current_value = 0 WHERE tenant_id = from_tenant;
    IF clear_awb THEN
      DELETE sptn.* FROM shipping_provider_tracking_number sptn, facility f, party p WHERE sptn.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    ELSE
      DELETE sptn.* FROM shipping_provider_tracking_number sptn, facility f, party p WHERE sptn.used = 1 AND sptn.facility_id = f.id AND f.id = p.id AND p.tenant_id = from_tenant;
    END IF;

    UPDATE task SET last_exec_time = NULL, last_exec_result = NULL WHERE tenant_id = from_tenant;
    DELETE ot.*  FROM onetime_task ot, user u WHERE ot.user_id = u.id AND u.tenant_id = from_tenant;
  END $$
DELIMITER ;