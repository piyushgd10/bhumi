DELIMITER $$
USE `uniware`$$
DROP TRIGGER IF EXISTS `create_sale_order_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `create_sale_order_trigger` AFTER INSERT ON `sale_order`
FOR EACH ROW
BEGIN
   INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, tenant_id) values('SaleOrder', NEW.id, concat('SaleOrder-', NEW.id), 'StatusCode', 'NEW', NEW.status_code, NEW.tenant_id);
END $$

DROP TRIGGER /*!50032 IF EXISTS */ `sale_order_status_change_trigger`$$

CREATE TRIGGER `sale_order_status_change_trigger` AFTER UPDATE ON `sale_order`
FOR EACH ROW
BEGIN
	IF NEW.status_code != OLD.status_code THEN
	   INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, tenant_id) values('SaleOrder', NEW.id, concat('SaleOrder-', NEW.id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.tenant_id);
	END IF;
END $$

DELIMITER ;

