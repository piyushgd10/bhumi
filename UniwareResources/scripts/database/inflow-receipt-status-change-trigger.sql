DELIMITER $$

DROP TRIGGER IF EXISTS `inflow_receipt_item_status_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `inflow_receipt_item_status_change_trigger` AFTER UPDATE ON `inflow_receipt_item`
FOR EACH ROW
BEGIN
    DECLARE CURRENT_FACILITY_ID INT;
    IF NEW.status_code != OLD.status_code and NEW.status_code = 'COMPLETE' and (OLD.status_code = 'QC_COMPLETE' or OLD.status_code = 'QC_PENDING') THEN
	    SELECT facility_id INTO @CURRENT_FACILITY_ID FROM `inflow_receipt` WHERE id = NEW.inflow_receipt_id;
        UPDATE item_type_inventory_snapshot set open_purchase = open_purchase - NEW.quantity, acknowledged=0 where item_type_id = NEW.item_type_id and facility_id = @CURRENT_FACILITY_ID;
    END IF;
END $$

DROP TRIGGER IF EXISTS `inflow_receipt_status_change_trigger`$$

CREATE DEFINER=`root`@`localhost` TRIGGER `inflow_receipt_status_change_trigger` AFTER UPDATE ON `inflow_receipt`
FOR EACH ROW
BEGIN
    IF NEW.status_code != OLD.status_code THEN
        SET @TENANT_ID = (select tenant_id from party where id = NEW.facility_id);
        INSERT INTO notification(entity, identifier, group_identifier, field, old_value, new_value, facility_id, tenant_id) values('InflowReceipt', NEW.id, concat('PurchaseOrder-', NEW.purchase_order_id), 'StatusCode', OLD.status_code, NEW.status_code, NEW.facility_id, @TENANT_ID);
    END IF;
END $$


DELIMITER ;