silentPrint = app.trustedFunction (
	function() {
		app.beginPriv();
		this.print({bUI:false, bSilent:true, bShrinkToFit:true});
		app.endPriv();
	}
)