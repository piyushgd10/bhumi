#!/usr/bin/python

import os
import subprocess
import concurrent.futures
from datetime import datetime
from datetime import timedelta
import csv
import sys, getopt
from pyelasticsearch import ElasticSearch

es = ElasticSearch("http://elasticsearch1.unicommerce.infra:9200")

# Function
def index_item_types(server, all):
    dt = datetime.now()
    file_name = '/tmp/' + server + '.csv'
    print 'Dumping updated item types to file ' + file_name
    min_delta = timedelta(minutes=duration)
    given_time_back = (dt - min_delta).strftime('%Y-%m-%d %H:%M:%S')
    given_time_later = (dt + min_delta).strftime('%Y-%m-%d %H:%M:%S')
    if all == '1' :
        print "******** indexing all entries"
        p1 = subprocess.Popen(["mysql -h" + server + " -uroot -puniware uniware -e \"select concat(t.code, '-', it.id) as id, t.code as tenant_code, it.sku_code as sku_code, it.scan_identifier as scan_identifier, it.type as type, it.name as name, it.enabled as enabled, it.ean as ean, it.upc as upc, it.isbn as isbn, it.max_retail_price as max_retail_price, it.base_price as base_price, it.image_url as image_url, it.created as created, it.updated as updated  from item_type it, tenant t where it.tenant_id = t.id and it.updated < '" + given_time_later + "'\" >" + file_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
    else:
        print "********* indexing recent entries"
        p1 = subprocess.Popen(["mysql -h" + server + " -uroot -puniware uniware -e \"select concat(t.code, '-', it.id) as id, t.code as tenant_code, it.sku_code as sku_code, it.scan_identifier as scan_identifier, it.type as type, it.name as name, it.enabled as enabled, it.ean as ean, it.upc as upc, it.isbn as isbn, it.max_retail_price as max_retail_price, it.base_price as base_price, it.image_url as image_url, it.created as created, it.updated as updated  from item_type it, tenant t where it.tenant_id = t.id and it.updated > '" + given_time_back + "'\" >" + file_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
    p1.wait()
    print 'Done dumping updated item types for ' +  server
    print 'indexing...'
    with open(file_name, 'rb') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        i = 0
        documents = []
        for row in reader:
            doc = {
                'tenantCode': row['tenant_code'],
                'skuCode' : row['sku_code'],
                'scanIdentifier' : row['scan_identifier'],
                'type' : row['type'],
                'id' : row['id'],
                'name' : row['name'],
                'enabled' : True if row['enabled'] == "1" else False,
                'ean' : row['ean'] if row['ean'] != "NULL" else None,
                'upc' : row['upc'] if row['upc'] != "NULL" else None,
                'isbn' : row['isbn'] if row['isbn'] != "NULL" else None,
                'maxRetailPrice' : float(row['max_retail_price']) if row['max_retail_price'] != "NULL" else None,
                'basePrice' : float(row['base_price']) if row['base_price'] != "NULL" else None,
                'imageUrl' : row['image_url'] if row['image_url'] != "NULL" else None,
                'created' : row['created'],
                'updated' : row['updated'],
            }
            documents.append(doc)
            i += 1
            if (i%1000 == 0):
                res = es.bulk((es.index_op(doc, id=doc.get('id')) for doc in documents), index="uniware", doc_type='item_type')
                documents = []

        if(len(documents) > 0):
            res = es.bulk((es.index_op(doc, id=doc.get('id')) for doc in documents), index="uniware", doc_type='item_type')

        print 'indexing completed!!'
        print 'total time : ' + str(datetime.now() - dt)
        print 'total indexed items : ' + str(i)

        msg = 'Message : total time : ' + str(datetime.now() - dt) + ', total indexed items : ' + str(i)
        message_path = '/tmp/msg'
        os.system('echo ' + msg + ' > ' + message_path)

        email = 'bhuvneshwar@unicommerce.com'
        linuxCMD = 'mutt -s "ElasticSearch Indexing" %s < %s' % (email, message_path)
        os.system(linuxCMD)


# Defaults
db_servers_file = '/usr/etc/dbservers.conf'
duration = 24
server_name = None
all = '0'

try:
    opts, args = getopt.getopt(sys.argv[1:],"hf:d:s:a",["file=","duration=","server=", "all"])
except getopt.GetoptError:
    print 'elasticsearch_indexer.py -f <db_server_file> -d <duration_in_hours> -s <server_name> -a'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print 'elasticsearch_indexer.py -f <db_server_file> -d <duration_in_hours> -s <server_name> -a'
        sys.exit()
    elif opt in ("-f", "--file"):
        db_servers_file = arg
    elif opt in ("-d", "--duration"):
        duration = int(arg)
    elif opt in ("-s", "--server"):
        server_name = arg
    elif opt in ("-a", "--all"):
        all = '1'


print 'Duration: ' + str(duration) + ' minutes'


if (server_name is None or server_name == ''):
    # Calling servers.conf to get list of Servers
    print 'DB file: ' + db_servers_file
    serverscript = subprocess.Popen(["cat " + db_servers_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
    server_data = serverscript.communicate()[0]
    SERVERS = []
    future_to_server = {}
    lines = iter(server_data.splitlines())
    for line in lines:
        SERVERS.append(line)
    print 'Done getting server list'
    with concurrent.futures.ThreadPoolExecutor(max_workers=40) as executor:
        # Start the load operations
        for server in SERVERS:
            future_to_server[executor.submit(index_item_types, server, all)] =  server

        for future in concurrent.futures.as_completed(future_to_server):
            server = future_to_server[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (server, exc))
            else:
                print('%s' % (data))
else:
    print 'Indexing item types for server ' + server_name
    index_item_types(server_name, all)