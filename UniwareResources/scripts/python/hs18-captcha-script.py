#!/usr/bin/python
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import urllib, urllib2, demjson, os, shutil, requests
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

display = Display(visible=0, size=(800, 600))
display.start()

#
# Function to read captcha image url from HS18
#

def getHS18CaptchaImage():
	browser = webdriver.Firefox()
	browser.get('http://merchant.homeshop18.com/dfc-web/jsp/welcome.xhtml')
	try:
		elem = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.ID, "recaptcha_challenge_image")))
		url = elem.get_attribute('src')
		if not url :
	   		print "ERROR : No captcha found on HS18"
		else :
	   		print "Captcha Image Url : " + url
	   		solveCaptchaAndSave(url)
	finally:
		browser.quit()
		display.stop()

#
# Function to Solve the Captcha using decaptcha API and saving in mongo
#

def solveCaptchaAndSave(url):

	url_params = url.split('?c=')
	challenge_id = url_params[1].split('&th=')[0]
	print "Challenge Id : " + challenge_id

	print "Downloading Captcha to File"
	file = requests.get(url, stream=True)
	dump = file.raw
	location = os.path.abspath("file.jpg")
	with open("file.jpg", 'wb') as location:
	     shutil.copyfileobj(dump, location)
	del dump

	print "Solving Captcha"
	register_openers()
	datagen, headers = multipart_encode({"function":"picture2","username":"kapil@unicommerce.com","password":"sunny@123","pict_to":"0","pict_type":"0","submit":"send","pict": open("file.jpg", "rb")})
	request = urllib2.Request("http://poster.de-captcher.com/", datagen, headers)
	captcha_repsonse =  urllib2.urlopen(request).read()
	captcha_repsonse_array = captcha_repsonse.split('|')
	challengeText = captcha_repsonse_array[len(captcha_repsonse_array)-1]
	print "Captcha Response : " + challengeText

	print "Saving captcha"
	if challengeText != '' :
		req_url = "http://www.unicommerce.com/client/captcha/save"
		captcha = {"captchaDTO":{"imageUrl":url,"challengeId":challenge_id,"challengeText":challengeText,"used":False}}
		req_data =  demjson.encode(captcha)
		req = urllib2.Request(req_url, req_data)
		req.add_header('Content-Type', 'application/json')
		rsp = urllib2.urlopen(req)
		content = rsp.read()
	else :
		print "Not saving blank captcha"

getHS18CaptchaImage()